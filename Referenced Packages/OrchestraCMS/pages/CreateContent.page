<apex:page controller="cms.CreateContentController" id="createContent" showHeader="false" sidebar="false" cache="false" title="Orchestra CMS" standardStylesheets="false" action="{!initializeInterface}">
    <head>

        <apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms.css')}" />
        <!-- The file is not included in static resources. -->
        <!-- <apex:stylesheet value="{!URLFOR($Resource.OrchestraCMSUI, '/selector/color/css/jPicker-1.1.6.min.css')}" /> -->

        <c:CmsScripts />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/TabInterface.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/dam/LibraryManagerDlgs.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/dam/LibraryManager.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/dam/LibraryBrowser.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__ckedit,         'ckeditor/ckeditor.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/selector/page/PageSelector.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/selector/link/LinkSelector.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/selector/link/LinkSelectorDialog.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/selector/color/jpicker-1.1.6.min.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, IF(showActionBar, '/editor/CreateContent.js', '/CreateContentInterface.js'))}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/editor/TagEditor.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/AccessDialogs.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/multilingual/LanguageManager.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/selector/AccessLevelSelector.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/selector/TargetSelector.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/selector/RestrictionGroupSelector.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/selector/RestrictionCustomSelector.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/selector/PrioritySelector.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/widget/JQueryFullDialog.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, 'VersionHistory.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSPublic, '/OCMSSecureUrlLoad.js')}" />
        <apex:includeScript value="{!IF(showActionBar, URLFOR($Resource.cms__OrchestraCMSUI, '/CreateContentScripts.js'), '')}" />
        <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/OrchestraEditorAPI.js')}" />

        <script type="text/javascript">
            var uiData = JSON.parse($.orchestracmsUtil.base64Decode('{!uiDataJSONBase64}'));

            var uiAccess = uiData.access;
            var uiContent = uiData.content;
            var uiSettings = uiData.ui;
            var ocmsEditorAPI = new OrchestraEditorAPI(uiData.orchestraEditorAPI);

            // Fix for dialog positioning
            CKEDITOR.on('dialogDefinition', function(dialogEvent){
                var dialogName = dialogEvent.data.name;
                var dialog = dialogEvent.data.definition.dialog;

                dialog.on('show', function() {
                    this.move(this.getPosition().x, $(this.getParentEditor().container.$).offset().top);
                });
            });

            // Set the z index when opening, in order to allow stacking amongst jqueryui dialogs if necessary.
            CKEDITOR.on('instanceCreated', function(e) {
                e.editor.config.baseFloatZIndex = $(e.editor.element.$).zIndex();

                // Manage the z-index on a maximize event so that the editor doesn't appear underneath things.
                e.editor.on('maximize',function(maximizeEvent) {
                    maximizeEvent.editor.container.getChild(1).setStyles({'z-index': maximizeEvent.editor.config.baseFloatZIndex});
                });
                
                // Additional unicode entities go here
                // #8209 - No-break hyphen
                var existingEntities = e.editor.config.entities_additional;
                var extraOCMSEntities = '#8209'; // Comma-delimited String ex. '#8209,#1234,#5678'
                e.editor.config.entities_additional = existingEntities + (existingEntities.length > 0 ? ',' : '') + extraOCMSEntities;
            });

            /** Holds all the language information for the content as an object.  All frames reference this object. **/
            var allLanguageInformation = {};
            if (window.parent !== undefined && window.parent.allLanguageInformation !== undefined) {
                allLanguageInformation = window.parent.allLanguageInformation;
            } else {
                allLanguageInformation = uiContent.allLanguageInformation;
            }

            // Things to do if this is a child instance of the create content page.
            if (uiAccess.showActionBar !== true) {

                var $jParent = window.parent.$;
                var $thisFrame = $(window.frameElement);

                //need global scope so we can clean up
                var unlockOnClose;
                var updateAvailableLanguagesSubscriber;
                var removeUpdateAvailableLanguagesSubscriber;
                var availableLangFunc;
                var unlockFunc;
                var removeUpdateFunc;
                var ce;
            }


          $(document).data('cms', {
              context         : 'orchestracms',
              namespace       : uiSettings.namespace,
              prefix          : uiSettings.prefix,
              csrf_token      : uiSettings.token,
              site_name       : uiSettings.sname,
              translationView : uiAccess.multilingualAccessLevel,
              createManagePage: uiAccess.createManagePagePermission,
              languageInUse   : uiContent.languageInUse,
              disableAll      :  uiAccess.disableAll,
              allowPublicTags   : uiAccess.showPublicTags,
              allowPersonalTags : uiAccess.showPersonalTags,
              allowPrivateShare : uiAccess.showShare,
              isMultiLingual     : uiAccess.showLanguages
          });
          var cms = $(document).data('cms');
        
          function resizeContentEditorArea(){
              /*
              var cntEditorPanel = (typeof parent.resizeContentEditorArea == 'function') ? $jParent('.ocms-content-editor-panel, .ocms-content-translate-panel'): $('.ocms-content-editor-panel, .ocms-content-translate-panel');
              var eph1 = cntEditorPanel.css('padding-top');
              var eph2 = cntEditorPanel.css('padding-bottom');
              if (eph1 !== null && eph1 !== undefined)
                  eph1 = parseInt(eph1.substring(0,eph1.length-2));
              else
                  eph1 = 0;
              if (eph2 !== null && eph2 !== undefined)
                  eph2 = parseInt(eph2.substring(0,eph2.length-2));
              else
                  eph2 = 0;
              $('#contentEditBlock').height(($('body').outerHeight(true))-($('#contentEditActionBar').outerHeight(true) + $('.contentEditForm').outerHeight(true))-(eph1+ eph2));
              */
          }
        
        
          $(document).ready(function(){
        
            // if this is the content or translate frame then we want to mirror changes made to "language neutral" settings from one frame to the other
            if (!uiAccess.showActionBar) {

                function _mirrorUpdateValue(mObject) {
                    $('#' + mObject.id).val(mObject.value);
                }
        
                function _mirrorUpdateChecked(mObject) {
                    $('#' + mObject.id).prop('checked', mObject.value).trigger('change', true);
                }
        
                function _mirrorUpdateColor(mObject) {
                    $('#' + mObject.id).val(mObject.value).keyup();
                }
        
                function _mirrorPushButton(mObject) {
                    if ($('#' + mObject.id).data('events').hasOwnProperty('click')) {
                        $.each($('#' + mObject.id).data('events').click, function(index, action) {
                             if (action.namespace != 'ocms.publishedClick') {
                                action.handler.call();
                             }
                        });
                    }
                }
        
                // for side-by-side editing set up our action mirroring
        
                //** memmory leak */
        
        
                $thisFrame.action_mirror({
                    "bMultilingual":    uiAccess.showLanguages,             // actions will only get mirrored when the bMultilingual flag is true!
                    "mirrorActions":    {
                         "updateValue":     _mirrorUpdateValue              // map action-name(s) to mirror handler routine(s)
                        ,"updateChecked":   _mirrorUpdateChecked
                        ,"updateColor":     _mirrorUpdateColor
                        ,"pushButton":      _mirrorPushButton
                    },
                    "context":          self,                               // context for handler routines to run in
                    "suffix":           "contentEditorSubframe",            // only need this when container element doesn't have an Id attribute,
                    "dontUseId":        true
                });
        
                // get a handle to our action mirror widget so we can call its "action" mirror method directly
        
        
                $thisFrame.mirror =  $(window.frameElement).action_mirror('instance');
        
                // find the language neutral controls and prepare to "mirror" changes to them to the other frame
                $('.ocmsLanguageNeutral').each(function(idx) {
                    var cType = $(this).attr('type');
                    var id  = $(this).attr('id');
                    if (cType === 'text' || cType === 'email' || cType === 'password' || cType === 'number' || cType === 'search' || cType === 'tel' || cType === 'url') {
                        $(this).keyup(function() {
                            $thisFrame.mirror.action("updateValue", {id: id, value: $(this).val()});
                        });
                        $(this).on('colorUpdate', function(){ // special for jPicker
                            $thisFrame.mirror.action("updateColor", {id: id, value: $(this).val()});
                        });
                    } else if (cType === 'checkbox' || cType === 'radio') {
                        $(this).change(function(event, internal){
                            if (!internal){
                                $thisFrame.mirror.action("updateChecked", {id: id, value: $(this).prop('checked')});
                            }
                        });
                    } else if ($(this).is('select')) {
                        $(this).change(function() {
                            $thisFrame.mirror.action("updateValue", {id: id, value: $(this).val()});
                        });
                    } else if ($(this).hasClass('ocmsLanguageNeutralButton')) {
                        $(this).on('click.ocms.publishedClick', function() {
                            $thisFrame.mirror.action("pushButton", {id: id, value:"click"});
                        });
                    }
                });
            // end of if not showActionBar (ie if this is an editor "subframe")
            } else {
                // Add the appropriate height to the body
                if (window.frameElement) {
                    $('body').css('min-height', $(window.frameElement).height() + 'px');
                }
            }

            if($.orchestracmsUtil.isSet(uiContent.content)) {
        
                //this should only happen for the top frame
                var unlockFunc = function(sEvt, sObj, cbFunc) {
        
                        if('IS.CLOSING' == sObj && uiContent.isLockedByMe) {
                            var data = {};
                            data['service'] = 'LockingManagerService';
                            data['action']  = 'unlockContent';
                            data['sname']   = uiContent.content.siteName;
                            data['content_ids'] = uiContent.content.Id;
                            var handler = function(response_string){
                                if (typeof JSON != 'undefined') {
                                    var response = JSON.parse(response_string);
                                    if (response.error != undefined) {
                                        $('<div></div>').ocmsShowErrorPopup({
                                            message: $.orchestracmsUtil.htmlEncode(response.error.message)
                                        });
                                    }
                                }
                            }
                            var options = {};
                            options['cb'] = handler;
                            doServiceRequest(data, options);
                        }
                };
        
                unlockOnClose = addSubscriber('Tab.Event.'+thisTabId(), unlockFunc);

                ce = $('#edit-toolbar').content_editor({
                    actionSave              : null,
                    actionSaveAndClose      : null,
                    allCollaborators        : JSON.parse(uiContent.allCollaborators),
                    allLayouts              : JSON.parse(uiContent.allLayouts),
                    allow_tags              : uiAccess.allowTags,
                    allowClone              : uiAccess.allowClone,
                    allowDelete             : uiAccess.allowDelete,
                    allowExpire             : uiAccess.allowExpire,
                    allowNewRevision        : uiAccess.allowNewRevision,
                    allowNewVersion         : uiAccess.allowNewVersion,
                    allowPublish            : uiAccess.allowPublish,
                    allowPublishedDelete    : uiAccess.allowPublishedDelete,
                    allowSave               : uiAccess.allowSave,
                    allowSentForApprovalEdit: uiAccess.allowSentForApprovalEdit,
                    allVisitors             : JSON.parse(uiContent.allVisitors),
                    autoSave                : JSON.parse(uiSettings.autoSave),
                    cliId                   : uiContent.current_content_layout_instance.Id,
                    content                 : JSON.parse(uiContent.serializedContent),
                    content_expiry_date     : uiContent.content_expiry_date,
                    content_status          : uiContent.content_status,
                    content_type            : uiContent.contentTypeName,
                    contentAvailable        : $.orchestracmsUtil.htmlEncode(uiContent.available),
                    contentId               : $.orchestracmsUtil.htmlEncode(uiContent.content.Id),
                    contentName             : $.orchestracmsUtil.htmlEncode(uiContent.contentName),
                    contentVersion          : $.orchestracmsUtil.htmlEncode(uiContent.contentVersion),
                    current_cl              : JSON.parse(uiContent.current_content_layoutJSON),
                    customRestrictions      : JSON.parse(uiContent.customRestrictionsJSON),
                    deleteUrl               : uiSettings.deleteUrl,
                    disableAll              : uiAccess.disableAll,
                    disableContentType      : uiAccess.disableContentType,
                    disableDates            : uiAccess.disableDates,
                    disableDescription      : uiAccess.disableDescription,
                    disableEditAccessLevels : uiAccess.disableEditAccessLevels,
                    disableEditCompliance   : uiAccess.disableEditCompliance,
                    disableEditTargets      : uiAccess.disableEditTargets,
                    disableExcludeFromSearch: uiAccess.disableExcludeFromSearch,
                    disableName             : uiAccess.disableName,
                    disablePriority         : uiAccess.disablePriority,
                    disableSelectLanguages  : uiAccess.disableSelectLanguages,
                    disableTranslateProperties: uiAccess.disableTranslateProperties,
                    emailEnabled            : uiAccess.emailEnabled,
                    groupRestrictions       : JSON.parse(uiContent.groupRestrictionsJSON),
                    hierarchyOverride       : uiContent.hierarchyOverride,
                    isPrimaryLanguage       : uiContent.languageInUse.Id == allLanguageInformation.contentPrimaryLanguage.Id,
                    isTaxonomy              : uiContent.isTaxonomy,
                    isTemplateSharingOn     : uiSettings.isTemplateSharingOn,
                    language                : uiContent.languageInUse.Id,
                    levels                  : JSON.parse(uiSettings.levels),
                    lock_state              : uiContent.lockState,
                    lockeddate              : uiContent.lockedDate,
                    originId                : uiContent.content_origin.Id,
                    preview_link            : uiSettings.preview_link,
                    published               : uiContent.isPublished,
                    readOnlyCollaborator    : uiAccess.readOnlyCollaborator,
                    removeBtnActive         : ocmsUI.image.getImage('btn_close_active'),
                    removeBtnInactive       : ocmsUI.image.getImage('btn_close_inactive'),
                    requireTranslationLanguages: allLanguageInformation.requireTranslationLanguages,
                    selectedTargets         : JSON.parse(uiContent.selectedTargets),
                    sentForApproval         : uiContent.sentForApproval,
                    showCompliance          : uiAccess.showCompliance,
                    showLanguages           : uiAccess.showLanguages,
                    showPersonalTags        : uiAccess.showPersonalTags,
                    showPublicTags          : uiAccess.showPublicTags,
                    showSendForApproval     : uiAccess.showSendForApproval,
                    showShare               : uiAccess.showShare,
                    showSplitView           : allLanguageInformation.numberOfLanguagesForContent > 1,
                    showTargets             : uiAccess.showTargets,
                    showTaxonomy            : uiAccess.showTaxonomy,
                    showTemplates           : uiAccess.showTemplates,
                    sitePrefix              : $.orchestracmsUtil.htmlEncode(uiSettings.sitePrefix),
                    sname                   : uiContent.siteName,
                    state                   : uiContent.state,
                    tab                     : thisTabId(),
                    template_options        : JSON.parse(uiContent.layoutAddOptions),
                    templates               : JSON.parse(uiContent.layouts),
                    token                   : uiSettings.token,
                    translationView         : uiAccess.multilingualAccessLevel,
                    type                    : uiContent.contentType,
                    uname                   : uiSettings.userFirstName + ' ' + uiSettings.userLastName,
                    VersionJSON             : uiContent.versionsJSON
                });
        
            }

            if(uiAccess.showLanguages){
                $('.ocms-content-translate-panel').toggle();
        
                if (!uiAccess.showActionBar) {

                    // An event to monitor the status of split view and then pass it along to any CKEDITOR instances in this frame.
                    $jParent(window.parent.document).on('split_view_changed.'+uiContent.content.Id+'.'+ uiContent.languageInUse.Id, function() {
                        // If we don't have any CKEDITOR instances we can skip the rest.
                        if (!$.isEmptyObject(CKEDITOR.instances)) {
                            // Only do this if we are enabling split view.
                            var tryingToEnable = $jParent(".ocms-content-translate-panel").css('display') == 'none';
                            if (tryingToEnable) {
                                // fire this event on all CKEDITOR instances.
                                for (var instance in CKEDITOR.instances) {
                                    if (CKEDITOR.instances.hasOwnProperty(instance)) {
                                        CKEDITOR.instances[instance].fire('resizeForSplitView');
                                    }
                                }
                            }
                        }

                        // this is to ensure that textareas do not go over their boundaries in split view
                        $.each($("textarea"), function (count, element) {
                            if($(element).width() > 600){
                                $(element).css("width", "600px");
                            }

                        });

                    });
                    // Disconnect this event when the frame is unloaded.
                    window.onbeforeunload = function (e) {
                        if ($jParent)
                            $jParent(window.parent.document).off('split_view_changed.'+uiContent.content.Id+'.'+ uiContent.languageInUse.Id);
                    };
                    // For each ckeditor created, add an event to resize the frames if the CKEDITOR gets resized.
                    CKEDITOR.on('instanceCreated', function(e) {
                        e.editor.on('instanceReady', function() {
                            parent.resizeEditorFrames();
                        });
                        e.editor.on('resize',function() {
                            parent.resizeEditorFrames();
                        });
                        // Resize this to the minimum of existing size or half of the screen width.
                        e.editor.on('resizeForSplitView', function() {
                            var currentWidth = parseInt(window.getComputedStyle(e.editor.container.$).getPropertyValue('width')) + 12;
                            var newWidth = Math.min((parseInt(window.getComputedStyle(document.querySelector('body')).getPropertyValue('width')) / 2) - 20, currentWidth);
                            e.editor.resize(newWidth, NaN);
                        });
                    });
                    CKEDITOR.on('instanceDestroyed', function(e) {
                        e.editor.removeAllListeners();
                    });

                    var saveFunctionDefinition =  function(){
                        var translationStatus = [];
                        var thisTranslationStatus = $thisFrame.closest('div').find('.languageTranslated').val() == 'true';
                        translationStatus.push({"id":uiContent.languageInUse.Id, "translated":thisTranslationStatus});
        
                        var thisLanguage = $.grep(allLanguageInformation.contentLanguages, function(lang){
                            return uiContent.languageInUse.Id == lang.Id;
                        })[0];
                        var languageIsAlreadyTranslated = allLanguageInformation.translatedLanguages.some(function(language){
                            return uiContent.languageInUse.Id == language.Id;
                        });
        
                        if (thisTranslationStatus === true) {
                            if (!languageIsAlreadyTranslated) {
                                allLanguageInformation.translatedLanguages.push(thisLanguage);
                                allLanguageInformation.requireTranslationLanguages = $.grep(allLanguageInformation.requireTranslationLanguages, function(lang){
                                    return lang.Id != thisLanguage.Id;
                                });
                                ce.content_editor('option', 'requireTranslationLanguages', allLanguageInformation.requireTranslationLanguages);
                            }
                        }
                        else if (thisTranslationStatus === false) {
                            if (languageIsAlreadyTranslated) {
                                allLanguageInformation.translatedLanguages = $.grep(allLanguageInformation.translatedLanguages, function(lang){
                                    return lang.Id != thisLanguage.Id;
                                });
                                allLanguageInformation.requireTranslationLanguages.push(thisLanguage);
                                ce.content_editor('option', 'requireTranslationLanguages', allLanguageInformation.requireTranslationLanguages);
                            }
        
                        }
        
                        return {'translationStatus':translationStatus};
                    };
        
                    // Set the translation status.
                    var translationStatusBox = $thisFrame.closest('div').find('.languageTranslated');
                    if (allLanguageInformation.defaultSiteLanguage.Id == uiContent.languageInUse.Id) {
                        $thisFrame.closest('div').find('.languageTranslated option[value="true"]').prop("selected",true);
                    } else {
                        var currentLanguageIsTranslated = $.orchestracmsUtil.inArray(uiContent.languageInUse, allLanguageInformation.translatedLanguages, function(item, check){
                            return JSON.stringify(item) == JSON.stringify(check)
                        });
                        translationStatusBox.prop('disabled', false);
                        translationStatusBox.find('option[value="'+ currentLanguageIsTranslated +'"]').prop("selected",true);
                    }
        
                    // If the user is allowed to send things for approval we need to figure out if the button should be enabled yet.
                    // Also, make sure the content or page is not in a published state
                    if (uiAccess.showSendForApproval && !uiContent.isPublished && uiContent.isLockedByMe) {
                        var langOneNeedsTranslation = $jParent('#languageTranslatedOne option:selected').val() == 'false';
                        var langTwoNeedsTranslation = $jParent('#languageTranslatedTwo option:selected').val() == 'false';
                        var remaining = allLanguageInformation.requireTranslationLanguages.length;

                        if (remaining <= 2 && (remaining == 0 || langOneNeedsTranslation || langTwoNeedsTranslation)) {
                            if (remaining == 0) {
                                ce.content_editor('instance').enableSidebarActions('SendForApproval');
                            } else {
                                ce.content_editor('instance').disableSidebarActions('SendForApproval');
                            }
        
                            $jParent('.languageTranslated').change(function(){
                                if ($(this).val() == 'true'){
                                    remaining--;
                                } else {
                                    remaining++
                                }
        
                                if (remaining === 0) {
                                    ce.content_editor('instance').enableSidebarActions('SendForApproval');
                                } else {
                                    ce.content_editor('instance').disableSidebarActions('SendForApproval');
                                }
                            });
        
                        } else {
                            ce.content_editor('instance').disableSidebarActions('SendForApproval');
                        }
        
                    // If the send for approval button should not be shown, then ensure it is not an available option.
                    } else {
                        ce.content_editor('instance').disableSidebarActions('SendForApproval');
                    }
        
                    var permissionText;
                    if (uiAccess.disableAll) {
                        permissionText = 'Viewing';
                        $thisFrame.closest('div').find('.languageTranslated').prop('disabled', true);
                    }else{
                        switch (uiAccess.multilingualAccessLevel){
                            case 'TRANSLATE':
                                permissionText = 'Translating';
                                $('.ocmsLanguageNeutral').prop('disabled', true);
                                ce.content_editor('registerSaveFunction', saveFunctionDefinition);
                                break;
                            case 'READONLY':
                                permissionText = 'Viewing';
                                $thisFrame.closest('div').find('.languageTranslated').prop('disabled', true);
                                $($thisFrame[0].contentDocument).find('input, textarea, select').prop('disabled', true);
                                CKEDITOR.config.readOnly = true;
                                break;
                            case 'EDIT':
                                permissionText = 'Editing';
                                ce.content_editor('registerSaveFunction', saveFunctionDefinition);
                                break;
                            default:
                                permissionText = 'Viewing';
                        }
                    }
                    var permissionMessage = 'Currently '+ permissionText;
                    $thisFrame.closest('div').find('.ocmsHelpText').text(permissionMessage);
        
                } else {
        
                    $('#ocmsLanguageOptionsOne').append(buildSelectBoxes('One'));
                    $('#ocmsLanguageSelectorOne').change(function(){
                        setContentFrame($('.contentFrame')[0]);
                    });
        
                    // Build the options dialogs.
                    $('#ocmsLanguageSelectorOne').append(optionsList(''));
        
                }
        
            }
            else {
    	       // If the org does not have multilingual licenses...
    	       if (uiAccess.showSendForApproval && !uiContent.isPublished && uiContent.isLockedByMe) {
                   ce.content_editor('instance').enableSidebarActions('SendForApproval');
    	       } else {
                   ce.content_editor('instance').disableSidebarActions('SendForApproval');
                }
            }

            setTabStatus(thisTabId(), $.orchestracmsUtil.htmlEncode(uiContent.content_status_state));
            resizeContentEditorArea();
        
            $.orchestracmsSecureUrlLoad.doLoadS3SecureUrl();
        
        
            //if the content is published disable all input
            //If the content is shared as read-only to this user then also disable all input.
            if(uiAccess.disableAll) {
                //disable all inputs, this will make it easier when creating templates. Template creates won't have to worry about disabling the fields themselves it makes the code messy and a pain
                //might as well disable everything with javascript client side and just make sure if someone disables javascript
                $('.ocms-content-editor-panel, .ocmsContentEditor').find('input, textarea, select').prop('disabled', true);
                CKEDITOR.config.readOnly = true;
        
                // The language selector dropdown should be available in all circumstances.
                $('.ocmslanguageSelected').prop('disabled', false);
        
            }

          });
          //# sourceURL=OrchestraCMS/CreateContent.page
        </script>

    </head>
    <body class="ocms autoHeight" style="padding: 0px; margin: 0px;">
        <div id="edit-toolbar"></div>

        <apex:outputPanel rendered="{!!hasAccess}" layout="none">
            <div style="padding: 10px;">You do not have the privileges required to edit this content.</div>
        </apex:outputPanel>

        <apex:outputPanel rendered="{!hasAccess}" layout="none">
            <apex:outputPanel rendered="{!showActionBar}" layout="none">
                <div id="contentEditActionBar"></div>
                <div>
                    <div id="contentEditSidePanel" />
                    <div id="contentEditBlock">
                        <div class="ui-layout-center">
                            <table class="ocmsContentEditor" >
                                <tr class="ocmsEditorTitle" >
                                    <td id="editorHeading">
                                        <div id="editorTitle" />
                                        <div id="editorStatus" />
                                    </td>
                                </tr>
                                <tr style="height: 100%;"><td style="min-width: 1292px; width: 100%; height: 100%; white-space: nowrap;">
                                    <div class="ocms-content-editor-panel">

                                        <apex:outputPanel rendered="{!!showLanguages}" layout="none">
                                            <apex:composition template="{!EditPage}" />
                                        </apex:outputPanel>

                                        <apex:outputPanel rendered="{!showLanguages}" layout="none">
                                            <div class="ocmsLanguageOptions" id="ocmsLanguageOptionsOne"></div>
                                            <iframe border="0" frameborder="0" class="contentFrame ocmsCleanUp" src="" id="contentFrame"></iframe>
                                            <script type="text/javascript">
                                                var contentFrame = document.querySelector('.contentFrame');
                                                iFrameResize({heightCalculationMethod: 'lowestElement', checkOrigin: false, minHeight: 450}, contentFrame);
                                                setContentFrame(contentFrame);
                                            </script>
                                        </apex:outputPanel>

                                    </div>
                                    <apex:outputPanel rendered="{!showLanguages}" layout="none">
                                        <div class="ocms-content-translate-panel">
                                            <div class="ocmsLanguageOptions" id="ocmsLanguageOptionsTwo"></div>
                                            <iframe border="0" frameborder="0" class="translateFrame ocmsCleanUp" src="" id="translateFrame"></iframe>
                                            <script type="text/javascript">
                                                var translateFrame = document.querySelector('.translateFrame');
                                                iFrameResize({heightCalculationMethod: 'lowestElement', checkOrigin: false, minHeight: 450}, translateFrame);
                                            </script>
                                        </div>
                                    </apex:outputPanel>
                                </td></tr>
                            </table>
                        </div>
                        <div class="ui-layout-south" id="ContentItemDetailPanel" />
                    </div>
                </div>
            </apex:outputPanel>
            <apex:outputPanel rendered="{!!showActionBar}" layout="none">
                <div id="preventMarginCollapse" style="padding-top: 1px;"></div>
                <apex:composition template="{!EditPage}" />
            </apex:outputPanel>
        </apex:outputPanel>
    </body>
</apex:page>