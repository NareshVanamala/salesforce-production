<apex:page controller="cms.setupUnlock" showHeader="false" sidebar="false" standardStylesheets="false">
	<html>
		<head>
			<title>Unlock Content and Pages</title>
			<apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms.css')}" />
			<apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms.setupmenu.css')}" />
			<apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms.edit.css')}" />
			<c:CmsScripts />
			<apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/TabInterface.js')}" />

			<script type="text/javascript">
                var uiData = JSON.parse($.orchestracmsUtil.base64Decode('{!uiDataJSONBase64}'));
				var prefix = '';
                var pkg_namespace = uiData.namespace;

                if (pkg_namespace != null && pkg_namespace != '')
                	prefix = pkg_namespace + '__';

				$(document).data('cms', {
					'context' 	: 'orchestracms',
					'namespace' : uiData.namespace
				});

        		$('document').ready(function() {
					// Get the setup menu
					var params = {
						service: 'SetupService',
						action: 'getSetupMenu',
						activeItem: 'unlock'
					};

            		var handler = function(json, success) {
						json = $.orchestracmsUtil.parseJSON(json);
						if (json.error === undefined && (success)) {
							var menuContainer = document.querySelector('#ocmsSetupMenu');
							if (menuContainer !== null)
								menuContainer.innerHTML = json.success.message;
						}
            		};

					var options = {
						cb: handler,
						readonly: true
					};
					doServiceRequest(params, options);
        		});

                $(document).ready(function() {
                	$('#lockedContentSelectAll').click(function() {
                		if ($('#lockedContentSelectAll').is(':checked')) {
                			$('#unlockFilterContentList .lockedItemsSelectItem').prop('checked', true);
                			$('.unlockButton').button("enable");
                		} else {
                			$('#unlockFilterContentList .lockedItemsSelectItem').prop('checked', false);
                			$('.unlockButton').button("disable");
                		}
                	});

                	$('#lockedPageSelectAll').click(function() {
                		if ($('#lockedPageSelectAll').is(':checked')) {
                			$('#unlockFilterPageList .lockedItemsSelectItem').prop('checked', true);
                			$('.unlockButton').button("enable");
                		} else {
                			$('#unlockFilterPageList .lockedItemsSelectItem').prop('checked', false);
                			$('.unlockButton').button("disable");
                		}
                	});

                	$('.unlockButton').button().click(function() {
                		// Find all the selected stuff
                		var content_ids = '';
                		var first = true;
                		$('.unlockButton').button("disable");

                		$.each($('#unlockFilterContentList .lockedItemsSelectItem:checked'),function(index,element) {
                			if (!first)
                				content_ids += ',';
                			content_ids += $(element).data('id');
                			first = false;
                		});

                		if (content_ids != '') {
                			var data= {
								action: 'unlockContent',
								service: 'LockingManagerService',
								sname: uiData.sname,
								content_ids: content_ids
							};

                			var handler = function(json, result) {
                            	json = $.orchestracmsUtil.parseJSON(json);
                				if ((result.status ==true) && (json.error === undefined)) {
                					$('#unlockFilterContentList tr.unlockRow').remove();
                					search("content");
                				} else {
                					showError('Error with unlocking content: ', $.orchestracmsUtil.htmlEncode(json.error.message), function() {
                						location.reload(true);
                					});
                				}
                			}

                			var options = {
								cb: handler
							};
                			doServiceRequest(data, options);
                		}

                		var page_ids = '';
                		first = true;
                		$.each($('#unlockFilterPageList .lockedItemsSelectItem:checked'),function(index,element) {
                			if (!first)
                				page_ids += ',';
                			page_ids += $(element).data('id');
                			first = false;
                		});

                		if (page_ids != '') {
                			var data = {
								action: 'unlockPage',
								service: 'LockingManagerService',
								sname: uiData.sname,
								page_ids: page_ids
							};

                			var handler = function(json, result) {
                	        	json = $.orchestracmsUtil.parseJSON(json);
                				if ((result.status ==true) && (json.error === undefined)) {
                					$('#unlockFilterPageList tr.unlockRow').remove();
                					search('page');
                				} else {
                					showError('Error with unlocking page: ', $.orchestracmsUtil.htmlEncode(json.error.message), function() {
                						location.reload(true);
                					});
                				}
                			}

                			var options = {
								cb: handler
							};
                			doServiceRequest(data, options);
                		}
                	});

                	$('#lockedSince').datetime_selector({
                		type: 'datetime',
                		showlabel: false
                	});

                	self.defaultInputMessage = 'Start typing to filter the list';
                	search(null);

                	$("#filterName").val(defaultInputMessage);
                	$("#filterLockedBy").val(defaultInputMessage);

                	$('#filterName, #filterLockedBy').click(function() {
                		if (!$(this).hasClass('focused')) { // Already have focus?
                			$(this).select();				// No - select all text in input
                			$(this).addClass('focused');	// And add class as a flag
                		}
                	});

                	$("#filterName, #filterLockedBy").blur(function() {
                  		if ($(this).val() == '')
                  			$(this).val(defaultInputMessage);
                  		$(this).removeClass('focused');		// Remove our flag
                	});

                	$('#filterName, #filterLockedBy').keyup(function() {
                		search(null);
                	});
                	$('#filterTypeSelect').change(function() {
                		search(null);
                	});
                	$('#lockedSince').change(function() {
                		search(null);
                	});
                	$('.dt-clear-cell').click(function() {
                		search(null);
                	});

                	$('.unlockButton').button("disable");
                });

                function search(objType) {
                	var itemType;
                	if (objType != null)
                		itemType = objType;
                	else
                		itemType = $('#filterTypeSelect').val();

                	if (itemType == 'content' || itemType == 'All') {
                		var data = {
							action: 'getFilteredLockedContent',
							service: 'LockingManagerService',
							sname: uiData.sname
						};

                		if ($('#filterName').val() != defaultInputMessage)
                			data.name = $('#filterName').val();

                		if ($('#filterLockedBy').val() != defaultInputMessage)
                			data.LockedBy = $('#filterLockedBy').val();

                		data.lockedSince = $('#lockedSince').datetime_selector('getValue');

                		var handler = function(json, result) {
                        	json = $.orchestracmsUtil.parseJSON(json);
                			if ((result.status == true) && (json.error === undefined))
                				updateContentListRows(json);
                			else
                				showError('Error with getting filtered locked content: ', $.orchestracmsUtil.htmlEncode(json.error.message));
                		}

                		var options = {
							cb: handler
						};
                		doServiceRequest(data, options);
                	}

                	if (itemType == 'page' || itemType == 'All') {
                		var data = {
							action: 'getFilteredLockedPages',
							service: 'LockingManagerService',
							sname: uiData.sname
						};

                		if ($('#filterName').val() != defaultInputMessage)
                			data.name = $('#filterName').val();

                		if ($('#filterLockedBy').val() != defaultInputMessage)
                			data.LockedBy = $('#filterLockedBy').val();

                		data.lockedSince = $('#lockedSince').datetime_selector('getValue');

                		var handler = function(json, result) {
                        	json = $.orchestracmsUtil.parseJSON(json);
                			if ((result.status == true) && (json.error === undefined))
                				updatePageListRows(json);
                			else
                				showError('Error with getting filtered locked page(s): ', $.orchestracmsUtil.htmlEncode(json.error.message));
                		}

                		var options = {
							cb: handler
						};
                		doServiceRequest(data, options);
                	}
                } // end of search

                function updateContentListRows(newUnlockItems) {
                	// Clear old values if any
                	$('#unlockFilterContentList tr.unlockRow').remove();
                	var trId = 0;

                	$.each(newUnlockItems, function(index, element) {
                		var Today = new Date();
                		var rowDate = new Date(element[prefix +'LockedForEditDate__c'].substr(0, element[prefix +'LockedForEditDate__c'].indexOf('+')));
                		var rowDateStr = '' + rowDate.getDate() + ' '+ monthNames[rowDate.getMonth()] + ', ' + rowDate.getFullYear();

                		$('#unlockFilterContentList').append('<tr class="unlockRow"><td>'
							+ '<input id="' + element.Id + '" class="lockedItemsSelectItem ocmsCheckbox" data-id="' + element.Id + '" type="checkbox"/><label for="' + element.Id + '"></label>'
							+ '</td><td>' + $.orchestracmsUtil.htmlEncode(element[prefix + 'Name__c']) + '</td><td>' + $.orchestracmsUtil.htmlEncode(element[prefix
							+ 'Content_Type__r'][prefix + 'Label__c']) + '</td><td>' + rowDateStr
							+ '</td><td>' + $.orchestracmsUtil.htmlEncode(element[prefix + 'LockedForEditBy__r']['Name']) + '</td></tr>'
                		);

                		trId++;
                	});

                	$('.lockedItemsSelectItem').change(function() {
                		if ($('.lockedItemsSelectItem').is(':checked'))
                			$('.unlockButton').button("enable");
                		else
                			$('.unlockButton').button("disable");
                	});
                } // end of updateContentListRows

                function updatePageListRows(newUnlockItems) {
                	// Clear old values if any
                	$('#unlockFilterPageList tr.unlockRow').remove();
                	var trId = 0;

                	$.each(newUnlockItems, function(index, element) {
                		var Today = new Date();
                		var rowDate = new Date(element[prefix +'LockedForEditDate__c'].substr(0, element[prefix +'LockedForEditDate__c'].indexOf('+')));
                		var rowDateStr = '' + rowDate.getDate() + ' '+ monthNames[rowDate.getMonth()] + ', ' + rowDate.getFullYear();

                		$('#unlockFilterPageList').append('<tr class="unlockRow"><td>'
                            + '<input id="' + element.Id + '" class="lockedItemsSelectItem ocmsCheckbox" data-id="' + element.Id + '" type="checkbox"/><label for="' + element.Id + '"></label>'
							+ '</td><td>' + $.orchestracmsUtil.htmlEncode(element[prefix +'Name__c']) + '</td><td>Page</td><td>' + rowDateStr
                			+ '</td><td>' + $.orchestracmsUtil.htmlEncode(element[prefix + 'LockedForEditBy__r']['Name']) + '</td></tr>'
                		);

                		trId++;
                	});

                	$('.lockedItemsSelectItem').change(function() {
                		if ($('.lockedItemsSelectItem').is(':checked'))
                			$('.unlockButton').button("enable");
                		else
                			$('.unlockButton').button("disable");
                	});
                } // end of updatePageListRows

                var monthNames = [ "January", "February", "March", "April", "May", "June",
					"July", "August", "September", "October", "November", "December" ];

                function showError(title, message, func) {
                	$('<div></div>').ocmsShowErrorPopup({
                		title : title,
                	    message : message,
                	    onClose: func
                	});
            	} // end of showError
			</script>
		</head>

		<body class="ocms">
			<script type="text/javascript" id="ocmsSiteDetailsPageHTML">
				$($.orchestracmsUtil.jsonDecode(uiData.setupUnlock)).appendTo('body');
			</script>
		</body>
	</html>
</apex:page>