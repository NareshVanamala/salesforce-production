<apex:page controller="cms.CTC" sidebar="false" standardStylesheets="false" cache="false"  showHeader="false">
    <html>
        <head>
            <title>Content Templates</title>
            <apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms-min.css')}" />
            <apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms.setupmenu.css')}" />
            <c:CmsScripts />    
            <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/ctc/vendor/lazyload.min.js')}" />   
            <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/widget/JQueryFileUpload.js')}" />   
            
            <script>
                $(document).data('cms', {
                    'context': 'orchestracms',
                    'namespace': '{!namespace}',
                    'hasCTCAccess': {!hasCTCAccess}
                });
            
                var nsPrefix;

                $('document').ready(function() {
                    // Get the setup menu
                    var params = {
                        action: 'getSetupMenu',
                        service: 'SetupService',
                        activeItem: 'manage-content-layout-generator'
                    };
            
                    var handler = function(json, success) {
                        json = $.orchestracmsUtil.parseJSON(json);
                        if (json.error === undefined && (success)) {
                            var menuContainer = document.querySelector('#ocmsSetupMenu');
                            if (menuContainer !== null)
                                menuContainer.innerHTML = json.success.message;
                        }
                    };
                    
                    var options = {
                        cb: handler,
                        readonly: true
                    };
                    doServiceRequest(params, options);
                });
            
                $(document).ready(function() {
                    $.orchestracmsUtil.loadScript('ui','ctc/ctc.js');
                    $('.ocms-progress').hide(); 
                    
                    if ('{!JSENCODE(currentLayout.Name__c)}' !== '')
                        $('#ctName').prop("disabled", true);
                    
                    // Load the content layout
                    nsPrefix = $(document).data('cms')['namespace'] == '' ? '' : $(document).data('cms')['namespace']+'__';
                    
                    updateAttributeList('{!JSENCODE(currentLayout.Attribute_List__c)}');
                        
                    $('#mapAttributesBtn').button().click(function() {
                        mapContentTemplateAttributes(); 
                    });

                    window.app = new window.cms_ctc.LayoutGen('.ctGen', {
                        id:                 '{!JSENCODE(currentLayout.Id)}',
                        name:               '{!JSENCODE(currentLayout.Name__c)}',
                        label:              '{!JSENCODE(currentLayout.Label__c)}',
                        description:        '{!JSENCODE(currentLayout.Description__c)}',
                        definition:         '{!JSENCODE(currentLayout.Attribute_Definition__r.Definition_Json__c)}',
                        attributeList:      '{!JSENCODE(currentLayout.Attribute_List__c)}',
                        generateTemplate:   '{!JSENCODE(currentLayout.Generate_Template__c)}',
                        thumbnailUploadKey: '{!JSENCODE(ThumbnailUploadKey)}',
                        types:              '{!JSENCODE(attributeTypesJson)}',
                        iconId:             '{!JSENCODE(currentLayout.Thumbnail_Id__c)}'
                    });
                    
                    if ($('#customSearchIndexFields > option').length < 1)
                        $('#siAttributeMapping').remove();
                });
                
                function generateAttributeList(attributeList) {
                    var templateAttributeList = $('.templateAttributeList');
                    
                    if ($('#customSearchIndexFields > option').length < 1 || attributeList === null || attributeList === undefined || attributeList.length === 0){
                        $('#siAttributeMapping').remove();
                    } else {
                        getAttributeMapping();
                        attributeList = attributeList.split(";");
                        
                        $('#templateAttributeList .templateAttributeList option').filter(function() {
                            var index = $.inArray($(this).val(), attributeList);
                            if (index > -1)
                                attributeList.splice(index, 1);
                        });
                        
                        $.each(attributeList, function(index, attr){
                          templateAttributeList.append("<option value="+$.orchestracmsUtil.htmlEncode(attr)+">"+$.orchestracmsUtil.htmlEncode(attr)+"</option>");
                        });
                    }
                }    
            
                function deleteAttribute(e) {
                    $(e.toElement.parentElement).remove();
                    updateAttributeList();
                }
            
                function addAnotherAttribute() {
                    if ($('#templateAttributeList .templateAttributeList').children('option').length > ($('#additionalAttributes').children('span').length + 2)) {
                        var deleteAttributeSelection = $('<a href="#" onclick="deleteAttribute(event)" class="ocms-icon ocms-icon-16 bigX16" title="Delete Attribute" style="display: inline-block; vertical-align: super; height: 1.7em; text-indent: -999em; width: 25px; margin-left: 3px;">Delete Attribute</a>');
                        var attributeWrapper = $('<span style="display: block;" />');
                        
                        $('#templateAttributeList').find('.templateAttributeList').parent().clone().appendTo(attributeWrapper);
                        attributeWrapper.append(deleteAttributeSelection);
                        $("#additionalAttributes").append(attributeWrapper);
                    }
                }
                
                function updateAttributeList() {
                    var attList;
                    
                    $('.templateAttributeList option:selected').filter(function() {
                        if (attList === undefined || attList === null)
                            attList  = $(this).val();
                        else
                            attList += ';' + $(this).val();
                    });
                    
                    $('.templateAttributeList option').filter(function() {
                        if (attList.indexOf($(this).val()) >= 0)
                            $(this).prop('disabled', true);
                        else
                            $(this).prop('disabled', false);
                    });
                }
                
                function getAttributeMapping() {
                    var layoutName = '{!encodedExternalId}';
                    var data = {
                        name: '{!siteName}',
                        action: 'getAttributeMapping',
                        service: 'SetupService',
                        clname: layoutName,
                        fname: $('#customSearchIndexFields option:selected').val()
                    };
                    
                    var options = {
                        cb: cbHandler,
                        bAsync: true
                    };
                    doServiceRequest(data, options);
                }
                
                function mapContentTemplateAttributes() {
                    var attList; 

                    $('.ocms-progress').show();
                    $('.templateAttributeList option:selected').filter(function() {
                        
                        if ($(this).val() !== 'none'){
                            if (attList === undefined || attList === null)
                                attList  = $(this).val();
                            else
                                attList += ';' + $(this).val();
                        }
                    });
                    
                    var data = {
                        sname: '{!siteName}',
                        action: 'saveAttributeMapping',
                        service: 'SetupService',
                        clname: '{!encodedExternalId}',
                        aname: attList,
                        fname: $('#customSearchIndexFields option:selected').val(),
                        flabel: $('#customFieldLabel').val(),
                        cfgId: $('#customSearchIndexFields option:selected').attr('cfgId')
                    };   
                    
                    var options = {
                        cb: cbHandler,
                        bAsync: true
                    };
                    doServiceRequest(data, options);
                }
                
                var cbHandler = function(json, success) {
                    $('#additionalAttributes').children().remove();
                    updateAttributeList();
                    json = $.orchestracmsUtil.parseJSON(json);

                    if (success.status && (json.error === undefined)) {
                        var fieldMapRecord = json.success.message;
                        if (fieldMapRecord !== undefined && fieldMapRecord !== null) {
                            var attrList = fieldMapRecord[nsPrefix+'Name__c'].split(";");
                            var label = fieldMapRecord[nsPrefix+'Label__c'];
                               
                            if (label === undefined || label === null)
                                $('#customFieldLabel').val("");
                            else
                                $('#customFieldLabel').val(label);
                               
                            $('#customSearchIndexFields option:selected').attr('cfgId', fieldMapRecord.Id);
                               
                            if (attrList.length > 0) {
                                for (i = 0; i < attrList.length; i++) {
                                    if (i == 0) {
                                        $('#templateAttributeList .templateAttributeList option').filter(function(){ return $(this).val() == attrList[i]; }).prop('selected', true);
                                    } else {
                                        addAnotherAttribute();
                                        $($('#additionalAttributes .templateAttributeList')[i-1]).children('option').filter(function(){ return $(this).val() == attrList[i]; }).prop('selected', true);
                                    }
                                }
                            } else {
                                $('#templateAttributeList .templateAttributeList option').filter(function(){ return $(this).val() == "none"; }).prop('selected', true);
                            }
                               
                            updateAttributeList();
                            $('.ocms-progress').hide();
                        } else {
                            $('#templateAttributeList .templateAttributeList option').filter(function(){ return $(this).val() == "none"; }).prop('selected', true);
                            $('#customFieldLabel').val("");
                            $('.ocms-progress').hide();
                        }
                    } else {
                        $('.ocms-progress').hide();
                    }
                };
            </script>
        </head>
    
        <body class="ocms">
            <div id="ocmsSetupMenu" />
            <div style="margin-left: 210px; padding: 10px;">
                <div class="ctGen">
                    <div class="ocms-setup-breadcrumb">
                        <div class="ocms-setup-breadcrumb-start"></div>
                        <div class="ocms-setup-breadcrumb-item">Setup</div>
                        <div class="ocms-setup-breadcrumb-separator"></div>
                        <div class="ocms-setup-breadcrumb-item">Content Templates</div>
                        <div class="ocms-setup-breadcrumb-separator-active"></div>
                        <div class="ocms-setup-breadcrumb-item-active">Content Template Creator</div>
                        <div class="ocms-setup-breadcrumb-end"></div>
                        <div class="ocms-clear"></div>
                    </div>
        
                    <h1>Content Template Creator</h1>
                    <div class="row">
                        <div class="column contentTemplateInputs">
                            <div class="row">
                                <div class="column contentTemplateLabel">
                                    <label for="ctName">Name</label>
                                </div>
                                <div class="column contentTemplateInput">
                                    <input type="text" class="ctName" id="ctName" placeholder="Content Template Name" value="{!theName}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="column contentTemplateLabel">
                                    <label for="ctLabel">Label</label>
                                </div>
                                <div class="column contentTemplateInput">
                                    <input type="text" class="ctLabel" id="ctLabel" placeholder="Content Template Label" value="{!theLabel}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="column contentTemplateLabel">
                                    <label for="ctDescription">Description</label>
                                </div>
                                <div class="column contentTemplateInput">
                                    <textarea class="ctDescription" placeholder="(optional but recommended)">{!theDescription}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="column contentTemplateInputs">
                            <apex:outputText escape="false" rendered="{!currentLayout.Id != null}">
                                <div>
                                    <button class="deleteCTC">Delete</button> <br /><br /><br />
                                </div>
                            </apex:outputText>
                            <div>
                                <label class="ocmsLabel">Thumbnail (optional)</label>
                                <div class="contentTemplateImageContainer">
                                    <apex:outputText escape="false" rendered="{!currentLayout.cms__Thumbnail_Id__c != ''}">
                                        <img src="/servlet/servlet.FileDownload?file={!currentLayout.Thumbnail_Id__c}" />
                                    </apex:outputText>
                                </div>
                                <div class="thumbnailUploadContainer"></div>
                            </div>
                        </div>
                    </div>
                    
                    <div id="siAttributeMapping">
                        <h2>Map Attributes</h2>         
                        <div class="row">
                            <div class="column"  style = "width: 120px; text-align: end;">
                                <label for="customSearchIndexFields">Search Index Custom Field</label>
                            </div>
                            <div class="column">
                                <div id="CustomFieldList"   style="width: 150px;">
                                    <select id="customSearchIndexFields" onchange="getAttributeMapping();">
                                        <apex:repeat value="{!CustomSearchIndexFields}" var="field"> 
                                            <option value="{!field}" cfgId=""> {!field} </option>
                                        </apex:repeat>
                                    </select>
                                </div>
                            </div>
                            <div class="column"  style = "width: 85px; text-align: end;">
                                <label for="customFieldLabel">Custom Label</label>
                            </div>
                            <div class="column">
                                <input id="customFieldLabel" name="fieldLabel" type="text" style="width: 150px;"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column" style = "width: 120px; text-align: end;">
                                <label for="templateAttributeList">Attribute</label>
                            </div>
                            <div class="column">
                                <div id="templateAttributeList" >
                                    <div style="width: 150px; display: inline-block;">
                                        <select class="templateAttributeList" onchange="updateAttributeList()">
                                            <option value="none"> ----- </option>
                                        </select>
                                    </div>
                                    <a href="#" onclick="addAnotherAttribute()" class="ocms-icon ocms-icon-16 add16" title="Add Attribute" style="display: inline-block; vertical-align: super; height: 1.7em; text-indent: -999em; width: 25px;">Add Attribute</a>
                                </div>
                                <div id="additionalAttributes" />
                            </div>
                            <div class="column">
                                <div class="buttons" style="margin-left: 65px;">     
                                    <div id="mapAttributesBtn">Map</div>
                                </div>
                            </div>
                        </div>
                    </div>
        
                    <div class="row editorButtons">
                        <a href="#" class="button saveLayout">Save</a>
                        <a href="/apex/ContentLayoutInstaller?sname={!SiteName}" class="button cancelLayout">Cancel</a>
                    </div>
        
                    <h2>Attributes and Editor Settings</h2>
                    <p class="helpText">Modifying an in-use content template will affect your site's timeline and is not advised.</p>
                    <ul class="attributeSections"></ul>
        
                    <h2>Markup</h2>
                    <apex:outputPanel rendered="{!hasCTCAccess}">
                        <div>
                            <div class="generateQuickButtons">
                                <ul class="attributes"></ul>
                            </div>
                            <textarea rows="10" placeholder="Generate Template" id="generateTemplate"></textarea>
                            <div class="generateQuickButtons">
                                <ul class="ids">
                                    <li>Insert ID</li>
                                    <li><button class="addToken" data-name="contentId">Content ID</button></li>
                                    <li><button class="addToken" data-name="contentOriginId">Content Origin ID</button></li>
                                    <li><button class="addToken" data-name="cliId">Content Layout Instance ID</button></li>
                                    <li><button class="addToken" data-name="pcliId">Page Content Layout Instance ID</button></li>
                                </ul>                           
                            </div>
                        </div>
                    </apex:outputPanel>
                    <apex:outputPanel rendered="{!!hasCTCAccess}">
                        <div>
                            <textarea rows="10" placeholder="Generate Template" id="generateTemplate" disabled="disabled"></textarea>
                        </div>
                    </apex:outputPanel>
                    <div class="row editorButtons">
                        <a href="#" class="button saveLayout">Save</a>
                        <a href="/apex/ContentLayoutInstaller?sname={!SiteName}" class="button cancelLayout">Cancel</a>
                    </div>
                </div>
            </div>
            <div class="ocms-progress">
                <img src="{!URLFOR($Resource.jqueryui, '/css/images/General/80px_AnimatedIndeterminateProgress.gif')}" />
                <span class="ocms-progress-text">
                    <div>Please</div><div>wait</div>
                </span>
            </div>
        </body>
    </html>
</apex:page>