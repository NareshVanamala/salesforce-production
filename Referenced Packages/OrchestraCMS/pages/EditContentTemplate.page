<apex:page controller="cms.BasicController" showHeader="false" sidebar="false" standardStylesheets="false">
	<html>
		<head>
			<title>Edit Content Template</title>
	    	<apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms-min.css')}" />
	    	<apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms.setupmenu.css')}" />
			<c:CmsScripts />
			<apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/TabInterface.js')}" />
			<apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/widget/JQueryFileUpload.js')}" />

			<script type="text/javascript">
                var uiData = JSON.parse($.orchestracmsUtil.base64Decode('{!uiDataJSONBase64}'));
				var nsPrefix;
				var clInstallExternalId;
				var customSearchIndexFields = JSON.parse(uiData.searchIndexFields);
				var namespace = uiData.namespace == null || uiData.namespace == '' ? '' : uiData.namespace;

				$(document).data('cms', {
					'context' 	: 'orchestracms',
					'namespace' : namespace
				});

		        $('document').ready(function() {
		            // Get the setup menu
		            var params = {
						service: 'SetupService',
						action: 'getSetupMenu',
						activeItem: 'manage-content-layouts'
					};

		            var handler = function(json, success) {
		                json = $.orchestracmsUtil.parseJSON(json);
		                if (json.error === undefined && (success)) {
		                    var menuContainer = document.querySelector('#ocmsSetupMenu');
							if (menuContainer !== null) {
							    menuContainer.innerHTML = json.success.message;
							}
		                }
		            };

		            var options = {
						cb: handler,
						readonly: true
					};
		            doServiceRequest(params, options);
		        });

				$(document).ready(function() {
					var selectHtml;

					for (var i = 0; i < customSearchIndexFields.length; i++) {
						selectHtml += '<option value="' + $.orchestracmsUtil.htmlEncode(customSearchIndexFields[i]) + '" cfgId=""> '
							+ $.orchestracmsUtil.htmlEncode(customSearchIndexFields[i]) + ' </option>';
					}

					$('#customSearchIndexFields').html(selectHtml);

					// Load the content layout
					nsPrefix = $(document).data('cms')['namespace'] == '' ? '' : $(document).data('cms')['namespace']+'__';

					var params = {
						service: 'SetupService',
						action: 'loadContentTemplate',
						sname: uiData.sname,
						id: uiData.genericId
					};

					var handler = function(response) {
						var response = JSON.parse(response);
						var selected;
						var layout = response.cl;
						var taxonomies = response.taxonomies;

						$('#templateName').val(layout[nsPrefix+'Name__c']);
						$('#templateLabel').val(layout[nsPrefix+'Label__c']);
						$('#templateDescription').val(layout[nsPrefix+'Description__c']);
						clInstallExternalId = layout[nsPrefix+'Install_External_Id__c'];

						try {
							selected = layout[nsPrefix+'Taxonomies__c'].split(/\s*;\s*/);
						} catch (e) {
							console.log('Taxonomies value is: '+layout[nsPrefix+'Taxonomies__c']);
							selected = [];
						}

						var taxonomySelectorContainer = $('#taxonomySelectorContainer');
						var taxonomySelector = $('<select multiple></select>');

						$.each(taxonomies, function(id, name) {
							name = $.orchestracmsUtil.htmlEncode(name);
							taxonomySelector.append('<option value="'+id+'" '+($.inArray(id,selected) < 0 ? '' : 'selected')+'>'+name+'</option>');
						});

						taxonomySelectorContainer.append(taxonomySelector);

	                    var templateAttributeList = $('.templateAttributeList');
	                    var res = layout[nsPrefix+'Attribute_List__c'];

	                    if ($('#customSearchIndexFields > option').length < 1 || res === null || res === undefined) {
	                        $('.mapAttributes').remove();
	                    } else {
	                        getAttributeMapping();
							res = res.split(";");

							$.each(res, function(index, attr){
							    templateAttributeList.append("<option value="+$.orchestracmsUtil.htmlEncode(attr)+">"+$.orchestracmsUtil.htmlEncode(attr)+"</option>");
							});
						}

						var saveContentTemplate = function() {
							var params = {
								service: 'SetupService',
								id: layout.Id,
								action: 'saveContentTemplate',
								name: $('#templateName').val(),
								label: $('#templateLabel').val(),
								description: $('#templateDescription').val()
							};

							var selectedTaxonomies = taxonomySelector.find('option:selected');
							var selectedTaxonomyString = '';

							$.each(selectedTaxonomies, function(index, option){
								if (selectedTaxonomyString !== '')
									selectedTaxonomyString += ';';
								selectedTaxonomyString += $(option).attr('value');
							});

							params.taxonomies = selectedTaxonomyString;

							doServiceRequest(params, {cb: function(response){
								window.location = '/apex/ContentLayoutInstaller?sname=' + uiData.sname;
							}});
						};

						$('#saveContentTemplateBtn').button().click(function() {
							saveContentTemplate();
						});
						
						$('.cancelBtn').button().click(function() {
							window.location = '/apex/ContentLayoutInstaller?sname=' + uiData.sname;
						});

	                    $('#mapAttributesBtn').button().click(function() {
	                        mapContentTemplateAttributes();
	                    });
					};

					var options = {
						cb: handler
					};
					doServiceRequest(params, options);
				});

				function deleteAttribute(e) {
	                $(e.toElement.parentElement).remove();
	                updateAttributeList();
	            }

	            function addAnotherAttribute() {
	                if ($('#templateAttributeList .templateAttributeList').children('option').length > ($('#additionalAttributes').children('span').length + 2)) {
		                var deleteAttributeSelection = $('<a href="#" onclick="deleteAttribute(event)" class="ocms-icon ocms-icon-16 bigX16 removeAttrBtn" title="Delete Attribute">Delete Attribute</a>');
		                var attributeWrapper = $('<div class="inlineField"/>');

		                $('#templateAttributeList').find('.templateAttributeList').clone().appendTo(attributeWrapper);
		                deleteAttributeSelection.appendTo(attributeWrapper);
		                $("#additionalAttributes").append(attributeWrapper);
		            }
	            } // end of addAnotherAttribute

	            function updateAttributeList() {
	                var attList;

	                $('.templateAttributeList option:selected').filter(function() {
	                    if (attList === undefined || attList === null)
	                        attList  = $(this).val();
	                    else
	                        attList += ';' + $(this).val();
	                });

	                $('.templateAttributeList option').filter(function() {
	                    if (attList.indexOf($(this).val()) >= 0)
	                        $(this).prop('disabled', true);
	                    else
	                        $(this).prop('disabled', false);
	                });
	            } // end of updateAttributeList

	            function getAttributeMapping(){
		            var data = {
						sname: uiData.sname,
						action: 'getAttributeMapping',
						service: 'SetupService',
						clname: clInstallExternalId,
						fname: $('#customSearchIndexFields option:selected').val()
					};

		            var options = {
						cb: cbHandler,
						bAsync: true
					};
		            doServiceRequest(data, options);
	            } // end of getAttributeMapping

				function mapContentTemplateAttributes() {

                    $('#mapAttributesBtn').button({disabled: true});

					var attList;

	                $('.templateAttributeList option:selected').filter(function() {
	                    if ($(this).val() !== 'none') {
		                    if (attList === undefined || attList === null)
			                    attList  = $(this).val();
		                    else
		                        attList += ';' + $(this).val();
		                }
	                });

	                var data = {
						sname: uiData.sname,
						action: 'saveAttributeMapping',
						service: 'SetupService',
						clname: clInstallExternalId,
						aname: attList,
						fname: $('#customSearchIndexFields option:selected').val(),
						flabel: $('#customFieldLabel').val(),
						cfgId: $('#customSearchIndexFields option:selected').attr('cfgId')
					};

	                var options = {
						cb: cbHandler,
						bAsync: true
					};
	                doServiceRequest(data, options);
	            } // end of mapContentTemplateAttributes

	            var cbHandler = function (json, success) {
	                $('#additionalAttributes').children().remove();
	                updateAttributeList();

	                json = $.orchestracmsUtil.parseJSON(json);

                    $('#mapAttributesBtn').button({disabled: false});

	                if (success.status && (json.error === undefined)) {
	                    var fieldMapRecord = json.success.message;

	                    if (fieldMapRecord !== undefined && fieldMapRecord !== null) {
	                        var attrList = fieldMapRecord[nsPrefix+'Name__c'].split(";");
	                        var label    = fieldMapRecord[nsPrefix+'Label__c'];

	                        if (label === undefined || label === null)
	                        	$('#customFieldLabel').val("");
	                        else
	                            $('#customFieldLabel').val(label);

	                        $('#customSearchIndexFields option:selected').attr('cfgId', fieldMapRecord.Id);

	                        if (attrList.length > 0) {
		                        for (i = 0; i < attrList.length; i++) {
		                            if (i == 0) {
		                                $('#templateAttributeList > .templateAttributeList option').filter(function(){ return $(this).val() == attrList[i]; }).prop('selected', true);
		                            } else {
		                                addAnotherAttribute();
		                                $($('#additionalAttributes .templateAttributeList')[i-1]).children('option').filter(function(){ return $(this).val() == attrList[i]; }).prop('selected', true);
		                            }
		                        }
		                    } else {
		                        $('#templateAttributeList > .templateAttributeList option').filter(function(){ return $(this).val() == "none"; }).prop('selected', true);
		                    }

	                        updateAttributeList();
	                    } else {
		                    $('#templateAttributeList > .templateAttributeList option').filter(function(){ return $(this).val() == "none"; }).prop('selected', true);
	                        $('#customFieldLabel').val("");
	                    }
	                }
	            };
			</script>
		</head>

		<body class="ocms">
			<div id="ocmsSetupMenu" />
			<div class="setupPage">
				<div class="breadcrumb">
                    <div class="breadcrumbItem"><apex:outputLink value="/apex/Setup?sname={!sname}">Setup</apex:outputLink></div>
                    <div class="breadcrumbSeparator"/>
					<div class="breadcrumbItem"><apex:outputLink value="/apex/ContentLayoutInstaller?sname={!sname}">Content Templates</apex:outputLink></div>
                    <div class="breadcrumbSeparator"/>
					<div class="breadcrumbItemActive">Edit Content Template</div>
                </div>
				<form class="editForm">
					<label>Name</label>
						<input id="templateName" name="templateName" type="text" disabled="disabled"/>
					<label>Label</label>
						<input id="templateLabel" name="templateLabel" type="text"/>
					<label>Description</label>
						<textarea id="templateDescription" name="templateDescription" rows="3" maxlength="255"></textarea>
					<label>Assign Taxonomies</label>
						<span>Assign taxonomy categories to the content template. Advanced use only.<br />
							Assigned categories are eligible to become attributes on content created with this template.
						</span>
                        <div id="taxonomySelectorContainer"></div>
					<div class="mapAttributes">
						<h2 class="noTop">Map Attributes</h2>
							<label for="customSearchIndexFields">Search Index Custom Field</label>
							    <select id="customSearchIndexFields" onchange="getAttributeMapping();"></select>
			                <label for="customFieldLabel">Custom Label</label>
			                    <input id="customFieldLabel" name="fieldLabel" type="text"/>
			            <div id="templateAttributeList" >
			                <label>Attribute(s)</label>
								<div class="inlineField">
									<select class="templateAttributeList" onchange="updateAttributeList()">
				                        <option value="none"> ----- </option>
				                    </select>
				                	<a href="#" onclick="addAnotherAttribute()" class="ocms-icon ocms-icon-16 add16 addAttrBtn" title="Add Attribute">Add Attribute</a>
								</div>
			            </div>
			            <div id="additionalAttributes"/>
			            <div class="ctBtns">
			                <div id="mapAttributesBtn">Map</div>
			            </div>
					</div>
					<div id="saveContentTemplateBtn">Save</div>
					<div class="cancelBtn">Cancel</div>
				</form>
			</div>
		</body>
	</html>
</apex:page>