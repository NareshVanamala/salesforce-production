<apex:page controller="cms.LibrariesController" showHeader="false" sidebar="false" cache="false"  standardStylesheets="false" title="Setup">
    <html>
        <head>
            <title>Add Library</title>
        	<apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms-min.css')}" />
        	<apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms.setupmenu.css')}" />
            <c:CmsScripts />

            <script type="text/javascript">
                var uiData = JSON.parse($.orchestracmsUtil.base64Decode('{!uiDataJSONBase64}'));
                var currentLibrary = JSON.parse(uiData.currentLibrary);
                var namespace = uiData.namespace == null ? '' : uiData.namespace;

                $(document).data('cms', {
					'context' 	: 'orchestracms',
					'namespace' : namespace
				});

                $('document').ready(function() {
                    // Get the setup menu
                    var params = {
                        service: 'SetupService',
                        action: 'getSetupMenu',
                        activeItem: 'libraries'
                    };

                    var handler = function(json, success) {
                        json = $.orchestracmsUtil.parseJSON(json);
                        if (json.error === undefined && (success)) {
                            var menuContainer = document.querySelector('#ocmsSetupMenu');
        					if (menuContainer !== null)
        					    menuContainer.innerHTML = json.success.message;
                        }
                    };

                    var options = {
                        cb: handler,
                        readonly: true
                    };
                    doServiceRequest(params, options);
                });

                $(document).ready(function(){
                    $('#saveButton').button().click(function() {
                        saveLibrary();
                        $('#saveButton').button('disable');
                    });

                    $('#cancelButton').button().click(function() {
                        currentLibrary = null;
						window.location = '/apex/Libraries?sname=' + uiData.sname
                    });

                    $('#testButton').button().click(function() {
                        testConnection();
                    });
                });

                function saveLibrary() {
                    var popup = $('<div></div>').ocmsShowInfoPopup({
                        message: 'Saving Library...',
                        forceProgrammaticClose: true
                    });

                    uiData.message = '';
                    $('.errorMsg').html('').hide();

                    currentLibrary.name = $('#nameInput').val();
                    currentLibrary.description = $('#descInput').val();
                    currentLibrary.share = $('input[name="radShare"]:checked').val() == 'true';

                    currentLibrary.link = $('#linkInput').val();
                    currentLibrary.repositoryLink = $('#repoLinkInput').val();
                    currentLibrary.accessKey = $('#accessInput').val();
                    currentLibrary.secretKey = $('#secretInput').val();
                    currentLibrary.canUpload = $('#canUpload').is(':checked');
                    currentLibrary.canBulkUpload = $('#canBulkUpload').is(':checked');
                    currentLibrary.isVersioned = true;

                    if (currentLibrary.origin == 'Amazon CloudFront - Secure') {
                        currentLibrary.secondaryAccessKey = $('#access2Input').val();
                        currentLibrary.secondarySecretKey = $('#secret2Input').val();
                    }

                    var params = {
                        service: 'SetupService',
                        action: 'saveLibrary',
                        sname: uiData.sname,
                        allowCallouts: true,
                        currentLibrary: JSON.stringify(currentLibrary)
                    };

                    var handler = function(json, result) {
                        json = $.orchestracmsUtil.parseJSON(json);
                        if ((result.status === true) && (json.isSuccess)) {
                            currentLibrary = null;
                            window.location = '/apex/Libraries?sname=' + $.orchestracmsUtil.htmlEncode(uiData.sname);
                        } else {
                            uiData.message = json.message;
                            showErrorMessage('Error saving library: ' + $.orchestracmsUtil.htmlEncode(json.message));
                            $('#saveButton').button('enable');
                        }
                        popup.ocmsShowInfoPopup('closeMessage');
                    };

                    var options = {
                        cb: handler,
                        readonly: false
                    };
                    doServiceRequest(params, options);
                } // end of saveLibrary

                function testConnection() {
                    uiData.message = '';
                    $('.errorMsg').html('').hide();

                    currentLibrary.name = $('#nameInput').val();
                    currentLibrary.description = $('#descInput').val();
                    currentLibrary.share = $('input[name="radShare"]:checked').val() == 'true';
                    currentLibrary.isVersioned = true;
                    currentLibrary.link = $('#linkInput').val();
                    currentLibrary.repositoryLink = $('#repoLinkInput').val();
                    currentLibrary.accessKey = $('#accessInput').val();
                    currentLibrary.secretKey = $('#secretInput').val();
                    currentLibrary.canUpload = $('#canUpload').is(':checked');
                    currentLibrary.canBulkUpload = $('#canBulkUpload').is(':checked');

                    if (currentLibrary.origin == 'Amazon CloudFront - Secure') {
                        currentLibrary.secondaryAccessKey = $('#access2Input').val();
                        currentLibrary.secondarySecretKey = $('#secret2Input').val();
                    }

                    var params = {
                        service: 'SetupService',
                        action: 'testConnection',
                        allowCallouts: true,
                        currentLibrary: JSON.stringify(currentLibrary)
                    };

                    var handler = function(json, result) {
                        json = $.orchestracmsUtil.parseJSON(json);
                        if ((result.status === true) && (json.isSuccess)) {
                            var resp = json.message;
                            $('.connectStatus').html($.orchestracmsUtil.htmlEncode(resp));
                        } else {
                            $('.connectStatus').html($.orchestracmsUtil.htmlEncode(json.message));
                        }
                    };

                    var options = {
                        cb: handler,
                        readonly: false
                    };
                    doServiceRequest(params, options);
                } // end of testConnection

                function showErrorMessage(message) {
					if (uiData.message !== null || uiData.message !== '')
						$('.errorMsg').html($.orchestracmsUtil.htmlEncode(uiData.message)).show();

					$('<div></div>').ocmsShowErrorPopup({
                        title: 'Error',
                        message : message,
                        width: 300
                    });
				} // end of showErrorMessage
            </script>
        </head>

        <body class="ocms">
            <script type="text/javascript" id="ocmsCreateLibraryPageHTML">
                $($.orchestracmsUtil.jsonDecode(uiData.createLibraryHTML)).appendTo('body');
            </script>
        </body>
    </html>
</apex:page>