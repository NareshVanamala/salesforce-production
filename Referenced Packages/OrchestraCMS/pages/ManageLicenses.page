<apex:page controller="cms.ManageLicenseController" standardStylesheets="false" showHeader="false" sidebar="false">
    <html>
        <head>
            <title>Licenses</title>
            <apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms-min.css')}" />
            <apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms.setupmenu.css')}" />
            <c:CmsScripts />
            <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUtil, '/cometd.js')}" />
            <apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUtil, '/jquery.cometd.js')}" />

	        <script type="text/javascript">
                var secretDialog;
                var featureLicenses;
                var userLicenses;
                var uiData = JSON.parse($.orchestracmsUtil.base64Decode('{!uiDataJSONBase64}'));
                var licenseConnector = JSON.parse(uiData.licenseConnector);
                var featurePacks = JSON.parse(uiData.featurePacks);

                $('document').ready(function() {
                    // Get the setup menu
                    var params = {
                        service: 'SetupService',
                        action: 'getSetupMenu',
                        activeItem: 'licenses'
                    };

                    var handler = function(json, success) {
                        json = $.orchestracmsUtil.parseJSON(json);
                        if (json.error === undefined && (success)) {
                            var menuContainer = document.querySelector('#ocmsSetupMenu');
					        if (menuContainer !== null)
					            menuContainer.innerHTML = json.success.message;
                        }
                    };

                    var options = {
                        cb: handler,
                        readonly: true
                    };
                    doServiceRequest(params, options);
                });

		        $(document).ready(function() {
                    if(uiData.isMultilingualEnabled && !uiData.defaultLanguageSet) {
                        var sname = JSON.stringify(uiData.sname);
                        $(".ocms-toolbar-button a").attr('onclick', '');
                        addTab('Setup','Setup','apex/SetupLanguages?sname=' + sname,'ocms~~Setup');
                        showMessage("<h1>Important Information</h1><p>OrchestraCMS Multilingual license has been installed.</p>"+
                            "<p>Before utilizing the benefits this new feature has to offer you must first set up the default language for your OrchestraCMS site.</p>"+
                            "<p>To set up the default language for this site go to Setup > Languages.</p>");
                    }

					$('#checkNow').button().click(function() {
                        var popup = $('<div></div>').ocmsShowInfoPopup({
							message: 'Checking license keys...',
                            forceProgrammaticClose: true
						});

                        var params = {
                            service: 'SetupService',
                            action: 'getLicenses',
                            allowCallouts: true,
                            sname: uiData.sname,
                            licenseConnector: JSON.stringify(licenseConnector),
                            licenseKey: $('#licenseKeyInput').val(),
                            licenseSecretKey: $('#licenseSecretKeyInput').val()
                        };

						var handler = function(json, result) {
							json = $.orchestracmsUtil.parseJSON(json);
                            if ((result.status === true) && (json.isSuccess)) {
                                var resp = JSON.parse(json.message);
                                licenseConnector = JSON.parse(resp.licenseConnector);
                                featureLicenses = JSON.parse(resp.featureLicenses);
                                userLicenses = JSON.parse(resp.userLicenses);
                                resetTables();
                                $('.licenseLastChecked').html('Last Checked: ' + $.orchestracmsUtil.htmlEncode(resp.lastChecked));

                                if ($('#secretShow').hasClass('hidden'))
                                    toggleSecret();
							} else {
                                showErrorMessage('Error', 'Error checking license keys: ' + $.orchestracmsUtil.htmlEncode(json.message));
                            }

                            popup.ocmsShowInfoPopup('closeMessage');
						};

						var options = {
                            cb: handler,
                            readonly: false
                        };
						doServiceRequest(params, options);
		 			});

					$('#save').button().click(function(){
                        var popup = $('<div></div>').ocmsShowInfoPopup({
							message: 'Saving license keys...',
                            forceProgrammaticClose: true
						});

                        var params = {
                            service: 'SetupService',
                            action: 'updateLicenseKeys',
                            sname: uiData.sname,
                            licenseKey: $('#licenseKeyInput').val(),
                            licenseSecretKey: $('#licenseSecretKeyInput').val()
                        };

						var handler = function(json, result) {
							json = $.orchestracmsUtil.parseJSON(json);
                            if ((result.status == true) && (json.isSuccess)) {
                                var resp = JSON.parse(json.message);
                                licenseConnector = JSON.parse(resp.licenseConnector);
                                featureLicenses = JSON.parse(resp.featureLicenses);
                                userLicenses = JSON.parse(resp.userLicenses);
                                resetTables();

                                if ($('#secretShow').hasClass('hidden'))
                                    toggleSecret();
							} else {
                                showErrorMessage('Error', 'Error saving license keys: ' + $.orchestracmsUtil.htmlEncode(json.message));
                            }

                            popup.ocmsShowInfoPopup('closeMessage');
						};

						var options = {
                            cb: handler,
                            readonly: false
                        };
						doServiceRequest(params, options);
					});

                    $('#secretShowLink').on('click', function() {
                        toggleSecret();
			        });

                    $('#secretHideLink').on('click', function() {
                        toggleSecret();
			        });
	 			});

			   function toggleSecret() {
                    $('#secretShow').toggleClass('hidden');
				    $('#secret').toggleClass('hidden');
			   }

               function showErrorMessage(title, message) {
                    $('<div></div>').ocmsShowErrorPopup({
                        title: title,
                        message : message,
                        width: 300
                    });
               }

			   function showMessage(sMessage) {
				    $('<div/>').ocmsShowWarningPopup({
					    message: sMessage
				    });
			   }

               function resetTables() {
                    var featureLicenseHtml;
                    if (featureLicenses.length > 0) {
                        for (var i = 0; i < featureLicenses.length; i++) {
                            if (featureLicenses[i].license_expired)
                                featureLicenseHtml += '<tr class="licenseExpired">';
                            else
                                featureLicenseHtml += '<tr>';

                            featureLicenseHtml += '<td>' + $.orchestracmsUtil.htmlEncode(featureLicenses[i].license_name) + '</td><td>'
                                + $.orchestracmsUtil.htmlEncode(featureLicenses[i].license_status) + '</td><td>'
                                + $.orchestracmsUtil.htmlEncode(featureLicenses[i].license_expiry_date_text) + '</td><td></td></tr>';
                        }
                    }

                    if (featurePacks.length > 0) {
                        for (var k = 0; k < featurePacks.length; k++) {
                            featureLicenseHtml += '<tr><td colspan="3">' + $.orchestracmsUtil.htmlEncode(featurePacks[k].PackageName) + '</td><td>'
                                + '<a href="' + $.orchestracmsUtil.htmlEncode(featurePacks[k].detailsURL) + '">License Detail</a></td></tr>';
                        }
                    }

                    var userLicenseHtml;
                    if (userLicenses.length > 0) {
                        for (var j = 0; j < userLicenses.length; j++) {
                            if (userLicenses[j].license_expired)
                                userLicenseHtml += '<tr class="licenseExpired">';
                            else
                                userLicenseHtml += '<tr>';

                            userLicenseHtml += '<td>' + $.orchestracmsUtil.htmlEncode(userLicenses[j].license_name) + '</td><td>'
                                + $.orchestracmsUtil.htmlEncode(userLicenses[j].license_quantity) + '</td><td>'
                                + $.orchestracmsUtil.htmlEncode(userLicenses[j].license_used) + '</td><td>'
                                + $.orchestracmsUtil.htmlEncode(userLicenses[j].license_status) + '</td><td>'
                                + $.orchestracmsUtil.htmlEncode(userLicenses[j].license_expiry_date_text) + '</td></tr>';
                        }
                    }

                    if (featureLicenseHtml != null)
                        $('#featureLicenseTable tbody').html(featureLicenseHtml);

                    if (userLicenseHtml != null)
                       $('#userLicenseTable tbody').html(userLicenseHtml);
               } // end of resetTables
			</script>
  		</head>

		<body class="ocms">
            <script type="text/javascript" id="ocmsManageLicensesPageHTML">
                $($.orchestracmsUtil.jsonDecode(uiData.manageLicensesHTML)).appendTo('body');
            </script>
  		</body>
	</html>
</apex:page>