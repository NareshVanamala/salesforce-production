<apex:page controller="cms.SetupLanguages" showHeader="false" sidebar="false" standardStylesheets="false" action="{!flushPlatformCache}">
	<html>
		<head>
			<title>Manage Languages</title>
			<apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms.css')}" />
			<apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms.setupmenu.css')}" />
			<apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms.edit.css')}" />
			<c:CmsScripts />
			<apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/TabInterface.js')}" />
			<apex:includeScript value="{!URLFOR($Resource.cms__OrchestraCMSUI, '/multilingual/LanguageManager.js')}" />

			<script type="text/javascript">
				var uiData = JSON.parse($.orchestracmsUtil.base64Decode('{!uiDataJSONBase64}'));
				var defaultLanguage = JSON.parse(uiData.OCMSdefaultLanguage);
				var availableLanguages = JSON.parse(uiData.availableLanguages);
				
			    $(document).data('cms', {
			        'context'   : 'orchestracms',
			        'namespace' : uiData.namespace,
			        'csrf_token': uiData.token,
			        'site_name' : uiData.siteName
			    });

			    $('document').ready(function() {
			        // Get the setup menu
			        var params = {
						service: 'SetupService',
						action: 'getSetupMenu',
						activeItem: 'languages'
					};

			        var handler = function(json, success) {
			            json = $.orchestracmsUtil.parseJSON(json);
			            if (json.error === undefined && (success)) {
			                var menuContainer = document.querySelector('#ocmsSetupMenu');
			                if (menuContainer !== null)
			                    menuContainer.innerHTML = json.success.message;
			            }
			        };

			        var options = {
						cb: handler,
						readonly: true
					};
			        doServiceRequest(params, options);
			    });

				$(document).ready(function() {
				    $('#DisableLanguage').button().on('click', function() {
				       activateButton('disable');
			        });
			        $('#EditLanguage').button().on('click', function() {
			           activateButton('edit');
			        });
				    $('#AddLanguage').button().on('click', function() {
			           activateButton('add');
			        });
			        $('#SetDefault').button().on('click', function() {
			           activateButton('newDefault');
			        });

				    $('#EditLanguage').button("disable");
				    $('#DisableLanguage').button("disable");
			        $('#SetDefault').button("disable");

				    var enableAdd = uiData.OCMSLanguageCount < uiData.maxOCMSLanguages;

				    $('#languageLimit').text('Limit ' + uiData.OCMSLanguageCount + '/' + uiData.maxOCMSLanguages);

				    if (!enableAdd)
			            $('#AddLanguage').button("disable");

			        if (uiData.hasdefaultLanguage)
			            $('#'+defaultLanguage.Id+'icon').append($(ocmsUI.icon({
							name: 'success12',
							title: '(Site Default Language)'
						})).css('margin', '0 0 3px 5px'));

				    // Gray out all disabled languages
				    $('.langCheckbox').filter(function() {
			            if ($(this).parent().parent().find('.lang_active').text().trim() == "false") {
			                $(this).parent().parent().css('background-color', '#ebebeb');	// background color on the tr
			                $(this).parent().parent().find('td').css('color', '#b8b8b8');	// foreground color needs to go on each child td
			            }
			        });

				    $('.langCheckbox').change(function() {
				        var isDefaultChecked = false;
			            var isActiveChecked  = false;

				        var checkCount = $('.langCheckbox').filter(function() {
				        	if ($(this).is(':checked')) {
				            	if (uiData.hasdefaultLanguage) {
					            	if ($(this).parent().parent().find('#' + defaultLanguage.Id + 'icon').children().length > 0)
					                	isDefaultChecked = true;
				                }

				                if ($(this).parent().parent().find('.lang_active').text().trim() == "true")
			                    	isActiveChecked = true;

				                $(this).parent().parent().addClass('selected_language');
				                return $(this);
				            } else {
				            	$(this).parent().parent().removeClass('selected_language');
				            }
				        });

				        if (checkCount.length > 0) {
				        	if (isDefaultChecked) {
			                	$('#DisableLanguage').button("disable");
				        	} else {
			                	$('#DisableLanguage').button("enable");
			                	if (isActiveChecked)
			            			$('#DisableLanguage').button('option', 'label', 'Disable');
			                	else
			                		$('#DisableLanguage').button('option', 'label', 'Enable');
				        	}
				        	$('#AddLanguage').button("disable");

				        	if (checkCount.length == 1) {
			                	$('#EditLanguage').button("enable");
			                	if (!isDefaultChecked && isActiveChecked)
			                    	$('#SetDefault').button("enable");
				        	} else {
			                	$('#EditLanguage').button("disable");
			                	$('#SetDefault').button("disable");
				        	}
				        } else {
				           $('#DisableLanguage').button("disable");
				           $('#EditLanguage').button("disable");
				           if (enableAdd) { $('#AddLanguage').button("enable"); }
			               $('#SetDefault').button("disable");
				        }
					});

					// Handle checkbox for forcing use of Salesfore User Language (in production mode)
					$('#useUserLanguage').change(function() {
						saveSiteLanguageFlags();
					});

			        // Handle checkbox for enabling Language cookie
			        $('#enableLanguageCookie').change(function() {
			            saveSiteLanguageFlags();
			        });
				});

			    function saveSiteLanguageFlags() {
			        var data = {
						service: 'LanguageManagerAjax',
						action: 'setSiteLanguageFlags',
						token: uiData.token,
			         		sname: uiData.siteName,
						flagValue: ($('#useUserLanguage').filter(':checked').length === 1 ? 'true' : 'false'),
						enableCookie: ($('#enableLanguageCookie').filter(':checked').length === 1 ? 'true' : 'false')
					};

			        var handler = function(json, Success) {
			            json = $.orchestracmsUtil.parseJSON(json);
			            if ((!Success.status) && (json.error !== undefined)) {
			                $('<div></div>').ocmsShowErrorPopup({
			                    title : 'Error setting site language determination rule',
			                    message : $.orchestracmsUtil.htmlEncode(json.error.message)
			                });
			            }
			        };

			        var options = {
						cb: handler
					};
			        doServiceRequest(data, options);
			    } // end of saveSiteLanguageFlags

			    function activateButton(action) {
			        var langList  = [];
			        var lc;
			        var langcount = 0;
			        var isdefault = false;
			        var id;

			        if (action === 'disable') {
			            action = 'enable';
			            $('.selected_language').filter(function() {
			        		langList.push($(this).find(".lang_code").text());

			                if ($(this).find('.lang_active').text().trim() == "true")
			        			action = 'disable';
			        	});

			            lc = langList;
			            langcount = lc.length;
			        } else {
			    		lc = $(".selected_language > .lang_code").text();
			            id = $(".selected_language > .lang_code").attr('id');
			        }

			        if ((action === 'edit') && uiData.hasdefaultLanguage) {
			            if($('.selected_language > td > #'+defaultLanguage.Id+'icon').children().length > 0)
			                isdefault = true;
			        }

			        $('<div></div>').ocmsLanguageEditor({
			            languageList     : availableLanguages,
			            sname            : uiData.siteName,
			            siteId           : uiData.siteId,
			            token            : uiData.token,
			            defaultLanguage  : defaultLanguage.Name,
			            defaultLanguageId: defaultLanguage.Id,
			            action           : action,
			            languageCode     : lc,
			            languageName     : $(".selected_language > .lang_name > span").first().text(),
			            languageLabel    : $(".selected_language > .lang_label").text(),
			            description      : $(".selected_language > .lang_description").text(),
			            priority         : $(".selected_language > .lang_priority").text(),
			            disablefb        : ($(".selected_language > .fb_lang").text().trim() == "true" ? false : true),
			            langcount        : langcount,
			            isdefault        : isdefault,
			            languageId       : id
			        });
			    } // end of activateButton

			    function checkPriority (val) {
			        if (!isNaN(val) && val.length > 3)
			            $('#languagePriority').val(val.substring(0,3));
			        else if (((!isNaN(val) && (val<=0))) || isNaN(val))
			            $('#languagePriority').val('');
			    }
			</script>
		</head>
		<body class="ocms">
		   	<script type="text/javascript" id="ocmsSetupLanguagesPageHTML">
            	$($.orchestracmsUtil.jsonDecode(uiData.setupLanguagesHTML)).appendTo('body');
         	</script>
		</body>
	</html>
</apex:page>