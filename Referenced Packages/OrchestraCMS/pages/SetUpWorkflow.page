<apex:page id="installer" controller="cms.SetUpWorkflowController" showHeader="false" sidebar="false" cache="false" title="OrchestraCMS" standardStyleSheets="false">
    <html style="padding: 0px; margin: 0px;">
        <head>
            <title>Approval Process</title>
    		<apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms-min.css')}" />
    		<apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms.setupmenu.css')}" />
            <apex:stylesheet value="{!URLFOR($Resource.cms__jqueryui, '/css/ocms.css')}" />
    		<c:CmsScripts />

            <script type="text/javascript">
                var uiData = JSON.parse($.orchestracmsUtil.base64Decode('{!uiDataJSONBase64}'));

                $('document').ready(function() {
        	        // Get the setup menu
        	        var params = {
                        service: 'SetupService',
                        action: 'getSetupMenu',
                        activeItem: 'approval-process'
                    };

        	        var handler = function(json, success) {
        	            json = $.orchestracmsUtil.parseJSON(json);
        	            if (json.error === undefined && (success)) {
        	                var menuContainer = document.querySelector('#ocmsSetupMenu');
        	                if (menuContainer !== null)
        	                    menuContainer.innerHTML = json.success.message;
        	            }
        	        };

        	        var options = {
                        cb: handler,
                        readonly: true
                    };        	            
                    
                    doServiceRequest(params, options);
                    initialize();
                });

                function initialize() {
                    // Set the last checked date:
                    $('#ocmsLastCheckedApprovalsDate').html($.orchestracmsUtil.htmlEncode(uiData.lastCheckedDate));

                    // Set up the check now button
                    $('#checkButton').button().click(function() {
                        $.orchestracmsUtil._showWaitMessage('Checking for an approval process.');

                        var params = {
                            service: 'SetupService',
                            action: 'checkForApprovalProcess'
                        };

                        var handler = function(json, success) {
                            json = $.orchestracmsUtil.parseJSON(json);
                            $.orchestracmsUtil._closeWaitMessage();
                            if (json.error === undefined && (success.status === true)) {
                                uiData = json.success.message;
                                initialize();
                            } else {
                                var errorMessage = '';
                                if (json.error !== undefined && json.error.message !== undefined)
                                    errorMessage = $.orchestracmsUtil.htmlEncode(json.error.message);
                                showErrorMessage('Error checking for approval proccess: No approval process found or an error occurred.<br/>' + errorMessage);
                            }
                        };

                        var options = {
                            cb: handler
                        };
                        doServiceRequest(params, options);
                    });

                    if (uiData.workflowExists) {
                        $('.workflowEnabled').removeClass('hidden');
                        $('#ocmsToggleSwitch').ocmsToggleSwitch({
                            'state'     : uiData.workflowEnabled,
                            'onChange'  : function(newValue, toggleSwitch) {
                                var params = {
                                    service: 'SetupService',
                                    action: (newValue ? 'enableApprovalProcess' : 'disableApprovalProcess')
                                };

                                var handler = function(json, success) {
                                    json = $.orchestracmsUtil.parseJSON(json);
                                    $.orchestracmsUtil._closeWaitMessage();
                                    if (json.error !== undefined && (success.status !== true)) {
                                        // If there was an error, reset the state of the switch:
                                        toggleSwitch.setState(!newValue);

                                        var errorMessage = '';
                                        if (json.error !== undefined && json.error.message !== undefined)
                                            errorMessage = $.orchestracmsUtil.htmlEncode(json.error.message);
                                        showErrorMessage('Error enabling/disabling approval proccess: ' + errorMessage);
                                    }
                                }

                                var options = {
                                    cb: handler
                                };
                                doServiceRequest(params, options);
                            }
                        });
                    } else {
                        $('.workflowEnabled').addClass('hidden');
                    }
                } // end of initialize

                function showErrorMessage(message) {
                    $('<div></div>').ocmsShowErrorPopup({
                        title: 'Error',
                        message : message,
                        width: 300
                    });
                }
            </script>
        </head>

        <body class="ocms">
            <script type="text/javascript" id="ocmsWorkflowsPageHTML">
                $($.orchestracmsUtil.jsonDecode(uiData.workflowsHTML)).appendTo('body');
            </script>
        </body>
    </html>
</apex:page>