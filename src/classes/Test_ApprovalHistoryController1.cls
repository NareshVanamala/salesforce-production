@isTest(seealldata = true)
private class Test_ApprovalHistoryController1  {
   static testmethod void TestCaseApproval() {  
   Test.startTest();
    
        Id NewhireRequest=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormParent' and SobjectType = 'NewHireFormProcess__c'].id; 
        Id NewhireRequestSubcase=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormChild' and SobjectType = 'NewHireFormProcess__c'].id; 
        id uid = [select id, name from user limit 1].id;
    
       // NewHireFormProcess__c cs1 = [select id,name from NewHireFormProcess__c where id='a4GP0000000CdnM'];  
        
        NewHireFormProcess__c nh = new NewHireFormProcess__c();
        nh.recordtypeid = NewhireRequest;
        nh.First_Name__c = 'Testing';
        nh.Last_Name__c = 'parent';
        nh.Supervisor__c = 'Ninad';
        nh.Office_Location__c = 'Chennai';
        nh.status__c='New';
        nh.HR_ManagerApprovalStatus__c = 'Sumitted For Approval';
        nh.Employee_Role__c ='Developer';
        nh.DepartmentUpdate__c = '01-External Sales';
        nh.Parking_General__c = true;
        nh.Parking_Executive__c =true;
        nh.Parking_Contractor__c = true;
        insert nh;
        
        nh.HR_ManagerApprovalStatus__c = 'Approved';
        nh.status__c = 'In process';
        update nh;
        
        NewHireFormProcess__c nh1 = new NewHireFormProcess__c();
        nh1.AccessRequest__c = nh.id;
        nh1.recordtypeid=NewhireRequestSubcase;
        nh1.subject__c='Parking General Requset';
        nh1.Parking_General_Status__c = 'Submitted for Approval';
        nh1.status__c = 'Submitted for Approval';
        nh1.HR_ManagerApprovalStatus__c='Approved';
        nh1.First_Name__c=nh.First_Name__c;
        nh1.Last_Name__c=nh.Last_Name__c;
        nh1.Supervisor__c=nh.Supervisor__c;
        nh1.Office_Location__c=nh.Office_Location__c;
        nh1.Employee_Role__c=nh.Employee_Role__c;
        nh1.DepartmentUpdate__c=nh.DepartmentUpdate__c;
        nh1.Title__c=nh.Title__c; 
        nh1.Application_owner__c = uid ;
        nh1.Application_Implementor1__c = uid;
        insert nh1;
        nh1.status__c = 'closed'; 
        nh1.Parking_General_Status__c = 'Approved';
        update nh1;
        
        
        System.assertEquals( nh.First_Name__c,nh1.First_Name__c);

        NewHireFormProcess__c nh2 = new NewHireFormProcess__c();
        nh2.AccessRequest__c = nh.id;
        nh2.recordtypeid=NewhireRequestSubcase;
        nh2.subject__c='Parking Executive Request';
        nh2.Parking_Executive_Status__c= 'Submitted for Approval';
        nh2.status__c = 'Submitted for Approval';
        nh2.HR_ManagerApprovalStatus__c='Approved';
        nh2.First_Name__c=nh.First_Name__c;
        nh2.Last_Name__c=nh.Last_Name__c;
        nh2.Supervisor__c=nh.Supervisor__c;
        nh2.Office_Location__c=nh.Office_Location__c;
        nh2.Employee_Role__c=nh.Employee_Role__c;
        nh2.DepartmentUpdate__c=nh.DepartmentUpdate__c;
        nh2.Title__c=nh.Title__c; 
        nh2.Application_owner__c = uid ;
        nh2.Application_Implementor1__c = uid;
        insert nh2;
        nh2.status__c = 'closed'; 
        nh2.Parking_Executive_Status__c= 'Approved';
        update nh2;
        
                System.assertEquals( nh.First_Name__c,nh2.First_Name__c);

        
        NewHireFormProcess__c nh3 = new NewHireFormProcess__c();
        nh3.AccessRequest__c = nh.id;
        nh3.recordtypeid=NewhireRequestSubcase;
        nh3.subject__c='Parking Contractor Request';
        nh3.Parking_Contractor_request__c= 'Submitted for Approval';
        nh3.status__c = 'Submitted for Approval';
        nh3.HR_ManagerApprovalStatus__c='Approved';
        nh3.First_Name__c=nh.First_Name__c;
        nh3.Last_Name__c=nh.Last_Name__c;
        nh3.Supervisor__c=nh.Supervisor__c;
        nh3.Office_Location__c=nh.Office_Location__c;
        nh3.Employee_Role__c=nh.Employee_Role__c;
        nh3.DepartmentUpdate__c=nh.DepartmentUpdate__c;
        nh3.Title__c=nh.Title__c; 
        nh3.Application_owner__c = uid ;
        nh3.Application_Implementor1__c = uid;
        insert nh3;
        nh3.status__c = 'closed'; 
        nh3.Parking_Executive_Status__c= 'Approved';
        update nh3;
        
                System.assertEquals( nh.First_Name__c,nh3.First_Name__c);

     pagereference ref = system.Currentpagereference();
     ref.getParameters().put('id',nh.id);  
     ApexPages.StandardController controller = new ApexPages.StandardController(nh);
     
            list<ProcessInstance> processInstances  = [select id from ProcessInstance where TargetObjectId =:nh.id];
        // system.assertequals(processInstances.size(),1);
     NewHireQuoteApprovalHistoryController1 n = new NewHireQuoteApprovalHistoryController1();
      n.getquoteId1();
     NewHireQuoteApprovalHistoryController1.MyWrapper m = new NewHireQuoteApprovalHistoryController1.MyWrapper();
     
     //NewHireQuoteApprovalHistoryController1 mcls = new NewHireQuoteApprovalHistoryController1(controller);
     //mcls.wcampList = mcls.getwcampList();
    
    Test.stopTest();
     }
  
  }