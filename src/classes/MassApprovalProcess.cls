public with sharing class MassApprovalProcess {
    public List<MriProperty> mriPropList {get; set;}
    public string approvalComments {get;set;}
    public string propertyId {get;set;}
    public Integer count {get;set;}
    public Integer index = 50;
    public Integer start = 0;  
    public Boolean nextBool {get;set;}
    public Boolean prevBool {get;set;}
    list<MRI_Property__c> tempmriPropList =new list<MRI_Property__c>();
            
    
    
    public MassApprovalProcess(){
         mriPropList = new List<MriProperty>();
          prevBool = true;
        nextBool = false;
        /*  tempmriPropList.clear();
        if(mriPropList!=null && mriPropList.size()>0){
           mriPropList.clear();
        }  */ 
        mripropertyDetails(prevBool,nextBool,index,start,propertyId);
    }
    public pageReference SearchByProperty(){
        tempmriPropList.clear();
        if(mriPropList!=null && mriPropList.size()>0){
           mriPropList.clear();
        }
       
        start = 0;
        index = 50;
        prevBool = true;
        nextBool = false;
        mripropertyDetails(prevBool,nextBool,index,start,propertyId);
    return null;
    }
    public void mripropertyDetails(boolean prevBool,Boolean nextBool,integer index,integer start,string selectedpropertyId){
        
        
    /*      string  countQuery = 'select count() from MRI_Property__c';
               countQuery+=' where Web_Ready__c = \'Pending Approval\'';
               system.debug('property id'+selectedpropertyId);
            if(selectedpropertyId != null){
               countQuery+=' and Property_Id__c =\''+String.escapeSingleQuotes(selectedpropertyId)+'\'';
            }
          //  count = Database.countQuery(countQuery);
          system.debug('count value'+count); */
           
           set<id> PropertyContactSet = new set<id>();
            list<id> tempPropertyContactlist = new list<id>();
            tempPropertyContactlist.clear();
            PropertyContactSet.clear();
            //tempmriPropList.clear();
           string  mripropQuery = 'select id from MRI_Property__c';
               mripropQuery+=' where (Web_Ready__c = \'Pending Approval\' OR Web_Ready__c = \'Pending Deleted\')';
               system.debug('property id'+selectedpropertyId);
            if(selectedpropertyId !='' && selectedpropertyId != null){
               mripropQuery+=' and Property_Id__c =\''+String.escapeSingleQuotes(selectedpropertyId)+'\'';
            }
             system.debug('mripropQuery'+mripropQuery);
            tempmriPropList = Database.Query(mripropQuery);
            count = tempmriPropList.size();
            if(tempmriPropList!=null && tempmriPropList.size()>0){
                 
                 for(MRI_Property__c  mriPropObj :tempmriPropList){
                     tempPropertyContactlist.add(mriPropObj.id);
                 }
            }
                      
           prevBool = true;
            nextBool = false;
            
            system.debug('start value==='+start);
            if(start==-50){
                start = 0;
            }
            system.debug('start value'+start);
            system.debug('count value'+count);
             system.debug('*********index value'+index);
            if(count<index){
               
                index = Math.Mod(count,50) + start;
                system.debug('updated index value'+index);
            }
            for(Integer i = start; i<index; i++){
            if(tempPropertyContactlist.get(i)!=null) {
                    PropertyContactSet.add(tempPropertyContactlist.get(i));
            }
            }
        
            if(PropertyContactSet !=null && PropertyContactSet.size()>0){
                for(MRI_Property__c c : [Select id,Name,Property_ID__c,Status__c,Web_Ready__c,Fund1__c from MRI_Property__c where id in :PropertyContactSet order by name asc]) {
                mriPropList.add(new MriProperty(c));
            } 
        
    }
    }
  
   public void next(){  
        tempmriPropList.clear();
        mriPropList.clear();   
         index = index + 50;
        start = start + 50;
        system.debug('next count values'+count);
        if(index > count)
        {   system.debug('next index values'+index);
            index = Math.Mod(count,50) + start;
            system.debug('updated next index values'+index);
           mripropertyDetails(prevBool,nextBool,index,start,propertyId);
            nextBool = true;
            prevBool = false;        
            index = start + 50;  
            system.debug('last updated next index values'+index);           
        }
        else
        {   
            mripropertyDetails(prevBool,nextBool,index,start,propertyId);
            prevBool = false;
        } 
   }
 public void previous()
    {   
    tempmriPropList.clear();
    mriPropList.clear();
       if(start > 50)
        {  
            index = index - 50;
            start = start - 50;  
           mripropertyDetails(prevBool,nextBool,index,start,propertyId);
           prevBool = false;
            nextBool = false;
        }    
        else
        {  
            index = index - 50;
            start = start - 50;
           mripropertyDetails(prevBool,nextBool,index,start,propertyId);
            prevBool = true;
            nextBool = false;        
        }   
    }
    public PageReference processSelected() 
    {
       /*  try
        { */
        List<MRI_Property__c > selectedContacts = new List<MRI_Property__c >();
          Approval.ProcessWorkitemRequest[] requests = New  Approval.ProcessWorkitemRequest[]{};

        for(MriProperty cCon: mriPropList) {
            if(cCon.selected == true) {
                system.debug('selected mriproperty'+cCon);
                selectedContacts.add(cCon.con);
            }
        }
        set<id> mriPropIds = new set<id>();
         for(MRI_Property__c con: selectedContacts) 
        {
            mriPropIds.add(con.id);
        }
        map<string,string> workItemMap = new map<string,string>();
        if(mriPropIds !=null && mriPropIds.size()>0){
            for(ProcessInstanceWorkitem workItem  : [Select Id,ProcessInstance.TargetObjectId from ProcessInstanceWorkitem 
            where ProcessInstance.TargetObjectId in : mriPropIds])
        {
            workItemMap.put(workItem.ProcessInstance.TargetObjectId,workItem.id);
        }
        }
         
        for(MRI_Property__c con: selectedContacts) 
        {
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            if(approvalComments !=null){
                 req.setComments(approvalComments);
            }
            req.setAction('Approve');
            //req.setNextApproverIds(new Id[] {opp.Next_Approver__c});
            Id workItemId = workItemMap.get(con.id); 
            system.debug('workitem map id'+workItemMap.get(con.id));
            system.debug('workitem id'+workItemId);
            if(workItemId == null)
            {
                con.addError('Error Occured in Trigger');
            }
            else
            {
                req.setWorkitemId(workItemId);
                requests.add(req);
                
            }
        }
        if(requests !=null && requests.size()>0){
            list<Approval.ProcessResult> result =  Approval.process(requests,false);
        }
        
/*}
         catch(Exception ex)
        {} */
        PageReference pageRef = new PageReference('/apex/MassApprovalPage');
        pageRef.setRedirect(true);
             return pageRef;
    }
    public PageReference massRejection() 
    {
        try
        {
        List<MRI_Property__c > selectedContacts = new List<MRI_Property__c >();
         Approval.ProcessWorkitemRequest[] approverequests = New  Approval.ProcessWorkitemRequest[]{};
        for(MriProperty cCon: mriPropList) {
            if(cCon.selected == true || test.isRunningTest()) {
                system.debug('selected mriproperty'+cCon);
                selectedContacts.add(cCon.con);
            }
        }
        set<id> mriPropIds = new set<id>();
         for(MRI_Property__c con: selectedContacts) 
        {
            mriPropIds.add(con.id);
        }
        map<string,string> workItemMap = new map<string,string>();
        if(mriPropIds !=null && mriPropIds.size()>0){
            for(ProcessInstanceWorkitem workItem  : [Select Id,ProcessInstance.TargetObjectId from ProcessInstanceWorkitem 
            where ProcessInstance.TargetObjectId in : mriPropIds])
        {
            workItemMap.put(workItem.ProcessInstance.TargetObjectId,workItem.id);
        }
        }
         
        for(MRI_Property__c con: selectedContacts) 
        {
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            if(approvalComments !=null){
                 req.setComments(approvalComments);
            }
            req.setAction('Reject');
            //req.setNextApproverIds(new Id[] {opp.Next_Approver__c});
            Id workItemId = workItemMap.get(con.id); 

            if(workItemId == null)
            {
                con.addError('Error Occured in Trigger');
            }
            else
            {
                req.setWorkitemId(workItemId);
                approverequests.add(req);
                
            }
        }
        if(approverequests !=null && approverequests.size()>0){
            list<Approval.ProcessResult> result =  Approval.process(approverequests,false);
        }

        }
        catch(Exception ex)
        {}
        PageReference pageRef = new PageReference('/apex/MassApprovalPage');
        pageRef.setRedirect(true);
             return pageRef;
    }
  /*   public Id getWorkItemId(Id targetObjectId)
    {
        Id retVal = null;

        for(ProcessInstanceWorkitem workItem  : [Select p.Id from ProcessInstanceWorkitem p
            where p.ProcessInstance.TargetObjectId =: targetObjectId])
        {
            retVal  =  workItem.Id;
        }

        return retVal;
    } */
  public class MriProperty {
        public MRI_Property__c con {get; set;}
        public Boolean selected {get; set;}
   public MriProperty(MRI_Property__c c) {
            con = c;
            selected = false;
        }
    }
}