Public with sharing class newCompController
{
   Id dealid;
   Deal__c  dealnew;
   Id compid;
   
   Public PageReference createnewCompController()
   {
      dealnew= new Deal__c();
      dealid= ApexPages.currentPage().getParameters().get('id');
      system.debug('Check the deal id..'+dealid);
      dealnew=[select id,Address__c,Going_NOI__c,Going_In_CAP_Rate__c,Loan_Term_All_Cash__c,Address_Line_1__c,Year_1_NOI__c,Households__c,Median_Income__c,Population__c,Acquisition_Fee__c,Average_NOI__c,CAP_Rate__c,City__c,Name,Source__c,Date_Identified__c,Fund__c,Initial_10_yr_Fad__c,Initial_5_yr_Fad__c,Land_Acres__c,Occupancy__c,Overall_CAP_Rate__c,Overall_Yield_IRR__c,Ownership_Type__c,Parking_Ratio__c,Parking_Spaces__c,Primary_Use__c, Property_Type__c,Region__c,Source_Notes__c,State__c,Tenancy__c,Total_Price__c,Total_SF__c,Vacant_SF__c,Year_Built__c,Zip_Code__c,Assumable_Debt__c from Deal__c where id=:dealid];
      system.debug('Check the deal '+dealnew);
      Comp__c c = new Comp__c(); 
      c.X5_mile_Households__c=dealnew.Households__c;
      c.Median_Income__c = dealnew.Median_Income__c;  
      c.Population__c = dealnew.Population__c;
      c.Acquisition_Fee__c=dealnew.Acquisition_Fee__c;
      c.Asking_CAP_Rate__c = dealnew.Going_In_CAP_Rate__c;
      c.Average_NOI__c=dealnew.Average_NOI__c;
      c.CAP_Rate__c=dealnew.CAP_Rate__c;
      c.City__c=dealnew.City__c;
      c.Date_Identified__c=dealnew.Date_Identified__c;
      c.Fund__c=dealnew.Fund__c;
      c.Going_NOI__c=dealnew.Year_1_NOI__c;
      c.Initial_10_yr_Fad__c=dealnew.Initial_10_yr_Fad__c;
      c.Initial_5_yr_Fad__c= dealnew.Initial_5_yr_Fad__c;
      c.Land_Acres__c=dealnew.Land_Acres__c;
      c.name=dealnew.name;
      c.Occupancy__c = dealnew.Occupancy__c;
      c.Overall_CAP_Rate__c = dealnew.Overall_CAP_Rate__c;
      c.Overall_Yield__c = dealnew.Overall_Yield_IRR__c;
      c.Ownership_Type__c = dealnew.Ownership_Type__c;
      //c.Parking_Ratio__c = dealnew.Parking_Ratio__c;
      c.Parking_Spaces__c = dealnew.Parking_Spaces__c;
      c.Primary_Use__c = dealnew.Primary_Use__c;
      c.Address_Line_1__c = dealnew.Address__c;
      //c.Property__c = dealnew.Property__c;
      c.Property_Type__c = dealnew.Property_Type__c;
      c.Region__c = dealnew.Region__c;
      c.Source_Notes__c = dealnew.Source_Notes__c;
      c.State__c = dealnew.State__c;
      c.Tenancy__c = dealnew.Tenancy__c;
      //c.Total_Price__c = dealnew.Total_Price__c;
      c.Total_SF__c = dealnew.Total_SF__c;
      c.Vacant_SF__c = dealnew.Vacant_SF__c;
      c.Year_Built__c = dealnew.Year_Built__c;
      c.Zip_Code__c = dealnew.Zip_Code__c;
      c.All_Cash__c = dealnew.Loan_Term_All_Cash__c;
      c.Assumable_Debt__c = dealnew.Assumable_Debt__c;
      if (Schema.sObjectType.Comp__c .isCreateable())

      insert c;
      compid=c.id;
     
      PageReference clonepage = new PageReference('/'+ compid+ '/e?retURL=%2F'+ compid);  
      return clonepage;
      
   }
   
   
   
   
   
 }