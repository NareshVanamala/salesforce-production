@isTest
private class Test_Submittocompliance 
 {
   public static testMethod void TestforSubmittocompliance () 
   {
          Case c1 = new Case();
          c1.Status = 'Approved';
          c1.Subject = 'Hello';
          c1.Priority= 'High';
          c1.Origin = 'Email';
          c1.Description = 'Hello test class';
          insert c1;
          
         RecordType rtype = [Select Name, Id From RecordType where isActive=true and sobjecttype='Case'and name='Compliance Case Management'];
         
         
          Case c2 = new Case();
          c2.Status = c1.Status;
          c2.Subject = c1.Subject;
          c2.Priority= c1.Priority;
          c2.Origin = c1.Origin;
          c2.Description = c1.Description;
          c2.RecordTypeId = rtype.id;
          insert c2;
          
          system.assertequals(c2.Priority,c1.Priority);

          ApexPages.currentPage().getParameters().put('id',c2.id);  
          ApexPages.StandardController controller = new ApexPages.StandardController(c2);  
          submitTocompliance  x = new submitTocompliance(controller);
          x.createCase();
          
   }
 }