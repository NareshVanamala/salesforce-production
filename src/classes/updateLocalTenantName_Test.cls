@isTest(SeeAllData=true)
private class updateLocalTenantName_Test {
    static testMethod void updatelocalTenTest(){
    MRI_Property__c mri = [select id from MRI_Property__c limit 1];
    Lease__c lease = [select id,name from Lease__c where Tenant_Name__c !=null limit 1];    
    Property_Contacts__c pc = new Property_Contacts__c();
    
    pc.Name='test_contact';
    pc.MRI_Property__c=mri.id;
    pc.Local_Tenant_Name__c='testing';
    insert pc;
    system.assertequals(pc.MRI_Property__c,mri.id);

    Property_Contact_Leases__c pcl= new Property_Contact_Leases__c();
    pcl.Property_Contacts__c=pc.id;
    pcl.Contact_Type__c='Assignee';
    pcl.Lease__c=lease .id;
    insert pcl;
    system.assertequals(pcl.Lease__c,lease .id);

    }
    
    }