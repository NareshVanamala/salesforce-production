global class Websites_CCDrupalContactJob implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

   global String accountIdString='';
   String query= '';
   String ColeCapitalContactsEndPointURL=Websites_HttpClass.DrupalEndPointURLs('Cole Capital Contacts');  

   public boolean isFromTestClass=false;

   public void checkTest(){
      if(Test.isRunningTest() && (Websites_Drupal_Endpoint_URLs__c.getAll().Values()==null || Websites_Drupal_Endpoint_URLs__c.getAll().Values().isEmpty())){
          Websites_HttpClass.insertDrupalEnpointURLsForTest();
      }
      if(Test.isRunningTest() && !isFromTestClass){
          Test.setMock(HttpCalloutMock.class, new Websites_HttpClass.Mock('{"status": "SUCCESS"}', 200));
      }
   }
   
   global Websites_CCDrupalContactJob(List<Id> accountsIds){
      checkTest();
      for(Id a: accountsIds){
          this.accountIdString += '\'' + a + '\'' + ',';
      }
      if(this.accountIdString!=''){
         this.accountIdString = this.accountIdString.substring(0, this.accountIdString.length()-1); 
      }
      query = 'select id from Contact limit 0';
      
      if(this.accountIdString!=''){
          query= 'select id from contact where FA__c=true and account.id IN ('+ this.accountIdString+')' ;
      }
      
   }

   global Websites_CCDrupalContactJob(){
      checkTest();
      query= 'select id from contact where FA__c=true' ;
   
   }
   
   global Database.QueryLocator start(Database.BatchableContext BC){
      
      if(Test.isRunningTest())
        query= 'select id from contact limit 1' ;

      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<Contact> contacts){
           
       List<Id> contactIds= new List<Id>();
       
       for(Contact c: contacts){
            contactIds.add(c.Id);
       }
       system.debug('contacts.size()'+contacts.size());
       //String ColeCapitalContactsEndPointURL=Websites_HttpClass.DrupalEndPointURLs('Cole Capital Contacts');
       try{
            Websites_HttpClass.makeCallSync(ColeCapitalContactsEndPointURL, 
                                    Websites_HttpClass.COLECAPITAL_DRUPAL_TOKEN, 
                                    Websites_HttpClass.METHOD_POST,
                                    Websites_HttpClass.TIMEOUT_MAX, 
                                    Websites_HttpClass.ACCEPT, 
                                    JSON.serialize(contactids));
        }catch(CalloutException e){
            system.debug(logginglevel.info, '-----------CalloutException: '+e.getStackTraceString()+'\n'+e.getMessage());
        }  
   }

   global void finish(Database.BatchableContext BC){
       system.debug(logginglevel.info, '-----------Job finished---------');
   }
}