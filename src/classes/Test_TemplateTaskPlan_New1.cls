@isTest
private class Test_TemplateTaskPlan_New1 {

    static testMethod void myUnitTest11() {
      
            PageReference pageRef1 = Page.TemplateTaskPlan_New;
            system.test.setCurrentPageReference(pageRef1);   
            
            Template__c temp = new Template__c();
            temp.Name = 'Template';
            
            insert temp ; 
            
           /* Template__c temp2 = new Template__c();
            temp2.Name = 'Template';
            insert temp2 ; */
            
            Task_Plan__c tp = new Task_Plan__c ();
            tp.Name='test';
            tp.priority__c = 'Medium';
            tp.Index__c = 1;
            tp.Template__c =temp.Id ; 
            insert tp ; 
            system.assertequals(tp.Template__c,temp.Id );

            
            Task_Plan__c childTask = new Task_Plan__c ();
            ChildTask.Name='SubTask1';
            ChildTask.Priority__c = 'Medium';
            ChildTask.Index__c=1;
            childTask.Parent__c = tp.id;
            childTask.Template__c=temp.id;
            Insert childTask;
            
            system.assertequals(childTask.Parent__c,tp.id);

           
            Task_Plan__c tp0 = new Task_Plan__c ();
            tp0.Name='test1';
            tp0.Field_to_update_Date_Received__c ='Accenture_Due_Date__c';
            tp0.Date_Received__c =Date.newInstance(1999,12, 12);
           tp0.Field_to_update_for_date_needed__c ='Accenture_Due_Date__c';
            tp0.Date_Needed__c =null ;
             tp0.Index__c=2;
            tp0.Template__c =temp.Id ; 
            
            insert tp0 ;
            
            
            Task_Plan__c tp1 = new Task_Plan__c ();
            tp1.Name='test1';
            tp1.Field_to_update_Date_Received__c ='Appraisal_Date__c';
            tp1.Date_Received__c =Date.newInstance(1999,12, 12);
           tp1.Field_to_update_for_date_needed__c ='Appraisal_Date__c';
            tp1.Date_Needed__c =null ;
            tp1.Index__c=3;
            tp1.Template__c =temp.Id ; 
            
            insert tp1 ;
            
            
          
          
              Task_Plan__c tp2 = new Task_Plan__c ();
            tp2.Name='test1';
            tp2.Field_to_update_Date_Received__c ='Estimated_COE_Date__c';
            tp2.Date_Received__c =Date.newInstance(1999,12, 12);
           tp2.Field_to_update_for_date_needed__c ='Dead_Date__c';
            tp2.Date_Needed__c =null ;
             tp2.Index__c=4;
            tp2.Template__c =temp.Id ; 
            
            insert tp2 ;
          
          
            Task_Plan__c tp3 = new Task_Plan__c ();
            tp3.Name='test1';
            tp3.Field_to_update_Date_Received__c ='Rep_Burn_Off__c';
            tp3.Date_Received__c =Date.newInstance(1999,12, 12);
           tp3.Field_to_update_for_date_needed__c ='LOI_Signed_Date__c';
            tp3.Date_Needed__c =null ;
             tp3.Index__c=5;
            tp3.Template__c =temp.Id ; 
            
            insert tp3 ;
            
            system.assertequals(tp3.Template__c,temp.Id );

          
          
            Task_Plan__c tp4 = new Task_Plan__c ();
            tp4.Name='test1';
            tp4.Field_to_update_Date_Received__c ='Site_Visit_Date__c';
            tp4.Date_Received__c =Date.newInstance(1999,12, 12);
           tp4.Field_to_update_for_date_needed__c ='Estimated_Hard_Date__c';
            tp4.Date_Needed__c =null ;
             tp4.Index__c=6;
            tp4.Template__c =temp.Id ; 
            
            insert tp4 ;
           ApexPages.currentPage().getParameters().put('parentindex','1'); 
           ApexPages.currentPage().getParameters().put('removeRec','1');
           ApexPages.currentPage().getParameters().put('Subtask','1'); 
           ApexPages.currentPage().getParameters().put('partask','1');  
           ApexPages.currentPage().getParameters().put('id',temp.id);           
         
            
            ApexPages.StandardController sc1 = new ApexPages.standardController(temp);
            TemplateTaskPlan_New taskPlans1 = new TemplateTaskPlan_New(sc1);
            taskPlans1.addTask();
             taskPlans1.getRelatedObjectPicklist();
           taskPlans1.addsubTask();
          taskPlans1.removesubTask();
            taskPlans1.checkDuplicateTasks();
            taskPlans1.arrangetask();
             taskPlans1.getdateFields();
            taskPlans1.nutralact();
            taskPlans1.nutralexists();
            Taskplans1.Edit1();
           TaskPlans1.CalldateFields();
            taskPlans1.Save();
            TaskPlans1.RemoveTask();
    
            
    } 
    

    
}