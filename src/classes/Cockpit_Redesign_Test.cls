@isTest(SeeAllData=true)
public class Cockpit_Redesign_Test{
     static testmethod void myTest_Method1(){
        
       Account acc = new Account();
       acc.name='cockpitcontact1FirstName';
       insert acc;
       system.assert(acc!=null);
         
         
       Contact cockpitcontact1= new Contact();
       cockpitcontact1.firstname='cockpitcontact1FirstName';
       cockpitcontact1.lastname='cockpitcontact1lastName';
       cockpitcontact1.Territory__c='HeartLand';
       cockpitcontact1.Phone='4802375787'; 
       cockpitcontact1.accountid=acc.id; 
       insert cockpitcontact1;
       system.assert(cockpitcontact1!=null);
         
             
       Contact cockpitcontact11= new Contact();
       cockpitcontact11.firstname='cockpitcontact11FirstName';
       cockpitcontact11.lastname='cockpitcontact11lastName';
       cockpitcontact11.Territory__c='Chesapeake';
       cockpitcontact11.Phone='4802375797'; 
       insert cockpitcontact11;   
       system.assert(cockpitcontact11!=null);
             
       task t =new task();
       t.status='Completed';
       t.activitydate=system.today();
       t.whoid= cockpitcontact1.id;
       t.ownerid=Userinfo.getuserid();  
       insert t;  
       system.assert(t!=null);
          
       task tt =new task();
       tt.status='New';
       tt.activitydate=system.today()-1;
       tt.whoid= cockpitcontact11.id;
       tt.ownerid=Userinfo.getuserid();  
       insert tt;  
       system.assert(tt!=null); 
         
       REIT_Investment__c ret=new REIT_Investment__c();
       ret.Rep_Contact__c=cockpitcontact1.id;
       ret.Fund__c='3770';
       ret.Current_Capital__c=6975.00;
       insert ret;
       system.assert(ret!=null);
          
                     
       Automated_Call_List__c CockpitAutomatedCalllist= new Automated_Call_List__c();
       CockpitAutomatedCalllist.name='CockpitAutomatedcallListName';
       CockpitAutomatedCalllist.Type_Of_Call_List__c='Campaign Call List';
       CockpitAutomatedCalllist.IsDeleted__c=false;
       CockpitAutomatedCalllist.Archived__c=false;
       insert CockpitAutomatedCalllist;
       system.assert(CockpitAutomatedCalllist!=null);
       
       Automated_Call_List__c CockpitAutomatedCalllist1= new Automated_Call_List__c();
       CockpitAutomatedCalllist1.name='CockpitAutomatedcallListName';
       CockpitAutomatedCalllist1.Type_Of_Call_List__c='Campaign Call List';
       CockpitAutomatedCalllist1.IsDeleted__c=false;
       CockpitAutomatedCalllist1.Archived__c=false;
       insert CockpitAutomatedCalllist1;
       system.assert(CockpitAutomatedCalllist1!=null);
       
       
       Contact_Call_List__c cockpitCCL=new Contact_Call_List__c();
       cockpitCCL.Automated_Call_List__c=CockpitAutomatedCalllist.id;
       cockpitCCL.contact__c=cockpitcontact1.id;
       cockpitCCL.IsDeleted__c=false;
       cockpitCCL.Call_Complete__c=false;
       insert cockpitCCL;
       system.assert(cockpitCCL!=null);
       
       Contact_Call_List__c cockpitCCL1=new Contact_Call_List__c();
       cockpitCCL1.Automated_Call_List__c=CockpitAutomatedCalllist.id;
       cockpitCCL1.contact__c=cockpitcontact11.id;
       cockpitCCL1.IsDeleted__c=false;
       cockpitCCL1.Call_Complete__c=false;
       insert cockpitCCL1;
       system.assert(cockpitCCL1!=null);     
       
       Contact_Call_List__c cockpitCCL2=new Contact_Call_List__c();
       cockpitCCL2.Automated_Call_List__c=CockpitAutomatedCalllist1.id;
       cockpitCCL2.contact__c=cockpitcontact1.id;
       cockpitCCL2.IsDeleted__c=false;
       cockpitCCL2.Call_Complete__c=false;
       insert cockpitCCL2;
       system.assert(cockpitCCL2!=null);
       
       Manage_Cockpit__c ct=new Manage_Cockpit__c();
       Id MngRelatedList= Schema.SObjectType.Manage_Cockpit__c.getRecordTypeInfosByName().get('Manage Related Lists').getRecordTypeId(); 
       ct.recordtypeid=MngRelatedList;
       
       
       Id MngRelatedList1= Schema.SObjectType.Manage_Cockpit__c.getRecordTypeInfosByName().get('Manage Related Lists').getRecordTypeId(); 
       Manage_Cockpit__c ctyy=new Manage_Cockpit__c();
       ctyy.recordtypeid=MngRelatedList1;
       ctyy.Section_Name__c='Activity History';
       ctyy.Related_List_API_Name__c='task';
       ctyy.Show_On_Cockpit__c=True;
       ctyy.Field_Set_Name__c ='SalesCockpit';
       ctyy.Contact_look_Up__c='whoid';
       ctyy.Order__c=1;
       insert ctyy;
       system.assert(ctyy!=null); 
       
       Id MngAdvDetails= Schema.SObjectType.Manage_Cockpit__c.getRecordTypeInfosByName().get('Manage Advisor Details').getRecordTypeId(); 
       Manage_Cockpit__c ctyy1=new Manage_Cockpit__c();
       ctyy.recordtypeid=MngAdvDetails;
       ctyy.Section_Name__c='Advisor Details';
       ctyy.Show_On_Cockpit__c=True;
       ctyy.Field_Set_Name__c ='SalesCockpit';
       ctyy.Object_Name__c='contact';
       ctyy.Order__c=1;
       insert ctyy1;
       system.assert(ctyy1!=null); 
       
       Manage_Cockpit__c cty1=new Manage_Cockpit__c();
       cty1.recordtypeid=MngRelatedList1;
       cty1.Section_Name__c='REIT Investments';
       cty1.Related_List_API_Name__c='REIT_Investment__c';
       cty1.Show_On_Cockpit__c=True;
       cty1.Field_Set_Name__c ='SalesCockpit';
       cty1.Contact_look_Up__c='Rep_Contact__c';
       cty1.Order__c=2;
       insert cty1;
       system.assert(cty1!=null); 
             
       Test.StartTest();
       string searchText1='{"FirstName":"Naresh","Territory__c":"Atlantic;AZ/UT","Last_Contact_Date__c":"2016-12-10","Account.X1031_Selling_Agreement__c":"CCPT V;CCIT II","Account.Suspended__c":true}';
       string inboundcalllist=Cockpit_Redesign.getthecontactforInboundCall(searchText1);
       
       String myoverduetaskstring = '{\n' +
       '"taskid":'+t.id+','+
       ' "CallRecap" : ' + 
      '{ "Subject__c" : "Left Voice Mail" , ' + 
        '"OwnerId" : "IT Service Account"  , ' + 
        '"Call_Type__c" : "Outbound"  , ' + 
        '"Description" : "Testing the description " },\n' +
     ' "NewTask" : ' + 
      '{ "Call_Type__c" : "Outbound" , ' + 
        '"OwnerId" : "IT Service Account"  , ' + 
        '"ActivityDate" : "2016-11-23"  , ' + 
        '"Subject" : "Testing the application" },\n' + 
         
      ' "NewEvent" : ' + 
      '{ "StartDateTime" : "2016-11-19T21:20:00" , ' + 
        '"EndDateTime" : "2016-11-20T21:50:00"  , ' + 
        '"OwnerId" : "IT Service Account"  , ' + 
        '"Event_Subject__c" : "Group Appointment"  , ' + 
        '"Call_Type__c" : "Outbound"  , ' + 
        '"Location" : "Phoenix"  , ' + 
        '"Description" : "Testing the application" },\n' +     
        
     ' "Contact__r" : ' + 
      '{ "FirstName" : "cockpitcontact1FirstName"  , ' + 
        '"LastName" : "cockpitcontact1lastName"  , ' + 
        '"Territory__c" : "HeartLand" },\n' +     
      ' "Account" : ' + 
      '{ "Id" : "'+acc.Id+'",' +  
        '"Name" : "HeartLandAcct" },\n' + 
        
    ' "contactId" : "'+cockpitcontact1.id+'",\n' +
    ' "cclId" : "'+cockpitCCL1.id+'"\n' +
    '}';
       
       //string myoverduetaskstring='{"taskid":"'+t.id+'","CallRecap":{"Call_Type__c":"Outbound","Subject__c":"Left Voice Mail","OwnerId":"Naresh Vanamala","Description":"test"},"NewTask":{"Call_Type__c":"Outbound","OwnerId":"Naresh Vanamala","ActivityDate":"2016-12-16","Subject":"test"},"NewEvent":{"StartDateTime":"2016-12-09T11:06:00","EndDateTime":"2016-12-09T11:36:00","OwnerId":"John Marshall","Event_Subject__c":"Ext Appt Event","Call_Type__c":"Outbound","Location":"test","Description":"test"},"nextWhoId":"'+cockpitcontact11.id+'"}';
       string overduetaskreturnstring=Cockpit_Redesign.completeOverduetasks(myoverduetaskstring);
       string calllistnameUI='{"Territory":"Atlantic;AZ/UT;Central CA;Chesapeake","callqueue":"Campaign Call List","contactRecordTypeId": "012300000004rLn"} ';
       string myname=Cockpit_Redesign.getCallListNames(calllistnameUI);
       string cclrecordsname1='{"Territory":"Atlantic;AZ/UT;HeartLand;Chesapeake", "callList":"CockpitAutomatedcallListName", "callQueue":"Campaign Call List", "searchString":"test", "contactRecordTypeId": "012300000004rLn"}';
       string CCLrecordnamw1=Cockpit_Redesign.passCCLRecords(cclrecordsname1);
       
       
      String jsonInput = '{\n' +
     ' "CallRecap" : ' + 
      '{ "Subject__c" : "Left Voice Mail" , ' + 
        '"OwnerId" : "IT Service Account"  , ' + 
        '"Call_Type__c" : "Outbound"  , ' + 
        '"Description" : "Testing the description " },\n' +
     ' "NewTask" : ' + 
      '{ "Call_Type__c" : "Outbound" , ' + 
        '"OwnerId" : "IT Service Account"  , ' + 
        '"ActivityDate" : "2016-11-23"  , ' + 
        '"Subject" : "Testing the application" },\n' + 
         
      ' "NewEvent" : ' + 
      '{ "StartDateTime" : "2016-11-19T21:20:00" , ' + 
        '"EndDateTime" : "2016-11-20T21:50:00"  , ' + 
        '"OwnerId" : "IT Service Account"  , ' + 
        '"Event_Subject__c" : "Group Appointment"  , ' + 
        '"Call_Type__c" : "Outbound"  , ' + 
        '"Location" : "Phoenix"  , ' + 
        '"Description" : "Testing the application" },\n' +     
        
     ' "Contact__r" : ' + 
      '{ "FirstName" : "cockpitcontact1FirstName"  , ' + 
        '"LastName" : "cockpitcontact1lastName"  , ' + 
        '"Territory__c" : "HeartLand" },\n' +     
      ' "Account" : ' + 
      '{ "Id" : "'+acc.Id+'",' +  
        '"Name" : "HeartLandAcct" },\n' + 
        
    ' "contactId" : "'+cockpitcontact1.id+'",\n' +
    ' "cclId" : "'+cockpitCCL1.id+'"\n' +
    '}';
    //"Account":{"attributes":{"type":"Account","url":"/services/data/v40.0/sobjects/Account/0015000000k0gUMAAY"},
    //"Name":"Test_AJ_Acc1","Id":"0015000000k0gUMAAY","Active_Selling_Agreement__c":null}
    String jsonInput1 = '{\n' +
      ' "Contact__r" : ' + 
      '{ "FirstName" : "cockpitcontact1FirstName"  , ' + 
        '"LastName" : "cockpitcontact1lastName"  , ' + 
        '"Territory__c" : "HeartLand" },\n' + 
     ' "Account":{"Id":"'+acc.id+'",\n'+
     '"X1031_Selling_Agreement__c":"CCPT II;CCPT II - Add on;GOFI;CCPTIII;CCPTIII - Add On;CCPT IV;CCPT V",\n'+
     '"Name":"WFG Investments, Inc.","attributes":{"url":"/services/data/v38.0/sobjects/Account/0015000000Jp0nIAAR","type":"Account"}},\n'+       
    ' "contactId" : "'+cockpitcontact1.id+'",\n' +
    ' "cclId" : "'+cockpitCCL.id+'"\n' +
    '}'; 
    
    
      string myvalues;
      //string mycondetail=Cockpit_Redesign.passContactAndRelatedList(cockpitcontact1.id);
      string resultstring=Cockpit_Redesign.SaveAndCompleteCall(jsonInput);
      string mydetails=Cockpit_Redesign.passDetailstoUI('myvalues');
      string condetail=Cockpit_Redesign.ContactSave(jsonInput1);
      string overduetsklist=Cockpit_Redesign.passFollowUpandOverdueTask(jsonInput1);      
      contact conty=[select id, firstname ,lastname from contact where firstname='cockpitcontact1FirstName' and lastName='cockpitcontact1lastName' limit 1];
      string ctJason = JSON.serializepretty(conty);

      Cockpit_Redesign.completeCall(cockpitCCL.id,t,cockpitcontact1.id);
      Cockpit_Redesign.completeCall('',t,cockpitcontact1.id);
      string snoozedetail='{"cclIds":"'+cockpitCCL.id+'","contactId":"'+cockpitcontact1.id+'","nextcclid":"'+cockpitCCL1.id+'","snoozeDate":"2017-11-28T06:34:00","taskId":"","nextTaskId":null}'; 
      string mycontact=Cockpit_Redesign.snoozeContact(snoozedetail);
      string snoozedetail1='{"cclIds":null,"contactId":"'+cockpitcontact1.id+'","nextcclid":"'+cockpitCCL1.id+'","snoozeDate":"2017-11-28T06:34:00","taskId":"","nextTaskId":null}'; 
      string snoozeContact=Cockpit_Redesign.snoozeContact(snoozedetail1);
      string skipConstring ='{"cclIds":"'+cockpitCCL.id+'", "contactId":"'+cockpitcontact1.id+'","nextTaskId":"'+t.id+'", "nextcclid":"'+cockpitCCL1.id+'", "skipReason":"Just called", "taskId":"'+t.id+'"}';
      string mycontact1=Cockpit_Redesign.skipContact(skipConstring);   
      string snoozetask='{"cclIds":null,"contactId":null,"nextcclid":null,"snoozeDate":"2017-11-28T06:34:00","taskId":"'+tt.id+'","nextTaskId":"'+t.id+'"}';
      //string mysnnozedTask1=Cockpit_Redesign.snoozeContact(snoozetask);
      //Cockpit_Redesign.getAdvisordetailSection(cockpitcontact1.id);
      Test.StopTest();
  } 
  
}