public with sharing class REIT_ProxyVotes 
{

    public boolean showArrowInvsetorName { get; set; }
    public boolean showArrowCCPT5fund { get; set; }
    public boolean showArrowCCIT2fund{ get; set; }
    public boolean showArrowINAVafund{ get; set; }
    public boolean showArrowINAVwfund{ get; set; }
    public boolean showArrowINAVifund{ get; set; }
    public boolean showArrowCCPT4fund { get; set; }
    public Integer showmorecount{get;set;}
    public boolean bnCount{get;set;}
    
    
    
    public void ShowMore()
        {
        showmorecount = showmorecount + 10; 
        if(showmorecount >= WrapperCls.size())
        bnCount= true; 
        }
    
    public String sortArrow {
        get { if (sortDir == null) { sortArrow = 'sortDesc'; } return sortArrow; }
        set;
      }

        private String soql {get;set;}
        public String sortDir {
        get { if (sortDir == null) { sortDir = 'asc'; } return sortDir; }
        set;
        }
        public String sortField {
        get
         {
          if (sortField == null) 
          {
          sortField = 'CCPT4fund';
          showArrowCCPT4fund =true;
           } return sortField; }
        set;
        }

    public void toggleSort()
     {
     
       list<REITProxyShareWrappercls>Nolist=new list<REITProxyShareWrappercls>();
       list<REITProxyShareWrappercls>NAlist=new list<REITProxyShareWrappercls>();
       list<REITProxyShareWrappercls>Yeslist=new list<REITProxyShareWrappercls>(); 
       system.debug('<<<<<<Welcome to togglesortMethod>>>>>>>>>>>>>>');
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
        sortArrow = sortDir.equals('asc') ? 'sortDesc' : 'sortAsc';
        system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> '+sortField);
        if(sortField=='Investor1')
        {
          showArrowInvsetorName=true; 
          showArrowCCPT5fund =false;
          showArrowCCIT2fund=false;
          showArrowINAVafund=false;
          showArrowINAVwfund=false;
          showArrowINAVifund=false;
          showArrowCCPT4fund=false; 
        }  
        if(sortField=='CCPT5fund')
        {
          showArrowInvsetorName=false; 
          showArrowCCPT5fund =true;
          showArrowCCIT2fund=false;
          showArrowINAVafund=false;
          showArrowINAVwfund=false;
          showArrowINAVifund=false;
          showArrowCCPT4fund=false; 
          
        }  
        if(sortField=='CCIT2fund')
        {
          showArrowInvsetorName=false; 
          showArrowCCPT5fund =false;
          showArrowCCIT2fund=true;
          showArrowINAVafund=false;
          showArrowINAVwfund=false;
          showArrowINAVifund=false;
          showArrowCCPT4fund=false; 
          
        }  
        if(sortField=='INAVafund')
        {
          showArrowInvsetorName=false; 
          showArrowCCPT5fund =false;
          showArrowCCIT2fund=false;
          showArrowINAVafund=true;
          showArrowINAVwfund=false;
          showArrowINAVifund=false;
          showArrowCCPT4fund=false; 
          
        }  
        
         if(sortField=='INAVwfund')
        {
          showArrowInvsetorName=false; 
          showArrowCCPT5fund =false;
          showArrowCCIT2fund=false;
          showArrowINAVafund=false;
          showArrowINAVwfund=true;
          showArrowINAVifund=false;
          showArrowCCPT4fund=false; 
          
        }  
         if(sortField=='INAVwfund')
        {
          showArrowInvsetorName=false; 
          showArrowCCPT5fund =false;
          showArrowCCIT2fund=false;
          showArrowINAVafund=false;
          showArrowINAVwfund=true;
          showArrowINAVifund=false;
          showArrowCCPT4fund=false; 
          
        }  
        
         if(sortField=='INAVifund')
        {
          showArrowInvsetorName=false; 
          showArrowCCPT5fund =false;
          showArrowCCIT2fund=false;
          showArrowINAVafund=false;
          showArrowINAVwfund=false;
          showArrowINAVifund=true;
          showArrowCCPT4fund=false; 
          
        }  
        if(sortField=='CCPT4fund')
        {
          showArrowInvsetorName=false; 
          showArrowCCPT5fund =false;
          showArrowCCIT2fund=false;
          showArrowINAVafund=false;
          showArrowINAVwfund=false;
          showArrowINAVifund=false;
          showArrowCCPT4fund=true; 
          
        }  
        getWrapperCls();
        
    }

   public String contactid{get;set;} 
   public String myValue{get;set;}
   public list<REIT_Proxy_Votes__c> REITProxyVotes{get;set;}
   public map<String, list<REIT_Proxy_Votes__c>>InvestorNametoREITmap;
   public list<REITProxyShareWrappercls> WrapperCls;   
   public String getcontactid() 
   {
        system.debug('>>>>>>>>>>>>>> '+contactid);
        return contactid;
   }
    
   public void setcontactid (String s) 
   {
         myValue=s;
   } 
      
   public REIT_ProxyVotes()
   {
      
    system.debug('check the id'+contactid);
    showArrowCCPT4fund=true;
    sortDir='desc';
    sortArrow='sortAsc';
     showmorecount=10;
           
    
   
   }
    
    public List<REITProxyShareWrappercls> getWrapperCls()
   {
   
       System.debug('*****************This is Wrapperlist method***************** ');
        system.debug('check the contact id'+contactid );
         /*****************crete the map for investor*****************/
       list<REIT_Proxy_Votes__c>CCITIIFundlist=new list<REIT_Proxy_Votes__c>();
      list<REIT_Proxy_Votes__c>INAVAFundlist=new list<REIT_Proxy_Votes__c>();
      list<REIT_Proxy_Votes__c>INAVWFundlist=new list<REIT_Proxy_Votes__c>();
      list<REIT_Proxy_Votes__c>INAVIFundlist=new list<REIT_Proxy_Votes__c>();
      list<REIT_Proxy_Votes__c>CCPTVFundlist=new list<REIT_Proxy_Votes__c>();
      list<REIT_Proxy_Votes__c>CCPTIVFundlist=new list<REIT_Proxy_Votes__c>();
       
      Map<String, list<REIT_Proxy_Votes__c>>InvestorNametoCCITIIREITmap=new map<String, list<REIT_Proxy_Votes__c>>();
      Map<String, list<REIT_Proxy_Votes__c>>InvestorNametoINAVWREITmap=new map<String, list<REIT_Proxy_Votes__c>>();
      Map<String, list<REIT_Proxy_Votes__c>>InvestorNametoINAVIREITmap=new map<String, list<REIT_Proxy_Votes__c>>();
      Map<String, list<REIT_Proxy_Votes__c>>InvestorNametoINAVAREITmap=new map<String, list<REIT_Proxy_Votes__c>>();
      Map<String, list<REIT_Proxy_Votes__c>>InvestorNametoCCPTVREITmap=new map<String, list<REIT_Proxy_Votes__c>>();
      Map<String, list<REIT_Proxy_Votes__c>>InvestorNametoCCPTIVREITmap=new map<String, list<REIT_Proxy_Votes__c>>();
      
      
      
      list<REIT_Proxy_Votes__c>myCT= [select id,name,Investor__c,Fund_Name__c,Amount__c,Shares__c,VoteStatus__c from REIT_Proxy_Votes__c where Contact__c=:contactid order by Investor__c] ;
     system.debug('This is my list'+myCT);
      for(REIT_Proxy_Votes__c RCV:myCT)
      {
         if(RCV.Fund_Name__c=='CCIT II')
          CCITIIFundlist.add(RCV);
         if(RCV.Fund_Name__c=='Income NAV – W')
          INAVWFundlist.add(RCV);
        if(RCV.Fund_Name__c=='Income NAV – I')
          INAVIFundlist.add(RCV); 
        if(RCV.Fund_Name__c=='Income NAV – A')
          INAVAFundlist.add(RCV);  
         if(RCV.Fund_Name__c=='CCPT V')
          CCPTVFundlist.add(RCV);  
        if(RCV.Fund_Name__c=='CCPT IV')
          CCPTIVFundlist.add(RCV);  
     }
     
     //CCPT IV Map
      if(CCPTIVFundlist.size()>0)
       {
         for(REIT_Proxy_Votes__c ri:CCPTIVFundlist)
         {
           if(InvestorNametoCCPTIVREITmap.containsKey(ri.Investor__c))
                 InvestorNametoCCPTIVREITmap.get(ri.Investor__c).add(ri);
           else
               InvestorNametoCCPTIVREITmap.put(ri.Investor__c, new List<REIT_Proxy_Votes__c>{ri});
          }
       }
     
     
     
     //CCIT-II map
       if(CCITIIFundlist.size()>0)
       {
         for(REIT_Proxy_Votes__c ri:CCITIIFundlist)
         {
           if(InvestorNametoCCITIIREITmap.containsKey(ri.Investor__c))
                 InvestorNametoCCITIIREITmap.get(ri.Investor__c).add(ri);
           else
               InvestorNametoCCITIIREITmap.put(ri.Investor__c, new List<REIT_Proxy_Votes__c>{ri});
          }
       }
       //INAV_I map
        if(INAVIFundlist.size()>0)
       {
         for(REIT_Proxy_Votes__c ri:INAVIFundlist)
         {
           if(InvestorNametoINAVIREITmap.containsKey(ri.Investor__c))
                 InvestorNametoINAVIREITmap.get(ri.Investor__c).add(ri);
           else
               InvestorNametoINAVIREITmap.put(ri.Investor__c, new List<REIT_Proxy_Votes__c>{ri});
          }
       }
      //INAV_W map
        if(INAVWFundlist.size()>0)
       {
         for(REIT_Proxy_Votes__c ri:INAVWFundlist)
         {
           if(InvestorNametoINAVWREITmap.containsKey(ri.Investor__c))
                 InvestorNametoINAVWREITmap.get(ri.Investor__c).add(ri);
           else
               InvestorNametoINAVWREITmap.put(ri.Investor__c, new List<REIT_Proxy_Votes__c>{ri});
          }
       }
       //INAV-A map   
        if(INAVAFundlist.size()>0)
       {
         for(REIT_Proxy_Votes__c ri:INAVAFundlist)
         {
           if(InvestorNametoINAVAREITmap.containsKey(ri.Investor__c))
                 InvestorNametoINAVAREITmap.get(ri.Investor__c).add(ri);
           else
               InvestorNametoINAVAREITmap.put(ri.Investor__c, new List<REIT_Proxy_Votes__c>{ri});
          }
       }
       //CCPT V map
        if(CCPTVFundlist.size()>0)
       {
         for(REIT_Proxy_Votes__c ri:CCPTVFundlist)
         {
           if(InvestorNametoCCPTVREITmap.containsKey(ri.Investor__c))
                 InvestorNametoCCPTVREITmap.get(ri.Investor__c).add(ri);
           else
               InvestorNametoCCPTVREITmap.put(ri.Investor__c, new List<REIT_Proxy_Votes__c>{ri});
          }
       }
   
   
   
      WrapperCls= new List<REITProxyShareWrappercls >(); 
      List<AggregateResult> groupedResults=[select Investor__c from REIT_Proxy_Votes__c where (Contact__c=:contactid and Fund__c!=null) GROUP BY Investor__c order by Investor__c ASC];
      system.debug('This is my aggregate results'+groupedResults);
       for (AggregateResult ar : groupedResults)
       {
          system.debug('This is my ar'+ar) ;
          string Investor=(String)ar.get('Investor__c');
        
          String CCIT2;
          String CCPT5;
          String CCPT4;
          String INAVi;
          String INAVw;
          String INAVa;        
          
           if(InvestorNametoCCPTIVREITmap.get(Investor)!=null)
          {
            list<REIT_Proxy_Votes__c>CCPTIVlist=InvestorNametoCCPTIVREITmap.get(Investor);
            if(CCPTIVlist.size()>0)
            {
               CCPT4=CCPTIVlist[0].VoteStatus__c;
            }
         
         }
          
                 
           
          if(InvestorNametoCCITIIREITmap.get(Investor)!=null)
          {
            list<REIT_Proxy_Votes__c>CCITIIlist=InvestorNametoCCITIIREITmap.get(Investor);
            if(CCITIIlist.size()>0)
            {
               CCIT2=CCITIIlist[0].VoteStatus__c;
            }
         
         }
         if(InvestorNametoINAVAREITmap.get(Investor)!=null)
         {
              list<REIT_Proxy_Votes__c>INAVAlist=InvestorNametoINAVAREITmap.get(Investor); 
                INAVa=INAVAlist[0].VoteStatus__c;
          }
          if(InvestorNametoINAVWREITmap.get(Investor)!=null)
          {
              list<REIT_Proxy_Votes__c>INAVWlist=InvestorNametoINAVWREITmap.get(Investor);
                 INAVw=INAVWlist[0].VoteStatus__c;
          }
          
          if(InvestorNametoINAVIREITmap.get(Investor)!=null)
          {
             list<REIT_Proxy_Votes__c>INAVIlist=InvestorNametoINAVIREITmap.get(Investor);
                  INAVi=INAVIlist[0].VoteStatus__c;
           }
          
          
          if(InvestorNametoCCPTVREITmap.get(Investor)!=null)
          {
          list<REIT_Proxy_Votes__c>CCPTVlist=InvestorNametoCCPTVREITmap.get(Investor);
                 CCPT5=CCPTVlist[0].VoteStatus__c;
          }
                
           if((CCPT5!='No')&&(CCPT5!='Yes'))
             CCPT5='N/A'; 
           if((CCPT4!='No')&&(CCPT4!='Yes'))
             CCPT4='N/A'; 
           if((CCIT2!='No')&&(CCIT2!='Yes'))
             CCIT2='N/A'; 
           if((INAVa!='No')&&(INAVa!='Yes'))
            INAVa='N/A'; 
          if((INAVw!='No')&&(INAVw!='Yes'))
             INAVw='N/A';       
          if((INAVi!='No')&&(INAVi!='Yes'))
             INAVi='N/A';       
        REITProxyShareWrappercls wrap=new REITProxyShareWrappercls(Investor,CCPT5,CCPT4,CCIT2,INAVa,INAVw,INAVi); 
       
           system.debug('<<>>>>'+wrap);
           WrapperCls.add(wrap);
            bnCount = WrapperCls.size()<= 10;
           
       
       
       }
         list<REITProxyShareWrappercls>yeslist=new list<REITProxyShareWrappercls>();
         list<REITProxyShareWrappercls>Nolist=new list<REITProxyShareWrappercls>();
         list<REITProxyShareWrappercls>NAlist=new list<REITProxyShareWrappercls>();
          system.debug('let me know the sort field'+sortField); 
         for(REITProxyShareWrappercls ctt:WrapperCls)
         { 
           if(sortField=='CCPT5fund')
           {
              if(ctt.CCPT5fund=='No')
                Nolist.add(ctt);
              if((ctt.CCPT5fund=='N/A' )||(ctt.CCPT5fund==null))
                NAlist.add(ctt);
              if(ctt.CCPT5fund=='Yes')
               yeslist.add(ctt);   
           }  
           if(sortField=='CCPT4fund')
           {
              if(ctt.CCPT4fund=='No')
                Nolist.add(ctt);
              if((ctt.CCPT4fund=='N/A' )||(ctt.CCPT5fund==null))
                NAlist.add(ctt);
              if(ctt.CCPT4fund=='Yes')
               yeslist.add(ctt);   
           }  
           
                      
           if(sortField=='CCIT2fund')
           {
              if(ctt.CCIT2fund=='No')
                Nolist.add(ctt);
              if((ctt.CCIT2fund=='N/A' )||(ctt.CCIT2fund==null))
                NAlist.add(ctt);
              if(ctt.CCIT2fund=='Yes')
               yeslist.add(ctt);   
           }   
           if(sortField=='INAVafund')
           {
              if(ctt.INAVafund=='No')
                Nolist.add(ctt);
              if((ctt.INAVafund=='N/A' )||(ctt.INAVafund==null))
                NAlist.add(ctt);
              if(ctt.INAVafund=='Yes')
               yeslist.add(ctt);   
           }   
           if(sortField=='INAVwfund')
           {
              if(ctt.INAVwfund=='No')
                Nolist.add(ctt);
              if((ctt.INAVwfund=='N/A' )||(ctt.INAVwfund==null))
                NAlist.add(ctt);
              if(ctt.INAVwfund=='Yes')
               yeslist.add(ctt);   
           }  
           if(sortField=='INAVifund')
           {
              if(ctt.INAVifund=='No')
                Nolist.add(ctt);
              if((ctt.INAVifund=='N/A' )||(ctt.INAVifund==null))
                NAlist.add(ctt);
              if(ctt.INAVifund=='Yes')
               yeslist.add(ctt);   
           }  
         }  
         system.debug('Check the sortdirection'+sortDir);
         system.debug('Check the sortdirection'+sortArrow);
         list<REITProxyShareWrappercls>mylist=new list<REITProxyShareWrappercls>();
         
         if((sortDir=='asc')&&(sortArrow=='sortDesc'))
        {
         
               mylist.addAll(yeslist); 
               mylist.addAll(Nolist);
                mylist.addAll(NAlist);
         }
         
        else  if((sortDir=='desc')&&(sortArrow=='sortAsc'))
        {
               mylist.addAll(Nolist);
                mylist.addAll(NAlist); 
               mylist.addAll(yeslist); 
               
         }
        
         
        WrapperCls=new list<REITProxyShareWrappercls>();
        WrapperCls.addAll(mylist);
       
       
     return WrapperCls;
    }  

     public class REITProxyShareWrappercls 
     {
    
        public String  Investor1{set;get;}
        public string CCIT2fund{set;get;}  
        public string CCPT5fund{set;get;} 
        public string CCPT4fund{set;get;} 
        public string INAVafund{set;get;} 
        public string INAVwfund{set;get;} 
        public string INAVifund{set;get;} 
        public REITProxyShareWrappercls(string Investorname, string ccptV, string CCPT4,String ccitII, string INAVa, string INAVw, String INAVi)
       {     
        
           Investor1=Investorname;
           CCIT2fund=ccitII;
           CCPT5fund=ccptV;
           CCPT4fund=CCPT4;
           INAVafund=INAVa;
           INAVwfund=INAVw;
           INAVifund=INAVi;
                       
       }
       
        
       
    }



}