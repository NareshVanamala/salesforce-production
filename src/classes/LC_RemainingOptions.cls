global with sharing class LC_RemainingOptions 
{

    public LC_LeaseOpprtunity__c LeaseOpportunity{get;set;}
    public String ids {get;set;}
   
    public LC_RemainingOptions(ApexPages.StandardController controller)
    {
       ids = ApexPages.currentPage().getParameters().get('id');
       system.debug('get me the current deal id'+ids);
       if(ids!=null)
       LeaseOpportunity=[select id,Remaining_Options_Intact__c,RemainingOptions_Description__c from LC_LeaseOpprtunity__c  where id=:ids];
       system.debug('LeaseOpportunity before update'+LeaseOpportunity);
    }
   public LC_RemainingOptions()
   {
   
   }
   public pagereference  AdviserCallList()
   {
        // string ids = ApexPages.currentPage().getParameters().get('id');
           //if(LeaseOpportunity.Remaining_Options_Intact__c==True)
              // LeaseOpportunity.RemainingOptions_Description__c ='';
        
             update  LeaseOpportunity;
            system.debug('LeaseOpportunity after update'+ids );
    
     PageReference pg = new PageReference('/apex/LC_RemainingOptions?id=' + ids );
      pg.getParameters().put('message', 'Your Message here');
     pg.setRedirect(true);
   return pg;
   
   }
    
}