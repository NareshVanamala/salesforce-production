public with sharing class FundSplitController {
    public String quoteId1 {get;set;}
    public list <Event_Request_Split__c>Fundsplitlists ;
   
    public List<Event_Request_Split__c> getFundsplits() {
     try{
         
        Fundsplitlists =new list<Event_Request_Split__c> ();    
     
      if (quoteId1 != null)
     
       Fundsplitlists = [Select Id,Department_Split__c,Department_Split_Percentage__c,Fund_Split__c,Fund_Split_Percentage__c,Region_Split__c,Split__c from Event_Request_Split__c where Event_Automation_Request__c= :quoteId1];
       // return Fundsplitlists ;
       System.debug('Check the list..'+Fundsplitlists );       
      
      return Fundsplitlists;
    } 
     
    Catch(Exception e)
    {
    return null;
     }
}     
}