@isTest private class CM_CustomVendorTest
{
    static testmethod void test() 
    {
    
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standard@testorg.com');
        Insert u;
        
        Vendor__c Vc= new Vendor__c();
        Vc.name='TestSnehal';
        Vc.City__c='Hyderbad';
        insert Vc;
        
        MRI_PROPERTY__c mpt= new MRI_PROPERTY__c();
        mpt.name='TestRec';
        mpt.Property_Type__c= 'Freestanding Retail';
        insert mpt;
        MRI_PROPERTY__c mp = [SELECT ID, Team__c FROM MRI_PROPERTY__c  Where ID =:mpt.ID];
        
        System.assertNotEquals( Vc.name,mpt.name);

        Project__c pct= new Project__c();
        pct.name='TestProject';
        pct.Building__c=mpt.id;
        pct.Status__c ='Complete';
        Pct.Vendor__c = Vc.Id;
        Pct.Actual_Cost__c = 123456;
        pct.Work_Type__c = 'Tenant Improvement';
        pct.Tenant_Name__c ='Test Tenant';
        pct.Maintenance_Manager__c= u.id;
        insert pct;
        
        Project__c pct1= new Project__c();
        pct1.name='TestProject1';
        pct1.Building__c=mpt.id;
        pct1.Status__c ='Complete';
        pct1.Vendor__c = Vc.Id;
        pct1.Actual_Cost__c = 123456;
        pct1.Work_Type__c = 'Tenant Improvement';
        pct1.Tenant_Name__c ='Test Tenant';
        pct1.Maintenance_Manager__c= u.id;
        insert pct1; 
        
        System.assertNotEquals(pct.name,pct1.name);
                      
        Project__c pct2= new Project__c();
        pct2.name='TestProject1';
        pct2.Building__c=mpt.id;
        pct2.Status__c ='Complete';
        pct2.Vendor__c = Vc.Id;
        pct2.Actual_Cost__c = 123456;
        pct2.Work_Type__c = 'BTS';
        pct2.Tenant_Name__c ='Test Tenant';
        pct2.Maintenance_Manager__c= u.id;
        insert pct2;
        
        System.assertEquals(pct2.name,'TestProject1');
        
        ApexPages.StandardController controller = new ApexPages.StandardController(pct); 
        ApexPages.currentPage().getParameters().put('id',pct.id);
        CM_CustomVendor csr=new CM_CustomVendor(controller );
        csr.selectedvendor='TestSnehal';
        csr.SaveVendor();
        
    }
    
 }