public without sharing class ocmsICSAttachmenttest {
public string string1{get;set;}
   
    Public ocmsICSAttachmenttest(apexpages.standardcontroller controller)
    {
    string urlid = apexpages.currentpage().getparameters().get('id');
    if(urlid!=null)
    {
    campaign e=[select id,startdate,enddate,name,Event_Title__c,Description,status,Meeting_Start_Date_Time__c,Meeting_End_Date_Time__c from campaign where id=:urlid];
    String formatted_startDate='';
    String formatted_endDate='';
    string Description1='';
    if(e.startdate!=null)
    {
    Date startDate = e.startdate;
    formatted_startDate = string.valueof(startDate);
    }
    else
    {
    formatted_startDate='';
    }
    if(e.enddate!=null)
    {
    Date enddate = e.enddate;
    formatted_endDate = string.valueof(endDate);
    }
    else
    {
    formatted_enddate='';
    }
    string location1='';
    string Subject1=e.name;
    if(e.Description!=null)
    {
    Description1=e.Description.replace('\n', ' ');
    }
    else{
    Description1='';
    }
    String fromAddress = 'colecapital@arcp.com';
    String toAddress = Userinfo.getuseremail();
    string1=doIcsAttachment(formatted_startDate, formatted_endDate, location1,Description1, subject1, fromAddress, toAddress);
     system.debug('@@@'+string1);
    }
    
    ID urlid1 = apexpages.currentpage().getparameters().get('str');
    if(urlid1!=null)
    {
    WebCast__c e=[select name,Is_Webcast_Replay__c,id,Campaigns__c,Contacts__c,Description__c,Email_Address__c,iCalendar_URL__c,meeting_ID__c,Name__c,Start_Date_Time__c,Status__c,Viewble_on_website__c,Webcast_Replay_URL__c,Add_to_Calender__c from WebCast__c where id=:urlid1];
    String formatted_startDate='';
    string Description1='';
    if(e.Start_Date_Time__c!=null)
    {
    Datetime startDate = e.Start_Date_Time__c;
    formatted_startDate = startDate.format('yyyyMMdd\'T\'Hmm\'00\'');
    }
    else
    {
    formatted_startDate='';
    }
    string location1='';
    string Subject1=e.name;
    if(e.Description__c!=null)
    {
    Description1=e.Description__c.replace('\n', ' ');
    }
    else{
    Description1='';
    }
    String fromAddress = 'colecapital@arcp.com';
    String toAddress = Userinfo.getuseremail();
    string1=doIcsAttachment1(formatted_startDate, location1, Description1, subject1, fromAddress, toAddress);
    system.debug('@@@'+string1);
    }
    
    
    }
    
    
  public string doIcsAttachment(String formatted_startDate, String formatted_endDate, String location,string Description,
                                            String subject, String fromAddress, String toAddress) {
                    // Now Contruct the ICS file
                    system.debug('@@@@@@@@@'+Description);
                   String [] icsTemplate = new List<String> {'BEGIN:VCALENDAR',
                        'PRODID:-//Schedule a Meeting',
                        'VERSION:2.0',
                        //'METHOD:REQUEST',
                        'BEGIN:VEVENT',
                        'DTSTART: ' + formatted_startDate,
                        'DTSTAMP: '+ String.valueof(DateTime.Now()),
                        'DTEND: ' + formatted_endDate,
                        'LOCATION: ' + location,
                        'SUMMARY: ' + subject,
                       // 'UID: ' + String.valueOf(Crypto.getRandomLong()),
                        'X-ALT-DESC;FMTTYPE=text/calendar: ',
                       'DESCRIPTION: ' +Description,
                      //  'ORGANIZER:MAILTO: ' + fromAddress,
                        'ATTENDEE;CN="' + 'TestingICSAttachment' + '";RSVP=TRUE:mailto: ' + toAddress,
                        'END:VEVENT',
                        'END:VCALENDAR'
                    };
            String attachment = String.join(icsTemplate, '\n');
            return attachment;
        }
        
 public string doIcsAttachment1(String formatted_startDate,String location,string Description,
                                            String subject, String fromAddress, String toAddress) {
                    // Now Contruct the ICS file
                    system.debug('@@@@@@@@@'+Description);
                   String [] icsTemplate = new List<String> {'BEGIN:VCALENDAR',
                        'PRODID:-//Schedule a Meeting',
                        'VERSION:2.0',
                        //'METHOD:REQUEST',
                        'BEGIN:VEVENT',
                        'DTSTART: ' + formatted_startDate,
                        'DTSTAMP: '+ String.valueof(DateTime.Now()),
                       // 'DTEND: ' + formatted_endDate,
                        'LOCATION: ' + location,
                        'SUMMARY: ' + subject,
                       // 'UID: ' + String.valueOf(Crypto.getRandomLong()),
                        'X-ALT-DESC;FMTTYPE=text/calendar: ',
                       'DESCRIPTION: ' +Description,
                      //  'ORGANIZER:MAILTO: ' + fromAddress,
                        'ATTENDEE;CN="' + 'TestingICSAttachment' + '";RSVP=TRUE:mailto: ' + toAddress,
                        'END:VEVENT',
                        'END:VCALENDAR'
                    };
            String attachment = String.join(icsTemplate, '\n');
            return attachment;
        }      
}