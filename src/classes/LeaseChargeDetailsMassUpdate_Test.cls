@isTest(SeeAllData = true)
private class LeaseChargeDetailsMassUpdate_Test
{
     static testMethod void testDoGet() {
    
     MRI_PROPERTY__c mriPropObj = new MRI_PROPERTY__c();
     mriPropObj = [Select id from MRI_PROPERTY__c where State__c = 'AZ' limit 1];
     
        Lease__c leaseobj =new Lease__c();
        leaseobj.name = 'Test Lease';
        leaseobj.Lease_ID__c='15644';
        leaseobj.MRI_PROPERTY__c = mriPropObj.id;
        leaseobj.MRI_PM__c = userinfo.getuserid();
        leaseobj.Lease_Status_Time_Stamp__c= Datetime.now();
        leaseobj.Lease_Status__c='Active';
        leaseobj.Lease_Category__c='Lease Category 1';
        leaseobj.Income_Category__c='ADM-Administrative Fee';
        leaseobj.Web_Ready__c='Pending Approval';
        insert leaseobj;
        
        //leaseobj =[Select id from Lease__c where Lease_Status__c = 'Active' limit 1];
        Lease_Charges_Request__c leaseCharReqobj = new  Lease_Charges_Request__c();
        leaseCharReqobj.Lease__c=leaseobj.id;
        leaseCharReqobj.Source_Code__c='test';
        leaseCharReqobj.Approval_Status__c='Not Submitted for Approval';
        leaseCharReqobj.MRI_Property__c = leaseobj.MRI_PROPERTY__c;
        //leaseobj.LeaseChargesRequest__c='test';
        insert leaseCharReqobj;
        
        Lease_Charge_Details__c leaseCharDetailsobj = new  Lease_Charge_Details__c();
        leaseCharDetailsobj.LeaseChargesRequest__c=leaseCharReqobj.id;
        leaseCharDetailsobj.Charge_Code__c='CB1 - (TIC ONLY) Chargeable Bldg Maint #1';
        leaseCharDetailsobj.Charge_Type__c='NRC - Invoice (Insurance)';
        leaseCharDetailsobj.Description__c='Test';
        leaseCharDetailsobj.Mass_Update__c = true;
        insert leaseCharDetailsobj; 
        
        Test.startTest();
  
         System.assertEquals(leaseobj.MRI_PROPERTY__c, mriPropObj.id);
        System.assertEquals(leaseobj.MRI_PROPERTY__c, leaseCharReqobj.MRI_Property__c);
       System.assertEquals(leaseCharReqobj.id, leaseCharDetailsobj.LeaseChargesRequest__c);

        
        ApexPages.CurrentPage().getparameters().put('id', leaseCharReqobj.id);
        ApexPages.StandardController StandardsObjectController =  new ApexPages.StandardController(leaseCharReqobj);
        LeaseChargeDetailsMassUpdate coninvattctrl  = new LeaseChargeDetailsMassUpdate(StandardsObjectController);
        //LeaseChargeDetailsPdfExtn.createAttachment(leaseCharReqobj.id);
        LeaseChargeDetailsMassUpdate.MassUpdate(leaseCharReqobj.id);
        Test.stopTest();
    }
    
  }