@isTest
private class Test_RegionSplitclass 
{
   public static testMethod void TestforRegionSplitclass () 
   {     
        Account acc = new Account();
        acc.Name = 'Test Prospect Account';
        Insert acc;
        
        //Set up user
        User us = [SELECT Id FROM User limit 1];          
           
        Event_Automation_Request__c ear = new Event_Automation_Request__c();
        ear.Name='BDE13006';
        ear.Event_Name__c='testeventname';
        ear.Start_Date__c=System.Today();
        ear.End_Date__c=System.Today();
        ear.Broker_Dealer_Name__c=acc.id;
        ear.Event_Location__c='testlocation';
        ear.Key_Account_Manager_Name_Requestor__c=us.id;
        ear.Event_Venue__c='testvenue';
        insert ear;
        
        //id, Region_Split__c,name,Split__c from Event_Request_Split__c
        Event_Request_Split__c ers = new Event_Request_Split__c();
        ers.Region_Split__c='Florida IBD';
        ers.Split__c=100;
        ers.name='reg';
        ers.Event_Automation_Request__c=ear.id;
        insert ers;
        
          
        
        ApexPages.currentPage().getParameters().put('id',ear.id); 
        
        ApexPages.StandardController controller = new ApexPages.StandardController(ear);
        RegionSplitclass rsc = new RegionSplitclass(controller);              
        
        rsc.updatedlist1=ers;
        rsc.removecon();
        rsc.editRecord();       
        rsc.SaveRecord();
        rsc.newProgram();
        rsc.savenewProg();
        rsc.cancelnewProg();       
    }
}