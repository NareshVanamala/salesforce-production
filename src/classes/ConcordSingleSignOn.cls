global class ConcordSingleSignOn {
 
         public Contact con;
         public string email{get;set;}
         public string key{get;set;}
         public string clientid{get;set;}
                 
        public ConcordSingleSignOn(ApexPages.StandardController controller)
        {
        
            //this.con=(Contact)controller.getRecord();        
            //this.con=[select Id,FirstName,LastName,Email,MailingStreet,MailingCity,MailingPostalCode,MailingState,MailingCountry,Company_Name__c,Account.Name,Phone from Contact Where Id=:con.Id];  
             //User user=new User();        
            //user=[select id, FirstName, LastName,Email from User where id = :Userinfo.getUserId()];        
            //clientid='101';
            //email=user.email;
             //Datetime test=Datetime.now();
             //string curDate=test.format('yyyyMd');
             //System.Debug(curDate); 
             //string keyValue='101'+user.email+curDate+'cole2011';
             //Blob bkey=Crypto.generateDigest('MD5',Blob.valueOf(keyValue));
            // key=EncodingUtil.convertToHex(bkey);
                  
                                       
        }
        
        @AuraEnabled
        public static string loginToConcord_Lightning(string cid){
            return loginToConcord(cid, UserInfo.getUserId());
        }
        webservice static string loginToConcord(string cid, string uid) 
        {
 
            User user=new User();        
            user=[select id, FirstName, LastName,Email,UserName from User where id = :uid];   
            Contact con=[select Id,FirstName,LastName,Email,MailingStreet,MailingCity,MailingPostalCode,MailingState,MailingCountry,Company_Name__c,Account.Name,Phone from Contact Where Id=:cid];  
     
             Datetime test=Datetime.now();
             String company;
             String curDate=test.format('yyyyMd');
             System.Debug(curDate); 
             String keyValue='101'+user.UserName+curDate+'cole2011';
             System.Debug(keyValue);
             Blob key=Crypto.generateDigest('MD5',Blob.valueOf(keyValue));
             String token=EncodingUtil.convertToHex(key);
             System.Debug(token);
              
            if (con.Company_Name__c != null)
                {company=con.Company_Name__c;}
            else 
                {company=con.Account.Name;}
            
            
            string params='clientid=101';
            params=params + '&username=' +EncodingUtil.urlEncode(user.UserName, 'UTF-8');
            params=params + '&key=' + token;
            params=params + '&RID=' + con.id;
            params=params + '&recFN=' + con.FirstName;
            params=params + '&recLN=' + con.LastName;
            params=params + '&recFirm=' + company;
            params=params + '&recAD1=' + con.MailingStreet;
            params=params + '&recCity=' + con.MailingCity;
            params=params + '&recState=' + con.MailingState;
            params=params + '&recZip=' + con.MailingPostalCode;
            params=params + '&recPhone=' + con.Phone;
            
            string result='https://secure.concordpromotions.com/single-sign.asp?' + params;

            //string result='http://stage-cole.concordms.com/single-sign.asp?' + params; //clientid=101&username='+EncodingUtil.urlEncode(user.email, 'UTF-8')+'&key=' + token;
            return result;
    }
    
        webservice static void processConcordOrder(string xml) 
        {
             System.Debug(xml);

         }
        
     }