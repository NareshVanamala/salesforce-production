@isTest
Public class CreateREITProxyVotes_Test
{
  static testmethod void testProxyVotes()
  {
      
       Account a = new Account();
       a.name = 'testaccount';
       insert a;
  
    
       Contact c1= new Contact();
       c1.accountid=a.id;
       c1.firstname='xxx';
       c1.lastname='yyy';
       insert c1;
       
       System.assertNotEquals( a.name, c1.firstname);

       list<REIT_Investment__c>reclist= new list<REIT_Investment__c>();
       REIT_Investment__c rcv= new REIT_Investment__c();
       rcv.Original_Capital__c=678989.00;
       rcv.Investor_Name__c='NINADTAMBE';
       rcv.Rep_Contact__c=c1.id;
       rcv.Fund__c='3376';
       rcv.Current_Units__c=798990;
       rcv.Account__c='7799400016';
       reclist.add(rcv);
       
       REIT_Investment__c rcv1= new REIT_Investment__c();
       rcv1.Original_Capital__c=678989.00;
       rcv1.Investor_Name__c='NINADTAMBE';
       rcv1.Rep_Contact__c=c1.id;
       rcv1.Fund__c='3772';
       rcv1.Current_Units__c=798990;
       rcv1.Account__c='7799400016';
       reclist.add(rcv1);
       insert reclist;
        
        CreateREITProxyVotes sch = new CreateREITProxyVotes ();
        Database.executebatch(sch);
       
   }
   
 }