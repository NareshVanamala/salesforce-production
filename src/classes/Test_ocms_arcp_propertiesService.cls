@isTest (SeeAllData=true)
private class Test_ocms_arcp_propertiesService {
    
    @isTest static void test_method_one() {
        ocms_propertiesService wps = new ocms_propertiesService();
        
        wps.gettype();

        Map<String,String> sendMap = new Map<String,String>();
        sendMap.put('action','getIndustryList');
        system.assert(sendMap!=Null);

        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getTenantList');
        wps.executeRequest(sendMap);

        sendMap.clear();
        

        
        sendMap.put('action','getStateList');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getCityList');
        sendMap.put('state','TX');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getResultTotal');
        sendMap.put('state','TX');
        sendMap.put('city','Houston');
        sendMap.put('tenantIndustry','Office');
        sendMap.put('tenant','**VACANT**');
        sendMap.put('filterRetail','true');
        sendMap.put('filterMultiTenant','true');
        sendMap.put('filterOffice','true');
        sendMap.put('filterIndustrial','true');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getPropertyList');
        sendMap.put('state','TX');
        sendMap.put('city','Houston');
        sendMap.put('tenantIndustry','Office');
        sendMap.put('tenant','**VACANT**');
        sendMap.put('filterRetail','true');
        sendMap.put('filterMultiTenant','true');
        sendMap.put('filterOffice','true');
        sendMap.put('filterIndustrial','true');
        wps.executeRequest(sendMap);



        wps.loadResponse();
    }
    
   
}