public with sharing class LC_ApprovalHistoryController
 {
    public String relatedRecordId {get;set;}
    public String str{get;set;}
    public set<id>Subcaseid;
    public list<ProcessInstanceStep >plist;
    public list<ProcessInstance>plist1;
   // Public list<Employee__c>quote;
    Public list<LC_LeaseOpprtunity__c>quoteasset;
    public set<id>ProcessCaseid;
    public map<id,String> subjectCaseMap;
    public map<id, list<ProcessInstanceHistory>> processinstancestepmap;
    public map<id,string>StepToComment; 
    public list<MyWrapper> wcampList;
    public String myValue;
  
    
    public List<MyWrapper> getwcampList () {
        
         ProcessCaseid= new set<id>();
         subjectCaseMap= new map<id,String>();
         processinstancestepmap= new map<id, list<ProcessInstanceHistory>>();
         StepToComment=new map<id,String>();
         plist= new list<ProcessInstanceStep>();
         plist1= new list<ProcessInstance>();
         wcampList= new list<MyWrapper>();
         wcampList= new list<MyWrapper>();
         system.debug('>>>>>>>>>>>>>> '+myValue);
         //system.debug('get the list size of cases'+quote.size()); 
 
         plist1=[SELECT ProcessDefinitionID,CreatedById,CreatedDate,Id,IsDeleted,LastModifiedById,LastModifiedDate,Status,SystemModstamp,TargetObjectId,TargetObject.Name,(SELECT SystemModstamp, StepStatus, ProcessInstanceId, OriginalActorId,OriginalActor.name, Id, CreatedDate, CreatedById, Comments, ActorId,Actor.name FROM StepsAndWorkitems ORDER BY CreatedDate DESC ) FROM ProcessInstance   where TargetObjectId =:relatedRecordId ORDER BY CreatedDate DESC];
        
        system.debug('Check the list of processInstance..'+plist1);  
          for(processinstance plist2:plist1)
        {
        for(ProcessInstanceHistory ri:plist2.StepsAndWorkitems)
        {  
          if(ri.stepstatus=='Started')
          StepToComment.put(ri.ProcessInstanceId,ri.Comments);
              
          if(processinstancestepmap.containsKey(ri.ProcessInstanceId))
                 processinstancestepmap.get(ri.ProcessInstanceId).add(ri);
           else
               processinstancestepmap.put(ri.ProcessInstanceId, new List<ProcessInstanceHistory>{ri});
             
        }
        }
        system.debug('Check the map...'+processinstancestepmap);  
         MyWrapper w;
       for(ProcessInstance acc:plist1)
        {
            w = new MyWrapper();
            w.wAccseesRequestName=subjectCaseMap.get(acc.TargetObjectId) ;
            w.wcaption=StepToComment.get(acc.id);
            w.wStatus= acc.Status;
           // w.wApprover= acc.OriginalActorId;
            //w.wActualApprover=acc.ActorId; 
            //w.wDate= acc.SystemModstamp;
            //w.wComments= acc.Comments;
            w.wConList=processinstancestepmap.get(acc.id);
            system.debug('check the conlist'+w.wConList);
            wcampList.add(w);
        }
         
        system.debug('check the wrapperlist..'+wcampList);
        return wcampList;
    
    
    }

     
  public class MyWrapper
  { 

    public String wAccseesRequestName{get;set;}
    public String wStatus{get;set;}
    public String wApprover{get;set;}
    public String wActualApprover{get;set;}
    public Date wDate{get;set;}
    public String wComments{get;set;}
    public string wcaption{get;set;} 
    public List<ProcessInstanceHistory> wConList{get;set;} 
    public MyWrapper()
    {
        this.wConList = new List<ProcessInstanceHistory>();
     }
   }
     
      
 }