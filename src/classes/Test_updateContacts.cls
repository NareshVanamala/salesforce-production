@isTest
public class Test_updateContacts
 {
    static testmethod void Eventupdate()
    {
       Database.QueryLocator QL;
       Database.BatchableContext BC;
        Database.QueryLocator QL1;
       Database.BatchableContext BC1;
       
       Id rrrecordtype = [select Id, Name,DeveloperName from RecordType where DeveloperName ='NonInvestorRepContact' and SobjectType = 'Contact'].id;
        Account a = new Account();
       a.name = 'Test';
       insert a;
       
        list<contact> clist = new list<contact>();
        list<contact> clist1 = new list<contact>();
        list<contact> clist2 = new list<contact>();
        list<contact> clist3 = new list<contact>();
        list<contact> clist4 = new list<contact>();
        list<contact> clist5 = new list<contact>();
       
       Contact c = new Contact();
       c.lastname = 'Cockpit';
       c.Accountid = a.id;
       c.Relationship__c ='Accounting';
       c.Contact_Type__c = 'Cole';
       c.Wholesaler_Management__c = 'Producer A';
       c.Territory__c ='Chicago';
       c.RIA_Territory__c ='Southeast Region';
       c.RIA_Consultant_Picklist__c ='Brian Mackin';
       c.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c.Wholesaler__c='Andrew Garten';
       c.Regional_Territory_Manager__c ='Andrew Garten';
       c.Internal_Wholesaler__c ='Aaron Williams';
       c.Territory_Zone__c = 'NV-50';
       c.recordtypeid = rrrecordtype;
       c.Priority__c='1';
       c.Next_Planned_External_Appointment__c=system.today()-12 ; 
       c.Next_External_Appointment__c = system.today()+30; 
       
       system.assertequals(c.Accountid,a.id);

       clist.add(c);
       //insert clist;       
       
             
       Contact c2 = new Contact();
       c2.lastname = 'Cockpit1234';
       c2.Accountid = a.id;
       c2.Relationship__c ='Accounting';
       c2.Contact_Type__c = 'Cole9';
       c2.Wholesaler_Management__c = 'Producer A';
       c2.Territory__c ='Chicago';
       c2.RIA_Territory__c ='Southeast Region';
       c2.RIA_Consultant_Picklist__c ='Brian Mackin';
       c2.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c2.Wholesaler__c='Andrew Garten';
       c2.Regional_Territory_Manager__c ='Andrew Garten';
       c2.Internal_Wholesaler__c ='Aaron Williams';
       c2.Territory_Zone__c = 'NV-50';
       c2.recordtypeid = rrrecordtype;
       c2.Priority__c='3';
        c2.Next_Planned_External_Appointment__c =system.today()-12 ; 
       c2.Next_External_Appointment__c = null;
       clist.add(c2);
       insert clist;
       
       system.assertequals(c2.Accountid,a.id);

        
        UpdateContacts e5= new UpdateContacts ();
       ID batchprocessid5 = Database.executeBatch(e5);
       QL = e5.start(bc);
       e5.execute(BC,clist);
       e5.finish(bc);
       
        
       
    }
 
 
 
 }