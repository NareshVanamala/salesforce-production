global class PortfolioBatchRedesign implements Database.Batchable<SObject>
{
    global Portfolio__c pf = new Portfolio__c ();
    List<Deal__c> pfd = new List<Deal__c>();
    List<Deal__c> pfd11 = new List<Deal__c>();
    Deal__c DealNamelist = new Deal__c();
    List<Deal__c> pfdForstatus = new List<Deal__c>();
    set<Id> portids = new Set<Id>();
    set<String>Cityset= new set<String>();
    //String City = null;
    
global Database.QueryLocator start(Database.BatchableContext BC)  
{
    String Query;
    Query = 'Select id from Portfolio__c';
    return Database.getQueryLocator(Query);
   
}

global void execute(Database.BatchableContext BC,List<sObject> scope)  
{
    Map<Id, List<Deal__c>> dealREITs = new Map<Id, List<Deal__c>>();
    List<Portfolio__c> portlist = new List<Portfolio__c>();
    Map<Id, Portfolio__c> portfolioMap = new Map<Id, Portfolio__c>();
    List<Deal__c> scopeDeals = new List<Deal__c>();
    Set<Id> dealIds = new Set<Id>();
    
     for(sObject a:scope)
      dealIds.add(a.Id);
      
      scopeDeals =  [Select RecordType.name,Property_Type__c,AcquisitionDepartment__c,Occupancy__c,RR_RemainingTerm__c,SLB_Rem_Term__c,Rent_PSF__c,CAP_Rate__c,Year_1_NOI__c,Price_PSF_Contract_Price__c,Total_SF__c,Contract_Price__c,Closing_Date__c, Hard_Date__c,Source__c,RR_Rent_Bump__c,LOI_Sent_Date__c,recordtypeid,Cole_Initials__c,Tenant_s__c,Id,Seller_s_Broker_Company__r.name,Seller_s_Broker__r.name,Property_Owner_Contact__r.name,Property_Owner_Company__r.name,Deal_Type__c,Property_Owner_Contact__c,Property_Owner_Company__c,Deal_Status__c,City__c, Name,State__c,Primary_Use__c,RR_SPRating__c,Date_Identified__c,Go_Hard_Date__c,Estimated_COE_Date__c,RR_Reimb_Type__c,Seller_s_Broker__c,Seller_s_Broker_Company__c,Portfolio_Deal__c from Deal__c where Portfolio_Deal__c in:dealIds];
      for(Deal__c rd : scopeDeals)
      {    
       if(dealREITs.containsKey(rd.Portfolio_Deal__c))
       {
         dealREITs.get(rd.Portfolio_Deal__c).add(rd);
       }
       else
       {
         dealREITs.put(rd.Portfolio_Deal__c, new List<Deal__c>{rd});
       } 
      }
      
      portlist = [select id,Rem_Term__c,Status__c,Hard_Date__c,Acquisition_Department__c,Team__c,Properties__c,Cole_Initials__c,Property_Owner_Company__c,Property_Owner_Contact__c,Deal_Type__c,Property_Type__c,SLB_Rem_Term__c,Rent_PSF__c,Cap_Rate__c,Year_1_NOI__c,Price_PSF__c,Occupancy__c,Total_SF__c,Contract_Price__c,Source__c,Major_Tenants__c,Seller_s_Broker_Company__c,Seller_s_Broker_Contact__c,Lease_Type__c, Rent_Bump__c,LOI_Sent_Date__c,Deal_Closing_Date__c, Est_COE_Date__c,City__c, State__c,S_P_Rating__c,Primary_Use__c,Date_Identified__c from Portfolio__c where Id IN :dealREITs.keyset()];
      System.debug('%%%%%%%%Portfoli size' + portlist.size());
      
      for(Portfolio__c  pfd : portlist)
      {
         portfolioMap.put(pfd.id,pfd);
      }
      
      for(Id rep : dealREITs.keyset())
      {
        //decimal currentcapitalthisyear= 0;
        Cityset= new set<String>();
        set<String>Stateset= new set<String>();
        set<String>RRSpratingset= new set<String>();
        set<String>PrimaryUseset= new set<String>();
        set<Date>Dateidentifiedset= new set<Date>();
        set<Date>goHarddateset= new set<Date>();
        set<Date>Harddateset= new set<Date>();
        set<Date>ESTCOEdateset= new set<Date>();
        list<Date>ESTCOEdateset11= new list<Date>();
    
        set<Date>Closingdateset=new set<Date>();    
        list<Date>Closindareset11=new list<Date>();    
        set<Date>LOISentdateset= new set<Date>();
        
        
        
        list<Date>Hardset11=new list<Date>();
       // set<Date>Harddateset= new set<Date>();
        
        //Harddateset.add(rct.REthinkAP__Hard_Date__c);
         Date HardDate1;   
    
        //set<string>rentbumpset= new set<String>();
        set<String>Rembtypeset= new set<String>();
        set<String>SellersBrokerContactset= new set<String>(); 
        set<String>SellersBrokerCompanyset = new set<String>(); 
        set<string>rrrentbumpSet= new set<String>();
        set<String>majortenentsSet= new set<String>();
        set<String>SourceSet= new set<String>();
        List<Integer> Occupancyavglist = new List<Integer>();
        String City=null;
        String State=null;
        String RRSPrating=null;
        String PrimaryUse=null;
        String Dateidentified=null;
        String goHarddate=null;
        String  Harddate=null;
        String ESTCOEdate=null;
        Date ESTCOEdate1;
        Date ClosingDate;   
        String ClosingDatestring=null; 
        List<String> slbRemtermavglist = new List<String>();   
        //String slbRemterm=null;
        decimal slbRemterm = 0;
        decimal slbRemterm1=0;  
        decimal slbRemtermavg = 0;  
        string LOISEntdate=null;
        String Rembtype=null;
        string rentbump=null;
        String SellersBrokerContact=null;
        String SellersBrokerCompany=null;
        String Majortenent=null;
        String sourcevalue=null;
        Integer totalsf=0;
        List<Integer> caprateavglist = new List<Integer>();
        String Caprate1=null;
        decimal caprate=0.00;
        decimal caprateavg = 0;
        List<Integer> rentpsfavglist = new List<Integer>();
        String rentpsf1=null;
        decimal rentpsf=0.00;
        decimal rentpsfavg = 0;
        List<Integer> pricepsfavglist = new List<Integer>();
        String pricepsf1=null;
        decimal pricepsf=0.00;
        decimal pricepsfavg = 0;
        String Occupacy1=null;
        Decimal Occupacy =0;
        Integer Occupacyavg = 0;
        decimal Contractprice=0;
        decimal noi=0;
        decimal term=0;
        String finalTerm=null;
        decimal finalterm1=0;
        Integer count=0;
        String propertytype = null;
        String DealType = null;
        String PropertyOwnerContact= null;
        String PropertyOwnerCompany= null;
        String ColeContact= null;
        String Acqdept =null;
        String Properties= null;
        String Team= null;
        String Status= null;
        List<String> dealrecords = new List<String>();
        List<Id> dealrecordIds = new List<Id>();
        String recordtypeteam = null;
        Integer Reviewingcount=0;
        Integer Marketed=0; 
        Integer LOISent=0;
        Integer SignedLOI=0; 
        Integer EscrowPending=0;
        Integer EscrowStudy=0;
        Integer EscrowHard=0;
        Integer ClosedOwned=0;
        Integer closedsold=0;
        Integer OnHold=0;
        Integer Lost=0;
        Integer Dead=0;
        Integer DeadLegal=0;
        Integer UnderContractSignedPSA=0;
        list<Deal__c> deallist= new list<Deal__c>();
            
      if(portfolioMap.containsKey(rep))
        {
         deallist=dealREITs.get(rep);
         deallist.sort();
         for(Deal__c rct : deallist ) 
            {
               system.debug('check the deal'+rct);
                if(rct.City__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    Cityset.add(rct.City__c);
                    City=rct.City__c;
                }     
                System.debug('********CitySet' + Cityset.size() );
                System.debug('********testfirst' + City);
                if(rct.State__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    Stateset.add(rct.State__c);
                    State=rct.State__c;
                }
                 if(rct.RR_SPRating__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                     RRSpratingset.add(rct.RR_SPRating__c);
                     RRSPrating=rct.RR_SPRating__c;
                }
                if(rct.Primary_Use__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    PrimaryUseset.add(rct.Primary_Use__c);
                    PrimaryUse=rct.Primary_Use__c;     
                }
                if(rct.Date_Identified__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    Dateidentifiedset.add(rct.Date_Identified__c);
                    date d=rct.Date_Identified__c;
                    integer month1= d.month();
                    Integer day1= d.day();
                    Integer year1= d.year();
                    Dateidentified=string.valueOf(month1)+'/'+String.valueOf(day1)+'/'+String.valueOf(year1); 
                }
                if(rct.Go_Hard_Date__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    goHarddateset.add(rct.Go_Hard_Date__c);
                    date d=rct.Go_Hard_Date__c;
                    integer month1= d.month();
                    Integer day1= d.day();
                    Integer year1= d.year();
                    goHarddate=string.valueOf(month1)+'/'+String.valueOf(day1)+'/'+String.valueOf(year1);     
                }
                if(rct.Estimated_COE_Date__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    ESTCOEdateset11.add(rct.Estimated_COE_Date__c);
                    ESTCOEdateset.add(rct.Estimated_COE_Date__c);
                    ESTCOEdate1=rct.Estimated_COE_Date__c;                            
                }
                if(rct.Closing_Date__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    Closindareset11.add(rct.Closing_Date__c);
                    Closingdateset.add(rct.Closing_Date__c);
                    ClosingDate=rct.Closing_Date__c;                          
                }
                if(rct.Hard_Date__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    Hardset11.add(rct.Hard_Date__c);
                    Harddateset.add(rct.Hard_Date__c);
                    HardDate1=rct.Hard_Date__c;                          
                }
                
              
                if(rct.LOI_Sent_Date__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    LOISentdateset.add(rct.LOI_Sent_Date__c);
                    date d=rct.LOI_Sent_Date__c;
                    integer month1= d.month();
                    Integer day1= d.day();
                    Integer year1= d.year();
                    LOISEntdate =string.valueOf(month1)+'/'+String.valueOf(day1)+'/'+String.valueOf(year1);                         
                }
                if(rct.RR_Reimb_Type__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    Rembtypeset.add(rct.RR_Reimb_Type__c);
                    Rembtype=rct.RR_Reimb_Type__c;
                }
                if(rct.RR_Rent_Bump__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    rrrentbumpSet.add(rct.RR_Rent_Bump__c);
                    rentbump =rct.RR_Rent_Bump__c;
                }                                                                   
                if(rct.Seller_s_Broker__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    SellersBrokerContactset.add(rct.Seller_s_Broker__r.name);
                    SellersBrokerContact=rct.Seller_s_Broker__r.name;
                }   
                if(rct.Seller_s_Broker_Company__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    SellersBrokerCompanyset.add(rct.Seller_s_Broker_Company__r.name);
                    SellersBrokerCompany=rct.Seller_s_Broker_Company__r.name;
                }  
                if(rct.Tenant_s__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    majortenentsSet.add(rct.Tenant_s__c);
                    Majortenent=rct.Tenant_s__c;
                }  
                if(rct.Source__c!=null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    SourceSet.add(rct.Source__c); 
                    sourcevalue= rct.Source__c;  
                } 
                if(rct.Contract_Price__c != null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    Contractprice += rct.Contract_Price__c;
                    System.debug('$$$$$$' + Contractprice);
                }
                if(rct.Year_1_NOI__c != null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    noi += rct.Year_1_NOI__c;
                    System.debug('$$$$$$' + noi);
                }
                if(rct.Total_SF__c!= null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    totalsf += Integer.valueof(rct.Total_SF__c);
                    System.debug('$$$$$$' + totalsf);
                }
                if(rct.Occupancy__c!= null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    Occupancyavglist.add(Integer.valueof(rct.Occupancy__c));
                    Occupacy += Integer.valueof(rct.Occupancy__c);
                    Occupacyavg  = Integer.valueof(Occupacy / Occupancyavglist.size());
                    System.debug('$$$$$$' + Occupacyavg);
                }
                if(rct.Price_PSF_Contract_Price__c!= null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    pricepsfavglist.add(Integer.valueof(rct.Price_PSF_Contract_Price__c));
                    pricepsf += rct.Price_PSF_Contract_Price__c;
                    pricepsfavg = pricepsf / pricepsfavglist.size();
                    System.debug('$$$$$$' + pricepsfavg);
                }
                if(rct.CAP_Rate__c!= null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    caprateavglist.add(Integer.valueof(rct.CAP_Rate__c));
                    caprate += rct.CAP_Rate__c;
                    caprateavg = caprate / caprateavglist.size();
                    System.debug('$$$$$$' + caprateavg);
                }
                if(rct.Rent_PSF__c!= null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    rentpsfavglist.add(Integer.valueof(rct.Rent_PSF__c));
                    rentpsf += rct.Rent_PSF__c;
                    rentpsfavg = rentpsf / rentpsfavglist.size();
                    System.debug('$$$$$$' + rentpsf);
                }
                if(rct.SLB_Rem_Term__c!= null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    slbRemtermavglist.add(String.valueof(rct.SLB_Rem_Term__c));
                    slbRemterm += rct.SLB_Rem_Term__c;
                    slbRemtermavg = slbRemterm / slbRemtermavglist.size();
                    System.debug('$$$$$$' + slbRemterm);
                }
                if((rct.RR_RemainingTerm__c!=null)&&(rct.RR_RemainingTerm__c!='Varies') && (rct.Deal_Status__c!='Dead') && (rct.Deal_Status__c!='Dead-Legal'))
                {
                     count= count+1;
                     String rrreamainingterm1=rct.RR_RemainingTerm__c;
                     decimal rrreamainingterm= decimal.valueOf(rrreamainingterm1);
                     term= term+ rrreamainingterm;
                }
                if(rct.Property_Type__c != null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    propertytype = rct.Property_Type__c;
                    System.debug('$$$$$$' + propertytype);
                }
                if(rct.Deal_Type__c != null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    DealType = rct.Deal_Type__c;
                    System.debug('$$$$$$' + DealType);
                }
                if(rct.Property_Owner_Contact__r.name != null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    PropertyOwnerContact = rct.Property_Owner_Contact__r.name;
                    System.debug('$$$$$$' + PropertyOwnerContact);
                }
                if(rct.Property_Owner_Company__r.name != null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    PropertyOwnerCompany = rct.Property_Owner_Company__r.name;
                    System.debug('$$$$$$' + PropertyOwnerCompany);
                }
                if(rct.Cole_Initials__c != null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    ColeContact = rct.Cole_Initials__c;
                    System.debug('$$$$$$' + ColeContact);
                }
                if(rct.AcquisitionDepartment__c != null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                    acqdept= rct.AcquisitionDepartment__c;
                    System.debug('$$$$$$' + acqdept);
                }
                if(rct.id != null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                   dealrecords.add(String.valueOf(rct.id));
                   dealrecordIds.add(rct.id);
                   System.debug('^^^^Properties size' + dealrecords.size());
                }
               if(rct.Deal_Status__c!= null)
                {
                   if(rct.Deal_Status__c=='Reviewing')
                       Reviewingcount = Reviewingcount+1;
                   if(rct.Deal_Status__c=='Marketed')
                       Marketed= Marketed+1;     
                    if(rct.Deal_Status__c=='LOI Sent')
                       LOISent= LOISent+1;      
                     if(rct.Deal_Status__c=='Signed LOI')
                       SignedLOI= SignedLOI+1; 
                    
                     if(rct.Deal_Status__c=='Under Contract/ Signed PSA')
                       UnderContractSignedPSA= UnderContractSignedPSA+1;    
                    
                     if(rct.Deal_Status__c=='Escrow Pending')
                       EscrowPending= EscrowPending+1; 
                    
                     if(rct.Deal_Status__c=='Escrow Study')
                       EscrowStudy= EscrowStudy+1; 
                    
                      if(rct.Deal_Status__c=='Escrow Hard')
                       EscrowHard= EscrowHard+1;  
                      
                       if(rct.Deal_Status__c=='Closed/Owned')
                       ClosedOwned= ClosedOwned+1;  
                      
                      
                        if(rct.Deal_Status__c=='Closed/Sold')
                       closedsold= closedsold+1;          
                      
                       if(rct.Deal_Status__c=='On Hold')
                       OnHold= OnHold+1; 
                      
                        if(rct.Deal_Status__c=='Lost')
                       Lost= Lost+1; 
                      
                        if(rct.Deal_Status__c=='Dead')
                       Dead= Dead+1; 
                      
                       if(rct.Deal_Status__c=='Dead-Legal')
                       DeadLegal= DeadLegal+1; 
                       
                       
                   //Status = rct.REthinkAP__Deal_Status__c;
                }
               // if(rct.REthinkAP__Hard_Date__c != null)
               // {
                //   Harddate = String.valueOf(rct.REthinkAP__Hard_Date__c);
                //}
                if(rct.RecordType.name != null && rct.Deal_Status__c!='Dead' && rct.Deal_Status__c!='Dead-Legal')
                {
                  System.debug(')))))RecordType' + rct.RecordType.name);
                  recordtypeteam = rct.RecordType.name;
                  
                }
               
                
          }
          //**********Assign the city value**********
            if(Cityset.size()== 1)
            {
                //pf.Dl_City__c = City;
                portfolioMap.get(rep).City__c = City;     
                System.debug('********1' + portfolioMap.get(rep).City__c);
            }
            
            else if(Cityset.size()>1)
            {
               portfolioMap.get(rep).City__c = 'Varies';
               System.debug('********2' + portfolioMap.get(rep).City__c);
            }
            
            else if(Cityset.size()==0)
            {
               portfolioMap.get(rep).City__c = null;
               System.debug('********2' + portfolioMap.get(rep).City__c);
            }
             //*********assign the value to state***********
            if(Stateset.size()== 1)
            {
                portfolioMap.get(rep).State__c= State;
            }
            else if(Stateset.size()>1)
            {
                portfolioMap.get(rep).State__c= 'Varies';
            }
            else If(Stateset.size()==0)
            {
                portfolioMap.get(rep).State__c= null;
            }
             //*********assign the value to Rrsprating ***********
            if(RRSpratingset.size()== 1)
            {
                portfolioMap.get(rep).S_P_Rating__c= RRSPrating;
            }
            else If(RRSpratingset.size()> 1)
            {
                portfolioMap.get(rep).S_P_Rating__c = 'Varies';
            }
            else If(RRSpratingset.size()==0)
            {
                portfolioMap.get(rep).S_P_Rating__c = null;
            }
            //***********assign the value to Primaryuserset***********
            if(PrimaryUseset.size()== 1)
            {
                portfolioMap.get(rep).Primary_Use__c = PrimaryUse;
            }
            else If(PrimaryUseset.size()> 1)
            {
                portfolioMap.get(rep).Primary_Use__c = 'Varies';
            }
            else if(PrimaryUseset.size()==0)
            {
                portfolioMap.get(rep).Primary_Use__c = null;
            }
            // **********assign the value to Dateidentified*********** 
            if(Dateidentifiedset.size()== 1)
            {
                portfolioMap.get(rep).Date_Identified__c = Dateidentified;
            }
            else  If(Dateidentifiedset.size()> 1)
            {
                portfolioMap.get(rep).Date_Identified__c = 'Varies';
            }
            else if(Dateidentifiedset.size()==0)
            {
                portfolioMap.get(rep).Date_Identified__c = null;
            }
             //********************Est coe date***********
            //ESTCOEdateset11.addAll(ESTCOEdateset);
            //system.debug('Check the first record of the list...'+ESTCOEdateset11[0]);
            if(ESTCOEdateset.size()== 1)
            {
                portfolioMap.get(rep).Est_COE_Date__c=string.valueOf(ESTCOEdate1);
            }
            else if(ESTCOEdateset.size()> 1)
            {
                portfolioMap.get(rep).Est_COE_Date__c = String.valueOf(ESTCOEdateset11[0]);
            }
            else if(ESTCOEdateset.size()==0)
            {
                portfolioMap.get(rep).Est_COE_Date__c = null ;
            }
            //*********************Closing date*******************//                    
            if(Closingdateset.size()==1)
            {
                portfolioMap.get(rep).Closing_Date__c= string.valueOf(ClosingDate);
            }                       
            else if(Closingdateset.size()>1)
            {
                portfolioMap.get(rep).Closing_Date__c=string.valueof(Closindareset11[0]);
            }
            else if(Closingdateset.size()==0)
            {
                portfolioMap.get(rep).Closing_Date__c=null;
            }
           //*******************hard date********************// 
            if(Harddateset.size()==1)
            {
                portfolioMap.get(rep).Hard_Date__c= date.valueOf(HardDate1);
            }                       
            else if(Harddateset.size()>1)
            {
                portfolioMap.get(rep).Hard_Date__c=date.valueof(Hardset11[0]);
            }
            else if(Harddateset.size()==0)
            {
                portfolioMap.get(rep).Hard_Date__c=null;
            }
                 //****************hard date *****************//
            //********LOT sent date********* 
            if(LOISentdateset.size()== 1)
            {
                portfolioMap.get(rep).LOI_Sent_Date__c = LOISEntdate;
            }
            else if(LOISentdateset.size()> 1)
            {
                portfolioMap.get(rep).LOI_Sent_Date__c = 'Varies';
            }
            else if(LOISentdateset.size()==0)
            {
                portfolioMap.get(rep).LOI_Sent_Date__c = null ;
            }
            //********RRREnt Bump*********   
            if(rrrentbumpSet.size()== 1)
            {
                portfolioMap.get(rep).Rent_Bump__c =rentbump ;
            }
            else If(rrrentbumpSet.size()>1)
            {
                portfolioMap.get(rep).Rent_Bump__c = 'Varies';
            }
            else If(rrrentbumpSet.size()==0)
            {
                portfolioMap.get(rep).Rent_Bump__c = null;
            }
            //*****************Remptype********** 
            if(Rembtypeset.size()== 1)
            {
                portfolioMap.get(rep).Lease_Type__c = Rembtype;
            }                       
            else if(Rembtypeset.size()>1)
            {
                portfolioMap.get(rep).Lease_Type__c = 'Varies';
            }
            else if(Rembtypeset.size()==0)
            {
                portfolioMap.get(rep).Lease_Type__c =null ;
            }
            //***************Sellerbrokercontact***** 
            if(SellersBrokerContactset.size()== 1)
            {
                portfolioMap.get(rep).Seller_s_Broker_Contact__c = SellersBrokerContact;
            }
            else if(SellersBrokerContactset.size()> 1)
            {
                portfolioMap.get(rep).Seller_s_Broker_Contact__c = 'Varies';
            }
            else if(SellersBrokerContactset.size()==0)
            {
                portfolioMap.get(rep).Seller_s_Broker_Contact__c =null ;
            }
            //**************sellerbroker**************
            if(SellersBrokerCompanyset.size()== 1)
            {
                portfolioMap.get(rep).Seller_s_Broker_Company__c = SellersBrokerCompany;
            }
            else if(SellersBrokerCompanyset.size()> 1)
            {
                portfolioMap.get(rep).Seller_s_Broker_Company__c = 'Varies';
            }
            else if(SellersBrokerCompanyset.size()==0)
            {
                portfolioMap.get(rep).Seller_s_Broker_Company__c = null ;
            }
            //***********calculating the major tenants***************     
            if(majortenentsSet.size()== 1)
            {
                portfolioMap.get(rep).Major_Tenants__c = Majortenent;
            }
            else if(majortenentsSet.size()> 1)
            {
                portfolioMap.get(rep).Major_Tenants__c = 'Varies';
            }
            else if(majortenentsSet.size()==0)
            {
                portfolioMap.get(rep).Major_Tenants__c = null;
            }                    
            //get the source value******************  
            if(SourceSet.size()== 1)
            {
                portfolioMap.get(rep).Source__c= sourcevalue;
            }
            else if(SourceSet.size()> 1)
            {
                portfolioMap.get(rep).Source__c= 'Varies';
            }
            else if(SourceSet.size()==0)
            {
                portfolioMap.get(rep).Source__c =null;
            }
            //Calculating Contract price**********
             if(Contractprice != null)
            {
               // Contractprice=Integer.valueof(groupedResults[0].get('CPRICE'));
               Contractprice = Contractprice.setscale(2);
                portfolioMap.get(rep).Contract_Price__c=Contractprice;
                //System.debug('$$$$$$' + Contractprice);
            }
            if(totalsf != null)
            {                
                portfolioMap.get(rep).Total_SF__c = totalsf;
            }
            if(noi != null)
            {               
            noi = noi.setscale(2); 
                portfolioMap.get(rep).Year_1_NOI__c = noi;
            }
            if(Occupacy != null)
            {
               Occupacy1 = string.valueOf(Occupacyavg);
               Occupacy =  decimal.valueOf(Occupacy1);
               portfolioMap.get(rep).Occupancy__c = String.valueOf(Occupacy)+'%';
            }
            if(pricepsf != null)
            {
               pricepsf1 = string.valueOf(pricepsfavg);
               pricepsf =  decimal.valueOf(pricepsf1);
               pricepsf = pricepsf.setscale(2);
               portfolioMap.get(rep).Price_PSF__c = '$' + String.valueOf(pricepsf);
            }
            if(caprate != null)
            {
               caprate1 = string.valueOf(caprateavg);
               caprate =  decimal.valueOf(caprate1);
               caprate = caprate.setscale(2);
               portfolioMap.get(rep).Cap_Rate__c = String.valueOf(caprate)+'%';
            }
            if(rentpsf != null)
            {
               rentpsf1 = string.valueOf(rentpsfavg);
               rentpsf =  decimal.valueOf(rentpsf1);
               rentpsf = rentpsf.setscale(2);
               portfolioMap.get(rep).Rent_PSF__c = '$' + String.valueOf(rentpsf);
            }
            if(slbRemterm != null)
            {
               slbRemterm1= slbRemtermavg;
               slbRemterm =  slbRemterm1;
               slbRemterm = slbremterm.setscale(2);
               portfolioMap.get(rep).SLB_Rem_Term__c = String.valueOf(slbRemterm);
            }
             if(term>0)
             {
                 finalterm1=(term /count);
                 finalterm1= finalterm1.setscale(2);
                 finalTerm=String.valueOf(finalterm1);
                 portfolioMap.get(rep).Rem_Term__c = finalTerm;
             } 
             if(propertytype != null)
             {
                portfolioMap.get(rep).Property_Type__c = propertytype;
             }
             if(DealType != null)
             {
                portfolioMap.get(rep).Deal_Type__c= DealType;
             }
             if(PropertyOwnerContact != null)
             {
                portfolioMap.get(rep).Property_Owner_Contact__c = PropertyOwnerContact;
             }
             if(PropertyOwnerCompany != null)
             {
                portfolioMap.get(rep).Property_Owner_Company__c = PropertyOwnerCompany;
             }
             if(ColeContact != null)
             {
                portfolioMap.get(rep).Cole_Initials__c = ColeContact;
             }
              if(Acqdept!= null)
             {
                portfolioMap.get(rep).Acquisition_Department__c= Acqdept;
             }
             //portfolioMap.get(rep).Properties__c= string.valueof(dealrecords.size());
             portfolioMap.get(rep).Properties__c= string.valueof(dealrecordIds.size());
             System.debug('*******2222' + portfolioMap.get(rep).Properties__c);
         //Updating status of portfolio//    
              if(Reviewingcount>0)
              portfolioMap.get(rep).Status__c ='Reviewing';
            else if(Marketed>0)
               portfolioMap.get(rep).Status__c ='Marketed';
            else if(LOISent>0)
               portfolioMap.get(rep).Status__c ='LOI Sent';
            else if(SignedLOI>0)
               portfolioMap.get(rep).Status__c ='Signed LOI';               
            else if(UnderContractSignedPSA>0)
               portfolioMap.get(rep).Status__c ='Under Contract/ Signed PSA';
             else if(EscrowPending>0)
               portfolioMap.get(rep).Status__c ='Escrow Pending';
            else if(EscrowStudy>0)
               portfolioMap.get(rep).Status__c ='Escrow Study';   
            else if(EscrowHard>0)
               portfolioMap.get(rep).Status__c ='Escrow Hard';
            else if(ClosedOwned>0)
               portfolioMap.get(rep).Status__c ='Closed/Owned';
            else if(closedsold>0)
               portfolioMap.get(rep).Status__c ='Closed/Sold';
            else if(OnHold>0)
               portfolioMap.get(rep).Status__c ='On Hold';
            else if(Lost>0)
               portfolioMap.get(rep).Status__c ='Lost';
            else if(Dead>0)
               portfolioMap.get(rep).Status__c ='Dead';                
              else if(DeadLegal>0)
               portfolioMap.get(rep).Status__c ='Dead-Legal';    
            /* if(Harddate != null)
             {
                portfolioMap.get(rep).Dl_Hard_date__c= Date.ValueOf(Harddate);
             }*/
             if(goHarddate != null)
             {
                portfolioMap.get(rep).Go_Hard_Date__c = goHarddate;
             }
             if(recordtypeteam != null)
             {
                portfolioMap.get(rep).Team__c = recordtypeteam;
             }
     
     }
     
     deallist.clear();
      if(portfolioMap.get(rep).Status__c =='Dead' || portfolioMap.get(rep).Status__c =='Dead-Legal')
    {

portfolioMap.get(rep).City__c = null;
portfolioMap.get(rep).State__c= null;
portfolioMap.get(rep).S_P_Rating__c = null;
portfolioMap.get(rep).Primary_Use__c= null;
portfolioMap.get(rep).Date_Identified__c = null;
 portfolioMap.get(rep).Est_COE_Date__c=null;
portfolioMap.get(rep).Closing_Date__c=null;
portfolioMap.get(rep).Hard_date__c=null;
portfolioMap.get(rep).LOI_Sent_Date__c =null ;
portfolioMap.get(rep).Rent_Bump__c = null;
portfolioMap.get(rep).Lease_Type__c =null ;
portfolioMap.get(rep).Seller_s_Broker_Contact__c =null ;
portfolioMap.get(rep).Seller_s_Broker_Company__c =null ;
portfolioMap.get(rep).Major_Tenants__c =null;
portfolioMap.get(rep).Source__c =null;
portfolioMap.get(rep).Contract_Price__c=null;
portfolioMap.get(rep).Total_SF__c =null;
 portfolioMap.get(rep).Year_1_NOI__c=null;
portfolioMap.get(rep).Occupancy__c= null;
portfolioMap.get(rep).Price_PSF__c= null;
 portfolioMap.get(rep).Cap_Rate__c= null;
portfolioMap.get(rep).Rent_PSF__c = null;
portfolioMap.get(rep).SLB_Rem_Term__c = null;
portfolioMap.get(rep).Rem_Term__c= null;
 portfolioMap.get(rep).Property_Type__c = null;
portfolioMap.get(rep).Deal_Type__c = null;
portfolioMap.get(rep).Property_Owner_Contact__c = null;
portfolioMap.get(rep).Property_Owner_Company__c = null;
portfolioMap.get(rep).Cole_Initials__c = null;
portfolioMap.get(rep).Properties__c= null;
 portfolioMap.get(rep).Go_Hard_Date__c = null;
 portfolioMap.get(rep).Team__c = null;

    }
    }
    
  update portfolioMap.values();
    
}

global void finish(Database.BatchableContext BC) {
}


}