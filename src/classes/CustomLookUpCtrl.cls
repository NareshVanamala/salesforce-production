public with sharing class CustomLookUpCtrl{
   public string TextId{get;set;}
   public string FieldAPI{get;set;}
   public string ObjectAPI{get;set;}
   public boolean picklist{get;set;}
   public List<dropdownToWrapper> dropdownToWrapper{get;set;}
   public CustomLookUpCtrl(){
      dropdownToWrapper = new List<dropdownToWrapper>();
      FieldAPI ='';
      ObjectAPI ='';
      dropdownToWrapper.clear();
      fieldPicklistValues();
   }

   public pageReference fieldPicklistValues(){
   FieldAPI ='';
      ObjectAPI ='';
       String FieldAPIName ='';
       String ObjectAPIName ='';
       FieldAPIName = apexpages.currentpage().getparameters().get('FieldAPIName');
       ObjectAPIName = apexpages.currentpage().getparameters().get('ObjectAPIName');
       system.debug('FieldAPIName'+FieldAPIName);
       system.debug('ObjectAPIName'+ObjectAPIName);
       picklist=false;
       if(FieldAPIName != null && ObjectAPIName != null){
          system.debug('TextId'+TextId);
          dropdownToWrapper.clear();
         // List<string> plValues = new List<string>();
          Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
          Map<String, Schema.SObjectField> fieldMap = schemaMap.get(ObjectAPIName).getDescribe().fields.getMap();        
          for(Schema.SObjectField sfield : fieldMap.Values()){
              schema.describefieldresult dfield = sfield.getDescribe();
              schema.Displaytype disfield= dfield.getType();
                          
              string selectedFieldName = string.valueOf(dfield.getSobjectField());
              system.debug('selectedFieldName'+selectedFieldName);
              if(selectedFieldName == FieldAPIName){
                 system.debug('Inner dfield.getType()'+dfield.getType());
                 if(string.valueOf(dfield.getType()) == 'BOOLEAN'){
                  //  plValues.add('True');
                   // plValues.add('False');
                    dropdownToWrapper.add(new dropdownToWrapper('True'));
                    dropdownToWrapper.add(new dropdownToWrapper('False'));
                 }
                 else{
                    picklist=true;
                    for(Schema.PicklistEntry ple : dfield.getPicklistValues()) {
                     //   plValues.add(ple.getLabel());
                        dropdownToWrapper.add(new dropdownToWrapper(ple.getLabel()));
                    }
                }
                 break;
              } 
         }
         //system.debug('plValues'+ plValues);     
       }
       return null;
   }
   public class dropdownToWrapper{
       public string dropdownValue {get;set;}
       public string records{get;set;}
       public dropdownToWrapper(string dropdownValue){
           this.dropdownValue = dropdownValue;
           this.records = dropdownValue;
      }
   }
}