public with sharing class ERHomeClass {

    public String sMail {get; set; }
    public List<Event_Automation_Request__c> earList{get;set;}
    public List<CampaignMember> campList{get;set;}
    public List<CampaignMember> campPendList{get;set;}
    public List<CampaignMember> campApprList{get;set;}
    public List<CampaignMember> campDeclList{get;set;}
    public Boolean log{get;set;}
    public Boolean review{get;set;}
    
    public PageReference loginCheck() {
      system.debug('check the smail..'+sMail);      
        if(sMail!='')  
        {
           PageReference reference=new PageReference('/apex/ERApprovalPage?emid='+sMail);
           reference.setRedirect(true);  
           return reference;           
        }
        else
        {            
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Please enter your mail id to login.');
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
}