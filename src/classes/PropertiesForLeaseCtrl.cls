global virtual with sharing class PropertiesForLeaseCtrl{

    private List<SObject> propertyList;
    private String html;
    public string webready='Approved';
    public string multitenant='%Multi-Tenant%';
    public string Anchored='%Anchored%';
    public Map<String, String> stateMap = new Map<String, String>(); 
    
  
    global PropertiesForLeaseCtrl() {
        Map<String, Website_States__c> websiteStatesCustSetting = Website_States__c.getAll();
        List<String> stateshrtname = new List<String>();
        stateshrtname.addAll(websiteStatesCustSetting.keySet());
        stateshrtname.sort();
        for(string stateshort:stateshrtname){
            Website_States__c websiteStates = websiteStatesCustSetting.get(stateshort);
            stateMap.put(websiteStates.State__c,websiteStates.State_Full_Name__c);
        }
   
    } 
  
  public Integer getResultTotal(String state, String city,String[] filters,String[] sqftfilters){
        
       
        String query;
                       
      //query = 'SELECT COUNT() FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Property_type__c LIKE \'' + multitenant + '\' and Status__c in (\'Owned\',\'Managed\')';
        query = 'SELECT COUNT() FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Property_Type_Group__c LIKE \'' + Anchored+ '\' and Status__c in (\'Owned\',\'Managed\')';
        
        
        if(state != null && state != '' && state != 'all'){
            query += ' AND State__c = \'' + state + '\'';
        }

        if(city != null && city != '' && state != 'all' && city != 'all'){
            query += ' AND City__c = \'' + city + '\'';
        }
        if(filters != null && filters.size() > 0){
            query += ' AND ( ';
            Boolean first = true;
            for(String f: filters){
                system.debug('selected filters'+f);
                
                if(first){
                    first=false;
                }else{
                    query += ' OR ';
                }
                if(f=='Multi-Tenant Anchored,Multi-Tenant Grocery'){
                    list<string> selectedString = f.split(',');
                    query += '(Sector__C in ( ';
                    query +='\'' + selectedString[0] + '\'';
                    query +=',';
                    query +='\'' + selectedString[1] + '\'';
                     query += ' ))';
                    
                }else{
                     query += '(Sector__C= \'' + f + '\')';
                }
            }
               query += ' )';
        }
         if(sqftfilters!= null && sqftfilters.size() > 0){
         string leasestatus = 'Immediately Available';
         query += 'AND Lease_Status__c =: leasestatus';
            query += ' AND ( ';
            Boolean first = true;
            for(String f1: sqftfilters){
                if(first){
                    first=false;
                }else{
                    query += ' OR ';
                }
        Integer Sqft;
        Sqft= integer.valueof(f1);
        if(f1=='1'){
            query += '(SqFt__c >=1 and SqFt__c <=2500)';
        }else if(f1=='2501'){
             query += '(SqFt__c >=2501 and SqFt__c <=5000)';
        }else if(f1=='5001'){
             query += '(SqFt__c >=5001 and SqFt__c <=10000)';
        }else if(f1=='10001'){
             query += '(SqFt__c >=10001 and SqFt__c <=15000)';
        }else if(f1=='15001'){
             query += '(SqFt__c >=15001 and SqFt__c <=25000)';
        }else if(f1=='25001'){
             query += '(SqFt__c >=25001 and SqFt__c <=100000)';
        }
           }
               query += ' )';
        }
        System.debug('dynamic'+query);
        return Database.countQuery(query);

    } 
   
   
   public List<SObject> getPropertyList(String state, String city, String sortBy, String rLimit, String offset,String[] filters,String[] sqftfilters){

        List<SObject> propertyList;
        String query;    
        query = 'SELECT Id,Name,Common_Name__c, Web_Name__c,Address__c,Lease_Status__c, City__c, State__c,Zip_Code__c,Status__c, Gross_Leaseable_Area__c, Property_Type__c, Date_Acquired__c, Location__c, property_id__c, Main_Image_Formula__c, SqFt__c FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\'  and Property_Type_Group__c LIKE \'' + Anchored + '\' and Status__c in (\'Owned\',\'Managed\')';
        
        
        if(state != null && state != '' && state != 'all'){
            query += ' AND State__c = \'' + state + '\'';
        }

        if(city != null && city != '' && state != 'all' && city != 'all'){
            query += ' AND City__c = \'' + city + '\'';
        }
          if(filters != null && filters.size() > 0){
            query += ' AND ( ';
            Boolean first = true;
            for(String f: filters){
                system.debug('selected filters'+f);
                
                if(first){
                    first=false;
                }else{
                    query += ' OR ';
                }
                if(f=='Multi-Tenant Anchored,Multi-Tenant Grocery'){
                    list<string> selectedString = f.split(',');
                    query += '(Sector__C in ( ';
                    query +='\'' + selectedString[0] + '\'';
                    query +=',';
                    query +='\'' + selectedString[1] + '\'';
                    query += ' ))';
                    
                }else{
                     query += '(Sector__C= \'' + f + '\')';
                }
            }
               query += ' )';
        }
          if(sqftfilters!= null && sqftfilters.size() > 0){
          string leasestatus = 'Immediately Available';
          query += 'AND Lease_Status__c =: leasestatus';
            query += ' AND ( ';
            Boolean first = true;
            for(String f1: sqftfilters){
                if(first){
                    first=false;
                }else{
                    query += ' OR ';
                }
        Integer Sqft;
        Sqft= integer.valueof(f1);
        if(f1=='1'){
            query += '(SqFt__c >=1 and SqFt__c <=2500)';
        }else if(f1=='2501'){
             query += '(SqFt__c >=2501 and SqFt__c <=5000)';
        }else if(f1=='5001'){
             query += '(SqFt__c >=5001 and SqFt__c <=10000)';
        }else if(f1=='10001'){
             query += '(SqFt__c >=10001 and SqFt__c <=15000)';
        }else if(f1=='15001'){
             query += '(SqFt__c >=15001 and SqFt__c <=25000)';
        }else if(f1=='25001'){
             query += '(SqFt__c >=25001 and SqFt__c <=100000)';
        }
            }
               query += ' )';
        }


        if(sortBy != null && sortBy != ''){
            
             if( sortBy != 'location' ){
                query += ' ORDER BY ' + sortBy + ' ASC ';
            } else {
                query += ' ORDER BY State__c, City__c ';
            }
        }

        if(rLimit != null && rLimit != ''){
            query += ' LIMIT ' + rLimit;
        }

        if(offset != null && offset != ''){
            query += ' OFFSET ' + offset;
        }

        System.debug('dynamic123'+query);
        propertyList = Database.query(query);
  
       
       return propertyList;
    }
    
    
        public String getStateList(String statename){
        system.debug('state12345678'+statename);
        List<SObject> stateList;
        List<String> outList = new List<String>();
        String response = '';
        Boolean first = true;        
        //String query = 'SELECT State__c FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Property_type__c LIKE \'' + multitenant + '\' and Status__c in (\'Owned\',\'Managed\')'; 
        String query = 'SELECT State__c FROM Web_Properties__c';
        if(statename!= null && statename != '' && statename!= 'all'){
            statename= statename.escapeEcmaScript();
            query += ' WHERE State__c = \'' + statename+ '\'';
        }

        
        query += ' GROUP BY State__c ORDER BY State__c ASC';
        system.debug('state123456'+query );
        stateList = Database.query(query);
        stateList = sanitizeList(stateList, 'State__c');

        response+='[';

        for(SOBject s : stateList){
            if(first){
                first=false;
            }else{
                response+=', ';
            }
            response+='{"short":"' + String.valueOf(s.get('State__c')) + '", ';
            if(stateMap.get(String.valueOf(s.get('State__c'))) != null)
                response+='"long":"' + stateMap.get(String.valueOf(s.get('State__c'))) + '"}';
            else
                response+='"long":"' + String.valueOf(s.get('State__c')) + '"}';
        }

        response+=']';

        return response;

    }
    
     public List<SObject> getCityList(String state){

        List<SObject> cityList;        
        String query = 'SELECT City__c FROM Web_Properties__c WHERE State__c = \'' + state + '\' and Web_Ready__c=\'' + webready + '\' and Property_Type_Group__c LIKE \'' + Anchored + '\' and Status__c in (\'Owned\',\'Managed\') GROUP BY City__c ORDER BY City__c ASC';
        system.debug('cityservicequery'+query );
        cityList = Database.query(query);

        return sanitizeList(cityList, 'City__c');

    }
  
   
    private List<SObject> sanitizeList(List<SObject> inList, String field){

        Integer ctr = 0;

        while(ctr < inList.size()){
            if(inList[ctr].get(field) == null){
                inList.remove(ctr);
            } else {
                ctr++;
            }
        }
        return inList;

    }
}