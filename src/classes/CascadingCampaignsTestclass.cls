@isTest
private class CascadingCampaignsTestclass {
  public static testMethod void testCascadingCampaigns() {
  
     Campaign cam = new Campaign();
    cam.name= 'Dinearound-National Conference-Cole Capital';
    cam.status = 'In Progress';
    cam.isActive=true;
    cam.parentid=null;
    insert cam;
    cam.Master_Camp_Id__c=cam.id; 
    
    Campaign subcam = new Campaign();
    subcam.name= 'RSVP-National Conference-Cole Capital';
    subcam.status = 'In Progress';
    subcam.isActive=true;
    subcam.parentid=cam.id;
    subcam.Master_Camp_Id__c=cam.id;
    insert subcam;
    System.assertnotEquals( cam.name,subcam.name);
    
    Campaign subcam1 = new Campaign();
    subcam1.name= 'Attend-National Conference-Cole Capital';
    //subcam1.status = 'Completed';
    subcam1.Master_Camp_Id__c=cam.id;
    subcam1.isActive=true;
    subcam1.parentid= subcam.id;
    insert subcam1;
    System.assertEquals(subcam1.name,'Attend-National Conference-Cole Capital');

    Contact c = new Contact();
    c.firstname='RSVP';
    c.lastname='test';
    insert c;
    
    Contact ct = new Contact();
    ct.firstname='Attend';
    ct.lastname='testing';
    insert ct;      
    System.assertnotEquals(c.firstname,ct.firstname);  
      
    CampaignMember member= new CampaignMember();
    member.CampaignId = cam.id;
    member.ContactId = c.id;
    member.Status='RSVP';
    insert member;
            
    CampaignMember member1= new CampaignMember();
    member1.CampaignId = cam.id;
    member1.ContactId = ct.id;
    member1.Status='Attend';
    insert member1;
    System.assertnotEquals(member.Status,member1.Status);  

    cam.status='completed';
    update cam;
    
    CampaignMember member2=new CampaignMember();
    member2.ContactId = c.id;
    member2.campaignid=subcam.id;
    member2.status='RSVP';
  
    CampaignMember member3=new CampaignMember();
    member3.ContactId = ct.id;
    member3.campaignid=subcam1.id;
    member3.status='Attend';
    System.assertnotEquals(member2.Status,member3.Status);  

  
  }
}