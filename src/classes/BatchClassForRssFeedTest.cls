@isTest
Public  with sharing class BatchClassForRssFeedTest  {
     static testmethod void testRssFeed() {
        
       string  profileid= [Select id from profile where name='System Administrator' limit 1].id;
        User userToCreate = new User();
       userToCreate.FirstName = 'David123';
       userToCreate.LastName  = 'Liu';
       userToCreate.Email     = 'dvdkliu+sfdc99@gmail.com';
       userToCreate.Username  = 'sfdc-dreamer055092015123@gmail.com';
       userToCreate.Alias     = 'fatty';
       userToCreate.ProfileId = profileid;
       userToCreate.TimeZoneSidKey    = 'America/Denver';
       userToCreate.LocaleSidKey      = 'en_US';
       userToCreate.EmailEncodingKey  = 'UTF-8';
       userToCreate.LanguageLocaleKey = 'en_US';
       insert userToCreate;
       
       System.assertEquals( userToCreate.FirstName,'David123');
       
       String query ='Select id from user where id=:userToCreate.id limit 1';
        /*  Test.setMock(BatchClassForRssFeed.class, new batchClassMockCalloutTest());
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
         BatchClassForRssFeed batch = new BatchClassForRssFeed();
        
        req.requestURI = '/services/apexrest/Accounts';  
        req.addParameter('page', '0');
        req.addParameter('time', '1390296387');
        req.addParameter('hash', '1wer2547');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res; */
       // String results = StoreController.getAccounts();

        test.starttest();
         // Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
         BatchClassForRssFeed batch = new BatchClassForRssFeed();
        database.executebatch(batch,1);
        test.stopTest();  
        
    }
}