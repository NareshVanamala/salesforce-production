public with sharing class RC_CreateCertificate
{

   public RC_Certificate_of_Insurance__c Certcate{get;set;}
   public String ids {get;set;}
   public String id {get;set;}
   public List<String> strs = new List<String>();
   public List<String> selectedDealIds = new List<String>();
   public list<RC_Location__c>locationlist{get;set;}
   public list<RC_Property__c>propertylist{get;set;}
   
   
   public RC_CreateCertificate()
   {
       Certcate=new RC_Certificate_of_Insurance__c(); 
       ids = ApexPages.currentPage().getParameters().get('ids');
       selectedDealIds = ids.split(',');
       system.debug('Thses are my selected'+selectedDealIds );
       locationlist=[select id,Property__c from RC_Location__c where Property__c in:selectedDealIds]; 
       system.debug('Thses are my locations'+locationlist.size()); 
       propertylist=[select id, name ,Risk_Portfolio__c from RC_Property__c where id in:selectedDealIds ];
       
       
   }


    public PageReference SAVE() 
    {
       
       system.debug('Entered in Save block');
       
       RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'RC_Certificate_of_Insurance__c' and DeveloperName = 'COI_Entry']; 
       list<RC_Certificate_of_Insurance__c>insertlist= new list<RC_Certificate_of_Insurance__c>();
       list<RC_Certificate_of_Insurance__c>PropertyRealtedinsertlist= new list<RC_Certificate_of_Insurance__c>();
       list<RC_Certificate_of_Insurance__c>Updatedist= new list<RC_Certificate_of_Insurance__c>();
       if(locationlist.size()>0)
       {
          for(RC_Location__c cr:locationlist)
          {
           
            RC_Certificate_of_Insurance__c crt= new RC_Certificate_of_Insurance__c();
            crt=Certcate.clone();
            crt.Location__c=cr.id;
            crt.recordtypeid=NewRecType.id; 
            insertlist.add(crt);
            
          }
        }  
       /* if(propertylist.size()>0)
       {
          for(RC_Property__c cr:propertylist)
          {
            RC_Certificate_of_Insurance__c crt= new RC_Certificate_of_Insurance__c();
            crt=Certcate.clone();
            crt.Property__c=cr.id;
            crt.recordtypeid=NewRecType.id; 
            insertlist.add(crt);
            
          }
        } */  
          
            if (Schema.sObjectType.RC_Certificate_of_Insurance__c.isCreateable())

            insert insertlist;
         
          
    string returnId;
    if(propertylist.size()>0)
    {
       
      returnId= propertylist[0].Risk_Portfolio__c;
    
    
    }   
        PageReference LseOptyPage = new PageReference('/' + returnId);
          LseOptyPage .setRedirect(true);
             
        return LseOptyPage ;
    }

    public PageReference Cancel() 
    {
        string returnId;
        if(propertylist.size()>0)
        { 
          returnId= propertylist[0].Risk_Portfolio__c;
        }   
        PageReference LseOptyPage = new PageReference('/' + returnId);
        LseOptyPage .setRedirect(true);
        return LseOptyPage ;
    }

}