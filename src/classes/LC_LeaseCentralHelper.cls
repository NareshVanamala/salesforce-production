Public class LC_LeaseCentralHelper
{
    public Static void GetLeaseOpportunityRecord(Id Lopid)
    {
       List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();
       Id leaseOpptid=Lopid;
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitted for Approval');  
        req.setObjectId(leaseOpptid);   
        approvalReqList.add(req);
     
       List<Approval.ProcessResult> resultList1 = Approval.process(approvalReqList);
       list<LC_LeaseOpprtunity__c>leaseOpplist=new list<LC_LeaseOpprtunity__c>();
       leaseOpplist=[select id,Final_Step_Processes__c from LC_LeaseOpprtunity__c where id=:leaseOpptid limit 1];
       leaseOpplist[0].Final_Step_Processes__c=false;
       update leaseOpplist[0];
    }
   
   public Static void SaveLETSFormCopy(Id Lopid,decimal myInteger, string mystring)
   {
       //Attachment Code Starts
            LC_LeaseOpprtunity__c ccop=[select id, name from LC_LeaseOpprtunity__c where id=:Lopid];
            PageReference pdf = Page.LC_LETSFormPdfPage;
            pdf.getParameters().put('id',ccop.id);
            Attachment attach = new Attachment();
            Blob body;
            try 
            {
               if (Test.IsRunningTest())
                {
                 body =Blob.valueOf('UNIT.TEST');
                 }
              else
              {
                body = pdf.getContentAsPdf();

          
              }
            
            } 
            catch (VisualforceException e) 
            {
            //body = Blob.valueOf('Some Text');
            }
            
            attach.Body = body;
              if (mystring=='Called from Lets form')
            {
              string myname='LETS Form'+string.valueof(myInteger)+'.pdf';
              attach.Name = myname;
            }
            else if(mystring=='Called from Process Builder')
            {
              attach.Name = 'LETS Form Original'+string.valueof(myInteger)+'.pdf';
            }
            attach.IsPrivate = false;
            attach.ParentId = Lopid;
            if (Schema.sObjectType.Attachment.isCreateable())

            insert attach;
            //Attachment Code ends
     
   
   
   }
   
}