global without sharing class ocms_PortalContactId implements cms.ServiceInterface {
    
    public ocms_PortalContactId() {
        
    }

    global System.Type getType() { return ocms_PortalContactId.class;}

    global String executeRequest (Map <String, String> p) {
        String response = '{"success": false}';
        if (p.get('action') == 'createContactCookie') {
            createContactCookie();
            response = '{"success": true}';
        }
        return response;
    }

    //User Id
    public String userId {get; set;}

    /**
    * @desc This function will create a new cookie if the cookie does not exist
    *       or if the current contact id doesn't match the cookie id         
    **/
    public string createContactCookie () {
        Cookie contactCookie; 
        Cookie contactCookie1;      
        String contactId;
        Blob cryptoKey = EncodingUtil.base64Decode('U4mPMMiAUZGm00AzjAKstw==');

        if (!Test.isRunningTest()){
            userId = UserInfo.getUserId();
        }

        SObject sObj = [SELECT Contact.Id FROM User WHERE Id = :userId];
        contactId = (String) sObj.get('ContactId');

        if (contactId != null) {

            //Blob data = EncodingUtil.base64Decode(contactId);
            Blob data = Blob.valueOf(contactId);
            
            Blob encryptedData = Crypto.encryptWithManagedIV('AES128', cryptoKey , data );
            String b64Data = EncodingUtil.base64Encode(encryptedData);
            String EcontactId = EncodingUtil.urlDecode(b64Data, 'UTF-8');

            contactCookie = ApexPages.currentPage().getCookies().get('ContactId');
            if (contactCookie != null) {
                if (contactCookie.getValue() != contactId) {
                    contactCookie = new Cookie('ContactId',contactId,null,-1,false);
                    contactCookie1 = new Cookie('EContactId',EcontactId,null,-1,false);
                    ApexPages.currentPage().setCookies(new Cookie[]{contactCookie});
                    ApexPages.currentPage().setCookies(new Cookie[]{contactCookie1});
                }
            } else {
                contactCookie = new Cookie('ContactId',contactId,null,-1,false);
                contactCookie1 = new Cookie('EContactId',EcontactId,null,-1,false);
                ApexPages.currentPage().setCookies(new Cookie[]{contactCookie});
                ApexPages.currentPage().setCookies(new Cookie[]{contactCookie1});
            }
        }
        
        return contactId;
    }
}