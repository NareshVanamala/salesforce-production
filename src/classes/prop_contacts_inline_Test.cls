@isTest(seeAlldata =true)
public class prop_contacts_inline_Test {
    static testMethod void testmethod1(){
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
        Account acc = new Account (
        Name = 'newAcc1'
        );  
        insert acc;
        Contact con = new Contact (
        AccountId = acc.id,
        LastName = 'portalTestUser'
        );
        insert con;
        Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
         
        User newUser = new User(
        profileId = p.id,
        username = 'newUser0615201720511@arcpreit.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = con.id
        );
        insert newUser;

        MRI_Property__c mrpObj = new MRI_Property__c();
        mrpObj.Name = 'testmrp';
        mrpObj.Property_Manager__c = newUser.id;
        mrpObj.Fund1__c = 'ARCP';
        mrpObj.Date_Acquired__c = Date.today();
        mrpObj.Common_Name__c = 'cname';
        mrpObj.Property_Id__c = '1234';
        insert mrpObj;

        list<Lease__c> leaseLst = new list<Lease__c>();
        
        Lease__c lObj1 = new Lease__c();
        lObj1.MRI_Property__c = mrpObj.Id;
        lObj1.Tenant_Name__c = 'testTenent1';
        lObj1.Lease_Status__c = 'Active';
        leaseLst.add(lObj1);
        
        Lease__c lObj2 = new Lease__c();
        lObj2.MRI_Property__c = mrpObj.Id;
        lObj2.Tenant_Name__c = 'testTenent1';
        lObj2.Lease_Status__c = 'In Active';
        leaseLst.add(lObj2);
        
        
        Lease__c lObj3 = new Lease__c();
        lObj3.MRI_Property__c = mrpObj.Id;
        lObj3.Tenant_Name__c = 'testTenent1';
        lObj3.Lease_Status__c = 'Active';
        leaseLst.add(lObj3);
        
        insert leaseLst;
        
        Property_Contacts__c pcObj1 = new Property_Contacts__c();
        pcObj1.Name = 'test1';
        pcObj1.Address__c='Address';
        pcObj1.Company_Name__c ='Company_Name' ;
        pcObj1.Department__c = 'Department' ;
        pcObj1.Notes__c = 'Notes' ;
        pcObj1.Email_Address__c = 'Test@gmail.com' ;
        pcObj1.Phone__c = '1234567890' ;
        pcObj1.City__c ='City' ;
        pcObj1.Address_Line_2__c = 'Address_Line_2';
        pcObj1.Cell_Phone__c = '1234567890';
        pcObj1.Fax_Number__c='1234567890';
        pcObj1.Title__c = 'Title';
        pcObj1.State__c ='State__c' ;
        pcObj1.Zipcode__c = '12345' ;
        pcObj1.MRI_Property__c = mrpObj.Id;
        insert pcObj1;
        
        Property_Contact_Leases__c pclObj1 = new Property_Contact_Leases__c();
        pclObj1.Lease__c = lObj1.Id;
        pclObj1.Property_Contacts__c = pcObj1.Id;
        pclObj1.Contact_Type__c = 'Developer;Financials;Gross Sales';
        insert pclObj1;
        
        ApexPages.currentPage().getParameters().put('mid', mrpObj.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(mrpObj);
        prop_contacts_inline1 classObj = new prop_contacts_inline1(sc);

        List<prop_contacts_inline1.cLease> cLeaselist = new List<prop_contacts_inline1.cLease>();
        cLeaselist.add(new prop_contacts_inline1.cLease(lObj1));
        cLeaselist.add(new prop_contacts_inline1.cLease(lObj2));
        classObj.leaseList = cLeaselist;
        List<prop_contacts_inline1.mywrapper> mywrapperlist = new List<prop_contacts_inline1.mywrapper>();
        mywrapperlist.add(new prop_contacts_inline1.mywrapper(pclObj1));
        classObj.getmylist();
        classObj.getmassapplylist();
        classObj.closePopup();
        classObj.ApplyPropertyContacts();
        classObj.createNewRecord();
        classObj.enable();
        classObj.getContacts();
        classObj.getleaseList();
        classObj.getmassapplylist();
        classObj.getmylist();
        classObj.getSelected();
        classObj.getshownewcon();
        classObj.getSnoozeradio();
        classObj.massapply();
        classObj.getmassapplylist();
        classObj.Save1();
        classObj.prconl.Contact_Type__c = 'Financials;Gross Sales';
        classObj.Save();
        classObj.SearchRecord();
        classObj.showPopup();
      
     

      
        
        
    }
     
}