@isTest
Public class LC_LETSFormPdfController_Test
{
  static testmethod void testCreatelease()
  {
           
        test.starttest();
         map<string,string> leaseNametoDesriptionMap = new map<string,string>(); 
     
         
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         mc.Enity_Id__c = 'P001';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU';
         mylease.Suite_Sqft__c=1234;
         mylease.Lease_ID__c='A046677';
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;

         Centralized_Helper_Object__c  chb = new Centralized_Helper_Object__c() ;
         chb.Name ='Lease Central-I';
         chb.LeaseCentral_Approval_Instructions__c= ' Test Instructions';
         insert chb;

         LC_LeaseOpprtunity__c lc1= new LC_LeaseOpprtunity__c();
         lc1.name='TestOpportunity';
         lc1.Lease_Opportunity_Type__c='Lease - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.LC_OtherOpportunity_Description__c='Thisis my description';
         lc1.Base_Rent__c=12345;
         lc1.Milestone__c= 'Lets In Progress';
         lc1.Status__c='Active';
         insert lc1;
         
         List<LC_OptionSchedule__c> OppSchedule = new list<LC_OptionSchedule__c>();

         LC_OptionSchedule__c ops = new LC_OptionSchedule__c();
         ops.Lease_Opportunity__c= lc1.id;
         ops.Months_Years__c='Months';
         ops.From_In_Months__c='1';
         ops.To_In_Months__c='3';
         ops.Amount__c=12345;
         OppSchedule.add(ops);
         
         LC_OptionSchedule__c ops1 = new LC_OptionSchedule__c();
         ops1.Lease_Opportunity__c= lc1.id;
         ops1.Months_Years__c='Months';
         ops1.From_In_Months__c='4';
         ops1.To_In_Months__c='5';
         ops1.Amount__c=12345;
         OppSchedule.add(ops1);
         
         LC_OptionSchedule__c ops2 = new LC_OptionSchedule__c();
         ops2 .Lease_Opportunity__c= lc1.id;
         ops2 .Months_Years__c='Months';
         ops2 .From_In_Months__c='6';
         ops2 .To_In_Months__c='8';
         ops2 .Amount__c=12345;
         OppSchedule.add(ops2);
         insert OppSchedule;
         
         List<LC_RentSchedule__c > RntSchedule = new list<LC_RentSchedule__c >();
        
         LC_RentSchedule__c rs= new LC_RentSchedule__c();
         rs.Lease_Opportunity__c= lc1.id; 
         rs.Lease__c= mylease.id;   
         rs.Months_Years__c='Months';
         rs.From_In_Months__c='1';
         rs.To_In_Months__c='3';
         rs.Annual_PSF__c=12345;
         RntSchedule .add(rs);
        
         LC_RentSchedule__c rs1= new LC_RentSchedule__c();
         rs1.Lease_Opportunity__c= lc1.id; 
         rs1.Lease__c= mylease.id;     
         rs1.Months_Years__c='Months';
         rs1.From_In_Months__c='5';
         rs1.To_In_Months__c='8';
         rs1.Annual_PSF__c=12345;
         RntSchedule .add(rs1);
         insert RntSchedule ;

         List<LC_Clauses__c> clauses= new list<LC_Clauses__c >();

         LC_Clauses__c c1 = new LC_Clauses__c ();
         c1.Clause_Type__c = '125PEU';
         c1.Clause_Body__c = 'Permitted Use';
         c1.Order_On_LETS_form__c = 1 ;
         c1.Show_On_LETs_Form__c = TRUE;
         clauses.add(c1);
     
         LC_Clauses__c c3 = new LC_Clauses__c ();
         c3.Clause_Type__c = '305FIX';
         c3.Clause_Body__c = 'Rent Commencement/Fixturization Period';
         c3.Order_On_LETS_form__c = 2 ;
         c3.Show_On_LETs_Form__c = TRUE;
         clauses.add(c3); 
                  
         LC_Clauses__c c4 = new LC_Clauses__c ();
         c4 .Clause_Type__c = '139LINS';
         c4 .Clause_Body__c = 'LAndlords Insurance';
         c4.Order_On_LETS_form__c = 3 ;
         c4.Show_On_LETs_Form__c = TRUE;
         clauses.add(c4); 

         LC_Clauses__c c6 = New LC_Clauses__c();
         c6 .Name='108SECD';
         c6 .Clause_Type__c= 'Secutity Deposite';
         c6.Order_On_LETS_form__c = 4 ;
         c6.Show_On_LETs_Form__c = TRUE;
         clauses.add(c6); 
         
         LC_Clauses__c c7 = New LC_Clauses__c();
         c7.Name='127DRK';
         c7.Clause_Type__c= 'Go Dark Right';
         c7.Order_On_LETS_form__c = 5 ;
         c7.Show_On_LETs_Form__c = TRUE;
         clauses.add(c7); 
         insert clauses;
            
         List<LC_ClauseType__c> clauseType= new list<LC_ClauseType__c>();
     
         LC_ClauseType__c ct1 = new LC_ClauseType__c ();
         ct1.Clauses__c=c1.id;
         ct1.Lease_Opportunity__c= lc1.id;
         ct1.Name=c1.Clause_Type__c;
         ct1.ClauseType_Name__c=c1.Clause_Body__c;
         ct1.Clause_Description__c='Permitted Use';
         clauseType.add(ct1); 
        
         LC_ClauseType__c ct3 = new LC_ClauseType__c ();
         ct3.Clauses__c=c1.id;
         ct3.Lease_Opportunity__c= lc1.id;
         ct3.Name=c3.Clause_Type__c;
         ct3.ClauseType_Name__c=c3.Clause_Body__c;
         ct3.Clause_Description__c='Fixturization Period (Rent Commencement)';
         clauseType.add(ct3); 
         
         LC_ClauseType__c ct4 = new LC_ClauseType__c ();
         ct4.Clauses__c=c4.id;
         ct4.Lease_Opportunity__c= lc1.id;
         ct4.Name=c4.Clause_Type__c;
         ct4.ClauseType_Name__c=c4.Clause_Body__c;
         ct4.Clause_Description__c='Landlords Insurance';
         clauseType.add(ct4); 
         
         LC_ClauseType__c ct6 = new LC_ClauseType__c ();
         ct6.Clauses__c=c6.id;
         ct6.Lease_Opportunity__c= lc1.id;
         ct6.Name=c6.Clause_Type__c;
         ct6.ClauseType_Name__c=c6.Clause_Body__c;
         ct6.Clause_Description__c='Security Deposite';
         clauseType.add(ct6); 
         
         LC_ClauseType__c ct7 = new LC_ClauseType__c ();
         ct7.Clauses__c=c7.id;
         ct7.Lease_Opportunity__c= lc1.id;
         ct7.Name=c7.Clause_Type__c;
         ct7.ClauseType_Name__c=c7.Clause_Body__c;
         ct7.Clause_Description__c='Security Deposite';
         clauseType.add(ct7); 
         
         insert clausetype;
         
         if((ct1.ClauseType_Name__c!=null)&&(ct1.Clause_Description__c!=null) )
         leaseNametoDesriptionMap.put(ct1.ClauseType_Name__c,ct1.Clause_Description__c);
         
         if((ct3.ClauseType_Name__c!=null)&&(ct3.Clause_Description__c!=null) )
         leaseNametoDesriptionMap.put(ct3.ClauseType_Name__c,ct3.Clause_Description__c);

         if((ct4.ClauseType_Name__c!=null)&&(ct4.Clause_Description__c!=null) )
         leaseNametoDesriptionMap.put(ct4.ClauseType_Name__c,ct4.Clause_Description__c);
          
         if((ct6.ClauseType_Name__c!=null)&&(ct6.Clause_Description__c!=null) )
         leaseNametoDesriptionMap.put(ct6.ClauseType_Name__c,ct6.Clause_Description__c);
         
         if((ct7.ClauseType_Name__c!=null)&&(ct7.Clause_Description__c!=null) )
         leaseNametoDesriptionMap.put(ct7.ClauseType_Name__c,ct7.Clause_Description__c);

        
        System.assertEquals(myLease.MRI_PROPERTY__c , mc.id);
        System.assertEquals( lc1.Lease__c, mylease.id);
        System.assertEquals( 3,OppSchedule.size());
        System.assertNotEquals( 0,RntSchedule.size());
        System.assertNotEquals( 0,clauses.size());
        System.assertNotEquals( 0,clausetype.size());
        
         ApexPages.StandardController stdcon = new ApexPages.StandardController(lc1);
          ApexPages.currentPage().getParameters().put('id',lc1.id);
       LC_LETSFormPdfController  LCcontroller = new  LC_LETSFormPdfController(stdcon);
       
    test.stoptest();
 
    
  } 
  
  static testmethod void testCreatelease1()
  {  
  test.starttest();
         map<string,string> leaseNametoDesriptionMap = new map<string,string>(); 
         
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
         
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU';
         mylease.Suite_Sqft__c=1234;
         mylease.Lease_ID__c='A046677';
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         Centralized_Helper_Object__c  chb = new Centralized_Helper_Object__c() ;
         chb.Name ='Lease Central-I';
         chb.LeaseCentral_Approval_Instructions__c= ' Test Instructions';
         insert chb;

         LC_LeaseOpprtunity__c lc1= new LC_LeaseOpprtunity__c();
         lc1.name='TestOpportunity';
         lc1.Lease_Opportunity_Type__c='Lease - Renewal';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.LC_OtherOpportunity_Description__c='Thisis my description';
         lc1.Base_Rent__c=12345;
         insert lc1;
         
         List<LC_Clauses__c> clauses= new list<LC_Clauses__c >();

         LC_Clauses__c c1 = new LC_Clauses__c ();
         c1.Clause_Type__c = '125PEU';
         c1.Clause_Body__c = 'Permitted Use';
         c1.Order_On_LETS_form__c = 1 ;
         c1.Show_On_LETs_Form__c = TRUE;
         clauses.add(c1);
     
         LC_Clauses__c c3 = new LC_Clauses__c ();
         c3.Clause_Type__c = '305FIX';
         c3.Clause_Body__c = 'Rent Commencement/Fixturization Period';
         c3.Order_On_LETS_form__c = 2 ;
         c3.Show_On_LETs_Form__c = TRUE;
         clauses.add(c3); 
                  
         LC_Clauses__c c4 = new LC_Clauses__c ();
         c4 .Clause_Type__c = '139LINS';
         c4 .Clause_Body__c = 'LAndlords Insurance';
         c4.Order_On_LETS_form__c = 3 ;
         c4.Show_On_LETs_Form__c = TRUE;
         clauses.add(c4); 

         LC_Clauses__c c6 = New LC_Clauses__c();
         c6 .Name='108SECD';
         c6 .Clause_Type__c= 'Secutity Deposite';
         c6.Order_On_LETS_form__c = 4 ;
         c6.Show_On_LETs_Form__c = TRUE;
         clauses.add(c6); 
         
         LC_Clauses__c c7 = New LC_Clauses__c();
         c7.Name='127DRK';
         c7.Clause_Type__c= 'Go Dark Right';
         c7.Order_On_LETS_form__c = 5 ;
         c7.Show_On_LETs_Form__c = TRUE;
         clauses.add(c7); 
         insert clauses;
            
         List<LC_ClauseType__c> clauseType= new list<LC_ClauseType__c>();
     
         LC_ClauseType__c ct1 = new LC_ClauseType__c ();
         ct1.Clauses__c=c1.id;
         ct1.Lease_Opportunity__c= lc1.id;
         ct1.Name=c1.Clause_Type__c;
         ct1.ClauseType_Name__c=c1.Clause_Body__c;
         ct1.Clause_Description__c='Permitted Use';
         clauseType.add(ct1); 
        
         LC_ClauseType__c ct3 = new LC_ClauseType__c ();
         ct3.Clauses__c=c1.id;
         ct3.Lease_Opportunity__c= lc1.id;
         ct3.Name=c3.Clause_Type__c;
         ct3.ClauseType_Name__c=c3.Clause_Body__c;
         ct3.Clause_Description__c='Fixturization Period (Rent Commencement)';
         clauseType.add(ct3); 
         
         LC_ClauseType__c ct4 = new LC_ClauseType__c ();
         ct4.Clauses__c=c4.id;
         ct4.Lease_Opportunity__c= lc1.id;
         ct4.Name=c4.Clause_Type__c;
         ct4.ClauseType_Name__c=c4.Clause_Body__c;
         ct4.Clause_Description__c='Landlords Insurance';
         clauseType.add(ct4); 
         
         LC_ClauseType__c ct6 = new LC_ClauseType__c ();
         ct6.Clauses__c=c6.id;
         ct6.Lease_Opportunity__c= lc1.id;
         ct6.Name=c6.Clause_Type__c;
         ct6.ClauseType_Name__c=c6.Clause_Body__c;
         ct6.Clause_Description__c='Security Deposite';
         clauseType.add(ct6); 
         
         LC_ClauseType__c ct7 = new LC_ClauseType__c ();
         ct7.Clauses__c=c7.id;
         ct7.Lease_Opportunity__c= lc1.id;
         ct7.Name=c7.Clause_Type__c;
         ct7.ClauseType_Name__c=c7.Clause_Body__c;
         ct7.Clause_Description__c='Security Deposite';
         clauseType.add(ct7); 
         
         insert clausetype;
         
         Delete ct1;
         Delete ct3;
         Delete ct4;
         Delete ct6;
         Delete ct7;
         
         if((ct1.ClauseType_Name__c!=null)&&(ct1.Clause_Description__c!=null) )
         leaseNametoDesriptionMap.put(ct1.ClauseType_Name__c,ct1.Clause_Description__c);
         
         if((ct3.ClauseType_Name__c!=null)&&(ct3.Clause_Description__c!=null) )
         leaseNametoDesriptionMap.put(ct3.ClauseType_Name__c,ct3.Clause_Description__c);

         if((ct4.ClauseType_Name__c!=null)&&(ct4.Clause_Description__c!=null) )
         leaseNametoDesriptionMap.put(ct4.ClauseType_Name__c,ct4.Clause_Description__c);
          
         if((ct6.ClauseType_Name__c!=null)&&(ct6.Clause_Description__c!=null) )
         leaseNametoDesriptionMap.put(ct6.ClauseType_Name__c,ct6.Clause_Description__c);
         
         if((ct7.ClauseType_Name__c!=null)&&(ct7.Clause_Description__c!=null) )
         leaseNametoDesriptionMap.put(ct7.ClauseType_Name__c,ct7.Clause_Description__c);
   
        

         LC_OptionSchedule__c ops = new LC_OptionSchedule__c();
         ops.Lease_Opportunity__c= lc1.id;
         ops.Months_Years__c='Months';
         ops.From_In_Months__c='1';
         ops.To_In_Months__c='3';
         ops.Amount__c=12345;
         insert ops ;
       try 
       { 
         LC_OptionSchedule__c ops1 = new LC_OptionSchedule__c();
         ops1 .Lease_Opportunity__c= lc1.id;
         ops1 .Months_Years__c='Months';
         ops1 .From_In_Months__c='2';
         ops1 .To_In_Months__c='2';
         ops1 .Amount__c=12345;
         insert ops1 ;
      }          
       catch(Exception e)
        {
        Boolean expectedExceptionThrown =  e.getMessage().contains('Duplicate From and To values.') ? true : false;
        System.AssertEquals(expectedExceptionThrown, true );
        } 
       try 
       { 
         LC_OptionSchedule__c ops1 = new LC_OptionSchedule__c();
         ops1 .Lease_Opportunity__c= lc1.id;
         ops1 .Months_Years__c='Months';
         ops1 .From_In_Months__c='4';
         ops1 .To_In_Months__c='8';
         ops1 .Amount__c=12345;
         insert ops1 ;
       }  
         catch(Exception e)
        {
        Boolean expectedExceptionThrown =  e.getMessage().contains('From(Months/Years) should be greater than To(Months/Years) of any previous records.') ? true : false;
        System.AssertEquals(expectedExceptionThrown, true);
        }
        
        try 
       { 
         LC_OptionSchedule__c ops1 = new LC_OptionSchedule__c();
         ops1 .Lease_Opportunity__c= lc1.id;
         ops1 .Months_Years__c='Months';
         ops1 .From_In_Months__c='2';
         ops1 .To_In_Months__c='2';
         ops1 .Amount__c=12345;
         insert ops1 ;
       }  
         catch(Exception e)
        {
        Boolean expectedExceptionThrown = e.getMessage().contains('To(Months/Years) should be greater than To(Months/Years) of any previous records.') ? true : false;
        System.AssertEquals(expectedExceptionThrown, false);
        } 
        
         LC_RentSchedule__c rs= new LC_RentSchedule__c();
         rs.Lease_Opportunity__c= lc1.id; 
         rs.Lease__c= mylease.id;   
         rs.Months_Years__c='Months';
         rs.From_In_Months__c='1';
         rs.To_In_Months__c='3';
         rs.Annual_PSF__c=12345;
         insert rs;
         Try{
         LC_RentSchedule__c rs1= new LC_RentSchedule__c();
         rs1.Lease_Opportunity__c= lc1.id; 
         rs1.Lease__c= mylease.id;   
         rs1.Months_Years__c='Months';
         rs1.From_In_Months__c='2';
         rs1.To_In_Months__c='2';
         rs1.Annual_PSF__c=12345;
         insert rs1;
       }
       
       catch(Exception e)
        {
        Boolean expectedExceptionThrown =  e.getMessage().contains('Duplicate From and To values.') ? true : false;
        System.AssertEquals(expectedExceptionThrown, true);
        } 
         System.assertEquals(myLease.MRI_PROPERTY__c , mc.id);
        System.assertEquals( lc1.Lease__c, mylease.id);
        System.assertNotEquals( 0,clauses.size());
        System.assertNotEquals( 0,clausetype.size());
         
         ApexPages.StandardController stdcon = new ApexPages.StandardController(lc1);
          ApexPages.currentPage().getParameters().put('id',lc1.id);
          LC_LETSFormPdfController  LCcontroller = new  LC_LETSFormPdfController(stdcon);
    test.stoptest();
 
    
  }   
  
 }