@isTest (SeeAllData=true)
private class test_ocms_PropertyforLeaseDetail
{
    
    @isTest static void test_getHTML() {

        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        
        Web_Properties__c wp=new Web_Properties__c();
        wp.name='test';
        wp.Long_Description__c='hi';
        wp.Tenant_Profile__c='hii';
        wp.Property_Type__c='hi';
        wp.sector__c='hi';
        wp.ProgramName__c='lowe';
        wp.Date_Acquired__c=system.today();
        wp.sqFt__c=2000;
        wp.Related_Resources__c='test';
        wp.status__c='approved for tenant';
        wp.Gross_Leaseable_Area__c=1800;
        wp.ImagesforLease__c='test';
        wp.Web_Ready__c='approved';
        wp.lease_status__c='sold';
        wp.Address__c = '1600 Amphitheatre Parkway';
        wp.City__c = 'Mountain View';
        wp.State__c    = 'CA';
        wp.Country__c ='USA';
        System.assertEquals(wp.Country__c ,'USA');

        try{
        insert wp;
        
        System.currentPageReference().getParameters().put('propertyId', wp.id); //Modify this value to point to a web_property__c record with values.
        
       
        ocms_PropertyforLeaseDetail detail = new ocms_PropertyforLeaseDetail();
        detail.getHTML();} catch(exception e){}
    }
    
    @isTest static void test_getHTML1() {

        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        
        Web_Properties__c wp=new Web_Properties__c();
        wp.name='test';
        wp.Long_Description__c='';
        wp.Tenant_Profile__c='';
        wp.Property_Type__c='hi';
        wp.sector__c='hi';
        wp.ProgramName__c='lowe';
        wp.Date_Acquired__c=system.today();
        wp.sqFt__c=2000;
        wp.Related_Resources__c='test';
        wp.status__c='approved for tenant';
        wp.Gross_Leaseable_Area__c=1800;
        wp.ImagesforLease__c='test';
        wp.Web_Ready__c='approved';
        wp.lease_status__c='sold';
        wp.Address__c = '1600 Amphitheatre Parkway';
        wp.City__c = 'Mountain View';
        wp.State__c    = 'CA';
        wp.Country__c ='USA';
        System.assertEquals(wp.Country__c ,'USA');

        try{
        insert wp;

        System.currentPageReference().getParameters().put('propertyId', wp.id); //Modify this value to point to a web_property__c record with values.
        
        
        ocms_PropertyforLeaseDetail detail = new ocms_PropertyforLeaseDetail();
        detail.getHTML();} catch(exception e){}
    }
    
}