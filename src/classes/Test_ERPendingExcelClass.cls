@isTest
public class Test_ERPendingExcelClass
{
    static testMethod void ERPendingExcelClassTest()
    { 
        User u = [select id,name from user where id = :Userinfo.getuserid()]; 
        Id EventRecordTypeId =[Select id,name,DeveloperName from RecordType where DeveloperName='BD_Due_Diligence_Record_Type' and SobjectType = 'Event_Automation_Request__c'].id; 
        Id CompRecordTypeId =[Select id,name,DeveloperName from RecordType where DeveloperName='Forum_Event' and SobjectType = 'CampaignMember'].id; 
  
        Account acc = new Account();
        acc.Name = 'Test Prospect Account';
        Insert acc;
      
        Contact c = new Contact();
        c.firstname='RSVP';
        c.lastname='test';
        c.email = 'a@gmail.com';
        c.accountid=acc.id;
        insert c;
        system.assertequals(c.accountid,acc.id);

        acc.Event_Approver__c = c.id;
        update acc;
        
        Contact ct = new Contact();
        ct.firstname='Attend';
        ct.lastname='testing';
        ct.accountid=acc.id;
        ct.email='skalamkar@colecapital.com';
        insert ct;
        system.assertequals(ct.accountid,acc.id);

        acc.Event_Approver__c=ct.id;
        User us = [SELECT Id FROM User limit 1];  
        
        Campaign cam = new Campaign();
        cam.name='Dinearound-National Conference-Cole Capital';
        cam.status='Pending';
        cam.status='Approved';
        cam.status='Rejected';
        cam.isActive=true;
        cam.parentid=null;
        insert cam;

        Event_Automation_Request__c ear = new Event_Automation_Request__c();
        ear.Name='BDE13006';
        ear.Event_Name__c='testeventname';
        ear.Start_Date__c=System.Today() + 1;
        ear.End_Date__c=System.Today() + 1;
        ear.Broker_Dealer_Name__c=acc.id;
        ear.Event_Location__c='testlocation';
        ear.Key_Account_Manager_Name_Requestor__c=us.id;
        ear.Event_Venue__c='testvenue';
        ear.Campaign__c=cam.id;
        insert ear;
        
        CampaignMember member= new CampaignMember();
        member.CampaignId = cam.id;
        member.ContactId = c.id;
        member.Status='Pending';
        insert member;
        system.assertequals(member.ContactId,c.id);

        CampaignMember member1= new CampaignMember();
        member1.CampaignId = cam.id;
        member1.ContactId = ct.id;
        member1.Status='Pending';
        insert member1;
        ApexPages.CurrentPage().getParameters().put('calllist','testeventname');
        ApexPages.CurrentPage().getParameters().put('emid','a@gmail.com');

       ERPendingExcelClass erp = new ERPendingExcelClass();
       //erp.sMail = 'a@gmail.com';
       //erp.campPendList=[select id, RecordTypeId, CampaignId ,Contact.FirstName,Contact.name, Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c,Broker_Dealer_Name__c ,contact.account.name from CampaignMember where CampaignId =:e.Campaign__c and status='Pending' and Contact.Account.Event_Approver__r.email=:sMail ORDER BY contact.name];
//erp.campApprList=[select id, RecordTypeId, CampaignId ,Contact.FirstName,Contact.name, Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c,Broker_Dealer_Name__c,contact.account.name from CampaignMember where CampaignId =:e.Campaign__c and status='Approved' and Contact.Account.Event_Approver__r.email=:sMail ORDER BY contact.name];
//erp.campDeclList=[select id, RecordTypeId, CampaignId ,Contact.FirstName,Contact.name, Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c,Broker_Dealer_Name__c,contact.account.name from CampaignMember where CampaignId =:e.Campaign__c and status='Declined' and Contact.Account.Event_Approver__r.email=:sMail ORDER BY contact.name];

  }
}