@isTest
private class ConcordSingleSignOnTest 
{    
    static testMethod void myUnitTest() 
    {      
            //Test converage for the myPage visualforce page

        //PageReference pageRef = Page.MyPage;

        //Test.setCurrentPageReference(pageRef);


        Account newAccount = new Account (name='XYZ Organization');

        insert newAccount;


        //create first contact

        Contact myContact = new Contact (FirstName='Joe',LastName='Schmoe',Email='bplumb@colecapital.com',AccountId=newAccount.id);

        insert myContact;
        
        System.assertNotEquals(newAccount.name,myContact.FirstName);

         User user=new User();        
         user=[select id from User where FirstName = 'Barry' and LastName='Plumb'];   
                
        ConcordSingleSignOn.loginToConcord(myContact.Id,user.Id);


       
    }

    static testMethod void loginToConcord_LightningTest() 
    {      
            //Test converage for the myPage visualforce page

        //PageReference pageRef = Page.MyPage;

        //Test.setCurrentPageReference(pageRef);


        Account newAccount = new Account (name='XYZ Organization');

        insert newAccount;


        //create first contact

        Contact myContact = new Contact (FirstName='Joe',LastName='Schmoe',Email='bplumb@colecapital.com',AccountId=newAccount.id);

        insert myContact;
        
        System.assertNotEquals(newAccount.name,myContact.FirstName);

         User user=new User();        
         user=[select id from User where FirstName = 'Barry' and LastName='Plumb'];   
        Test.startTest();        
            ConcordSingleSignOn.loginToConcord_Lightning(myContact.Id);
        Test.stopTest();

       
    }
}