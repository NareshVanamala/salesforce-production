public with sharing class deletecalllistclass{

public Call_List__c calllist{get;set;}
Public Task tsk{get;set;}
public List<Call_List__c> results{get;set;}
public string searchString{get;set;} // search keyword

List<Call_List__c> clist= new List<Call_List__c>();
List<Call_List__c> clist1= new List<Call_List__c>();
public Boolean flag{get;set;}
String Viewname='';

public Call_List__c getCallList() {
if(callList == null) callList = new Call_List__c();
return callList;
}
public void Init()
{
 flag=false;

}

public List<SelectOption> getListViews1() {
List<SelectOption> options1 = new List<SelectOption>();
if(searchString == null || searchString == ''){
  clist1= [select id, name from Call_List__c order by name];
}
else{
 clist1= [select id, name from Call_List__c where name like:'%'+String.escapeSingleQuotes(searchString)+'%'];
}
For(integer i=0;i<clist1.size();i++)
{
options1.add(new SelectOption(clist1[i].name,clist1[i].name));
}
return options1;
}

public String getViewname() {
return Viewname;
}
public void setViewname(String Viewname) {
this.Viewname = Viewname;
}

public PageReference deleteCall(){
   flag = true;
   List<Call_List__c> rec = [Select id, name from Call_List__c where name=:Viewname];
   List<Task> tcall = [select ID,Call_list__c from Task where status!='Completed' and Call_list__c=:Viewname limit 10000];
   System.debug('******tcall'+ tcall.size());
    //System.debug('******tcall'+ tcall.Call_list__c[0]);   
   delete tcall;
   delete rec;
   return null;
}

public PageReference cancel(){
   PageReference pageref = new PageReference('/apex/CalllistGeneratorVf');
      return pageref;
}
public deletecalllistclass() {
    calllist = new Call_List__c();
    //tsk = new Task();
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
    runSearch();  
  }
  
  public String getSearchString(){
       return searchString;
  }
  
  public void setSearchString(String SearchString) {
        this.SearchString= SearchString;
        }
 
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
 
  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    results = performSearch(searchString);               
  } 
 
  // run the search and return the records found. 
  private List<Call_List__c> performSearch(string searchString) {
 
    String soql = 'select id, name from Call_List__c';
    if(searchString != '' && searchString != null)
      soql = soql +  ' where name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\'';
    soql = soql + ' limit 250';
    System.debug(soql);
    return database.query(soql); 
 
  }
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
 
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
  

}