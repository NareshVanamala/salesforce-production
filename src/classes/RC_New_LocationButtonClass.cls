public with sharing class RC_New_LocationButtonClass
{
    public String recortypeid{get;set;}
    public String pid{get;set;}
    public RC_Location__c mp{get;set;}
    public RC_Location__c ProjectObj{get;set;}
    public RC_Property__c ppc{get;set;}
    public RC_Property__c ppc1{get;set;}
     String Ptype{get;set;}
    //String Url1;
    
  Public RC_New_LocationButtonClass(Apexpages.StandardController controller)
  {
      mp= (RC_Location__c)controller.getRecord();
      pid=mp.id;
      string pid=mp.Property__c;
      if(pid!=null)
      {
       ppc1=[select id, Name,Known_Name__c,Entity_Tenant_Code__c,Occupancy__c,Total_Square_Feet__c,Insurable_Value__c from RC_Property__c where id=:pid];
       Ptype=ppc1.Occupancy__c;
      }
   }
  
   public PageReference Gobacktostandardpage() 
   {  
         system.debug('Check the Property'+mp.Property__c);
        
        if(mp.Property__c==null) 
        {
         //string url1=System.Label.Current_Org_Url+'/a5F/e?nooverride=1&retURL=%2Fa5F%2Fo';
         string url1=System.Label.Current_Org_Url+'/a4Z/e?nooverride=1&retURL=%2Fa4Z%2Fo';
         PageReference acctPage = new PageReference(Url1);
         acctPage.setRedirect(true); 
         return acctPage;
        
        }
   
     else if((mp.Property__c!=null)&&(Ptype=='Single-Tenant'))
     {
          string bid1;
         string bid=mp.Property__c;
         system.debug('Check the Property'+bid);
        // ppc=[select id, Name,Known_Name__c,Entity_Tenant_Code__c,Total_Square_Feet__c,Insurable_Value__c from RC_Property__c where id=:bid];
         string tenantname=ppc1.Known_Name__c;
         tenantname=tenantname.replaceAll('&','%26');
         String name=PPC1.name;
         string EntityTCode=PPC1.Entity_Tenant_Code__c;
         String tsf=string.valueof(PPC1.Total_Square_Feet__c);
         String inSvalue=string.valueOf(PPC1.Insurable_Value__c);
         if(ppc1!=null)
         bid1=ppc1.Name;
         String s2 = bid1.replaceAll('&', '%26');
         //String Url1=System.Label.Current_Org_Url+'/a5F/e?nooverride=1&CF00NR0000001JRYN_lkid='+bid+'&CF00NR0000001JRYN='+s2+'&00NR0000001JRY8='+EntityTCode+'&Name='+name+'&00NR0000001JRYV='+tenantname+'&00NR0000001JRYR='+tsf+'&00NR0000001JRYO='+inSvalue+'&retURL=%2F'+ppc1.id+'';
         //  Url1= 'https://arcp--stage.cs2.my.salesforce.com/a5F/e?nooverride=1&CF00NR0000001JRYN='+s2+'&CF00NR0000001JRYN_lkid='+bid+'&retURL=%2Fa5HR000000001Hc';
        String Url1=System.Label.Current_Org_Url+'/a4Z/e?nooverride=1&CF00N50000003Lzol_lkid='+bid+'&CF00N50000003Lzol='+s2+'&00N50000003LzoW='+EntityTCode+'&Name='+name+'&00N50000003Lzot='+tenantname+'&00N50000003Lzop='+tsf+'&00N50000003Lzom='+inSvalue+'&retURL=%2F'+ppc1.id+'';

      
      
         PageReference acctPage = new PageReference(Url1);
         acctPage.setRedirect(true); 
         return acctPage; 
     }
     else if((mp.Property__c!=null)&&(Ptype!='Single-Tenant'))
     {
        system.debug('Check the Property'+mp.Property__c);
         string bid1;
         string bid=mp.Property__c;
         system.debug('Check the Property'+bid);
         if(ppc1!=null)
         bid1=ppc1.Name;
         String s2 = bid1.replaceAll('&', '%26');
       //String Url1=System.Label.Current_Org_Url+'/a5F/e?nooverride=1&CF00NR0000001JRYN_lkid='+bid+'&CF00NR0000001JRYN='+s2+'&00NR0000001JRY8='+EntityTCode+'&Name='+name+'&00NR0000001JRYV='+tenantname+'&00NR0000001JRYR='+tsf+'&00NR0000001JRYO='+inSvalue+'&retURL=%2Fa5HR000000001Hc';
        //string  Url1= System.Label.Current_Org_Url+'/a5F/e?nooverride=1&CF00NR0000001JRYN='+s2+'&CF00NR0000001JRYN_lkid='+bid+'&retURL=%2F'+ppc1.id+'';
         String Url1=System.Label.Current_Org_Url+'/a4Z/e?nooverride=1&CF00N50000003Lzol_lkid='+bid+'&CF00N50000003Lzol='+s2+'&retURL=%2F'+ppc1.id+'';
  
         PageReference acctPage = new PageReference(Url1);
         acctPage.setRedirect(true); 
         return acctPage; 
     
     }
     return null;
  }
}