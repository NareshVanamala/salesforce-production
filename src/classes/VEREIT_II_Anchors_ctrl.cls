global virtual with sharing class VEREIT_II_Anchors_ctrl extends cms.ContentTemplateController 
{
     
    global VEREIT_II_Anchors_ctrl (cms.CreateContentController cc) 
    {
      if(!test.isRunningTest())
      {
        super(cc);
      }
 
    }
    global VEREIT_II_Anchors_ctrl ()
    {

    }    
   global String title {get;set;} 
    
  /*  public String t1
    { 
        get 
        {
            return getProperty('t1');
        }
    }
    public String Order 
    {
        get 
        {
            return getProperty('Order');
        }
    }
    public String Text
    {
        get 
        {
            return getProperty('Text');
        }
    }
    public String Editor
    {
        get 
        {
            return getProperty('Editor');
        }
    }
   
    public String News
    {
        get 
        {       
            return this.getProperty('News');
        }
    }  */
    
     public String DefaultLeadership
    {
        get 
        {       
            return this.getProperty('DefaultLeadershipSample');
        }
    }  
    public cms.Link TargetPageLinkObj
    {
        get
        {
            return this.getPropertyLink('TargetPageLinkSample');
        }
    }
     public String htmlContent
     {
        get 
        {       
            return this.getProperty('htmlContent');
        }
    }  
    public String replaceMacros (String strContent) 
    {
        
        System.debug('In replaceMacros, got the input as ' + strContent);
        
        // This function will replace the Macros at run time e.g. ||LATESTNEWS|| will be replaced by latest News article
        String prContent = '<div class="newsContainer"><section class="main" style="clear: both;"><div class="ocmsPressReader">';

            for(Press_Releases__c sObj_PressRelease: [SELECT Id, Press_Release_Name__c,Date__c,Article_Link__c,Description__c,Published_Date__c FROM Press_Releases__c ORDER BY Published_Date__c DESC limit 1])
            {
                /*string str='<div class="col-md-home dates justleft" style="clear: none;">'+ 
                   '<p>'+ sObj_PressRelease.get('Date__c') + ':' + sObj_PressRelease.get('Press_Release_Name__c') +'</p>' +
                '</div>';*/
                string str = sObj_PressRelease.Date__c + '<br/>' + sObj_PressRelease.Press_Release_Name__c;
                 if(str.length()<=90)
                 {
                   prContent +='<div class="col-md-home dates justleft" style="clear: none;">'+ 
                               '<p>' + '<a class="ocms_link_pdf" href="' + sObj_PressRelease.get('Article_Link__c')  + '" target="_blank">'+str+'</a>'+'</p>' +
                            '</div>';
                 
                 }
                 else if(str.length()>90)
                 {
                           prContent +='<div class="col-md-home dates justleft" style="clear: none;">'+ 
                               '<p>' + '<a class="ocms_link_pdf" href="' + sObj_PressRelease.get('Article_Link__c')  + '" target="_blank">'+str.substring(0,90)+'...</a>'+'</p>' +
                            '</div>';
                            
                  }          
          
            }

         prContent  +='</div></section></div>';
        
        System.debug('prcontent is ' + prContent);

        String returnHTML = '';

     
   returnHTML = strContent.replace('||LATESTNEWS||', prContent); 
        System.debug('after replacing strContent is ' + returnHTML);
        return returnHTML; 
    }
  
    global override virtual String getHTML()
    {
        // short circuit and return the html directly
        
      // return htmlContent;
       return replaceMacros(htmlContent);
    }
    
}