global class UpdateContacts implements Database.Batchable<SObject>
{
list<contact> contactlist = new list<contact>();
list<contact> clist = new list<contact>();
list<contact> conlist = new list<contact>();
list<RecordType> rrrecordtype= new list<RecordType>(); 
global Database.QueryLocator Start(Database.BatchableContext BC)
{
    String Query;
    date d=system.today();
    rrrecordtype = [select Id, Name,DeveloperName from RecordType where (DeveloperName ='NonInvestorRepContact' or DeveloperName='Registered_Investment_Advisor_RIA_Contact' or DeveloperName='Dually_Registered_Contact')and SobjectType = 'Contact'];
    Query = 'Select Id,Name,RecordTypeId,First_Producer_Date__c,location__c,Next_Planned_External_Appointment__c ,Next_External_Appointment__c ,Last_External_Appointment__c,Priority__c from Contact where RecordTypeId in : rrrecordtype and  Next_Planned_External_Appointment__c<:d'; 
    System.debug('My Query Is.......'+Query);
    return Database.getQueryLocator(Query);
 }
global void Execute(Database.BatchableContext BC,List<Contact> Scope)
{ 
       for(contact c:scope)
     {
                if(c.Priority__c=='1')
                {
                      c.Next_Planned_External_Appointment__c = system.today() + 30; 
                      c.location__c = null;  
                      ContactList.add(c);
                }
              
              else if(c.Priority__c=='2' || c.Priority__c=='5')
               {
                      c.Next_Planned_External_Appointment__c = system.today() + 90;
                      c.location__c = null;  
                       ContactList.add(c);
                      
               }
                else if(c.Priority__c=='3' || c.Priority__c=='4' || c.Priority__c=='6')
                {
                       
                             c.Next_Planned_External_Appointment__c = system.today() + 120;
                             c.location__c = null;  
                              ContactList.add(c);
                                      
               }
             
      }    
      update contactlist;      

}
 global void finish(Database.BatchableContext BC)
       {
       
       }
       


}