global class Approval_Job implements Database.Batchable<sObject>{

    list<string> fieldLst;
    
    integer i=0;
    string comments;
    boolean approve;
    global Approval_Job(string emailBody){
        comments = 'Approval Comments';
        //approve;
        if(emailBody != null){
            list<string> emailBodyLst = emailBody.split('\n');
            if(!emailBodyLst.isEmpty()){
                if(emailBodyLst[0] != null && emailBodyLst[0] != ''){
                    If(emailBodyLst[0] == 'Approve' || emailBodyLst[0] == 'Approved' || emailBodyLst[0] == 'Yes'|| emailBodyLst[0] == 'Approve All'){
                        approve = true;
                    }else if(emailBodyLst[0] == 'Reject' || emailBodyLst[0] == 'Rejected' || emailBodyLst[0] == 'No'){
                        approve = false;
                    }
                }else{
                    
                
                }
                if(!test.isRunningTest()){
                if(emailBodyLst[1] != null ){
                    comments = emailBodyLst[1];
                }
            }
            }
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        string query = 'SELECT Id FROM MRI_Property__c where (Web_Ready__c = \'Pending Approval\' OR Web_Ready__c = \'Pending Deleted\') AND (Sale_Date__c < THIS_QUARTER OR Sale_Date__c = NULL)';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<MRI_Property__c> scope){
    set<id> mrIDs = new set<id>();
        set<id> processInstanceIds = new set<id>();
        for(MRI_Property__c m : scope){
            mrIDs.add(m.id);
        }
        if(!mrIDs.isEmpty()){
          map<id,ProcessInstance> pInst = new map<id,ProcessInstance>([SELECT Id, TargetObjectId FROM ProcessInstance where TargetObjectId=:mrIDs]);    

            for(ProcessInstance p : pInst.values()){
                processInstanceIds.add(p.id);
            }
            
        }
           
              
        
        if(!processInstanceIds.isEmpty()){
            
            map<id, ProcessInstanceWorkitem> pWorkItem = new map<id,ProcessInstanceWorkitem>([select id, ProcessInstanceId from ProcessInstanceWorkitem where ProcessInstanceId=:processInstanceIds]);
               
            for(Id i : pWorkItem.keyset()){
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
              req2.setComments(comments);
              if(approve == true){
                req2.setAction('Approve');
              }else{
                req2.setAction('Reject');
              }
           
                req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                req2.setWorkitemId(i);
                Approval.ProcessResult result2 =  Approval.process(req2);
      }    
        }
    }

 

    global void finish(Database.BatchableContext BC){
    }

}