@isTest()
  private class BatchcontactInvestorAttachment_Test { 
 private class Mock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse res = new HTTPResponse();
            res.setStatusCode(200);
            if (req.getEndpoint().endsWith('apikey') ){
                // Return a fake API_Key in the response
                res.setBody('dummy');
            } else if (req.getEndpoint().endsWith('apirequest') ){
                // Mock the response if necessary
                res.setBody('dummy');
            }
            return res;
        }
    }    
static testMethod void BatchcontactInvestorAttachment_TestMethod (){
Profile prof = [select id from profile where name='system Administrator'];
     User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
   Account accRec = new Account(name='testName', Ownerid = usr.id,Type='Banker');
   insert accRec ;
  contact conobj = new contact();
  conobj.LastName ='Test Conatct';
  conobj.Relationship__c = 'Prospect';
  conobj.Contact_Type__c = 'Cole';
  conobj.Territory__c = 'West TX';
  conobj.RIA_Territory__c = 'West Region';
  conobj.AccountId = accRec.id;
   insert conobj;
   System.assertEquals(conobj.LastName,'Test Conatct');
  Contact_InvestorList__c coninv = new Contact_InvestorList__c(name='testName', Pdoc__c=true,Attachment_Id__c='',Advisor_Name__c=conobj.id);
   insert coninv ;
    Test.setMock(HttpCalloutMock.class, new Mock());
   Test.startTest();
       BatchcontactInvestorAttachment batchconinv = new BatchcontactInvestorAttachment();
       ID batchprocessid = Database.executeBatch(batchconinv,1);
       Test.stopTest();
  }
}