@isTest
private class REITInvestmentTriggersTest {
    public static testMethod void testCalculateGradeInserts() {
        // insert 200 contacts
        List<Contact> contacts = new List<Contact>();
        for (Integer i=0; i<100; i++) {
            Contact c = new Contact();
            c.FirstName = 'Test';
            c.LastName = 'Test-'+i;
            contacts.add(c);
        }
            helper.run = FALSE;
        insert contacts;
     System.assertNotEquals(contacts.size(),50);

         
        // insert 1000 investments
        List<REIT_Investment__c> investments = new List<REIT_Investment__c>();
        for (Integer i=0; i<200; i++) {
            REIT_Investment__c ri = new REIT_Investment__c();
            ri.Rep_Contact__c = contacts.get(Math.mod(i, contacts.size())).Id;
            ri.Admit_Date__c = Date.today();
            //System.debug(ri);
            investments.add(ri);
        }
        Test.startTest();
        insert investments;
        Test.stopTest();

        // check Tickets_Current_Month__c values

        // delete 100 investments
        List<REIT_Investment__c> investments2 = new List<REIT_Investment__c>();
        for (Integer i=0; i<100; i++) {
            investments2.add(investments.get(i));
        }
        delete investments2;
        
        // check Tickets_Current_Month__c
    }
}