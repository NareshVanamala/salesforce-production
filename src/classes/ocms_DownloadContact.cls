public without sharing class ocms_DownloadContact {

    public String firstName {get;set;}
    public String lastName {get;set;}
    public String title {get;set;}
    public String department {get;set;}
    public String phone {get;set;}
    public String mobilePhone {get;set;}
    public String email {get;set;}
    public String city {get;set;}
    public String state {get;set;}
    public String street {get;set;}
    public String postalCode {get;set;}
    public String country {get;set;}

    public ocms_DownloadContact() {

        String userId = System.currentPageReference().getParameters().get('id');
        if(userId != null && userId != ''){

            //contact user = [SELECT FirstName, LastName, Title, Department,Custom_Mobile_Phone__c, CustomPhone__c, CustomEmail__c, MailingCity, MailingState, MailingStreet, MailingPostalCode, MailingCountry FROM contact WHERE Id=:userId LIMIT 1];
             user us = [SELECT FirstName,LastName,Title,Custom_Mobile_Phone__c,CustomPhone__c,CustomEmail__c,City,State,Street,PostalCode,Country,Department FROM user WHERE Id=:userId LIMIT 1];
           /* firstName = user.FirstName;
            lastName = user.LastName;
            title = user.Title;
            department = user.Department;
            phone = user.CustomPhone__c;
            mobilePhone = user.Custom_Mobile_Phone__c;
            email = user.CustomEmail__c;
            city = user.MailingCity;
            state = user.MailingState;
            street = user.MailingStreet;
            postalCode = user.MailingPostalCode;
            country = user.MailingCountry;*/
            firstName = us.FirstName;
            lastName = us.LastName;
            title = us.Title;
            department = us.Department;
            phone = us.CustomPhone__c;
            mobilePhone = us.Custom_Mobile_Phone__c;
            email = us.CustomEmail__c;
            city = us.City;
            state = us.State;
            street = us.Street;
            postalCode = us.PostalCode;
            country = us.Country;
            
            ApexPages.currentPage().getHeaders().put('Content-Disposition', 'attachment; filename=' + firstName + lastName + '.vcf');

        } else {

            ApexPages.currentPage().getHeaders().put('Content-Disposition', 'attachment; filename=blank.vcf');

        }
        
    }
}