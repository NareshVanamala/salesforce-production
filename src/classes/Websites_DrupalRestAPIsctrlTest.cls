@isTest(SeeAllData=true)
private class Websites_DrupalRestAPIsctrlTest{

	private class Mock implements HttpCalloutMock{
        String body='{"status": "SUCCESS"}';
        Integer status=200;

        public HTTPResponse respond(HTTPRequest req){
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(status);
            res.setBody(body);
            
            return res;
        }
    }

	@isTest
	static void VEREITPressReleasesTest(){
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new Mock());
		Websites_DrupalRestAPIsctrl cont= new Websites_DrupalRestAPIsctrl();
		cont.VEREITPressReleases();
		System.debug(logginglevel.info, '--------- cont.vpr_status: '+cont.vpr_status);
		System.assertEquals(true, cont.vpr_status.contains('success'));

		Test.stopTest();
	}

	@isTest
	static void VEREITPropertiesTest(){
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new Mock());
		Websites_DrupalRestAPIsctrl cont= new Websites_DrupalRestAPIsctrl();
		cont.VEREITProperties();
		System.debug(logginglevel.info, '--------- cont.vp_status: '+cont.vp_status);
		System.assertEquals(true, cont.vp_status.contains('success'));

		Test.stopTest();
	}

	@isTest
	static void ColeCapitalPropertiesTest(){
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new Mock());
		Websites_DrupalRestAPIsctrl cont= new Websites_DrupalRestAPIsctrl();
		cont.ColeCapitalProperties();
		System.debug(logginglevel.info, '--------- cont.ccp_status: '+cont.ccp_status);
		System.assertEquals(true, cont.ccp_status.contains('success'));

		Test.stopTest();
	}

	@isTest
	static void ColeCapitalContactsTest(){
		Test.startTest();
		//Test.setMock(HttpCalloutMock.class, new Mock());
		Websites_DrupalRestAPIsctrl cont= new Websites_DrupalRestAPIsctrl();
		cont.ColeCapitalContacts();
		System.debug(logginglevel.info, '--------- cont.ccc_status: '+cont.ccc_status);
		System.assertEquals(true, cont.ccc_status.contains('Processing'));

		Test.stopTest();
	}	


}