global with sharing class MassApplyActionPlanforAlldeals implements Database.Batchable<SObject>
{
   global String Query;
   global String portfolioid;
   global list<Template__c >insrtlist;
   global String currentactionplan;
   global string dealidimp;
   global String portfolioname;
   
   global MassApplyActionPlanforAlldeals (ID dealid, id ActionPlanid )  
   {  
   
        Deal__c currentdeal=[select id,Portfolio_Deal__c,Portfolio_Deal__r.name from Deal__c where id=:dealid];
        system.debug('check my deal'+currentdeal);
        currentactionplan=ActionPlanid ;
       
        dealidimp=dealid;
        portfolioid=currentdeal.Portfolio_Deal__c;
       
        portfolioname=currentdeal.Portfolio_Deal__r.name;
        if((currentdeal!=null)&&(currentdeal.Portfolio_Deal__c!=null))
        Query='select Estimated_COE_Date__c,Accenture_Due_Date__c,AcquisitionDepartment__c,Acquisition_Contact__c,Acquisition_Fee__c,Additional_Deposit__c,Address_Line_1__c,Address__c,Appraisal_Date__c,Appraisal__c,Appraised_Value__c,Assumable_Debt__c,Attorney__c,Audit_Notes__c,Average_Income__c,Average_NOI__c,Bond_Amount__c,Broker_Memo__c,Building_Name__c,Buyer_s_Broker__c,CAP_Rate__c,Case_Sensitive_ID__c,CD__c,Center_Name__c,City__c,Class__c,Closing_Date_Report__c,Closing_Date__c,Closing_Period__c,Closing_Probability__c,Cole_Initials__c,Commission__c,Contract_Buyer__c,Contract_Date_Progress__c,Contract_Date__c,Contract_Price__c,CreatedById,CreatedDate,Created_Date__c,CUSIP__c,Dark_Value__c,Date_Audit_Completed__c,Date_Identified__c,Days_Elapsed_Signed__c,Days_Elapsed__c,Dead_Date__c,Dead_Deal_Comments__c,Dead_Reasons__c,Dead_Reason__c,DealStatusHierarchy__c,Deal_Count__c,Deal_DT_ID__c,Deal_Progress__c,Deal_Status__c,Deal_Type__c,Debt__c,Docket_Comments__c,Entity__c,Equity__c,ESA__c,Estimated_Hard_Date__c,Estimated_SP_Date__c,Estoppel_Progress__c,Estoppel__c,Extension_Rights__c,EXT_Options__c,Federal_ID__c,Fund__c,GAAP_Cap__c,GAAP_NOI__c,General_Notes__c,GL_Entity_ID__c,Go_Hard_Date__c,Gross_Commission_Percent__c,Gross_Commission__c,Hard_Date_Progress__c,Hard_Date__c,Households__c,Id,Initial_5_yr_Fad__c,Initial_10_yr_Fad__c,Initial_Deposit__c,Initial_Yield__c,Insurable_Value__c,Investment_Committee_Approval_Progress__c,Investment_Committee_Approval__c,Investment_Type__c,IsDeleted,Land_Acres__c,Land_Value__c,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Latitude__c,Lease_Abstract_By__c,Lease_Abstract_Date__c,Lease_Abstract_Notes__c,Lease_Expiration1_c__c,Lease_Expiration__c,Lease_Term__c,Legal_Deal_Type__c,Legal_Department__c,Legal_Secretary__c,Legal_Sec_Email__c,Legal_Sec_Fax__c,Legal_Sec_Phone__c,Lender__c,Loan_Term_All_Cash__c,LOI_Sent_Date__c,LOI_Signed_Date__c,LOI_Signed_Progress__c,Longitude__c,Median_Income__c,MRI_Property_Type_picklist__c,Name,nd_Estimated_SP_Date__c,nd_SP_End_Date__c,NOI_Average__c,NOI_Going_In__c,Number_Of_Properties__c,Occupancy__c,Occupant_Name__c,Old_DC_External_ID__c,Open_Escrow_Date__c,Operations_Analyst__c,Outside_Counsel_Contact__c,Overall_CAP_Rate__c,Overall_Yield_IRR__c,Owned_Id__c,OwnerId,Ownership_Type__c,Ownership__c,Paralegal__c,Parking_Ratio__c,Parking_Spaces__c,Part_1_Start_Date__c,Passed_Comments__c,Passed_Date__c,Passed_Reason__c,PCR__c,Pipeline_Id__c,PM_Docket__c,Population__c,Portfolio_Deal__c,Preparer__c,Pre_Contract_Cap_Rate__c,Pre_Contract_NOI__c,Pre_Contract_Price__c,Price_PSF_Contract_Price__c,Price_PSF_Total_Price__c,Primary_Use__c,Property_Accountant__c,Property_Manager__c,Property_Owner_Company__c,Property_Owner_Contact__c,Property_Phone__c,Property_Type__c,Purchase_Date__c,Purchase_Price__c,Query_Sorted_Date__c,Range__c,Recorded_Deed__c,RecordTypeId,Region__c,Rent_PSF__c,Report_Deal_Name__c,Report_Hard_Date__c,Rep_Burn_Off__c,Reviewer__c,RR_ExpenseRecoveries__c,RR_LeaseExpiration__c,RR_Reimb_Type__c,RR_RemainingTerm__c,RR_Rent_Bump__c,RR_SPRating__c,Seller_Address__c,Seller_City_State_Zip__c,Seller_E_mail__c,Seller_Fax__c,Seller_Memo__c,Seller_Phone__c,Seller_SPE__c,Seller_s_Broker_Address__c,Seller_s_Broker_City_State_Zip__c,Seller_s_Broker_Company__c,Seller_s_Broker_E_mail__c,Seller_s_Broker_Fax__c,Seller_s_Broker_Phone__c,Seller_s_Broker__c,Seller_s_Counsel_Attorney__c,Seller_s_Counsel_Firm__c,Share_Amount__c,Shop_SF__c,Site_Visit_By__c,Site_Visit_Date__c,Site_Visit_Notes__c,SLB_Rem_Term__c,Source_Company__c,Source_Contact__c,Source_Notes__c,Source_Type__c,Source__c,SPE__c,State__c,Strategy__c,Study_Period__c,Surveys__c,SystemModstamp,Tenancy__c,Tenant_Count__c,Tenant_ROFR_Waiver_Progress__c,Tenant_ROFR_Waiver__c,Tenant_s__c,Title_Company__c,Title_Policy__c,Total_Deposit__c,Total_Investment__c,Total_Price__c,Total_SF__c,Underwriter_Contact__c,Underwriter_Preparer__c,Underwriter_Reviewer__c,Vacant_SF__c,Year_1_NOI__c,Year_Built__c,Year_Renovated__c,Zip_Code__c from Deal__c WHERE  Portfolio_Deal__c =:portfolioid and id!=:dealidimp';  
         //return database.getQueryLocator(query);   
      
     }
    global Database.QueryLocator start(Database.BatchableContext BC)  
    {  
      system.debug('This is start method');
      system.debug('Myquery is......'+Query);
      return Database.getQueryLocator(Query);  
    }  
  
    global void execute(Database.BatchableContext BC,List<Deal__c> scope)  
    {  
         insrtlist= new list<Template__c >();
         map<id,Template__c>dealActionplanmap= new map<id,Template__c>();
         list<Task>tasklist=new list<task>();
         system.debug('Can i get the actionplan id'+currentactionplan);
         Template__c actionPlan=[select id,Mass_Assign_To__c,Template__c,name,Send_Email__c from Template__c  where id=:currentactionplan];
         list<Task_Plan__c>taskplanlist=[select Action_Plan_Name__c,Assigned_To__c,Comments__c,CreatedById,CreatedDate,Create_Task__c,Date_Completed__c,Date_Needed__c,Date_Ordered__c,Date_Received__c,Deal_Name__c,External_Id__c,Field_to_update_Date_Received__c,Field_to_update_for_date_needed__c,Id,Index__c,IsDeleted,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Loan_Name__c,Name,Notify_Emails__c,Parent_Index__c,Parent__c,Position__c,Priority__c,Query_Param__c,Remainder__c,Status__c,SystemModstamp,TaskRelatedToloan__c,TaskRelatedto__c,Template__c from Task_Plan__c where Template__c=:currentactionplan and Parent__c=null];
         list<Deal__c> updatedlist=new list<Deal__c>();
         list<Task_Plan__c>insertTaskplanlist=new list<Task_Plan__c>(); 
         system.debug('Check the action plan'+actionPlan);
         list<Task_Plan__c>subtaskplanlist=[select Parent__r.Index__c,Action_Plan_Name__c,Assigned_To__c,Comments__c,CreatedById,CreatedDate,Create_Task__c,Date_Completed__c,Date_Needed__c,Date_Ordered__c,Date_Received__c,Deal_Name__c,External_Id__c,Field_to_update_Date_Received__c,Field_to_update_for_date_needed__c,Id,Index__c,IsDeleted,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Loan_Name__c,Name,Notify_Emails__c,Parent_Index__c,Parent__c,Position__c,Priority__c,Query_Param__c,Remainder__c,Status__c,SystemModstamp,TaskRelatedToloan__c,TaskRelatedto__c,Template__c from Task_Plan__c where Template__c=:currentactionplan and Parent__c!=null];
         list<Task_Plan__c>inertsubctlist=new list<Task_Plan__c>();
        
         map<decimal,Task_Plan__c>TaskplanMap= new map<decimal,Task_Plan__c>();
         //insert the action plan for all deals.
         for(Deal__c currentdeal : scope)  
        { 
          string namedeal= currentdeal.name; 
          Template__c actionPlan1= new Template__c ();
          if(namedeal.length()>60)
          actionPlan1.name='Action Plan –'+ namedeal.substring(0,60) ;
          else
          actionPlan1.name='Action Plan –'+ namedeal;
          actionPlan1.Send_Email__c=actionPlan.Send_Email__c;
          actionPlan1.Mass_Assign_To__c=actionPlan.Mass_Assign_To__c;
          actionPlan1.Deal__c=currentdeal.id;
          actionPlan1.Template__c =actionPlan.Template__c;
          insrtlist.add(actionPlan1);
        }
         if (Schema.sObjectType.Template__c.isCreateable())

         insert insrtlist;
        //form the map.
        for(Template__c ct:insrtlist)
          dealActionplanmap.put(ct.Deal__c,ct); 
          
          if(taskplanlist.size()>0)
          {
              
               for(Deal__c currentdeal : scope)
               {
                   for(Task_Plan__c T:taskplanlist)    
                   {
                       Task_Plan__c ct= new Task_Plan__c();
                       ct.create_task__c=true;
                       ct.Assigned_TO__c =T.Assigned_TO__c; 
                       ct.Template__c =dealActionplanmap.get(currentdeal.id).id;
                       ct.name=T.name;
                       ct.Date_Needed__c=T.Date_Needed__c;
                       ct.Date_Ordered__c=T.Date_Ordered__c;
                       ct.Date_Received__c=T.Date_Received__c;
                       ct.Status__c=T.Status__c;
                       ct.Index__c =T.Index__c ;
                       ct.Field_to_update_Date_Received__c = T.Field_to_update_Date_Received__c;
                       ct.Field_to_update_for_date_needed__c= T.Field_to_update_for_date_needed__c ;
                       ct.Notify_Emails__c =T.Notify_Emails__c ;
                       ct.Priority__c=T.Priority__c;
                       ct.Comments__c=T.Comments__c;
                       ct.Date_Completed__c=T.Date_Completed__c;
                     //this code is for updating the deal field 
                       If(T.Date_Received__c!=null && T.Field_to_update_Date_Received__c!=null) 
                     { 
                         if(T.Field_to_update_Date_Received__c=='Site_Visit_Date__c')
                          currentdeal.Site_Visit_Date__c=T.Date_Received__c; 
                         If(T.Field_to_update_Date_Received__c=='Accenture_Due_Date__c')
                          currentdeal.Accenture_Due_Date__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='Go-Hard Date')
                          currentdeal.Go_Hard_Date__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='Rep_Burn_Off__c')
                          currentdeal.Rep_Burn_Off__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='Passed_Date__c')
                          currentdeal.Passed_Date__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='Estimated_COE_Date__c')
                          currentdeal.Estimated_COE_Date__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='Hard_Date__c')
                          currentdeal.Hard_Date__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='Estimated_SP_Date__c')
                          currentdeal.Estimated_SP_Date__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='Purchase_Date__c')
                            currentdeal.Purchase_Date__c=T.Date_Received__c;
                        if(T.Field_to_update_Date_Received__c=='Estimated_Hard_Date__c')
                         currentdeal.Estimated_Hard_Date__c=T.Date_Received__c;  
                        if(T.Field_to_update_Date_Received__c=='Contract_Date__c')
                         currentdeal.Contract_Date__c=T.Date_Received__c;
                        if(T.Field_to_update_Date_Received__c=='Closing_Date__c')
                         currentdeal.Closing_Date__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='LOI_Signed_Date__c')
                         currentdeal.LOI_Signed_Date__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='LOI_Sent_Date__c')
                         currentdeal.LOI_Sent_Date__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='Lease_Abstract_Date__c')
                         currentdeal.Lease_Abstract_Date__c=T.Date_Received__c; 
                         // if(T.Field_to_update_Date_Received__c=='Lease_Expiration__c')
                         //currentdeal.Lease_Expiration__c=T.Date_Received__c;
                          if(T.Field_to_update_Date_Received__c=='Open_Escrow_Date__c')
                         currentdeal.Open_Escrow_Date__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='Purchase_Date__c')
                         currentdeal.Purchase_Date__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='Tenant_ROFR_Waiver__c')
                         currentdeal.Tenant_ROFR_Waiver__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='Date_Audit_Completed__c')
                         currentdeal.Date_Audit_Completed__c=T.Date_Received__c; 
                         if(T.Field_to_update_Date_Received__c=='Date_Identified__c')
                         currentdeal.Date_Identified__c=T.Date_Received__c; 
                         if(T.Field_to_update_Date_Received__c=='Investment_Committee_Approval__c')
                         currentdeal.Investment_Committee_Approval__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='Part_1_Start_Date__c')
                         currentdeal.Part_1_Start_Date__c=T.Date_Received__c;
                         if(T.Field_to_update_Date_Received__c=='Estoppel__c')
                         currentdeal.Estoppel__c=T.Date_Received__c;  
                          if(T.Field_to_update_Date_Received__c=='nd_Estimated_SP_Date__c')
                         currentdeal.nd_Estimated_SP_Date__c=T.Date_Received__c; 
                          if(T.Field_to_update_Date_Received__c=='nd_SP_End_Date__c')
                         currentdeal.nd_SP_End_Date__c=T.Date_Received__c; 
                      } 
                        If(T.Date_Needed__c!=null && T.Field_to_update_for_date_needed__c!=null) 
                    { 
                        if(T.Field_to_update_for_date_needed__c=='Estimated_COE_Date__c')
                           currentdeal.Estimated_COE_Date__c=T.Date_Needed__c;   
                        if(T.Field_to_update_for_date_needed__c=='Dead_Date__c')
                         currentdeal.Dead_Date__c=T.Date_Needed__c;
                        if(T.Field_to_update_for_date_needed__c=='Appraisal_Date__c')
                         currentdeal.Appraisal_Date__c=T.Date_Needed__c;
                        If(T.Field_to_update_for_date_needed__c=='Accenture_Due_Date__c')
                         currentdeal.Accenture_Due_Date__c=T.Date_Needed__c;
                        if(T.Field_to_update_for_date_needed__c=='Go-Hard Date')
                         currentdeal.Go_Hard_Date__c=T.Date_Needed__c;
                        if(T.Field_to_update_for_date_needed__c=='Rep_Burn_Off__c')
                         currentdeal.Rep_Burn_Off__c=T.Date_Needed__c;
                        if(T.Field_to_update_for_date_needed__c=='Site_Visit_Date__c')
                        currentdeal.Dead_Date__c=T.Date_Needed__c;
                       // if(T.Field_to_update_for_date_needed__c=='Query_Sorted_Date__c')
                        // currentdeal.Query_Sorted_Date__c=T.Rep_Burn_Off__c;
                        if(T.Field_to_update_for_date_needed__c=='Passed_Date__c')
                         currentdeal.Passed_Date__c=T.Date_Needed__c;
                        if(T.Field_to_update_for_date_needed__c=='Hard_Date__c')
                         currentdeal.Hard_Date__c=T.Date_Needed__c;
                        if(T.Field_to_update_for_date_needed__c=='Estimated_SP_Date__c')
                         currentdeal.Estimated_SP_Date__c=T.Date_Needed__c;
                        if(T.Field_to_update_for_date_needed__c=='Purchase_Date__c')
                         currentdeal.Purchase_Date__c=T.Date_Needed__c;
                        if(T.Field_to_update_for_date_needed__c=='Estimated_Hard_Date__c')
                         currentdeal.Estimated_Hard_Date__c=T.Date_Needed__c;  
                        if(T.Field_to_update_for_date_needed__c=='Contract_Date__c')
                         currentdeal.Contract_Date__c=T.Date_Needed__c;
                        if(T.Field_to_update_for_date_needed__c=='Closing_Date__c')
                         currentdeal.Closing_Date__c=T.Date_Needed__c;
                         if(T.Field_to_update_for_date_needed__c=='LOI_Signed_Date__c')
                         currentdeal.LOI_Signed_Date__c=T.Date_Needed__c;
                         if(T.Field_to_update_for_date_needed__c=='LOI_Sent_Date__c')
                         currentdeal.LOI_Sent_Date__c=T.Date_Needed__c;
                         if(T.Field_to_update_for_date_needed__c=='Lease_Abstract_Date__c')
                         currentdeal.Lease_Abstract_Date__c=T.Date_Needed__c;  
                         // if(T.Field_to_update_for_date_needed__c=='Lease_Expiration__c')
                         //currentdeal.Lease_Expiration__c=T.Date_Needed__c;
                          if(T.Field_to_update_for_date_needed__c=='Open_Escrow_Date__c')
                         currentdeal.Open_Escrow_Date__c=T.Date_Needed__c;
                         if(T.Field_to_update_for_date_needed__c=='Purchase_Date__c')
                         currentdeal.Purchase_Date__c=T.Date_Needed__c;
                         if(T.Field_to_update_for_date_needed__c=='Tenant_ROFR_Waiver__c')
                         currentdeal.Tenant_ROFR_Waiver__c=T.Date_Needed__c;
                         if(T.Field_to_update_for_date_needed__c=='Date_Audit_Completed__c')
                         currentdeal.Date_Audit_Completed__c=T.Date_Needed__c; 
                         if(T.Field_to_update_for_date_needed__c=='Date_Identified__c')
                         currentdeal.Date_Identified__c=T.Date_Needed__c; 
                         if(T.Field_to_update_for_date_needed__c=='Investment_Committee_Approval__c')
                         currentdeal.Investment_Committee_Approval__c=T.Date_Needed__c;
                         if(T.Field_to_update_for_date_needed__c=='Part_1_Start_Date__c')
                         currentdeal.Part_1_Start_Date__c=T.Date_Needed__c;
                         if(T.Field_to_update_for_date_needed__c=='Estoppel__c')
                         currentdeal.Estoppel__c=T.Date_Needed__c; 
                         if(T.Field_to_update_for_date_needed__c=='nd_Estimated_SP_Date__c')
                         currentdeal.nd_Estimated_SP_Date__c=T.Date_Needed__c;
                         if(T.Field_to_update_for_date_needed__c=='nd_SP_End_Date__c')
                         currentdeal.nd_SP_End_Date__c=T.Date_Needed__c;
                    }
                    insertTaskplanlist.add(ct);
                                   
                   }
                   updatedlist.add(currentdeal);  
                  }
               }
                  if (Schema.sObjectType.Task_Plan__c.isCreateable())

                  insert insertTaskplanlist;
                  
                  if (Schema.sObjectType.Deal__c.isUpdateable()) 

                  update updatedlist;
                 for(Task_Plan__c ct1:insertTaskplanlist)  
                  TaskplanMap.put(ct1.Index__c,ct1);    
               
                for(Task_Plan__c ctask:subtaskplanlist)
                {
                
                   Task_Plan__c subtask1= new Task_Plan__c();
                   subtask1.Parent__c=TaskplanMap.get(ctask.Parent__r.Index__c).id;
                   subtask1.name=ctask.name;
                   subtask1.Template__c = ctask.Template__c;
                   subtask1.Assigned_To__c=ctask.Assigned_To__c;
                   subtask1.Index__c = ctask.Index__c ;
                   subtask1.create_task__c=ctask.create_task__c; 
                   inertsubctlist.add(subtask1);
                              
                } 
                 if (Schema.sObjectType.Task_Plan__c.isCreateable())

                 insert inertsubctlist;
                 
             /*for(Task_Plan__c ct:insertTaskplanlist)
            {
               Task t= new Task();
                t.subject = ct.Name;
                system.debug('Give me the Name of record'+ct.Name);
                if(ct.Assigned_To__c!=null)t.ownerid=ct.Assigned_To__c;
                if(ct.Comments__c!=null)t.Description=ct.Comments__c;
                if(ct.Date_Completed__c!=null)t.Date_Completed__c=ct.Date_Completed__c;
                if(ct.Date_Needed__c!=null)t.Date_Needed__c=ct.Date_Needed__c;
                if(ct.Date_Received__c!=null)t.Date_Received__c=ct.Date_Received__c;
                if(ct.Date_Ordered__c!=null)t.Date_Ordered__c=ct.Date_Ordered__c;
                if(ct.Date_Needed__c!=null)t.ActivityDate = ct.Date_Needed__c;
                if(ct.Priority__c!=null)t.Priority=ct.Priority__c; 
                if(ct.Status__c!=null){t.Status=ct.Status__c;}
                if(ct.TaskRelatedto__c!=null)
                {
                    t.whatid=ct.TaskRelatedto__c;
                }
                String RecordId = ct.id;
                t.Action_Plan_Task__c= RecordId.substring(0,15);
                tasklist.add(t);
            
            }
            for(Task_Plan__c ct:inertsubctlist)
            {
               Task t= new Task();
                t.subject = ct.Name;
                system.debug('Give me the Name of record'+ct.Name);
                if(ct.Assigned_To__c!=null)t.ownerid=ct.Assigned_To__c;
                if(ct.Comments__c!=null)t.Description=ct.Comments__c;
                if(ct.Date_Completed__c!=null)t.Date_Completed__c=ct.Date_Completed__c;
                if(ct.Date_Needed__c!=null)t.Date_Needed__c=ct.Date_Needed__c;
                if(ct.Date_Received__c!=null)t.Date_Received__c=ct.Date_Received__c;
                if(ct.Date_Ordered__c!=null)t.Date_Ordered__c=ct.Date_Ordered__c;
                if(ct.Date_Needed__c!=null)t.ActivityDate = ct.Date_Needed__c;
                if(ct.Priority__c!=null)t.Priority=ct.Priority__c; 
                if(ct.Status__c!=null){t.Status=ct.Status__c;}
                if(ct.TaskRelatedto__c!=null)
                {
                    t.whatid=ct.TaskRelatedto__c;
                }
                String RecordId = ct.id;
                t.Action_Plan_Task__c= RecordId.substring(0,15);
                tasklist.add(t);
            
            }
            
            insert tasklist;*/
            
       }

        global void finish(Database.BatchableContext BC)  
        { 
           String samp = System.Label.Current_Org_Url;
          user u=[select id,name,email from user where id=:userinfo.getuserid()];
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          String[] toAddresses = new String[] {u.email};
          String[] bccAddresses = new String[] {'skalamkar@colecapital.com'};
           mail.setToAddresses(toAddresses);
           mail.setBccSender(true);
           mail.setBccAddresses(bccAddresses);
           mail.setPlainTextBody('Please verify the Action Plan has been applied to your deals.'+'\n'+'Please click below to view the portfolio'+'\n'+''+samp+'/'+ portfolioid );
           mail.setSubject('Action plans have been applied to all deals of ' + portfolioname  );
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                
    
        }
}