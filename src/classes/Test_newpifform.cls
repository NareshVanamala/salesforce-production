@isTest
private class Test_newpifform 
{
   public static testMethod void Testfornewpifform() 
   {        
        Id c1 =[Select id,name,DeveloperName from RecordType where DeveloperName='Parent' and SobjectType = 'Project_Initiation__c'].id; 
        Id urlid=null;
              
        string radio6='yes';
        Project_Initiation__c p2 = new Project_Initiation__c();
        p2.Name = 'Testing sandy';
        p2.Resource_Name_Description__c = 'testing';
        p2.Estimated_Time_Commitment__c='100';
        p2.recordtypeid=c1;
        p2.PMO_Group_Status__c= 'Submitted For Approval';
        p2.ARCP_Integration_Project__c=True;
        p2.Was_the_Project_Included_in_the_Original__c=True;
        p2.Projecr_Requires_Compliance_Approval__c=True;
        try{
        Insert p2;
        }
        catch( Exception e){ 
        }
        system.assertequals(p2.Resource_Name_Description__c,'testing');
        Id urlid1=p2.id;
        ApexPages.StandardController controller0 = new ApexPages.StandardController(p2);
               pifcontroller sc0 = new pifcontroller(controller0);
        pagereference pageref11 = system.Currentpagereference();
    pageref11.getParameters().put('id',p2.id); 

        
        string radio='yes';
        string radio1='yes';
        string radio2='yes';
        
        Project_Initiation__c p = new Project_Initiation__c();
        p.Name = 'Testing sandy';
        p.Resource_Name_Description__c = 'testing';
        p.Estimated_Time_Commitment__c='100';
        p.recordtypeid=c1;
        p.PMO_Group_Status__c= 'Submitted For Approval';
        p.ARCP_Integration_Project__c=True;
        p.Was_the_Project_Included_in_the_Original__c=True;
        p.Projecr_Requires_Compliance_Approval__c=True;
        try{
        Insert p;
        }
        catch( Exception e){ 
        }
        system.assertequals(p.PMO_Group_Status__c,'Submitted For Approval');

        Id urlid2=p.id;
   pagereference pageref1 = system.Currentpagereference();
    pageref1.getParameters().put('id',p.id); 
    
      
    ApexPages.StandardController controller = new ApexPages.StandardController(p);
        pifcontroller sc = new pifcontroller(controller);
      
       
     Attachment a  = new Attachment();
        a.Name='Unit Test 1';
        a.Description='Unit Test 1';
        a.Body=Blob.valueOf('Unit Test 1');
        a.parentid = p.id;
        try{
        Insert a;
        }
        catch( Exception e){ 
        }
       system.assertequals(a.parentid, p.id);

       sc.getattachments();
       sc.saveandsubmit();
       sc.submit();
       sc.getItems();
       sc.getItems1();
       sc.getItems2(); 
       
        string radio3='No';
        string radio4='No';
        string radio5='No';
        
        Project_Initiation__c p1 = new Project_Initiation__c();
        p1.Name = 'Testing sandy';
        p1.Resource_Name_Description__c = 'testing';
        p1.Estimated_Time_Commitment__c='100';
        p1.recordtypeid=c1;
        p1.PMO_Group_Status__c= 'Submitted For Approval';
        p1.ARCP_Integration_Project__c=False;
        p1.Was_the_Project_Included_in_the_Original__c=False;
        p1.Projecr_Requires_Compliance_Approval__c=False;
        try{
        Insert p1;
        }
        catch( Exception e){ 
        }    
        Id urlid3=p1.id;
  pagereference pageref = system.Currentpagereference();
    pageref.getParameters().put('id',p1.id); 
    
    ApexPages.StandardController controller1 = new ApexPages.StandardController(p1);
        pifcontroller sc1 = new pifcontroller(controller1);
      
       
     Attachment a1  = new Attachment();
        a1.Name='Unit Test 1';
        a1.Description='Unit Test 1';
        a1.Body=Blob.valueOf('Unit Test 1');
        a1.parentid = p1.id;
        try{
        Insert a1;
        }
        catch( Exception e){ 
        }
       
        
       sc1.getattachments();
       sc1.saveandsubmit();
       sc1.submit();
       sc1.getItems();
       sc1.getItems1();
       sc1.getItems2(); 
       
       
      
   }
}