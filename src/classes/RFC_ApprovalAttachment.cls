public class RFC_ApprovalAttachment{
 
public string Jobid;
public string attid;
public Map<String,String> urlMap=new Map<String,String>();
public List<Attachment> attach;

public string getJobid() {
return null;
}
public void setJobid(String s) {
Jobid=s;
}

public Map<String,String> getattach() {
attach=[Select id,name From Attachment Where Parentid=:Jobid and (name like '%PIF%' or name like '%RFC%') order by createddate desc limit 1 ];

for(Attachment att1:attach) {
String urls=URL.getSalesforceBaseURL().toExternalForm()+'/servlet/servlet.FileDownload?file='+att1.id ;
urlMap.put(urls,att1.Name);
}
return urlMap;
}

public void setattach(List<Attachment> ata) {
attach=ata;
}

}