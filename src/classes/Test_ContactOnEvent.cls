@isTest
public class Test_ContactOnEvent
{
 static testmethod void ContactOnEventtest()
   {
       User u = [select firstname from user where id=:userinfo.getuserid()];
        
       Account a = new Account();
       a.name = 'Test';
       insert a;
        
     Contact c = new Contact();
       c.lastname = 'Cockpit';
       c.Accountid = a.id;
       c.Relationship__c ='Accounting';
       c.Contact_Type__c = 'Cole';
       c.Wholesaler_Management__c = 'Producer A';
       c.Territory__c ='Chicago';
       c.RIA_Territory__c ='Southeast Region';
       c.RIA_Consultant_Picklist__c ='Brian Mackin';
       c.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c.Wholesaler__c='Andrew Garten';
       c.Regional_Territory_Manager__c ='Andrew Garten';
       c.Internal_Wholesaler__c ='Aaron Williams';
       c.Territory_Zone__c = 'NV-50';
       c.Next_External_Appointment__c = null;
       c.Next_Planned_External_Appointment__c=null;
       //c.Snooze_Time__c = system.now();
       //c.snoozed__c = true;
       //c.Location__c = 'HYD';
       //c.Hours_Days_to_Snooze__c = '1';
       insert c;
       
       c.priority__c = '1';
       c.Location__c = 'MBNR';
       c.Next_External_Appointment__c = system.now();
       update c;
       
       system.assertequals(c.Accountid,a.id);

        
       Event e = new Event();
        e.OwnerId = u.Id;
        e.StartDate__c=date.today();
        e.EndDate__c=date.today()+1;
        e.whoid=c.id;
        e.whatid = a.id;
        e.DurationInMinutes=30;
        e.ActivityDateTime=system.now()+1;
        e.Subject = 'Ext Appt Event';
        e.Type ='Client Meeting';
        e.Appointment_Status__c = 'Completed';
        e.Description ='Testing CRMID:01pP00000005Nas';
        
       insert e;
       
       system.assertequals(e.whatid,a.id);

       
       EventRelation et = new EventRelation();
       
       et.relationid=c.id;
       et.EventId=e.id;
      insert et;
       
       e.EndDate__c=date.today()+2;
       update e;
       
       
   
   }

}