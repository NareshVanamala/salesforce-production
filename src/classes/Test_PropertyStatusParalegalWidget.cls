@isTest
public class Test_PropertyStatusParalegalWidget
 {
   public static testMethod void TestforPropertyStatusParalegalWidget () 
   {
    
     List<User> ulist = [select id, name,Profile.Name from User where Profile.Name = 'Deal Central - Legal Team' and IsActive = true];
     user u1 =[select id,name from user where id= :Userinfo.getuserid()];
     user u =[select id,name from user where id= :Userinfo.getuserid()];
     
     String ids = u.id+','+u1.id;
        Deal__c d1 = new Deal__c();
        d1.Name ='Test13';  
        d1.Deal_Status__c = 'Reviewing';
        d1.Contract_Price__c=100;
        d1.Fund__c = 'TBD';
        d1.Paralegal__c = u1.id;
        insert d1;
        
        System.assertEquals(d1.Paralegal__c,u1.id);

        
        ApexPages.StandardController controller = new ApexPages.StandardController(u1);  
        PropertyStatusParalegalWidget x = new PropertyStatusParalegalWidget(controller);
       
        x.getUserList();
        x.getusername1();
        x.setusername1(ids);
        x.RunReport();
        
         //ApexPages.currentPage().getParameters().put('paralegalIds',ids);
        //PageReference ref = new PageReference('/apex/PropertyStatusParalegal?paralegalIds='+ ulist[0].id);
        
   }
   
 }