@isTest
private class Test_UpdateDepartmentHead
{
   public static testMethod void TestUpdateDepartmentHead() 
   {     

    set<String>DepartmentNameSet= new set<String>();
    list<String>Departmentlist= new list<String>();
    list<String>Departmentlist1= new list<String>();
    
    Event_Request_Split__c ect= new Event_Request_Split__c();
    list<Event_Request_Split__c>insertlist=new list<Event_Request_Split__c>();
    list<Event_Request_Split__c>insertlist1=new list<Event_Request_Split__c>();
  
    if(Departmentlist.size()>0)
    {
    Departmentlist[0]='Accounting';
    Departmentlist[1]='Human Resources';
    Departmentlist[2]='26-Information Technology ';
    Departmentlist[3]='ISRG';
    Departmentlist[4]='Leasing';
    Departmentlist[4]='Legal';
    Departmentlist[5]='Property Management';
    Departmentlist[6]='Real Estate Acquisitions ';
    Departmentlist[7]='Real Estate Finance ';
    Departmentlist[8]='Sales Strategy and Analyt';
    Departmentlist[9]='Underwriting ';
    Departmentlist[10]='National Accounts';
    Departmentlist[11]='05-Event Planning';
    Departmentlist[12]='40-External Sales ';
    Departmentlist[13]='45-Internal Sales ';
    Departmentlist[14]='Marketing and Sales Support';
    Departmentlist[15]='Operations and Service';
    Departmentlist[16]='Relationship Management ';
        }
    Event_Automation_Request__c ear1= new Event_Automation_Request__c();
     ear1.Start_Date__c=System.Today();
     ear1.name='BDE00111';
     insert ear1;
    
     
    for(String dept:Departmentlist)
    {
       Event_Request_Split__c ct= new Event_Request_Split__c();
       ct.Department_Split__c=dept;
       ct.Department_Split_Percentage__c=5;
       ct.Event_Automation_Request__c= ear1.id;
       insertlist.add(ct);
  
     }
     insert insertlist;
     
       Event_Request_Split__c ct1= new Event_Request_Split__c();
       ct1.Department_Split__c='Securities Legal and Compliance ';
       ct1.Department_Split_Percentage__c=20;
       ct1.Event_Automation_Request__c= ear1.id;
       insertlist1.add(ct1);
     
         system.assertequals(ct1.Event_Automation_Request__c,ear1.id);

     
       Event_Request_Split__c ct11= new Event_Request_Split__c();
       ct11.Region_Split__c='Central RIA';
       ct11.Split__c=100;
       ct11.Event_Automation_Request__c= ear1.id;
       insertlist1.add(ct11);
       
       Event_Request_Split__c ct111= new Event_Request_Split__c();
       ct111.Fund_Split__c='CCPT I';
       ct111.Fund_Split_Percentage__c=100;
       ct111.Event_Automation_Request__c= ear1.id;
       insertlist1.add(ct111);

        system.assertequals(ct111.Event_Automation_Request__c,ear1.id);

       

       insert insertlist1;

      
     
  
    }

}