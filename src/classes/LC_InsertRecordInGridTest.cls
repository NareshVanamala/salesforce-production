@isTest
Public class LC_InsertRecordInGridTest
{
    static testmethod void testunit()
    {
         test.starttest();
         Entity__c ent = new Entity__c();
         ent.name =' Test entity1';
         ent.Owned_ID__c = 'P001';
         insert ent;
         
         MRI_PROPERTY__c MRC= new MRI_PROPERTY__c();
         MRC.Property_ID__c='A1234';
         MRC.name='Test_MRI_Property';
         MRC.Enity_Id__c = 'P001';
         insert MRC; 
         
         System.assertNotEquals(ent.name, MRC.name);

         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU';
         mylease.Suite_Sqft__c=1234;
         mylease.Lease_ID__c='A046677';
         myLease.MRI_PROPERTY__c=MRC.id;
         insert mylease;
         
         LC_Helper_for_Grid__c mvc=new LC_Helper_for_Grid__c();
         mvc.name='Update Now';
         insert  mvc;
  
         System.assertNotEquals(myLease.name,mvc.name);

          LC_LeaseOpprtunity__c OLC=new LC_LeaseOpprtunity__c();
          OLC.name='OptionTypeCheckTest';
          OLC.Lease_Opportunity_Type__c='Lease - New';
          OLC.MRI_Property__c=MRC.id;
          OLC.Suite_Sqft__c=10;
          OLC.Base_Rent__c=null;
         
          insert OLC;
          
          String LOPPBaseRent=string.valueof(OLC.Base_Rent__c);
          //String LOPNaseRentPSF=string.valueof(OLC.Base_Rent_PSF__c);   
       
          LC_RentSchedule__c LRSC= new LC_RentSchedule__c();
          LRSC.Lease_Opportunity__c=OLC.id;
          LRSC.Income_Category_1__c='RNT';
          LRSC.Annual_PSF__c=100;
          insert LRSC;
      
          LC_RentSchedule__c LRSC0= new LC_RentSchedule__c();
          LRSC0.Lease_Opportunity__c=OLC.id;
          LRSC0.Annual_PSF__c=200;
          LRSC0.Income_Category_1__c='RNT';
          LRSC0.From_In_Months__c='1';
          LRSC0.To_In_Months__c='2';
         // insert LRSC0;
          
          
          LC_RentSchedule__c LRSC00= new LC_RentSchedule__c();
          LRSC00.Lease_Opportunity__c=OLC.id;
          LRSC00.Annual_PSF__c=250;
          LRSC00.Income_Category_1__c='RNT';
          LRSC00.From_In_Months__c='3';
          LRSC00.To_In_Months__c='4';
         // insert LRSC00;
          
          //LRSC00.Increase_in_rent__c=3;
          //Update LRSC00;
          
          
          LC_RentSchedule__c LRSC01= new LC_RentSchedule__c();
          LRSC01.Lease_Opportunity__c=OLC.id;
          LRSC01.Income_Category_1__c='RNT';
          LRSC01.Increase_in_rent__c=23;
          LRSC01.From_In_Months__c='4';
          LRSC01.To_In_Months__c='5';
          insert LRSC01;
          
           //mvc.Helper__c =true;
           //update mvc;
          
          LRSC.Annual_PSF__c=150;
          update LRSC;
                    
          LRSC.Annual_PSF__c=0.00;
          //update LRSC;
         
         //LRSC00.Increase_in_rent__c=40;
         //update LRSC0;
     test.stoptest();
          
    }
    
}