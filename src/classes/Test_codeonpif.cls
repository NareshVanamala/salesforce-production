@isTest
public class Test_codeonpif
{
    static testmethod void codeonpifTest()
    {   
       // Id uid=[SELECT Alias,Id,LastName FROM User LIMIT 1].id;
        
       string  profileid= [Select id from profile where name='System Administrator' limit 1].id;
       
       User userToCreate = new User();
       userToCreate.FirstName = 'David123';
       userToCreate.LastName  = 'Liu';
       userToCreate.Email     = 'dvdkliu+sfdc99@gmail.com';
       userToCreate.Username  = 'sfdc-dreamer0550920151237@vereit.com';
       userToCreate.Alias     = 'fatty';
       userToCreate.ProfileId = profileid;
       userToCreate.TimeZoneSidKey    = 'America/Denver';
       userToCreate.LocaleSidKey      = 'en_US';
       userToCreate.EmailEncodingKey  = 'UTF-8';
       userToCreate.LanguageLocaleKey = 'en_US';      
       insert userToCreate;
        
        list<Project_Initiation__c> plist = new list<Project_Initiation__c>();
        list<Project_Initiation__c> plist1 = new list<Project_Initiation__c>();
        
        Project_Initiation__c  p =new Project_Initiation__c();
        p.name = 'Test';
        p.Code__c = 'PRJ123';
        p.ARCP_Integration_Project__c = true;
        p.CapEx__c = 100.00;
        p.OpEx__c = 100.00;
        p.CFO_GRoup__c = userToCreate.id;
        p.Champion_group__c = userToCreate.id;
        //p.Compliance_group__c = userToCreate.id;
        p.PIF_Group__c = userToCreate.id;
        p.Sponsor__c = userToCreate.id;
        p.PMO_Group_Status__c = 'Submitted for Approval';
        //plist.add(p);
        insert p;
        
        system.assertequals(p.CFO_GRoup__c,userToCreate.id);

        Project_Initiation__c  p1 =new Project_Initiation__c();
        p1.name = 'Test';
        p.Code__c = 'PRJ123';
        p1.ARCP_Integration_Project__c = true;
        p1.CapEx__c = 100.00;
        p1.OpEx__c = 100.00;
        p1.PMO_Group_Status__c = 'Submitted for Approval';
       // plist.add(p1);
        insert p1;
        
        system.assertequals(p.name ,p1.name);

         
        Project_Initiation__c  p2 =new Project_Initiation__c();
        p2.name = 'Test';
        p2.Code__c = 'PRJ123';
        p2.ARCP_Integration_Project__c = false;
        p2.CapEx__c = 100.00;
        p2.OpEx__c = 100.00;
        p2.PMO_Group_Status__c = 'Submitted for Approval';
       // plist1.add(p2);
       insert p2;
        
        Project_Initiation__c  p3 =new Project_Initiation__c();
        p3.name = 'Test';
        p3.Code__c = 'PRJ123';
        p3.ARCP_Integration_Project__c = false;
        p3.CapEx__c = 100.00;
        p3.OpEx__c = 100.00;
        p3.PMO_Group_Status__c = 'Submitted for Approval';
       // plist1.add(p3);*/
        insert p3;
         system.assertequals(p2.name ,p3.name);

 }
 }