public with sharing class Submitedsuccess {

    public PageReference home() {
        
        PageReference NewHireFormPage = new PageReference ('/apex/employeeuserchangeform');
        NewHireFormPage .setRedirect(true);
        return NewHireFormPage ;
        
    }
    
    public PageReference UserChangehome() {
        
        PageReference UserchangePage = new PageReference ('/apex/employeeuserchangeform');
        UserchangePage.setRedirect(true);
        return UserchangePage;
        
    }

}