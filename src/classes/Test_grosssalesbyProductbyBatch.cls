@isTest
private class Test_grosssalesbyProductbyBatch 
{
public static testMethod void grosssalesbyProductbyBatch() 
{
Id recdBrokerDealer=[Select id,name,DeveloperName from RecordType where DeveloperName='BrokerDealerAccount' and SobjectType = 'Account'].id; 

Map<id,National_Account_Helper__c> newmapaccount= new Map<id,National_Account_Helper__c>();
Account a = new Account();
a.name = 'testaccount';
a.recordtypeid=recdBrokerDealer;
insert a;

List<National_Account_Helper__c> nahListExisting = new List<National_Account_Helper__c>();
List<REIT_Investment__c> reitnewlist = new List<REIT_Investment__c>();

List<National_Account_Helper__c> updatednahListExisting = new List<National_Account_Helper__c>();
nahListExisting = [select Account__c,AprilCCIT__c,AprilCCPT__c,AprilCCPTII__c,AprilCCPTIII__c,AprilCCPTIV__c,AprilIncomeNav__c,AugustCCIT__c,AugustCCPT__c,AugustCCPTII__c,AugustCCPTIII__c,AugustCCPTIV__c,AugustIncomeNav__c,CCITlastyear__c,CCPTIIILastYear__c,CCPTIILastYear__c,CCPTIVlastyear__c,CCPT_LastYear__c,CurrentYearMinus2CCIT__c,CurrentYearMinus2CCPT__c,CurrentYearMinus2CCPTII__c,CurrentYearMinus2CCPTIII__c,CurrentYearMinus2CCPTIV__c,CurrentYearMinus2IncomeNav__c,CurrentyearMinus3CCIT__c,CurrentYearMinus3CCPT__c,CurrentYearMinus3CCPTII__c,CurrentYearMinus3CCPTIII__c,CurrentYearMinus3CCPTIV__c,CurrentYearMinus3IncomeNav__c,FebCCIT__c,FebCCPT__c,FebCCPTII__c,FebCCPTIII__c,FebCCPTIV__c,FebIncomeNav__c,INCOMENAVLastyear__c,JanCCIT__c,JanCCPT__c,JanCCPTII__c,JanCCPTIII__c,JanCCPTIV__c,JanIncomeNav__c,JulyCCIT__c,JulyCCPT__c,JulyCCPTII__c,JulyCCPTIII__c,JulyCCPTIV__c,JulyIncomeNav__c,JuneCCIT__c,JuneCCPT__c,JuneCCPTII__c,JuneCCPTIII__c,JuneCCPTIV__c,JuneIncomeNav__c,MarCCIT__c,MarCCPT__c,MarCCPTII__c,MarCCPTIII__c,MarCCPTIV__c,MarIncomeNav__c,MayCCIT__c,MayCCPT__c,MayCCPTII__c,MayCCPTIII__c,MayCCPTIV__c,MayIncomeNav__c,NovCCIT__c,NovCCPT__c,NovCCPTII__c,NovCCPTIII__c,NovIncomeNav__c,OctCCIT__c,OctCCPT__c,OctCCPTII__c,OctCCPTIII__c,OctCCPTIV__c,OctIncomeNav__c,SeptCCIT__c,SeptCCPT__c,SeptCCPTII__c,SeptCCPTIII__c,SeptCCPTIV__c,SeptIncomeNav__c,DecCCIT__c,DecCCPT__c,DecCCPTII__c,DecCCPTIII__c,DecCCPTIV__c,DecIncomeNav__c,NovCCPTIV__c FROM National_Account_Helper__c WHERE Account__c=:a.id]; 

for(National_Account_Helper__c cnt:nahListExisting)
{
cnt.CCITlastyear__c=0;
cnt.CCPTIIILastYear__c=0;
cnt.CCPTIILastYear__c=0;
cnt.CCPTIVlastyear__c=0;
cnt.CCPT_LastYear__c=0;
cnt.AugustCCIT__c =0;
cnt.CurrentYearMinus2CCPT__c=0;
cnt.CurrentYearMinus2CCIT__c=0;
cnt.CurrentYearMinus2CCPTIII__c=0;
cnt.CurrentYearMinus2CCPTIV__c=0;
cnt.CurrentYearMinus2IncomeNav__c=0;
cnt.CurrentyearMinus3CCIT__c=0;
cnt.CurrentYearMinus3CCPT__c=0;
cnt.CurrentYearMinus3CCPTII__c=0;
cnt.CurrentYearMinus3CCPTIII__c=0;
cnt.CurrentYearMinus3CCPTIV__c=0;
cnt.CurrentYearMinus3IncomeNav__c=0;
updatednahListExisting.add(cnt); 
}
update updatednahListExisting;



Contact c1= new Contact();
c1.accountid=a.id;
c1.firstname='DonotTrustAnyone';
c1.lastname='ThatIsTrue';
insert c1;

system.assertequals(c1.accountid,a.id);

REIT_Investment__c ct= new REIT_Investment__c();
ct.Rep_Contact__c = c1.id;
date d = date.newInstance(Date.today().year()-1,6,25);
ct.Deposit_Date__c= d;
ct.Fund__c='3770';
ct.Current_Capital__c=15000;
//insert ct;
reitnewlist.add(ct); 
//updatednahListExisting[0].CCITlastyear__c= ct.Current_Capital__c;
REIT_Investment__c ct1= new REIT_Investment__c();
ct1.Rep_Contact__c = c1.id;
ct1.Deposit_Date__c= d;
ct1.Fund__c='3770';
ct1.Current_Capital__c=15000;
//insert ct1; 
reitnewlist.add(ct1);
REIT_Investment__c ccptlastyear= new REIT_Investment__c();
ccptlastyear.Rep_Contact__c = c1.id;
ccptlastyear.Deposit_Date__c= d;
ccptlastyear.Fund__c='3700';
ccptlastyear.Original_Capital__c=15000;
//insert ccptlastyear; 
reitnewlist.add(ccptlastyear);
REIT_Investment__c ccpt3= new REIT_Investment__c();
ccpt3.Rep_Contact__c = c1.id;
ccpt3.Deposit_Date__c= d;
ccpt3.Fund__c='3767';
ccpt3.Current_Capital__c=15000;
//insert ccpt3; 
reitnewlist.add(ccpt3);
/*REIT_Investment__c ct2= new REIT_Investment__c();
ct2.Rep_Contact__c = c1.id;
ct2.Deposit_Date__c= d;
ct2.Fund__c='3771';
ct2.Current_Capital__c=15000;
//insert ct2; 
reitnewlist.add(ct2);*/
REIT_Investment__c ct3= new REIT_Investment__c();
ct3.Rep_Contact__c = c1.id;
ct3.Deposit_Date__c= d;
ct3.Fund__c='3772';
ct3.Current_Capital__c=15000;
//insert ct3; 
reitnewlist.add(ct3);
REIT_Investment__c ct4= new REIT_Investment__c();
ct4.Rep_Contact__c = c1.id;
ct4.Deposit_Date__c= d;
ct4.Fund__c='3701';
ct4.Current_Capital__c=15000;
//insert ct4; 
reitnewlist.add(ct4);
//************************current year minus 2*************** 
REIT_Investment__c ct5= new REIT_Investment__c();
date d1 = date.newInstance(Date.today().year()-2,6,25);
ct5.Rep_Contact__c = c1.id;
ct5.Deposit_Date__c= d1;
ct5.Fund__c='3700';
ct5.Original_Capital__c=15000;
//insert ct5; 
reitnewlist.add(ct5);


REIT_Investment__c ccptIIcurrentyearminus2= new REIT_Investment__c();
ccptIIcurrentyearminus2.Rep_Contact__c = c1.id;
ccptIIcurrentyearminus2.Deposit_Date__c= d1;
ccptIIcurrentyearminus2.Fund__c='3701';
ccptIIcurrentyearminus2.Current_Capital__c=15000;
//insert ccptIIcurrentyearminus2; 
reitnewlist.add(ccptIIcurrentyearminus2);

REIT_Investment__c ccptIIIcurrentyearminus2= new REIT_Investment__c();
ccptIIIcurrentyearminus2.Rep_Contact__c = c1.id;
ccptIIIcurrentyearminus2.Deposit_Date__c= d1;
ccptIIIcurrentyearminus2.Fund__c='3767';
ccptIIIcurrentyearminus2.Current_Capital__c=15000;
//insert ccptIIIcurrentyearminus2; 
reitnewlist.add(ccptIIIcurrentyearminus2);


REIT_Investment__c ccITcurrentyearminus2= new REIT_Investment__c();
ccITcurrentyearminus2.Rep_Contact__c = c1.id;
ccITcurrentyearminus2.Deposit_Date__c= d1;
ccITcurrentyearminus2.Fund__c='3770';
ccITcurrentyearminus2.Current_Capital__c=15000;
//insert ccITcurrentyearminus2; 

reitnewlist.add(ccITcurrentyearminus2);

REIT_Investment__c ccPTIVcurrentyearminus2= new REIT_Investment__c();
ccPTIVcurrentyearminus2.Rep_Contact__c = c1.id;
ccPTIVcurrentyearminus2.Deposit_Date__c= d1;
ccPTIVcurrentyearminus2.Fund__c='3772';
ccPTIVcurrentyearminus2.Current_Capital__c=15000;
//insert ccPTIVcurrentyearminus2;
reitnewlist.add(ccPTIVcurrentyearminus2);

system.assert(reitnewlist!=Null);



/*REIT_Investment__c IncomeNavcurrentyearminus2= new REIT_Investment__c();
IncomeNavcurrentyearminus2.Rep_Contact__c = c1.id;
IncomeNavcurrentyearminus2.Deposit_Date__c= d1;
IncomeNavcurrentyearminus2.Fund__c='3771';
IncomeNavcurrentyearminus2.Current_Capital__c=15000;
//insert IncomeNavcurrentyearminus2;
reitnewlist.add(IncomeNavcurrentyearminus2);*/

insert reitnewlist;
 ApexPages.currentPage().getParameters().put('id',a.id); 
    ApexPages.StandardController controller = new ApexPages.StandardController(a);  
                 grosssalesbyProductbyBatch x = new grosssalesbyProductbyBatch (controller); 


   
 /*batch1.CCPTIIcurrentyrminus2 =      
 CCPTIIIcurrentyrminus2      
  CCPTIVcurrentyrminus2        
CCITcurrentyrminus2        
IncomeNavcurrentyrminus2       
 CCPTIcurrentyrminus2  
 TOTALcurrentyrminus2 */
 }
 }