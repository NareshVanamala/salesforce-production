@isTest(SeeAlldata = true)
private class Test_Mass_ApplyActionplan_class
{
   
  static testMethod void myUnitTest1()
 {
    User u = [SELECT id,name FROM USER WHERE id =:UserInfo.getUserId()];
    Template__c temp = new Template__c();
    temp.Name = 'Template';
    temp.Is_Template__c =true;
    temp.Mass_Assign_To__c=u.id;
    insert temp ; 
    system.assertequals(temp.Mass_Assign_To__c,u.id);
   
    Task_Plan__c tp = new Task_Plan__c ();
    /*tp.Name='test';
    tp.Template__c =temp.Id ; 
    tp.parent__c=null;
    tp.Field_to_update_Date_Received__c ='Accenture_Due_Date__c';
    tp.Date_Received__c =Date.newInstance(1999,12, 12);
    tp.Field_to_update_for_date_needed__c ='Passed_Date__c';
    tp.Date_Needed__c =Date.newInstance(1999,11, 12);
    tp.index__c=1;
    tp.Assigned_To__c=u.id;
    insert tp ; 

    Task_Plan__c tp0 = new Task_Plan__c ();
    tp0.Name='test1';
    tp0.Field_to_update_Date_Received__c ='Accenture_Due_Date__c';
    tp0.Date_Received__c =Date.newInstance(1999,12, 12);
    tp0.Field_to_update_for_date_needed__c ='Passed_Date__c';
    tp0.Date_Needed__c =Date.newInstance(1999,11, 12);
    tp0.Template__c =temp.Id ; 
    tp0.parent__c=tp.id; 
    tp0.index__c=1;
    tp0.Assigned_To__c=u.id;
    insert tp0 ;*/
    
    List<Task_Plan__c> TPL=new List<Task_Plan__c>();
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Dead_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Site_Visit_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Appraisal_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Passed_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Accenture_Due_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_COE_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Go-Hard Date', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Hard_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Rep_Burn_Off__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_SP_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Site_Visit_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Purchase_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Query_Sorted_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_Hard_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Passed_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Contract_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_COE_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Closing_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Hard_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='LOI_Signed_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_SP_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='LOI_Sent_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Purchase_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Lease_Abstract_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_Hard_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Lease_Expiration__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Contract_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Open_Escrow_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Closing_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Dead_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='LOI_Signed_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Tenant_ROFR_Waiver__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='LOI_Sent_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Date_Audit_Completed__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Lease_Abstract_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Date_Identified__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Lease_Expiration__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Investment_Committee_Approval__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Open_Escrow_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Part_1_Start_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Purchase_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Tenant_ROFR_Waiver__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Date_Audit_Completed__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Date_Identified__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Investment_Committee_Approval__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Part_1_Start_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estoppel__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);

insert TPL;
 
    
    Portfolio__c pct= new Portfolio__c();
    pct.Name='TestPortfolio';
    insert pct;
    
    Deal__c d = new Deal__c();
    d.Name='Testdeal12 ';
    // d.Field_to_update_Date_Received__c = Date.newInstance(12, 12, 1999);
    d.Appraisal_Date__c = Date.newInstance(1999,12, 12);
    //d.Accenture_Due_Date__c = Date.newInstance(12, 12, 1999);
    d.Rep_Burn_Off__c = Date.newInstance(1999,12, 12);
    d.Passed_Date__c = Date.newInstance(1999,12, 12);
    d.Hard_Date__c = Date.newInstance(1999,12, 12);
    d.Closing_Date__c = Date.newInstance(1999,12, 12);
    d.Purchase_Date__c = Date.newInstance(1999,12, 12);
    d.LOI_Sent_Date__c = Date.newInstance(1999,12, 12);
    d.Open_Escrow_Date__c = Date.newInstance(1999,12, 12);
    d.Portfolio_Deal__c=pct.id;
    d.Zip_Code__c='64051';
    d.Ownership_Interest__c='Fee Simple';
    d.HVAC_Warranty_Status__c ='Received';
    d.Roof_Warranty_Status__c = 'Received';
    d.Create_Workspace__c =true; 
    d.Build_to_Suit_Type__c ='Current';
    d.Estimated_COE_Date__c = date.today();
    d.Ownership_Interest__c = 'Fee Simple';
    d.Zip_Code__c = '98033';
    insert d ; 
    system.assertequals(d.Portfolio_Deal__c,pct.id);

   
    string dealid= d.id;
    
    Deal__c d1 = new Deal__c();
    d1.Name='Testdeal11 ';
    // d.Field_to_update_Date_Received__c = Date.newInstance(12, 12, 1999);
    d1.Appraisal_Date__c = Date.newInstance(1999,12, 12);
    //d.Accenture_Due_Date__c = Date.newInstance(12, 12, 1999);
    d1.Rep_Burn_Off__c = Date.newInstance(1999,12, 12);
    d1.Passed_Date__c = Date.newInstance(1999,12, 12);
    d1.Hard_Date__c = Date.newInstance(1999,12, 12);
    d1.Closing_Date__c = Date.newInstance(1999,12, 12);
    d1.Purchase_Date__c = Date.newInstance(1999,12, 12);
    d1.LOI_Sent_Date__c = Date.newInstance(1999,12, 12);
    d1.Open_Escrow_Date__c = Date.newInstance(1999,12, 12);
    d1.Portfolio_Deal__c=pct.id; 
    d1.Zip_Code__c='64052';
    d1.Ownership_Interest__c='Fee Simple';
    d1.HVAC_Warranty_Status__c ='Received';
    d1.Roof_Warranty_Status__c = 'Received';
    d1.Create_Workspace__c =true; 
    d.Build_to_Suit_Type__c ='Current';
    d.Estimated_COE_Date__c = date.today();
    d.Ownership_Interest__c = 'Fee Simple';
    d.Zip_Code__c = '98033';
    insert d1 ;
    string dealid2=d1.id;
    ApexPages.currentPage().getParameters().put('parentindex','1');
    ApexPages.currentPage().getParameters().put('ids','dealid,dealid2');
    ApexPages.StandardController sc = new ApexPages.standardController(temp);
    MassApplyActionPlan taskmass= new MassApplyActionPlan (sc);
    system.debug(taskmass.selectedDealIds);
    list<Deal__c >selecteddeallist=taskmass.getDeals();
    taskmass.changeTemplate();
    taskmass.templateSelected=temp.name; 
    system.debug('lets check the selected template..'+taskmass.templateSelected);
    system.debug('get me the selected deals..'+selecteddeallist);
    Template__c ct = new Template__c();
    ct.name= taskmass.templateSelected;
    ct.template__c=temp.Id; 
    ct.Mass_Assign_To__c=u.id; 
    ct.deal__c=d1.id;  
    insert ct;
    
    system.assertequals(ct.template__c,temp.Id);

    taskmass.actionPlanTasks=[select id, index__c,Assigned_To__c,Date_Needed__c,Date_Ordered__c,Date_Received__c,Field_to_update_Date_Received__c ,Field_to_update_for_date_needed__c,Notify_Emails__c ,Priority__c,Comments__c,name,Date_Completed__c from Task_Plan__c where Template__c =:temp.Id ]; 
    taskmass.checkDuplicateTasks();
    taskmass.getdateFields();
    taskmass.gettemplatesOptions();
    taskmass.getdateFields();
    taskmass.addTask();
    taskmass.addSubTask();
    taskmass.selectedDealIds.add(d1.id);
    taskmass.selectedDealIds.add(d.id);
    list<Task_Plan__c>listupdatedlst= new list<Task_Plan__c>(); 
    /*for(Task_Plan__c ctsnehal:taskmass.actionPlanTasks) 
    {
    ctsnehal.Assigned_To__c=Userinfo.getuserid();
    ctsnehal.Template__c =temp.Id; 
    listupdatedlst.add(ctsnehal);
    
    }
    upsert listupdatedlst;*/
    PageReference pref = new PageReference('/'+d1.Portfolio_Deal__c); 
    system.debug('check the selecteddeals...'+taskmass.selectedDealIds);
    //taskPlans.addTask();
    pref= taskmass.Save();
    
    Template__c ct1 = new Template__c();
    ct1.name= taskmass.templateSelected;
    ct1.template__c=temp.Id; 
    ct1.Is_Template__c=false;
    ct1.Deal__c=d.id;
    ct1.Mass_Assign_To__c=u.id; 
    insert ct1;
    
    Template__c ct2 = new Template__c();
    ct2.name= taskmass.templateSelected;
    ct2.template__c=temp.Id; 
    ct2.Is_Template__c=false;
    ct2.Deal__c=d1.id;
    ct2.Mass_Assign_To__c=u.id;
    insert ct2;
    list<Task_Plan__c>maintaskplans= new list<Task_Plan__c>();
    for(Task_Plan__c T:taskmass.actionPlanTasks)
    {
        Task_Plan__c cttask= new Task_Plan__c();
        cttask.Assigned_To__c=u.id;
        cttask.Template__c =ct1.id;
        cttask.name=T.name;
        //cttask.Date_Needed__c=T.Date_Needed__c;
        //cttask.Date_Ordered__c=T.Date_Ordered__c;
        //If(T.Date_Received__c!=null)
        //{
        //cttask.Date_Received__c=T.Date_Received__c;
        //cttask.Status__c='Completed';
        
        //}    
        cttask.Index__c =T.Index__c ;
        //cttask.Field_to_update_Date_Received__c = T.Field_to_update_Date_Received__c;
        //cttask.Field_to_update_for_date_needed__c= T.Field_to_update_for_date_needed__c ;
        //cttask.Notify_Emails__c =T.Notify_Emails__c ;
        //cttask.Priority__c=T.Priority__c;
        //cttask.Comments__c=T.Comments__c;
        //cttask.Date_Completed__c=T.Date_Completed__c;
        maintaskplans.add(cttask);
    }
    insert maintaskplans;
    
    //taskmass. RemoveTask();
} 

}