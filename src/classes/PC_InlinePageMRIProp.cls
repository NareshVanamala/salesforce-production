public with sharing class PC_InlinePageMRIProp{
    public String ObjectName {get; set;}
    public MRI_PROPERTY__c mriprop{get;set;}
    public List<Property_Contact_Leases__c> propcontacts{get;set;}
    public String myValue;
    public set<id> myobjectidset{get;set;}
    public string mripropid{get;set;}
    
    public PC_InlinePageMRIProp(ApexPages.StandardController controller){
   
    }
    public  PC_InlinePageMRIProp(){
        
    }
    public class mywrapper{
        
        public Property_Contact_Leases__c pcls {get; set;}
        public Property_Contact_Buildings__c Bcls {get; set;}
        public boolean selected {get; set;}
        public id recordid{get; set;}
        public id selrecId{get; set;}
        public id MRIRecordId{get; set;}
        public id LeaseRecordId{get; set;}
        public string PropertyContactName{get; set;}
        Public string PropertyContactCompanyname{get; set;}
        Public string EmailAddress{get; set;}
        Public string Phone{get; set;}
        Public string Address{get; set;}
        Public string City{get; set;}
        Public string State{get; set;}
        Public string Zipcode{get; set;}
        Public string ContactType{get; set;}
        Public string LocalTenant{get; set;}
        Public string CommonName{get; set;}
        Public string Notes{get; set;}
        
        public mywrapper(Id Myid,Id sel,id MRIId,Id LeaseId,string myname, String mycompanyname, String myemail,String Phone,String Address,String City,String State,String Zipcode,String ContactType,String LocalTenant,String CommonName,String Notes){
            
            this.recordid=myid;
            this.selrecId =sel;
            this.MRIRecordId =MRIId;
            this.LeaseRecordId =LeaseId;
            this.PropertyContactName=myname;
            this.PropertyContactCompanyname=mycompanyname;
            this.EmailAddress=myemail;
            this.Phone=Phone;
            this.Address=Address;
            this.City=City;
            this.State=State;
            this.Zipcode=Zipcode;
            this.ContactType=ContactType;
            this.LocalTenant=LocalTenant;
            this.CommonName=CommonName;
            this.Notes=Notes;
            this.pcls = pcls;
            selected = false; 
             //If you want all checkboxes initially selected, set this to true
        }    
    }

     public  List<mywrapper> mylist{get{
        mripropid= ApexPages.currentPage().getParameters().get('id');
        system.debug('this is my id'+mripropid);
        system.debug('this is my object'+ObjectName);
        myobjectidset=new set<id>(); 
        if(ObjectName =='Lease'){  
           for(Lease__c ls:[select id,MRI_PROPERTY__c,Lease_Status__c from Lease__c where MRI_PROPERTY__c=:mripropid and Lease_Status__c =:'active'])
               myobjectidset.add(ls.id);
           system.debug('this is my id'+myobjectidset);
              
           mylist = new List<mywrapper>();
           list<Property_Contact_Leases__c>foo=new list<Property_Contact_Leases__c>();
           foo=[select id,Property_Contacts__r.id,Property_Contacts__r.MRI_Property__c ,Property_Contacts__r.Name,Property_Contacts__r.Company_Name__c,
                                Property_Contacts__r.Email_Address__c,Property_Contacts__r.Phone__c,Property_Contacts__r.Address__c,
                                Property_Contacts__r.City__c,Property_Contacts__r.State__c,Property_Contacts__r.Zipcode__c,Contact_Type__c,Lease__r.Tenant_Name__c,Lease__r.MRI_Property__r.Common_Name__c,
                                 Property_Contacts__r.Notes__c from Property_Contact_Leases__c where Lease__c IN:myobjectidset];
                                
           mywrapper w;  
           for(Property_Contact_Leases__c pc:foo){
               id selrecId =pc.id;
               id recordid= pc.Property_Contacts__r.id;
               id MRIRecordId=mripropid;
               id LeaseRecordId=pc.Lease__c;
               string PropertyContactName=pc.Property_Contacts__r.Name;
               string PropertyContactCompanyname=pc.Property_Contacts__r.Company_Name__c;
               string EmailAddress=pc.Property_Contacts__r.Email_Address__c;
               string Phone =pc.Property_Contacts__r.Phone__c;
               string Address=pc.Property_Contacts__r.Address__c;
               string City=pc.Property_Contacts__r.City__c;
               string State=pc.Property_Contacts__r.State__c;
               string Zipcode=pc.Property_Contacts__r.Zipcode__c;
               string ContactType=pc.Contact_Type__c;
               string LocalTenant=pc.Lease__r.Tenant_Name__c;
               string CommonName=pc.Lease__r.MRI_Property__r.Common_Name__c;
               string Notes=pc.Property_Contacts__r.Notes__c;
               w= new mywrapper(recordid,selrecId,MRIRecordId,LeaseRecordId,PropertyContactName,PropertyContactCompanyname,EmailAddress,Phone,Address,City,State,Zipcode,ContactType,LocalTenant,CommonName,Notes); 
               mylist.add(w); 
                  
           }                      
           system.debug('this is my list'+mylist);
        }
        else if (ObjectName =='MRI Property'){
           mylist = new List<mywrapper>();
           list<Property_Contact_Buildings__c>foo= new list<Property_Contact_Buildings__c>();
           foo = [select id,Property_Contacts__r.id,Property_Contacts__r.MRI_Property__c ,Property_Contacts__r.Name,Property_Contacts__r.Company_Name__c,
                                Property_Contacts__r.Email_Address__c,Property_Contacts__r.Phone__c,Property_Contacts__r.Address__c,
                                Property_Contacts__r.City__c,Property_Contacts__r.State__c,Property_Contacts__r.Zipcode__c,Contact_Type__c,Center_Name__r.Tenant_Name__c,Center_Name__r.Common_Name__c,
                                Property_Contacts__r.Notes__c from Property_Contact_Buildings__c where Center_Name__c=:mripropid];
                      mywrapper w;            
           for(Property_Contact_Buildings__c pc:foo){
               id recordid= pc.Property_Contacts__r.id;
               id selrecId=pc.id;
               id LeaseRecordId=null;
               id MRIRecordId=mripropid;
               string PropertyContactName=pc.Property_Contacts__r.Name;
               string PropertyContactCompanyname=pc.Property_Contacts__r.Company_Name__c;
               string EmailAddress=pc.Property_Contacts__r.Email_Address__c;
               string Phone =pc.Property_Contacts__r.Phone__c;
               string Address=pc.Property_Contacts__r.Address__c;
               string City=pc.Property_Contacts__r.City__c;
               string State=pc.Property_Contacts__r.State__c;
               string Zipcode=pc.Property_Contacts__r.Zipcode__c;
               string ContactType=pc.Contact_Type__c;
               string LocalTenant=pc.Center_Name__r.Tenant_Name__c;
               string CommonName=pc.Center_Name__r.Common_Name__c;
               string Notes=pc.Property_Contacts__r.Notes__c;
               w= new mywrapper(recordid,selrecId,MRIRecordId,LeaseRecordId,PropertyContactName,PropertyContactCompanyname,EmailAddress,Phone,Address,City,State,Zipcode,ContactType,LocalTenant,CommonName,Notes); 
               mylist.add(w); 
                  
           }      
                
        }
        return mylist;
     }set;
    }   
    

}