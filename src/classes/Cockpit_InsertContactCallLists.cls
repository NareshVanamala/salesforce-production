global with sharing class Cockpit_InsertContactCallLists implements Database.Batchable<SObject>{
 global String Query;  
 global boolean internalFlag;  
 global Automated_call_List__c LockedCL ;
 global String conquery;
 global Automated_Call_List__c automatedCallList;
 global List<contact_call_list__c> callistname1;
 global Cockpit_InsertContactCallLists (Id AutCallListId){  
    string dynamicQuery ='';
    string campaignStatus='';
    boolean filters;
    system.debug('Welcome to Batch apex method...');
    automatedCallList = [Select Id,Name,Dynamic_Query__c,Campaign__c,Dynamic_Query_Filters__c,Internal_Flag__c,Description__c  From Automated_Call_List__c Where Id =: AutCallListId limit 1];
    internalFlag = automatedCallList.Internal_Flag__c;
    system.debug('Automated Call List Name'+automatedCallList);
    dynamicQuery = automatedCallList.Dynamic_Query_Filters__c;
    if(dynamicQuery != '' && dynamicQuery != null){
        dynamicQuery = dynamicQuery.replaceAll('null','');
        list<string> splitdynamicQuery = dynamicQuery.split('&&&&');
        system.debug('*****splitdynamicQuery*****'+splitdynamicQuery);
        system.debug('*****Selected Campaign Status*****'+splitdynamicQuery[0]);
        //system.debug('*****splitdynamicQuery[1]*****'+splitdynamicQuery[1]);
        campaignStatus = splitdynamicQuery[0];
        if(splitdynamicQuery.size() > 1&& splitdynamicQuery[1] != '' && splitdynamicQuery[1] != null)
          filters = true;
    }
    if(automatedCallList.Campaign__c != null){  
        List<CampaignMember>  cmlist= new List<CampaignMember>();      
        String camptextid = automatedCallList.Campaign__c;
        String campsubid = camptextid.Substring(0,15);
        String campaignQuery = '';
        campaignQuery = 'select id,Campaignid,Contactid from CampaignMember where Campaignid =' +'\''+automatedCallList.Campaign__c+'\'';
        if(campaignStatus!= null && campaignStatus != 'Select Campaign Status' && campaignStatus != null && campaignStatus != 'null')
            campaignQuery += 'and status ='+'\''+campaignStatus+'\'';
        System.debug('campaignQuery' + campaignQuery);
        cmlist = Database.query(campaignQuery);
        system.debug('+++++cmlist'+cmlist);
        system.debug('+++++cmlist size'+cmlist.size());
        List<String> conids= new List<String>();
        String str = '';
        String glue = '(';
        for(integer i=0;i<cmlist.size();i++){
            conids.add('\''+cmlist[i].ContactId+'\'');
            str = str + glue + '\''+cmlist[i].ContactId+'\'';
            glue = ',';
        }
        if(cmlist.size()>0){
           str += ')';
            //if(filters == true)
           conquery=' AND '+ 'id in '+ str; 
            //else
               //conquery= ' where '+ 'id in'+ str;
        }
                     
    }   
          System.debug('campaign query'+conquery);          

 }
 global Database.QueryLocator start(Database.BatchableContext BC){ 
    String exceptionMessage ='has not been edited because of'; 
    try{ 
      Query= automatedCallList.Dynamic_Query__c;  
      if(conquery!='' && conquery!= null)  
        Query = Query+ conquery;
      system.debug('Myquery is......'+Query);
      return Database.getQueryLocator(Query); 
    }
    catch(Exception e){
      query='Select Id from Contact limit 0';
      if(internalFlag == false){
         exceptionMessage ='has not been created because of'; 
         if(!Automated_Call_List__c.sObjectType.getDescribe().isDeletable())         
            Delete automatedCallList;
      }
         //Delete callistname;
      AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
 
      system.debug('Check the Exception.....'+e);
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      String[] toAddresses = new String[] {a.CreatedBy.Email};
      String[] bccAddresses = new String[] {'nvanamala@colecapital.com'};
      mail.setToAddresses(toAddresses);
      mail.setBccSender(true);
      mail.setBccAddresses(bccAddresses);
      mail.setSubject('Exception Message');
      mail.setPlainTextBody('CallList ' + automatedCallList.Name + ' ' +exceptionMessage + e.getMessage());
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      return Database.getQueryLocator(query);
    }
      
 }  
 global void execute(Database.BatchableContext BC,List<Contact> scope){  
    system.debug('Check the name of the call list name:'+automatedCallList);
    List<contact_call_list__c> callist = new List<contact_call_list__c>();
    If(scope.size()>0){
      for(Contact c : scope){
        contact_call_list__c cls = new contact_call_list__c();
        cls.CallList_Name__c = automatedCallList.name ;
        cls.Contact__c = c.Id;
        cls.Automated_Call_List__c= automatedCallList.id;
        cls.Description__c = automatedCallList.Description__c;
        cls.ContactCalllist_UniqueConbination__c =automatedCallList.Name+c.Id;
        callist.add(cls);

      }
    //system.debug('Final String value of calllist size\n'+size1);
      try{
         if(Schema.sObjectType.contact_call_list__c.isCreateable())
            insert callist;
      } 
      catch(system.dmlexception e) {
        System.debug('ccl not inserted: ' + e);
      }
        
        

   }
 }

 global void finish(Database.BatchableContext BC)  { 
    String sucessMessage = 'has been created';
    String sucessSubject = 'Call List generate process';
    if(internalFlag == true){
       sucessMessage = 'has been Updated';
       sucessSubject = 'Call List Update process'; 
    }
    AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
    System.debug('Final String automatedCallList.Name'+automatedCallList.Name);
    System.debug('a.CreatedBy.Email'+a.CreatedBy.Email);
    Automated_Call_List__c unlockcl = [Select Id, Name, TempLock__c  from Automated_Call_List__c where Name = :automatedCallList.Name limit 1];
    unlockcl.TempLock__c  = null;
    unlockcl.Internal_Flag__c  = true;
    if(Schema.sObjectType.Automated_Call_List__c.isUpdateable())
      update unlockcl;
  
    List<Contact_call_list__c> checkifinsert = [Select Id from Contact_Call_list__c where Automated_Call_List__c= :unlockcl.Id limit 1];
    // Send an email to the Apex job's submitter notifying of job completion.  
    If(checkifinsert.size()==0)
      sucessMessage = sucessMessage+' but it does not have any contact call lists.';  
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    String[] toAddresses = new String[] {a.CreatedBy.Email};
    //String[] bccAddresses = new String[] {'nvanamala@colecapital.com'};
    mail.setToAddresses(toAddresses);
    mail.setBccSender(true);
   // mail.setBccAddresses(bccAddresses);
    mail.setSubject(sucessSubject + a.Status);
    mail.setPlainTextBody('CallList ' + automatedCallList.Name + ' ' +sucessMessage);
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

 }
}