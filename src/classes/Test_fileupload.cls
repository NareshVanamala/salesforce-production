@isTest
private class Test_fileupload 
{
   public static testMethod void Testforfileupload() 
   {        
     
     Id c1 =[Select id,name,DeveloperName from RecordType where DeveloperName='Parent' and SobjectType = 'Project_Initiation__c'].id; 
          
   Project_Initiation__c p = new Project_Initiation__c();
        p.Name = 'Testing sandy';
        p.Resource_Name_Description__c = 'testing';
        p.Estimated_Time_Commitment__c='100';
        p.recordtypeid=c1;
        p.PMO_Group_Status__c= 'Submitted For Approval';
        p.ARCP_Integration_Project__c=True;
        p.Was_the_Project_Included_in_the_Original__c=True;
        p.Projecr_Requires_Compliance_Approval__c=True;
        try{
        Insert p;
        }
        catch( Exception e){ 
        }
           
        Attachment a  = new Attachment();
        a.Name='Unit Test 1';
        a.Description='Unit Test 1';
        a.Body=Blob.valueOf('Unit Test 1');
        a.parentid =p.id;
        a.contentType='txt';
        try{
        Insert a;
        }
        catch( Exception e){ 
        }
        
       string Body = 'Unit Test 12';
       
   FileUploadController.attachBlob(p.id,'','Unit Test 11','txt',Body);
        
        Attachment a1  = new Attachment();
        a1.Name='Unit Test 11';
        a1.Description='Unit Test 1';
        a1.Body=Blob.valueOf(Body);
        a1.parentid =p.id;
        a1.contentType='txt';
        try{
        Insert a1;
        }
        catch( Exception e){ 
        }
      system.assertequals(a1.parentid,p.id);

      
       string Body1 = 'Unit Test 13';
        
   FileUploadController.attachBlob(p.id,a1.id,'Unit Test 12','txt',Body1);
        
        Attachment a2  = new Attachment();
        a2.Name='Unit Test 12';
        a2.Description='Unit Test 1';
        a2.Body=Blob.valueOf(Body1);
        a2.parentid =p.id;
        a2.contentType='txt';
        try{
        Insert a2;
        }
        catch( Exception e){ 
        }
        
   }     
}