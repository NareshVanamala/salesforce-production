@isTest
public class Test_Convert_Opportunity
{
    static testMethod void ConvertOpportunity()
    {
         Account a = new Account();
         a.name= 'TestConvertOpportunity';
         insert a;   
         
         RecordType rtypes = [Select Name, Id From RecordType where isActive=true and sobjecttype='Opportunity' and name='Due Diligence'];
         
         Opportunity op= new opportunity();
         op.accountid=a.id;
         op.recordtypeid=rtypes.id; 
         Op.name= a.name;
         Op.StageName='New Opportunity'; 
         Op.CloseDate=System.today();
         insert op;
         
         system.assertequals(op.recordtypeid,rtypes.id);
        
         ApexPages.currentPage().getParameters().put('id',a.id);
         ApexPages.StandardController controller = new ApexPages.StandardController(a);
         convertAccountToOpportunities x = new convertAccountToOpportunities(controller );
         PageReference PR = new PageReference('/'+a.id);
         x.createOpportunity();
            
    }
    
}