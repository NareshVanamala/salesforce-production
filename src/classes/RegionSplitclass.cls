public with sharing class RegionSplitclass
{
    Public List<Event_Request_Split__c> memberList{get;set;}
    public list<Event_Request_Split__c> deletelist1;
    public Event_Request_Split__c updatedlist1;
    public list<Event_Request_Split__c> checklist1;
     public list<Event_Request_Split__c> checklist2;
    public integer removepos{get;set;}
    public string conid{get;set;}
    public Id recId{get;set;} 
    public Event_Request_Split__c memb{get;set;}
    public decimal totalsplit;
    public string fileName{get;set;} 
    public Blob fileBody{get;set;}
    public Attachment attachment{get;set;}
    public Attachment myatt{get;set;}
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    public Boolean showInputFile{get;set;}
    public Event_Automation_Request__c a; 
    public Boolean addnew{set;get;}
    public Boolean addnew1{set;get;}
    public Event_Request_Split__c addrow {set;get;} 
    public  set  <String> spiltset;
    public  String spiltlist;
    public  String spiltlist1;
    public RegionSplitclass (ApexPages.StandardController controller)
     {
        totalsplit=0; 
        addrow = new Event_Request_Split__c();
        //myAttachment = new Attachment();
        a =(Event_Automation_Request__c)controller.getrecord();
        memberList =[select  id, Region_Split__c,name,Split__c from Event_Request_Split__c where Event_Automation_Request__c=:a.id and Region_Split__c!=null order by LastModifiedDate DESC];
        deletelist1= new list<Event_Request_Split__c >();  
        updatedlist1=new Event_Request_Split__c ();
        recId= controller.getRecord().id;
        spiltset=new set<String>();
        
     }
    public Pagereference removecon()
    {
        if(memberList.size()>0)
        {
            string paramname= Apexpages.currentPage().getParameters().get('node');
            system.debug('Check the paramname'+paramname);
            for(Integer i=0;i<memberList.size();i++)
            {
              if(memberList[i].id==conid)
               {
                 removepos=i;
                 deletelist1.add(memberList[i]);
               }
            }
            if(removepos!=null)
            memberList.remove(removepos);
            system.debug('check the deletelist..'+deletelist1);
          
          if (Event_Request_Split__c.sObjectType.getDescribe().isDeletable()) 
            {
                    delete deletelist1;
            }                   
        }
       return ApexPages.currentPage();
   }
   
   // Edit Record method ....
   Pagereference p;
   public Id recordId {set;get;}
   public Boolean edit {set;get;}   
  
   public Pagereference editRecord()
   {
       recordId = conid;
       edit = true;
       return null;
       //p = new Pagereference('/apex/AdvancedProgrampage?id='+ApexPages.currentPage().getParameters().get('id'));
    }
   
  /* public Pagereference viewRecord()
   {
       recordId = conid;   
       myatt=[select id, name, parentid from Attachment where parentid=:recordId ];
       system.debug('check the attachement..'+myatt);
       p = new Pagereference('/'+myatt.id);
      // p.setRedirect(true);
       return p;
   }*/
   
   public Pagereference SaveRecord()
   {
     totalSplit=0;
     system.debug('Which value should I consider...'+memberList[0].Region_Split__c);
      if(memberList.size()>0)
      {
          for(Integer i=0;i<memberList.size();i++)
          {
             if(memberList[i].Split__c !=null)
             totalSplit= totalSplit+ memberList[i].Split__c;
            if(memberList[i].id==conid)
            {
                  updatedlist1.id= memberList[i].id;
                  updatedlist1.Region_Split__c= memberList[i].Region_Split__c;
                   updatedlist1.Split__c=memberList[i].Split__c ;
                   spiltlist1=memberList[i].Region_Split__c;
                   system.debug('Which value should I consider.2..'+spiltlist1);
            }
               else if(memberList[i].id!=conid)
              spiltset.add(memberList[i].id);
          }
          checklist2=[select  id, Region_Split__c,name,Split__c from Event_Request_Split__c where Event_Automation_Request__c=:a.id and  id   IN:spiltset  and Region_Split__c=: spiltlist1 order by  LastModifiedDate DESC];
           system.debug('Check checkcheck.'+checklist2.size());
          system.debug('Check checkcheck.'+spiltset);
        
      if(checklist2.size()>0)
{
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Already added Split for this region')); 
        
}     
  else if(checklist2.size()==0)      
{
   if(totalSplit>100)
   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Total Region Split cannot exceed 100%'));
   else if(totalSplit<=100)
   {
   if (Schema.sObjectType.Event_Request_Split__c.isUpdateable()) 

   update updatedlist1; 
   system.debug(updatedlist1)  ;
  edit = false; 
   }   
}   
      }

     return null; 
   }
    //adding new record
    

    public Pagereference newProgram()
    {
        totalsplit=0;
        memberList =[select  id, Region_Split__c,name,Split__c from Event_Request_Split__c where Event_Automation_Request__c=:a.id  and Region_Split__c!=null order by LastModifiedDate DESC];         
        if(memberList.size()>0)
        {
           for(Event_Request_Split__c cr:memberList)
           {
             if(cr.Split__c !=null)
             totalsplit=totalsplit+cr.Split__c ;      
           }
         }
        if(totalsplit<100)
        {
            addrow= new Event_Request_Split__c ();
            addnew=true; 
         }
         else if(totalsplit>=100)
         {         
             System.debug('not skipping');
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Total Region Split cannot exceed 100%')); 
         }
        return null;    
    }
   
    public Pagereference savenewProg()
    {
        addnew=false;
        totalsplit=0;
        spiltlist=addrow.Region_Split__c;
        try
         {
              memberList =[select  id, Region_Split__c,name,Split__c from Event_Request_Split__c where Event_Automation_Request__c=:a.id  and Region_Split__c!=null order by LastModifiedDate DESC];  
              checklist1=[select  id, Region_Split__c,name,Split__c from Event_Request_Split__c where Event_Automation_Request__c=:a.id  and  Region_Split__c=:spiltlist order by                    LastModifiedDate DESC];
         system.debug('Check tne spiltlist---'+spiltlist);
      if(checklist1.size()>0)
    {
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Already added Split for this region')); 
 }
   else if(checklist1.size()==0)
{
        if(memberList.size()>0)
              {
                 for(Event_Request_Split__c cr:memberList)
                 {
                     if(cr.Split__c!=null)
                     {
                      totalsplit=totalsplit+cr.Split__c ; 
                     }  
                  }        
               }
                if(addrow.Split__c!=null)
                totalsplit=totalsplit+ addrow.Split__c ;
               if(totalsplit<=100)
               {
                    
                    addrow.Event_Automation_Request__c= a.id;
                    a.Region_Split_Event__c=addrow.Region_Split__c;
                    if (Schema.sObjectType.Event_Automation_Request__c.isUpdateable()) 

                    update a;
                    if (Schema.sObjectType.Event_Request_Split__c.isCreateable())

                     insert addrow;
                    
                    /*system.debug('Added --->'+addrow.Id);
                     Helper_for_EventSplit__c helpme1;
     helpme1=[select id,name,Helper__c from Helper_for_EventSplit__c where name='SplitHelper'];
    system.debug('check the valur..'+helpme1.Helper__c );
                  helpme1.Helper__c=false;
                    update helpme1;
                    a.Region_Split_Event__c=addrow.Region_Split__c;
                    update a;
                    helpme1.Helper__c=true;
                    update helpme1;*/
                    memberList =[select  id, Region_Split__c,name,Split__c from Event_Request_Split__c where Event_Automation_Request__c=:a.id and Region_Split__c!=null order by LastModifiedDate DESC];
                 }
               if(totalsplit>100)
               {         
                 System.debug('not skipping');
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Total Region Split cannot exceed 100%')); 
                }
          }
   }
         catch(Exception e)
         {
            system.debug('Exception --->'+e);
         } 
         return ApexPages.currentPage();
      }
      
    public Pagereference cancelnewProg()
    {
        addnew=false;
        return null;    
    }  
   
    public Event_Request_Split__c event1
   { 
      get
     {     
         if (event1  == null) 
           event1  = new Event_Request_Split__c();   
           return event1 ; 
      }   
    set;  
     }
   
   /*public Pagereference Savevalue()
   {
   system.debug('Chcek the value...'+addrow.Region_Split__c);
   addrow.Event_Automation_Request__c= a.id;
   insert addrow;
   memberList =[select  id, Region_Split__c,name,Split__c from Event_Request_Split__c where Event_Automation_Request__c=:a.id order by LastModifiedDate DESC]; 
   return null;
    }*/
 }