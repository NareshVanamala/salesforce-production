global virtual without sharing class ocms_PropertyforLeaseDetail extends cms.ContentTemplateController{
    
    GetURL getURLObj = new GetURL();
    
    global ocms_PropertyforLeaseDetail() {
        
    }
    
    global override virtual String getHTML(){

        SObject property;
        String html;
        String propertyID = System.currentPageReference().getParameters().get('propertyId');
       // String grossarea = System.currentPageReference().getParameters().get('grossarea');
        
        string webready='Approved'; 
        String query = 'SELECT Name, Related_Resources__c, Common_Name__c, Web_Name__c,status__c, Gross_Leaseable_Area__c, ImagesforLease__c, Address__c, City__c, State__c, Zip_Code__c, Date_Acquired__c, Property_Type__c, Sector__c, sqFt__c, ProgramName__c, Long_Description__c,Lease_Description__c, Location__c, Location__latitude__s, Location__longitude__s, lease_status__c, Tenant_Profile__c, Photo__c FROM Web_Properties__c WHERE Id=\'' + propertyID + '\' and Web_Ready__c=\'' + webready + '\'';

        //String overview;
               
        //string leasecontacts;
        String tenantProfile;
        DateTime acquired;
        list<string> images=new list<string>();
        list<string> relatedresourcesurls=new list<string>();
        
        property = Database.query(query);

        
     /*   if(property.get('Long_Description__c') != null){
         string description= String.valueOf(property.get('Long_Description__c'));
            leasecontacts= description.substringAfter('Leasing Contacts:');
            if(leasecontacts==null || leasecontacts=='')
            {
            leasecontacts = description.substringAfter('Leasing Contact:');
            }
            overview = description.substringBefore('Leasing Contacts:');
            if(overview==null || overview=='')
            {
            overview= description.substringBefore('Leasing Contact:');
            }
            
            
        } else {
            overview = '';
            leasecontacts = '';
        }*/
         
        if(property.get('ImagesforLease__c') != null){
            images = String.valueOf(property.get('ImagesforLease__c')).split(',');
        }
        
        if(property.get('Related_Resources__c') != null){
            relatedresourcesurls = String.valueOf(property.get('Related_Resources__c')).split(',');
        }
        
    
        
        html =  '<div class="ocms_WebPropertyDetail" style="margin-top:6px !important">'+
                '<div id="ctl11_ctlPropertyDetail_divGoBack" style="height: 25px; padding-top: 5px;">';
           
     String title =  String.valueof(property.get('Web_Name__c'));
     
      String Sqft = '0';
      if (property.get('sqFt__c') != null)
          Sqft = String.valueof(property.get('sqFt__c'));    
      Integer length = Sqft.length();
      String formatedSqft = '';
      String temp = '';
      for (Integer i=length;i>0;i=i-3)
      {
          if (i>3)
              temp = Sqft.substring(i-3,i);
          else
              temp = Sqft.substring(0,i);
              
          if (formatedSqft != '')
              formatedSqft = temp + ',' + formatedSqft;
          else
              formatedSqft = temp;
      }

              
        html  +='<a href="/PropertiesLease" style="font-color:blue;">Back</a>'+
                '       <input type="hidden" id="latitude" value="' + property.get('Location__latitude__s') + '" /> ' +
                '       <input type="hidden" id="longitude" value="' + property.get('Location__longitude__s') + '" /> ' +
                '       <div class="ocms_title"><h3>' + title  + '</h3></div>' +
                '       <div class="ocms_infoSection">' +
                '           <div class="ocms_address">' +
                '               <div class="ocms_streetAddress">' + property.get('Address__c') + '</div>' +
                '               <div class="ocms_cityStateZip">' + property.get('City__c') + ', ' + property.get('State__c') + ' ' + property.get('Zip_Code__c') + '</div>' +
                '           </div>' +
                '           <table class="ocms_propertyData">' +
                '               <tr>' +
                '                   <td>Gross Leaseable Area:</td>' +
                '                   <td>' + formatedSqft  + '</td>' +
                '               </tr>' +
                '               <tr>' +
                '                   <td>Property Type:</td>' +
                '                   <td>' + String.valueOf(property.get('Property_Type__c')).replace('Multi-Tenant','')  + '</td>' +
                '               </tr>' +
                '               <tr>' +
                '                   <td>Lease Status:</td>' ;
                if(property.get('lease_Status__c')!=null && property.get('lease_Status__c')!='' && property.get('lease_Status__c')!='--None--')
                {
                html +='                   <td>' + property.get('lease_Status__c') + '</td>';
                }
         /*       else
                {
                html +='                   <td>Contact Us for Availability</td>';
                }*/
                html +='               </tr>' +
                '               <tr>' +
                '                   <td>Latitude:</td>' +
                '                   <td>' + property.get('Location__latitude__s') + '</td>' +
                '               </tr>' +
                '               <tr>' +
                '                   <td>Longitude:</td>' +
                '                   <td>' + property.get('Location__longitude__s') + '</td>' +
                '               </tr>' +
                '           </table>' +
                '</div>'+
                '       <div class="detailPhotos">'+
                '           <div class="ocms_photo"><img id="ctl11_ctlPropertyDetail_imgLarge" src="' + property.get('Photo__c') + '" /></div>' ;
                for(string img:images)
                {
        html+=  '<div class="detailPhotoThumb"><img src="'+img+'" width="38" height="38" /></div>'; 
                }
                
        html+=  '       </div>' +
                '       <div class="ocms_controls">' ;
                
                if(property.get('Long_Description__c') != null || property.get('Lease_Description__c') != null)
                {
                
                html+=' <div class="ocms_overview">Overview</div>';
                }
                
                              
                html+=' <div class="ocms_map">Map</div>' +
                '       </div>';
                
                if(property.get('Long_Description__c') != null || property.get('Lease_Description__c') != null)
                {
                html+='       <div class="ocms_overviewTab" id="overviewtab">';
            //    '           <div class="ocms_title">Overview</div>' +
            html+='<div class="ocms_body" style="white-space:pre-wrap;"><br/>';
            
            if(property.get('Long_Description__c') != null)
              html+= property.get('Long_Description__c') ;
            if(property.get('Lease_Description__c') != null)
              html+= property.get('Lease_Description__c');
             
                          html+='</div>' ;
            //    '           <div class="ocms_body" style="white-space:pre-wrap;">' + overview + '' +
            //    '<br/><div class="ocms_body" style="white-space:pre;"><b>Leasing Contacts:</b><br/>' + leasecontacts + '</div></div>' +
                
            
                if(relatedresourcesurls.size()>0)
                {
                    html+='<div class="leasing-right-box">'+
                    '<h4>Related Resources</h4> ';
                    for(string ab:relatedresourcesurls)
                    {
                        html+='<p><a href="'+ab.substringAfter(':')+'" target="'+property.get('Web_Name__c')+ab.substringBefore(':')+'" download>'+ab.substringBefore(':')+'</a></p>';
                    }
                }               
                html+= ' </div></div>' ;
                }
                
                html+='       <div class="ocms_mapTab" id="maptab">' +
                '           <div id="map-canvas"></div>' +
                '       </div>' +
                '   </div> </div>' ;
                
                html += '<script src="resource/1421371411000/ocms_propertiesforleaseDetail" type="text/javascript"></script>';
                
                if(property.get('Long_Description__c') != null || property.get('Lease_Description__c') != null)
                {
                html+='<script>$("#maptab").hide();</script>';
                }
                else
                {
                html+='<script>$("#overviewtab").hide()</script>';
                }
                
                
        return html;
    }

}