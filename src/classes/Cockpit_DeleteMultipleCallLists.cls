global with sharing class Cockpit_DeleteMultipleCallLists implements Database.Batchable<SObject>{
    global String Query; 
    global set<id> aclId = new set<id>(); 
    global list<Automated_Call_List__c> acl = new list<Automated_Call_List__c>();
    //global String alname='';
    global list<Automated_Call_List__c> locked = new list<Automated_Call_List__c>(); 
    global list<Automated_Call_List__c> lockedlist = new list<Automated_Call_List__c>(); 
    global list<Automated_Call_List__c> lockedlist1 = new list<Automated_Call_List__c>(); 
    global list<string> aclnames = new list<string>();
    global Cockpit_DeleteMultipleCallLists(Set<ID> clId) { 
    aclId = clId; 
    query = 'Select Id from contact_call_list__c Where Call_Complete__c = false and Automated_Call_List__c in:aclId';
    acl = [Select Id, Name,Archived__c,IsDeleted__c From Automated_Call_List__c Where Id in:aclId];
    locked = [Select Id, Name, Templock__c from Automated_Call_List__c where Id in:aclId];
    for(Automated_Call_List__c locked1:locked){
      locked1.Templock__c = 'General Lock';
      lockedlist.add(locked1);
    }
    if(Schema.sObjectType.Automated_Call_List__c.isUpdateable())
      update lockedlist;
}
global Database.QueryLocator start(Database.BatchableContext BC) { 
    system.debug('This is start method');
    system.debug('Myquery is......'+Query);
    return Database.getQueryLocator(query); 
    //return null;
} 

global void execute(Database.BatchableContext BC,List<contact_call_list__c> scope) { 
    List<contact_call_list__c> callist = new List<contact_call_list__c>();
    for(contact_call_list__c clsss : scope){
    clsss.IsDeleted__c = true;
    clsss.IsDeleted_Date__c= date.today();
    callist.add(clsss);
    }
    try {
      if(Schema.sObjectType.contact_call_list__c.isUpdateable())
        update callist;
    } 
    catch (exception e) {
    System.debug('ccl not updated: ' + e);
    }
}


global void finish(Database.BatchableContext BC) {
    String subject='Deletion of Call List ';
    String body='Following Call list have been deleted';
    String automatedCallListNames='';
    //list<Automated_Call_List__c> acllist = new list<Automated_Call_List__c>();
    list<Automated_Call_List__c> acl = [Select Id, Name,IsDeleted__c From Automated_Call_List__c Where Id in:aclId];
    if(acl.size()>1){
       subject='Deletion of Call Lists ';
       body='Following Call lists have been deleted';
    }       
    for(Automated_Call_List__c acl1:acl){
      acl1.IsDeleted__c=true ;
      automatedCallListNames += acl1.Name+'<br>';
    }
    AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()]; 
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    String[] toAddresses = new String[] {a.CreatedBy.Email};
    String[] bccAddresses = new String[] {'skalamkar@colecapital.com'};
    mail.setToAddresses(toAddresses);
    mail.setBccSender(true);
    mail.setBccAddresses(bccAddresses);
    mail.setSubject(subject + a.Status);
    string htmlbody='<html><body>'+body+'<br>' + automatedCallListNames + '<br> </body></html>';
    mail.setHtmlBody(htmlbody);

    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    for(Automated_Call_List__c locked1:locked){
    locked1.Templock__c = '';
    lockedlist1.add(locked1);
    }
    if(Schema.sObjectType.Automated_Call_List__c.isUpdateable())
      update lockedlist1;
}

}