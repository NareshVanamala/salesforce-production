@isTest(seeAlldata=true)
private class Test_EmployeeUserChangeClass1 {
    static testmethod void EmployeeUserChangeClasstest(){
     user u =[select id,name from user where id=:userinfo.getuserid()];
    NewHireFormProcess__c nh = new NewHireFormProcess__c();
    //nh.recordtypeid = NewhireRequest;
    nh.First_Name__c = 'Testing';
    nh.Last_Name__c = 'parent';
    nh.Supervisor__c = 'Ninad';
    nh.Office_Location__c = 'Chennai';
    nh.status__c='New';
    nh.HR_ManagerApprovalStatus__c = 'Submitted For Approval';
    nh.Employee_Role__c ='Developer';
    nh.DepartmentUpdate__c = '01-External Sales';
    nh.updating__c = false;
    insert nh;
    
    
    
    List<Employee__c> emplist = new List<Employee__c>();
    Employee__c emp = new Employee__c();
    emp.name = 'Test';
    emp.Start_Date__c= system.today();
    emp.Supervisor__c = 'Rajesh';
    emp.First_Name__c ='N';
    emp.Title__c ='Title';
    emp.Last_Name__c ='Test';
    emp.Move_User_To__c ='move user';
    emp.Office_Location__c ='Hyderabad';
    emp.Employee_Role__c  ='Developer';
    emp.Employee_Status__c='On Boarding inProcess';
    emp.Terminated__c = false;
    emp.Department__c='01-External Sales';
    emp.NewHireFormProcess__c = nh.id;
    insert emp;
    
    system.assertequals(emp.NewHireFormProcess__c, nh.id);
    

    
    Employee__c emp1 = new Employee__c();
    emp1.name = 'Test1';
    emp1.Start_Date__c= system.today();
    emp1.Supervisor__c = 'Ninad';
    emp1.First_Name__c ='Priya';
    emp1.Title__c ='Title';
    emp1.Last_Name__c ='Baskar';
    emp1.Move_User_To__c ='move user';
    emp1.Office_Location__c ='Chennai';
    emp1.Employee_Role__c  ='Developer';
    emp1.Employee_Status__c='On Boarding inProcess';
    emp1.Terminated__c = false;
    emp1.Department__c='01-External Sales';
    emp1.NewHireFormProcess__c = nh.id;
    system.assertequals(emp1.NewHireFormProcess__c,nh.id);

    //insert emp1;
    

    emplist.add(emp);
    //emplist.add(emp1);
    
    //insert emplist;
    System.debug(emplist.size());
    
     ApexPages.currentPage().getParameters().put('id',emp.id);       
    EmployeeUserChangeClass eclass = new EmployeeUserChangeClass();
    eclass.getSearchString();
    eclass.setSearchString('s');
    ApexPages.currentPage().getParameters().put('edt',emp.id);
    eclass.Selected();
    eclass.SelectedforTermination();
     eclass.search();
    eclass.runSearch();
    eclass.performSearch('N Test');
    //eclass.submit();
    eclass.CreateEmp();
   
    
    }
}