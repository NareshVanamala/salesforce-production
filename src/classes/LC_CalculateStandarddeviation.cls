global class LC_CalculateStandarddeviation
{
    @InvocableMethod
    public static void CalculateStandarddeviation(list<string>lcpid)
    {
        String LCid;
        if(lcpid.size()>0)
          LCid=  lcpid[0];
       
        LC_LeaseOpprtunity__c LCR=[select id,Standard_Deviation__c,Actual_NER__c,Budgeted_NER__c from LC_LeaseOpprtunity__c where id=:LCid];
       
        //double sum = 0;
        //double annualNER;
        //double BudgetedNER;
        
        double sum=0;
        double actualNER;
        double BudgetedNER;
       
        list<Integer > numbers=new list<Integer >();
        if((LCR!=null)&&(LCR.Actual_NER__c!= null)&&(LCR.Budgeted_NER__c!=null))
        {
           //annualNER=double.valueof(LCR.Actual_NER__c);
           //BudgetedNER=Double.valueof(LCR.Budgeted_NER__c); 
           //numbers.add(annualNER);
           //numbers.add(BudgetedNER);
           

           if((LCR.Actual_NER__c.containsAny('$'))&&(LCR.Actual_NER__c!= null))
           {
             //String mystring='$16.65';
                integer lenght1= LCR.Actual_NER__c.length();
                string s=LCR.Actual_NER__c.substring(1,lenght1);
                system.debug('checkthe vlue'+s);
               actualNER=double.valueOf(s);
           }  
           else if(LCR.Actual_NER__c!=null)
           {
             actualNER=double.valueOf(LCR.Actual_NER__c);
           }
            if((LCR.Budgeted_NER__c.containsAny('$'))&&(LCR.Budgeted_NER__c!=null))
           {
              integer lenght1= LCR.Budgeted_NER__c.length();
                string s=LCR.Budgeted_NER__c.substring(1,lenght1);
                system.debug('checkthe vlue'+s);
               BudgetedNER=double.valueOf(s);
             
           } 
          else if(LCR.Budgeted_NER__c!=null)
          {
           BudgetedNER=double.valueOf(LCR.Budgeted_NER__c);
          }
           
           
       
        system.debug('This is my actual nER'+actualNER);
        system.debug('This is my Budgeted nER'+BudgetedNER);
            /*for(Integer d : numbers)
            {
               system.debug('Check the sum'+sum );
               system.debug('Check the Number'+d);
                sum += d;
            }*/
          sum=  actualNER+BudgetedNER;
          double mean = sum /2 ;
        
        system.debug('This is my sun'+sum);
        system.debug('This is my sun'+mean );
       
       Double squaredDifferencesSum = 0;
        double actualNERDiff= Math.pow((actualNER-mean),2);
        double BudgetedNERDiff=Math.pow((BudgetedNER-mean),2); 
        system.debug('ActualNERDifference'+actualNERDiff);
        system.debug('BudgetedNERDiff'+BudgetedNERDiff);
      
      squaredDifferencesSum = actualNERDiff+BudgetedNERDiff;
        system.debug('<<<>>>>>squaredDifferencesSum'+actualNERDiff);
        
          Double standardDeviation = Math.sqrt(squaredDifferencesSum );
          decimal stry=decimal.valueOf(standardDeviation );
           decimal std=stry.setScale(1, RoundingMode.HALF_UP);
         system.debug('This is the deviation'+std);
         
        LCR.Standard_Deviation__c =string.valueOf(std);
        update LCR;
        
     }   
       
    }
    
}