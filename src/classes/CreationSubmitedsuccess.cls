public with sharing class CreationSubmitedsuccess {

    public PageReference home() {
        
        PageReference NewHireFormPage = new PageReference ('/apex/EmployeeUserChangeForm');
        NewHireFormPage .setRedirect(true);
        return NewHireFormPage ;
        
    }
    
}