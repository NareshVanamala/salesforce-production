global virtual with sharing class coleCaptialCFGAPICtrl{
    public static string jsonResponseRemote{get;set;}
    public String contactId {get;set;}
    public String accountId {get;set;}
    public String mailingState {get;set;}
    public String usergroup {get;set;}
     global coleCaptialCFGAPICtrl() {
     String query;
     system.debug('UserID'+UserInfo.getUserId());
     //if(!Test.isRunningTest()){
     if (UserInfo.getUserId()!= System.Label.CFG_Guest_User_Id){
     user usr = [select id,ContactId from user where id=:UserInfo.getUserId() limit 1];
     List<contact> conList = new List<contact>();
     conList = [select id,Accountid,mailingstate,RecordType.Name,Name from contact where id =: usr.contactId limit 1];
     if(!conList.isEmpty()){
        contactId = conList[0].id;
        accountId = conList[0].Accountid;
        mailingState = conList[0].mailingstate;
        usergroup = conList[0].RecordType.Name.replaceAll(' ','%20');  
        system.debug('User123'+usr);
        system.debug('Contact123'+conList[0]);
     }
     }
     else{
     usergroup = 'Web%20Anonymous';
     }
     if(Test.isRunningTest()){
        List<contact> testconList = new List<contact>();
        testconList = [select id,Accountid,mailingstate,RecordType.Name,Name from contact limit 1];
     if(!testconList.isEmpty()){
        contactId = testconList[0].id;  
     } 
         
     }
     }
    public string createList(String ListName){
        system.debug('**********ListName*************'+ListName);
        Forms_Literature_List__c literatureList = new Forms_Literature_List__c();
        literatureList.Contact__c = contactId;
        literatureList.List_Name__c =ListName;
        insert literatureList;
        return literatureList.id;
    }
     public string renameLiteratureList(String ListId, String ListName){
        system.debug('**********ListName*************'+ListName);
        Forms_Literature_List__c literatureList = new Forms_Literature_List__c();
        literatureList = [select id from Forms_Literature_List__c where id=:ListId];
        literatureList.List_Name__c=ListName;
        update literatureList;
        return literatureList.id;
    }
    public integer addToMultipleBookmark(string selectedListName,string bookMarkJson){
            system.debug('*******selectedListName******'+selectedListName+'*******bookMarkJson******'+bookMarkJson);
            List<Forms_Literature_List__c> formLitList = new List<Forms_Literature_List__c>();
            List<Forms_Literature_List__c> formLitList1 = new List<Forms_Literature_List__c>();
            formLitList = [Select id,List_Name__c from Forms_Literature_List__c where List_Name__c ='Default' and contact__c=:contactId];
            //if(selectedListName != 'Deafult'){
            formLitList1 = [Select id,List_Name__c from Forms_Literature_List__c where List_Name__c =:selectedListName and contact__c=:contactId];
            system.debug('**********selectedListName*************'+selectedListName);
            system.debug('**********formLitList1*************'+formLitList1);
            string literatureList='';
                      if(formLitList.isEmpty() && selectedListName=='Default'){
                            system.debug('**********formLitList enter one*************'+selectedListName); 
                            literatureList =  CreateList('Default');
                        }else if(!formLitList.isEmpty() && selectedListName=='Default'){
                            system.debug('**********formLitList enter two*************'+selectedListName); 
                            literatureList =  formLitList[0].id;
                        }else if(selectedListName!='Default' && !formLitList1.isEmpty()){
                            system.debug('**********formLitList enter three*************'+selectedListName);
                            literatureList =  formLitList1[0].id;
                        }
                        else if(selectedListName!='Default' && formLitList1.isEmpty()){
                            system.debug('**********formLitList enter four*************'+selectedListName);
                            literatureList =  CreateList(selectedListName);
                        }
            String[] result = bookMarkJson.split('\\*\\-\\-\\;\\-\\-\\*');
            system.debug('**********formLitList123 result*************'+result);
            list<Forms_Literature_Bookmarks__c> bookMarkList = new list<Forms_Literature_Bookmarks__c>();
            if(!result.isEmpty()){
                 for(integer i=0;i<result.size();i++){
                     system.debug('**********formLitList123 result.size()*************'+result.size());
                     String[] finalresult= result[i].split('\\*\\-\\-\\*');
                     system.debug('**********formLitList123456 finalresult*************'+finalresult);
                     system.debug('**********formLitList123 finalresult.size()*************'+finalresult.size());
                     Forms_Literature_Bookmarks__c  bookmarkObj = new Forms_Literature_Bookmarks__c();
                        bookmarkObj.Title__c = finalresult[0];
                        system.debug('**********formLitList123456 bookmarkObj.Title__c*************'+bookmarkObj.Title__c);
                        bookmarkObj.Description__c = finalresult[1];
                        system.debug('**********formLitList123456 finalresult[1]*************'+finalresult[1]);
                        bookmarkObj.Contact__c = contactId;
                        bookmarkObj.Download_File_URL__c = finalresult[2];
                        system.debug('**********formLitList123456 finalresult[2]*************'+finalresult[2]);
                        bookmarkObj.Max_Order__c = decimal.valueOf(finalresult[3]);
                        system.debug('**********formLitList123456 finalresult[3]*************'+finalresult[3]);
                        bookmarkObj.SKU__c = finalresult[4];
                        system.debug('**********formLitList123456 finalresult[4]*************'+finalresult[4]);
                        bookmarkObj.Thumbnail_URL__c = finalresult[5];
                        system.debug('**********formLitList123456 finalresult[5]*************'+finalresult[5]);
                        
                        string Dt = finalresult[6];
                        String[] str = dt.split(' ');
                        String[] dts = str[0].split('/');
                        Date myDate = date.newinstance(Integer.valueOf(dts[2]), Integer.valueOf(dts[0]), Integer.valueOf(dts[1]));
                        System.debug('--------------------------'+myDate);

                        bookmarkObj.Published_Date__c = myDate;
                        system.debug('**********formLitList123456 finalresult[6]*************'+finalresult[6]);
                        bookmarkObj.Expiration_Date__c = finalresult[7];
                        system.debug('**********formLitList123456 finalresult[7]*************'+finalresult[7]);
                        if(finalresult.size()>9){
                        system.debug('**********formLitList123456 above finalresult[9]*************'+finalresult[9]);
                        bookmarkObj.FINRA_PDF__c = '*---*'+finalresult[9];
                        system.debug('**********formLitList123456 finalresult[9]*************'+finalresult[9]);
                        }
                        if(finalresult.size()>10){
                        system.debug('**********formLitList123456 above finalresult[10]*************'+finalresult[9]);
                        bookmarkObj.FINRA_PDF__c += '*---*'+finalresult[10];
                        system.debug('**********formLitList123456 finalresult[10]*************'+finalresult[9]);
                        }
                        bookmarkObj.Forms_Literature_List__c = literatureList;
                        bookmarkObj.External_Id__c = contactId + bookmarkObj.Forms_Literature_List__c + finalresult[4];
                        bookMarkList.add(bookmarkObj);
                }
                if(!bookMarkList.isEmpty()){
                    system.debug('**********BookMark Details************'+bookMarkList); 
                    upsert bookMarkList External_Id__c;
                }
                  
             }                   
        List<Forms_Literature_Bookmarks__c> bookMarkListFinal = new List<Forms_Literature_Bookmarks__c>();
        bookMarkListFinal = [select id from Forms_Literature_Bookmarks__c where Contact__c=:contactID];
        return bookMarkListFinal.size();
    }
      public integer addToBookmark(string selectedListName,string title,string description,string url,integer maxorder,string sku,string thumbnailUrl,string pubDate,string expDate,string finraPDF,boolean isCount){
                       
            Forms_Literature_Bookmarks__c  bookmarkObj = new Forms_Literature_Bookmarks__c();
            
            bookmarkObj.Title__c = title;
            bookmarkObj.Description__c = description;
            bookmarkObj.Contact__c = contactId;
            bookmarkObj.Download_File_URL__c = url;
            bookmarkObj.Max_Order__c = decimal.valueOf(maxorder);
            bookmarkObj.SKU__c = sku;
            bookmarkObj.Thumbnail_URL__c = thumbnailUrl;
            string Dt = pubDate;
            String[] str = dt.split(' ');
            String[] dts = str[0].split('/');
            Date myDate = date.newinstance(Integer.valueOf(dts[2]), Integer.valueOf(dts[0]), Integer.valueOf(dts[1]));
            System.debug('--------------------------'+myDate);
            bookmarkObj.Published_Date__c = myDate;
            bookmarkObj.Expiration_Date__c = expDate;
            bookmarkObj.FINRA_PDF__c = finraPDF;
            
            system.debug('**********BookMark Details************'+bookmarkObj); 
            List<Forms_Literature_List__c> formLitList = new List<Forms_Literature_List__c>();
            List<Forms_Literature_List__c> formLitList1 = new List<Forms_Literature_List__c>();
            formLitList = [Select id,List_Name__c from Forms_Literature_List__c where List_Name__c ='Default' and contact__c=:contactId];
            //if(selectedListName != 'Deafult'){
            formLitList1 = [Select id,List_Name__c from Forms_Literature_List__c where List_Name__c =:selectedListName and contact__c=:contactId];
            system.debug('**********selectedListName*************'+selectedListName);
            system.debug('**********formLitList1*************'+formLitList1);
           // }
            if(formLitList.isEmpty() && selectedListName=='Default'){
                system.debug('**********formLitList enter one*************'+selectedListName); 
                bookmarkObj.Forms_Literature_List__c =  CreateList('Default');
            }else if(!formLitList.isEmpty() && selectedListName=='Default'){
                system.debug('**********formLitList enter two*************'+selectedListName); 
                bookmarkObj.Forms_Literature_List__c =  formLitList[0].id;
            }else if(selectedListName!='Default' && !formLitList1.isEmpty()){
                system.debug('**********formLitList enter three*************'+selectedListName);
                bookmarkObj.Forms_Literature_List__c =  formLitList1[0].id;
            }
            else if(selectedListName!='Default' && formLitList1.isEmpty()){
                system.debug('**********formLitList enter four*************'+selectedListName);
                bookmarkObj.Forms_Literature_List__c =  CreateList(selectedListName);
            }
            system.debug('**********bookmarkObj*************'+bookmarkObj);
            if(isCount != True){
            bookmarkObj.External_Id__c = contactId + bookmarkObj.Forms_Literature_List__c + sku;
            upsert bookmarkObj External_Id__c;
            }
        List<Forms_Literature_Bookmarks__c> bookMarkList = new List<Forms_Literature_Bookmarks__c>();
        bookMarkList = [select id from Forms_Literature_Bookmarks__c where Contact__c=:contactID];
        return bookMarkList.size();
    }
     public List<SObject> getBookmarkList(){
        List<SObject> bookMarkList;
        String query;
        
        
        query = 'select List_Name__c,contact__c,Bookmarks_Count__c,id from Forms_Literature_List__c where Contact__c=\'' + contactId + '\' Order by List_Name__c';
           
        System.Debug('query123456'+query );
       
        bookMarkList = Database.query(query);
        System.Debug('bookMarkList'+bookMarkList );
       
        return bookMarkList;
    }
    public List<SObject> getBookmarks(string listname, string sortOrder){
        List<SObject> bookMarkData;
        String query;    
        //query ='select List_Name__c ,(select Description__c,Pub_Date__c,Download_File_URL__c,Max_Order__c,SKU__c,Published_Date__c,Expiration_Date__c,FINRA_PDF__c,Thumbnail_URL__c,Title__c,Forms_Literature_List__c,contact__c  from Forms_Literature_Bookmarks__r) from Forms_Literature_List__c where Contact__c=\'' + contactId + '\' group by List_Name__c';
       if(listname == 'All'){
        query = 'select Description__c,contact__r.Name,Pub_Date__c,Download_File_URL__c,Max_Order__c,SKU__c,Published_Date__c,Expiration_Date__c,FINRA_PDF__c,Thumbnail_URL__c,Title__c,Forms_Literature_List__c,contact__c from Forms_Literature_Bookmarks__c where Contact__c=\'' + contactId + '\'';            
       }
       else{
       query = 'select Description__c,contact__r.Name,Pub_Date__c,Download_File_URL__c,Max_Order__c,SKU__c,Published_Date__c,Expiration_Date__c,FINRA_PDF__c,Thumbnail_URL__c,Title__c,Forms_Literature_List__c,contact__c from Forms_Literature_Bookmarks__c where Contact__c=\'' + contactId + '\' and Forms_Literature_List__c=\''+listName+'\'';         
       }
       if (sortOrder=='title' || sortOrder=='undefined' || sortOrder==''){
        query += 'Order By Title__c';
        }else if (sortOrder!='title'){
        query += 'Order By Published_Date__c Desc';
        }
        System.Debug('sortOrder123'+sortOrder );
        System.Debug('query123456'+query );     
        bookMarkData = Database.query(query);
        System.Debug('bookMarkData'+bookMarkData );     
        return bookMarkData;
    }
     public List<SObject> getBookmarkListsWithSKU(string skuName){
        List<SObject> bookMarkListsWithSKU;
        String query;    
        //query ='select List_Name__c ,(select Description__c,Download_File_URL__c,Max_Order__c,SKU__c,Published_Date__c,Expiration_Date__c,FINRA_PDF__c,Thumbnail_URL__c,Title__c,Forms_Literature_List__c,contact__c  from Forms_Literature_Bookmarks__r) from Forms_Literature_List__c where Contact__c=\'' + contactId + '\' group by List_Name__c';
        query = 'select Forms_Literature_List__r.List_Name__c from Forms_Literature_Bookmarks__c where Contact__c=\'' + contactId + '\' and SKU__c=\''+skuName+'\' GROUP BY Forms_Literature_List__r.List_Name__c';         
        System.Debug('SKULIST Query'+query );     
        bookMarkListsWithSKU = Database.query(query);
        System.Debug('SKULIST Data'+bookMarkListsWithSKU );     
        //return bookMarkListsWithSKU;
        return sanitizeList(bookMarkListsWithSKU, 'List_Name__c');
    }
    public List<SObject> getBookmarkListsWithOutSKU(string skuName){
        List<SObject> bookMarkListsWithOutSKU;
        String query;    
        query ='select List_Name__c from Forms_Literature_List__c where Contact__c=\'' + contactId + '\'';
        //query = 'select Forms_Literature_List__r.List_Name__c  from Forms_Literature_Bookmarks__c where Contact__c=\'' + contactId + '\' and SKU__c !=\''+skuName+'\' GROUP BY Forms_Literature_List__r.List_Name__c';         
        System.Debug('getBookmarkListsWithOutSKU Query'+query );     
        bookMarkListsWithOutSKU = Database.query(query);
        System.Debug('getBookmarkListsWithOutSKU Data'+bookMarkListsWithOutSKU );     
        //return bookMarkListsWithOutSKU;
        return sanitizeList(bookMarkListsWithOutSKU, 'List_Name__c');
    }
    private List<SObject> sanitizeList(List<SObject> inList, String field){

        Integer ctr = 0;

        while(ctr < inList.size()){
            if(inList[ctr].get(field) == null){
                inList.remove(ctr);
            } else {
                ctr++;
            }
        }
        return inList;

    }
    public integer removeItemFromCart(string cartObjId){
        List<Forms_Literature_Shopping_Cart__c> cartList = new List<Forms_Literature_Shopping_Cart__c>();
        if(cartObjId == 'All'){
        cartList = [select id from Forms_Literature_Shopping_Cart__c where contact__c=:contactId];     
        }
        else{
        cartList = [select id from Forms_Literature_Shopping_Cart__c where id=:cartObjId];
        }
        delete cartList;
        List<Forms_Literature_Shopping_Cart__c> returncartList = new List<Forms_Literature_Shopping_Cart__c>();
        returncartList = [select id,contact__r.Name from Forms_Literature_Shopping_Cart__c where Contact__c=:contactID];
        //string cart = returncartList.size() + '$$&&' + returncartList[0].contact__r.Name;
        integer cartSize = returncartList.size();
        system.debug('**********cartSize *************'+cartSize);
        if(returncartList.size() == 0)
        {
            cartSize = 0;
        }
        //string jsonResult = returncartList.size()+'&'+UserInfo.getUserName();
        return cartSize;
        //return 'success';
    }
    public integer removeItemFromBookmarks(string bookmarkObjId){
        List<Forms_Literature_Bookmarks__c> bookmarkList = new List<Forms_Literature_Bookmarks__c>();
        bookmarkList = [select id from Forms_Literature_Bookmarks__c where id=:bookmarkObjId];
        delete bookmarkList;
        List<Forms_Literature_Shopping_Cart__c> returncartList = new List<Forms_Literature_Shopping_Cart__c>();
        returncartList = [select id,contact__r.Name from Forms_Literature_Shopping_Cart__c where Contact__c=:contactID];
        //string cart = returncartList.size() + '$$&&' + returncartList[0].contact__r.Name;
        integer cartSize = returncartList.size();
        system.debug('**********removeItemFromBookmarks cartSize *************'+cartSize);
        if(returncartList.size() == 0)
        {
            cartSize = 0;
        }
        //string jsonResult = returncartList.size()+'&'+UserInfo.getUserName();
        return cartSize;
        //return 'success';
    }
    public integer removeLiteratureList(string listId){
        List<Forms_Literature_List__c> literatureList = new List<Forms_Literature_List__c>();
        literatureList = [select id from Forms_Literature_List__c where id=:listId];
        delete literatureList;
        List<Forms_Literature_Shopping_Cart__c> returncartList = new List<Forms_Literature_Shopping_Cart__c>();
        returncartList = [select id,contact__r.Name from Forms_Literature_Shopping_Cart__c where Contact__c=:contactID];
        //string cart = returncartList.size() + '$$&&' + returncartList[0].contact__r.Name;
        integer cartSize = returncartList.size();
        system.debug('**********removeLiteratureList cartSize *************'+cartSize);
        if(returncartList.size() == 0)
        {
            cartSize = 0;
        }
        //string jsonResult = returncartList.size()+'&'+UserInfo.getUserName();
        return cartSize;
        //return 'success';
    }
    public integer updateItemInCart(string cartObjId,string orderValue){
        System.Debug('cartObjId'+cartObjId);
        System.Debug('orderValue'+orderValue);
        integer orderVal = Integer.valueof(orderValue);
        List<Forms_Literature_Shopping_Cart__c> cartObjList = new  List<Forms_Literature_Shopping_Cart__c>();
        cartObjList = [select id from Forms_Literature_Shopping_Cart__c where id=:cartObjId];
        if(!cartObjList.isEmpty()){
            if(orderVal != 0 ){
            cartObjList[0].Quantity__c= orderVal;
            }
            update cartObjList;
        }
        //return cartObj.id;
        List<Forms_Literature_Shopping_Cart__c> returncartList = new List<Forms_Literature_Shopping_Cart__c>();
        returncartList = [select id,contact__r.Name from Forms_Literature_Shopping_Cart__c where Contact__c=:contactID];
        //string cart = returncartList.size() + '$$&&' + returncartList[0].contact__r.Name;
        integer cartSize = returncartList.size();
        system.debug('**********cartSize *************'+cartSize);
        if(returncartList.size() == 0)
        {
            cartSize = 0;
        }
        //string jsonResult = returncartList.size()+'&'+UserInfo.getUserName();
        return cartSize;
    }
     public integer updateAllItemsInCart(string cartMapJson){
        System.Debug('cartMapJson'+cartMapJson);
        String[] result = cartMapJson.split(',');
        //string removeItemsFromCart ='';
        List<String> removeItemsFromCart = new List<String>();
        List<Forms_Literature_Shopping_Cart__c> cartList = new List<Forms_Literature_Shopping_Cart__c>();
        map<string,string> mapToCart = new map<string,string>();
        list<string> finalMapresult = new list<string>();
        integer order;
        integer maxOrder;
        system.debug('******result******'+result);
        if(!result.isEmpty()){
            for(integer i=0;i<result.size();i++){
                 //if(result[i+1]<)
                 if(result[i+1] != '' && result[i+1] != null){
                 order=integer.valueof(result[i+1]);
                 }
                 else
                 {
                    order=0; 
                 }
                 maxOrder=integer.valueof(result[i+2]);
                 if(order<=maxOrder && order !=0){            
                 mapToCart.put(result[i],result[i+1]); 
                 }
                 if(order ==0)
                 {
                    removeItemsFromCart.add(result[i]);
                 }
                 i=i+2;
             }
        }
        system.debug('**********mapToCart********'+mapToCart);
        list<Forms_Literature_Shopping_Cart__c> formCartList = new list<Forms_Literature_Shopping_Cart__c>();
        if(mapToCart.keySet()!=null){
            for(Forms_Literature_Shopping_Cart__c cartObj :[select id,Quantity__c from Forms_Literature_Shopping_Cart__c where id in :mapToCart.keySet()]){
                if(mapToCart.containsKey(cartObj.id) && mapToCart.get(cartObj.id)!=null){
                    cartObj.Quantity__c= decimal.valueOf(mapToCart.get(cartObj.id));
                }
                formCartList.add(cartObj);
            }
        }
        if(!formCartList.isEmpty()){
            update formCartList;
        }
        if(!removeItemsFromCart.isEmpty()){
            cartList = [select id from Forms_Literature_Shopping_Cart__c where id IN: removeItemsFromCart];
            delete cartList;
        }
        //delete cartList;      
        //}
       //return 'success';
        List<Forms_Literature_Shopping_Cart__c> returncartList = new List<Forms_Literature_Shopping_Cart__c>();
        returncartList = [select id,contact__r.Name from Forms_Literature_Shopping_Cart__c where Contact__c=:contactID];
        //string cart = returncartList.size() + '$$&&' + returncartList[0].contact__r.Name;
        integer cartSize = returncartList.size();
        system.debug('**********cartSize *************'+cartSize);
        if(returncartList.size() == 0)
        {
            cartSize = 0;
        }
        //string jsonResult = returncartList.size()+'&'+UserInfo.getUserName();
        return cartSize;
    }
    public List<SObject> getCartList(){
        List<SObject> cartList;
        String query;
        //string contactId='0035000002JwVTY';
        
        query = 'select Title__c,Description__c,contact__r.Name,Download_File_URL__c,Published_Date__c,Expiration_Date__c,FINRA_PDF__c,Max_Order__c,Quantity__c,SKU__c,Thumbnail_URL__c from Forms_Literature_Shopping_Cart__c where Contact__c=\'' + contactId + '\'';
           
        System.Debug('query123456'+query );
       
        cartList = Database.query(query);
        System.Debug('cartList123'+cartList );
       
        return cartList;
    }
     public List<SObject> getContactData(){
        List<SObject> contactData;
        String query;
        //string contactId='0035000002JwVTY';
        
        query = 'select Name,lastname,firstname,Phone,Email,MailingStreet,MailingCity,MailingState,MailingPostalCode,Account.Name from Contact where id=\'' + contactId + '\'';
           
        System.Debug('contactData Query'+query );
       
        contactData = Database.query(query);
        System.Debug('contactData'+contactData );
       
        return contactData;
    }
    public integer saveCart(string title,string description,string url,integer maxorder,integer quantity,string sku,string thumbnailUrl,string pubDate,string expDate,string finraPDF,boolean isCount){
        if(!isCount){
            Forms_Literature_Shopping_Cart__c  cartObj = new Forms_Literature_Shopping_Cart__c();
            cartObj.Title__c = title;
            cartObj.Description__c = description;
            cartObj.Contact__c = contactId;
            cartObj.Download_File_URL__c = url;
            cartObj.Max_Order__c = decimal.valueOf(maxorder);
            cartObj.Quantity__c = decimal.valueOf(quantity);
            cartObj.SKU__c = sku;
            //cartObj.Subscribe__c = decimal.valueOf(subscibe);
            cartObj.Thumbnail_URL__c = thumbnailUrl;
            cartObj.Published_Date__c = pubDate;
            cartObj.Expiration_Date__c = expDate;
            cartObj.FINRA_PDF__c = finraPDF;
            cartObj.External_Id__c = contactId+sku;
            system.debug('**********cartObj*************'+cartObj);
            upsert cartObj External_Id__c;
        }
        
        List<Forms_Literature_Shopping_Cart__c> cartList = new List<Forms_Literature_Shopping_Cart__c>();
        cartList = [select id,contact__r.Name from Forms_Literature_Shopping_Cart__c where Contact__c=:contactID];
        //string cart = cartList.size() + '$$&&' + cartList[0].contact__r.Name;
        integer cartSize = cartList.size();
        system.debug('**********cartSize *************'+cartSize);
        if(cartList.size() == 0)
        {
            cartSize = 0;
        }
        //string jsonResult = cartList.size()+'&'+UserInfo.getUserName();
        return cartSize;
    }
    public string coleCaptialCFGAPI(string pageName){
        string jsonResponse='';
        string page = pageName;
        string inventory='inventory';
        //Http h = new Http();
        //HttpRequest req1 = new HttpRequest();
        string requestURL;
        string version=System.Label.CFG_Version;
        string accessToken =System.Label.CFG_Access_Token;
        requestURL = System.Label.CFG_Base_URL;
        //requestURL = 'https://mp-advisor.dstweb.com/rest/partner/';
        requestURL+=pageName.replaceAll(' ','%20');
        Boolean result = page.contains(inventory);
        Boolean orderResult = page.contains('order.json');
        Boolean singleOrder = page.contains('order/');
        if(result==TRUE && UserInfo.getUserId()!= System.Label.CFG_Guest_User_Id)
        {
        requestURL+='&state='+mailingState+'&firm='+accountId+'&usergroup='+usergroup;
        }
        if(result==TRUE && UserInfo.getUserId()== System.Label.CFG_Guest_User_Id)
        {
        requestURL+='&usergroup='+usergroup+'&firm=0013000000CV6t5AAD';      
        }
        if(orderResult==TRUE){
            
         //requestURL = 'https://mp-advisor.dstwebuat.com/rest/partner/';
         //requestURL+=pageName.replaceAll(' ','%20');    
         requestURL+='&CRMRecipientID='+UserInfo.getUserId()+'&OrderType='+System.Label.CFG_Order_Type;
        }
        if(singleOrder==TRUE){
         //requestURL = 'https://mp-advisor.dstwebuat.com/rest/partner/';
         //requestURL+=pageName.replaceAll(' ','%20');
         requestURL+='?CRMRecipientID='+UserInfo.getUserId()+'&OrderType='+System.Label.CFG_Order_Type;
        }
        system.debug('requestURL'+requestURL);
        if(!Test.isRunningTest()){
        Http h = new Http();
        HttpRequest req1 = new HttpRequest();
        req1.setEndpoint(requestURL);
        req1.setHeader('Authorization', +accessToken);
        req1.setHeader('Accept',version);
        system.debug('version'+version);
        system.debug('header'+accessToken);
        req1.setMethod('GET');
        req1.setTimeout(120000);
        HttpResponse res1 = h.send(req1);
        //Now we could parse through the JSON again and get the values that we want
        string valuetoShow = 'Forms & Literature Details: ' + res1.getBody();
        system.debug('body'+res1.getBody());
        jsonResponse =res1.getBody();
        //jsonResponse+=Userinfo.getUserName();
        }
        return jsonResponse;
    }
    public string submitOrder(string jsonOrder){
        string jsonResponse='';
        string jsonbody='';
        jsonbody = jsonOrder.replaceAll(' ','%20');
       
        system.debug('*******jsonbody*********'+jsonbody);
       // jsonbo
        //Http h = new Http();
        string version=System.Label.CFG_Version;
        string accessToken =System.Label.CFG_Access_Token;
       // HttpRequest req1 = new HttpRequest();
        string requestURL;
        requestURL = System.Label.CFG_Submit_Order_URL+UserInfo.getUserId()+'&OrderType='+System.Label.CFG_Order_Type;       
        system.debug('requestURL'+requestURL);
        if(!Test.isRunningTest()){
        Http h = new Http();
        HttpRequest req1 = new HttpRequest();
        req1.setEndpoint(requestURL);
        req1.setBody(jsonBody);
        req1.setHeader('Authorization', +accessToken);
        req1.setHeader('Accept',version);
        req1.setHeader('Content-Length', '512');
        system.debug('header'+accessToken);
        req1.setMethod('POST');
        req1.setTimeout(120000);
        HttpResponse res1 = h.send(req1);
        //Now we could parse through the JSON again and get the values that we want
        string valuetoShow = 'Forms & Literature Details: ' + res1.getBody();
        system.debug('body'+res1.getBody());
        jsonResponse =res1.getBody();
        }
        return jsonResponse;
    }
    public string sendEmail(string EmailList,string Notes,string AttachmentUrl){
         system.debug('body'+EmailList+'*****Notes*****'+Notes+'*****AttachmentUrl******'+AttachmentUrl);
         String messageBody = '';
         //integer resultSize;
         messageBody = '<html><body>'+Notes+'\r\n<br/>';
         system.debug('********AttachmentUrl********'+AttachmentUrl);
         String[] result = AttachmentUrl.split('\\&\\&\\$\\$');
         String[] splitResult;
         system.debug('result123'+result);
         if(!result.isEmpty()){
            system.debug('result.size()'+result.size());
            for(integer i=0;i<result.size();i++){
            system.debug('result[i]'+result[i]);
            splitResult = result[i].split('\\$\\$\\&\\&');
            system.debug('********splitResult[0]********'+splitResult[0]);
            system.debug('********splitResult[1]********'+splitResult[1]);
            messageBody += '<a href="'+splitResult[1]+'">'+splitResult[0]+'</a><br/>';
            system.debug('********inner messageBody********'+messageBody);
            
         }
         messageBody += '</body></html>';
         }
         //messageBody = '<html><body>'+Notes+'\r\n<br/>'+AttachmentUrl+'</body> </html>';
         list<string> emailsArray = new list<string>();
         emailsArray = EmailList.split(',');
         system.debug('********emailsArray********'+emailsArray);
         system.debug('********messageBody********'+messageBody);
         Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        //msg.setTemplateId([select id from EmailTemplate where DeveloperName='CFG_Template'].id);
         msg.setToAddresses(emailsArray);
         msg.setTargetObjectId(contactId);
         msg.setSaveAsActivity(true);
         msg.setSubject('Document Requested from www.colecapital.com');
         msg.setHtmlBody(messageBody);
         Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg });
         return null;
    }
}