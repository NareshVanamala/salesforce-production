global class Eventupdate implements Database.Batchable<SObject>
{
list<contact> contactlist = new list<contact>();
list<contact> clist = new list<contact>();
list<contact> conlist = new list<contact>();
list<RecordType> rrrecordtype= new list<RecordType>(); 
global Database.QueryLocator Start(Database.BatchableContext BC){

String Query;
rrrecordtype = [select Id, Name,DeveloperName from RecordType where (DeveloperName ='NonInvestorRepContact' or DeveloperName='Registered_Investment_Advisor_RIA_Contact' or DeveloperName='Dually_Registered_Contact')and SobjectType = 'Contact'];
Query = 'Select Id,Name,RecordTypeId,First_Producer_Date__c,location__c,Next_Planned_External_Appointment__c ,Next_External_Appointment__c ,Last_External_Appointment__c,Priority__c from Contact where RecordTypeId in : rrrecordtype'; 
System.debug('My Query Is.......'+Query);
return Database.getQueryLocator(Query);
}
global void Execute(Database.BatchableContext BC,List<Contact> Scope)
{ 
 
    for(contact c:scope)
     {
     if(c.Next_Planned_External_Appointment__c == null && c.Next_External_Appointment__c == null)
     {
              if(c.Priority__c=='1')
              {
                   if((c.Last_External_Appointment__c!=null)||(c.Last_External_Appointment__c>system.today()))
                   {
                       c.Next_Planned_External_Appointment__c = c.Last_External_Appointment__c + 30; 
                       c.location__c = null; 
                   
                   }
                   else if((c.Last_External_Appointment__c==null)||(c.Last_External_Appointment__c<system.today()))
                   {
                       c.Next_Planned_External_Appointment__c = system.today() + 30; 
                        c.location__c = null;  
                    }
               }
              else if(c.Priority__c=='2' || c.Priority__c=='5')
               {
                     if((c.Last_External_Appointment__c!=null)||(c.Last_External_Appointment__c>system.today()))
                     {
                         c.Next_Planned_External_Appointment__c = c.Last_External_Appointment__c + 90;
                         c.location__c = null; 
                     
                     }
                     
                     else if((c.Last_External_Appointment__c==null)||(c.Last_External_Appointment__c<system.today()))
                     {
                        c.Next_Planned_External_Appointment__c = system.today() + 90;
                         c.location__c = null;  
                       }  
               }
                else if(c.Priority__c=='3' || c.Priority__c=='4' || c.Priority__c=='6')
                {
                        if((c.Last_External_Appointment__c!=null)||(c.Last_External_Appointment__c>system.today()))
                        {
                            c.Next_Planned_External_Appointment__c = c.Last_External_Appointment__c + 120;
                             c.location__c = null;  
                         }
                         
                       else if((c.Last_External_Appointment__c==null)||(c.Last_External_Appointment__c<system.today()))
                       {
                             c.Next_Planned_External_Appointment__c = system.today() + 120;
                             c.location__c = null;  

                       
                       }      
               }
               ContactList.add(c);
              system.debug('contactlist******'+contactlist);
    }
    if(c.Next_External_Appointment__c != null && c.Next_External_Appointment__c < system.now())
    {
               if(c.Priority__c=='1')              
                { 
                                 
                 c.Next_Planned_External_Appointment__c = c.Next_External_Appointment__c + 30; 
                 //c.Last_External_Appointment__c=date.valueOf(c.Next_External_Appointment__c);
                  c.Next_External_Appointment__c = null;
                  c.location__c = null;  
                                 }              
            else if(c.Priority__c=='2' || c.Priority__c=='5')               
            {                        
            c.Next_Planned_External_Appointment__c = c.Next_External_Appointment__c + 90;
             //c.Last_External_Appointment__c=date.valueOf(c.Next_External_Appointment__c);
 
                c.Next_External_Appointment__c = null;  
                 c.location__c = null;  
            }               
             else if(c.Priority__c=='3' || c.Priority__c=='4' || c.Priority__c=='6')   
            {                        
            c.Next_Planned_External_Appointment__c = c.Next_External_Appointment__c + 120;
            //c.Last_External_Appointment__c=date.valueOf(c.Next_External_Appointment__c);
            c.Next_External_Appointment__c = null; 
           c.location__c = null;  
   
             }
             else if(c.Priority__c=='7') {
                c.Next_External_Appointment__c = null; 
                c.location__c = null;
             }
             conlist.add(c);
             System.debug('conlist**********'+conlist);
            
   }  
    if((c.Next_Planned_External_Appointment__c!=null)&&(c.Priority__c=='7'))
     {
          
          c.Next_Planned_External_Appointment__c=null; 
            conlist.add(c);
           
    }
    
              if(c.First_Producer_Date__c != null)
               {
               system.debug(c.First_Producer_Date__c+'**');
               
             if(system.today() <= c.First_Producer_Date__c.adddays(90))
             
            {
             system.debug('clist******'+clist);
                clist.add(c);
            }
            }
    }
   

  if(!ContactList.isEmpty())
   {
        update ContactList;
                
   }
    if(!conlist.isEmpty())
    {
     update conlist;   
   }      
            
     if(clist.size() > 0) 
  {       
         update clist;
        } 
            
}          


 global void finish(Database.BatchableContext BC)
       {
       
       }
       


}