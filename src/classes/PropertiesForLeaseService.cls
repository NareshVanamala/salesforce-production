global with sharing class PropertiesForLeaseService implements cms.ServiceInterface {

    public String JSONResponse {get; set;}
    public String action {get; set;}
    public Map<String, String> parameters {get; set;}
    public PropertiesForLeaseCtrl wpl = new PropertiesForLeaseCtrl();

    global PropertiesForLeaseService() {
        
    }

    global System.Type getType(){
        return PropertiesForLeaseService.class;
    }

    global String executeRequest(Map<String, String> p) {
        this.action = p.get('action');
        this.parameters = p; 
        this.LoadResponse();    
        return this.JSONResponse;
    }

    global void loadResponse(){
    
        if (this.action == 'getStateList'){
            this.getStateList(parameters.get('statename'));
        } else if (this.action == 'getCityList'){
            this.getCityList(parameters.get('state'));
        } else if (this.action == 'getResultTotal'){

            String state = parameters.get('state');
            String city = parameters.get('city');
            String[] filters = new List<String>();
            String[] sqftfilters= new List<String>();
        if(parameters.get('filterGroceryAnchored') != null && parameters.get('filterGroceryAnchored') != ''){
                filters.add(parameters.get('filterGroceryAnchored'));
            }
        if(parameters.get('filterPowerCenters') != null && parameters.get('filterPowerCenters') != ''){
                filters.add(parameters.get('filterPowerCenters'));
            }
        if(parameters.get('filterSQFT1') != null && parameters.get('filterSQFT1') != ''){
                sqftfilters.add(parameters.get('filterSQFT1'));
            }
        if(parameters.get('filterSQFT2') != null && parameters.get('filterSQFT2') != ''){
                sqftfilters.add(parameters.get('filterSQFT2'));
            }
        if(parameters.get('filterSQFT3') != null && parameters.get('filterSQFT3') != ''){
                sqftfilters.add(parameters.get('filterSQFT3'));
            }
        if(parameters.get('filterSQFT4') != null && parameters.get('filterSQFT4') != ''){
                sqftfilters.add(parameters.get('filterSQFT4'));
            }
        if(parameters.get('filterSQFT5') != null && parameters.get('filterSQFT5') != ''){
                sqftfilters.add(parameters.get('filterSQFT5'));
            }
        if(parameters.get('filterSQFT6') != null && parameters.get('filterSQFT6') != ''){
                sqftfilters.add(parameters.get('filterSQFT6'));
            }
            this.getResultTotal(state,city,filters,sqftfilters);

        } else if (this.action == 'getPropertyList'){

            String state = parameters.get('state');
            String city = parameters.get('city');
            String sortBy = parameters.get('sortBy');
            String rLimit = parameters.get('limit');
            String offset = parameters.get('offset');
            String[] filters = new List<String>();
            String[] sqftfilters= new List<String>();
        if(parameters.get('filterGroceryAnchored') != null && parameters.get('filterGroceryAnchored') != ''){
                filters.add(parameters.get('filterGroceryAnchored'));
            }
        if(parameters.get('filterPowerCenters') != null && parameters.get('filterPowerCenters') != ''){
                filters.add(parameters.get('filterPowerCenters'));
            }
        if(parameters.get('filterSQFT1') != null && parameters.get('filterSQFT1') != ''){
                sqftfilters.add(parameters.get('filterSQFT1'));
            }
        if(parameters.get('filterSQFT2') != null && parameters.get('filterSQFT2') != ''){
                sqftfilters.add(parameters.get('filterSQFT2'));
            }
         if(parameters.get('filterSQFT3') != null && parameters.get('filterSQFT3') != ''){
                sqftfilters.add(parameters.get('filterSQFT3'));
            }
         if(parameters.get('filterSQFT4') != null && parameters.get('filterSQFT4') != ''){
                sqftfilters.add(parameters.get('filterSQFT4'));
            }
         if(parameters.get('filterSQFT5') != null && parameters.get('filterSQFT5') != ''){
                sqftfilters.add(parameters.get('filterSQFT5'));
            }
         if(parameters.get('filterSQFT6') != null && parameters.get('filterSQFT6') != ''){
                sqftfilters.add(parameters.get('filterSQFT6'));
            }
            this.getPropertyList(state, city, sortBy, rLimit, offset,filters,sqftfilters);
            
        }/* else if (this.action == 'getsqft'){

            String state = parameters.get('state');
            String city = parameters.get('city');
            String sortBy = parameters.get('sortBy');
            String rLimit = parameters.get('limit');
            String offset = parameters.get('offset');

            this.getsqft(state, city, sortBy, rLimit, offset);
            
        }*/
    }

    public void getResultTotal(String state, String city,String[] filters,String[] sqftfilters){
        this.JSONResponse = JSON.serialize(wpl.getResultTotal(state, city, filters,sqftfilters));
    }

    public void getPropertyList(String state, String city,String sortBy, String rLimit, String offset,String[] filters,String[] sqftfilters){
        this.JSONResponse = JSON.serialize(wpl.getPropertyList(state, city, sortBy, rLimit, offset,filters,sqftfilters));
    }

    public void getStateList(String statename){
        this.JSONResponse = wpl.getStateList(statename);
    }
        
    public void getCityList(String state){
        this.JSONResponse = JSON.serialize(wpl.getCityList(state));
        system.debug('cityservice'+this.JSONResponse);
    }

}