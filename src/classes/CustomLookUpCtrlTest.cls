@isTest(seeAllData=true)
public class CustomLookUpCtrlTest{
    
    public static testmethod void fieldPicklistValuesTest(){
        test.starttest();
        PageReference pageRef = Page.CustomLookUpPopUp;
        pageRef.getParameters().put('FieldAPIName', 'Territory__c');
        pageRef.getParameters().put('ObjectAPIName','Contact');
        
        System.assertNotEquals(pageRef,Null);

        Test.setCurrentPage(pageRef);
        new CustomLookUpCtrl();
        test.stoptest();
    }
}