public with sharing class RC_New_CertificateButtonClass
{
    public String recortypeid{get;set;}
    public String pid{get;set;}
    public RC_Certificate_of_Insurance__c mp{get;set;}
    public RC_Certificate_of_Insurance__c ProjectObj{get;set;}
    public RC_Location__c ppc{get;set;}
    
  Public RC_New_CertificateButtonClass(Apexpages.StandardController controller)
  {
     recortypeid=ApexPages.CurrentPage().getParameters().get('RecordType');
     mp= (RC_Certificate_of_Insurance__c)controller.getRecord();
     pid=mp.id;
   }
  
   public PageReference Gobacktostandardpage() 
   {  
     if((mp.Location__c==null))
     {
      //String Url=System.Label.Current_Org_Url+'/a5C/e?nooverride=1&retURL=%2Fa5C%2Fo&RecordType='+mp.RecordTypeId+'&ent=01IR00000005onJ';
      String Url= System.Label.Current_Org_Url+'/a4V/e?nooverride=1&retURL=%2Fa4V%2Fo&RecordType='+mp.RecordTypeId+'&ent=01I500000003hbe';
       system.debug('Url'+Url); 
      PageReference acctPage = new PageReference(Url);
      acctPage.setRedirect(true); 
     return acctPage; 
     }  
     else if((mp.Location__c!=null))
     {
        
         system.debug('Check the Location'+mp.Location__c);
         string bid1;
         string bid=mp.Location__c;
         system.debug('Check the Location'+bid);
         ppc=[select id, Name,Account__c,Account__r.name from RC_Location__c where id=:bid];
         if(ppc!=null)
         bid1=ppc.Name;
         string accountid=ppc.Account__c;
         String accountName=ppc.Account__r.name;
         accountName=accountName.replaceAll('&','%26');
         
         String s2 = bid1.replaceAll('&', '%26');
        //String Url1=System.Label.Current_Org_Url+'/a5C/e?nooverride=1&CF00NR0000001JRQf_lkid='+bid+'&CF00NR0000001JRQf='+s2+'&CF00NR0000001JRNi_lkold='+accountid+'&CF00NR0000001JRNi='+accountName+'&retURL=%2F'+ppc.id+'&RecordType='+mp.RecordTypeId+'&ent=01IR00000005onJ';
          String Url1= System.Label.Current_Org_Url+'/a4V/e?nooverride=1&CF00N50000003Lzfq_lkid='+bid+'&CF00N50000003Lzfq='+s2+'&CF00N50000003Lzct_lkid='+accountid+'&CF00N50000003Lzct='+accountName+'&retURL=%2F'+ppc.id+'&RecordType='+mp.RecordTypeId+'&ent=01I500000003hbe'; 
         PageReference acctPage = new PageReference(Url1);
         acctPage.setRedirect(true); 
         return acctPage; 
     }
     return null;
  }
  
}