public with sharing class Cockpit_AdminCtrl extends Cockpit_AdminHelperClass{
    Automated_Call_List__c automatedCallList ;
    Automated_Call_List__c LockedACL;
    public String visibility{get;set;}
    public String campaignStatus{get;set;}
    public integer create_ActivityInLastXDays{get;set;}
    public integer create_openTaskDueInXdays{get;set;}
    SobjectFieldSetHelperClass fieldSet =  new SobjectFieldSetHelperClass();
    Cockpit_AdminHelperClass AdminHelperClass=  new Cockpit_AdminHelperClass();
    List<Automated_Call_List__c> autolist= new List<Automated_Call_List__c>();
    public boolean admin{get;set;}
    public boolean isSaveButtonDisabled{get;set;}
    public String loggedInUserId{get;set;}
    public String FieldName1{get;set;}
    public String FieldName2{get;set;}
    public String FieldName3{get;set;}
    public String FieldName4{get;set;}
    public String FieldName5{get;set;}
    public String FieldName6{get;set;}
    public String FieldName7{get;set;}
    public String FieldName8{get;set;}
    public String FieldName9{get;set;}
    public String FieldName10{get;set;}
    public String Operator1{get;set;}
    public String Operator2{get;set;}
    public String Operator3{get;set;}
    public String Operator4{get;set;}
    public String Operator5{get;set;}
    public String Operator6{get;set;}
    public String Operator7{get;set;}
    public String Operator8{get;set;}
    public String Operator9{get;set;}
    public String Operator10{get;set;}
    public String value1{get;set;}
    public String value2{get;set;}
    public String value3{get;set;}
    public String value4{get;set;}
    public String value5{get;set;}
    public String value6{get;set;}
    public String value7{get;set;}
    public String value8{get;set;}
    public String value9{get;set;}
    public String value10{get;set;}
    public String filterLogic{get;set;}
    public String ContactOption{get;set;} 
    PageReference pageRef12;
    public String selectedCustomCallList{get;set;}
    public Automated_Call_List__c customCallListVisibilty {get;set;}
    public List<Automated_Call_List__c> customCallList {get;set;}
    public Manage_Cockpit__c manageCallListValues {get;set;}
    public List<Manage_Cockpit__c> inActiveRelatedLists {get;set;}
    public List<Manage_Cockpit__c> activeRelatedLists {get;set;}
    public List<Manage_Cockpit__c> inactiveAdvisorDetails {get;set;}
    public List<Manage_Cockpit__c> ActiveAdvisorDetails {get;set;}
    public Id ManageCallListId {get;set;}
    public Id ManageRelatedListId {get;set;}
    public Id ManageAdvisorDetailsId {get;set;}
    public String generateCSVCallList{get;set;}
    public String visibilityCustomCallList{get;set;}
    public String deleteMultipleCallLists{get;set;}
    public boolean displayPopup {get; set;}
    public string showSuccMsg{get;set;}
    public Boolean showAdvanceFormula{get; set;}
    public String advanceFilterLogic{get;set;}

    public Cockpit_AdminCtrl() {
        showAdvanceFormula= false;
        advanceFilterLogic= '';
        automatedCallList = new Automated_Call_List__c();
        //Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
        admin=false;
        isSaveButtonDisabled = false;
        loggedInUserId = UserInfo.getUserId();
        user usr = [Select id,name,profile.Name from user where id=:UserInfo.getUserId() limit 1];
        if(usr.profile.Name == 'Cole Sales Manager User' || usr.profile.Name == 'MI&S' || usr.profile.Name == 'System Administrator IT')
           admin=true;   
        system.debug('admin123'+admin);
        Fieldname1 = '--None--';
        Fieldname2 = '--None--';
        Fieldname3 = '--None--';
        Fieldname4 = '--None--';
        Fieldname5 = '--None--';
        Fieldname6 = '--None--';
        Fieldname7 = '--None--';
        Fieldname8 = '--None--';
        Fieldname9 = '--None--';
        Fieldname10 = '--None--';
        visibility = 'Visible only to me';
        ContactOption = 'All Contacts';
        customCallListVisibilty = new Automated_Call_List__c();
        Map<String,Schema.RecordTypeInfo> recordTypesadjustment = Manage_Cockpit__c.sObjectType.getDescribe().getRecordTypeInfosByName();
        ManageCallListId = recordTypesadjustment.get('Manage Call Lists').getRecordTypeId(); 
        ManageRelatedListId = recordTypesadjustment.get('Manage Related Lists').getRecordTypeId();   
        ManageAdvisorDetailsId = recordTypesadjustment.get('Manage Advisor Details').getRecordTypeId();     
    } // --End of Constructor --
    public void closePopup(){       
        displayPopup = false;   
    } 
    // Get the Call List
    public Automated_Call_List__c getautomatedCallList(){       
        if(automatedCallList == null)
           automatedCallList = new Automated_Call_List__c();
        return automatedCallList;
    }
    //Creating CallList and Automated CallList from the Single Save method --Start--
    public PageReference save1(){  
        try{
           system.debug('automatedCallList.Name'+automatedCallList.Name);  
           if(validate('Create Call List',automatedCallList.Name,automatedCallList.Campaign__c,FieldName1,FieldName2,FieldName3,FieldName4,FieldName5,FieldName6,FieldName7,FieldName8,FieldName9,FieldName10,Operator1,Operator2,Operator3,Operator4,Operator5,Operator6,Operator7,Operator8,Operator9,Operator10)){
              List<Automated_Call_List__c> ACL = [Select Id,Name from Automated_Call_List__c where Name =: automatedCallList.name];
              System.debug('Duplicate Name Check' + ACL.size() + '*******' +automatedCallList.name+'******');
              if(ACL.size() >= 1){               
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Call List with the same Name already exists'));
              }
              else{
                 if(showAdvanceFormula && (advanceFilterLogic==null || advanceFilterLogic=='')){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Formula is Empty.'));
                    return null;     
                 }
                 system.debug('Testing Visisbility'+visibility);
                 LockedACL = new Automated_Call_List__c();
                 LockedACL.Name =  automatedCallList.name;
                 LockedACL.TempLock__c = 'General Lock';              
                 LockedACL.Activity_In_Last_X_Days__c= create_ActivityInLastXDays;
                 LockedACL.Open_Task_Due_In_X_Days__c = create_openTaskDueInXdays;
                 LockedACL.Description__c = automatedCallList.Description__c;
                 LockedACL.Custom_Logic__c = showAdvanceFormula!=false ? advanceFilterLogic : filterLogic;
                 LockedACL.isAdvanceFormula__c= showAdvanceFormula;
                 LockedACL.Campaign__c = automatedCallList.Campaign__c;
                 LockedACL.Priority__c = automatedCallList.Priority__c;
               
                 if(admin == true){
                    LockedACL.Type_Of_Call_List__c = 'Campaign Call List'; 
                    LockedACL.Visiblity__c = 'Visible to all users';   
                 }
                 else{
                    LockedACL.Type_Of_Call_List__c = 'Custom Call List';
                    LockedACL.Visiblity__c = visibility;
                 }
                 list<string> fieldsList = new list<string>();
                 Map<String, String> fieldsMap= new Map<String, String>();
                 string fitlerConditions = '';
                 fitlerConditions = campaignStatus+'&&&&';
                 if(FieldName1!=null && FieldName1!='--None--'){
                    fieldsList.add(String.escapeSingleQuotes(FieldName1)+'&&!!'+String.escapeSingleQuotes(Operator1)+'&&!!'+((Value1!='')?String.escapeSingleQuotes(Value1):null));
                    if(showAdvanceFormula)
                      fieldsMap.put('0', String.escapeSingleQuotes(FieldName1)+'&&!!'+String.escapeSingleQuotes(Operator1)+'&&!!'+((Value1!='')?String.escapeSingleQuotes(Value1):null));
                    fitlerConditions+=FieldName1+'&&!!'+Operator1+'&&!!'+((Value1!='')?Value1:null);
                 }
                 if(FieldName2!=null && FieldName2!='--None--'){
                    fieldsList.add(String.escapeSingleQuotes(FieldName2)+'&&!!'+String.escapeSingleQuotes(Operator2)+'&&!!'+((Value2!='')?String.escapeSingleQuotes(Value2):null));
                    if(showAdvanceFormula)
                      fieldsMap.put('1', String.escapeSingleQuotes(FieldName2)+'&&!!'+String.escapeSingleQuotes(Operator2)+'&&!!'+((Value2!='')?String.escapeSingleQuotes(Value2):null));
                    fitlerConditions+='-->'+FieldName2+'&&!!'+Operator2+'&&!!'+((Value2!='')?Value2:null);
                 }
                 if(FieldName3!=null && FieldName3!='--None--'){
                    fieldsList.add(String.escapeSingleQuotes(FieldName3)+'&&!!'+String.escapeSingleQuotes(Operator3)+'&&!!'+((Value3!='')?String.escapeSingleQuotes(Value3):null));
                    if(showAdvanceFormula)
                      fieldsMap.put('2', String.escapeSingleQuotes(FieldName3)+'&&!!'+String.escapeSingleQuotes(Operator3)+'&&!!'+((Value3!='')?String.escapeSingleQuotes(Value3):null));
                    fitlerConditions+='-->'+FieldName3+'&&!!'+Operator3+'&&!!'+((Value3!='')?Value3:null);
                 }
                 if(FieldName4!=null && FieldName4!='--None--'){
                    fieldsList.add(String.escapeSingleQuotes(FieldName4)+'&&!!'+String.escapeSingleQuotes(Operator4)+'&&!!'+((Value4!='')?String.escapeSingleQuotes(Value4):null));
                    if(showAdvanceFormula)
                      fieldsMap.put('3', String.escapeSingleQuotes(FieldName4)+'&&!!'+String.escapeSingleQuotes(Operator4)+'&&!!'+((Value4!='')?String.escapeSingleQuotes(Value4):null));
                    fitlerConditions+='-->'+FieldName4+'&&!!'+Operator4+'&&!!'+((Value4!='')?Value4:null);
                 }
                 if(FieldName5!=null && FieldName5!='--None--'){
                    fieldsList.add(String.escapeSingleQuotes(FieldName5)+'&&!!'+String.escapeSingleQuotes(Operator5)+'&&!!'+((Value5!='')?String.escapeSingleQuotes(Value5):null));
                    if(showAdvanceFormula)
                      fieldsMap.put('4', String.escapeSingleQuotes(FieldName5)+'&&!!'+String.escapeSingleQuotes(Operator5)+'&&!!'+((Value5!='')?String.escapeSingleQuotes(Value5):null));
                    fitlerConditions+='-->'+FieldName5+'&&!!'+Operator5+'&&!!'+((Value5!='')?Value5:null);
                 }
                 if(FieldName6!=null && FieldName6!='--None--'){
                    fieldsList.add(String.escapeSingleQuotes(FieldName6)+'&&!!'+String.escapeSingleQuotes(Operator6)+'&&!!'+((Value6!='')?String.escapeSingleQuotes(Value6):null));
                    if(showAdvanceFormula)
                      fieldsMap.put('5', String.escapeSingleQuotes(FieldName6)+'&&!!'+String.escapeSingleQuotes(Operator6)+'&&!!'+((Value6!='')?String.escapeSingleQuotes(Value6):null));
                    fitlerConditions+='-->'+FieldName6+'&&!!'+Operator6+'&&!!'+((Value6!='')?Value6:null);
                 } 
                 if(FieldName7!=null && FieldName7!='--None--'){
                    fieldsList.add(String.escapeSingleQuotes(FieldName7)+'&&!!'+String.escapeSingleQuotes(Operator7)+'&&!!'+((Value7!='')?String.escapeSingleQuotes(Value7):null));
                    if(showAdvanceFormula)
                      fieldsMap.put('6', String.escapeSingleQuotes(FieldName7)+'&&!!'+String.escapeSingleQuotes(Operator7)+'&&!!'+((Value7!='')?String.escapeSingleQuotes(Value7):null));
                    fitlerConditions+='-->'+FieldName7+'&&!!'+Operator7+'&&!!'+((Value7!='')?Value7:null);
                 }
                 if(FieldName8!=null && FieldName8!='--None--'){
                    fieldsList.add(String.escapeSingleQuotes(FieldName8)+'&&!!'+String.escapeSingleQuotes(Operator8)+'&&!!'+((Value8!='')?String.escapeSingleQuotes(Value8):null));
                    if(showAdvanceFormula)
                      fieldsMap.put('7', String.escapeSingleQuotes(FieldName8)+'&&!!'+String.escapeSingleQuotes(Operator8)+'&&!!'+((Value8!='')?String.escapeSingleQuotes(Value8):null));
                    fitlerConditions+='-->'+FieldName8+'&&!!'+Operator8+'&&!!'+((Value8!='')?Value8:null);
                 }
                 if(FieldName9!=null && FieldName9!='--None--'){
                    fieldsList.add(String.escapeSingleQuotes(FieldName9)+'&&!!'+String.escapeSingleQuotes(Operator9)+'&&!!'+((Value9!='')?String.escapeSingleQuotes(Value9):null));
                    if(showAdvanceFormula)
                      fieldsMap.put('8', String.escapeSingleQuotes(FieldName9)+'&&!!'+String.escapeSingleQuotes(Operator9)+'&&!!'+((Value9!='')?String.escapeSingleQuotes(Value9):null));
                    fitlerConditions+='-->'+FieldName9+'&&!!'+Operator9+'&&!!'+((Value9!='')?Value9:null);
                 }
                 if(FieldName10!=null && FieldName10!='--None--'){
                    fieldsList.add(String.escapeSingleQuotes(FieldName10)+'&&!!'+String.escapeSingleQuotes(Operator10)+'&&!!'+((Value10!='')?String.escapeSingleQuotes(Value10):null));
                    if(showAdvanceFormula)
                      fieldsMap.put('9', String.escapeSingleQuotes(FieldName10)+'&&!!'+String.escapeSingleQuotes(Operator10)+'&&!!'+((Value10!='')?String.escapeSingleQuotes(Value10):null));
                    fitlerConditions+='-->'+FieldName10+'&&!!'+Operator10+'&&!!'+((Value10!='')?Value10:null);
                 }

                 if(showAdvanceFormula){
                    ValidateAndOrExpression v= new ValidateAndOrExpression(advanceFilterLogic, fieldsMap.keySet(), true);
                    if(v.hasError){
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Check Advance Filter Logic.\n'+v.message));
                        return null;
                    }
                    advanceFilterLogic= ValidateAndOrExpression.convertExpressionKeys(advanceFilterLogic);
                 }
                 system.debug('**********campaignStatus**********'+campaignStatus);
                 system.debug('**********fieldsList**********'+fieldsList);
                 string campaignId = automatedCallList.Campaign__c;
                 
                 LockedACL.Dynamic_Query__c = 'select Id,Name,Internal_Wholesaler1__c,Territory__c,RIA_Territory__c from Contact'
                        + (!showAdvanceFormula ? AdminHelperClass.generateDynamicQueryFilters('Create Call List',fieldsList,filterLogic,ContactOption)
                                               : AdminHelperClass.generateAdvanceFormulaFilterCondition('Create Call List', ContactOption, fieldsMap, advanceFilterLogic));
                 LockedACL.Dynamic_Query_Filters__c = fitlerConditions;

                 if(Schema.sObjectType.Automated_Call_List__c.isCreateable())
                    insert LockedACL;  
                 system.debug('**********LockedACL**********'+LockedACL);
                 ID ACLID = LockedACL.Id;
                 displayPopup = true;
                 showSuccMsg = 'We will notify you via email, once the call list is created.';
                 Cockpit_InsertContactCallLists ICCL = new Cockpit_InsertContactCallLists(ACLID);
                 Id batchId = Database.executeBatch(ICCL);
                  
                 // batch process end
              }
           }                
           else{

           }
           if(LockedACL.Visiblity__c== 'Visible only to me'){                 
           
           }
           else if(LockedACL.Visiblity__c== 'Visible to all users'){ 
              Automated_Call_List__share automatedShare =  new Automated_Call_List__share();
              ID groupId = [select id from Group where Type = 'Organization'].id;
              automatedShare.ParentId = LockedACL.id;
              automatedShare.UserOrGroupId = groupId;
              automatedShare.AccessLevel = 'Edit';
              automatedShare.RowCause = Schema.Automated_Call_List__share.RowCause.Manual;
              if(Schema.sObjectType.Automated_Call_List__share.isCreateable()){
                 Database.SaveResult sr = Database.insert(automatedShare,false);
                 /*if(sr.isSuccess()){
                 
                 }
                 else{
                    Database.Error err = sr.getErrors()[0];
                    system.debug('*******err************'+err);
                    if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  && err.getMessage().contains('AccessLevel')){
                  
                    }
                    else{
                    }

                 }*/
              }
           }
           else{      
              system.debug('Automated Call List Id'+LockedACL.id);
              pageRef12 = new PageReference('/p/share/CustomObjectSharingEdit?parentId='+LockedACL.id+'&retURL=/apex/Cockpit_AdminUI'); 
              pageRef12.setRedirect(true);
              return pageRef12;           
           }
        }
        catch(Exception E){    
            system.debug('Exception---->'+E.getmessage()); 
        }
        return null;       
    } //--End of Save Method--
          
    //Delete multiple call lists
    public void deleteMultipleCallList(){     
        system.debug('deleteMultipleCallLists' + deleteMultipleCallLists); 
        list<string> deleteCallListsNames = new list<string>();
        if(deleteMultipleCallLists == '' || deleteMultipleCallLists == null || deleteMultipleCallLists == 'Select Call List'){
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select the call list')); 
        }
        else{
           set<Id> LockedACLIds = new set<id>();
           set<Id> ACLIds = new set<id>();         
           deleteCallListsNames = deleteMultipleCallLists.split('--&&--');
           list<Automated_Call_List__c> acllist = new list<Automated_Call_List__c>();
           list<Automated_Call_List__c> autolist = [Select id, name ,TempLock__c from Automated_Call_List__c where name in:deleteCallListsNames and Archived__c = false and IsDeleted__c=false];
           for(Automated_Call_List__c autolist1:autolist){
               if(autolist1.TempLock__c != null){
                  LockedACLIds.add(autolist1.id);
               }
               else{
                  autolist1.IsDeleted__c=true ;  
                  ACLIds.add(autolist1.id);
                  acllist.add(autolist1);         
               }
           }
           if(LockedACLIds.size()>0){
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Some operation is being on this call list So please wait for sometime'));
           }
           if(LockedACLIds.size()==0){
              system.debug('acllist' + acllist);
              if(Schema.sObjectType.Automated_Call_List__c.isUpdateable())
                 update acllist;
              system.debug('Batch ACLIds'+ACLIds);
              Cockpit_DeleteMultipleCallLists  batch = new Cockpit_DeleteMultipleCallLists (ACLIds);
              Id batchId = Database.executeBatch(batch);  
           }
        }               
    }

    // Getting Operators in the operator drop down box in VF page --- Start ---
    public List<SelectOption> getOperatorList1() {
        List<SelectOption> OperatorList1 = new List<SelectOption>();
        OperatorList1=createOperatorList(FieldName1);
        return OperatorList1;
    }
     
    public List<SelectOption> getOperatorList2() {
        List<SelectOption> OperatorList2 = new List<SelectOption>();
        OperatorList2=createOperatorList(FieldName2);
        return OperatorList2;  
    }
     
    public List<SelectOption> getOperatorList3() {
        List<SelectOption> OperatorList3 = new List<SelectOption>();
        OperatorList3=createOperatorList(FieldName3);
        return OperatorList3;
    }

    public List<SelectOption> getOperatorList4() {
        List<SelectOption> OperatorList4 = new List<SelectOption>();
        OperatorList4=createOperatorList(FieldName4);
        return OperatorList4;
    }

    public List<SelectOption> getOperatorList5() {
        List<SelectOption> OperatorList5 = new List<SelectOption>();
        OperatorList5=createOperatorList(FieldName5);
        return OperatorList5;
    }
    public List<SelectOption> getOperatorList6() {
        List<SelectOption> OperatorList6 = new List<SelectOption>();
        OperatorList6=createOperatorList(FieldName6);
        return OperatorList6;
    }
     
    public List<SelectOption> getOperatorList7() {
        List<SelectOption> OperatorList7 = new List<SelectOption>();
        OperatorList7=createOperatorList(FieldName7);
        return OperatorList7;  
    }
     
    public List<SelectOption> getOperatorList8() {
        List<SelectOption> OperatorList8 = new List<SelectOption>();
        OperatorList8=createOperatorList(FieldName8);
        return OperatorList8;
    }

    public List<SelectOption> getOperatorList9() {
        List<SelectOption> OperatorList9 = new List<SelectOption>();
        OperatorList9=createOperatorList(FieldName9);
        return OperatorList9;
    }

    public List<SelectOption> getOperatorList10() {
        List<SelectOption> OperatorList10 = new List<SelectOption>();
        OperatorList10=createOperatorList(FieldName10);
        return OperatorList10;
    }
     
    public void generateCSV(){
        system.debug('generateCSVCallList'+generateCSVCallList);
        if(generateCSVCallList == '' || generateCSVCallList == null || generateCSVCallList == 'Select Call List')
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select the call list'));
        else{
           Id aId;
           Automated_Call_List__c autolist = [Select id, name ,TempLock__c from Automated_Call_List__c where name =: generateCSVCallList and Archived__c = false and IsDeleted__c=false];
           aId = autolist.id;
           if(autolist.TempLock__c != null) {
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Some operation is being on this call list So please wait for sometime'));
           }
           else{
              Cockpit_GenerateCSV genCSV = new Cockpit_GenerateCSV(autolist.id);
              Id genCSVBatchId = Database.executeBatch(genCSV);
           }             
        }           // batch process end
    }
    
    public List<Schema.FieldSetMember> getfilteroptionsFields() {
        return SObjectType.Manage_Cockpit__c.FieldSets.Filter_and_Column_Options.getFields();
    }

    public Manage_Cockpit__c getmanageCallList(){
        manageCallListValues =  new Manage_Cockpit__c();
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getfilteroptionsFields()) {
            query += f.getFieldPath() + ', ';
        }
        system.debug('manageCallListValues123 query '+query );
        query += 'Id, Name,Visibility_To_Custom_Call_Lists__c,Show_Priority_And_Other_Call_Lists__c,Ability_To_Create_Custom_Call_Lists__c,Allow_Multiple_Selection__c FROM Manage_Cockpit__c where RecordTypeId =:ManageCallListId limit 1';
        manageCallListValues = Database.query(query);
        system.debug('manageCallListValues123'+manageCallListValues);
        return manageCallListValues;
    }
        
    //This function will update the manage call list filter in manage cockpit object
    public void updateCockpitSettingsValues(){
        if(Schema.sObjectType.Manage_Cockpit__c.isUpdateable())
           update manageCallListValues;
    }
    //This function will get all the related lists from manage cockpit
    public void getAdvisorAndRelatedListsDetails(){
        system.debug('Entered Advisor Details');
        inActiveAdvisorDetails = [select id,Name,Section_Name__c,Field_Set_URL__c,Expand__c from Manage_Cockpit__c where RecordTypeId=:ManageAdvisorDetailsId and Show_On_Cockpit__c=false order by Order__c limit 500];
        system.debug('inActiveAdvisorDetails'+inActiveAdvisorDetails);
        activeAdvisorDetails = [select id,Name,Section_Name__c,Field_Set_URL__c,Expand__c from Manage_Cockpit__c where RecordTypeId=:ManageAdvisorDetailsId and Show_On_Cockpit__c=true order by Order__c limit 500];
        system.debug('activeAdvisorDetails'+activeAdvisorDetails);
        inActiveRelatedLists = [select id,Name,Section_Name__c,Field_Set_URL__c,Expand__c from Manage_Cockpit__c where RecordTypeId=:ManageRelatedListId and Show_On_Cockpit__c=false order by Order__c limit 500];
        activeRelatedLists = [select id,Name,Section_Name__c,Field_Set_URL__c,Expand__c from Manage_Cockpit__c where RecordTypeId=:ManageRelatedListId and Show_On_Cockpit__c=true order by Order__c limit 500];
    }
    public PageReference updateVisibilityForCustomCallLists(){
        string returnURl =null;
        system.debug('visibilityCustomCallList'+visibilityCustomCallList);
        if(visibilityCustomCallList == '' || visibilityCustomCallList == null || visibilityCustomCallList == 'Select Custom Call List'){
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please seletct the custom call list'));             
        }
        else{           
           returnURl = AdminHelperClass.updateVisibility(visibilityCustomCallList,visibility);
           system.debug('returnURl'+returnURl);      
           PageReference pageref = new PageReference(returnURl);
           pageref.setRedirect(true);
           return pageref; 
        }  
        return null;        
    }
    
    
}