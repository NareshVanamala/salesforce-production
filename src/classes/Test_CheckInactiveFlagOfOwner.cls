@isTest
private class Test_CheckInactiveFlagOfOwner
{

    static testMethod void testApprovalSuccess()
    {
        User u= [select id, isactive,name from User where isActive=false order by name  limit 1];
        
        Employee__c ect= new Employee__c();
        ect.First_Name__c='snehal';
        ect.Last_Name__c='Kalamkar';
        ect.Employee_Status__c='On Board';
        ect.HR_Manager_Status__c='Approved';
        insert ect;
        
        Asset_Inventory__c aect= new Asset_Inventory__c();
        aect.Name='TestAsset';
        aect.Asset_Type__c='Software';
        aect.Application_Implementor__c=u.id;
        aect.Application_Owner__c= u.id;      
        insert aect;
    
        Asset__c  ct= new Asset__c();
        ct.Access_Name__c='TestAsset';
        ct.Application_Implementor__c=u.id;
        ct.Application_Owner__c=u.id; 
        ct.Asset_Inventory__c=aect.id;
        insert ct;
        
        system.assertequals(ct.Application_Implementor__c,u.id);


        
        
         Asset__c  ct1= new Asset__c();
        ct1.Access_Name__c='TestAsset';
        ct1.Application_Implementor__c=u.id;
        ct1.Application_Owner__c=u.id; 
        ct1.Asset_Inventory__c=aect.id;
        insert ct1;
        Test.StartTest();
         checkActiveflagOfApprover batch1  = new checkActiveflagOfApprover();
         Id batchId1 = Database.executeBatch(batch1,200);
    
    
    
    }   
 
}