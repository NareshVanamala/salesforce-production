public class AssetAutomationController {

public AssetAutomationController(ApexPages.StandardController controller) {}
public Flow.Interview.Asset_Inventory aFlow {get; set;}

 public String getIDS() {
        if (aFlow!=null)
        { 
         return aFlow.RecordID;
        }
   else return '';

    }
    public PageReference getFinishPage(){
    PageReference p = new PageReference('/' + getIDS() );
    p.setRedirect(true);
    return p;
    }
}