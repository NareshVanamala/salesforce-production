global class classforPdocRefreshProcess implements Database.Batchable<SObject>,Database.Stateful
{
       global String Query;
       global list<Contact_InvestorList__c >processinglist;
       global string contactid;
       global String investmentlistid;
       global  Map<Id, List<Attachment>> InvestorListAttchmentMap  = new Map<Id, List<Attachment>>();
       
              
   global classforPdocRefreshProcess ( )  
   {  
         processinglist=new list<Contact_InvestorList__c>();
         Query='select id,Pdoc_Ready__c,Sending_PDoc_Complete__c,Trigger_PDoc_Email__c,Pdoc_Email_delivery__c,PDocs_Request__c from Contact where Sending_PDoc_Complete__c=true and Pdoc_Email_delivery__c=true'; 
        //return database.getQueryLocator(query);   
        system.debug('What is the problem'+Query);
      
     }
     
      global Database.QueryLocator start(Database.BatchableContext BC)  
    {  
      system.debug('This is start method');
      system.debug('Myquery is......'+Query);
      return Database.getQueryLocator(Query);  
    }  
      global void execute(Database.BatchableContext BC,List<Contact> Scope)  
      { 
          
           system.debug('scope...'+Scope);
         //Now get the Investment list of the contact and get the attachment of those  records.
           set<id>contactidset= new set<id>();
            for(Contact cty:Scope)
           {
             contactidset.add(cty.id);
            }
           processinglist=[select id,Send_Pdoc_complete__c,Advisor_Name__c from Contact_InvestorList__c where Advisor_Name__c in:contactidset  ];
           system.debug('lets check the size of the list'+processinglist.size());
           
           list<contact>updatedlist= new list<Contact>();                   
          
             for(Contact cty:Scope)
           {
             cty.Sending_PDoc_Complete__c=false;
             cty.Trigger_PDoc_Email__c =false;
            //cty.Pdoc_Email_delivery__c=false;
             cty.Pdoc_Ready__c=true;
             
             updatedlist.add(cty);
             
            }
              update updatedlist;
            //now reset the investmentlist flgs.
              list<Contact_InvestorList__c >updatedlist1=new list<Contact_InvestorList__c >();
             for(Contact_InvestorList__c cty:processinglist)
           {
             cty.Send_Pdoc_complete__c=false;
             cty.Pdoc__c=false;
             updatedlist1.add(cty);
             
            } 
             update  updatedlist1;
         
     }     
    global void finish(Database.BatchableContext BC)  
    { 
       // AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
       
    }
    
  }