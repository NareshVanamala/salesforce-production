@isTest
public class Test_Cockpitclass 
{
/*static testmethod void myTestMethod1()
{
Test.StartTest();
CallList__c cl = new CallList__c();
cl.name = 'TestList1';
cl.Field_API__c = 'Territory__c';
cl.Operator__c = 'equals';
cl.Value__c ='HeartLand';
cl.Field_API1__c = 'Phone';
cl.Operator1__c = 'equals';
cl.Value1__c ='8473361717';
cl.Field_API2__c = 'firstName';
cl.Operator2__c = 'Contains';
cl.Value2__c ='test';
cl.Field_API3__c = 'Lastname';
cl.Operator3__c = 'contains';
cl.Value3__c ='record';
cl.Field_API4__c = 'License__c';
cl.Operator4__c = 'includes';
cl.Value4__c ='7,21';
cl.Description__c ='Hello Test record';
insert cl;
system.debug('******' + cl.name);
System.Assert(cl!=null);
Automated_Call_List__c al = new Automated_Call_List__c();
al.name = 'TestList1';
al.Call_Reason__c='Order Literature';
al.Call_Reason_Detail__c='Single Sheet';
al.Call_Resolution__c='Complete No FU Needed';
insert al;
Account a1 = new Account();
a1.name='FirstAccount';
insert a1;
System.Assert(al!=null);
System.debug('Check the Automated call list name' + al.name);
Contact c = new Contact();
c.accountid = a1.id;
c.Firstname='Test';
c.Lastname='record';
c.License__c='7';
c.Phone='8473361717';
c.Territory__c='HeartLand';
c.Title='Hellocontact';
//c.Wholesaler__c ='00550000001C5VP';
c.MailingCity='Florida';
insert c;
System.Assert(c!=null);
System.debug('Check the Contact name' + c.firstname);
Contact c1 = new Contact();
c1.Firstname='Test1';
c1.Lastname='record1';
c1.License__c='21';
c1.Phone='8473361717';
c1.Territory__c='HeartLand';
c1.Title='Hellocontact';
c1.MailingCity='Florida';
c1.Middle_Name__c='Priya';
c1.Note__c='HJHHHHH';
c1.Contact_Nickname__c='Priya';
c1.Phone='1234565';
c1.Company_Name__c='Priya';
insert c1;
System.Assert(c1!=null);
System.debug('Check the second Contact name' + c.firstname);


Manual_or_Automatic__c m = new Manual_or_Automatic__c();
m.name='Manual';
m.Manual_Assignment__c = true;
insert m;

Account a=new Account();
a.name='TestAccount';
insert a;
System.debug('Check the second Contact name' + a.name);

Campaign cg=new Campaign();
cg.name='MyCampaign';
insert cg;
System.Assert(cg!=null);
System.debug('Check the second Contact name' + cg.name);

contact_call_list__c clist = new contact_call_list__c();
clist.Call_Complete__c = false;
clist.CallListOrder__c = al.id;
clist.CallList_Name__c='TestList1';
clist.Contact__c = c.id;
clist.Owner__c=UserInfo.getUserid();
clist.Active_date__c=system.today();

clist.Call_Type__c='OutBound';
clist.Subject__c='Prospecting';
clist.Campaign__c=cg.id;
clist.Account__c=a.id;
clist.Priority__c=1;
clist.ActivedatePriority__c=1;
insert clist;
System.debug('Contact Call list Name' +clist.CallList_Name__c);

Activity_Connector_Log_A_Call__c ct1= new Activity_Connector_Log_A_Call__c();
ct1.Contact__c= clist.Contact__c;
ct1.Account__c=clist.Account__c;
ct1.Campaign__c=clist.Campaign__c;
ct1.User__c=clist.Owner__c;
ct1.Subject__c=clist.Subject__c;
ct1.Due_Date__c=system.today();
ct1.Comments__c='Checknow';
insert ct1;
System.Assert(ct1!=null);
System.debug('Contact Call list Name' +ct1.Contact__c);
date activedateforsecondRec=date.newInstance(Date.today().Year(),date.today().month(),date.today().day()-2);

contact_call_list__c clist1 = new contact_call_list__c();
clist1.Call_Complete__c = false;
clist1.CallListOrder__c = al.id;
clist1.CallList_Name__c='TestList1';
clist1.Contact__c = c1.id;
clist1.campId__c = cl.CampaignName__c;
clist1.Owner__c=UserInfo.getUserid();
clist1.ReasonToSkip__c='Not Interested'; 
clist1.Active_date__c=activedateforsecondRec;
clist1.Priority__c=1;
insert clist1;
System.Assert(clist1!=null);
System.debug('Contact Call list Name' +clist1.Contact__c);

Call_Result__c cresult= new Call_Result__c();
cresult.Call_Type__c='Outbound';
cresult.Assigned_to__c=UserInfo.getUserid();
cresult.Contact_Name__c=c.id;
cresult.Subject__c='Prospecting';
cresult.name= clist.CallList_Name__c;
cresult.Need_F_U__c=false; 
insert cresult;

CallTrackphaseIIIndev_vs call = new CallTrackphaseIIIndev_vs();
call.cid=clist1.Id;
call.CallTrackphaseIIIndev_vs(); 
call.calllistgenerator();
call.AdviserCallList();
call.NextAdvDetails();
call.CreateNewTask();
call.cid = clist1.Id;
call.CreatecompletedTask();
call.ContactSave();
call.CalllistResultSave();

call.CreateActivityConnector();
call.next();
call.Previous();
call.Calculate();
call.condetail();
call.ShowMore();




Task t= new Task();
t.ownerid=UserInfo.getUserid();
t.status='Not started';
t.whoid=c1.id;
insert t;


contact_call_list__c clist11 = new contact_call_list__c();
clist11.Call_Complete__c = false;
clist11.CallListOrder__c = al.id;
clist11.CallList_Name__c='TestList13';
clist11.Contact__c = c1.id;
clist11.Owner__c=UserInfo.getUserid();
clist11.Campaign__c=cg.id;
clist11.Priority__c=1;
clist11.Call_Type__c='Inbound';
clist11.Subject__c='Service Related';
insert clist11;

CallTrackphaseIIIndev_vs call1 = new CallTrackphaseIIIndev_vs();
call1.cid = clist11.Id;
call1.AdviserCallList();
call1.CreatecompletedTask();
call1.CreateNewTask();



Account act=new Account();
act.name='TESTMYACCOUNT';
insert act;

date Birthdate=date.newInstance(Date.today().Year()-23,date.today().month()-3,date.today().day()-2);







contact_call_list__c clist12 = new contact_call_list__c();
clist12.Call_Complete__c = false;
clist12.CallListOrder__c = al.id;
clist12.CallList_Name__c='TestList4';
clist12.Contact__c = c1.id;
clist12.Owner__c=UserInfo.getUserid();
clist12.Account__c=a.id;
clist12.Priority__c=1;
clist12.Call_Type__c='Inbound';
clist12.Subject__c='Service Related';

insert clist12;
task t7= new task();
t7.whatid=clist12.Account__c;
t7.whoid=clist12.Contact__c;
t7.ownerid=clist12.Owner__c;
t7.status='completed';
t7.Call_list__c= clist12.CallList_Name__c;
t7.subject=clist12.CallListOrder__r.Subject__c; 
insert t7;

CallTrackphaseIIIndev_vs call12 = new CallTrackphaseIIIndev_vs();
call12.cid = clist12.id;


call12.AdviserCallList();
call12.condetail(); 
call12.CreatecompletedTask();
call12.CreateNewTask();
call12.GettheCallList();
call12.contactSave();
call12.ShowMore();
call12.ShowMore1();
call12.ShowMore2();
call12.ShowMore3();
call12.CreateNewAutomatedCallList();

CallTrackphaseIIIndev_vs.CompleteCallfrmallists(c.id,'TestList4',al.id);

Test.stopTest();

}*/
}