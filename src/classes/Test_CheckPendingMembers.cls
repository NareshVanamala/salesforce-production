@isTest
private class Test_CheckPendingMembers
{
      public static testMethod void testCheckPendingMembers()
      { 
        Contact con = new Contact();
        con.firstname='RSVP';
        con.lastname='test';
        con.MailingCity='testcity';
        con.email='test1@mail.com';
        con.MailingState='teststate';
        insert con;

        Account acc = new Account();
        acc.Name = 'Test Prospect Account';      
        Insert acc;

        Contact c = new Contact();
        c.firstname='RSVP';
        c.lastname='test';
        c.accountid=acc.id;
        c.MailingCity='testcity';
        c.MailingState='teststate';
        c.email='test@mail.com';
        insert c;
        
        system.assertequals(c.accountid,acc.id);

        Contact ct = new Contact();
        ct.firstname='Attend';
        ct.lastname='testing';
        ct.accountid=acc.id;
        ct.MailingCity='testcity';
        ct.MailingState='teststate';
        ct.email='skalamkar@colecapital.com';
        insert ct;
        //Creating one more account...
       

        acc.Event_Approver__c=ct.id;
        update acc;
        //User us = [SELECT Id FROM User WHERE isActive = True limit 1];  
        User us = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() limit 1];
        
        Campaign cam = new Campaign();
        cam.name='Synopsys 2013';
        //cam.status='Pending';
        //cam.status='Approved';
        //cam.status='Rejected';
        cam.isActive=true;
      
        insert cam;
        
        CampaignMemberStatus cms1 = new CampaignMemberStatus(CampaignId=cam.Id, Label='Pending', HasResponded=true,sortorder=3);            
        insert cms1;

        //lsttask1=[select id, contactid,CampaignId ,Contact.FirstName,Contact.name,contact.accountid,Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c  from CampaignMember where  CampaignId =:CalllistEvent.Campaign__r.id and status='Pending' and contact.Account.Event_Approver__c!=null limit 2000];

        Event_Automation_Request__c ear = new Event_Automation_Request__c();
        ear.Name='BDE13006';
        ear.Event_Name__c='testeventname';
        ear.Start_Date__c=System.Today();
        ear.End_Date__c=System.Today() + 5;
        ear.Broker_Dealer_Name__c=acc.id;
        ear.Event_Location__c='testlocation';
        ear.Key_Account_Manager_Name_Requestor__c=us.id;
        ear.Event_Venue__c='testvenue';
        ear.Campaign__c=cam.id;
        insert ear;
        system.assertequals(ear.Campaign__c,cam.id);

  
        CampaignMember member= new CampaignMember();
        member.CampaignId = cam.id;
        member.ContactId = c.id;
        member.Status='Pending';
        insert member; 

        CampaignMember member1= new CampaignMember();
        member1.CampaignId = cam.id;
        member1.ContactId = ct.id;
        member1.Status='Pending';
        insert member1;

        

        CheckPendingMembers cttt= new CheckPendingMembers();  
        CheckPendingMembers.MyWrapper testwrapper  = new CheckPendingMembers.MyWrapper();         

        cttt.CallList=ear.Event_Name__c; 
        cttt.bMail='skalamkar@colecapital.com';
        system.debug('Acc is : ' + acc);
        List<CampaignMember> lstCM = [SELECT Id, Status, CampaignId FROM CampaignMember where CampaignId = :cam.id  ];
        system.debug('Members are : ' + lstCM);
        //cttt.lsttask1= [select id from CampaignMember where CampaignId = :cam.id and status='Pending']; 
             
        cttt.AdviserCallList();
        cttt.selectAllAccounts();
        cttt.doAction2();
        cttt.doAction();
        cttt.sendEmail();
        
        

    }  
}