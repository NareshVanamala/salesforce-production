Public with sharing class GeneratePropertyFactsheetController
{
   Id dealid;
   public list<Rent_Roll__c>rentRolls;
   public list<Loan_Relationship__c>loans;
   public  GeneratePropertyFactsheetController() 
   {
      dealid=ApexPages.currentPage().getParameters().get('id');
      system.debug('check the id..'+dealid);
      mydeal=[select  Pipeline_Id__c,Year_1_NOI__c,Outside_Counsel_Contact__c,Outside_Counsel_Contact__r.name,Outside_Counsel_Contact__r.Account.Name,Outside_Counsel_Contact__r.AccountId,Seller_s_Counsel_Firm__r.name,Seller_s_Broker_Company__r.name,Property_Owner_Contact__r.firstname,MRI_Property_Type_picklist__c,Seller_s_Broker_Company__c,Seller_s_Broker__r.email,Seller_s_Broker__r.Fax,Seller_s_Broker__r.Phone,Seller_s_Broker__r.MailingPostalCode,Seller_s_Broker__r.MailingState,Seller_s_Broker__r.MailingCity, Seller_s_Broker__r.Mailingstreet,Seller_s_Broker__r.name,Seller_s_Broker__c,Property_Owner_Contact__r.email,Seller_s_Counsel_Attorney__r.email,Seller_s_Counsel_Attorney__r.Fax,Seller_s_Counsel_Attorney__r.Phone,Seller_s_Counsel_Attorney__r.MailingPostalCode,Seller_s_Counsel_Attorney__r.Mailingstreet,Seller_s_Counsel_Attorney__r.MailingCity,Seller_s_Counsel_Attorney__r.MailingState, Seller_s_Counsel_Attorney__r.name,Property_Owner_Company__c,Property_Owner_Company__r.name,Seller_s_Counsel_Attorney__c,Seller_s_Counsel_Firm__c,Seller_Memo__c,Property_Owner_Contact__r.Fax,Property_Owner_Contact__r.Phone,Property_Owner_Contact__r.MailingPostalCode,Property_Owner_Contact__r.MailingState,Property_Owner_Contact__r.Mailingstreet,Property_Owner_Contact__r.MailingCity,Property_Owner_Contact__r.name,Property_Owner_Contact__c,Property_Owner_Contact__r.lastname, Buyer_s_Broker__c,Additional_Deposit__c,Initial_Deposit__c,CAP_Rate__c,Price_PSF_Total_Price__c,Price_PSF_Contract_Price__c,Accenture_Due_Date__c,Part_1_Start_Date__c,GL_Entity_ID__c,Federal_ID__c,Property_Manager__c,Building_Name__c,Center_Name__c, Address__c,City__c,Property_Phone__c,Zip_Code__c,State__c,MRI_Property_Type__c,Tenancy__c,Ownership_Type__c,Year_Renovated__c,Year_Built__c,Total_SF__c,Total_Price__c,NOI_Going_In__c,Average_NOI__c,GAAP_Cap__c,Study_Period__c,Closing_Period__c,Estimated_COE_Date__c,Lease_Abstract_Notes__c,Lease_Abstract_Date__c ,Site_Visit_By__c,Site_Visit_Date__c,Site_Visit_By__r.name,Lease_Abstract_By__r.name,Site_Visit_Notes__c,id,Name,Fund__c,Paralegal__c,Contract_Price__c,Attorney__c,Deal_Status__c,Property_Sub_Type__c,Primary_Use__c,Property_Location__c,Division__c,JV_Partner_Name__c,Number_of_Floors__c,Sale_Leaseback__c,Build_To_Suit__c,BTS_Completion_Date__c,Land_Acres__c,Parking_Spaces__c,Ownership_Interest__c from Deal__c where id=:dealid];
      system.debug('check the mydeal..'+mydeal);
    }
   public Deal__c mydeal
   { 
      get
     {     
         if (mydeal == null) 
           mydeal= new Deal__c ();   
           return mydeal; 
      }   
    set;  
  }
   public List<Rent_Roll__c>getrentRolls() {
    rentRolls=[select id,Name_of_Tenant__c,Suite_ID__c,Floor__c,SF__c,Lease_Start__c,Lease_Expiration__c,store__c,Tenant_Type_ID__c,Nat_Tenant_ID__c,SIC_Group_Code__c,NAICS_Code__c,Lease_Type__c,Lease_Code__c,Reimb_Code__c,Reimb_Type__c,S_P_Rating__c from Rent_Roll__c where Deal__c =:mydeal.id];
        //return SObjectType.Deal__c.FieldSets.Purchase_Report_Property_Status.getFields();
    return rentRolls;
    } 
  public List<Loan_Relationship__c>getloans() {
    loans=[select id,Loan_Amount__c,LTV__c,Lender__c,Lender__r.name,Rate_Spread__c,Fixed_Floating__c,Index__c,Amort_I_O__c from Loan_Relationship__c where Deal__c =:mydeal.id];
        //return SObjectType.Deal__c.FieldSets.Purchase_Report_Property_Status.getFields();
    return loans;
    } 
  
  
   
}