public with sharing class MultipleAttachmentsController 
{ 
    public MultipleAttachmentsController(ApexPages.StandardController controller) {
        recId = ApexPages.currentPage().getParameters().get('id');
        attType = new Event_Automation_Request__c();        
        statusOptions = new List<SelectOption>();

        // Use DescribeFieldResult object to retrieve status field.
        Schema.DescribeFieldResult statusFieldDescription = Event_Automation_Request__c.Attachment_Type__c.getDescribe();

        // For each picklist value, create a new select option
        for (Schema.Picklistentry picklistEntry:statusFieldDescription.getPicklistValues()){

            statusOptions.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));

            // obtain and assign default value
            if (picklistEntry.defaultValue){
                attType.Attachment_Type__c = pickListEntry.getValue();
            }  
        }

    }

    public Id recId;
    public Event_Automation_Request__c attType{get;set;}
    public List<SelectOption> statusOptions {get;set;}    
    Public Attachment attachment1;
    public Attachment getattachment1()
    {
        attachment1 = new Attachment();
        return attachment1;
    }    

    public MultipleAttachmentsController()
    { 
               
    }

    public PageReference createRecordAndAttachments()
    {   
        system.debug('>>>>>>>>>>>>>pageref>>>>>>>>>>>>>>>>>'+recId);
        // Insert Attachments;                     
        
        List<Attachment> attList = new List<Attachment>();
        
        if(attachment1.body==null)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please add attachment'));
        }
        else if(attachment1.body.size()>5242880)
        {
            system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> >5MB..');
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Attachment size is not greater than 5MB'));            
        }
        else
        {
            try
            {       
                String strTest = attachment1.name;
                String[] arrTest = strTest.split('\\.');
                
                Attachment a1 = new Attachment(parentId = recId, name=attType.Attachment_Type__c+'.'+arrTest[1], body = attachment1.body); 
                
                if(attachment1.body!=null)
                {
                    attList.add(a1);        
                }
                
                if(attList.size()>0)
                {
                    if (Schema.sObjectType.Attachment.isCreateable())

                    insert attList;
                }
                
                attachment1.Body=null;
                attachment1.clear();
                attList.clear();

                // View the record
                //return new PageReference('/'+recId);
                //return new PageReference('/apex/RemoveAttachBTN?id='+recId);
                //return null;
            }
            catch (DMLException e)
            {
                system.debug('check the attachement..'+ attList);
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
            }
            return new PageReference('/apex/RemoveAttachBTN?id='+recId);
        }
        return null;
    }
    public PageReference goBack()
    {  
        system.debug('>>>>>>>>>>>>>pageref>>>>>>>>>>>>>>>>>'+recId);

        return new PageReference('/apex/RemoveAttachBTN?id='+recId);
        //return null;        
    }
}