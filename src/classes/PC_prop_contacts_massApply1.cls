public with sharing class PC_prop_contacts_massApply1 extends PC_PropContactsHelperClass{

    public String Selectedcons { get; set; }
    public List<String> selectedPropContacts = new List<String>();
    public String ids {get;set;}
    public list<Property_Contact_Leases__c >pclist{get;set;}
    public list<Lease__c>leaselist{get;set;}
    public List<clease> WrapperleaseList {get; set;}
    public set<ID> idset=new set<ID>();
    public set<String> nameset=new set<String>();
    public set<String> contpset=new set<String>();  
    public String LocalTenantConcept{get;set;}
    public String commonname{get;set;}
    public String propertid{get;set;}
    public List<Lease__c> results{get;set;}
    public string propManager {get;set;}
    public string Proptype {get;set;}
    public string Propstate {get;set;}
    public String soql;
    public list<Lease__c> selectedleaselist{get;set;}
    public set<ID> selectedids=new set<ID>();
    public List<Property_Contact_Leases__c> pcntlist{get;set;}
    public List<Lease__c> ableset{get;set;}
    public List<Lease__c> unableset{get;set;}
    public boolean show1{get;set;}
    public boolean show2{get;set;}
    public boolean show3{get;set;}
    public boolean show4{get;set;}
    public Map<Property_Contact_Leases__c,set<Lease__c>> unablemap{get;set;}
    public list<string> mylist{get;set;}
    public integer totalrecs = 0;
    public integer OffsetSize = 0;
    public integer LimitSize= 1000;
    public Lease__c selectedlease{get;set;}
    public string localtname{get;set;}
    //****************these are the variable declared by snehal
    public Property_Contact_Leases__c pc{get;set;}
    public string leaseid{get;set;}
    public list<Lease__c> insertleaselist{get;set;}
    public list<Lease__c> FirstJunctionobjectleaselist{get;set;}
    public list<Lease__c> updatedleaselist{get;set;}
    public list<Lease__c> NotInsertleaselist{get;set;}
    //public list<Lease__c> GrossalesFinanciallist{get;set;}
    //public list<Lease__c> Grossaleslist{get;set;}
    //Public list<Lease__c>financiallist{get;set;}
    
    public list<Lease__c>createdsuceesfully{get;set;}
    public list<Lease__c>NotCreatedSuccessfully{get;set;}
    //public list<Property_Contact_Leases__c>Finalinsertlist{get;set;}
    public list<Property_Contact_Leases__c>propConLeasesInsertList{get;set;}
    public list<Property_Contact_Leases__c>FinalUpdateMatchedPropContLeasesList{get;set;}
   // public list<Property_Contact_Leases__c>CompleteFinalinsertlist{get;set;}
    public list<Property_Contact_Leases__c>updateMatchedPropContLeasesList{get;set;}
    //public list<Property_Contact_Leases__c >showlease{get;set;}
    public string both{get;set;}
    public string gc{get;set;}
    public string Finacial{get;set;}
    public PC_prop_contacts_massApply1(ApexPages.StandardController controller)
    {
        results = new List<Lease__c>();
        createdsuceesfully=new list<Lease__c>();
        NotCreatedSuccessfully=new list<Lease__c>();
        //showlease=new list<Property_Contact_Leases__c >();
        insertleaselist=new list<Lease__c>();
        FirstJunctionobjectleaselist=new list<Lease__c>();
        updatedleaselist=new list<Lease__c>();
        NotInsertleaselist=new list<Lease__c>();
       // GrossalesFinanciallist=new list<Lease__c>();
        //Grossaleslist=new list<Lease__c>();
        //financiallist=new list<Lease__c>();
        selectedlease=new Lease__c ();
        //CompleteFinalinsertlist=new list<Property_Contact_Leases__c>();
       // Finalinsertlist=new list<Property_Contact_Leases__c>();
        propConLeasesInsertList=new list<Property_Contact_Leases__c>();
        FinalUpdateMatchedPropContLeasesList=new list<Property_Contact_Leases__c>();
        updateMatchedPropContLeasesList=new list<Property_Contact_Leases__c>();
        mylist=new list<String>();
        both='Gross Sales & Financials';
        gc='Gross Sales';
        Finacial='Financials';
        selectedPropContacts =new list<String>();
        ids = ApexPages.currentPage().getParameters().get('lid');
        pc=new Property_Contact_Leases__c(); 
        system.debug('ids'+ids);
        system.debug('ids'+ids);
        if(Ids!=Null)
        selectedPropContacts = ids.split(','); 
        system.debug('ids'+ids);
        system.debug('selectedPropContacts'+selectedPropContacts );  
        for(String s:selectedPropContacts){
            if(s==null || s==''){
                system.debug('ids'+ids);
                system.debug('s'+s);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Select a Property Contact'));              
            }
            else
            idset.add(s);
        }
        String leaseid='';
        if(ids!=''){
             pc=[select id,Lease__r.Tenant_Name__c,Lease__c,Property_Contacts__c,Contact_Type__c from Property_Contact_Leases__c where id=:ids];
             leaseid=pc.Lease__c;
        }
        
         pcntlist= [select id,Lease__r.Tenant_Name__c,Lease__c,Property_Contacts__c,Contact_Type__c from Property_Contact_Leases__c where id IN:idset];
         
        for(Property_Contact_Leases__c pcl: pcntlist){
            if(pcl.Contact_Type__c!=null) {
                nameset.add(pcl.Lease__r.Tenant_Name__c);
                contpset.add(pcl.Contact_Type__c);
            }
        }

        List<Lease__c> lelist=[select id,Tenant_Name__c,Name,MRI_Lease_ID__c,Property_ID__c,MRI_PROPERTY__r.Common_Name__c,MRI_PROPERTY__r.Property_Manager__c,MRI_PROPERTY__r.Property_Type__c,MRI_PROPERTY__r.State__c,MRI_PROPERTY__r.Property_ID__c from  Lease__c where Tenant_Name__c IN:nameset and Lease_Status__c='Active' and id!=:leaseid];
        totalrecs=lelist.size();
        
        if(WrapperleaseList==null){
        system.debug('OffsetSize'+OffsetSize);
            WrapperleaseList=new List<clease>();
            if(!nameset.isEmpty()) {
                for (Lease__c foo : [select id,Tenant_Name__c,Name,MRI_Lease_ID__c,Property_ID__c,MRI_PROPERTY__r.Common_Name__c,MRI_PROPERTY__r.Property_Manager__c,MRI_PROPERTY__r.Property_Type__c,MRI_PROPERTY__r.State__c,MRI_PROPERTY__r.Property_ID__c from  Lease__c where Tenant_Name__c IN:nameset and Lease_Status__c='Active' and id!=:leaseid order by MRI_PROPERTY__r.Common_Name__c ASC Limit :LimitSize]) {
                    WrapperleaseList.add(new clease(foo));
                }
                system.debug('WrapperleaseList Size'+WrapperleaseList.size());
                
            }
        }
        
        show1=false;
        show2=false;
        show3=true;
        show4=true;
    }
 
    public class clease
     {
        public Lease__c  Clon {get; set;}
        public Boolean selected {get; set;}
        
        //This is the contructor method. When we create a new cContact object we pass a Contact that is set to the con property. We also set the selected value to false
        public clease(Lease__c   c)
        {
            Clon = c;
            selected = false;
             if(Test.isRunningTest()) {selected = true;}
        }
     }
        
    public void customSearch(){
        OffsetSize=0;
        LimitSize=1000;
        SearchRecord();
    
    }    
        
    public void SearchRecord()
    { 

        results = performSearch(LocalTenantConcept,commonname,propertid,PropManager,Proptype,Propstate);
        system.debug('results'+results);
        WrapperleaseList = new List<clease>();
        for(Lease__c c:results)
        {
            WrapperleaseList.add(new clease(c));
        }
        
               system.debug('WrapperleaseList'+propState);  
 
    } 
    
    public List<Lease__c> performSearch(String LtName,String cname,String Pid,String propManager,String PropType,String propState) { 
        system.debug('result123456'+cname+''+Pid+''+propManager+''+PropType+''+propState);  
       
        Property_Contact_Leases__c pc2=[select id,Lease__r.Tenant_Name__c,Lease__c,Property_Contacts__c,Contact_Type__c  from Property_Contact_Leases__c where id=:ids];
        String leaseid=pc2.Lease__c;
        selectedlease=[select id, name,Tenant_Name__c from lease__c where id=:leaseid];
        localtname=selectedlease.Tenant_Name__c;
        soql = 'select id,Tenant_Name__c,Name,Property_ID__c,MRI_Lease_ID__c ,MRI_PROPERTY__r.Common_Name__c,MRI_PROPERTY__r.Property_Manager__c,MRI_PROPERTY__r.Property_Type__c,MRI_PROPERTY__r.State__c,MRI_PROPERTY__r.Property_ID__c from  Lease__c where Tenant_Name__c =:localtname and Lease_Status__c = \'Active\'';
        system.debug('$$$$$$');
        if(LtName!= ''&& LtName!= null )
         {

            //soql=soql+ ' and MRI_PROPERTY__r.Tenant_Name__c LIKE \''+String.escapeSingleQuotes(LtName)+'\'';
            soql=soql+ ' and MRI_PROPERTY__r.Tenant_Name__c =:LtName' ;

          
        }   
        if(cname!= ''&& cname!= null )  {

           // soql=soql+ ' and MRI_PROPERTY__r.Common_Name__c LIKE \''+String.escapeSingleQuotes(cname)+'\'';
            soql=soql+ ' and MRI_PROPERTY__r.Common_Name__c =:cname' ;
          
        }
       if(Pid!= ''&& Pid!= null ) 
        {
            system.debug('$$$$$$');
            soql=soql+ ' and MRI_PROPERTY__r.Property_ID__c=:Pid' ;
           
        }       
       if(propManager!= 'None'&& propManager!= null ) 
        {
            system.debug('$$$$$$');
            soql=soql+ ' and MRI_PROPERTY__r.Property_Manager__c=:propManager' ;
           
        }   
         if(PropType!= 'None'&& PropType!= null) 
        {
            system.debug('$$$$$$');
            soql=soql+ ' and MRI_PROPERTY__r.Property_Type__c=:PropType' ;
           
        }  
        if(propState!= 'None'&& propState!= null) 
        {
            system.debug('$$$$$$');
            soql=soql+ ' and MRI_PROPERTY__r.State__c=:propState' ;
           
        }       
        
        list<Lease__c >leaserecs=new list<Lease__c >();
        leaserecs= database.query(soql); 
        totalRecs =leaserecs.size();
        
        soql=soql+' order by MRI_PROPERTY__r.Common_Name__c ASC' +' Limit '+ LimitSize+ ' OFFSET  '+OffsetSize;
        system.debug('dynamicquery'+soql);
      
        leaserecs=new list<Lease__c >();
        leaserecs= database.query(soql); 
        //totalRecs =leaserecs.size();
        system.debug('<<>>>'+totalRecs );
        system.debug('$$$$$$'+soql);
        system.debug('OffsetSize123'+OffsetSize);
       return leaserecs; 
    
    }
    
    public PageReference done(){
        Pagereference pgref=new Pagereference('/a3W/o');
        pgref.setRedirect(true);
        return pgref;
    }
    
     public list<SelectOption> getPropManagers(){
        List<SelectOption> propManagerOptions = new List<SelectOption>();
        List<AggregateResult> mriProp = new List<AggregateResult>();
        mriProp  =  [select Property_Manager__c propmanagerId,Property_Manager__r.Name propmanagername from MRI_Property__c where Property_Manager__c!=null group by Property_Manager__c,Property_Manager__r.Name order by Property_Manager__r.Name asc limit 10000];
        propManagerOptions.add(new selectOption('', ' None '));
        for(AggregateResult aggResult : mriProp){
             propManagerOptions.add(new SelectOption(String.valueOf(aggResult.get('propmanagerId')),String.valueOf(aggResult.get('propmanagername'))));
        }
        return propManagerOptions;
    }
    
     public list<SelectOption> getProptypes(){
        system.debug('mriProptype');
        List<SelectOption> ProptypeOptions = new List<SelectOption>();
        List<AggregateResult> mriProptype = new List<AggregateResult>();
        mriProptype  =  [select Property_Type__c propertytype from MRI_Property__c where Property_Type__c!=null group by Property_Type__c order by Property_Type__c asc limit 10000];
        system.debug('mriProptype'+mriProptype);
        ProptypeOptions.add(new selectOption('', ' None '));
        for(AggregateResult aggResult : mriProptype){
             ProptypeOptions.add(new SelectOption(String.valueOf(aggResult.get('propertytype')),String.valueOf(aggResult.get('propertytype'))));
        }
        return ProptypeOptions;
    }
    public list<SelectOption> getPropstates(){
        system.debug('mriProptype');
        List<SelectOption> PropstateOptions = new List<SelectOption>();
        List<AggregateResult> mriPropstate = new List<AggregateResult>();
        mriPropstate  =  [select state__c state from MRI_Property__c where state__c!=null group by state__c order by state__c asc limit 10000];
        system.debug('mriPropstate'+mriPropstate);
        PropstateOptions.add(new selectOption('', ' None '));
        for(AggregateResult aggResult : mriPropstate){
             PropstateOptions.add(new SelectOption(String.valueOf(aggResult.get('state')),String.valueOf(aggResult.get('state'))));
        }
        return PropstateOptions;
    }
    
    public PageReference SaveRecord(){
        show1=true;
        show2=true;
        show3=false;
        show4=false;     
       //ableset=new List<Lease__c>();
       /// unableset=new List<Lease__c>();
        selectedleaselist = new List<Lease__c>();
        List<Lease__c> selleaselist = new List<Lease__c>();
        set<ID> lsidset=new set<ID>();
        Map<Id,list<string>> existingPropertyconatctids = new Map<Id,list<string>>();
        for(clease ls:WrapperleaseList){
            if (ls.selected == true|| test.isRunningTest()) {
                selectedleaselist.add(ls.Clon); 
                //id=ls.Clon.id;
            }
        }
        selleaselist = [select id,name,Tenant_Name__c,MRI_Lease_ID__c,MRI_PROPERTY__r.Common_Name__c,MRI_PROPERTY__r.Property_ID__c from Lease__c where id in:selectedleaselist];
        //system.debug('id'+id);     
        system.debug('selectedleaselist'+selleaselist);
        system.debug('selectedleaselist.size()'+selleaselist.size());
        
        if(selectedleaselist.isEmpty()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a Lease'));
            return null;        
        }
        insertPropConLeases(selectedleaselist,pc.Property_Contacts__c,pc.Contact_Type__c,true,null);
        system.debug('InsertedPropContLeasesList'+InsertedPropContLeasesList);
        system.debug('SkippedPropContLeasesList'+SkippedPropContLeasesList);
        PageReference np = new PageReference('/apex/leaseset');
        np.setRedirect(false);

       return np;     
    } 
            
    
    }