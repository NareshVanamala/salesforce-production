public with sharing class MRIProperty_AcquisitionDispositionInvoke {

    static Map<String, String> BAD_ACQ_STATUS= new Map<String, String>{'On Hold'=>'On Hold', 'Lost'=>'Lost', 'Passed'=>'Passed'};

    @InvocableMethod(label='invokeMRIProperty' description='invokeMRIProperty')
    public static void invokeMRIProperty (List<string> inputParams) {
        
        List<MRI_PROPERTY__c> props= [select id, Property_ID__c,Property_Owner_SPE__c, Related_Deal__c, Disposition_in_DC__c
                                    from MRI_PROPERTY__c where id in :inputParams];

        //system.debug(logginglevel.info, '-----------props: '+props);

        Set<Id> acquisitionDispositionIds= new Set<Id>();
        
        Map<String, String> propertyIds= new Map<String, String>();

        for(MRI_PROPERTY__c prop: props){
            propertyIds.put(prop.Property_ID__c, prop.Property_ID__c);
            acquisitionDispositionIds.add(prop.Related_Deal__c);
            acquisitionDispositionIds.add(prop.Disposition_in_DC__c);
        }

        Map<Id, Deal__c> deals= new Map<Id, Deal__c>([select id, Deal_Status__c, Owned_Id__c,Property_Owner_SPEL__c, RecordType.Name from Deal__c 
                                                    where RecordType.Name IN ('Acquisition', 'Disposition','Sale Leaseback') 
                                                        and Deal_Status__c Not In ('Dead','Dead-Legal') 
                                                        and (Owned_Id__c IN :propertyIds.keySet() or Id IN :acquisitionDispositionIds)
                                                    order by Owned_Id__c,RecordType.Name,createddate asc]);
        
        system.debug(logginglevel.info, '-----------deals: '+deals.keySet());

        
        Map<Id, MRI_PROPERTY__c> updateMap= new Map<Id, MRI_PROPERTY__c>();

        for(MRI_PROPERTY__c prop: props){
            
            for(Deal__c deal: deals.values()){

                // existing mri_properties will get effected. This is the case where reference needs to be removed. type acquisition
                if(prop.Related_Deal__c== deal.Id && !prop.Property_ID__c.equalsIgnoreCase(deal.Owned_Id__c)){
                    
                    prop.Related_Deal__c= null;
                    if(updateMap.get(prop.Id)!=null){
                        updateMap.get(prop.Id).Related_Deal__c= null;
                        updateMap.get(prop.Id).Property_Owner_SPE__c=null;
                        continue;
                    }
                    
                    // existing mri_properties will get effected. This is the case where reference needs to be removed. type disposition
                }else if (prop.Disposition_in_DC__c== deal.Id && !prop.Property_ID__c.equalsIgnoreCase(deal.Owned_Id__c)) {
                    
                    prop.Disposition_in_DC__c= null;
                    if(updateMap.get(prop.Id)!=null){
                        updateMap.get(prop.Id).Disposition_in_DC__c= null;                      
                        continue;
                    }
                    
                // newly created or ownedId modified on deal and new mri_property satisfies the criteria. type acquisition
                }else if (prop.Property_ID__c.equalsIgnoreCase(deal.Owned_Id__c) && (deal.RecordType.Name=='Acquisition' || deal.RecordType.Name=='Sale Leaseback') && BAD_ACQ_STATUS.get(deal.Deal_Status__c)==null) {
                    
                    prop.Related_Deal__c= deal.Id;
                    if(updateMap.get(prop.Id)!=null){
                        updateMap.get(prop.Id).Related_Deal__c= deal.Id;
                        updateMap.get(prop.Id).Property_Owner_SPE__c= deal.Property_Owner_SPEL__c;
                        continue;
                    }
                    
                // newly created or ownedId modified on deal and new mri_property satisfies the criteria. type disposition
                }else if (prop.Property_ID__c.equalsIgnoreCase(deal.Owned_Id__c) && deal.RecordType.Name=='Disposition') {
                    
                    prop.Disposition_in_DC__c= deal.Id;
                    if(updateMap.get(prop.Id)!=null){
                        updateMap.get(prop.Id).Disposition_in_DC__c= deal.Id;                       
                        continue;
                    }
                    
                }else{
                    continue;
                }

                updateMap.put(prop.Id, prop);

            }//for

        }//for

        //system.debug(logginglevel.info, '--------------updateMap: \n'+updateMap.keySet());
        if(updateMap.isEmpty())
            return;

        try{
            update updateMap.values();
        }catch(Exception e){
            //system.debug('-------------Exception: \n'+e.getCause()+'\n'+e.getMessage()+'\n'+e.getStackTraceString());
        }

    }//end
}