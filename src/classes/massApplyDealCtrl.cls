public with sharing class massApplyDealCtrl{
    public string portfolioId{get;set;}
    public string loanId{get;set;}
    public list<Loan_Relationship__c> loanRelationShipList {get;set;}
    public massApplyDealCtrl(ApexPages.StandardController stdController){
        portfolioId = ApexPages.CurrentPage().getParameters().get('portId') ;
        loanId = ApexPages.CurrentPage().getParameters().get('selectedLoan');
        loanRelationShipList = new list<Loan_Relationship__c>();
        loanRelationShipList = [select id,Name,Loan__r.Name,Deal__r.name,Purchase_Price__c,Loan_Amount__c,Ratio__c,Loan__r.Total_Loan_Amount__c,Loan__r.Total_Purchase_Price__c from Loan_Relationship__c where Loan__c =:loanId limit 1000];
    }
   public pageReference Save(){
    update loanRelationShipList;
    PageReference page = new PageReference('/'+loanId);
    page.setRedirect(true);
    return page;
    }
    Public pageReference cancel(){
    PageReference page = new PageReference('/'+loanId );
    system.debug('page123'+page );
    page.setRedirect(true);
    return page;
    }
   
}