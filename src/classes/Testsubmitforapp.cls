@isTest(seeAllData =true)
private class Testsubmitforapp  {
 
    static testMethod void testApprovalSuccess()
    {
        
        Id uid=[SELECT Alias,Id,LastName FROM User LIMIT 1].id;
        string  profileid= [Select id from profile where name='System Administrator' limit 1].id;
        User userToCreate = new User();
        userToCreate.FirstName = 'David123';
        userToCreate.LastName  = 'Liu';
        userToCreate.Email     = 'dvdkliu+sfdc99@gmail.com';
        userToCreate.Username  = 'sfdc-dreamer055092015123@vereit.com';
        userToCreate.Alias     = 'fatty';
        userToCreate.ProfileId = profileid;
        userToCreate.TimeZoneSidKey    = 'America/Denver';
        userToCreate.LocaleSidKey      = 'en_US';
        userToCreate.EmailEncodingKey  = 'UTF-8';
        userToCreate.LanguageLocaleKey = 'en_US';      
        insert userToCreate;
        system.assert(userToCreate!=null);
        
        list<Project_Initiation__c> plist = new list<Project_Initiation__c >();
        list<PIF_Child__c>pclist = new list<PIF_Child__c>();
       
        Project_Initiation__c  p = new Project_Initiation__c ();
        p.name = 'Test';
        p.Selected_Champions__c = uid;
        p.ARCP_Integration_Project__c = true;
        p.CapEx__c = 100.00;
        p.OpEx__c = 100.00;
        p.Sponsor__c = uid;
        p.CFO_GRoup__c = userToCreate.id;
        p.Champion_group__c = userToCreate.id;
        //p.Compliance_group__c = userToCreate.id;
        p.PIF_Group__c = userToCreate.id;
        p.Sponsor__c = userToCreate.id;
        p.PMO_Group_Status__c = 'Submitted for Approval';
        insert p;
        System.assertEquals(p.Selected_Champions__c,uid);   
       
        PIF_Child__c  objectchild = new PIF_Child__c();
        objectchild.name = p.name;
        objectchild.PIF_parent__c = p.id;
        objectchild.Sponsor__c = p.Sponsor__c;
        insert objectchild;
        objectchild.PMO_Group_Status__c = 'Submitted for Approval';
        try{
            update objectchild;
        }
        catch(Exception e){}
         
        p.PMO_Group_Status__c = 'Approved';
        p.PIF_Group_Status__c = 'Submitted for Approval';
         
        PIF_Child__c  objectchild1 = new PIF_Child__c();
        objectchild1.name = p.name;
        objectchild1.PIF_parent__c = p.id;
        objectchild1.Sponsor__c = p.Sponsor__c;
        insert objectchild1;
        objectchild1.PMO_Group_Status__c = 'Approved';
        objectchild1.PIF_Group_Status__c = 'Submitted for Approval';
         try{
        update objectchild1;}
        catch(Exception e){}
        
        System.assertEquals(objectchild1.PIF_parent__c,p.id);

        
        p.PMO_Group_Status__c = 'Approved';
        p.PIF_Group_Status__c = 'Approved';
        p.Champion_Status__c = 'Submitted for Approval';
         
        PIF_Child__c  objectchild2 = new PIF_Child__c();
        objectchild2.name = p.name;
        objectchild2.PIF_parent__c = p.id;
        objectchild2.Sponsor__c = p.Sponsor__c;
        insert objectchild2;
        objectchild2.PMO_Group_Status__c = 'Approved';
        objectchild2.PIF_Group_Status__c = 'Approved';
        objectchild2.Champion_Group_Status__c = 'Submitted for Approval';  
         try{
        update objectchild2;}
        catch(Exception e){}
        
         System.assertEquals(objectchild2.PIF_parent__c,p.id);

        
        p.PMO_Group_Status__c = 'Approved';
        p.PIF_Group_Status__c = 'Approved';
        p.Champion_Status__c = 'Approved';
        p.Sponsor_status__c = 'Submitted for Approval';  
          
        PIF_Child__c  objectchild3 = new PIF_Child__c();
        objectchild3.name = p.name;
        objectchild3.PIF_parent__c = p.id;
        objectchild3.Sponsor__c = p.Sponsor__c;
        insert objectchild3;
        objectchild3.PMO_Group_Status__c = 'Approved';
        objectchild3.PIF_Group_Status__c = 'Approved';
        objectchild3.Champion_Group_Status__c = 'Approved';  
        objectchild3.Sponsor_Status__c = 'Submitted for Approval';  
         try{
        update objectchild3;}
        catch(Exception e){}
        
        System.assertEquals(objectchild3.PIF_parent__c,p.id);
 
   }
 
}