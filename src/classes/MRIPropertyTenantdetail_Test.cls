@isTest(SeeAllData=true)
public class MRIPropertyTenantdetail_Test{
     static testMethod void MRIPropertyTenant(){
        MRI_Property__c mriProp = new MRI_Property__c();
        mriProp.Name = 'Sample PropCon';
        mriProp.AvgLeaseTerm__c =72.00;
        mriProp.Property_Id__c ='12345';
        mriProp.OwnerId =UserInfo.getuserid();
        mriProp.Property_Manager__c=UserInfo.getuserid();
        insert mriProp;
        Lease__c leaseObj=new Lease__c();
        leaseObj.MRI_Property__c=mriProp.id;
        leaseObj.name='sample lease';
        leaseObj.Sales_Report_Required__c =true;
        leaseObj.Financials_Required__c =true;
        leaseObj.Lease_Status__c ='Active';
        leaseObj.Lease_ID__c='ABC1201010';
        insert leaseObj;
        System.assertNotEquals(mriProp.Name,leaseObj.name);
        
        String mriPropid = apexpages.currentpage().getparameters().put('id' , mriProp.id);
         ApexPages.StandardController controller = new ApexPages.StandardController(mriProp);
        MRIPropertyTenantdetail MRIPropertyTenantdetailClass = new MRIPropertyTenantdetail(controller);
    }
}