global virtual without sharing class PropertyforLeaseDetail extends cms.ContentTemplateController{
    
    GetURL getURLObj = new GetURL();
    
    global PropertyforLeaseDetail() {
        
    }
    
    global override virtual String getHTML(){

        SObject property;
        String html;
        String propertyID = System.currentPageReference().getParameters().get('propertyId');
       // String grossarea = System.currentPageReference().getParameters().get('grossarea');
        
        string webready='Approved'; 
        String query = 'SELECT Name,Common_Name__c, Web_Name__c,status__c, Gross_Leaseable_Area__c,Address__c, City__c, State__c, Zip_Code__c, Date_Acquired__c, Website_Fund__c,Property_Type__c, Sector__c, sqFt__c, ProgramName__c, Long_Description__c,Lease_Description__c, Location__c, Location__latitude__s, Location__longitude__s, lease_status__c, Tenant_Profile__c,Aerial__c,Brochure__c,Demographics__c,Site_Plan1__c,Main_Image__c,Image1__c,Image2__c,Image3__c,Image4__c,Image5__c FROM Web_Properties__c WHERE Id=\'' + propertyID + '\' and Web_Ready__c=\'' + webready + '\'';

        //String overview;
               
        //string leasecontacts;
        String tenantProfile;
        DateTime acquired;
        list<string> images=new list<string>();
                
        property = Database.query(query);
           
      String title =  String.valueof(property.get('Web_Name__c'));
     
      String Sqft = '0';
      if (property.get('sqFt__c') != null)
      Sqft = String.valueof(property.get('sqFt__c'));    
      Integer length = Sqft.length();
      String formatedSqft = '';
      String temp = '';
      for (Integer i=length;i>0;i=i-3)
      {
          if (i>3)
              temp = Sqft.substring(i-3,i);
          else
              temp = Sqft.substring(0,i);
              
          if (formatedSqft != '')
              formatedSqft = temp + ',' + formatedSqft;
          else
              formatedSqft = temp;
      }
        
           html =  '<div class="container inside">'+
                   ' <input type="hidden" id="latitude" value="' + property.get('Location__latitude__s') + '" /> ' +
                   ' <input type="hidden" id="longitude" value="' + property.get('Location__longitude__s') + '" /> ' +'<div class="col-md-9 row alignCenter"><div id="propertyDetails"><div class="propertyPhoto"><ul class="large">';
               
              
       if(property.get('Main_Image__c')!=null && property.get('Main_Image__c')!='')
        html+=  '<li><img src="'+property.get('Main_Image__c')+'" width="273" height="182"></li>'; 
       if(property.get('Image1__c')!=null && property.get('Image1__c')!='')
        html+=  '<li><img src="'+property.get('Image1__c')+'" width="273" height="182"></li>'; 
       if(property.get('Image2__c')!=null && property.get('Image2__c')!='')
        html+=  '<li><img src="'+property.get('Image2__c')+'" width="273" height="182"></li>'; 
       if(property.get('Image3__c')!=null && property.get('Image3__c')!='')
        html+=  '<li><img src="'+property.get('Image3__c')+'" width="273" height="182"></li>'; 
       if(property.get('Image4__c')!=null && property.get('Image4__c')!='')
        html+=  '<li><img src="'+property.get('Image4__c')+'" width="273" height="182"></li>'; 
       if(property.get('Image5__c')!=null && property.get('Image5__c')!='')
        html+=  '<li><img src="'+property.get('Image5__c')+'" width="273" height="182"></li>';
               
        html+=  '</ul><ul class="thumb">' ;
              
      if(property.get('Main_Image__c')!=null && property.get('Main_Image__c')!='')
        html+=  '<li><img src="'+property.get('Main_Image__c')+'" width="36" height="24"></li>'; 
      if(property.get('Image1__c')!=null && property.get('Image1__c')!='')
        html+=  '<li><img src="'+property.get('Image1__c')+'" width="36" height="24"></li>'; 
      if(property.get('Image2__c')!=null && property.get('Image2__c')!='')
        html+=  '<li><img src="'+property.get('Image2__c')+'" width="36" height="24"></li>'; 
      if(property.get('Image3__c')!=null && property.get('Image3__c')!='')
        html+=  '<li><img src="'+property.get('Image3__c')+'" width="36" height="24"></li>'; 
      if(property.get('Image4__c')!=null && property.get('Image4__c')!='')
        html+=  '<li><img src="'+property.get('Image4__c')+'" width="36" height="24"></li>'; 
      if(property.get('Image5__c')!=null && property.get('Image5__c')!='')
        html+=  '<li><img src="'+property.get('Image5__c')+'" width="36" height="24"></li>'; 
              
                
                
        html+=  '</ul></div><div class="propertyDetail"><div class="propertyRowDetail"><div class="title">' + title  + '</div><div class="location">' + property.get('Address__c') + '<br/>' + property.get('City__c') + ', ' + property.get('State__c') + ' ' + property.get('Zip_Code__c') + '</div><div class="Fund"><span class="result">Fund: </span>' + property.get('Website_Fund__c')  + '</div><div class="PropertyType"><span class="result">Property Type: </span>' + String.valueOf(property.get('Property_Type__c')).replace('Multi-Tenant','')  + '</div><div class="GrossLeaseableArea"><span class="result">Gross Leaseable Area: </span>' + formatedSqft  + '</div><div class="LeaseStatus"><span class="result">Lease Status: </span>' ;
                if(property.get('lease_Status__c')!=null && property.get('lease_Status__c')!='' && property.get('lease_Status__c')!='--None--')
                {
                html +=property.get('lease_Status__c');
                }
                html +='</div><div class="LeaseStatus"><span class="result">Latitude: </span>' + property.get('Location__latitude__s') + '</div><div class="LeaseStatus"><span class="result">Longitude: </span>' + property.get('Location__longitude__s') + '</div></div></div></div><div id="propertyTabs"><ul class="tabs">';
                html+='<li class="active">List View</li><li>Map View</li><li>Documents</li>';  
                html+='</ul><ul class="tabDetail">';
                html+='<li style="white-space: pre-wrap; display:none;">';
                html+='<div class="pull-left leasing-right-box" style="background:none!important;">';
                if(property.get('Long_Description__c') != null)
                html+= property.get('Long_Description__c') ;
                if(property.get('Lease_Description__c') != null)
                html+= property.get('Lease_Description__c');
                html+='</div>';
                html+='</li><li style="display:none;"><div id="map-canvas" style="width:100%;"></div></li>';
              
                html+='<li style="white-space: pre-wrap; display:none;">';
               
                    html+='<div class="pull-left leasing-right-box downloadLnk" style="background:none!important;">';
                      if(property.get('Brochure__c') != null && property.get('Brochure__c')!='')  
                        html+='<p><a href="'+property.get('Brochure__c')+'" target="'+property.get('Web_Name__c')+'">Download Brochure</a></p>';
                    if(property.get('Aerial__c') != null && property.get('Aerial__c')!='')  
                        html+='<p><a href="'+property.get('Aerial__c')+'" target="'+property.get('Web_Name__c')+'">Download Aerial</a></p>';
                    if(property.get('Site_Plan1__c') != null && property.get('Site_Plan1__c')!='')  
                        html+='<p><a href="'+property.get('Site_Plan1__c')+'" target="'+property.get('Web_Name__c')+'">Download Site Plan</a></p>';
                    if(property.get('Demographics__c') != null && property.get('Demographics__c')!='')  
                        html+='<p><a href="'+property.get('Demographics__c')+'" target="'+property.get('Web_Name__c')+'">Download Demographics</a></p>';
                    
                    html+='</div>';
              
                
              html +='</li></ul></div></div></div>' ;
                
                
             
        return html;
    }

}