public with sharing class PC_Mass_Apply_Building_Contacts extends PC_PropContactsHelperClass{

    public String Selectedcons { get; set; }
    public List<String> selectedPropContacts = new List<String>();
    public String ids {get;set;}
    public list<Property_Contact_Buildings__c >pclist{get;set;}
    public list<MRI_PROPERTY__c>leaselist{get;set;}
    public List<clease> WrapperleaseList {get; set;}
    public set<ID> idset=new set<ID>();
    public set<String> nameset=new set<String>();
    public set<String> contpset=new set<String>();  
    public String LocalTenantConcept{get;set;}
    public String commonname{get;set;}
    public String propertid{get;set;}
    public List<MRI_PROPERTY__c> results{get;set;}
    public string propManager {get;set;}
    public string Proptype {get;set;}
    public string Propstate {get;set;}
    public String soql;
    public list<MRI_PROPERTY__c> selectedBuildingslist{get;set;}
    public set<ID> selectedids=new set<ID>();
    public List<Property_Contact_Buildings__c> pcntlist{get;set;}
    //public List<MRI_PROPERTY__c> ableset{get;set;}
   // public List<MRI_PROPERTY__c> unableset{get;set;}
    public boolean show1{get;set;}
    public boolean show2{get;set;}
    public boolean show3{get;set;}
    public boolean show4{get;set;}
    public Map<Property_Contact_Buildings__c,set<MRI_PROPERTY__c>> unablemap{get;set;}
    public list<string> mylist{get;set;}
    public integer totalrecs = 0;
    public integer OffsetSize = 0;
    public integer LimitSize= 1000;
    public MRI_PROPERTY__c selectedlease{get;set;}
    public string localtname{get;set;}
    //****************these are the variable declared by snehal
    public Property_Contact_Buildings__c pc{get;set;}
    public string leaseid{get;set;}
    public list<Property_Contact_Buildings__c >showlease{get;set;}
    public string both{get;set;}
    public string gc{get;set;}
    public string Finacial{get;set;}
    public PC_Mass_Apply_Building_Contacts(ApexPages.StandardController controller)
    {
        results = new List<MRI_PROPERTY__c>();
        showlease=new list<Property_Contact_Buildings__c >();
        selectedlease=new MRI_PROPERTY__c ();
        mylist=new list<String>();
        both='Gross Sales & Financials';
        gc='Gross Sales';
        Finacial='Financials';
        selectedPropContacts =new list<String>();
        ids = ApexPages.currentPage().getParameters().get('lid');
        pc=new Property_Contact_Buildings__c(); 
        system.debug('$$$$$'+ids);
        system.debug('$$$$$'+ids);
        if(Ids!=Null)
        selectedPropContacts = ids.split(','); 
        system.debug('$$$$$'+ids);
        system.debug('Checjchecj'+selectedPropContacts );  
        for(String s:selectedPropContacts){
            if(s==null || s==''){
                system.debug('$$$$$'+ids);
                system.debug('$$$$$'+s);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Select a Property Contact'));              
            }
            else
            idset.add(s);
        }
        String leaseid='';
        if(ids!=''){
             pc=[select id,Center_Name__r.Tenant_Name__c,Center_Name__c,Property_Contacts__c,Contact_Type__c from Property_Contact_Buildings__c where id=:ids];
             leaseid=pc.Center_Name__c;
        }
        
         pcntlist= [select id,Center_Name__r.Tenant_Name__c,Center_Name__c,Property_Contacts__c,Contact_Type__c from Property_Contact_Buildings__c where id IN:idset];
         
        for(Property_Contact_Buildings__c pcl: pcntlist){
            if(pcl.Contact_Type__c!=null) {
                nameset.add(pcl.Center_Name__r.Tenant_Name__c);
                contpset.add(pcl.Contact_Type__c);
            }
        }

        List<MRI_PROPERTY__c> lelist=[select id,Tenant_Name__c,Name,Property_ID__c,Common_Name__c,Property_Manager__c,Property_Type__c,State__c from  MRI_PROPERTY__c where Tenant_Name__c IN:nameset and id!=:leaseid];
        totalrecs=lelist.size();
        
        if(WrapperleaseList==null){
        system.debug('OffsetSize'+OffsetSize);
            WrapperleaseList=new List<clease>();
            if(!nameset.isEmpty()) {
                for (MRI_PROPERTY__c foo : [select id,Tenant_Name__c,Name,Property_ID__c,Common_Name__c,Property_Manager__c,Property_Type__c,State__c from  MRI_PROPERTY__c where Tenant_Name__c IN:nameset  and id!=:leaseid order by Common_Name__c ASC Limit :LimitSize]) {
                    WrapperleaseList.add(new clease(foo));
                }
                system.debug('WrapperleaseList123'+WrapperleaseList.size());
                
            }
        }
        
        show1=false;
        show2=false;
        show3=true;
        show4=true;
    }
    
    public class clease
     {
        public MRI_PROPERTY__c  Clon {get; set;}
        public Boolean selected {get; set;}
        
        //This is the contructor method. When we create a new cContact object we pass a Contact that is set to the con property. We also set the selected value to false
        public clease(MRI_PROPERTY__c   c)
        {
            Clon = c;
            selected = false;
             if(Test.isRunningTest()) {selected = true;}
        }
     }
        
    public void customSearch(){
        OffsetSize=0;
        LimitSize=1000;
        SearchRecord();
    
    }    
        
    public void SearchRecord()
    { 

        results = performSearch(LocalTenantConcept,commonname,propertid,PropManager,Proptype,Propstate);
        system.debug('result123'+results);
        WrapperleaseList = new List<clease>();
        for(MRI_PROPERTY__c c:results)
        {
            WrapperleaseList.add(new clease(c));
        }
        
        
    } 
    
    public List<MRI_PROPERTY__c> performSearch(String LtName,String cname,String Pid,String propManager,String PropType,String propState) { 
        system.debug('result123456'+cname+''+Pid+''+propManager+''+PropType+''+propState);  
       
        Property_Contact_Buildings__c pc2=[select id,Center_Name__r.Tenant_Name__c,Center_Name__c,Property_Contacts__c,Contact_Type__c  from Property_Contact_Buildings__c where id=:ids];
        String leaseid=pc2.Center_Name__c;
        selectedlease=[select id, Tenant_Name__c,Name,Property_ID__c ,Common_Name__c,Property_Manager__c,Property_Type__c,State__c from MRI_PROPERTY__c where id=:leaseid];
        localtname=selectedlease.Tenant_Name__c;
        soql = 'select id,Tenant_Name__c,Name,Property_ID__c ,Common_Name__c,Property_Manager__c,Property_Type__c,State__c from  MRI_PROPERTY__c where Tenant_Name__c =:localtname ';
        system.debug('$$$$$$');
        if(LtName!= ''&& LtName!= null )
         {

            //soql=soql+ ' and MRI_PROPERTY__r.Tenant_Name__c LIKE \''+String.escapeSingleQuotes(LtName)+'\'';
            soql=soql+ ' and Tenant_Name__c =:LtName' ;

          
        }   
        if(cname!= ''&& cname!= null )  {

           // soql=soql+ ' and MRI_PROPERTY__r.Common_Name__c LIKE \''+String.escapeSingleQuotes(cname)+'\'';
            soql=soql+ ' and Common_Name__c =:cname' ;
          
        }
       if(Pid!= ''&& Pid!= null ) 
        {
            system.debug('$$$$$$');
            soql=soql+ ' and Property_ID__c=:Pid' ;
           
        }       
       if(propManager!= 'None'&& propManager!= null ) 
        {
            system.debug('$$$$$$');
            soql=soql+ ' and Property_Manager__c=:propManager' ;
           
        }   
         if(PropType!= 'None'&& PropType!= null) 
        {
            system.debug('$$$$$$');
            soql=soql+ ' and Property_Type__c=:PropType' ;
           
        }  
        if(propState!= 'None'&& propState!= null) 
        {
            system.debug('$$$$$$');
            soql=soql+ ' and State__c=:propState' ;
           
        }       
        
        list<MRI_PROPERTY__c >leaserecs=new list<MRI_PROPERTY__c >();
        leaserecs= database.query(soql); 
        totalRecs =leaserecs.size();
        
        soql=soql+' order by Common_Name__c ASC' +' Limit '+ LimitSize+ ' OFFSET  '+OffsetSize;
        system.debug('dynamicquery'+soql);
      
        leaserecs=new list<MRI_PROPERTY__c >();
        leaserecs= database.query(soql); 
        //totalRecs =leaserecs.size();
        system.debug('<<>>>'+totalRecs );
        system.debug('$$$$$$'+soql);
        system.debug('OffsetSize123'+OffsetSize);
       return leaserecs; 
    
    }
    
    public PageReference done(){
        Pagereference pgref=new Pagereference('/a3W/o');
        pgref.setRedirect(true);
        return pgref;
    }
    
     public list<SelectOption> getPropManagers(){
        List<SelectOption> propManagerOptions = new List<SelectOption>();
        List<AggregateResult> mriProp = new List<AggregateResult>();
        mriProp  =  [select Property_Manager__c propmanagerId,Property_Manager__r.Name propmanagername from MRI_Property__c where Property_Manager__c!=null group by Property_Manager__c,Property_Manager__r.Name order by Property_Manager__r.Name asc limit 10000];
        propManagerOptions.add(new selectOption('', ' None '));
        for(AggregateResult aggResult : mriProp){
             propManagerOptions.add(new SelectOption(String.valueOf(aggResult.get('propmanagerId')),String.valueOf(aggResult.get('propmanagername'))));
        }
        return propManagerOptions;
    }
    
     public list<SelectOption> getProptypes(){
        system.debug('mriProptype');
        List<SelectOption> ProptypeOptions = new List<SelectOption>();
        List<AggregateResult> mriProptype = new List<AggregateResult>();
        mriProptype  =  [select Property_Type__c propertytype from MRI_Property__c where Property_Type__c!=null group by Property_Type__c order by Property_Type__c asc limit 10000];
        system.debug('mriProptype'+mriProptype);
        ProptypeOptions.add(new selectOption('', ' None '));
        for(AggregateResult aggResult : mriProptype){
             ProptypeOptions.add(new SelectOption(String.valueOf(aggResult.get('propertytype')),String.valueOf(aggResult.get('propertytype'))));
        }
        return ProptypeOptions;
    }
    public list<SelectOption> getPropstates(){
        system.debug('mriProptype');
        List<SelectOption> PropstateOptions = new List<SelectOption>();
        List<AggregateResult> mriPropstate = new List<AggregateResult>();
        mriPropstate  =  [select state__c state from MRI_Property__c where state__c!=null group by state__c order by state__c asc limit 10000];
        system.debug('mriPropstate'+mriPropstate);
        PropstateOptions.add(new selectOption('', ' None '));
        for(AggregateResult aggResult : mriPropstate){
             PropstateOptions.add(new SelectOption(String.valueOf(aggResult.get('state')),String.valueOf(aggResult.get('state'))));
        }
        return PropstateOptions;
    }
    
    public PageReference SaveRecord(){
        show1=true;
        show2=true;
        show3=false;
        show4=false;     
        //ableset=new List<MRI_PROPERTY__c>();
        //unableset=new List<MRI_PROPERTY__c>();
        selectedBuildingsList=new List<MRI_PROPERTY__c>();
        set<ID> lsidset=new set<ID>();
        Map<Id, List<string>> Propertyconatctids = new Map<Id, List<string>>();
        for(clease ls:WrapperleaseList){
            if (ls.selected == true|| test.isRunningTest()) {
                selectedBuildingsList.add(ls.Clon); 
            //  id=ls.Clon.id;
            }
        }
        //system.debug('$$$$$'+id);     
        system.debug('$$$$$selectedBuildingsList'+selectedBuildingsList);
        if(selectedBuildingsList.isEmpty()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a Lease'));
            return null;        
        }
        insertPropConBuildings(selectedBuildingsList,pc.Property_Contacts__c,pc.Contact_Type__c,true,null);
       
       return Page.PC_leaseset_Building_Contacts;
             
           
        } 
            
     
    }