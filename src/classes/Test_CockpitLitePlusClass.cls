@isTest
Public class Test_CockpitLitePlusClass{
     /*static testmethod void CockpitLitePlusTest()
     {
     
       User u = [select firstname from user where id=:userinfo.getuserid()];
       
       Id strRecordTypeId =[Select id,name,DeveloperName from RecordType where DeveloperName='NonInvestorRepContact' and SobjectType = 'contact'].id; 
      
        Apexpages.currentPage().getParameters().put('firstName', 'pageparam');
        list<account> alist = new list<account>();
        
       Account a = new Account();
       a.name = 'Test';
       a.X1031_Selling_Agreement__c = 'CCIT II';
       alist.add(a);
       
       Account a1 = new Account();
       a1.name = 'Test';
       a1.X1031_Selling_Agreement__c = 'CCPT IV';
       alist.add(a1);
       insert alist;
      
       system.assertNotequals(alist,Null);

         Campaign  comp = new Campaign();
         comp.name = 'Test';
         insert comp;
       
       
       Contact c1 = new Contact();
       c1.lastname = 'Cockpit';
       c1.Accountid = a.id;
       c1.Relationship__c ='Accounting';
       c1.Contact_Type__c = 'Cole';
       c1.Wholesaler_Management__c = 'Producer A';
       c1.Territory__c ='Chicago';
       c1.RIA_Territory__c ='Southeast Region';
       c1.RIA_Consultant_Picklist__c ='Brian Mackin';
       c1.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c1.Wholesaler__c='Andrew Garten';
       c1.Regional_Territory_Manager__c ='Andrew Garten';
       c1.Internal_Wholesaler__c ='Aaron Williams';
       c1.Territory_Zone__c = 'NV-50';
       c1.Next_Planned_External_Appointment__c = system.now();
       c1.Campaign__c = comp.id;
       c1.recordtypeid = strRecordTypeId;
       insert c1;
       
        
     
       
       
       Contact c2 = new Contact();
       c2.lastname = 'Cockpit';
       c2.Accountid = a.id;
       c2.Relationship__c ='Accounting';
       c2.Contact_Type__c = 'Cole';
       c2.Wholesaler_Management__c = 'Producer A';
       c2.Territory__c ='Chicago';
       c2.RIA_Territory__c ='Southeast Region';
       c2.RIA_Consultant_Picklist__c ='Brian Mackin';
       c2.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c2.Wholesaler__c='Andrew Garten';
       c2.Regional_Territory_Manager__c ='Andrew Garten';
       c2.Internal_Wholesaler__c ='Aaron Williams';
       c2.Territory_Zone__c = 'NV-50';
       c2.Next_Planned_External_Appointment__c = system.now();
       c2.Campaign__c = comp.id;
       c2.recordtypeid = strRecordTypeId;
       insert c2;
      
       system.assertequals(c1.lastname,c2.lastname);

       
       Contact c = new Contact();
       c.lastname = 'Cockpit';
       c.Accountid = a.id;
       c.Relationship__c ='Accounting';
       c.Contact_Type__c = 'Cole';
       c.Wholesaler_Management__c = 'Producer A';
       c.Territory__c ='Chicago';
       c.RIA_Territory__c ='Southeast Region';
       c.RIA_Consultant_Picklist__c ='Brian Mackin';
       c.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c.Wholesaler__c='Andrew Garten';
       c.Regional_Territory_Manager__c ='Andrew Garten';
       c.Internal_Wholesaler__c ='Aaron Williams';
       c.Territory_Zone__c = 'NV-50';
       c.Next_Planned_External_Appointment__c = system.now();
       c.Campaign__c = comp.id;
       c.recordtypeid = strRecordTypeId;
      c.Snooze_Time__c = system.now();
    c.snoozed__c = true;
     c.recordtypeid = strRecordTypeId;
   
     c.Hours_Days_to_Snooze__c = '1';
    insert c;
      
       
    // Map<integer,Contact> rowmap;
        // rowmap.put(1,c); 
                
        
       Event e = new Event();
        e.OwnerId = u.Id;
        e.whoid=c.id;
        e.whatid = a.id;
        e.ActivityDate=system.today();
        e.Type ='Client Meeting';
        e.StartDate__c=system.today();
        e.EndDate__c=system.today()+1;
        e.DurationInMinutes=5;
        e.ActivityDateTime=system.now();
        insert e;

       
        Task t = new Task();
        t.Ownerid = u.id;
        t.Subject = 'External Appointment';
        t.whatid = a.id;
        t.Status = 'Completed';
        t.ActivityDate = system.today();
        t.Priority = 'Normal';
      insert t;
      
         
         
        
       Apexpages.currentPage().getParameters().put('firstName','cockpit');
       list<string> sgaSelectValues=new list<string>{'CCPT V','CCIT','CCPT IV'};
       list<string> availableList=new list<string>{'CCPT V','CCIT','CCPT IV'};
       CockpitLitePlusClass objclass = new CockpitLitePlusClass();
       //objclass.tskObj= t;
       //objclass.eventObj = e;
       //objclass.CallList='50'; 
       //objclass.con=c;
       //objclass.advCon=c;
       //objclass.Acon=a;
       //objclass.Task1=t;
       //objclass.topenrellist=EL;
       //objclass.sortDir = 'asc';
       //objclass.sortField ='Note__c';
       objclass.toggleSort();
       //objclass.sortField ='Mailingcity';
       objclass.toggleSort();
       //objclass.sortField ='MailingStreet';
       objclass.toggleSort();
       //objclass.sortField ='Mailingstate';
       objclass.toggleSort();
        //objclass.sortField ='Priority__c';
       objclass.toggleSort();
       //objclass.sortField ='Account.name';
       objclass.toggleSort();
       //objclass.sortField ='Wholesaler_Management__c';
       objclass.toggleSort();
       //objclass.sortField ='Next_Planned_External_Appointment__c';
       objclass.toggleSort();
       //objclass.sortField ='Phone';
       objclass.toggleSort();
       //objclass.sortField ='Email';
       objclass.toggleSort();
       //objclass.sortField ='';
       objclass.toggleSort();
       objclass.getwmanagement();
       objclass.getownershipOptions1();
       objclass.paging();
    //objclass.populateDescription();
       objclass.AdviserCallList();
       objclass.toggleSort();
       objclass.trr= 'Arizona Region';
       objclass.RelatedCity();
       objclass.trr='Central CA';
       objclass.RelatedCity();
       objclass.trr='Chicago';
       objclass.RelatedCity();
       objclass.trr='Florida';
       objclass.RelatedCity();
       objclass.trr='Central Region';
       objclass.RelatedCity();
       objclass.trr='East Metro Region';
       objclass.RelatedCity();
       objclass.trr='Four Corners';
       objclass.RelatedCity();
       objclass.trr='Great Lakes';
       objclass.RelatedCity();
       objclass.trr='Great Lakes 1';
       objclass.RelatedCity();
       objclass.trr='Great Lakes 2';
       objclass.RelatedCity();
       objclass.trr='Hawaii';
       objclass.RelatedCity();
       objclass.trr='Heartland';
       objclass.RelatedCity();
       objclass.trr='Mid-Atlantic';
       objclass.RelatedCity();
       objclass.trr='Midwest';
       objclass.RelatedCity();
       objclass.trr='Mid West';
       objclass.RelatedCity();
       objclass.trr='Midwest Region';
       objclass.RelatedCity();
       objclass.trr='New England';
       objclass.RelatedCity();
       objclass.trr='NJ & Eastern PA';
       objclass.RelatedCity();
       objclass.trr='North Central';
       objclass.RelatedCity(); 
       objclass.trr='North Central 1';
       objclass.RelatedCity();
       objclass.trr='North Central 2';
       objclass.RelatedCity();
       objclass.trr='North Central 3';
       objclass.RelatedCity();
       objclass.trr='Northeast Region';
       objclass.RelatedCity();
       objclass.trr='Northern CA';
       objclass.RelatedCity();
       objclass.trr='NY CT';
       objclass.RelatedCity();
       objclass.trr='NY Metro';
       objclass.RelatedCity(); 
       objclass.trr='Ohio Region';
       objclass.RelatedCity();
       objclass.trr='Ohio Valley';
       objclass.RelatedCity();
       objclass.trr='Pacific NW';
       objclass.RelatedCity();
       objclass.trr='Penn';
       objclass.RelatedCity();
       objclass.trr='South Central';
       objclass.RelatedCity();
       objclass.trr='South Central 1';
       objclass.RelatedCity();
       objclass.trr='South Central 2';
       objclass.RelatedCity(); 
       objclass.trr='Southeast';
       objclass.RelatedCity();
       objclass.trr='Southeast 1';
       objclass.RelatedCity();
       objclass.trr='Southeast 2';
       objclass.RelatedCity();
       objclass.trr='Southeast 3';
       objclass.RelatedCity();
       objclass.trr='Southeast Region';
       objclass.RelatedCity();
       objclass.trr='Southern';
       objclass.RelatedCity();
       objclass.trr='Southern CA';
       objclass.RelatedCity(); 
       objclass.trr='Southwest';
       objclass.RelatedCity();
       objclass.trr='Southwest 1';
       objclass.RelatedCity();
       objclass.trr='Southwest 2';
       objclass.RelatedCity();
       objclass.trr='West Region';
       objclass.RelatedCity();
       objclass.trr='Tri-State';
       objclass.RelatedCity();
       objclass.trr='Not Assigned';
       objclass.RelatedCity(); 
       objclass.getsgaSelectValues();
       objclass = new CockpitLitePlusClass();
       objclass.cId=c.id;
       objclass.condetail();
       string nxtcid=null;
       objclass.cId=nxtcid;
       objclass.condetail();
       objclass.ScheduleTask();
       objclass.Calculate();
       objclass.getDetailLink();
       objclass.unCheckthevalue();
       objclass.scheduleCallrecap();
       Boolean eventcheck=true;
       Boolean callrecapcheck=true;
       Boolean taskcheck=true;
       Boolean eventcheck1=true;
       Boolean callrecapcheck1=true;
       Boolean taskcheck1=true;
       objclass.scheduleEventorTask();
       objclass.rowmap = new map<integer,contact>();
       objclass.Contacts = new list<contact>();
       objclass.Contacts.add(c);
       objclass.removepos=1;
       objclass.nextclick=0;
       objclass.rowmap.put(1,c);
       try{
     objclass.SnoozeCon();
     }
     catch(exception ex){}
     
      CockpitLitePlusClass objclass1;
      objclass1=new CockpitLitePlusClass(); 
       objclass1.getSnoozeradio();
       try{
     objclass1.SnoozeCon();
     }
     catch(exception ex){}
    
      objclass1.showmore();
      try{
     objclass1.ShowMore3();}catch(exception ex1){}
     try{objclass1.ShowMore4();}catch(exception ex2){}
     objclass1.sortField='Name';
     try{
     objclass1.toggleSort();
     }
     catch(exception ex4){}
     try{
     objclass1.next();}
     catch(exception ee){}
     
      CockpitLitePlusClass objclass2;
      objclass2=new CockpitLitePlusClass(); 
      objclass2.advCon=c;     
           
     }


*/
}