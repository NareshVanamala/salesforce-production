@isTest
private class Test_MultipleAttachmentsController
{
   public static testMethod void TestforMultipleAttachmentsController() 
   {    
        Account acc = new Account();
        acc.Name = 'Test Prospect Account';
        Insert acc;
        
        //Set up user
        User us = [SELECT Id FROM User limit 1];          
           
        Event_Automation_Request__c ear = new Event_Automation_Request__c();
        ear.Name='BDE13006';
        ear.Event_Name__c='testeventname';
        ear.Start_Date__c=System.Today();
        ear.End_Date__c=System.Today();
        ear.Broker_Dealer_Name__c=acc.id;
        ear.Event_Location__c='testlocation';
        ear.Key_Account_Manager_Name_Requestor__c=us.id;
        ear.Event_Venue__c='testvenue';
        ear.Attachment_Type__c='testatttype';
        insert ear;

        String nameFile = 'hello';
        Attachment attachment= new Attachment(); 
        attachment.OwnerId = UserInfo.getUserId();  
        attachment.ParentId = ear.Id;// the record the file is attached to   
        attachment.Name = nameFile;     
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');  
        attachment.body=bodyBlob;   
        insert attachment;
        
        system.assertequals(attachment.ParentId,ear.Id);

        
        ApexPages.currentPage().getParameters().put('id',ear.id);
        
        MultipleAttachmentsController mac1 = new MultipleAttachmentsController();
        
        ApexPages.StandardController controller = new ApexPages.StandardController(ear);
        MultipleAttachmentsController mac = new MultipleAttachmentsController(controller);
        
        mac.getattachment1();
        
        //mac.Attachment_Type__c = 'testatttype';      
        Blob blb=Blob.valueOf('hahahablob'); 
        //mac.contentFile=blb;  
        //mac.addrow.Name='testrowname'; 
        List<Attachment> attachments=[select id, name, body from Attachment where parent.id=:ear.Id]; 
        
        mac.createRecordAndAttachments();
        mac.goBack();     
    }
}