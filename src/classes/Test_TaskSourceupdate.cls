@isTest
public class Test_TaskSourceupdate 
 {
    static testmethod void TaskSourceupdate()
    {
       Database.QueryLocator QL;
       Database.BatchableContext BC;
       list<task> tlist=new list<task>(); 
        
      Account a = new Account();
       a.name = 'Test';
       insert a;
             
       Contact c = new Contact();
       c.lastname = 'Cockpit';
       c.Accountid = a.id;
       c.Relationship__c ='Accounting';
       c.Contact_Type__c = 'Cole';
       c.Wholesaler_Management__c = 'Producer A';
       c.Territory__c ='Chicago';
       c.RIA_Territory__c ='Southeast Region';
       c.RIA_Consultant_Picklist__c ='Brian Mackin';
       c.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c.Wholesaler__c='Andrew Garten';
       c.Regional_Territory_Manager__c ='Andrew Garten';
       c.Internal_Wholesaler__c ='Aaron Williams';
       c.Territory_Zone__c = 'NV-50';
       c.Priority__c='1';
       c.Next_Planned_External_Appointment__c = null; 
       c.Next_External_Appointment__c = null; 
       insert c;
       
        system.assertequals(c.Accountid,a.id);

       
        Task t=new Task();
        t.ownerid= userinfo.getuserid();
        t.type= 'Outbound'; 
        t.subject='Ext Appt event';
        t.whoid=c.id; 
        t.status = 'Completed';
        t.ActivityDate =system.today();
        t.Description='Test';
        tlist.add(t);
        
        Task t1=new Task();
        t1.ownerid= userinfo.getuserid();
        t1.type= 'Outbound'; 
        t1.subject='Ext Appt event';
        t1.whoid=c.id; 
        t1.status = 'Completed';
        t1.ActivityDate =system.today();
        t1.Description='Test';
        tlist.add(t1);
        system.assertequals(t1.whoid,c.id);

        insert tlist;        
        
      
       TaskSourceupdate e= new TaskSourceupdate();
       ID batchprocessid = Database.executeBatch(e);
       QL = e.start(bc);
       e.execute(BC,tlist);
       e.finish(bc);
       
      
             
    }
 
 
 
 }