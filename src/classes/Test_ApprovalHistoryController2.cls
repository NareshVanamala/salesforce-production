@isTest(seealldata = true)
private class Test_ApprovalHistoryController2  {
   static testmethod void TestCaseApproval() {  
   Test.startTest();
    
        
        user u =[select id,name from user where id=:userinfo.getuserid()];
   
    Employee__c emp = new Employee__c();
    emp.name = 'Test';
    emp.Start_Date__c= system.today();
    emp.Supervisor__c = 'Supervisor';
    emp.First_Name__c ='N';
    emp.Title__c ='Title';
    emp.Last_Name__c ='Test';
    emp.Move_User_To__c ='move user';
    emp.Office_Location__c ='Hyderabad';
    emp.Employee_Role__c  ='Developer';
    emp.Employee_Status__c='On Boarding inProcess';
    insert emp;
    
        emp.Employee_Status__c='On Boarding';
       
         try
        {
            update emp;
         }
        catch(Exception e)
        {
        system.debug('exception'+e);
        } 

        
    Asset__c a1 = new Asset__c();
    a1.Added_On__c= system.today();
    a1.Asset_Status__c='Added';
    a1.Added_On__c = system.today();
   // a1.Asset_Inventory__c = ai.id;
    a1.Access_Name__c = 'Test';
    a1.Access_Type__c = 'Building';
    a1.Email_Subject_Asset__c='email';
    a1.Removed_On__c = system.today();
    a1.Termination_Status__c ='Active';
    a1.Terminating__c = false;
    a1.Updating__c = false;
    a1.Application_Owner__c = u.id;
    a1.Application_Implementor__c = u.id;
    a1.Employee__c = emp.id;
    try
        {
            insert a1;
         }
        catch(Exception e)
        {
        system.debug('exception'+e);
        }
    
    a1.Asset_Status__c='Removal In Process';
     try
        {
            update a1;
         }
        catch(Exception e)
        {
        system.debug('exception'+e);
        } 
        
     System.assertEquals(a1.Employee__c,emp.id);

      
     Asset__c a2 = new Asset__c();
    a2.Added_On__c= system.today();
    a2.Asset_Status__c='Added';
    a2.Added_On__c = system.today();
   // a2.Asset_Inventory__c = ai.id;
    a2.Access_Name__c = 'Test';
    a2.Access_Type__c = 'Building';
    a2.Email_Subject_Asset__c='email';
    a2.Removed_On__c = system.today();
    a2.Termination_Status__c ='Active';
    a2.Terminating__c = false;
    a2.Updating__c = false;
    a2.Application_Owner__c = u.id;
    a2.Application_Implementor__c = u.id;
    a2.Employee__c = emp.id;
    
    try
        {
            insert a2;
         }
        catch(Exception e)
        {
        system.debug('exception'+e);
        }
    
    a2.Asset_Status__c='Removal In Process';
    //update a2;  
  
        System.assertEquals(a2.Employee__c,emp.id);

         Asset__c a3 = new Asset__c();
    a3.Added_On__c= system.today();
    a3.Asset_Status__c='Added';
    a3.Added_On__c = system.today();
   // a3.Asset_Inventory__c = ai.id;
    a3.Access_Name__c = 'Test';
    a3.Access_Type__c = 'Building';
    a3.Email_Subject_Asset__c='email';
    a3.Removed_On__c = system.today();
    a3.Termination_Status__c ='Active';
    a3.Terminating__c = false;
    a3.Updating__c = false;
    a3.Application_Owner__c = u.id;
    a3.Application_Implementor__c = u.id;
    a3.Employee__c = emp.id;
    try
        {
        insert a3;
         }
        catch(Exception e)
        {
        system.debug('exception'+e);
        }


    a3.Asset_Status__c='Removal In Process';
    //update a3;  
    
        System.assertEquals(a3.Employee__c,emp.id);
   
     pagereference ref = system.Currentpagereference();
     ref.getParameters().put('id',emp.id);  
     ApexPages.StandardController controller = new ApexPages.StandardController(emp);
     
            list<ProcessInstance> processInstances  = [select id from ProcessInstance where TargetObjectId =:emp.id];
        // system.assertequals(processInstances.size(),1);
     NewHireQuoteApprovalHistoryController1 n = new NewHireQuoteApprovalHistoryController1();
     n.getwcampList();
      n.getquoteId1();
      n.setquoteId1('s');
     NewHireQuoteApprovalHistoryController1.MyWrapper m = new NewHireQuoteApprovalHistoryController1.MyWrapper();
     
     //NewHireQuoteApprovalHistoryController1 mcls = new NewHireQuoteApprovalHistoryController1(controller);
     //mcls.wcampList = mcls.getwcampList();
    
    Test.stopTest();
     }
  
  }