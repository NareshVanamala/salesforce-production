@isTest
private class TestforBDsystemsclass 
 {
   public static testMethod void TestforBDsystemsclass () 
   {
          Account a= new Account();
          a.name='Testaccount1';
          //RecordType rtypes = [Select Name, Id From RecordType where isActive=true and sobjecttype='Account'and name='National_Accounts'];
          //a.recordtypeid=rtypes.id;
          insert a;  
          
          B_D_Systems__c ct= new B_D_Systems__c();
          ct.Account__c=a.id;
          ct.Description__c='TestingtheBDsystem';
          ct.name='Testingthe Program';
          insert ct;
          
          B_D_Systems__c ct2= new B_D_Systems__c();
          ct2.Account__c=a.id;
          ct2.Description__c='TestingtheBDsystem23';
          ct2.name='Testingthe Programyy';
          insert ct2;
          
          String nameFile = 'hello';
                   
           Attachment attachment= new Attachment();
           attachment.OwnerId = UserInfo.getUserId();
           attachment.ParentId = ct.Id;// the record the file is attached to
           attachment.Name = nameFile;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
           attachment.body=bodyBlob;
           insert attachment;
           
              
         ApexPages.currentPage().getParameters().put('id',a.id);
         ApexPages.StandardController controller = new ApexPages.StandardController(a); 
         BDsystemsclass x = new BDsystemsclass(controller); 
         x.nameFile='Vasu_Snehal';
         Blob blb=Blob.valueOf('Vasu_SnehalVasu_SnehalVasu_Snehal');         
         x.contentFile=blb;
         x.addrow.Name='B_D_Systems Name';
         List<Attachment> attachments=[select id, name from Attachment where parent.id=:ct.id];
         System.assertEquals(1, attachments.size());
                       
         x.newProgram();
         x.savenewProg();
         x.editrecord();
         //x.viewRecord();
         x.SaveRecord();
         x.removecon();
         x.cancelnewProg(); 
                  

       }
         
          
           
 }