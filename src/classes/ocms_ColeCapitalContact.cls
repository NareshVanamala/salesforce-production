global virtual with sharing class ocms_ColeCapitalContact extends cms.ContentTemplateController
{
    @TestVisible private List<SObject> ProductList;
    @TestVisible private String html;
    
   /* Service methods */
    public List<SObject> getProductList()
    {
       
        list<String>UserNameList=new list<String>();
        set<String>UserNameSet=new set<String>(); 
        list<String>UserContactlist=new list<String>();
       
        list<SObject>Externallist=new list<SObject>();
        list<SObject>Internallist=new list<SObject>();
        list<SObject>Virtuallist=new list<SObject>();
        
        set<String>UserContactSet=new set<String>();
        List<SObject> ProductList=new list<Sobject>();
        User u=[select id, firstname, lastname,contactid from user where id=:UserInfo.getUserId()];
        string cid=u.contactid ;
        Contact C=[select id,Image_URL__c,firstName,Internal_Wholesaler1__c,Sales_Director__c,Title,Regional_Territory_Manager1__c,lastName,Internal_Wholesaler__c,Wholesaler__c,Regional_Territory_Manager__c from Contact where id=:cid];
        string internalwholealer=C.Internal_Wholesaler1__c;
        String SalesDirectorName=C.Sales_Director__c;
        String RegionalTerritoryName=C.Regional_Territory_Manager1__c;
        set<string>VirtualUserset=new set<String>();
        set<string>InternalUserset=new set<String>();
        set<string>ExternalUserset=new set<String>();
       
         if(SalesDirectorName!=null)
          ExternalUserset.add(SalesDirectorName);
          
         if(internalwholealer!=null) 
         InternalUserset.add(internalwholealer);
         
         if(RegionalTerritoryName!=null)
         VirtualUserset.add(RegionalTerritoryName);
         
       
        //Userset.add(RegionalTerritoryName);
        //Userset.add(internalwholealer);
        //Userset.add(SalesDirectorName);
         
        User RegionalTerritoryManager;
        User InternalSales;
        User ExteranlSales; 
         //list<User>relatedUserlist=[select id,name,Address,Phone,email from User where id in:ExternalUserset];
        if(ExternalUserset.size()>0)
          ExteranlSales = [select id,name,Custom_Mobile_Phone__c,CustomPhone__c,CustomEmail__c,Title,Image_URL__c from User where id in:ExternalUserset limit 1];
        
        if(InternalUserset.size()>0)
         InternalSales=[select id,name,Custom_Mobile_Phone__c,CustomPhone__c,CustomEmail__c,Title,Image_URL__c from User where id in:InternalUserset limit 1];
       
        if(VirtualUserset.size()>0)
         RegionalTerritoryManager =[select id,name,Custom_Mobile_Phone__c,CustomPhone__c,CustomEmail__c,Title,Image_URL__c from User where id in:VirtualUserset limit 1];
        
        
        //query+=' where ( name=:InternalName or name=:SalesDirector or name=:RegionalTerritoryOfficer ) and email!=null';
          if(ExteranlSales!=null)
          ProductList.add(ExteranlSales);
          if(InternalSales!=null)
          ProductList.add(InternalSales);
          if(RegionalTerritoryManager!=null)
         ProductList.add(RegionalTerritoryManager);
         
        
       // ProductList= Database.query(query );
        return ProductList;
       //return sanitizeList(ProductList);
    }
       
      /* Create Content */
    @TestVisible private String writeControls()
    {
        String html = '';
        return html;
     }
     @TestVisible private String writeListView()
     {
        String html = '';
        User u=[select id, firstname, lastname,contactid from user where id=:UserInfo.getUserId()];
        string cid=u.contactid ;
        Contact C=[select id,Image_URL__c,firstName, lastName,Internal_Wholesaler__c,Wholesaler__c,Regional_Territory_Manager__c from Contact where id=:cid];
        string INName=C.Internal_Wholesaler__c;
        String SDName=C.Wholesaler__c;
        String RTName=C.Regional_Territory_Manager__c;
        string conid=c.id;
        
        html += '<input type="hidden" id="INname" value="' + INName + '" />';
        html += '<input type="hidden" id="SDname" value="' + SDName + '" />';
        html += '<input type="hidden" id="RTname" value="' + RTName + '" />';
        html += '<input type="hidden" id="contactid" value="' + c.id + '" />';
              
         html += '<article class="topMar7 wrapper teer3">'+
        '<h1>My Cole Capital Contacts</h1>'+
        '<div class="clearfix">&nbsp;</div>'+
        '<article class="topMar50">';
                
       if(ProductList.size()>0)
       {
       html += '<div class="colone borderR padR45">';
        for(SObject pl : ProductList)
        {
            if(pl.get('Image_URL__c')==null)
            {
            html +='<img height="170px" src="/colecapital/servlet/servlet.FileDownload?file=00P5000000KSr08EAD" width="136px" />';
            }
            else
            {
            html +='<img height="170px" src="'+ pl.get('Image_URL__c') +'" width="136px" />';
            }
            html+='<div class="newsBlock">'+
            '<div class="listText"><span>'+pl.get('name')+'</span><br />';
            if(pl.get('Title')!=null)
            {
            html +='<span>'+pl.get('Title')+'</span><br/>';
            }
            if(pl.get('CustomEmail__c')!=null)
            {
            html +='<span>'+pl.get('CustomEmail__c')+'</span><br/>';
            }
            if(pl.get('CustomPhone__c')!=null)
            {
            html +='<span>'+pl.get('CustomPhone__c')+'</span><br/>';
            }
            if(pl.get('Custom_Mobile_Phone__c')!=null)
            {
            html +='<span>'+pl.get('Custom_Mobile_Phone__c')+'</span><br/>';
            }
          html +='<span><a class="button nospace" href="/c__downloadContact?id='+pl.get('id')+'">download contact info</a></span></div>&nbsp;'+
          //  html +='</div>&nbsp;'+
           '</div>';
         //   '<div class="clearfix">&nbsp;</div></div>';
       }   
         html+='</div>'+
         '<aside class="latestnews">'+
            '<h5>Connect with Cole</h5>'+
            '<input class="button nospace" type="button" value="Request a Call" name="registerbtn1" onclick="requestacall();"/>'+
            '<div class="clearfix">&nbsp;</div>'+
            '<input class="button nospace" type="button" value="Schedule a Meeting" name="registerbtn2" onclick="scheduleameeting();"/>'+
            '<div class="clearfix">&nbsp;</div>'+
            '<input class="button nospace" type="button" value="Schedule a Seminar" name="registerbtn3" onclick="scheduleaseminar();"/>'+
            '<div class="clearfix">&nbsp;</div>'+
            '<p>Submit Comments or Questions:<br/>'+
            '<input class="register-input firstname" id="Comments" name="Comments" type="text" /></p>'+
            '<div class="clearfix">&nbsp;</div>'+
            '<input class="button nospace" type="button" value="submit" name="registerbtn" onclick="submitquestions();"/></aside>';
         
         
       }  
       else
       {
       html +='<p>There are no cole contacts to be displayed,please contact us.</p>';
       }
        html += '</article>'+          
            
            '<div class="clearfix">&nbsp;</div>'+
            '</article>';
            
          return html;              
     }             
       
       
        global override virtual String getHTML()
        {
            String html = '';
            
            html += writeControls();
            
            ProductList= getProductList();
            if(ProductList.size()>0)
            html += writeListView();
            html += '<script src="resource/1452790433000/ocms_colecapitalcontact" type="text/javascript"></script>';
            return html;
        }

}