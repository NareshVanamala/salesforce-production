@isTest
  private class StateApprovalTrackingTest{
      static testMethod void AZtest(){
        Case c = new case(Subject = 'Test AZ Case');
        insert c;
        
        Test.startTest();
        c.AZ__c = true;
        update c;
        Test.stopTest();
        
        List<State_Approval_History__c> fieldHistory = [select id,Case__c,Approved_By__c,Old_Value__c,New_Value__c from State_Approval_History__c where Case__c = :c.Id];
        System.assertEquals(1,fieldHistory.size());
        System.assertEquals('false',fieldHistory[0].Old_Value__c);
        System.assertEquals('true',fieldHistory[0].New_Value__c);
        System.assertEquals(c.Id,fieldHistory[0].Case__c);
        
   }
  }