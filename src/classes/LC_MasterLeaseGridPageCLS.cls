public with sharing class LC_MasterLeaseGridPageCLS 
{
    public List<Lease__c> leList {get;set;}
    public LC_LeaseOpprtunity__c loList;
    public Id recId {get;set;}
    public List<Lease__c> allLease{get;set;}
    public List<Lease__c> selectedLeasess{get;set;}
    public List<wrapLease> wrapLeaseList {get; set;}
    public Boolean selected {get; set;}
    public boolean ShowMasterGrid{get; set;}
    private ApexPages.StandardSetController standardController;
    public LC_MasterLeaseGridPageCLS(ApexPages.StandardController standardController) 
    {
        recId = ApexPages.currentPage().getParameters().get('id');
        selectedLeasess = new List<Lease__c>();   
        system.debug('************************************* SELECTED LIST IN CON'+selectedLeasess);     
        loList = [Select id,Name,MRI_Property__c,Master_Lease__c,Lease__r.Master_Lease_ID__c,Lease_Opportunity_Type__c From LC_LeaseOpprtunity__c where id=:recId]; 
   if((loList.Master_Lease__c==True)&&((loList.Lease_Opportunity_Type__c=='Master Lease - New')||(loList.Lease_Opportunity_Type__c=='Master Lease - Amendment')||(loList.Lease_Opportunity_Type__c=='Master Lease - Renewal')||(loList.Lease_Opportunity_Type__c =='Master Lease - Assignment')||(loList.Lease_Opportunity_Type__c =='Master Lease - Rent Restructure & Extension')||(loList.Lease_Opportunity_Type__c =='Master Lease - Sublease')))
        {
            ShowMasterGrid=True;
            Id mriid =loList.MRI_Property__c;
            String masid;
            leList = [select id,Name,SuiteId__c,MRI_PROPERTY__c,Property_ID__c,MRI_PROPERTY__r.Name,LeaseId__c,Master_Lease__c,MRI_PROPERTY__r.Common_Name__c,Master_Lease_ID__c,Center_Name__c,Tenant_Contact_Name__c,MRI_Lease_ID__c,
            Tenant_Name__c,Suite_Sqft__c,MRI_PROPERTY__r.NOI__c,MRI_PROPERTY__r.Property_ID__c,MRI_PROPERTY__r.Property_Owner_SPE__c,MRI_PROPERTY__r.Property_Owner_SPE__r.Name,Expiration_Date__c from Lease__c where Master_Lease__c=true and MRI_PROPERTY__c=:mriid];
            string stri=leList[0].Master_Lease_ID__c;
            allLease = [select id,Name,SuiteId__c,MRI_PROPERTY__c,Property_ID__c,LeaseId__c,MRI_PROPERTY__r.Name,MRI_PROPERTY__r.Common_Name__c,Master_Lease__c,Master_Lease_ID__c,Center_Name__c,Tenant_Contact_Name__c,MRI_Lease_ID__c,
            Tenant_Name__c,Suite_Sqft__c,MRI_PROPERTY__r.NOI__c,MRI_PROPERTY__r.Property_ID__c,MRI_PROPERTY__r.Property_Owner_SPE__c,MRI_PROPERTY__r.Property_Owner_SPE__r.Name,Expiration_Date__c from Lease__c where Master_Lease_ID__c=:stri and Master_Lease__c=true  ];
                 
            for(Lease__c le:leList) 
            {
                masid = le.Master_Lease_ID__c;
                system.debug('********************************** Master ID: '+masid);
            }
            if(wrapLeaseList == null) 
            {
                wrapLeaseList = new List<wrapLease>();
                for(Lease__c Lea1:allLease) 
                {
                wrapLeaseList.add(new wrapLease(Lea1));
                system.debug('********************** wrapLeaseList VALUES: '+wrapLeaseList);
                }
            }
          
        }
        else if(loList.Master_Lease__c==False)
        ShowMasterGrid=False;
    }
 
    public PageReference Selectrec() 
    {
       
        List<Helper_for_Master_Lease__c> hmlList = new List<Helper_for_Master_Lease__c>();
     
       if(wrapLeaseList.size()>0)
       {
        for(wrapLease wrapLeaseobj : wrapLeaseList) 
        {
            if(wrapLeaseobj.selected == true) 
            {
                selectedLeasess.add(wrapLeaseobj.Leas);
            }
        }
        system.debug('************************************* SELECTED LIST'+selectedLeasess);
         }
         string ss;
       
             Map<id,Helper_for_Master_Lease__c> hmlMap = new Map<id,Helper_for_Master_Lease__c>([Select Id,Name,Lease_Opportunity__c,MRI_Property__c,Lease__c from Helper_for_Master_Lease__c where Lease_Opportunity__c=:recId]);                   
            
            for(Lease__c ct:selectedLeasess)
            {
                
                Helper_for_Master_Lease__c hml = new Helper_for_Master_Lease__c();
              
                for (Id id : hmlMap.keySet()) {  
                System.debug('************************ MAP MRI ' + hmlMap.get(id).MRI_Property__c);
                if((hmlMap.get(id).MRI_Property__c == ct.MRI_PROPERTY__c) && (hmlMap.get(id).Lease__c == ct.id)) 
                {
                   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Record already selected for New Master Lease. Please select another record.'));
                   return null;
                }
            } 
                hml.Building_Id__c=ct.MRI_PROPERTY__r.Property_ID__c;
                hml.Name__c=ct.Center_Name__c;
                hml.GLA_Suite_SQFT__c=ct.Suite_Sqft__c;
                hml.Going_on_NOI1__c=ct.MRI_PROPERTY__r.NOI__c;
                hml.Lease__c=ct.id;
                hml.Lease_Expiration_Date__c=ct.Expiration_Date__c;
                hml.Lease_Id__c=ct.MRI_Lease_ID__c;
                hml.Lease_Opportunity__c=recId;
                hml.LTC_Name__c=ct.Tenant_Name__c;
                hml.Master_Lease_Id__c=ct.Master_Lease_ID__c;
                hml.MRI_Property__c=ct.MRI_PROPERTY__c;
                
                hmlList.add(hml);                   
            }   
            if(!hmlList.isEmpty()) 
            {
                if (Schema.sObjectType.Helper_for_Master_Lease__c.isCreateable())

                insert hmlList;
            } 
     
       
        return Null;
    }

    @TestVisible  public class wrapLease 
    {
        public Lease__c Leas {get; set;}
        public Boolean selected {get; set;}
      @TestVisible  public wrapLease(Lease__c l) 
        {
            Leas = l;
            selected = false;
        }
    }

}