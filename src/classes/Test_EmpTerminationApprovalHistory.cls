@isTest(seeAlldata=true)
private class Test_EmpTerminationApprovalHistory
{   
    
    public static testMethod void Testforfinraclass () 
    {   
        Id uid=[Select id,name from user limit 1].id;
        Id rtp=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormParent' and SobjectType = 'NewHireFormProcess__c'].id;
        Id rtc=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormChild' and SobjectType = 'NewHireFormProcess__c'].id;
    
     
        NewHireFormProcess__c nh1 = new NewHireFormProcess__c();
        nh1.recordtypeid=rtp;
        nh1.HR_ManagerApprovalStatus__c='Approved';
        nh1.MRI_Access_Requested__c = true;
        nh1.If_MRI_Access_is_Needed_Indicate_Secur__c = 'test';
        nh1.Salesforce_Deal_Central_Access_Required__c = true;
        nh1.First_Name__c = 'Testing';
        nh1.Last_Name__c = 'parent';
        nh1.Supervisor__c = 'Ninad';
        nh1.Office_Location__c = 'Chennai';
        nh1.Employee_Role__c ='Developer';
        nh1.DepartmentUpdate__c = '26-Information Technology';
        nh1.status__c='closed';
        nh1.Webex_Access_Requested__c = true;
        nh1.VDI_Access_Requested__c = true;
        nh1.VPN_Access_Requested__c = true;
        nh1.Argus_Access_Requested__c = true;
        nh1.DST_Access_Requested__c = true;
        nh1.AVID_Account_Requested__c = true;
        nh1.BNA_Access_Requested__c = true;
        nh1.Box_com_Requested__c = true;
        nh1.Chatham_Requested__c = true;
        nh1.ADP_Requested__c = true;
        nh1.Concur_Account_Requested__c = true;
        nh1.Data_Warehouse_GL_Requested__c  = true;
        nh1.Data_Warehouse_Real_Estate_Requested__c  =true;
        nh1.Data_Warehouse_Sales_Requested__c  =true;
        nh1.Kardin_Requested__c  =true;
        nh1.OneSource_Requested__c  =true;
        nh1.RisKonnect_Requested__c =true;
        nh1.Web_Filings_Requested__c = true;
        nh1.Parking_General__c = true;
        nh1.Parking_Executive__c =true;
        nh1.Parking_Contractor__c = true;
        nh1.Provide_Desktop__c =true;
        nh1.Deploy_External_IT_Package__c =true;
        nh1.Provide_Laptop__c =true;
        nh1.Dual_Monitor_Display__c =true;
        nh1.Provide_iPhone__c =true;
        nh1.Provide_iPad__c =true;
        nh1.General__c =true;
        nh1.Facilities__c =true;
        nh1.RE_Legal__c =true;
        nh1.IT_Infrastructure__c =true;
        nh1.HR__c =true;
        nh1.Compliance__c =true;
        nh1.Property_Management__c =true;
        nh1.Audio_Visual__c =true;
        nh1.Executive__c =true;
        nh1.B2_Storage__c =true;
        nh1.Active_Directory_Group_1__c = 'Sec-26-Information Technology';
        nh1.Active_Directory_Group_2__c = 'Sec-26-Information Technology';
        nh1.Active_Directory_Group_3__c = 'Sec-26-Information Technology';
        nh1.Active_Directory_Group_4__c = 'Sec-26-Information Technology';
        nh1.Active_Directory_Group_5__c = 'Sec-26-Information Technology';
        nh1.Active_Directory_Group_6__c = 'Sec-26-Information Technology';
        
        // nh1.status__c='In Progress';
        nh1.Active_Directory_Group2_Status__c = 'Approved';
        nh1.Active_Directory_Group1_Status__c = 'Approved';
        nh1.Active_Directory_Group3_Status__c = 'Approved';
        nh1.Active_Directory_Group4_Status__c = 'Approved';
        nh1.Active_Directory_Group5_Status__c = 'Approved';
        nh1.Active_Directory_Group6_Status__c = 'Approved';
        nh1.ADP_Request_Status__c = 'Approved';
        nh1.Argus_Access_Request_Status__c = 'Approved';
        nh1.AVID_Account_Request_Status__c = 'Approved';
        nh1.BNA_Access_Request_Status__c = 'Approved';
        nh1.Box_com_Request_Status__c = 'Approved';
        nh1.Building_Audio_Visual_Status__c = 'Approved';
        nh1.Building_B2Storage_Status__c = 'Approved';
        nh1.Building_Compliance_Status__c = 'Approved';
        nh1.Building_Executive_Status__c = 'Approved';
        nh1.Building_Facilities_Status__c = 'Approved';
        nh1.Building_General_Status__c = 'Approved';
        nh1.Building_HR_Status__c = 'Approved';
        nh1.Building_IT_Infrastructure_Status__c = 'Approved';
        nh1.Building_PropertyManagement_Status__c = 'Approved';
        nh1.Building_RELegal_Status__c = 'Approved';
        nh1.Chatham_Request_Status__c = 'Approved';
        nh1.Concur_Account_Request_Status__c = 'Approved';
        nh1.Data_Warehouse_GL_Request_Status__c = 'Approved';
        nh1.DataWarehouse_RealEstate_RequestStatus__c = 'Approved';
        nh1.Data_Warehouse_SalesRequestStatus__c = 'Approved';
        nh1.DST_Access_Requested_Status__c = 'Approved';
        nh1.HR_ManagerApprovalStatus__c = 'Approved';
        nh1.Kardin_Request_Status__c = 'Approved';
        nh1.MRI_Request_Status__c = 'Approved';
        nh1.OneSource_Request_Status__c = 'Approved';
        nh1.Parking_Executive_Status__c = 'Approved';
        nh1.Parking_General_Status__c = 'Approved';
        nh1.Parking_Contractor_request__c = 'Approved';
        nh1.Remote_Desktop_Request_Status__c = 'Approved';
        nh1.RisKonnect_Request_Status__c = 'Approved';
        nh1.SalesForce_Deal_Central_Request_Status__c = 'Approved';
        nh1.VDI_Access_Request_Status__c = 'Approved';
        nh1.VPN_Access_Request_Status__c = 'Approved';
        nh1.Webex_Access_Request_Status__c = 'Approved';
        nh1.Web_Filings_Request_Status__c = 'Approved';
        
        nh1.Hardware_Deploy_External__c = 'Approved';
        nh1.Hardware_Dual_Monitor_Display__c = 'Approved';
        nh1.Hardware_Provide_Desktop__c = 'Approved';
        nh1.Hardware_Provide_Ipad__c = 'Approved';
        nh1.Hardware_Provide_Iphone__c = 'Approved';
        nh1.Hardware_Provide_Laptop__c = 'Approved';
        
        insert nh1;
     
        Employee__c e = new Employee__c();
        e.First_Name__c = nh1.First_Name__c;
        e.Last_Name__c = nh1.Last_Name__c;
        e.Supervisor__c = nh1.Supervisor__c;
        e.Department__c= nh1.DepartmentUpdate__c;
        e.Title__c = nh1.Title__c;
        e.Office_Location__c = nh1.Office_Location__c;
        e.Start_Date__c = nh1.Start_Date__c;
        e.Employee_Role__c = nh1.Employee_Role__c;
        e.Employee_Status__c = 'Terminated';
        insert e;

        Asset__c a = new Asset__c();
        a.Access_Name__c = 'MRI';
        a.Access_Type__c = 'Application';  
        a.Employee__c = e.id;
        a.Termination_Status__c = 'Inactive';
        a.Application_Implementor__c = uid;
        insert a;
        
        system.assertequals(a.Employee__c,e.id);

        //OppController.wrapperOpp Wrapvar = new OppController.wrapperOpp();
        //EmpTerminationApprovalHistory.MyWrapper em =new EmpTerminationApprovalHistory.MyWrapper();
        //em.getwcampList();
        
        
    }
}