@isTest
Public class RC_CreateClaimClass_Test
{
  static testmethod void testCreateClaimClass()
  {
     
      Account ac= new Account();
      ac.name='Test-Account';
      insert ac;
  
  
      RC_Property__c PRT=new RC_Property__c();
     PRT.name='Amazon DC 2014 USAA~A11123';
     insert PRT;
     

     
     RC_Location__c LC=new RC_Location__c();
     LC.name='Amazon DC 2014 USAA~A11123';
     LC.Entity_Tenant_Code__c='12345';
     LC.Property__c=PRT.id;
      LC.Account__c=ac.id;
     insert LC;
           System.assertEquals(PRT.name,LC.name);

      RC_Claim__c rcClaim= new RC_Claim__c();
      rcClaim.Location__c=LC.id;
      rcClaim.Date_of_Loss__c=system.today();
      insert rcClaim;
     
       RC_CreateClaimClass RcClass= new RC_CreateClaimClass();
       RcClass.showCoveragetype=true;
       RcClass.showClaimtype=false;
       RcClass.showClaimantType=false;
       RcClass.showThirdPartyManager=false;
       RcClass.showPoliceinformation=false;
       RcClass.showMedicalinformation=false;
       RcClass.showNewWitnessScreen=false;
       RcClass.showExistingWitnesscreen =false;
       RcClass.showAddAttachment =False;
       RcClass.selectedlocation='Amazon DC 2014 USAA~A11123';
       RcClass.Newclaimcrated=rcClaim;
       //make all the link except coverage type link
       RcClass.Coveragelink=true;
       RcClass.Submitlink=true;
       RcClass.medicallink=false;
       RcClass.Claimink=false;
       RcClass.ClaimantTypeink=false;
       RcClass.Policeink=false;
       RcClass.ThirdPartyManagerink=false;
       RcClass.Witnesslink=false;  
       RcClass.ShowCoverageTypescereen();
       RcClass.Newclaimcrated.Coverage_Type__c='GLBI';
       RcClass.GLBICoveragetype=true;
       RcClass.SaveCoverageType();
       RcClass.Newclaimcrated.Date_of_Loss__c=system.today();
       RcClass.Newclaimcrated.Accident_Description__c='Chcekcheck check';
       RcClass.Newclaimcrated.Fund_Name__c='CCIT';
       RcClass.Newclaimcrated.Location__c=LC.id;
       RcClass.SaveClaimDetails();
       RcClass.SaveClaimantInfo();
       RcClass.Newclaimcrated.Local_Site_Manager_First_Name__c='FirstName';
       RcClass.Newclaimcrated.Local_Site_Manager_Last_Name__c='LastName';
       RcClass.Newclaimcrated.Local_Site_Manager_Phone__c='6022286138';
       RcClass.Newclaimcrated.Local_Site_Manager_Email__c='ntambe@aracpreit.com';
       RcClass.Newclaimcrated.Local_Site_Manager_Comments__c='TestTestTest';
       RcClass.SaveThirdpartyManagerInfo();
       RcClass.SavePoliceInformation();
       RcClass.SaveMedicalInformation();
       RcClass.newWiness.First_Name__c='Ninad';
       RcClass.newWiness.Claim__c=rcClaim.id;
       RcClass.SaveWitnessInfo();
       RcClass.GotoAddWitnessScreen();
       RcClass.NoGotoSubmitScreen();
       RcClass.showClaimantInfrmpage();
       RcClass.showThirdpartyManger();
       RcClass.showClaimInfrmpage();
       RcClass.showPolicyManger();
       RcClass.showmedicalInformation();
        RcClass.showAddAttachment();
       RcClass.Newclaimcrated.Coverage_Type__c='GLBI';
       RcClass.GLBICoveragetype=true;
       RCClass.AddWitness();
       RCClass.showwitnesslInformation();
       Blob b = Blob.valueOf('Test Data');
       Attachment attachment = new Attachment();
       attachment.ParentId = rcClaim.ID;
       attachment.Name = 'Test Attachment for Parent';
       attachment.Body = b;
       insert(attachment);
       RcClass.addattchment();
       RcClass.attsave();
       RcClass.addMore();
       RcClass.attdone();
  
  
    
       RcClass.GLBICoveragetype=false;
       RCClass.GLPDCoveragetype=true;
       RcClass.SaveCoverageType();
  
  
  }
  
}