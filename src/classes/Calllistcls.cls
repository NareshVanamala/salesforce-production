public with sharing class Calllistcls
{
   /* CallList__c callList ;

   
    public List<CallList__c > results{get;set;}
    public string edituserLogic{get;set;}
    
    List<CallList__c > clist= new List<CallList__c >();
    List<Automated_Call_List__c> autolist= new List<Automated_Call_List__c>();
    List<User> ulist = new List<User>();
    public Boolean flag{get;set;}
    public String vname{get;set;}
    public Boolean success{get;set;}
    public Boolean bnNext{get;set;}
    public Boolean bnPrevious{get;set;}
    public Boolean bnFirst{get;set;}
    public Boolean bnLast{get;set;}
   
    String Fieldnamea='--None--';
    String Fieldname1b='--None--';
    String Fieldname2c='--None--';
    String Fieldname3d='--None--';
    String Fieldname4e='--None--';
    String Operatora='';
    String Operator1b='';
    String Operator2c='';
    String Operator3d='';
    String Operator4e='';
    String valuea='';
    public String value1b{get;set;}
    public String value2c{get;set;}
    public String value3d{get;set;}
    public String value4e{get;set;}
    public String description{get;set;}
    public Integer cmCount;
    public Integer first{get;set;}
    public Integer rows{get;set;}
    public String CampaignName{get;set;}
    public CallList__c filter{get;set;}
    String[] Username= new String[]{};
    List<id>ownerOfNew = new list<id>();
    list<User>OwnerOfNewCon=new list<User>();
    String username1='';
    public Map<String,String> mapfieldnames{get; set;}
    public List<Contact> conlist{get;set;}
    public ApexPages.StandardSetController ssc;
    PageReference pageRef12;

    list<Contact_Call_List__c> lsttask = new list<Contact_Call_List__c>();
    public list<contact> lstContact{set;get;}
    public List<SelectOption> options1{set;get;} 
    public String CallList1 {set;get;}
    public Map<Id,Contact_Call_List__c> mapOfContactToTask{get;set;}
    public Map<integer,Contact_Call_List__c> rowmap{get;set;}
    public boolean showPrev{set;get;}
    public boolean showNext{set;get;}
    public Integer rownum {get;set;}{rownum=-10;}
    public Boolean bnSelect{get;set;}   
    public Contact_Call_List__c tskObj {set;get;}
    public list<Contact_Call_List__c>contactcalllist{get;set;}
   
    id userid;
    Integer PageNo = 1;
    
    public void init()
    {
       flag=false;
       success=false;
       cmCount = 0;
       value1b='';
       value2c='';
       value3d='';
       value4e='';
       CampaignName='';
       Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
       Schema.SObjectType sobjType = gd.get('contact');
       Schema.DescribeSObjectResult r = sobjType.getDescribe();
       Map<String,Schema.SObjectField> M = r.fields.getMap();
       System.debug('+++m'+M);
       for(String fieldName1 : M.keyset())
        {
            Schema.SObjectField field = M.get(fieldName1);                                                    
            Schema.DescribeFieldResult fieldDesc = field.getDescribe();                    
            System.debug('+++++label'+fieldDesc.getLabel());
            System.debug('+++++label'+fieldDesc.getname());
            mapfieldnames.put(fieldDesc.getname(),String.ValueOf(fieldDesc.gettype()));                                        
        }
    } 

    
    public CallList__c getCallList()
    {        
        if(callList == null)
        callList = new CallList__c ();
        return callList;
    }

    
    public Boolean validate()
    {
        if(callList.CampaignName__c== NULL && callList.Field_API__c == null && callList.Field_API1__c == null && 
        callList.Field_API2__c == null && callList.Field_API3__c == null && 
        callList.Field_API4__c == null && callList.Operator__c == null && 
        callList.Operator1__c == null  && callList.Operator2__c == null && 
        callList.Operator3__c == null && callList.Operator4__c == null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter atleast one filter value'));
            return false;
        }
        if((callList.Field_API__c != null && callList.Operator__c == null) || (callList.Field_API__c == null && callList.Operator__c != null) ||          
        (callList.Field_API1__c != null && callList.Operator1__c == null) || (callList.Field_API1__c == null && callList.Operator1__c != null) ||
        (callList.Field_API2__c != null && callList.Operator2__c == null) || (callList.Field_API2__c == null && callList.Operator2__c != null) ||
        (callList.Field_API3__c != null && callList.Operator3__c == null) || (callList.Field_API3__c == null && callList.Operator3__c != null) ||
        (callList.Field_API4__c != null && callList.Operator4__c == null) || (callList.Field_API4__c == null && callList.Operator4__c != null))
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select the operator'));
            return false;
        } 
         return true;
    } 
    public Calllistcls() 
    {
        first=0;
        rows=10;
        bnNext = false;
        bnPrevious = false;
        bnFirst = false;
        bnLast = false;
        bnselect = false;
        calllist = new CallList__c ();
        mapfieldnames = new Map<String,String>();
        mapfieldnames.put('--None--','--None--');
        mapfieldnames.put('Account.Name','STRING');
        
        
        
        lstContact = new list<contact>();
        set<String> call = new Set<String>();    
        List<String> lscall = new List<String>();
        showPrev = false;
        showNext = false;
      
    } 
    
    
    
    public string getFormTag() 
    {
         return System.currentPageReference().getParameters().get('frm');
    }  
    
    public string getTextBox() 
    {
         return System.currentPageReference().getParameters().get('txt');
    } 
    public List<SelectOption> getFields()
    {
       List<SelectOption> fldoptions = new List<SelectOption>();
       Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
       Schema.SObjectType sobjType = gd.get('Contact');
       Schema.DescribeSObjectResult r = sobjType.getDescribe();
       Map<String,Schema.SObjectField> M = r.fields.getMap();
       System.debug('+++m'+M);
       fldoptions.add(new SelectOption('--None--','--None--'));
       fldoptions.add(new SelectOption('Account.Name','Account Name'));
       for(String fieldName1 : M.keyset())
       {
         Schema.SObjectField field = M.get(fieldName1);                                                    
         Schema.DescribeFieldResult fieldDesc = field.getDescribe();                    
         System.debug('+++++label'+fieldDesc.getLabel());
         System.debug('+++++label'+fieldDesc.getname());
         mapfieldnames.put(fieldDesc.getname(),String.ValueOf(fieldDesc.gettype()));
         fldoptions.add(new SelectOption(fieldDesc.getname(),fieldDesc.getlabel()));                                        
       }
       SelectOptionSorter.doSort(fldoptions, SelectOptionSorter.FieldToSort.Label);
       return fldoptions;
    }

    
    public String getFieldnamea()
    {
       return Fieldnamea;
    }        
    public void setFieldnamea(String Fieldnamea)
    {
       this.Fieldnamea= Fieldnamea;
    }
    
    
    public String getFieldname1b() 
    {
      return Fieldname1b;
    }
    public void setFieldname1b(String Fieldname1b) 
    {
      this.Fieldname1b = Fieldname1b;
    }

    public String getFieldname2c() 
    {
      return Fieldname2c;
    }
    public void setFieldname2c(String Fieldname2c) 
    {
      this.Fieldname2c = Fieldname2c;
    }

    public String getFieldname3d() 
    {
      return Fieldname3d;
    }
    public void setFieldname3d(String Fieldname3d) 
    {
      this.Fieldname3d = Fieldname3d;
    }

    public String getFieldname4e() 
    {
      return Fieldname4e;
    }
    public void setFieldname4e(String Fieldname4e) {
      this.Fieldname4e = Fieldname4e;
    }
    public List<SelectOption> getOperatorList() 
    {
         List<SelectOption> OperatorList = new List<SelectOption>();
         OperatorList=createOperatorList(Fieldnamea);
         return OperatorList;
    }
     
    public List<SelectOption> getOperatorList1() 
    {
         List<SelectOption> OperatorList1 = new List<SelectOption>();
         OperatorList1=createOperatorList(Fieldname1b);
         return OperatorList1;  
    }
     
    public List<SelectOption> getOperatorList2() 
    {
         List<SelectOption> OperatorList2 = new List<SelectOption>();
         OperatorList2=createOperatorList(Fieldname2c);
         return OperatorList2;
    }

    public List<SelectOption> getOperatorList3() 
    {
         List<SelectOption> OperatorList3 = new List<SelectOption>();
         OperatorList3=createOperatorList(Fieldname3d);
         return OperatorList3;
    }

    public List<SelectOption> getOperatorList4() 
    {
         List<SelectOption> OperatorList4 = new List<SelectOption>();
         OperatorList4=createOperatorList(Fieldname4e);
         return OperatorList4;
    }
     
    public List<SelectOption> createOperatorList(String FieldAPI)
    {
        List<SelectOption> Oproptions = new List<SelectOption>();
        Oproptions.add(new SelectOption('', '--None--'));
        if((String.valueof(mapfieldnames.get(FieldAPI))) == 'BOOLEAN' )
        {
          Oproptions.add(new SelectOption('equals', 'equals'));
        }
        else{
        Oproptions.add(new SelectOption('not equal to', 'not equal to'));
        Oproptions.add(new SelectOption('equals', 'equals'));
        if((String.valueof(mapfieldnames.get(FieldAPI))) == 'STRING' || (String.valueof(mapfieldnames.get(FieldAPI))) == 'PICKLIST' || (String.valueof(mapfieldnames.get(FieldAPI))) == 'TEXTAREA' || (String.valueof(mapfieldnames.get(FieldAPI))) == 'PHONE' ||(String.valueof(mapfieldnames.get(FieldAPI))) == 'Email' )
        {  
           Oproptions.add(new SelectOption('starts with', 'starts with'));
           Oproptions.add(new SelectOption('contains', 'contains'));
           Oproptions.add(new SelectOption('does not contain', 'does not contain'));
        }
        else if((String.valueof(mapfieldnames.get(FieldAPI))) == 'INTEGER' || (String.valueof(mapfieldnames.get(FieldAPI)))== 'CURRENCY' || (String.valueof(mapfieldnames.get(FieldAPI)))== 'DOUBLE' || (String.valueof(mapfieldnames.get(FieldAPI))) == 'PERCENT'|| (String.valueof(mapfieldnames.get(FieldAPI))) == 'DATE' ||(String.valueof(mapfieldnames.get(FieldAPI)))== 'DATETIME')    
        {
          Oproptions.add(new SelectOption('less than', 'less than'));
          Oproptions.add(new SelectOption('greater than', 'greater than'));
          Oproptions.add(new SelectOption('less or equal', 'less or equal'));
          Oproptions.add(new SelectOption('greater or equal', 'greater or equal'));
        }    
        else if((String.valueof(mapfieldnames.get(FieldAPI))) == 'MULTIPICKLIST' )
        {
          Oproptions.add(new SelectOption('includes', 'includes'));
          Oproptions.add(new SelectOption('excludes', 'excludes'));
        }  
        }        
        return Oproptions;
    }
     
    public String getOperatora() 
    {
      return Operatora;
    }
    public void setOperatora(String Operatora) 
    {
      this.Operatora = Operatora;
    }
     
    public String getOperator1b() 
    {
      return Operator1b;
    }
    public void setOperator1b(String Operator1b) 
    {
      this.Operator1b = Operator1b;
    }

    public String getOperator2c() 
    {
      return Operator2c;
    }
    public void setOperator2c(String Operator2c) 
    {
      this.Operator2c = Operator2c;
    }

    public String getOperator3d() 
    {
      return Operator3d;
    }
    public void setOperator3d(String Operator3d) 
    {
      this.Operator3d = Operator3d;
    }

    public String getOperator4e() 
    {
      return Operator4e;
    }
    public void setOperator4e(String Operator4e) 
    {
      this.Operator4e = Operator4e;
    } // ---End---

    public String getValuea() 
    {
      return valuea;
    }
    public void setValuea(String valuea) 
    {
      this.valuea = valuea;
    }
     
    public void editFilterCriteria()
    {
     string Viewname = ( String )JSON.deserialize( Apexpages.currentPage().getParameters().get('editCallListNames'), string.class ) ; 
      Automated_Call_List__c lock = [Select Id, Name, TempLock__c from Automated_Call_List__c where Name= :Viewname ];
      System.debug('****' + lock + lock.TempLock__c);
        
     If (lock.TempLock__c != null) {
          
     else
     {
      filter = [Select id,CampaignName__c,Description__c,Custom_Logic__c,CampaignName__r.Name,Field_API__c,Field_API1__c,Field_API2__c,Field_API3__c,Field_API4__c,Field_Name__c,Field_Name1__c,Field_Name2__c,Field_Name3__c,Field_Name4__c,Operator__c,Operator1__c,Operator2__c,Operator3__c,Operator4__c, Value__c,Value1__c,Value2__c,Value3__c,Value4__c from CallList__c where Name =:Viewname];
 
      FieldNamea = filter.Field_API__c==null?'--None--':filter.Field_API__c;
      Operatora= filter.Operator__c;
      valuea = filter.Value__c;
      
      Fieldname1b = filter.Field_API1__c==null?'--None--':filter.Field_API1__c;
      Operator1b= filter.Operator1__c;
      value1b = filter.Value1__c;
      
      Fieldname2c = filter.Field_API2__c==null?'--None--':filter.Field_API2__c;
      Operator2c= filter.Operator2__c;
      value2c = filter.Value2__c;
      
      Fieldname3d = filter.Field_API3__c==null?'--None--':filter.Field_API3__c;
      Operator3d= filter.Operator3__c;
      value3d = filter.Value3__c;
      
      Fieldname4e = filter.Field_API4__c==null?'--None--':filter.Field_API4__c;
      Operator4e= filter.Operator4__c;
      value4e = filter.Value4__c;
      
      edituserLogic = filter.Custom_Logic__c;
      
      description = filter.Description__c;
      System.debug(FieldNamea  + '^^^^^^^^^^' + filter);
      }
    }
    
    public void updateCallList(){       
       string Viewname = ( String )JSON.deserialize( Apexpages.currentPage().getParameters().get('editCallListNames'), string.class ) ; 
        System.debug('**Entering update callist');
        CallList__c filter1 = [Select id,Name,Description__c ,Custom_Logic__c, CampaignName__c,Field_API__c,Field_API1__c,Field_API2__c,Field_API3__c,Field_API4__c,Field_Name__c,Field_Name1__c,Field_Name2__c,Field_Name3__c,Field_Name4__c,Operator__c,Operator1__c,Operator2__c,Operator3__c,Operator4__c, Value__c,Value1__c,Value2__c,Value3__c,Value4__c from CallList__c where Name =:Viewname];
        filter1.Field_API__c= Fieldnamea=='--None--'?null:FieldNamea;
        filter1.Field_API1__c= Fieldname1b=='--None--'?null:FieldName1b;
        filter1.Field_API2__c= FieldName2c=='--None--'?null:FieldName2c;
        filter1.Field_API3__c= FieldName3d=='--None--'?null:FieldName3d;
        filter1.Field_API4__c= FieldName4e=='--None--'?null:FieldName4e;
        filter1.Operator__c = Operatora;
        filter1.Operator1__c = Operator1b;
        filter1.Operator2__c = Operator2c;
        filter1.Operator3__c = Operator3d;
        filter1.Operator4__c = Operator4e;
        filter1.Value__c = valuea ;
        filter1.Value1__c = value1b ;
        filter1.Value2__c = value2c ;
        filter1.Value3__c = value3d ;
        filter1.Value4__c = value4e ;
        filter1.CampaignName__c = filter.CampaignName__c;
        filter1.Description__c = filter.Description__c;
        filter1.Custom_Logic__c=edituserLogic;
        System.debug('filter.Custom_Logic__c'+filter.Custom_Logic__c);
        update filter1;
        List<Contact_call_List__C> updusr = [ Select Id, Owner__c, CallList_Name__c,Assigned_Date__c from Contact_Call_List__C where CallList_Name__c = :filter1.Name And owner__c != Null And Call_Complete__c = False];
       
        Set<Id> userIds = new Set<Id>();
        Map<Id,date> mapOfUsertoassignedDate=new map<Id,date>();
        for (Integer i = 0; i< updusr.size(); i++)
        {
        userIds.add(updusr[i].owner__c);
        mapOfUsertoassignedDate.put(updusr[i].owner__c,updusr[i].Assigned_Date__c);
        }
         System.debug('User Ids'+UserIds);
         System.debug('User and date Map.....This is not fair, man....'+ mapOfUsertoassignedDate);
         List<Calllist__c> cls = [Select Id, CampaignName__c  from calllist__c where ID = :filter1.Id];
               
               
               Set<ID> activatebatch = new Set<ID>();
               
               for(Calllist__c abc : cls)
               {
               activatebatch.add(abc.Id);
               }                
               
                 calllistedit1 batch = new calllistedit1(activatebatch);
                 Id batchId = Database.executeBatch(batch);
                 If(cls[0].CampaignName__c == null)      {    
                  calllistupdate batch1 = new calllistupdate(activatebatch,userIds,mapOfUsertoassignedDate );
                  Id batchId1 = Database.executeBatch(batch1);
                 }
                 If(cls[0].CampaignName__c != null){ 
                  calllistupdatefromcm batch2 = new calllistupdatefromcm(activatebatch, userIds, mapOfUsertoassignedDate );
                  Id batchId2 = Database.executeBatch(batch2);
                 }
               
        system.debug('***************call'+filter1.CampaignName__c);
                   
    }
    public PageReference cancel()
    {
          PageReference cancelref = new PageReference('/apex/calllistpage');
          cancelref.setRedirect(true);
          return cancelref;
    }
    public PageReference GeneralLock(){
    
    
     string Viewname = ( String )JSON.deserialize( Apexpages.currentPage().getParameters().get('editCallListNames'), string.class ) ; 
     list<Automated_Call_List__c>  lock = new list<Automated_Call_List__c>();
        lock = [Select Id, Name, TempLock__c from Automated_Call_List__c where Name= :ViewName];
       System.debug('****' + lock + lock[0].TempLock__c);
        
       
      return null;
    }*/

}