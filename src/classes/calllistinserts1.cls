global class calllistinserts1 implements Database.Batchable<SObject>{

  
/*global  String Query;  
global CallList__c call;
global Automated_call_List__c LockedCL ;
global  integer size ;
global  string size1; 
global List<calllist__c> callistname ;
global List<contact_call_list__c> callistname1  ;
global Map<Integer, String> filterMap = new Map<Integer, String>();
global calllistinserts1 (Set<Id> clIds)  
{  
  system.debug('Welcome to Batch apex method...');
 callistname = [Select Id, Name From calllist__c Where Id in: clIds];
 call=[select id,Name,CampaignName__c, CampaignName__r.id,Custom_Logic__c,Contact_Type__c,Field_API__c, Field_API1__c, Field_API2__c,Field_API3__c,Field_API4__c,Operator__c,Operator1__c,Operator2__c,Operator3__c,Operator4__c,Value__c,Value1__c,Value2__c,Value3__c,Value4__c from  CallList__c  where id=:callistname[0].id ];

LockedCL = [Select Id, Name From Automated_call_List__c Where Name =: callistname[0].Name];
 
 system.debug('Call List Name'+callistname);
} */
global Database.QueryLocator start(Database.BatchableContext BC)  
{  
   /*try
    { filterMap.clear();
      String condn1=null;
      String condn2=null;
      String condn3=null;
      String condn4=null;
      String condn5=null;
      string cond1;
      string conditions =null;
      list<Contact>conlist= new list<Contact>();  
      list<Contact_Call_List__c >contactcalllist=new list<Contact_Call_List__c >();  
      String conquery = 'select Id,Name from Contact';
      System.debug('*******:QUERY:********\n'+conquery);
      GerateDynamicQuery generateQuery =  new GerateDynamicQuery();
      if(call.Field_API__c!=null && call.Field_API__c!='' && call.Operator__c!='' && call.Operator__c!=null)
      condn1= generateQuery.conditionquery(call.Field_API__c,call.Operator__c,call.Value__c);
      if(call.Field_API1__c!=null && call.Field_API1__c!='' && call.Operator1__c!='' && call.Operator1__c!=null)
      condn2= generateQuery.conditionquery(call.Field_API1__c,call.Operator1__c,call.Value1__c);
      if(call.Field_API2__c!=null && call.Field_API2__c!='' && call.Operator2__c!='' && call.Operator2__c!=null)
      condn3= generateQuery.conditionquery(call.Field_API2__c,call.Operator2__c,call.Value2__c);
      if(call.Field_API3__c!=null && call.Field_API3__c!='' && call.Operator3__c!='' && call.Operator3__c!=null)
        condn4= generateQuery.conditionquery(call.Field_API3__c,call.Operator3__c,call.Value3__c);
      if(call.Field_API4__c!=null && call.Field_API4__c!='' && call.Operator4__c!='' && call.Operator4__c!=null){
           condn5= generateQuery.conditionquery(call.Field_API4__c,call.Operator4__c,call.Value4__c);
           
      }
       
    
      If(condn1 != null  && call.Field_API__c != null)
          filterMap.put(1,condn1);
      
        if( condn2 != null && call.Field_API1__c != null)
            filterMap.put(2,condn2);
          
        if( condn3 != null && call.Field_API2__c != null)
            filterMap.put(3,condn3);
         
        if(condn4 != null && call.Field_API3__c != null)
            filterMap.put(4,condn4);
        if(condn5 != null && call.Field_API4__c != null)
            filterMap.put(5,condn5);
        if(String.isNotBlank(call.Custom_Logic__c)){
            conditions = call.Custom_Logic__c;
            for(Integer i = 1; i <= filterMap.size(); i++){
                if(conditions.contains(String.valueOf(i))){
                    conditions = conditions.replace(String.valueOf(i),filterMap.get(i));
                }
            }
            conquery += ' WHERE (' + conditions + ') ';
        }else{
            If(condn1 != null  && call.Field_API__c != null)  
                conquery=conquery+' where '+condn1;
            if( condn2 != null && call.Field_API1__c != null)
                conquery=conquery+' AND '+ condn2;
            if( condn3 != null && call.Field_API2__c != null)
                conquery=conquery+' AND '+condn3;
            if(condn4 != null && call.Field_API3__c != null)
                conquery=conquery+' AND '+condn4;
            if(condn5 != null && call.Field_API4__c != null)
                conquery=conquery+' AND '+condn5; 
        }
        if(call.Contact_Type__c == 'My Contacts'){
            if( condn1 != null  && call.Field_API__c != null)
                  conquery= conquery+' AND '+ 'Ownerid = '+'\''+Userinfo.getUserId()+'\'';
            else
                  conquery= conquery+' where '+ 'Ownerid = '+'\''+Userinfo.getUserId()+'\'';       
        }
        else if(call.CampaignName__c != null){  
            List<CampaignMember>  cmlist= new List<CampaignMember>();      
            
            String camptextid = call.CampaignName__c;
            String campsubid = camptextid.Substring(0,15);
            
            cmlist=[select id,Campaignid,Contactid from CampaignMember where Campaignid =: call.CampaignName__c];
            
            System.debug('&&&&&5' + campsubid);
            system.debug('+++++cmlist'+cmlist.size());
            List<String> conids= new List<String>();
            String str = '';
            String glue = '(';
            for(integer i=0;i<cmlist.size();i++){
                  conids.add('\''+cmlist[i].ContactId+'\'');
                  str = str + glue + '\''+cmlist[i].ContactId+'\'';
                  glue = ',';
            }
            if(cmlist.size()>0){
                str += ')';
                if(condn1 != null  && call.Field_API__c != null)
                     conquery= conquery+' AND '+ 'id in '+ str;
                    
                   else
                     conquery= conquery+' where '+ 'id in'+ str;
                }
                System.debug(',,,,,,,,,,campaign query'+conquery);      
            }         
            conquery = conquery ;
            
            Query= conquery;
                    
                 
System.debug('*******:QUERY:********\n'+Query);
  system.debug('This is start method');
  system.debug('Myquery is......'+Query);
return Database.getQueryLocator(query); 
return null;
}
catch(Exception e)
{
 query='Select Id from Contact limit 0'; 
 Delete lockedCL;
 Delete callistname;
 system.debug('Check the Exception.....'+e);
 Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
 String[] toAddresses = new String[] {'ntambe@colecapital.com','ksingh@colecapital.com'};
 String[] bccAddresses = new String[] {'priya.skrish@gmail.com','skalamkar@colecapital.com'};
 mail.setToAddresses(toAddresses);
 mail.setBccSender(true);
 mail.setBccAddresses(bccAddresses);
 mail.setSubject('Exception Message');
 mail.setPlainTextBody
 ('CallList ' + call.Name + ' ' +
  'has not been created because of .' + e.getMessage());
 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
 
  return Database.getQueryLocator(query);
  return null;
  
}*/

return null;
 
}  

global void execute(Database.BatchableContext BC,List<Contact> scope)  
{  
/* system.debug('Check the name of the call list name:'+call);
List<contact_call_list__c> callist = new List<contact_call_list__c>();
If(scope.size()>0)
{
for(Contact c : scope) {
contact_call_list__c cls = new contact_call_list__c();
cls.CallList_Name__c = call.name ;
cls.Contact__c = c.Id;
cls.campId__c = call.CampaignName__c;
callist.add(cls)

}
system.debug('Final String value of calllist size\n'+size1);
try {

           insert callist;
        } catch (system.dmlexception e) {
            System.debug('ccl not inserted: ' + e);
        }
        
        

}*/
}

global void finish(Database.BatchableContext BC)  
{ 
/*AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
 
 string s = size1;
 
  System.debug('Final String value of calllist s\n'+s);
  System.debug('Final String call.Name'+call.Name);
  
  Automated_Call_List__c unlockcl = [Select Id, Name, TempLock__c  from Automated_Call_List__c where Name = :call.Name];
  unlockcl.TempLock__c  = null;
  update unlockcl;
  
List<Contact_call_list__c> checkifinsert = [Select Id from Contact_Call_list__c where calllistorder__c = :unlockcl.Id limit 1];
If(checkifinsert.size()>0)
{
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
String[] toAddresses = new String[] {a.CreatedBy.Email};
String[] bccAddresses = new String[] {'nvanamala@colecapital.com'};
mail.setToAddresses(toAddresses);
mail.setBccSender(true);
mail.setBccAddresses(bccAddresses);
mail.setSubject('Call List generate process ' + a.Status);
mail.setPlainTextBody
('CallList ' + call.Name + ' ' +
'has been created. ');
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}*/
}
}