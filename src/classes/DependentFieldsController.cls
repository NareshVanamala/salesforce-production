public with sharing virtual class DependentFieldsController {
    public class Result{
        public String territory;
        public List<Dependent> dependents;

        public Result(String territory){
            this.territory=territory;
            dependents= new List<Dependent>();
        }
    }
    public class Dependent{
        public String type;
        public String label;
        public List<String> values;

        public Dependent(String type, String label, List<String> values){
            this.type= type;
            this.label= label;
            this.values= values;
        }
    }
    
    public class DependentRequest {
        public String objectName;
        public String keyField;
    }

    public static String GetDependentOptions(String pObjName, String pControllingFieldName){
        Schema.DescribeSObjectResult objectMeta = Schema.describeSObjects(new String[]{pObjName})[0];
        Map<String, Schema.SObjectField> objFieldMap = objectMeta.fields.getMap();
        
        List<Map<String,List<String>>> returnObj= new List<Map<String,List<String>>>();
        Boolean parentParsed=false;
        Map<String, Result> results= new Map<String, Result>();

        Map<String, String> dependentFieldMap= new Map<String, String>();

        for(String field: objFieldMap.keySet()){
            Schema.DescribeFieldResult fieldDescribe= objFieldMap.get(field).getDescribe();
            if(fieldDescribe.getController()!= null && fieldDescribe.getController().getDescribe().getName().toLowerCase().trim()==pControllingFieldName.toLowerCase().trim()){
                dependentFieldMap.put(fieldDescribe.getName(), fieldDescribe.getLabel());
            }
        }

        for(String depField: dependentFieldMap.keySet()){
            Map<String,List<String>> fieldMap= GetDependentOptions(pObjName, pControllingFieldName, depField, objFieldMap);
            if(!parentParsed){
                for(String territory: fieldMap.keySet()){
                    if(territory==null || territory.trim()=='')
                        continue;
                    Result result= new Result(territory);
                    Dependent d= new Dependent(depField, dependentFieldMap.get(depField), fieldMap.get(territory));
                    result.dependents.add(d);
                    results.put(territory, result);
                }
                parentParsed=true;
            }else{
                for(String territory: fieldMap.keySet()){
                    if(territory!=null && territory.trim()!='' && results.get(territory)!=null){
                        Dependent d= new Dependent(depField, dependentFieldMap.get(depField), fieldMap.get(territory));
                        results.get(territory).dependents.add(d);
                    }
                }
            }


            //returnObj.add(fieldMap);
        }
            system.debug(logginglevel.info, JSON.serialize(results.values()));
        
        return JSON.serialize(results.values());
        //return returnObj;
    }
    public static Map<String,List<String>> GetDependentOptions(String pObjName, String pControllingFieldName, String pDependentFieldName, Map<String, Schema.SObjectField> objFieldMap){
        Map<String,List<String>> objResults = new Map<String,List<String>>();
        //get the string to sobject global map
        /*Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
        if (!objGlobalMap.containsKey(pObjName))
            return objResults;
        *///get the type being dealt with
        //Schema.SObjectType pType = objGlobalMap.get(pObjName);
        //Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();
        //verify field names
        if (!objFieldMap.containsKey(pControllingFieldName) || !objFieldMap.containsKey(pDependentFieldName))
            return objResults;     
        //get the control values   
        List<Schema.PicklistEntry> ctrl_ple = objFieldMap.get(pControllingFieldName).getDescribe().getPicklistValues();
        //get the dependent values
        List<Schema.PicklistEntry> dep_ple = objFieldMap.get(pDependentFieldName).getDescribe().getPicklistValues();
        //iterate through the values and get the ones valid for the controlling field name
        Bitset objBitSet = new Bitset();
        //set up the results
        for(Integer pControllingIndex=0; pControllingIndex<ctrl_ple.size(); pControllingIndex++){           
            //get the pointer to the entry
            Schema.PicklistEntry ctrl_entry = ctrl_ple[pControllingIndex];
            //get the label
            String pControllingLabel = ctrl_entry.getLabel();
            //create the entry with the label
            objResults.put(pControllingLabel,new List<String>());
        }
        //cater for null and empty
         objResults.put('',new List<String>());
         objResults.put(null,new List<String>());
        //check the dependent values
        for(Integer pDependentIndex=0; pDependentIndex<dep_ple.size(); pDependentIndex++){          
            //get the pointer to the dependent index
            Schema.PicklistEntry dep_entry = dep_ple[pDependentIndex];
            //get the valid for
            String pEntryStructure = JSON.serialize(dep_entry);                
            TPicklistEntry objDepPLE = (TPicklistEntry)JSON.deserialize(pEntryStructure, TPicklistEntry.class);
            //if valid for is empty, skip
            if (objDepPLE.validFor==null || objDepPLE.validFor==''){
                continue;
            }
            //iterate through the controlling values
            for(Integer pControllingIndex=0; pControllingIndex<ctrl_ple.size(); pControllingIndex++){    
                if (objBitSet.testBit(objDepPLE.validFor,pControllingIndex)){                   
                    //get the label
                    String pControllingLabel = ctrl_ple[pControllingIndex].getLabel();
                    objResults.get(pControllingLabel).add(objDepPLE.label);
                }
            }
        } 
        //system.debug(logginglevel.info, objResults);
        return objResults;
    }
}