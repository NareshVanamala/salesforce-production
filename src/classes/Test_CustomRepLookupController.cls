@isTest
Public class Test_CustomRepLookupController
{
    static testmethod void testunit1()
    {
     
                Account acc = new Account();
                acc.Name = 'Test Prospect Account';      
                Insert acc;
        
                Contact c = new Contact();
                c.firstname='TFirst1/TFirst2';
                c.lastname='TLast1/TLast2';
                c.accountid=acc.id;
                c.MailingCity='testcity';
                c.MailingState='teststate';
                c.email='test1@mail.com';
                c.Contact_Type__c = 'Registered Rep';
                insert c;
                system.assertequals(c.accountid,acc.id);

                Account acc1 = new Account();
                acc1.Name = 'Test Prospect Account';      
                Insert acc1;
        
                Contact c1 = new Contact();
                c1.firstname='TFirst1/TFirst2';
                c1.lastname='TLast1/TLast2';
                c1.accountid=acc1.id;
                c1.MailingCity='testcity';
                c1.MailingState='teststate';
                c1.email='test@mail.com';
                c1.Contact_Type__c = 'Registered Rep';
                c1.Rep_CRD__c ='repCRD';
                insert c1;
                
                        system.assertequals(c1.accountid,acc1.id);

                REIT_Investment__c newreit = new REIT_Investment__c (Investor_Name__c = 'Test_Investor',
                Account__c= '1150221562',Rep_Contact__c=c.id,
                Rep_ID__c ='BE111',Fund__c='3772'); 
                insert newreit;
                 newreit=[select id,name from REIT_Investment__c where id =:newreit.id];
                
                
                List<SplitTickets__c> lstSplTkt = new  List<SplitTickets__c>();
                SplitTickets__c  spt = new SplitTickets__c(REIT_Investment__c= newreit.id,Rep__c=c.id );
                lstSplTkt.add(spt);
                
                 SplitTickets__c  spt1 = new SplitTickets__c(REIT_Investment__c= newreit.id,Rep__c=c.id );
                 lstSplTkt.add(spt1);
                 
                 
                
               
               ApexPages.currentPage().getParameters().put('lksearch',newreit.name);
                ApexPages.currentPage().getParameters().put('frm','Page:myForm');
                ApexPages.currentPage().getParameters().put('txt','test split');
                
                ApexPages.StandardController stdcon = new ApexPages.StandardController(newreit);      
                CustomRepLookupController replcon= new CustomRepLookupController(stdcon);
                
                test.starttest();
                replcon.getTextBox();
                replcon.getFormTag();
                replcon.searchString='TFirst1 TLast1';
                replcon.search();
                replcon.saveSplitContact();
                replcon.CancelSplitContact(); 
                test.stoptest();
    } 
   
  }