global virtual with sharing class ocms_Vkit_ColeCapitalContact extends cms.ContentTemplateController
{
    @TestVisible private List<Contact> ProductList;
    @TestVisible private String html;  
     
    public List<SObject> getProductList()
    {
        List<SObject> ProductList;
        User u=[select id, firstname, lastname,contactid from user where id=:UserInfo.getUserId()];
        string cid=u.contactid ;
        Contact C=[select id,Image_URL__c,Internal_Wholesaler1__c,Sales_Director__c,Regional_Territory_Manager1__c,firstName, lastName,Internal_Wholesaler__c,Wholesaler__c,Regional_Territory_Manager__c from Contact where id=:cid];
        string InternalName=C.Internal_Wholesaler1__c;
        string SalesDirector=C.Sales_Director__c;
        string RegionalTerritoryOfficer=C.Regional_Territory_Manager1__c;       
        String query1 = 'select id,name,Home_Phone__c,Phone,email,Title,Image_URL__c from User where id=:SalesDirector ';
        String query2 = 'select id,name,Home_Phone__c,Phone,email,Title,Image_URL__c from User  where id=:InternalName ';
        String query3 = 'select id,name,Home_Phone__c,Phone,email,Title,Image_URL__c from User where id=:RegionalTerritoryOfficer';
        List<SObject> con = new List<SObject>();
        if(Database.countquery('select count()from User where id=:SalesDirector')>0)
        {
        con.add(Database.query(query1));
        
        }
        if(Database.countquery('select count()from User where id=:InternalName')>0)
        {
        con.add(Database.query(query2));
        }
        if(Database.countquery('select count()from User where id=:RegionalTerritoryOfficer')>0)
        {
        con.add(Database.query(query3));
        }
        system.debug('outer'+con);
        ProductList=con;
        return ProductList;
       
    }
       
   
    @TestVisible private String writeControls()
    {
        String html = '';
        return html;
     }
     @TestVisible private String writeListView()
     {
        String html = '';
        User u=[select id, firstname, lastname,contactid from user where id=:UserInfo.getUserId()];
        string cid=u.contactid ;
        Contact C=[select id,Image_URL__c,firstName, lastName,Internal_Wholesaler__c,Wholesaler__c,Regional_Territory_Manager__c from Contact where id=:cid];
        string INName=C.Internal_Wholesaler__c;
        String SDName=C.Wholesaler__c;
        String RTName=C.Regional_Territory_Manager__c;
        string conid=c.id;
        
        html += '<input type="hidden" id="INname" value="' + INName + '" />';
        html += '<input type="hidden" id="SDname" value="' + SDName + '" />';
        html += '<input type="hidden" id="RTname" value="' + RTName + '" />';
        html += '<input type="hidden" id="contactid" value="' + c.id + '" />';
              
       html += '<div style="text-align:center;"><h2>Connect With Your Cole Capital Contacts</h2></div><div class="clear"></div><br/>';        
         if(ProductList.size()>0)
       {
       
        for(SObject pl : ProductList)
        {
        string formatedPhoneNumber;
            if(pl.get('Image_URL__c')==null)
            {
            html +='<div class ="wholesaler" style="float:left;margin-left: 12.8%;width: 18.3%;min-height: 250px;"><img class ="wholesalerContImg" src="/servlet/servlet.FileDownload?file=00P5000000KSr08EAD"/>';            }
            else
            {
            html +='<div class ="wholesaler" style="float:left;margin-left: 12.8%;width: 18.3%;"><img class ="wholesalerContImg" src="'+ pl.get('Image_URL__c') +'"/>';
            }
            
            html +='<p><span style="font-size:16px;font-family:georgia;color:#444;font-weight:bold;">'+pl.get('name')+'</span>';
            
            if(pl.get('Title')!=null)
            {
            html +='<br/>'+pl.get('Title')+'';
            }
            
            if(pl.get('Home_Phone__c')!=null)
            {
            html +='<br/>'+pl.get('Home_Phone__c')+'';
            }
            if(pl.get('Phone')!=null)
            {
            if(pl.get('Phone') !=null){
                string nondigits = '[^0-9]';
                 
                String phonenumber = String.valueOf(pl.get('Phone'));
                String PhoneDigits = phonenumber.replaceAll(nondigits,'');
                //if (phonenumber.length() == 10) 
                formatedPhoneNumber = PhoneDigits.substring(0,3)+ '.' +
                       PhoneDigits.substring(3,6)+ '.' +
                       PhoneDigits.substring(6,10);
                       }  
            html +='<br/>'+formatedPhoneNumber+'';
            }
           // html +='<br/><strong><a target="_blank" style="color:#336699;" href="#">Email Me »</a></strong><br/><strong></strong></p>';
           html +='<br/><strong><a style="color:#336699;" href="mailto:' + pl.get('email') + '?subject=%20ColeCapital:%20Question%20Submitted%20&body=%20Hi%20' + pl.get('name') + ',%0A%0D%0A">Email Me »</a></strong><br/><strong></strong></p>';
            html +='</div>';
         
       }   
        }
     html +='<div class="clear"></div>';
       
        
     
            
          return html;              
     }             
       
       
        global override virtual String getHTML()
        {
            String html = '';
            
            html += writeControls();
            ProductList= getProductList();
            html += writeListView();
            html += '<script src="resource/ocms_colecapitalcontact" type="text/javascript"></script>';
            return html;
        }

}