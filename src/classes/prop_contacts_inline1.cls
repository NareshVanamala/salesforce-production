public with sharing class prop_contacts_inline1 extends PC_PropContactsHelperClass{

    public ApexPages.StandardController controller {get; set;}
    public MRI_PROPERTY__c mriprop{get;set;}
    public List<Property_Contact_Leases__c> propcontacts{get;set;}
    public set<id> lsidset=new set<id>();
    public set<id> pcidset=new set<id>();
    public String id {get;set;}
    public boolean ShowJustContactType{get;set;}
    public String ids {get;set;}
    public String MRIId  {get;set;}
    public List<String> strs = new List<String>();
    public List<String> selectedleaseids = new List<String>();
    public List<Property_Contacts__c> results{get;set;}
    public string Searchtext{get;set;}  
    public List<cContact>PropertyContactList {get; set;}
    public List<cContact>selectedPropertycontactList {get; set;}
    public list<Property_Contacts__c>PContactlist{get; set;}
    public String leaseOppid{get; set;}
    public string SearchBy {get; set;}
    public boolean showsearch{get; set;}
    public boolean shownew{get; set;}
    public boolean showContactType{get; set;}
    public Property_Contacts__c vdr{get; set;}
    public Property_Contact_Leases__c pcl{get; set;}
    public String soql;
    public string MRIPropid{get; set;}
    public Lease__c leaserec{get; set;} 
    public String showNewButton { get; set; }
    public String message{ get; set; }
    public List<Property_Contact_Leases__c> Selectedcons{ get; set; }
    public List<mywrapper> mylist {get; set;}
    public List<Property_Contact_Leases__c> lidlist {get; set;} 
    public set<ID> idset {get; set;}
    public String collegeString{get;set;}
    
    public List<cLease>leaseList {get; set;}
    public MRI_PROPERTY__c mripid;
    
    public set<ID> mriset{get; set;}
    public Property_Contact_Leases__c prconl{get; set;}
    //public boolean showexist{get;set;}
    
    public List<massapply>massapplylist {get; set;}
    public boolean showmain{get;set;}
    //public boolean showexist2{get;set;}
    public String mystring{get;set;}
    
    public string contactId;
    public List<Property_Contact_Leases__c> pclist{get;set;}
    public integer clspop{get; set;}
    public string showmycontype{get; set;} 
    public Boolean refreshPage { get; set; }
    public String LeaseId;
    public String PCLId;
    
    public prop_contacts_inline1(ApexPages.StandardController controller){
        ShowJustContactType = false;
        LeaseId = apexpages.currentpage().getparameters().get('LeaseId');
        PCLId = apexpages.currentpage().getparameters().get('PCLId');
        prconl=new Property_Contact_Leases__c(); 
        if(LeaseId != null && LeaseId != '' && PCLId != '' && PCLId != null){
           ShowJustContactType = true;
           showContactType = true;
           system.debug('showContactType'+showContactType);
           Property_Contact_Leases__c PCLObj = [select id,Contact_Type__c from Property_Contact_Leases__c where id=:PCLId limit 1];
           system.debug('PCLObj'+PCLObj);
           prconl.Contact_Type__c = PCLObj.Contact_Type__c;
        }
        refreshPage = false;
        this.controller = controller;
        showmycontype='';
        this.mriprop = (MRI_PROPERTY__c)controller.getRecord();
        mripid=mriprop;
        string MRIId=mriprop.id;
        clspop=0;
        system.debug('<<<<<give me id>>>>'+MRIId+mriprop );
        for(Lease__c ls:[select id,MRI_PROPERTY__c,Lease_Status__c from Lease__c where MRI_PROPERTY__c=:mriprop.id and Lease_Status__c='Active'])
      

            lsidset.add(ls.id);
                        
    
        results=new list<Property_Contacts__c>();
        selectedPropertycontactList =new list<cContact>();
        showsearch=true;  
        shownew= false;
        showContactType = false;
        vdr= new Property_Contacts__c();
        pcl= new Property_Contact_Leases__c(); 
        runSearch(); 
        message = System.currentPagereference().getParameters().get('msg');
        
        mylist=getmylist();
        idset=new set<ID>();
        
        mriset=new set<ID>();
      
        
        //showexist=false;
        showmain=true;
        
        mystring='';
        
        contactId='';
        pclist=new List<Property_Contact_Leases__c>();
    }
    


    public class mywrapper {
        public Property_Contact_Leases__c pcls {get; set;}
        public boolean selected {get; set;}

        public mywrapper(Property_Contact_Leases__c pcls) {
            this.pcls = pcls;
            selected = false; //If you want all checkboxes initially selected, set this to true
        }
    }
    

    public List<mywrapper> getmylist() {
        if (mylist == null) {
            mylist = new List<mywrapper>();
            if(!lsidset.isEmpty()) {
                for (Property_Contact_Leases__c foo : [select id,Property_Contacts__r.id,Property_Contacts__r.MRI_Property__c ,Property_Contacts__r.Name,Property_Contacts__r.Company_Name__c,
                                Property_Contacts__r.Email_Address__c,Property_Contacts__r.Phone__c,Property_Contacts__r.Address__c,
                                Property_Contacts__r.City__c,Property_Contacts__r.State__c,Property_Contacts__r.Zipcode__c,Contact_Type__c,Lease__r.Tenant_Name__c,
                                Property_Contacts__r.Notes__c from Property_Contact_Leases__c where Lease__c IN:lsidset]) {
                    mylist.add(new mywrapper(foo));
                }
            }
        }
        return mylist;
    }
    public void processSelectedcons() {
        system.debug('$$$$$$$$$');
        Selectedcons = new List<Property_Contact_Leases__c>();
        for (mywrapper mywrp : mylist) {
            if (mywrp.selected = true) {
                Selectedcons.add(mywrp.pcls); // This adds the wrapper wFoo's real Foo__c
            }
        }
        system.debug('$$$$$$$$$'+Selectedcons);
        for(Property_Contact_Leases__c conid:Selectedcons)
            idset.add(conid.id);
        system.debug('$$$$$$$$$'+idset);
        collegeString = '';
        for(String s:idset) {
           collegeString += (collegeString==''?'':',')+s;
        }
        system.debug('$$$$$$$$$'+collegeString);
        showPopup();
    }
    
    public boolean displayPopup {get; set;}     
    
    public void closePopup() {        
        displayPopup = false;    
    }     
    public void showPopup() {        
        displayPopup = true;    
    }
    
    public ID selectedVersion {get; set;}
    
    public PageReference massapply() {
        system.debug('$$$$$$$$$');
        processSelectedcons();
        PageReference pgref=new PageReference('/apex/mass_apply');
        pgref.setRedirect(true);
        return pgref;
    }
    public PageReference enable() {
        system.debug('$$$$$$$$$$$$');
        selectedVersion = System.currentPageReference().getParameters().get('checked');
        system.debug('$$$$$$$$$$$$'+selectedVersion);
        for(cContact v : PropertyContactList){
            if(v.con.Id == selectedVersion){
                v.selected = true;
            }
            else{
                v.selected =false; 
            }
        }
        return null;
    }
    public PageReference Save1()
    {     
        mystring=ApexPages.currentPage().getParameters().get('cid');
        system.debug('$$$'+mystring);
        shownew=false; 
        showContactType= True;
        system.debug('$$$$'+shownew);
        leaseList=getleaseList();
        return null;
    }
    public PageReference Save(){ 
        String duplicateContactTypeMsg='';
        LeaseId = apexpages.currentpage().getparameters().get('LeaseId');
        String PropertContactId = apexpages.currentpage().getparameters().get('PropConId');
        PCLId = apexpages.currentpage().getparameters().get('PCLId');
        system.debug('outer prconl.Contact_Type__c'+prconl.Contact_Type__c);
        if(prconl.Contact_Type__c == null || prconl.Contact_Type__c == '')
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select contact type'));
        else if(LeaseId != null && LeaseId != '' && prconl.Contact_Type__c != null && prconl.Contact_Type__c != '' && PropertContactId != null && PropertContactId != '' && PCLId != '' && PCLId != null){
           system.debug('LeaseID123'+LeaseId);
           PageReference pgref;
           list<Lease__c> selLeaseList = [select id,Tenant_Name__c,Name,MRI_PROPERTY__c,Lease_Status__c from Lease__c where id =:LeaseID and Lease_Status__c='Active'];
           system.debug('selLeaseList1234'+selLeaseList);
           system.debug('prconl.Contact_Type__c'+prconl.Contact_Type__c);
           duplicateContactTypeMsg = insertPropConLeases(selLeaseList,PropertContactId,prconl.Contact_Type__c,false,PCLId);
           if(duplicateContactTypeMsg == null || duplicateContactTypeMsg == ''){
              refreshPage = true;
              clspop=1; 
              pgref=new PageReference('/apex/PC_Redirectpage?id='+clspop);   
              pgref.setRedirect(true);
              system.debug('pgref123'+pgref);             
              return pgref;
           }
           else{
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,duplicateContactTypeMsg));
              return null;
           }
        }   
        else{
           string propConId='';
           system.debug('entered save leaseList'+leaseList);
           list<Lease__c> selLeaseList = new list<Lease__c>();
           PageReference pgref;
           system.debug('Save leaseList'+leaseList);
           for(cLease llist:leaseList){
               if(llist.selected==true){
                  selLeaseList.add(llist.leas);
               }
           } 
        if(selLeaseList.size()==0)
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a lease'));
        else{
            Property_Contacts__c pc=new Property_Contacts__c();
            if(vdr.name!= null && vdr.name!=''){
               system.debug('vdr.name'+vdr.name);
               pc.Address__c=vdr.Address__c;
               pc.Company_Name__c = vdr.Company_Name__c ;
               pc.Department__c = vdr.Department__c ;
               pc.Notes__c = vdr.Notes__c ;
               pc.Email_Address__c = vdr.Email_Address__c ;
               pc.Phone__c = vdr.Phone__c ;
               pc.City__c = vdr.City__c ;
               pc.Address_Line_2__c = vdr.Address_Line_2__c;
               pc.Cell_Phone__c = vdr.Cell_Phone__c;
               pc.Fax_Number__c= vdr.Fax_Number__c;
               pc.Title__c = vdr.Title__c;
               pc.State__c = vdr.State__c ;
               pc.Zipcode__c = vdr.Zipcode__c ;
               pc.name=vdr.name;
               if(Schema.sObjectType.Property_Contacts__c.isCreateable())
                  insert pc;
               propConId=pc.id;
            }      
            else{
                propConId = selectedPropertycontactList[0].con.id;
                system.debug('selectedPropertycontactList[0].con.id'+selectedPropertycontactList[0].con.id); 
            }
        }
        system.debug('selLeaseList123'+selLeaseList);
        duplicateContactTypeMsg = insertPropConLeases(selLeaseList,propConId,prconl.Contact_Type__c,false,null);
        system.debug('duplicateContactTypeMsg12133'+duplicateContactTypeMsg);
        if(duplicateContactTypeMsg == null || duplicateContactTypeMsg == ''){
           refreshPage = true;
           clspop=1; 
           pgref=new PageReference('/apex/PC_Redirectpage?id='+clspop);   
           pgref.setRedirect(true); 
           system.debug('pgref1234'+pgref);
           return pgref;
        }
        else{
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,duplicateContactTypeMsg));
           return null;
        }
     }
       return null;
   }
  

    public PageReference createNewRecord(){
        String mid=ApexPages.currentPage().getParameters().get('cid');
        system.debug('$$$'+mid);
        shownew=true; 
        showContactType= True;
        showsearch=false; 
        showmain=false;
        return null;
    }
    
    @testvisible private void runSearch(){ 
        results = performSearch(Searchtext);
        PropertyContactList = new List<cContact>();
        for(Property_Contacts__c c:results){
            PropertyContactList.add(new cContact(c));
        }
    }  
    private List<Property_Contacts__c> performSearch(string Searchtext){
        soql = 'select id,Name,Company_Name__c,Email_Address__c, Phone__c,MRI_Property__c from Property_Contacts__c';
        system.debug('$$$$$$'+soql);
        system.debug('$$$$$$'+SearchBy);        
        if((Searchtext== '')||(Searchtext== null)) { 
            soql=soql+ ' order by name ASC' ;
            soql = soql + ' limit 200';
            system.debug('$$$$$$'+soql);
        }      
        if(Searchtext!= '' && Searchtext!= null && SearchBy =='Name') { 
            soql=soql+ ' where name=:Searchtext' ;
            soql = soql + ' limit 200';
            system.debug('$$$$$$'+soql);
        }
        else if(Searchtext!= '' && Searchtext!= null && SearchBy =='All fields' ){
            soql = soql +  ' where ((name LIKE \'%' + String.escapeSingleQuotes(Searchtext)+'%\')'+'OR'+'(Address__c LIKE \'%' + String.escapeSingleQuotes(Searchtext)+'%\'))';
            system.debug('$$$$$$'+soql);
        }
        if(Searchtext!= '' && Searchtext!= null && SearchBy==null){
            soql = soql +  ' where ((name LIKE \'%' + String.escapeSingleQuotes(Searchtext)+'%\')'+'OR'+'(Address__c LIKE \'%' + String.escapeSingleQuotes(Searchtext)+'%\'))';
            system.debug('$$$$$$'+soql);
        }
       return database.query(soql); 
    }
    public List<SelectOption> getSnoozeradio(){
        List<SelectOption> snoozeoptions = new List<SelectOption>(); 
        snoozeoptions.add(new SelectOption('Name','Name')); 
        snoozeoptions.add(new SelectOption('All fields','All fields')); 
        return snoozeoptions ;
    }
     
    public List<cContact> getContacts() {
        if(PropertyContactList == null){
            PropertyContactList = new List<cContact>();
            for(Property_Contacts__c c:results) {
               PropertyContactList.add(new cContact(c));
            }
        }
        return PropertyContactList ;
    }
    public void SearchRecord(){
        showsearch=true;  
        shownew=false;  
        showContactType= False;
        //showexist=false;
        runSearch();  
    }
    

    public PageReference ApplyPropertyContacts() {

   
       
        if(PropertyContactList.size()>0)
        {
          
              for(cContact ct:PropertyContactList)
              {
                 if(ct.selected==true)
                  selectedPropertycontactList.add(ct);
                  mriset.add(ct.con.MRI_Property__c);
              }
        }   
        system.debug('$$$'+mriset);
        
        if(selectedPropertycontactList.isEmpty() &&selectedPropertycontactList.size()==0 ){
            shownew=false; 
            showsearch=true; 
            showContactType = True;
            //showexist=false;            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a Contact'));
            return null;                        
        }
        shownew=false; 
        showsearch=false; 
        showContactType =True;
        
        //showexist=true;
        system.debug('selectedPropertycontactList'+selectedPropertycontactList);
        leaseList=getleaseList();
         
       return null;    
    }  
        public PageReference getSelected() {
            contactId = ApexPages.currentPage().getParameters().get('selconid');
            system.debug('$$$$$'+contactId);
            return null;
        }
    
    public Boolean getshownewcon() {
        boolean b;
        if(PropertyContactList.size()>0){
            for(cContact Ver : PropertyContactList){
                if(Ver.selected == true){
                    b= true;
                }
                else{
                    b= false;
                }
            }
        }
        return b;
    }

    public class cContact{
        public Property_Contacts__c con {get; set;}
        public Boolean selected {get; set;}

        public cContact(Property_Contacts__c c)
        {
            con = c;
            selected = false;
       }
    }
    public class cLease{
        public Lease__c leas {get; set;}
        public Boolean selected {get; set;}

        public cLease(Lease__c c)
        {
            leas = c;
            selected = false;
       }
    }
    public List<cLease> getleaseList() {
    
        system.debug('Welcome to getlease method');
        system.debug('$$$');
        system.debug('$$$'+mriset);     
        system.debug('<<<<<give me id>>>>'+MRIId+mriprop );
        MRI_PROPERTY__c mripid2=(MRI_PROPERTY__c )controller.getRecord();
        system.debug('$$$'+mripid2);
        String mid=ApexPages.currentPage().getParameters().get('cid');
        system.debug('$$$'+mid);        
        if (leaseList == null) {
            leaseList = new List<cLease>();
            for (Lease__c foo : [select id,Tenant_Name__c,Name,MRI_PROPERTY__c,Lease_Status__c from Lease__c where MRI_PROPERTY__c =:mid and Lease_Status__c='Active']) {
                leaseList.add(new cLease(foo));
            }
        }
        system.debug('$$$'+leaseList);
        return leaseList;
    }   
    public class massapply{
        public Lease__c leas {get; set;}
        public Boolean selected {get; set;}

        public massapply(Lease__c c)
        {
            leas = c;
            selected = false;
       }
    }
    public List<massapply> getmassapplylist() {
        if (massapplylist == null) {
            massapplylist = new List<massapply>();
 
            for (Lease__c foo : [select Tenant_Name__c,Name,Property_ID__c,MRI_PROPERTY__c,Lease_Status__c from Lease__c 
                                LIMIT 3])                               
                massapplylist.add(new massapply(foo));
            
        }
        system.debug('$$$'+massapplylist);
        return massapplylist;
    } 


  
    }