@isTest (SeeAllData=true)
private class test_ocms_PropertyforleaseService {
    
    @isTest static void test_method_one() {
        ocms_PropertyforleaseService  wps = new ocms_PropertyforleaseService();
        
        wps.gettype();

        Map<String,String> sendMap = new Map<String,String>();
        sendMap.clear();
        sendMap.put('action','getStateList');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getCityList');
        sendMap.put('state','TX');
        
        System.assert(sendMap != Null);

        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getResultTotal');
        sendMap.put('state','TX');
        sendMap.put('city','Houston');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getPropertyList');
        sendMap.put('state','TX');
        sendMap.put('city','Houston');
       wps.executeRequest(sendMap);



        wps.loadResponse();
    }
    
  
}