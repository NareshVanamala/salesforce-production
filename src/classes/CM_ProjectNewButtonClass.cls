public with sharing class CM_ProjectNewButtonClass
{
    public String recortypeid{get;set;}
    public String pid{get;set;}
    Project__c mp{get;set;}
    Project__c ProjectObj{get;set;}
    MRI_PROPERTY__c ppc{get;set;}
    
  Public CM_ProjectNewButtonClass(Apexpages.StandardController controller)
  {
     recortypeid=ApexPages.CurrentPage().getParameters().get('RecordType');
     mp= (Project__c)controller.getRecord();
     pid=mp.id;
   }
  
   public PageReference Gobacktostandardpage() 
   {  
     if((mp.Building__c==null))
     {
     //Stage URL
     //String Url=System.Label.Current_Org_Url+'/'+System.Label.ProjectID+'/e?nooverride=1&retURL=%2F'+System.Label.ProjectID+'%2Fo&RecordType='+mp.RecordTypeId+'&ent=01I500000003doK&Name=Will Auto fill';
    
     //Production URL
     String Url=System.Label.Current_Org_Url+'/'+System.Label.ProjectID+'/e?nooverride=1&retURL=%2F'+System.Label.ProjectID+'%2Fo&RecordType='+mp.RecordTypeId+'&ent=01I500000003doK&Name=Will Auto fill';
     system.debug('Url'+Url); 
     PageReference acctPage = new PageReference(Url);
     acctPage.setRedirect(true); 
     return acctPage; 
     }  
      else if((mp.Building__c!=null))
     {
        
         system.debug('Check the building'+mp.Building__c);
         string bid1;
         Id aid;
         string ast;
         String AsMgrId;
         String AsMgrName;
         String PrMgrId;
         String PrMgrName;
         string bid=mp.Building__c;
         system.debug('Check the building'+bid);
         ppc=[select id, Name,Asset_Manager__r.Name,Asset_Manager__c,Property_Manager__c,Property_Manager__r.Name from MRI_PROPERTY__c where id=:bid];
         if(ppc!=null)
         {
             bid1=ppc.Name;
             if(ppc.Asset_Manager__c!=null)
             {
                 AsMgrId=ppc.Asset_Manager__c ;
                 AsMgrName=ppc.Asset_Manager__r.name;
                 AsMgrName=AsMgrName.replaceAll('&','%26');
             }
              if(ppc.Property_Manager__c!=null)
             {
                 PrMgrId=ppc.Property_Manager__c;
                 PrMgrName=ppc.Property_Manager__r.name;
                 PrMgrName=PrMgrName.replaceAll('&','%26');
             }
         }
         String s2 = bid1.replaceAll('&', '%26');
        //Stage URL
        //String Url1=System.Label.Current_Org_Url+'/'+System.Label.ProjectID+'/e?nooverride=1&CF00NR0000001HslJ_lkid='+bid+'&CF00NR0000001HslJ='+s2+'&CF00NR0000001HslT_lkid='+AsMgrId+'&CF00NR0000001HslT='+AsMgrName+'&CF00NR0000001Hslw_lkid='+PrMgrId+'&CF00NR0000001Hslw='+PrMgrName+'&retURL=%2F'+System.Label.ProjectID+'%2Fo&RecordType='+mp.RecordTypeId+'&ent=01I500000003doK&Name=Will Auto fill';
        //Production URL
          String Url1=System.Label.Current_Org_Url+'/'+System.Label.ProjectID+'/e?nooverride=1&CF00N50000003L5hJ_lkid='+bid+'&CF00N50000003L5hJ='+s2+'&CF00N50000003L5hS_lkid='+AsMgrId+'&CF00N50000003L5hS='+AsMgrName+'&CF00N50000003L5hn_lkid='+PrMgrId+'&CF00N50000003L5hn='+PrMgrName+'&retURL=%2F'+System.Label.ProjectID+'%2Fo&RecordType='+mp.RecordTypeId+'&ent=01I500000003doK&Name=Will Auto fill';
         PageReference acctPage = new PageReference(Url1);
         acctPage.setRedirect(true); 
         return acctPage; 
     }
     return null;
  }
  
}