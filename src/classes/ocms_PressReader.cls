global virtual without sharing class ocms_PressReader extends cms.ContentTemplateController
{
    global ocms_PressReader() 
    {
    }

    global override virtual String getHTML()
    {
        String html = '<div class="container home"><section class="main" style="clear: both;"><div class="col-md-home"></div><div class="ocmsPressReader">';
        for(Press_Releases__c sObj_PressRelease: [SELECT Id, Press_Release_Name__c,Date__c,Article_Link__c,Description__c,Published_Date__c FROM Press_Releases__c ORDER BY Published_Date__c DESC limit 4])
        {
               
                html  +='<div class="col-md-home dates justleft" style="clear: none;">'+
                '<h4>' + sObj_PressRelease.get('Date__c') + '</h4>' +
                '<p>' + sObj_PressRelease.get('Press_Release_Name__c') + '<br><br>' +
                '<a class="ocms_link_pdf" href="' + sObj_PressRelease.get('Article_Link__c')  + '" target="_blank">Read More »</a></p>' +
                '<p>&nbsp;</p>' +
                '</div>';
        }
        html +='</div>';
         html  +='<div class="right" style="clear:both;"><a href="http://www.vereit.com/press-releases" target="_blank">View More</a></div></div></section>';
        system.debug(html);
        return html;
    }
    
    
    
    
    
}