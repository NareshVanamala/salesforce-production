public with sharing class ticketcount {

    public list<Contact>lstConts1{get;set;}
    public list<Contact>lstConts2{get;set;}
    public list<Contact>lstConts3{get;set;}
    public list<id>contids=new list<id>();
    public String accname{set;get;}
    
    public Integer ticket1to3col1 {set;get;}
    public Integer ticket4to24col1 {set;get;}
    public Integer ticket25morecol1 {set;get;}
    public Integer rowtotalcol1 {set;get;}
    public Integer firstrowtotal {set;get;}
    
    public Integer ticket1to3col2 {set;get;}
    public Integer ticket4to24col2 {set;get;}
    public Integer ticket25morecol2 {set;get;}
    public Integer rowtotalcol2 {set;get;}
    public Integer secondrowtotal {set;get;}
    
    public Integer ticket1to3col3 {set;get;}
    public Integer ticket4to24col3 {set;get;}
    public Integer ticket25morecol3 {set;get;}
    public Integer rowtotalcol3 {set;get;}
    public Integer thirdrowtotal{set;get;}
    
    public Integer lastrowtotal{set;get;}
    
    //Days Calculation
    Date d1 = Date.today() - 30;
    Date d2 = Date.today() - 60;
    Date d3 = Date.today() - 180;
    
    
    public ticketcount(ApexPages.StandardController controller) {
    
       
       ticket1to3col1 =0;
       ticket4to24col1 =0;
       ticket25morecol1 =0;
       rowtotalcol1 =0;
       firstrowtotal=0;
       
       ticket1to3col2 =0;
       ticket4to24col2 =0;
       ticket25morecol2 =0;
       rowtotalcol2 =0;
       secondrowtotal =0;
       
       ticket1to3col3 =0;
       ticket4to24col3 =0;
       ticket25morecol3 =0;
       rowtotalcol3 =0;
       thirdrowtotal =0;
       
       lastrowtotal=0;
       
       accname='';
      
       //Retrieve the account record:       
       Account a=[select id, name from Account where id = :ApexPages.currentPage().getParameters().get('id')];
       //Account a=[select id, name from Account where id = '001P000000adMsB'];
       accname = a.name;
             
       // For the column "30-60" days.
       
       //Retrive all the contacts which is having the account as "National Accounts"
       lstConts1=[select id,name,Territory__c,Total_Cole_Tickets_Only_Funds_in_SF__c,Last_Producer_Date__c from Contact where accountid =:a.id and Last_Producer_Date__c >=:d2 and Last_Producer_Date__c < :d1];
       System.debug('*****consize' + lstConts1.size());
       system.debug('check the date today-30-------'+d1);
       system.debug('check the date today-60-------'+d2);
       system.debug('check the date today-180-------'+d3);
       for(Contact Ct :lstConts1)
       {
                 
         if(Ct.Total_Cole_Tickets_Only_Funds_in_SF__c >=1 && Ct.Total_Cole_Tickets_Only_Funds_in_SF__c <=3)
          ticket1to3col1 = ticket1to3col1 + 1; 
                              
         else if(Ct.Total_Cole_Tickets_Only_Funds_in_SF__c >=4 && Ct.Total_Cole_Tickets_Only_Funds_in_SF__c <=24){ 
          ticket4to24col1 = ticket4to24col1 + 1;
          }          
         else if(Ct.Total_Cole_Tickets_Only_Funds_in_SF__c >=25)
          ticket25morecol1 = ticket25morecol1 + 1;
                                            
      }
      rowtotalcol1 = ticket1to3col1 + ticket4to24col1 + ticket25morecol1 ;
      
           
      // For the column "60-180" days.
      
      //Retrive all the contacts which is having the account as "National Accounts"
       lstConts2=[select id,name,Territory__c,Total_Cole_Tickets_Only_Funds_in_SF__c,Last_Producer_Date__c from Contact where accountid =:a.id and Last_Producer_Date__c >= :d3 and   Last_Producer_Date__c < :d2];
       System.debug('*****consize' + lstConts2.size());

       
       for(Contact Ct :lstConts2)
       {
                 
         if(Ct.Total_Cole_Tickets_Only_Funds_in_SF__c >=1 && Ct.Total_Cole_Tickets_Only_Funds_in_SF__c <=3)
          ticket1to3col2 = ticket1to3col2 + 1; 
                              
         else if(Ct.Total_Cole_Tickets_Only_Funds_in_SF__c >=4 && Ct.Total_Cole_Tickets_Only_Funds_in_SF__c <=24){ 
          ticket4to24col2 = ticket4to24col2 + 1;
          }          
         else if(Ct.Total_Cole_Tickets_Only_Funds_in_SF__c >=25)
          ticket25morecol2 = ticket25morecol2 + 1;
                                            
      }
      rowtotalcol2 = ticket1to3col2 + ticket4to24col2 + ticket25morecol2 ;
      
      
      // For the column "180+" days.
      
      //Retrive all the contacts which is having the account as "National Accounts"
       lstConts3=[select id,name,Territory__c,Total_Cole_Tickets_Only_Funds_in_SF__c,Last_Producer_Date__c from Contact where accountid =:a.id and Last_Producer_Date__c < :d3];
       System.debug('*****consize' + lstConts3.size());
       
       for(Contact Ct :lstConts3)
       {
                 
         if(Ct.Total_Cole_Tickets_Only_Funds_in_SF__c >=1 && Ct.Total_Cole_Tickets_Only_Funds_in_SF__c <=3)
          ticket1to3col3 = ticket1to3col3 + 1; 
                              
         else if(Ct.Total_Cole_Tickets_Only_Funds_in_SF__c >=4 && Ct.Total_Cole_Tickets_Only_Funds_in_SF__c <=24){ 
          ticket4to24col3 = ticket4to24col3 + 1;
          }          
         else if(Ct.Total_Cole_Tickets_Only_Funds_in_SF__c >=25)
          ticket25morecol3 = ticket25morecol3 + 1;
                                            
      }
      rowtotalcol3 = ticket1to3col3 + ticket4to24col3 + ticket25morecol3 ;
 
      
      
      //Lasttotalcolumncalculation
      firstrowtotal = ticket1to3col1 + ticket1to3col2 + ticket1to3col3;
      secondrowtotal = ticket4to24col1 + ticket4to24col2 + ticket4to24col3;
      thirdrowtotal = ticket25morecol1 + ticket25morecol2 + ticket25morecol3;
      lastrowtotal = rowtotalcol1 + rowtotalcol2 + rowtotalcol3;
      

    }

}