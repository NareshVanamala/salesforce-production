@isTest
    private class Test_ClassforAppendingTrigger 
    {
        public static testMethod void  Test_ClassforAppendingTrigger ()  
        {
            
            MRI_PROPERTY__c  mo = new MRI_PROPERTY__c () ;
            
            mo.Name ='Name' ;
            mo.Common_Name__c='test';
            mo.Property_Manager__c=UserInfo.getUserId();
            mo.AvgLeaseTerm__c  = 12 ;
            mo.Long_Description__c  = 'test' ; 
            mo.Web_Ready__c='Approved';
            mo.status__c='Owned';
            mo.Program_Id__c='ARCP';
            mo.Property_ID__c='PL1234789'; 
            insert mo ; 
            
            
            Deal__c dct= new Deal__c();
            dct.name='Mydeal';
            dct.City__c='Phoneix';
            dct.State__c='Ap';
            dct.Zip_Code__c='1234';
            dct.Building_Name__c=mo.Name;
            dct.Center_Name__c=mo.Common_Name__c;
            //dct.Pipeline_Id__c='PL1234789';
            dct.Owned_Id__c='PL1234789';
            dct.Property_Manager__c= mo.Property_Manager__c;
            dct.Address__c='Parijat colony';
            insert dct;
    
           dct.City__c='hyderabad';
           update dct;
            
           system.assertequals(dct.Building_Name__c,mo.Name);

           
           list<Deal__c > deallist= new list<Deal__c >();
           
           Deal__c dect1= new Deal__c();
           dect1.name='Mydeal1';
           dect1.City__c='Phoneix';
           deallist.add(dect1);
         
           
           Deal__c dect11= new Deal__c();
           dect11.name='Mydeal12';
           dect11.City__c='Pune';
           deallist.add(dect11);
         
          //deallist=[select id,City__c,State__c,Zip_Code__c,Address__c from Deal__c where City__c='Phoneix'];
          insert deallist;
           
           system.assertnotequals(deallist,Null);

        }
    
    
  }