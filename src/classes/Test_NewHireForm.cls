@isTest
private class Test_NewHireForm
 {
      public static testMethod void TestNewHireForm () 
      {
          Id rtp=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormParent' and SobjectType = 'NewHireFormProcess__c'].id;
          Id rtc=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormChild' and SobjectType = 'NewHireFormProcess__c'].id;
        
        NewHireFormProcess__c c = new NewHireFormProcess__c();
        
        
        ApexPages.StandardController sc= new ApexPages.StandardController(c);
        NewHireFormProcess n = new NewHireFormProcess(sc);
        
        c.recordtypeid = rtp;
        c.HR_ManagerApprovalStatus__c = 'Sumitted For Approval';
       
       // c.Origin = 'Web Site';
          insert c;
          n.submitCase();
          
       system.assertequals(c.recordtypeid,rtp);

          
      }
}