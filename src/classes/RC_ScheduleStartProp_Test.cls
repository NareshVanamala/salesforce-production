@isTest
public class RC_ScheduleStartProp_Test 
{
    /*  static testmethod void myTestMethod1()
    {    
        Test.StartTest();
        RC_ScheduleStartProp cls= new RC_ScheduleStartProp ();
        SchedulableContext sc;
        cls.execute(sc); 
    }
    */
    
    static testMethod void myUnitTest() {

            Test.startTest();
                    String CRON_EXP = '0 0 0 1 1 ? 2050';  
                    String jobId = System.schedule('testScheduledApex', CRON_EXP, new RC_ScheduleStartProp () );

                    CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];

                    System.assertEquals(CRON_EXP, ct.CronExpression); 
                    System.assertEquals(0, ct.TimesTriggered);
                    System.assertEquals('2050-01-01 00:00:00', String.valueOf(ct.NextFireTime));

            Test.stopTest();
    }
}