global class ScheduleBatchEventDelete implements Schedulable
{
   global void execute (SchedulableContext SC)
   {
    BatchEventDelete ao = new BatchEventDelete (); 
    Database.executebatch(ao);
   }
}