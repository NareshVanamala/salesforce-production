global virtual without sharing class ocms_propertiesforlease extends cms.ContentTemplateController{

    private List<SObject> propertyList;
    private String html;
    public string webready='Approved';
    public string multitenant='%Multi-Tenant%';
    public Map<String, String> stateMap = new Map<String, String>(); 
    
  
    global ocms_propertiesforlease() {
        stateMap.put('AL','Alabama');
        stateMap.put('AK','Alaska');
        stateMap.put('AZ','Arizona');
        stateMap.put('AR','Arkansas');
        stateMap.put('CA','California');
        stateMap.put('CO','Colorado');
        stateMap.put('CT','Connecticut');
        stateMap.put('DE','Delaware');
        stateMap.put('FL','Florida');
        stateMap.put('GA','Georgia');
        stateMap.put('HI','Hawaii');
        stateMap.put('ID','Idaho');
        stateMap.put('IL','Illinois');
        stateMap.put('IN','Indiana');
        stateMap.put('IA','Iowa');
        stateMap.put('KS','Kansas');
        stateMap.put('KY','Kentucky');
        stateMap.put('LA','Louisiana');
        stateMap.put('ME','Maine');
        stateMap.put('MD','Maryland');
        stateMap.put('MA','Massachusetts');
        stateMap.put('MI','Michigan');
        stateMap.put('MN','Minnesota');
        stateMap.put('MS','Mississippi');
        stateMap.put('MO','Missouri');
        stateMap.put('MT','Montana');
        stateMap.put('NE','Nebraska');
        stateMap.put('NV','Nevada');
        stateMap.put('NH','New Hampshire');
        stateMap.put('NJ','New Jersey');
        stateMap.put('NM','New Mexico');
        stateMap.put('NY','New York');
        stateMap.put('NC','North Carolina');
        stateMap.put('ND','North Dakota');
        stateMap.put('OH','Ohio');
        stateMap.put('OK','Oklahoma');
        stateMap.put('OR','Oregon');
        stateMap.put('PA','Pennsylvania');
        stateMap.put('RI','Rhode Island');
        stateMap.put('SC','South Carolina');
        stateMap.put('SD','South Dakota');
        stateMap.put('TN','Tennessee');
        stateMap.put('TX','Texas');
        stateMap.put('UT','Utah');
        stateMap.put('VT','Vermont');
        stateMap.put('VA','Virginia');
        stateMap.put('WA','Washington');
        stateMap.put('WV','West Virginia');
        stateMap.put('WI','Wisconsin');
        stateMap.put('WY','Wyoming');
   
    } 
  
  public Integer getResultTotal(String state, String city){
        
       // set<string> vacantleaseids=new set<string>();
        String query;
       /* set<string> cd=new set<string>();
         
        list<Vacant_Lease__c> vl=[select id,name from Vacant_Lease__c];
        
        for(Vacant_Lease__c code1:vl)
        {
           cd.add(code1.name);
        }*/
                
        query = 'SELECT COUNT() FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Property_type__c LIKE \'' + multitenant + '\' and Status__c in (\'Owned\',\'Managed\')';
        
        
        if(state != null && state != '' && state != 'all'){
            query += ' AND State__c = \'' + state + '\'';
        }

        if(city != null && city != '' && state != 'all' && city != 'all'){
            query += ' AND City__c = \'' + city + '\'';
        }

        return Database.countQuery(query);

    } 
   
   
   public List<SObject> getPropertyList(String state, String city, String sortBy, String rLimit, String offset ){

        List<SObject> propertyList;
        String query;    
     /*   set<string> cd=new set<string>();
               
        list<Vacant_Lease__c> vl=[select id,name,Suite_SQFT__c from Vacant_Lease__c];
        
        for(Vacant_Lease__c code1:vl)
        {
           cd.add(code1.name);
        }*/
      
        query = 'SELECT Id, Name, Common_Name__c, Web_Name__c,Lease_Status__c, City__c, State__c, Status__c, Gross_Leaseable_Area__c, Property_Type__c, Date_Acquired__c, Location__c, property_id__c, Image__c, SqFt__c FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Property_type__c LIKE \'' + multitenant + '\' and Status__c in (\'Owned\',\'Managed\')';
        
        
        if(state != null && state != '' && state != 'all'){
            query += ' AND State__c = \'' + state + '\'';
        }

        if(city != null && city != '' && state != 'all' && city != 'all'){
            query += ' AND City__c = \'' + city + '\'';
        }



        if(sortBy != null && sortBy != ''){
            
             if( sortBy != 'location' ){
                query += ' ORDER BY ' + sortBy + ' ASC ';
            } else {
                query += ' ORDER BY State__c, City__c ';
            }
        }

        if(rLimit != null && rLimit != ''){
            query += ' LIMIT ' + rLimit;
        }

        if(offset != null && offset != ''){
            query += ' OFFSET ' + offset;
        }


        propertyList = Database.query(query);
        
    /*   list<web_properties__c> wblist=new list<web_properties__c>();
       list<Vacant_Lease__c> vllist=[select id,Suite_SQFT__c,name from Vacant_Lease__c where Suite_SQFT__c!=null];
        
       for(SObject record : propertyList) 
       {
       wblist.add((web_properties__c) record);
       }
       
       for(web_properties__c wb:wblist)
       {
       integer squareft=0;
           
       for(Vacant_Lease__c vl1:vllist)
       {
       if(wb.property_id__c==vl1.name)
       {
       squareft=squareft+integer.valueof(vl1.Suite_SQFT__c);
       }
       }
       wb.Gross_Leaseable_Area__c=squareft;
       wblist.add(wb);
       }
       update wblist;
       */
       
       return propertyList;
    }
    
    /*  public string getsqft(String state, String city, String sortBy, String rLimit, String offset){
       String response = '';
       Boolean first = true;
       List<web_properties__c> wblist;
       String query;    
       set<string> cd=new set<string>();
               
        list<Vacant_Lease__c> vl1=[select id,name,Suite_SQFT__c from Vacant_Lease__c];
        
        for(Vacant_Lease__c code1:vl1)
        {
           cd.add(code1.name);
        }
      
        query = 'SELECT Id, Name, Lease_Status__c, City__c, State__c, Status__c, Gross_Leaseable_Area__c, Property_type__c, Date_Acquired__c, Location__c, property_id__c, Image__c FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Property_type__c LIKE \'' + multitenant + '\' and Status__c in (\'Owned\',\'Managed\')';

        
        if(state != null && state != '' && state != 'all'){
            query += ' AND State__c = \'' + state + '\'';
        }

        if(city != null && city != '' && state != 'all' && city != 'all'){
            query += ' AND City__c = \'' + city + '\'';
        }



        if(sortBy != null && sortBy != ''){
            if( sortBy == 'Name' ){
                query += ' ORDER BY Name ASC ';
            }else if( sortBy != 'location' ){
                query += ' ORDER BY ' + sortBy + ' ASC ';
            } else {
                query += ' ORDER BY State__c, City__c ';
            }
        }

        if(rLimit != null && rLimit != ''){
            query += ' LIMIT ' + rLimit;
        }

        if(offset != null && offset != ''){
            query += ' OFFSET ' + offset;
        }


       wblist= Database.query(query);
 
       list<Vacant_Lease__c> vllist=[select id,Suite_SQFT__c,name from Vacant_Lease__c where Suite_SQFT__c!=null];
       
       response+='[';
       for(web_properties__c wb:wblist)
       {
       integer squareft=0;
            if(first){
                first=false;
            }else{
                response+=', ';
            }
       for(Vacant_Lease__c vl:vllist)
       {
       if(wb.property_id__c==vl.name)
       {
       squareft=squareft+integer.valueof(vl.Suite_SQFT__c);
       }
       }
       response+='{"short":"' + wb.Id + '", ';
       response+='"long":"' + squareft + '"}';
       }
       response+=']';
       return response;
      
       }
       */
       
              
      
       
       
       public String getStateList(){

        List<SObject> stateList;
        List<String> outList = new List<String>();
        String response = '';
        Boolean first = true;
     /*   set<string> cd=new set<string>();
               
        list<Vacant_Lease__c> vl=[select id,name,Suite_SQFT__c from Vacant_Lease__c];
        
        for(Vacant_Lease__c code1:vl)
        {
           cd.add(code1.name);
        }*/
        
        String query = 'SELECT State__c FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Property_type__c LIKE \'' + multitenant + '\' and Status__c in (\'Owned\',\'Managed\') GROUP BY State__c ORDER BY State__c ASC';
        stateList = Database.query(query);
        stateList = sanitizeList(stateList, 'State__c');

        response+='[';

        for(SOBject s : stateList){
            if(first){
                first=false;
            }else{
                response+=', ';
            }
            response+='{"short":"' + String.valueOf(s.get('State__c')) + '", ';
            if(stateMap.get(String.valueOf(s.get('State__c'))) != null)
                response+='"long":"' + stateMap.get(String.valueOf(s.get('State__c'))) + '"}';
            else
                response+='"long":"' + String.valueOf(s.get('State__c')) + '"}';
        }

        response+=']';

        return response;

    }
    
     public List<SObject> getCityList(String state){

        List<SObject> cityList;
      /*  set<string> cd=new set<string>();
               
        list<Vacant_Lease__c> vl=[select id,name,Suite_SQFT__c from Vacant_Lease__c];
        
        for(Vacant_Lease__c code1:vl)
        {
           cd.add(code1.name);
        }*/
        
        String query = 'SELECT City__c FROM Web_Properties__c WHERE State__c = \'' + state + '\' and Web_Ready__c=\'' + webready + '\' and Property_type__c LIKE \'' + multitenant + '\' and Status__c in (\'Owned\',\'Managed\') GROUP BY City__c ORDER BY City__c ASC';
        cityList = Database.query(query);

        return sanitizeList(cityList, 'City__c');

    }
  
   
    private List<SObject> sanitizeList(List<SObject> inList, String field){

        Integer ctr = 0;

        while(ctr < inList.size()){
            if(inList[ctr].get(field) == null){
                inList.remove(ctr);
            } else {
                ctr++;
            }
        }
        return inList;

    }
   
    private String writeControls(){
        String html = '';
      
        html+='<article class="topMar7 wrapper teer3">'+
              '<h3>PROPERTIES FOR LEASE</h3>'+
              '</article>';   
              
        html += '<div class="ocms_WebPropertyList">' +
                '   <div class="ocms_WP_Controls">' +
                '   <div class="ocms_WP_narrowResultsArea">' +
                '<div class="ocms_WP_narrowResults"><a href="javascript:void(0)" id="ctl11_PropertySearch1_lnkNarrowResults" onClick="toggleNarrowResultsForm();">Narrow Results</a></div>'+
                '       <div class="ocms_WP_searchForm" id="ctl11_PropertySearch1_narrowResultsForm">' +
                '           <div class="ocms_WP_leftColumn">' +
                '               <div class="byLocationForm">' +
                '                   <div class="ocms_WP_label"><strong>State</strong></div>' +
                '                   <select class="stateDropdown">' +
                '                       <option value="all">All States</option>' +
                '                   </select>' +
                '                   <div class="ocms_WP_label"><strong>City</strong>' +
                '                   <select class="cityDropdown">' +
                '                       <option value="all">All Cities</option>' +
                '                   </select>' +
                '                   </div>' +
                '               </div>' +
                '           </div>' +
                '       </div>' +
                '   </div>' +
                '   <div class="tabSwitchBottom">' +
                '       <ul class="heroContentNav">' +
                '           <li class="listViewControl">List View</li>' +
                '           <li class="mapViewControl">Map View</li>' +
                '       </ul>' +
                '   </div>' +
                '   <div class="resultBar">' +
                '       <div class="resultBarCount">' +
                '           <div class="propertyCountContainer"><span class="propertyCount"></span> <span class="propertyText"></span></div>' +
                '           <div class="displayingResults">Displaying <span class="lower"></span> - <span class="upper"></span> of <span class="propertyCount"></span></div>' +
                '       </div>' +
                '       <div class="sort">' +
                '           <div class="ocms_WP_label"><strong>Sort By</strong></div>' +

                '           <div class="uniformDropdown">' +
                '               <select class="sortByDropdown">' +
                '                   <option value="Web_Name__c">Property Name</option>' +
                '                   <option value="location">Location</option>' +
                '                   <option value="Gross_Leaseable_Area__c">Gross Leaseable Area</option>' +
                '                   <option value="Lease_Status__c">Status</option>' +
                '               </select>' +
                '           </div>' +
                '       </div>' +
                '       <div class="resultBarPaginationTop" style="text-align:right;">' +
                '           <div class="leftArrow"></div>' +
                '           <div class="paginateText">Page <span class="pageLower"></span> of <span class="pageUpper"></span></div>' +
                '           <div class="rightArrow"></div>' +
                '       </div>' +
                '   </div>' +
                '   </div>' +
                '   <div class="propertyListView"></div>' +
                '   <div class="propertyMapView">' +
                '       <div class="propertyList"></div>' +
                '       <div id="map-canvas"></div>' +
                '   </div>' +
                '   <div class="resultBarPaginationBottom" style="text-align:right;">' +
                '       <div class="leftArrow"></div>' +
                '       <div class="paginateText">Page <span class="pageLower"></span> of <span class="pageUpper"></span></div>' +
                '       <div class="rightArrow"></div>' +
                '   </div>' +
                '</div>';

        return html;
    }
    
    global override virtual String getHTML(){
        String html = '';

        html += writeControls();
   
        html += '<script src="/resource/1425427969000/ocms_propertiesforlease" type="text/javascript"></script>';

        return html;
    }
    

}