public with sharing class EmbeddedController
{
 public ApexPages.StandardController stdCtrl {get; set;}
 public Boolean refreshPage {get; set;}
  
 public EmbeddedController(ApexPages.standardController std)
 {
  stdCtrl=std;
  refreshPage=false;
 }
  
 public PageReference save()
 {
  refreshPage=true;
  stdCtrl.save();
   
                return null;
 }
}