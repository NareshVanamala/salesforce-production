@isTest
private class TestforEconomicinvestementStrategyclass 
 {
      public static testMethod void TestforEconomicinvestementStrategyclass  () 
      {
          Account a= new Account();
          a.name='Testaccount1';
          //RecordType rtypes = [Select Name, Id From RecordType where isActive=true and sobjecttype='Account'and name='National_Accounts'];
          //a.recordtypeid=rtypes.id;
          insert a;  
          
          Economic_Investment_Strategy__c ct= new Economic_Investment_Strategy__c();
          ct.Account__c=a.id;
          ct.Description__c='Hyderabaddd';
          ct.name='Testingthe Programing';
          insert ct;
          
          Economic_Investment_Strategy__c ct2= new Economic_Investment_Strategy__c();
          ct2.Account__c=a.id;
          ct2.Description__c='Hyderabadddd';
          ct2.name='Testingthe Programing34';
          insert ct2;
          
          
          String nameFile = 'hello';
                   
           Attachment attachment= new Attachment();
           attachment.OwnerId = UserInfo.getUserId();
           attachment.ParentId = ct.Id;// the record the file is attached to
           attachment.Name = nameFile;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
           attachment.body=bodyBlob;
           insert attachment;
           
              
        
         ApexPages.currentPage().getParameters().put('id',a.id);
         ApexPages.StandardController controller = new ApexPages.StandardController(a); 
         EconomicinvestementStrategyclass x = new EconomicinvestementStrategyclass(controller);  
         
         x.nameFile='Vasu_Snehal';
         Blob blb=Blob.valueOf('Vasu_SnehalVasu_SnehalVasu_Snehal');         
         x.contentFile=blb;
         x.addrow.Name='B_D_Systems Name';
         List<Attachment> attachments=[select id, name from Attachment where parent.id=:ct.id];
         System.assertEquals(1, attachments.size());
         
         
         x.newProgram();
         x.savenewProg();
         x.editrecord();
         //x.viewRecord();
         x.SaveRecord();
         x.removecon();
         x.cancelnewProg();          

       }
         
          
           
 }