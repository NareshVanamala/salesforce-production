public with sharing class  CP_HelperAccConProdcls{
public List<New_Product__c> npList {get;set;}
public List<New_Product__c> allpro{get;set;}
public List<productWrapperClass> pwcList{get;set;}
public Boolean isFailed{get;set;}

public Account acc;
public List<New_Product__c> selectprod {get;set;}
public Map <String, Boolean> chkIndividuals {get;set;}
public Map <String, New_Product__c> npListmap {get;set;}
public Boolean selc{get;set;}
public Boolean dup{get;set;}
public Boolean alrt{get;set;}
public Boolean isStatusDisplay{get;set;}
private integer totalRecs = 0;    
private integer index = 0;
private integer blockSize = 10;  
    public CP_HelperAccConProdcls() {
        this.chkIndividuals=new Map <String, Boolean>();
        this.npListmap=new Map <String, New_Product__c>();
        pwcList = new List<productWrapperClass>();
        npList = new List<New_Product__c>();
        selectprod = new List<New_Product__c>();
        allpro = Database.Query('Select id, Name,Competitor__c,Competitor__r.Name, Price_Per_Share__c, Dividend__c, Strategy__c,Talking_Points__c  From New_Product__c LIMIT :blockSize OFFSET :index');
        totalRecs = [select count() from New_Product__c];
        acc =new Account();
        selc=false;
        dup=false;
        alrt=false;
        isFailed=false;
        isStatusDisplay = false;   
    }

    public List<New_Product__c> getMemb() {
        List<New_Product__c> membs = Database.Query('Select id, Name,Competitor__c,Competitor__r.Name, Price_Per_Share__c, Dividend__c, Strategy__c,As_of_Date_Text__c,Status__c,Yield__c,Last_Valuation_Date__c,Talking_Points__c  From New_Product__c LIMIT :blockSize OFFSET :index');
        System.debug('Values are ' + membs);
        return membs;
    }  

    public void Searchpro(String str) {
        pwcList.clear();
        system.debug('******************************* CP_HelperAccConProdcls STR :'+str);
        npList = [Select id, Name,Competitor__c,Competitor__r.Name, Price_Per_Share__c, Dividend__c, Strategy__c,
                            As_of_Date_Text__c,Status__c,Yield__c,Last_Valuation_Date__c,Talking_Points__c  From New_Product__c Where ((Name Like : str ) or (Competitor__r.Name Like : str ) ) ];
        if(npList.isEmpty()) {
            selc=true;
        }
        else {
            selc=false;
        }
         
        for(New_Product__c np:npList){
            productWrapperClass pwc = new productWrapperClass(np,false,null);
            pwcList.add(pwc);
        }
    }

    public Void addAcc(ID curid, Boolean selected) {
        isStatusDisplay = true;
        List<Account_Product__c> apList = new List<Account_Product__c>();

        selectprod.clear();
        isFailed = false;
        Map<integer,productWrapperClass> pwcMap = new Map<integer,productWrapperClass>();
        integer count = 0;
        for(productWrapperClass pwc : pwcList){
            if(pwc.selectedValue){
                selectprod.add(pwc.np);
                pwcMap.put(count,pwc);
                count++;
                system.debug('##ram##');
            } 
        }
        
        for(New_Product__c npr : selectprod) {
            Account_Product__c ap =new Account_Product__c();     
            system.debug('### Con id '+curid);       
            ap.Account__c=curid;
            ap.Product__c=npr.id;
            apList.add(ap);                  
        }
        
        Savepoint sp = Database.setSavepoint();
        system.debug('## Ram ## aplist Size='+apList.size());
        system.debug('## Ram ## pwcMap Size='+pwcMap.size());
        List<Database.SaveResult> results = new List<Database.SaveResult>();
        if(apList.size()>0) {
        if (Schema.sObjectType.Account_Product__c.isCreateable())
            results = Database.insert(apList,false);             
        }
        for(integer i=0;i<pwcMap.keySet().size();i++){
            if(results[i].isSuccess()){
                pwcMap.get(i).status = 'Added';
            }
            else{
                pwcMap.get(i).status = 'Record already selected, Please uncheck this one.';
                pwcMap.get(i).isFailed = true;
                isFailed = true;
            }
        }
        if(isFailed){ 
            for(productWrapperClass pwc:pwcList){
                pwc.status = pwc.status == 'Added'?null:pwc.status;
                pwc.status = pwc.selectedValue == false ? null : pwc.status;
            }     
           Database.rollback(sp);
        }
    }
    

    public void addCon(ID curid, Boolean selected) {        
        isStatusDisplay = true;
        List<Contact_Product__c> cpList = new List<Contact_Product__c>();
        
        selectprod.clear();
        isFailed = false;
        Map<integer,productWrapperClass> pwcMap = new Map<integer,productWrapperClass>();
        integer count = 0;
        for(productWrapperClass pwc : pwcList){
            if(pwc.selectedValue){
                selectprod.add(pwc.np);
                pwcMap.put(count,pwc);
                count++;
                system.debug('##ram##');
            } 
        }
        
        for(New_Product__c npr : selectprod) {
            Contact_Product__c cp = new Contact_Product__c();
             if (Schema.sObjectType.Contact_Product__c.fields.Contact__c.isAccessible())
            cp.Contact__c = curid;
            if (Schema.sObjectType.Contact_Product__c.fields.Product__c .isAccessible())
             cp.Product__c = npr.id;
            cpList.add(cp);
        }
        
        Savepoint sp = Database.setSavepoint();

        List<Database.SaveResult> results = new List<Database.SaveResult>();
        if(cpList.size()>0) {
            if (Schema.sObjectType.Contact_Product__c.isCreateable())
            results = Database.insert(cpList,false); 
            for(integer i=0;i<pwcMap.keySet().size();i++){
                if(results[i].isSuccess()){
                    pwcMap.get(i).status = 'Added';
                }
                else{
                    system.debug('######'+results[i].getErrors()[0].getmessage());
                    pwcMap.get(i).status = 'Record already selected for Account. Please select another record.';
                    pwcMap.get(i).isFailed = true;
                    isFailed = true;
                }
            }            
        }
        
        if(isFailed){ 
            for(productWrapperClass pwc:pwcList){
                pwc.status = pwc.status == 'Added'?null:pwc.status;
                pwc.status = pwc.selectedValue == false ? null : pwc.status;
            }     
           Database.rollback(sp);
        }

    }
    /*  
    public class wrapAccount {
    public New_Product__c prod {get; set;}
    public Boolean selected {get; set;}
    public wrapAccount(New_Product__c p) {
    prod = p;
    selected = false;
    }  
    }*/

    public void beginning() {
        index = 0;
    }
    public void previous() {
        index = index - blockSize;
    }

    public void next() {
        index = index + blockSize;
    }

    public void end() {
        index = totalrecs - math.mod(totalRecs,blockSize);
    }        

    public boolean getprev() {
        if(index == 0)
        return true;
        else
        return false;
    }  

    public boolean getnxt() {
        if((index + blockSize) > totalRecs)
        return true;
        else
        return false;
    }
    
    Public class productWrapperClass{
        public New_Product__c np{get;set;}
        public Boolean selectedValue{get;set;}
        public String status{get;set;}
        public Boolean isFailed{get;set;}
        public productWrapperClass(New_Product__c npp,Boolean flag,String status){
            this.np = npp;
            this.selectedValue = flag;
            this.status= status;
            this.isFailed = false;
        }
    
    }
    
}