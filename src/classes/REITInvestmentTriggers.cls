public class REITInvestmentTriggers {
    public static Boolean run = true;

    public static void CalculateInvestorGrade(List<REIT_Investment__c> recs) {
        if (run) {
            Date todayMinus30 = date.today()-30;
            Set<Id> ctcts = new Set<Id>();
            for (REIT_Investment__c ri : recs) {
                ctcts.add(ri.Rep_Contact__c);
            }
            if (ctcts.size() > 0) {
                List<Contact> lcs = [SELECT Id, Tickets_Current_Month__c, (SELECT Id FROM REIT_Investments__r WHERE ADMIT_DATE__c > :todayMinus30) FROM Contact WHERE Id IN :ctcts];
                for (Contact c : lcs) {
                    c.Tickets_Current_Month__c = 0;
                    System.debug(c.REIT_Investments__r.size());
                    //for (REIT_Investment__c ri : c.REIT_Investments__r) {
                    //  if (ri.Admit_Date__c > date.today()-30){
                    //      c.Tickets_Current_Month__c++;
                    //  }
                    //}
                    c.Tickets_Current_Month__c = c.REIT_Investments__r.size();
                }
                update lcs;
            }
        }
    }
}