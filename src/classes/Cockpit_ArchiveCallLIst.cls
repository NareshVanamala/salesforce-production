global with sharing class Cockpit_ArchiveCallLIst implements Database.Batchable<SObject>{    
    global String Query; 
   
    global Database.QueryLocator start(Database.BatchableContext BC){  
        //string myid='a114B0000001g2JQAQ';
        String MyoverdueCallList='My Overdue & Due Today Tasks';
        String InboundCallList='Inbound Calls';
        query = 'Select Id,Last_Activity_Date__c,Archived__c from Automated_Call_List__c Where Archived__c=false and IsDeleted__c=false and Name!=:InboundCallList and Name!=:MyoverdueCallList' ;
        system.debug('This is start method');
        system.debug('Myquery is......'+Query);
        return Database.getQueryLocator(query);  
    }  
    
   global void execute(Database.BatchableContext BC,List<Automated_Call_List__c> scope)  {  
       list<Automated_Call_List__c> acl=new list<Automated_Call_List__c>();
       Integer numberofdays; 
       Id MngCallListRecId = Schema.SObjectType.Manage_Cockpit__c.getRecordTypeInfosByName().get('Manage Call Lists').getRecordTypeId();
       system.debug('this is the id'+MngCallListRecId );
       Manage_Cockpit__c ManageCallListRec=[select id,recordtypeid,name,Archive_Call_List_in_X_days__c from Manage_Cockpit__c where recordtypeid=:MngCallListRecId limit 1];
       
        if(ManageCallListRec!=null)
          numberofdays=integer.valueof(ManageCallListRec.Archive_Call_List_in_X_days__c);
       
       date d=system.today();
           
      if(scope.size()>0){
        for(Automated_Call_List__c acty :scope){
           if(acty.Last_Activity_Date__c!=null){
          
            system.debug('I am inside ..'+acty);
            system.debug('This is last activity date'+acty.Last_Activity_Date__c+ 'and this is todays date'+d);
            date dtetpcompare=acty.Last_Activity_Date__c;
            integer dayssincelastactivity=dtetpcompare.daysBetween(d) ;
            system.debug('These are number of days'+dayssincelastactivity);
            if(dayssincelastactivity>numberofdays){
              acty.Archived__c=true;
              acl.add(acty);
            }  
           }
        }
      }
     
      if(acl.size()>0){
       if(Schema.sObjectType.Automated_Call_List__c.isUpdateable())
         update acl;
      } 
   }
  global void finish(Database.BatchableContext BC)  {
    //Database.executeBatch(new Cockpit_ArchiveContactCallLIst(),200);
  }
}