public with sharing class CommunityUserDetails {
     private final Contact con;
     public User userObj{get;set;}
     public CommunityUserDetails(ApexPages.StandardController stdController) {
        this.con = (Contact)stdController.getRecord();
        userObj = new User();
         try{
            userObj = [Select id ,name,IsActive,LastLoginDate,CreatedDate,LastPasswordChangeDate from User where ContactId =:con.Id limit 1];
            }catch(exception e){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Not a registered Cole Capital Website User'));
         
         }
    }
}