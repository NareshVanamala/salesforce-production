public with sharing class RemoveContacts
{

   /* CallList__c callList ;
      //Search module
    public List<CallList__c> results{get;set;}
    public List<SelectOption> ListViews{set;get;}
    public string searchString{get;set;} // search keyword

    List<Automated_Call_List__c> clist= new List<Automated_Call_List__c>();
    public List<User> ulist{get;set;}
    public Boolean flag{get;set;}
    public String vname{get;set;}
    public Boolean success{get;set;}
    public Boolean bnNext{get;set;}
    public Boolean bnPrevious{get;set;}
    public Boolean bnFirst{get;set;}
    public Boolean bnLast{get;set;}
    String Viewname='';
    String FieldName ='--None--';
    String Fieldname1b='--None--';
    String Fieldname2c='--None--';
    String Fieldname3d='--None--';
    String Fieldname4e='--None--';
    String Operatora='';
    String Operator1b='';
    String Operator2c='';
    String Operator3d='';
    String Operator4e='';
    String valuea='';
    public String value1b{get;set;}
    public String value2c{get;set;}
    public String value3d{get;set;}
    public String value4e{get;set;}
    public string query1;
    public Integer cmCount;
    public Integer first{get;set;}
    public Integer rows{get;set;}
    public String CampaignName{get;set;}
    public list<id>removecontactids{get;set;}
    public list<id>FinalRemovecontactids{get;set;} 
    public CallList__c filter{get;set;}
    public CallList__c filter1{get;set;}
    String[] Username= new String[]{};
    public String username1{set;get;}
    public Map<String,String> mapfieldnames{get; set;}
    public List<Contact> conlist{get;set;}
    public list<Contact_Call_List__c>Removethelist{get;set;}
    public ApexPages.StandardSetController ssc;
    PageReference pageRef12;

    list<Contact_Call_List__c> lsttask = new list<Contact_Call_List__c>();
    public list<contact> lstContact{set;get;}
    public List<SelectOption> options1{set;get;} 
    public String CallList1 {set;get;}
    public Map<Id,Contact_Call_List__c> mapOfContactToTask{get;set;}
    public Map<integer,Contact_Call_List__c> rowmap{get;set;}
    public boolean showPrev{set;get;}
    public boolean showNext{set;get;}
    public Integer rownum {get;set;}{rownum=-10;}
    public Boolean bnSelect{get;set;}   
    public Contact_Call_List__c tskObj {set;get;}
    public list<Contact_Call_List__c>contactcalllist{get;set;}
    id userid;
    Integer PageNo = 1;
    GerateDynamicQuery generateQuery =  new GerateDynamicQuery();
    
    //At the page load we will initialize the picklist with the contacts fields.--Start
    public void init()
    {
       flag=false;
       success=false;
       cmCount = 0;
       value1b='';
       value2c='';
       value3d='';
       value4e='';
       CampaignName='';
       Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
       Schema.SObjectType sobjType = gd.get('contact');
       Schema.DescribeSObjectResult r = sobjType.getDescribe();
       Map<String,Schema.SObjectField> M = r.fields.getMap();
       System.debug('+++m'+M);
       for(String fieldName1 : M.keyset())
        {
            Schema.SObjectField field = M.get(fieldName1);                                                    
            Schema.DescribeFieldResult fieldDesc = field.getDescribe();                    
            //System.debug('+++++label'+fieldDesc.getLabel());
           // System.debug('+++++label'+fieldDesc.getname());
            mapfieldnames.put(fieldDesc.getname(),String.ValueOf(fieldDesc.gettype()));                                        
        }
    } //--End of Init Method

    // Get the Call List
    public CallList__c filter1()
    {        
        if(filter1== null)
        filter1= new CallList__c();
        return filter1;
    }

    //Doing Validation for all the picklist, textboxes in the VF page. --Start--
    public Boolean validate()
    {
        if(filter1.CampaignName__c== NULL && FieldName== null && FieldName1b == null && 
        FieldName2c == null && FieldName3d == null && 
        FieldName4e == null && Operatora == null && 
        Operator1b == null  && Operator2c == null && 
        Operator3d == null && Operator4e == null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter atleast one filter value'));
            return false;
        }
        if((FieldName != null && Operatora  == null) || (FieldName== null && Operatora != null) ||          
        (FieldName1b  != null && Operator1b == null) || (FieldName1b  == null && Operator1b != null) ||
        (FieldName2c != null && Operator2c == null) || (FieldName2c == null && Operator2c != null) ||
        (FieldName3d != null && Operator3d  == null) || (FieldName3d == null && Operator3d  != null) ||
        (FieldName4e  != null && Operator4e == null) || (FieldName4e  == null && Operator4e != null))
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select the operator'));
            return false;
        } 
         return true;
    } //--End of validate method --
   
    //Constructor of the class  --Start--
    public RemoveContacts() 
    {
        first=0;
        rows=10;
        bnNext = false;
        bnPrevious = false;
        bnFirst = false;
        bnLast = false;
        bnselect = false;
        calllist = new CallList__c();
        mapfieldnames = new Map<String,String>();
        mapfieldnames.put('--None--','--None--');
        mapfieldnames.put('Account.Name','STRING');
               
    } // --End of Constructor --

    
    //Getter and Setter for Viewname
   public String getViewname() {
      return Viewname;
    }
    public void setViewname(String Viewname) {
      this.Viewname = Viewname;
    }
    
    public List<SelectOption> getUserList() 
    {
        List<SelectOption> options1 = new List<SelectOption>();
        List<Profile> plist = new List<Profile>();
        List<Id> pid= new List<Id>();
         plist = [select id,name from Profile where name='Internal Sales' or name ='External Sales'];
         For(integer i=0;i<plist.size();i++)
             pid.add(plist[i].id);
           ulist= [select id, name from User where ProfileId in : pid and IsActive = true order By Name];
           for(integer i=0;i<ulist.size();i++)
           {
                options1.add(new SelectOption(ulist[i].id,ulist[i].name));
           }
           System.debug('options1----->'+options1);
            return options1;
    }
    
    public String[] getUsername() {
        return Username;
    }
    public void setUsername(String[] Username) {
        this.Username = Username;
    }

    //***Calling this method to initialize the selectlist with contact fields******
    public List<SelectOption> getFields()
    {
       List<SelectOption> fldoptions = new List<SelectOption>();
       Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
       Schema.SObjectType sobjType = gd.get('Contact');
       Schema.DescribeSObjectResult r = sobjType.getDescribe();
       Map<String,Schema.SObjectField> M = r.fields.getMap();
       System.debug('+++m'+M);
       fldoptions.add(new SelectOption('--None--','--None--'));
       fldoptions.add(new SelectOption('Account.Name','Account Name'));
       for(String fieldName1 : M.keyset())
       {
         Schema.SObjectField field = M.get(fieldName1);                                                    
         Schema.DescribeFieldResult fieldDesc = field.getDescribe();                    
         //System.debug('+++++label'+fieldDesc.getLabel());
        // System.debug('+++++label'+fieldDesc.getname());
         mapfieldnames.put(fieldDesc.getname(),String.ValueOf(fieldDesc.gettype()));
         fldoptions.add(new SelectOption(fieldDesc.getname(),fieldDesc.getlabel()));                                        
       }
       SelectOptionSorter.doSort(fldoptions, SelectOptionSorter.FieldToSort.Label);
       return fldoptions;
    }

    // Getter and Setter for FieldName
    public String getFieldName ()
    {
       return FieldName ;
    }        
    public void setFieldName (String FieldName )
    {
       this.FieldName = FieldName ;
    }
    
    // Getter and Setter for FieldName1,2,3,4   ---Start--
    public String getFieldname1b() 
    {
      return Fieldname1b;
    }
    public void setFieldname1b(String Fieldname1b) 
    {
      this.Fieldname1b = Fieldname1b;
    }

    public String getFieldname2c() 
    {
      return Fieldname2c;
    }
    public void setFieldname2c(String Fieldname2c) 
    {
      this.Fieldname2c = Fieldname2c;
    }

    public String getFieldname3d() 
    {
      return Fieldname3d;
    }
    public void setFieldname3d(String Fieldname3d) 
    {
      this.Fieldname3d = Fieldname3d;
    }

    public String getFieldname4e() 
    {
      return Fieldname4e;
    }
    public void setFieldname4e(String Fieldname4e) {
      this.Fieldname4e = Fieldname4e;
    }// ---End---

    // Getting Operators in the operator drop down box in VF page --- Start ---
    public List<SelectOption> getOperatorList() 
    {
         List<SelectOption> OperatorList = new List<SelectOption>();
         OperatorList=createOperatorList(FieldName );
         return OperatorList;
    }
     
    public List<SelectOption> getOperatorList1() 
    {
         List<SelectOption> OperatorList1 = new List<SelectOption>();
         OperatorList1=createOperatorList(Fieldname1b);
         return OperatorList1;  
    }
     
    public List<SelectOption> getOperatorList2() 
    {
         List<SelectOption> OperatorList2 = new List<SelectOption>();
         OperatorList2=createOperatorList(Fieldname2c);
         return OperatorList2;
    }

    public List<SelectOption> getOperatorList3() 
    {
         List<SelectOption> OperatorList3 = new List<SelectOption>();
         OperatorList3=createOperatorList(Fieldname3d);
         return OperatorList3;
    }

    public List<SelectOption> getOperatorList4() 
    {
         List<SelectOption> OperatorList4 = new List<SelectOption>();
         OperatorList4=createOperatorList(Fieldname4e);
         return OperatorList4;
    }
     
    public List<SelectOption> createOperatorList(String FieldAPI)
    {
        List<SelectOption> Oproptions = new List<SelectOption>();
        Oproptions.add(new SelectOption('', '--None--'));
        if((String.valueof(mapfieldnames.get(FieldAPI))) == 'BOOLEAN' )
        {
          Oproptions.add(new SelectOption('equals', 'equals'));
        }
        else{
        Oproptions.add(new SelectOption('not equal to', 'not equal to'));
        Oproptions.add(new SelectOption('equals', 'equals'));
        if((String.valueof(mapfieldnames.get(FieldAPI))) == 'STRING' || (String.valueof(mapfieldnames.get(FieldAPI))) == 'PICKLIST' || (String.valueof(mapfieldnames.get(FieldAPI))) == 'TEXTAREA' || (String.valueof(mapfieldnames.get(FieldAPI))) == 'PHONE' ||(String.valueof(mapfieldnames.get(FieldAPI))) == 'Email' )
        {  
           Oproptions.add(new SelectOption('starts with', 'starts with'));
           Oproptions.add(new SelectOption('contains', 'contains'));
           Oproptions.add(new SelectOption('does not contain', 'does not contain'));
        }
        else if((String.valueof(mapfieldnames.get(FieldAPI))) == 'INTEGER' || (String.valueof(mapfieldnames.get(FieldAPI)))== 'CURRENCY' || (String.valueof(mapfieldnames.get(FieldAPI)))== 'DOUBLE' || (String.valueof(mapfieldnames.get(FieldAPI))) == 'PERCENT'|| (String.valueof(mapfieldnames.get(FieldAPI))) == 'DATE' ||(String.valueof(mapfieldnames.get(FieldAPI)))== 'DATETIME')    
        {
          Oproptions.add(new SelectOption('less than', 'less than'));
          Oproptions.add(new SelectOption('greater than', 'greater than'));
          Oproptions.add(new SelectOption('less or equal', 'less or equal'));
          Oproptions.add(new SelectOption('greater or equal', 'greater or equal'));
        }    
        else if((String.valueof(mapfieldnames.get(FieldAPI))) == 'MULTIPICKLIST' )
        {
          Oproptions.add(new SelectOption('includes', 'includes'));
          Oproptions.add(new SelectOption('excludes', 'excludes'));
        }
        }          
        return Oproptions;
    }
     
    public String getOperatora() 
    {
      return Operatora;
    }
    public void setOperatora(String Operatora) 
    {
      this.Operatora = Operatora;
    }
     
    public String getOperator1b() 
    {
      return Operator1b;
    }
    public void setOperator1b(String Operator1b) 
    {
      this.Operator1b = Operator1b;
    }

    public String getOperator2c() 
    {
      return Operator2c;
    }
    public void setOperator2c(String Operator2c) 
    {
      this.Operator2c = Operator2c;
    }

    public String getOperator3d() 
    {
      return Operator3d;
    }
    public void setOperator3d(String Operator3d) 
    {
      this.Operator3d = Operator3d;
    }

    public String getOperator4e() 
    {
      return Operator4e;
    }
    public void setOperator4e(String Operator4e) 
    {
      this.Operator4e = Operator4e;
    } // ---End---

    public String getValuea() 
    {
      return valuea;
    }
    public void setValuea(String valuea) 
    {
      this.valuea = valuea;
    }
        
    Public void Go()
    {
       System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!Entering Go method!!!!!!!!!!!!!!!!!!!!!!!!!');
       removecontactids=new list<id>();  
       Viewname = (String)JSON.deserialize( Apexpages.currentPage().getParameters().get('Viewname'), string.class ) ;     
       System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!Entering Go method!!!!!!!!!!!!!!!!!!!!!!!!!'+Viewname);       
       //Viewname      
       //Viewname = ApexPages.currentPage().getParameters().get('vname');
       list<Contact_Call_List__c>contactcalllist=[select id, name,CallList_Name__c,Owner__c,Contact__c,Call_Complete__c,CallListOrder__r.IsDeleted__c  from Contact_Call_List__c where CallList_Name__c=:Viewname and Owner__c=:UserNameId and Call_Complete__c=false and CallListOrder__r.IsDeleted__c=false and CallListOrder__r.Archived__c=false and IsDeleted__c=false limit 1000 ];
       system.debug('Check the viewName.....'+ Viewname);
       system.debug('Check the records in the list...'+ contactcalllist.size());
       for(Contact_Call_List__c crt: contactcalllist)
       removecontactids.add(crt.Contact__c);
      
       system.debug('Check the ids where I am getting the correct or not.....'+ removecontactids);
              
        String condn1='';
        String condn2='';
        String condn3='';
        String condn4='';
        String condn5='';
        String conquery='';
        List<User> userlist = new List<User>();
        list<Contact_Call_List__c> contactlist = new list<Contact_Call_List__c>();     
        filter1 =new CallList__c();
       //filter1 = [select id,Name,CampaignName__c,CampaignName__r.id,Contact_Type__c,Field_API__c, Field_API1__c, Field_API2__c,Field_API3__c,Field_API4__c,Operator__c,Operator1__c,Operator2__c,Operator3__c,Operator4__c,Value__c,Value1__c,Value2__c,Value3__c,Value4__c from  CallList__c where name=:viewName];
        system.debug('Welcome to Go method this is to disply contacts................');                    
        try
        {           
            conquery='select id,name,Account.name,Territory__c,Phone  from Contact';
            System.debug('++++fdname'+FieldName);
            
            if(FieldName!=null && FieldName!='' && Operatora!=null && Operatora!='' && valuea!=null && valuea!='' )
            condn1=generateQuery.conditionquery(FieldName,Operatora,valuea);
            System.debug('Check the condition....&&&condn1--->'+condn1);
          
            if(FieldName1b!=null && FieldName1b!='' && Operator1b!=null && Operator1b!='' && Value1b!=null && Value1b!='' )
            condn2=generateQuery.conditionquery(FieldName1b,Operator1b,Value1b);
             System.debug('Check the condition....&&&condn1--->'+condn2);
          
            if(FieldName2c!=null && FieldName2c!='' && Operator2c!=null && Operator2c!='' && Value2c!=null && Value2c!='' )
            condn3=generateQuery.conditionquery(FieldName2c,Operator2c,Value2c);
            System.debug('Check the condition....&&&condn1--->'+condn3);
           
            if(FieldName3d!=null && FieldName3d!='' && Operator3d!=null && Operator3d!='' && Value3d!=null && Value3d!='' )
            condn4=generateQuery.conditionquery(FieldName3d,Operator3d,Value3d);
            system.debug('Check the condition....&&&condn1--->'+condn4);
            
            if(FieldName4e!=null && FieldName4e!='' && Operator4e!=null && Operator4e!='' && Value4e!=null && Value4e!='' )           
            condn5=generateQuery.conditionquery(FieldName4e,Operator4e,Value4e);
            system.debug('Check the condition....&&&condn1--->'+condn5);
           
            If(condn1 != null && condn1!='')  
            conquery=conquery+' where '+ condn1;
            system.debug('Lets check the Conquery,Ninad.....'+conquery);
            
            if(condn2 != null && condn2 !='')
            conquery=conquery+' AND '+ condn2;
            system.debug('Lets check the Conquery,Ninad.....'+conquery);
            
            if( condn3 != null && condn3!='' )
            conquery=conquery+' AND '+condn3;
            System.debug('******if3:'+conquery);
               
           if(condn4 != null && condn4  !='' )
           conquery=conquery+' AND '+condn4;
           system.debug('check the conquery....'+conquery);
             
           if(condn5 != null && condn5 !='' )
           conquery=conquery+' AND '+condn5;  
           system.debug('check the conquery....'+conquery);    
            
           
                String str = '';
                String glue = '(';
                for(integer i=0;i<removecontactids.size();i++)
                {
                  str = str + glue + '\''+removecontactids[i]+'\'';
                  glue = ',';
                }
                if(removecontactids.size()>0)
                {
                   str += ')';
                   if(FieldName!=null && FieldName!='' && Operatora!=null && Operatora!='' && valuea!=null && valuea!='' )
                     conquery= conquery+' AND '+ 'id in '+ str;
                   else
                     conquery= conquery+' where '+ 'id in'+ str;
                
                System.debug(',,,,,,,,,,campaign query'+conquery);
      
                     
            System.debug('++++query:'+conquery);
            conList = new List<Contact>(); 
            conList.clear();
            ssc = new ApexPages.StandardSetController(Database.getQueryLocator(conquery));
            ssc.setPageSize(10);
            for(sObject sObj: ssc.getRecords())
            {
              conList.add((Contact)sObj);
            }
            system.debug('Chimb Pavasan ran jhaala abadani.....'+conList.size());
            cmCount = conList.size();
           
        }
        else if(removecontactids.size()==0)
        {
          String Name1='Ravan';
          conquery='select id,name,Account.name,Territory__c,Phone  from Contact where name=:Name1';
           conList = new List<Contact>();            
            ssc = new ApexPages.StandardSetController(Database.getQueryLocator(conquery));
            ssc.setPageSize(10);
            for(sObject sObj: ssc.getRecords())
            {
              conList.add((Contact)sObj);
            }
            system.debug('Chimb Pavasan ran jhaala abadani.....'+conList.size());
            cmCount = conList.size();
          
        
        }
      }
              
        
        catch(exception e){
           System.debug('Exception in ----Create CallList------'+e.getMessage());
        }
        }
         public void Next(){
         system.debug('Next conList');
         conList= new List<Contact>();
         ssc.next();
         //ssc.getHasNext();
         for(sObject sObj: ssc.getRecords()){
            conList.add((Contact)sObj);
         }
         system.debug('Next conList'+conList);
        }
        public void Previous(){
         conList= new List<Contact>();
         ssc.previous();
         //ssc.getHasprevious();
         for(sObject sObj: ssc.getRecords()){
            conList.add((Contact)sObj);
         }
         system.debug('Previous conList'+conList);
        }   
                
        public String UserNameId {set;get;}
        
        public Pagereference RetrieveCallList()
        {
            System.debug('Entering Retrieve Method--->'+UserNameId);
            
            try
            {
             // This is the List of wholesaler whose name was selected in the dropdown Select User
          // User wholesaler = [ Select Id, Name from User where Name = :UserNameId limit 1];      
           
            set<String> call = new Set<String>();    
            List<String> lscall = new List<String>();
            List<Contact_Call_List__c> clist = [Select id ,CallListOrder__c , Owner__c,CallList_Name__c,Call_Complete__c,CallListOrder__r.IsDeleted__c from Contact_Call_List__c where Owner__c =:UserNameId and Call_Complete__c = false and CallListOrder__r.IsDeleted__c=false and CallListOrder__r.Archived__c=false];//wholesaler.Id];
            System.debug('&&&&&&clist' + clist);
            for(Contact_Call_List__c t : clist)
            call.add(t.CallList_Name__c); 
            call.add('--None--');
            ListViews = new  List<SelectOption>();
            lscall.AddAll(call);
            lscall.sort();
            for(String s : lscall){                
                ListViews.add(new SelectOption(s,s));
            }
            if(ListViews.size()==0)
               ListViews.add(new SelectOption('--None--','--None--'));
               
               return null;
        }catch(Exception e){ System.debug('Exception-----'+e);return null;} 
        
        }
        public String selCallList {set;get;}
        public list<SkippedContacts__c> SkipConlist=new list<SkippedContacts__c>();
        public PageReference SelectCallList() 
        {
        
        System.debug('selCallList ---->'+selCallList);
        Contact_Call_List__c[] concall=  DataBase.query('select Id,Contact__c,CallListOrder__r.id,CallList_Name__c,Contact__r.name from Contact_Call_List__c where Owner__c =:UserNameId and CallList_Name__c=:Viewname  and Contact__c IN'+selCallList);
        Database.DeleteResult[] DR_Dels = Database.delete(concall);
        string  userid=userinfo.getUserid();
        User activeuser1=[select name from User where id=:userid ];
        string name= activeuser1.name;    
        for(Contact_Call_List__c ccl : concall){
            Integer rcon=0;
            for(Contact c : conList){
                if(ccl.Contact__c == c.Id){
                   skippedContacts__c Skipped= new SkippedContacts__c();
                   Skipped.ContactName__c=ccl.Contact__r.name;
                   Skipped.Reason_for_Delete__c='Removed BY Admin';
                   Skipped.CallListName__c=ccl.CallList_Name__c;
                   Skipped.CallListByid__c=ccl.CallListOrder__r.id;
                   Skipped.ContactID__c=ccl.Contact__c;
                   Skipped.SkippedById__c=Userinfo.getuserid();
                   Skipped.Skiped_By__c=name;
                   Skipped.recordtypeid='012500000005TMC';
                   Skipped.SkippedForwholeSaler__c=UserNameId; 
                   Skipped.DeletedDate__c=system.today();
                   insert Skipped;
                  // SkipConlist.add(Skipped);
                    conList.remove(rcon);
                    break;
                }
                rcon = rcon+1;   
            }          
        }
        return null;
        }*/
}