@isTest
public class CallListGeneratortestclass{
/*
static testMethod void  CallListGeneratortestmethod() {

User u=[select id,name,Territory__c,email from user where id=: UserInfo.getUserId()];
u.Territory__c= 'Florida';
update u;
String[] ulist = new String[]{u.name};

Contact c= new Contact();
c.firstname='test';
c.lastname='test last';
c.Wholesaler__c=u.name;
c.Territory__c='Florida';
c.REIT_Investments_Count__c=4545;
c.Bonds__c=true;
c.Date_of_Birth__c=system.today();


insert c;

Campaign cn = new Campaign();
cn.name='campaignname';
insert cn;

CampaignMember  cm= new CampaignMember();
cm.Campaignid = cn.id;
cm.Contactid = c.id;
insert cm;

Task t = new Task();
//t.Whatid= case1.id;
t.status='Not Started';
t.Priority='Normal';
t.Whoid=c.id;
t.Ownerid=u.id;
t.subject='SalesCockpit';
insert t;

Call_List__c call = new Call_List__c();
call.name='callistname';
call.CampaignName__c=cn.id;
call.Contact_Type__c='type';
call.Field_API__c='Firstname';
call.Field_API1__c='REIT_Investments_Count__c';
call.Field_API2__c='Bonds__c';
call.Field_API3__c='Date_of_Birth__c';
call.Field_API4__c='Lastname';

call.Operator__c='starts with';
call.Operator1__c='greater than';
call.Operator2__c='equals';
call.Operator3__c='equals';
call.Operator4__c='starts with';

call.Value__c='t';
call.Value1__c='0';
call.Value2__c='true';
call.Value3__c='11/17/2011';
call.Value4__c='t';
insert call;



Apexpages.currentPage().getParameters().put('vname','callistname');
CalllistGeneratorCls cls= new CalllistGeneratorCls();

cls.init();
cls.validate();
cls.save1();
cls.deletecall();
cls.preview();
cls.renderdatetext();
//cls.fetchContact();
cls.previewredirect();
cls.disableLinks();
cls.Next();
cls.Previous();
cls.getFirstRows();
cls.getLastRows();
cls.getCallList();

//Apexpages.currentPage().getParameters().put('vname','callistname');

cls.getViewname();
cls.setViewname('callistname');

cls.getContactOption();
cls.setContactOption('ContactOption');
cls.getSearchString();
cls.setSearchString('SearchString');
cls.getListViews();
cls.search();
cls.getFormTag();
cls.getTextBox();
cls.getFields();
cls.getFieldname();
cls.setFieldname('Firstname');

cls.getFieldname1();
cls.setFieldname1('REIT_Investments_Count__c');
cls.getFieldname2();
cls.setFieldname2('Bonds__c');
cls.getFieldname3();
cls.setFieldname3('Date_of_Birth__c');
cls.getFieldname4();
cls.setFieldname4('Lastname');
cls.getOperatorList();
cls.getOperatorList1();
cls.getOperatorList2();
cls.getOperatorList3();
cls.getOperatorList4();
cls.getOperator();
cls.setOperator('starts with');
cls.getOperator1();
cls.setOperator1('equals');
cls.getOperator2();
cls.setOperator2('not equal to');
cls.getOperator3();
cls.setOperator3('greater than');
cls.getOperator4();
cls.setOperator4('equals');

cls.getUserList();
cls.getUsername();

cls.setUsername(ulist);

cls.Assign();
//setUsername(String[] Username)
cls.getOwner();
cls.conditionquery('FieldName','Operator','Value');
cls.cancel();
cls.canceltocockpitpage();
  
}
static testMethod void  CallListGeneratortestmethod1() {

User u=[select id,name,Territory__c,email from user where id=: UserInfo.getUserId()];
u.Territory__c= 'Florida';
update u;
String[] ulist = new String[]{u.name};

Contact c= new Contact();
c.firstname='test';
c.lastname='test last';
c.Wholesaler__c=u.name;
c.Territory__c='Florida';
c.REIT_Investments_Count__c=4545;
c.Bonds__c=true;
c.Date_of_Birth__c=system.today();

insert c;

Campaign cn = new Campaign();
cn.name='campaignname';
insert cn;

CampaignMember  cm= new CampaignMember();
cm.Campaignid = cn.id;
cm.Contactid = c.id;
insert cm;

Task t = new Task();
//t.Whatid= case1.id;
t.status='Not Started';
t.Priority='Normal';
t.Whoid=c.id;
t.Ownerid=u.id;
insert t;

Call_List__c call1 = new Call_List__c();
call1.name='callistname1';
//call.CampaignName__c=cn.id;
call1.Contact_Type__c='My Contacts';
call1.Field_API__c='Firstname';
call1.Field_API1__c='REIT_Investments_Count__c';
call1.Field_API2__c='Bonds__c';
call1.Field_API3__c='Date_of_Birth__c';
call1.Field_API4__c='CreatedDate';

call1.Operator__c='starts with';
call1.Operator1__c='greater than';
call1.Operator2__c='equals';
call1.Operator3__c='equals';
call1.Operator4__c='equals';

call1.Value__c='t';
call1.Value1__c='0';
call1.Value2__c='true';
call1.Value3__c='11/17/2011';
call1.Value4__c='2/28/2012 7:42 AM';
insert call1;


Apexpages.currentPage().getParameters().put('vname','callistname1');

CalllistGeneratorCls cls= new CalllistGeneratorCls();

cls.init();
cls.validate();
cls.save1();
cls.deletecall();
cls.search();
cls.preview();
cls.renderdatetext();
//cls.fetchContact();
cls.previewredirect();
cls.disableLinks();
cls.Next();
cls.Previous();
cls.getFirstRows();
cls.getLastRows();
cls.getCallList();
cls.getListViews();
cls.getViewname();
cls.setViewname('callistname1');

cls.getContactOption();
cls.setContactOption('ContactOption');
cls.getSearchString();
cls.setSearchString('SearchString');
cls.getFormTag();
cls.getTextBox();
cls.getFields();
cls.getFieldname();
cls.setFieldname('Firstname');

cls.getFieldname1();
cls.setFieldname1('REIT_Investments_Count__c');
cls.getFieldname2();
cls.setFieldname2('Bonds__c');
cls.getFieldname3();
cls.setFieldname3('Date_of_Birth__c');
cls.getFieldname4();
cls.setFieldname4('Lastname');
cls.getOperatorList();
cls.getOperatorList1();
cls.getOperatorList2();
cls.getOperatorList3();
cls.getOperatorList4();
cls.getOperator();
cls.setOperator('Operator');
cls.getOperator1();
cls.setOperator1('Operator1');
cls.getOperator2();
cls.setOperator2('Operator2');
cls.getOperator3();
cls.setOperator3('Operator3');
cls.getOperator4();
cls.setOperator4('Operator4');
cls.getUserList();
cls.getUsername();

cls.setUsername(ulist);

cls.Assign();
//setUsername(String[] Username)
cls.getOwner();
cls.conditionquery('FieldName','Operator','Value');
cls.cancel();
cls.canceltocockpitpage();


  
}
static testMethod void  CallListGeneratortestmethod2() {

User u=[select id,name,Territory__c,email from user where id=: UserInfo.getUserId()];
u.Territory__c= 'Heartland';
update u;
String[] ulist = new String[]{u.name};

Contact c= new Contact();
c.firstname='test1';
c.lastname='test last2';
c.Wholesaler__c=u.name;
c.Territory__c='Heartland';
c.REIT_Investments_Count__c=4545;
c.Bonds__c=true;
c.Date_of_Birth__c=system.today();

insert c;

Campaign cn = new Campaign();
cn.name='campaignname';
insert cn;

CampaignMember  cm= new CampaignMember();
cm.Campaignid = cn.id;
cm.Contactid = c.id;
insert cm;

Task t = new Task();
//t.Whatid= case1.id;
t.status='Not Started';
t.Priority='Normal';
t.Whoid=c.id;
t.Ownerid=u.id;
insert t;

Call_List__c call1 = new Call_List__c();
call1.name='callistname1';
//call.CampaignName__c=cn.id;
call1.Contact_Type__c='My Contacts';
call1.Field_API__c='CreatedDate';
call1.Field_API1__c='CreatedDate';
call1.Field_API2__c='CreatedDate';
call1.Field_API3__c='Date_of_Birth__c';
call1.Field_API4__c='CreatedDate';

call1.Operator__c='less than';
call1.Operator1__c='greater or equal';
call1.Operator2__c='less or equal';
call1.Operator3__c='not equal to';
call1.Operator4__c='greater than';

call1.Value__c='2/28/2012 7:42 AM';
call1.Value1__c='2/28/2012 7:42 AM';
call1.Value2__c='2/28/2012 7:42 AM';
call1.Value3__c='11/17/2011';
call1.Value4__c='2/28/2012 7:42 AM';
insert call1;


Apexpages.currentPage().getParameters().put('vname','callistname1');

CalllistGeneratorCls cls= new CalllistGeneratorCls();

cls.init();
cls.validate();
cls.save1();
cls.deletecall();
cls.search();
cls.preview();
cls.renderdatetext();
//cls.fetchContact();
cls.previewredirect();
cls.disableLinks();
cls.Next();
cls.Previous();
cls.getFirstRows();
cls.getLastRows();
cls.getCallList();
cls.getListViews();
cls.getViewname();
cls.setViewname('callistname1');

cls.getContactOption();
cls.setContactOption('ContactOption');
cls.getSearchString();
cls.setSearchString('SearchString');
cls.getFormTag();
cls.getTextBox();
cls.getFields();
cls.getFieldname();
cls.setFieldname('Firstname');

cls.getFieldname1();
cls.setFieldname1('REIT_Investments_Count__c');
cls.getFieldname2();
cls.setFieldname2('Bonds__c');
cls.getFieldname3();
cls.setFieldname3('Date_of_Birth__c');
cls.getFieldname4();
cls.setFieldname4('Lastname');
cls.getOperatorList();
cls.getOperatorList1();
cls.getOperatorList2();
cls.getOperatorList3();
cls.getOperatorList4();
cls.getOperator();
cls.setOperator('Operator');
cls.getOperator1();
cls.setOperator1('Operator1');
cls.getOperator2();
cls.setOperator2('Operator2');
cls.getOperator3();
cls.setOperator3('Operator3');
cls.getOperator4();
cls.setOperator4('Operator4');
cls.getUserList();
cls.getUsername();

cls.setUsername(ulist);

cls.Assign();
//setUsername(String[] Username)
cls.getOwner();
cls.conditionquery('FieldName','Operator','Value');
cls.cancel();
cls.canceltocockpitpage();


  
}
static testMethod void  CallListGeneratortestmethod3() {

User u=[select id,name,Territory__c,email from user where id=: UserInfo.getUserId()];
u.Territory__c= 'Southern';
update u;
String[] ulist = new String[]{u.name};

Contact c= new Contact();
c.firstname='test1';
c.lastname='test last2';
c.Wholesaler__c=u.name;
c.Territory__c='Southern';
c.REIT_Investments_Count__c=4545;
c.Bonds__c=true;
c.Date_of_Birth__c=system.today();

insert c;

Campaign cn = new Campaign();
cn.name='campaignname';
insert cn;

CampaignMember  cm= new CampaignMember();
cm.Campaignid = cn.id;
cm.Contactid = c.id;
insert cm;

Task t = new Task();
//t.Whatid= case1.id;
t.status='Not Started';
t.Priority='Normal';
t.Whoid=c.id;
t.Ownerid=u.id;
insert t;

Call_List__c call1 = new Call_List__c();
call1.name='callistname1';
//call.CampaignName__c=cn.id;
call1.Contact_Type__c='Campaign';
call1.Field_API__c='License__c';
call1.Field_API1__c='License__c';
call1.Field_API2__c='License__c';
call1.Field_API3__c='License__c';
call1.Field_API4__c='CreatedDate';

call1.Operator__c='includes';
call1.Operator1__c='excludes';
call1.Operator2__c='includes';
call1.Operator3__c='excludes';
call1.Operator4__c='greater than';

call1.Value__c='6;7';
call1.Value1__c='7;26';
call1.Value2__c='6,7';
call1.Value3__c='7,26';
call1.Value4__c='2/28/2012 7:42 AM';
insert call1;


Apexpages.currentPage().getParameters().put('vname','callistname1');

CalllistGeneratorCls cls= new CalllistGeneratorCls();

cls.init();
cls.validate();
cls.save1();
cls.deletecall();
cls.search();
cls.preview();
cls.renderdatetext();
//cls.fetchContact();
//cls.previewredirect();
cls.disableLinks();
cls.Next();
cls.Previous();
cls.getFirstRows();
cls.getLastRows();
cls.getCallList();
cls.getListViews();
cls.getViewname();
cls.setViewname('callistname1');

cls.getContactOption();
cls.setContactOption('ContactOption');
cls.getSearchString();
cls.setSearchString('SearchString');
cls.getFormTag();
cls.getTextBox();
cls.getFields();
cls.getFieldname();
cls.setFieldname('Firstname');

cls.getFieldname1();
cls.setFieldname1('REIT_Investments_Count__c');
cls.getFieldname2();
cls.setFieldname2('Bonds__c');
cls.getFieldname3();
cls.setFieldname3('Date_of_Birth__c');
cls.getFieldname4();
cls.setFieldname4('Lastname');
cls.getOperatorList();
cls.getOperatorList1();
cls.getOperatorList2();
cls.getOperatorList3();
cls.getOperatorList4();
cls.getOperator();
cls.setOperator('Operator');
cls.getOperator1();
cls.setOperator1('Operator1');
cls.getOperator2();
cls.setOperator2('Operator2');
cls.getOperator3();
cls.setOperator3('Operator3');
cls.getOperator4();
cls.setOperator4('Operator4');
cls.getUserList();
cls.getUsername();

cls.setUsername(ulist);

cls.Assign();
//setUsername(String[] Username)
cls.getOwner();
cls.conditionquery('FieldName','Operator','Value');
cls.cancel();
cls.canceltocockpitpage();


  
}*/
}