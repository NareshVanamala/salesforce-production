@isTest
private class Test_BDApprovalupdate 
{
static testmethod void test() 
{
 Id rrrecordtype = [select Id, Name,DeveloperName from RecordType where DeveloperName ='NonInvestorRepContact' and SobjectType = 'Contact'].id;
 list<BDApprovalHelper__c> blist = new list<BDApprovalHelper__c>();

  Account a = new Account();
       a.name = 'Test';
       insert a; 
      
  Contact c1 = new Contact();
       c1.lastname = 'Cockpit';
       c1.firstname='test';
       c1.Accountid = a.id;
       c1.Relationship__c ='Accounting';
       c1.Contact_Type__c = 'Cole';
       c1.Wholesaler_Management__c = 'Producer A';
       c1.Territory__c ='Chicago';
       c1.RIA_Territory__c ='Southeast Region';
       c1.RIA_Consultant_Picklist__c ='Brian Mackin';
       c1.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c1.Wholesaler__c='Andrew Garten';
       c1.Regional_Territory_Manager__c ='Andrew Garten';
       c1.Internal_Wholesaler__c ='Keith Severson';
       c1.Territory_Zone__c = 'NV-50';
       c1.Next_Planned_External_Appointment__c = system.now();
       c1.recordtypeid = rrrecordtype;
       c1.mailingstate='OH';
       c1.email='sandeep.mariyala@polarisft.com';
       insert c1;  
       
       
       Campaign cam = new Campaign();
       cam.name='Dinearound-National Conference-Cole Capital'; 
       cam.status='Pending'; 
       cam.status='Approved'; 
       cam.status='Rejected'; 
       cam.isActive=true;
       cam.parentid=null;
       insert cam;    
       
      CampaignMember member1= new CampaignMember();
       member1.CampaignId = cam.id;
        member1.ContactId = c1.id; 
         member1.Status='Approved';
         insert member1;
       
       
       
  for(integer i=0;i<5;i++)
  {     
  BDApprovalHelper__c b=new BDApprovalHelper__c();
  b.name='test';
  b.contact_name__c='test Cockpit';
  b.account_name__c='';
  b.CampaignMemeberId__c=member1.id;
  blist.add(b);
  }
  insert blist;
  
    Test.startTest();
    BDApprvalupdate np = new BDApprvalupdate();
    Database.BatchableContext bc;
    
    np.start(bc);
    np.execute(bc,blist);
    np.finish(bc);
    Test.stopTest();     
}
}