@isTest
private class RC_New_CertificateButtonClassTest
{
    static testmethod void test1() 
    {
       RC_Certificate_of_Insurance__c mp=new RC_Certificate_of_Insurance__c();
       RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'RC_Certificate_of_Insurance__c' and DeveloperName = 'COI_Entry']; 
       mp.RecordTypeId=NewRecType.id;
       String Url=System.Label.Current_Org_Url+'/a5C/e?nooverride=1&retURL=%2Fa5C%2Fo&RecordType='+mp.RecordTypeId+'&ent=01IR00000005';
       PageReference acctPage = new PageReference(Url);
       Test.setCurrentPage(acctPage); 
       
       ApexPages.StandardController controller = new ApexPages.StandardController(mp);
       RC_New_CertificateButtonClass csr=new RC_New_CertificateButtonClass(controller);
      
       csr.Gobacktostandardpage();
       
            
        RC_Property__c PRT=new RC_Property__c();
         PRT.name='Amazon DC 2014 USAA~A11123';
         PRT.City__c='Hyderabad';
         insert PRT;
         
         Account a= new Account();
         a.name='TestAccount';
         insert a;
        

         RC_Location__c LC=new RC_Location__c();
         LC.name='Amazon DC 2014 USAA~A11123';
         LC.Entity_Tenant_Code__c='12345';
         //LC.City__c='Hyderabad';
         LC.Property__c=PRT.id;
         LC.Account__c=a.id;
         insert LC;
         System.assertEquals(PRT.name,LC.name);
 
         string bid=LC.id;
         string bid1=LC.Name;
         string accountid=LC.Account__c;
         String accountName=LC.Account__r.name;
         //accountName=accountName.replaceAll('&','%26');
         String s2 = bid1.replaceAll('&', '%26');
          RC_Certificate_of_Insurance__c ct1=new RC_Certificate_of_Insurance__c();
          ct1.RecordTypeId=NewRecType.id;
          
         String Url1=System.Label.Current_Org_Url+'/a5C/e?nooverride=1&CF00NR0000001JRQf_lkid='+bid+'&CF00NR0000001JRQf='+s2+'&CF00NR0000001JRNi_lkold='+accountid+'&CF00NR0000001JRNi='+accountName+'&retURL=%2F'+LC.id+'&RecordType='+ct1.RecordTypeId+'&ent=01IR00000005onJ';
  
       
        ApexPages.StandardController controller1 = new ApexPages.StandardController(ct1);
       RC_New_CertificateButtonClass csr1=new RC_New_CertificateButtonClass(controller1);
       csr1.ppc=LC;
       csr1.mp=ct1;
        PageReference acctPage1 = new PageReference(Url1);
       Test.setCurrentPage(acctPage1); 
       csr1.mp.Location__c=LC.id;
       csr1.Gobacktostandardpage();
    
    
    
    }
    
    
    
}