public with sharing class NewhireNdcontroller{

    public String quoteId1{get;set;} 
    public String myValue{get;set;}
    public list<Asset__c>lst{get;set;}
    public Employee__c emp{get;set;}
   
   
   
      public list<Asset__c> FINRALicensedlist{get{
        lst = new list<Asset__c>();
        system.debug('-------------NewhireNdcontroller-------------->'+quoteId1);
        lst=[select id,name,Access_Type__c,Access_Name__c,Asset_Inventory__c,Updating__c,Application_Owner__c,Application_Implementor__c,Asset_Status__c,Employee__c,Removed_On__c,Added_On__c from Asset__c where Employee__c=:quoteId1 and Access_Type__c = 'FINRA License' limit 1000];
        FINRALicensedlist= lst;
        return FINRALicensedlist;
    } set;
    }
     
   
        public list<Asset__c> buildingTypelist{get{
        lst = new list<Asset__c>();
        system.debug('-------------NewhireNdcontroller-------------->'+quoteId1);
        lst=[select id,name,Access_Type__c,Access_Name__c,Asset_Inventory__c,Updating__c,Application_Owner__c,Application_Implementor__c,Asset_Status__c,Employee__c,Removed_On__c,Added_On__c from Asset__c where Employee__c=:quoteId1 and Access_Type__c = 'Building' limit 1000];
        buildingTypelist = lst;
        return buildingTypelist;
    } set;
    }
    public list<Asset__c> ParkingTypelist{get{
        lst = new list<Asset__c>();
        lst=[select id,name,Access_Type__c,Access_Name__c,Asset_Inventory__c,Updating__c,Application_Owner__c,Application_Implementor__c,Asset_Status__c,Employee__c,Removed_On__c,Added_On__c from Asset__c where Employee__c=:quoteId1 and Access_Type__c = 'Parking' limit 1000];
        ParkingTypelist = lst;
        return ParkingTypelist;
    } set;
    }
    public list<Asset__c>HardwareTypelist{get{
        lst = new list<Asset__c>();
        lst=[select id,name,Access_Type__c,Access_Name__c,Asset_Inventory__c,Updating__c,Application_Owner__c,Application_Implementor__c,Asset_Status__c,Employee__c,Removed_On__c,Added_On__c from Asset__c where Employee__c=:quoteId1 and Access_Type__c = 'Hardware' limit 1000];
        HardwareTypelist = lst;
        return HardwareTypelist;
    }set;
    }
    public list<Asset__c>softwareTypelist{get{
        lst = new list<Asset__c>();
        lst =[select Additional_Instructions_Requests1__c, Previous_Options1__c, Additional_Instructions_Requests__c,id,name,Access_Type__c,Additional_Description__c,Access_Name__c,Asset_Inventory__c,Updating__c,Application_Owner__c,Application_Implementor__c,Asset_Status__c,Employee__c,Removed_On__c,Added_On__c,Asset_Inventory__r.Additional_Descirption_Required__c from Asset__c where Employee__c=:quoteId1 and Access_Type__c = 'Software' limit 1000];
        softwareTypelist = lst;
        return softwareTypelist;
    }set;
    }
    public list<Asset__c>ActiveDirectoryTypelist{get{
        lst = new list<Asset__c>();
        lst =[select id,name,Access_Type__c,ActiveDirectoryGroup__c,ActiveDirectorygroupformula__c,Access_Name__c,Asset_Inventory__c,Updating__c,Application_Owner__c,Application_Implementor__c,Asset_Status__c,Employee__c,Removed_On__c,Added_On__c from Asset__c where Employee__c=:quoteId1 and Access_Type__c = 'Active Directory' and Access_Name__c!='Email Account' limit 1000];
        ActiveDirectoryTypelist = lst;
        return ActiveDirectoryTypelist;
    }set;
    }
    public list<Asset__c>EmailAccountlist{get{
        lst = new list<Asset__c>();
        lst =[select id,name,Access_Type__c,ActiveDirectoryGroup__c,ActiveDirectorygroupformula__c,Access_Name__c,Asset_Inventory__c,Updating__c,Application_Owner__c,Application_Implementor__c,Asset_Status__c,Employee__c,Removed_On__c,Added_On__c from Asset__c where Employee__c=:quoteId1 and Access_Type__c = 'Active Directory' and Access_Name__c='Email Account' limit 1000];
        EmailAccountlist= lst;
        return EmailAccountlist;
    }set;
    }
    
    
}