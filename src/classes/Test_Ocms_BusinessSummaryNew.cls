@isTest(seeAlldata=true)
public class Test_Ocms_BusinessSummaryNew
{
    @isTest public static void Test_Ocms_BusinessSummaryNew() 
    { 
        
      profile pf=[select id,name from profile where name='Cole Capital Community'];
        account a=new account();
        a.name='test';
        insert a;
        
        Contact c1= new Contact();
        c1.firstName='Ninad';
        c1.lastName='Tambe';
        c1.accountid=a.id;
        c1.email='test123@noemail.com';
        insert c1;
        
        system.assertequals(c1.accountid,a.id);

        User u = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = pf.id, country='United States',IsActive =true,
                ContactId = c1.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert u;
         
        system.runas(u)
        {
         REIT_Investment__c rc= new REIT_Investment__c();
         rc.Current_Capital__c=346677;
         rc.Investor_Contact__c=c1.id;
         rc.Rep_Contact__c=c1.id;
         rc.Fund__c='3770';
         insert rc;
         
          REIT_Investment__c rcc= new REIT_Investment__c();
         rcc.Current_Capital__c=346677;
         rcc.Investor_Contact__c=c1.id;
         rcc.Rep_Contact__c=c1.id;
         rcc.Fund__c='3776';
         insert rcc;
         
         REIT_Investment__c rc2= new REIT_Investment__c();
         rc2.Current_Capital__c=346677;
         rc2.Investor_Contact__c=c1.id;
         rc2.Rep_Contact__c=c1.id;
         rc2.Fund__c='3777';
         insert rc2;
         
         REIT_Investment__c rc12= new REIT_Investment__c();
         rc12.Current_Capital__c=346677;
         rc12.Investor_Contact__c=c1.id;
         rc12.Rep_Contact__c=c1.id;
         rc12.Fund__c='3775';
         insert rc12;
         
         system.assertequals(rc12.Rep_Contact__c,c1.id);

         
         update c1;
         
         
         
        Ocms_BusinessSummaryNew rc1=new Ocms_BusinessSummaryNew();
        try
        {
           rc1.getProductList(); 
         //  rc1.getProductList1();
           rc1.writeControls();
           rc1.writeMapView();
            
           rc1.getHtml();
           rc1.writeListView(); 
        //   rc1.writeListView1(); 
        }
          catch(Exception e)  {        }    
         
       }  
         
     }
     @isTest public static void Test_Ocms_BusinessSummaryNew1() 
    { 
        
      profile pf=[select id,name from profile where name='Cole Capital Community'];
        account a=new account();
        a.name='test';
        insert a;
        
        Contact c1= new Contact();
        c1.firstName='Ninad';
        c1.lastName='Tambe';
        c1.accountid=a.id;
        c1.email='test123@noemail.com';
        insert c1;
        
        system.assertequals(c1.accountid,a.id);

        User u = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = pf.id, country='United States',IsActive =true,
                ContactId = c1.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert u;
         
        system.runas(u)
        {
         REIT_Investment__c rc= new REIT_Investment__c();
         rc.Current_Capital__c=346677;
         rc.Investor_Contact__c=c1.id;
         rc.Rep_Contact__c=c1.id;
         rc.Fund__c='3770';
         insert rc;
         
          REIT_Investment__c rcc= new REIT_Investment__c();
         rcc.Current_Capital__c=346677;
         rcc.Investor_Contact__c=c1.id;
         rcc.Rep_Contact__c=c1.id;
         rcc.Fund__c='3776';
         insert rcc;
         
         REIT_Investment__c rc2= new REIT_Investment__c();
         rc2.Current_Capital__c=346677;
         rc2.Investor_Contact__c=c1.id;
         rc2.Rep_Contact__c=c1.id;
         rc2.Fund__c='3777';
         insert rc2;
         
         REIT_Investment__c rc12= new REIT_Investment__c();
         rc12.Current_Capital__c=346677;
         rc12.Investor_Contact__c=c1.id;
         rc12.Rep_Contact__c=c1.id;
         rc12.Fund__c='3775';
         insert rc12;
         
         system.assertequals(rc12.Rep_Contact__c,c1.id);

         update c1;
         
         
         
        Ocms_BusinessSummaryNew rc1=new Ocms_BusinessSummaryNew();
        try
        {
        //   rc1.getProductList(); 
           rc1.getProductList1();
           rc1.writeControls();
           rc1.writeMapView();
            
           rc1.getHtml();
        //   rc1.writeListView(); 
           rc1.writeListView1(); 
        }
          catch(Exception e)  {        }    
         
       }  
         
     }    
   }