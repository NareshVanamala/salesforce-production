@isTest
private class EmbeddedControllerTest{

    static testMethod void Test_ControllerEmbedded() {
        // set up some test data to work with
        Case c = new case(Subject = 'Test AZ Case');
        insert c;
        
        System.assertEquals(c.Subject,'Test AZ Case');

        // start the test execution context
        Test.startTest();

        // set the test's page to your VF page (or pass in a PageReference)
        Test.setCurrentPage(Page.ComplianceStatePicker);

        // call the constructor
        EmbeddedController controller = new EmbeddedController(new ApexPages.StandardController(c));

        // test action methods on your controller and verify the output with assertions
        controller.save();

        // stop the test
        Test.stopTest();
    }

}