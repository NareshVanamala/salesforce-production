@isTest(seeAlldata=true)
private class Test_Newhiresubmit1{
 
    static testMethod void testApprovalSuccess() 
    {   
        
        string departmentName;
        string departmentName1;
        Id NewhireRequest=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormParent' and SobjectType = 'NewHireFormProcess__c'].id; 
        Id NewhireRequestSubcase=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormChild' and SobjectType = 'NewHireFormProcess__c'].id; 
        Id uid=[SELECT Alias,Id,LastName FROM User LIMIT 1].id;
        
        // TEST CUSTOM SETTING 
        New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
        if(departmetnITEvent == null) {             
            departmetnITEvent = new New_Hire_Request__c(Name= 'CustomValues');
            departmetnITEvent.Application_Implementor__c =uid;
            departmetnITEvent.Application_Owner__c=uid;
            insert departmetnITEvent;
        }
        
       /* NewHire__c  departmetnITEvent1=NewHire__c.getvalues(departmentName1);
        if(departmetnITEvent1 == null) {             
            departmetnITEvent1 = new NewHire__c(Name= 'CustomValues');
            departmetnITEvent1.Application_Owner__c=uid;
            insert departmetnITEvent1;
        }*/
        
        NewHireFormProcess__c nh = new NewHireFormProcess__c();
        nh.recordtypeid = NewhireRequest;
        nh.First_Name__c = 'Testing';
        nh.Last_Name__c = 'parent';
        nh.Supervisor__c = 'Ninad';
        nh.Office_Location__c = 'Chennai';
        nh.status__c='New';
        nh.HR_ManagerApprovalStatus__c = 'Sumitted For Approval';
        nh.Employee_Role__c ='Developer';
        nh.DepartmentUpdate__c = '01-External Sales';
        
        nh.MRI_Access_Requested__c = true;
        nh.If_MRI_Access_is_Needed_Indicate_Secur__c = 'Test';
        nh.Salesforce_Deal_Central_Access_Required__c = true;
        nh.Webex_Access_Requested__c = true;
        nh.VDI_Access_Requested__c = true;
        nh.VPN_Access_Requested__c = true;
        nh.Argus_Access_Requested__c = true;
        nh.DST_Access_Requested__c = true;
        nh.AVID_Account_Requested__c = true;
        nh.BNA_Access_Requested__c = true;
        nh.Box_com_Requested__c = true;
        nh.Chatham_Requested__c = true;
        nh.ADP_Requested__c = true;
        nh.Concur_Account_Requested__c = true;
        nh.Data_Warehouse_GL_Requested__c  = true;
        nh.Data_Warehouse_Real_Estate_Requested__c  =true;
        nh.Data_Warehouse_Sales_Requested__c  =true;
        nh.Kardin_Requested__c  =true;
        nh.OneSource_Requested__c  =true;
        nh.RisKonnect_Requested__c =true;
        nh.Web_Filings_Requested__c = true;
        
        //Parking
        nh.Parking_General__c = true;
        nh.Parking_Executive__c =true;
        nh.Parking_Contractor__c = true;
        
        //Hardware
        nh.Provide_Desktop__c =true;
        nh.Deploy_External_IT_Package__c =true;
        nh.Provide_Laptop__c =true;
        nh.Dual_Monitor_Display__c =true;
        nh.Provide_iPhone__c =true;
        nh.Provide_iPad__c =true;
        
        //Building Access
        nh.General__c =true;
        nh.Facilities__c =true;
        nh.RE_Legal__c =true;
        nh.IT_Infrastructure__c =true;
        nh.HR__c =true;
        nh.Compliance__c =true;
        nh.Property_Management__c =true;
        nh.Audio_Visual__c =true;
        nh.Executive__c =true;
        nh.B2_Storage__c =true;
        
        nh.Active_Directory_Group_1__c = 'Sec-01-External Sales';
        nh.Active_Directory_Group_2__c = 'Sec-01-External Sales';
        nh.Active_Directory_Group_3__c = 'Sec-01-External Sales';
        nh.Active_Directory_Group_4__c = 'Sec-01-External Sales';
        nh.Active_Directory_Group_5__c = 'Sec-01-External Sales';
        nh.Active_Directory_Group_6__c = 'Sec-01-External Sales';
        insert nh;
        system.assertequals(nh.Active_Directory_Group_1__c,'Sec-01-External Sales');

        nh.HR_ManagerApprovalStatus__c = 'Approved';
        nh.status__c = 'In process';
       // update nh;
        
        
        /*
        NewHireFormProcess__c nh1 = new NewHireFormProcess__c();
        nh1.AccessRequest__c= nh.id;
        nh1.recordtypeid=NewhireRequestSubcase;
        nh1.subject__c='Hardware Desktop Request';
        nh1.status__c = 'Submitted for Approval';
        nh1.HR_ManagerApprovalStatus__c='Approved';
        nh1.First_Name__c=nh.First_Name__c;
        nh1.Last_Name__c=nh.Last_Name__c;
        nh1.Supervisor__c=nh.Supervisor__c;
        nh1.Office_Location__c=nh.Office_Location__c;
        nh1.Employee_Role__c=nh.Employee_Role__c;
        nh1.DepartmentUpdate__c=nh.DepartmentUpdate__c;
        nh1.Title__c=nh.Title__c; 
        insert nh1;
        //nh1.AccessRequest__r.status__c = 'closed';
        nh1.status__c = 'closed'; 
        update nh1;
        
        Employee__c e = new Employee__c();
        e.First_Name__c = nh.First_Name__c;
        e.Last_Name__c = nh.Last_Name__c;
        e.Supervisor__c = nh.Supervisor__c;
        e.Department__c= nh.DepartmentUpdate__c;
        e.Title__c = nh.Title__c;
        e.Office_Location__c = nh.Office_Location__c;
        e.Start_Date__c = nh.Start_Date__c;
        e.Employee_Role__c = nh.Employee_Role__c;
        insert e;  
        
        Asset__c a = new Asset__c();
        a.Access_Name__c = 'MRI';
        a.Access_Type__c = 'Application';  
        a.Employee__c = e.id;
        insert a;
        */
    }
    static testMethod void testApprovalSuccess1() {
    
        Id NewhireRequest=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormParent' and SobjectType = 'NewHireFormProcess__c'].id; 
        Id NewhireRequestSubcase=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormChild' and SobjectType = 'NewHireFormProcess__c'].id; 
        Id uid=[SELECT Alias,Id,LastName FROM User LIMIT 1].id;
        
        NewHireFormProcess__c nh2 = new NewHireFormProcess__c();
        nh2.recordtypeid = NewhireRequestSubcase;
        nh2.First_Name__c = 'Testing1';
        nh2.Last_Name__c = 'parent1';
        nh2.Supervisor__c = 'Ninad';
        nh2.Office_Location__c = 'Chennai';
        nh2.status__c='closed';
        nh2.HR_ManagerApprovalStatus__c = 'Approved';
        nh2.Employee_Role__c ='Developer';
        nh2.Application_owner__c = uid ;
        nh2.Application_Implementor1__c = uid;
        insert nh2;
                system.assertequals(nh2.HR_ManagerApprovalStatus__c,'Approved');

    
    }
 
}