@isTest
Public class Test_UpdateChildProducts    
{
    static testmethod void testunit()
    {
        test.starttest();
        Account acc = new Account();
        acc.Name = 'Test Prospect Account'; 
        Insert acc;
        
        Contact c = new Contact();
        c.firstname='RSVP';
        c.lastname='test';
        c.accountid=acc.id;
        c.MailingCity='testcity';
        c.MailingState='teststate';
        c.email='test@mail.com';
        c.Contact_Type__c = 'Registered Rep';
        insert c;
        
        Competitor__c newcomp = new Competitor__c(Name = 'New_York_Life_TEST',
        AUM__c = 10000,Street__c = '169 Lackawanna',City__c='Parsippany',
        State__c='NJ',ZIP__c='08633');
            
        insert newcomp;
        
        Products__c newprod = new Products__c();
        newprod.name = 'GP_TEST';
        newprod.Is_Master_Record__c = True;
        newprod.Competitor__c = newcomp.id;
        newprod.FFO_Ratio__c = 33.60;
        newprod.Launch_Date__c = Date.Valueof('2014-05-04');
        newprod.Expected_Close_Date__c = Date.Valueof('2014-06-07');
        newprod.Total_Properties_Loans__c = 300;
        newprod.Strategy__c = 'Debt-Equity;Globally Diversified';
        newprod.Average_Lease_term__c = 3.50;
        newprod.Talking_Points__c = 'My Comments';
        insert newprod;

        system.assertequals(newprod.Competitor__c,newcomp.id);


//select name,id,Average_Lease_term__c,Contact__c,Competitor__r.name,Diversification__c,Price_Per_Share__c,Dividend__c,Expected_Close_Date__c,FFO_Ratio__c,Launch_Date__c,MFFO_Ratio__c,Occupancy_Rate__c,Offering_size__c,Portfolio_Cap_Rate__c,Strategy__c,Talking_Points__c,Total_Properties_Loans__c from Products__c where((Competitor__r.name Like :str)or(name like:str)) and Is_Master_Record__c=true


        Products__c newprod1 = new Products__c();
        newprod1.name = 'GP_TEST';
        newprod1.Is_Master_Record__c = false;
        newprod1.Competitor__c = newcomp.id;
        newprod1.FFO_Ratio__c = 33.60;
        newprod1.Launch_Date__c = Date.Valueof('2014-05-04');
        newprod1.Expected_Close_Date__c = Date.Valueof('2014-06-07');
        newprod1.Total_Properties_Loans__c = 300;
        newprod1.Strategy__c = 'Debt-Equity;Globally Diversified';
        newprod1.Average_Lease_term__c = 3.50;
        newprod1.Talking_Points__c = 'My Comments';
        newprod1.Contact__c= c.id;
        insert newprod1;

        system.assertequals(newprod1.Contact__c,c.id);

        newprod.FFO_Ratio__c=40.66; 
         update newprod; 

            test.stoptest();
      } 
            
}