@isTest
private class WhatREITS_ContactTestcls{
static testMethod void  WhatREITS_Contact_test() {

  Account acc = new Account();
  acc.name = 'test';
  insert acc;
    Contact a = new Contact();
       a.accountid= acc.Id;
       a.LastName = 'First Account';
       a.RecordTypeid = '012300000004rLn';
       a.What_REITs__c='Dividend Capital';
       a.Income_Products_Used__c ='ETF;SMA';
       a.BD_Top_Producer__c='Jefferson Pilot';
       a.What_REITs_check__c = TRUE;
       helper.run = False;
       insert a;
         system.assertequals(a.accountid,acc.Id);

       Contact b = [ Select Id,RecordTypeid,What_REITs__c,Income_Products_Used__c,BD_Top_Producer__c from Contact where Id = :a.Id];
       b.What_REITs__c = 'Dividend Capital';
       b.What_REITs_check__c = TRUE;
       b.Income_Products_Used__c ='SMA';
       b.Income_Products_UsedCheck__c=True; 
       update b;
       
          }
       }