@isTest(seeAlldata=true)
private class LC_ApprovalHistoryController_Test
{   
   
    public static testMethod void Testforfinraclass () 
    {  
    
     Test.startTest();
      map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);

         system.debug('Still Angery...please let me know'+MYMAp);
    //********************with record typr New**********************    
     
     RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'New_Lease_Opportunity']; 
   //created New lease with look up to lease
         string recid=NewRecType.id;
         LC_LeaseOpprtunity__c lc1= new LC_LeaseOpprtunity__c();
         lc1.name='TestNEWOpportunity';
         lc1.Lease_Opportunity_Type__c='Lease - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Status__c='Active';
         lc1.Milestone__c='LETS In Progress';
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.LC_OtherOpportunity_Description__c='Thisis my description';
         lc1.Base_Rent__c=12345;
         lc1.recordtypeid=recid;
         insert lc1;
         //lc1.Milestone__c='Approval Pending';
         //lc1.Status__c='In Active';
         //update lc1;
         
         
     User user1 = [SELECT Id FROM User WHERE Alias='npati'];
            
    if([select count() from ProcessInstance where targetobjectid=:lc1.id] < 1)
        {       
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Approve.');
            req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
            req.setObjectId(lc1.Id);

            //Submit the approval request
            Approval.ProcessResult result = Approval.process(req);

        }
    
     pagereference ref = system.Currentpagereference();
     ref.getParameters().put('id',lc1.id);  
     ApexPages.StandardController controller = new ApexPages.StandardController(lc1);
     
            list<ProcessInstance> processInstances  = [select id from ProcessInstance where TargetObjectId =:lc1.id];
        // system.assertequals(processInstances.size(),1);
     LC_ApprovalHistoryController  n = new LC_ApprovalHistoryController ();
      n.getwcampList ();
     LC_ApprovalHistoryController.MyWrapper m = new LC_ApprovalHistoryController.MyWrapper();
        m.wAccseesRequestName='TEst Request';
        m.wStatus='Pending';
        m.wApprover='Approver 1';
        m.wActualApprover='Approver 1';
        //m.Date wDate==
        m.wComments='Test Comments';
        m.wcaption='Test Caption';
     
     //NewHireQuoteApprovalHistoryController1 mcls = new NewHireQuoteApprovalHistoryController1(controller);
     //mcls.wcampList = mcls.getwcampList();
    
    Test.stopTest();
   
   
    }
    
    
 }