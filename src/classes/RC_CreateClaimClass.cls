public with sharing class RC_CreateClaimClass
{

    public Void NoGotoSubmitScreen() {
        showAddAttachment =True;
        showWitnInfoQuestionscreen=false;
        
    }


    public void GotoAddWitnessScreen() {
       showwitnesslInformation=true;
      showNewWitnessScreen=true;
      showWitnInfoQuestionscreen=false;
    }


    
 /*public void Save() {
       update Newclaimcrated;
    }*/


   Public boolean showCoveragetype{get;set;}
   Public boolean showClaimtype{get;set;}
   Public boolean isPROPClaim{get;set;}
   Public boolean showClaimantType{get;set;}
   Public boolean showThirdPartyManager{get;set;}
   Public boolean showPoliceinformation{get;set;}
   Public boolean showMedicalinformation{get;set;}
   public boolean showwitnesslInformation { get; set; }
   public boolean showAddAttachment { get; set; }
   public string claimid{get;set;} 
   public RC_Claim__c Newclaimcrated{get;set;}
   Public boolean GLBICoveragetype{get;set;} 
   Public boolean GLPDCoveragetype{get;set;} 
   Public boolean PropCoveragetype{get;set;} 
   public String selectedlocation { get; set; }
   public  Witness__c  newWiness { get; set; }
   public boolean showNewWitnessScreen { get; set; }
   public boolean showExistingWitnesscreen { get; set; }
   public boolean showWitnInfoQuestionscreen{get; set;}
   public list<Witness__c > Witnesses { get; set; }
   //this is for commandlinks
   public boolean Coveragelink{get;set; }
   public boolean medicallink{get;set; }
   public boolean Claimink{get;set; }
   public boolean ClaimantTypeink{get;set; }
   public boolean Policeink{get;set; }
   public boolean ThirdPartyManagerink{get;set; }
   public boolean Witnesslink{get;set; }
   public boolean Submitlink{get;set; }
   //public string removeattid {get;set;}
   /*public String removeattid {
        get;
        set {
            removeattid = value;
            System.debug('***************** value: '+value);
        }
    }*/
    //*******Attchment Code***********
    // the parent object it
    public Id sobjId {get; set;}
   // list of existing attachments - populated on demand
    public List<Attachment> attachments;
    // list of new attachments to add
    public List<Attachment> newAttachments {get; set;}
     // the number of new attachments to add to the list when the user clicks 'Add More'
    public static final Integer NUM_ATTACHMENTS_TO_ADD=1;

    //*******Attchment Code***********
    public RC_CreateClaimClass()
    {
       showCoveragetype=true;
       showClaimtype=false;
       isPROPClaim=false;
       showClaimantType=false;
       showThirdPartyManager=false;
       showPoliceinformation=false;
       showMedicalinformation=false;
       showNewWitnessScreen=false;
       showExistingWitnesscreen =false;
       showAddAttachment =False;
       Newclaimcrated=new RC_Claim__c();
       //make all the link except coverage type link
       Coveragelink=true;
       Submitlink=true;
       medicallink=false;
       Claimink=false;
       ClaimantTypeink=false;
       Policeink=false;
       ThirdPartyManagerink=false;
       Witnesslink=false;
     
       showWitnInfoQuestionscreen=false;
      /*claimid=System.currentPageReference().getParameters().get('id');
       if(claimid!=null)
       {
         Newclaimcrated=[select Healthcare_Facility_Postal_Code__c,Healthcare_Facility_State__c,Healthcare_Facility_Address_Line_1__c,Healthcare_Facility_Address_Line_2__c,Healthcare_Facility_City__c,Healthcare_Facility_Name__c,Healthcare_Facility_Phone__c,Policy_Agnecy_Name__c,Police_Comments__c,Police_Phone__c,Police_Report__c,Local_Site_Manager_Phone__c,Local_Site_Manager_Email__c,Local_Site_Manager_Comments__c,Local_Site_Manager_Last_Name__c,Local_Site_Manager_First_Name__c,Claim_Information_Comments__c,Claimant_Work_Phone__c,Claimant_Home_Phone__c,Claimant_Postal_Code__c,Claimant_State__c,Claimant_City__c,Claimant_Address_Line2__c,Claimant_Address_Line1__c,Claimant_Last_Name__c,Claimant_First_Name__c,id,name,Coverage_Type__c,Location__c,Date_of_Loss__c,Accident_Description__c,Fund_Name__c,Claimant_Gender__c from RC_Claim__c where id=:claimid];
         if(Newclaimcrated.Coverage_Type__c=='GLBI')
           GLBICoveragetype=true;
         if(Newclaimcrated.Coverage_Type__c=='GLPD')
           GLPDCoveragetype=true;  
         if(Newclaimcrated.Coverage_Type__c=='PROP')
           PropCoveragetype=true;    
           
       } */
       newWiness = new Witness__c();
       Witnesses =new list<Witness__c>();
       
       //*******Attchment Code***********
        // instantiate the list with a single attachment
       newAttachments=new List<Attachment>{new Attachment()};
 

       //*******Attchment Code***********
    }
    
    
     public void showThirdpartyManger() {
        showThirdPartyManager=true;
        showCoveragetype=false;
         showClaimtype=false;
         isPROPClaim=false;
          showClaimantType=false;
          showPoliceinformation=false;
          showMedicalinformation=false;
          showwitnesslInformation =false;
          showAddAttachment =false;
          showWitnInfoQuestionscreen=false;
          
       
    }


    public void showClaimantInfrmpage() {
       showClaimantType=true;
        showCoveragetype=false;
         showClaimtype=false;
         isPROPClaim=false;
         showThirdPartyManager=false;
         showPoliceinformation=false;
         showMedicalinformation=false;
         showwitnesslInformation =false;
         showAddAttachment =false;
        showWitnInfoQuestionscreen=false;
       
    }


    public void showClaimInfrmpage() {
    
       
       showClaimtype=true;
       isPROPClaim=false;
       showCoveragetype=false;
       showClaimantType=false;
       showThirdPartyManager=false;
       showPoliceinformation=false;
       showMedicalinformation=false;
       showwitnesslInformation =false;
       showAddAttachment =false;
       showWitnInfoQuestionscreen=false;
       
    }


    public void ShowCoverageTypescereen()
    {
        showCoveragetype=true;
        showClaimtype=false;
        isPROPClaim=false;
         showClaimantType=false;
         showThirdPartyManager=false;
         showPoliceinformation=false;
         showMedicalinformation=false;
         showwitnesslInformation =false;
         showAddAttachment =false;
         
       
    }

     public void showwitnesslInformation() {
       showCoveragetype=false;
        showClaimtype=false;
        isPROPClaim=false;
         showClaimantType=false;
         showThirdPartyManager=false;
         showPoliceinformation=false;
         showMedicalinformation=false;
         showwitnesslInformation =false;
         showNewWitnessScreen=true;
         showExistingWitnesscreen =false;
         showAddAttachment =false;
         showWitnInfoQuestionscreen=true;
    }
    
     public void showAddAttachment()
      {
       showCoveragetype=false;
        showClaimtype=false;
        isPROPClaim=false;
         showClaimantType=false;
         showThirdPartyManager=false;
         showPoliceinformation=false;
         showMedicalinformation=false;
         showwitnesslInformation =false;
         showNewWitnessScreen=false;
         showExistingWitnesscreen =false;
         showAddAttachment =true;
         showWitnInfoQuestionscreen=false;
         
         
         
    }


    public void showmedicalInformation() {
        showMedicalinformation=true;
        showCoveragetype=false;
        showClaimtype=false;
        isPROPClaim=false;
         showClaimantType=false;
         showThirdPartyManager=false;
         showPoliceinformation=false;
         showwitnesslInformation =false;
         showAddAttachment =false;
         showWitnInfoQuestionscreen=false;
    }


    public void showPolicyManger() {
       
        showPoliceinformation=true;
        showCoveragetype=false;
        showClaimtype=false;
        isPROPClaim=false;
         showClaimantType=false;
         showThirdPartyManager=false;
         showwitnesslInformation =false;
        showAddAttachment =false;
        showWitnInfoQuestionscreen=false;
    }
    public Void SaveCoverageType() 
    {
    
     if((PropCoveragetype==false) && (GLPDCoveragetype==false) && (GLBICoveragetype==false)) 
     {
       Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,+'Please select a Coverage Type.'));
        showCoveragetype=True;
      } 
         
     else if(GLBICoveragetype==true) 
      {
        Newclaimcrated.Coverage_Type__c='GLBI';
       Coveragelink=true;
       medicallink=true;
       Claimink=true;
       ClaimantTypeink=true;
       Policeink=true;
       ThirdPartyManagerink=true;
       Witnesslink=true;
       showClaimtype=true;
        isPROPClaim=false;
       
      showCoveragetype=False;
      }
     
     else if(GLPDCoveragetype==true)
     {
        Newclaimcrated.Coverage_Type__c='GLPD';  
       Coveragelink=true;
       medicallink=false;
       Claimink=true;
       ClaimantTypeink=true;
       Policeink=true;
       ThirdPartyManagerink=true;
       Witnesslink=true;
       showClaimtype=true;
        isPROPClaim=false;
       
      showCoveragetype=False;
     }   
     else if(PropCoveragetype==true)
     {
       Newclaimcrated.Coverage_Type__c='PROP';  
         // Newclaimcrated.Coverage_Type__c='GLPD';  
       Coveragelink=true;
       medicallink=false;
       Claimink=true;
       ClaimantTypeink=false;
       Policeink=true;
       ThirdPartyManagerink=true;
       Witnesslink=true;
       showClaimtype=true;
        isPROPClaim=true;
       
      showCoveragetype=False;

      } 
         
      
    }
    
    public Void SaveClaimDetails()
    {
      try
      {
  
          system.debug('check the vlue'+selectedlocation);
          RC_Location__c rcl=[select id, name from RC_Location__c where name=:selectedlocation limit 1];
          system.debug('check the value'+rcl);
          if(rcl!=null)
          Newclaimcrated.Location__c=rcl.id;
          //update Newclaimcrated;
           showClaimtype=false;
            isPROPClaim=false;
           if(PropCoveragetype==true)
            showThirdPartyManager=true;
           else if(PropCoveragetype==false)
            showClaimantType=true;
       } 
       catch(Exception e)
       {
          system.debug('Check the exception'+e);
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select the location from search page')); 
       } 
      
    }
    
    public void SaveClaimantInfo() 
    {
        showClaimantType=false;
        showThirdPartyManager=true;
    }
    
     public Void SaveThirdpartyManagerInfo() 
     {         
         showThirdPartyManager=false;
         showPoliceinformation=true;         
    }

    public void SavePoliceInformation() {
       
           showPoliceinformation=false;
            if((PropCoveragetype==true)||(GLPDCoveragetype==true))
            {
                //showwitnesslInformation =true;
                //showNewWitnessScreen=true;
                showWitnInfoQuestionscreen=true;
          }
         else if((PropCoveragetype==false)||(GLPDCoveragetype==false))
            showMedicalinformation=true;

    }

     public void SaveMedicalInformation() {
      showMedicalinformation=false;
      //showwitnesslInformation=true;
     // showNewWitnessScreen=true;
     showWitnInfoQuestionscreen=true;
    }
  
   
    public void SaveWitnessInfo()
     {
       
         if(Newclaimcrated.id==null)
         {
             if(PropCoveragetype==true) 
             {
               RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'RC_Claim__c' and DeveloperName = 'PROP']; 
               Newclaimcrated.recordtypeid=NewRecType.id;
               Newclaimcrated.Coverage_Major__c='PROP';
               if (Schema.sObjectType.RC_Claim__c.isCreateable())

               insert Newclaimcrated;
             } 
             if(GLPDCoveragetype==true) 
             {
               RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'RC_Claim__c' and DeveloperName = 'GL']; 
               Newclaimcrated.recordtypeid=NewRecType.id;
               Newclaimcrated.Coverage_Major__c='GL';
               Newclaimcrated.Coverage_Minor__c='General Liability Property Damage';
               if (Schema.sObjectType.RC_Claim__c.isCreateable())

               insert Newclaimcrated;
             } 
             if(GLBICoveragetype==true) 
             {
               RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'RC_Claim__c' and DeveloperName = 'GL']; 
               Newclaimcrated.recordtypeid=NewRecType.id;
               Newclaimcrated.Coverage_Major__c='GL';
               Newclaimcrated.Coverage_Minor__c='General Liability Bodily Injury';
               if (Schema.sObjectType.RC_Claim__c.isCreateable())

               insert Newclaimcrated;
             } 
             

         }
         newWiness.Claim__c=Newclaimcrated.id;
         if (Schema.sObjectType.Witness__c.isCreateable())

         insert newWiness;
         Witnesses=[select First_Name__c,Last_Name__c,Address__c,Address_Line_2__c,City__c,Claim__c,Country__c,Home_Phone__c,Postal_Code__c,State__c,Work_Phone__c,Work_Phone_Extension__c from Witness__c where  Claim__c=:Newclaimcrated.id];
         system.debug('mylist'+Witnesses);

         showNewWitnessScreen=False;
         showExistingWitnesscreen =true;
      // showwitnesslInformation =False;
        
          newWiness = new Witness__c();
    }

   public void AddWitness() {
   
         
        showNewWitnessScreen =True;
        showExistingWitnesscreen =false;
   
    }
    
    public void addattchment()
    {
      showNewWitnessScreen =False;
      showExistingWitnesscreen =false;
      showwitnesslInformation =False;
       showAddAttachment =True;
    
    }

  
  

    /*public PageReference done() {
        return null;
    }*/

 //*******Attchment Code***********
 
   
    
   // retrieve the existing attachments
    public List<Attachment> getAttachments()
     
    {
      
    
        // only execute the SOQL if the list hasn't been initialised
        if (null==attachments)
      {
            attachments=[select Id, ParentId, Name, Description from Attachment where parentId=:Newclaimcrated.id];
      }

      return attachments;
    }
    
    // Add more attachments action method
    public void addMore()
    {
         getAttachments();
       
        // append NUM_ATTACHMENTS_TO_ADD to the new attachments list
        for (Integer idx=0; idx<NUM_ATTACHMENTS_TO_ADD; idx++)
        {
         
            newAttachments.add(new Attachment());
            
        }
        
    }   
    // Save action method
    public void attsave()
     {
        try
        {
          if(Newclaimcrated.id==null)
         {
             if(PropCoveragetype==true) 
             {
               RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'RC_Claim__c' and DeveloperName = 'PROP']; 
               Newclaimcrated.recordtypeid=NewRecType.id;
               Newclaimcrated.Coverage_Major__c='PROP';
               if (Schema.sObjectType.RC_Claim__c.isCreateable())

               insert Newclaimcrated;
             } 
             if(GLPDCoveragetype==true) 
             {
               RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'RC_Claim__c' and DeveloperName = 'GL']; 
               Newclaimcrated.recordtypeid=NewRecType.id;
               Newclaimcrated.Coverage_Major__c='GL';
               Newclaimcrated.Coverage_Minor__c='General Liability Property Damage';
               if (Schema.sObjectType.RC_Claim__c.isCreateable())

               insert Newclaimcrated;
             } 
             if(GLBICoveragetype==true) 
             {
               RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'RC_Claim__c' and DeveloperName = 'GL']; 
               Newclaimcrated.recordtypeid=NewRecType.id;
               Newclaimcrated.Coverage_Major__c='GL';
               Newclaimcrated.Coverage_Minor__c='General Liability Bodily Injury';
               if (Schema.sObjectType.RC_Claim__c.isCreateable())

               insert Newclaimcrated;
             } 
          

         }
        List<Attachment> toInsert=new List<Attachment>();
        sobjId=System.currentPageReference().getParameters().get('id');
        for (Attachment newAtt : newAttachments)
        {
           // if (newAtt.Body.size() > 3145728){
           // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'File size is larger than 3 MB, Please upload file which is less than 3 MB')); 
           // } 
           if (newAtt.Body.size() > 5242880){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'File size is larger than 5 MB, Please upload file which is less than 5 MB')); 
           } 
                  
            else if (newAtt.Body!=null)
            {
                newAtt.parentId=Newclaimcrated.id;
                toInsert.add(newAtt);
            }
        }
        if (Schema.sObjectType.Attachment.isCreateable())

        insert toInsert;
        newAttachments.clear();
        newAttachments.add(new Attachment());
        // null the list of existing attachments - this will be rebuilt when the page is refreshed
        attachments=null;
       }
       catch(Exception e)
          {
          
          system.debug('Thieis my exception'+e);
          }   
    }
    
    
    
    /*public void removeattachment() {    
       
        List<Attachment> removeattList = [select Id,Body,ParentId, Name, Description from Attachment where parentId=:Newclaimcrated.id];
        if(removeattList.size()>0) {
            delete removeattList;
        }       
    }*/
    
    
    
    
    
   
    // Action method when the user is done
    public PageReference attdone()
    {
      try
      {
        if(Newclaimcrated.id==null){
           if (Schema.sObjectType.RC_Claim__c.isCreateable())

            insert Newclaimcrated;
        }
        
        EmailTemplate emt=new EmailTemplate();
        
        if(Newclaimcrated.Coverage_Type__c!='PROP'){
        
            emt=[SELECT Id,DeveloperName from EmailTemplate where DeveloperName='RC_Email_AFter_Claim_Creation_Non_Prop'];
        }
        if(Newclaimcrated.Coverage_Type__c =='PROP'){
        
            emt=[SELECT Id,DeveloperName from EmailTemplate where DeveloperName='RC_Email_AFter_Claim_Creation_Prop'];
        }
        
        List<Attachment> upattachments=new List<Attachment>();
        
        for(Attachment att:[select Id,Body,ParentId, Name, Description from Attachment where parentId=:Newclaimcrated.id]){
            //Attachment atmnt = att.clone(false, false, false, false);
            Attachment atmnt =new Attachment();
            atmnt.ParentId=emt.id;
            atmnt.Body=att.Body;
            atmnt.Name=att.Name;
            atmnt.Description=att.Description;
            upattachments.add(atmnt);
        }
        if (upattachments.isEmpty()) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please add Attachment'));
            return null;             
        }
        if(!upattachments.isEmpty()){
           if (Schema.sObjectType.Attachment.isCreateable())

            insert upattachments;
        }
        
        Newclaimcrated.Attachments_Done__c=true;
        
        if (Schema.sObjectType.RC_Claim__c.isUpdateable()) 

        update Newclaimcrated;

        if(!upattachments.isEmpty()){
          if (Attachment.sObjectType.getDescribe().isDeletable()) 
            {
                    delete upattachments;
            }
        }       
       // Newclaimcrated.Attachments_Done__c=false;
        //update Newclaimcrated;
        // send the user to the detail page for the sobject 
       }
       
       Catch(Exception e)
       {
         system.debug('Checj the exception'+e);
       
       } 
        
        return new PageReference('/' + Newclaimcrated.id);
    }
 //*******Attchment Code***********




}