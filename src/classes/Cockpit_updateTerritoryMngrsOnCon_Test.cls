@isTest
public class Cockpit_updateTerritoryMngrsOnCon_Test 
{
 static testmethod void myTest_Method1(){
    
         list<string>excludeUserProfiles=new list<string>();
         list<User>ActiveUserlist=new list<User>();
         excludeUserProfiles=system.label.Cockpit_Exclude_User_Profiles.split(';');
         User usr=[select id, name from User where IsActive=true and profile.name not in:excludeUserProfiles limit 1];
         
         Test.StartTest();
         Contact con= new Contact();
         con.firstname='cockpitcontact1FirstName';
         con.firstname = 'FirstName';
         con.lastname='LastName';
         con.Territory__c='HeartLand';
         con.Phone='4802375787'; 
         con.Sales_Director__c=usr.id;
         con.Internal_Wholesaler1__c=usr.id;
         con.Regional_Territory_Manager1__c =usr.id;
         con.Wholesaler__c=usr.id;
         con.Internal_Wholesaler__c=usr.id;
         con.Regional_Territory_Manager__c=usr.id;
         insert con;
         
         System.assert(con!= null);
         
         Cockpit_updateTerritoryManagersOnContact.Cockpit_updateTerritoryManagersOnContact(New string[]{con.id});

}
}