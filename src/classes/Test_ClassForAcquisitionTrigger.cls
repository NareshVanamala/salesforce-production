@isTest
public class Test_ClassForAcquisitionTrigger
{ 
    public static testmethod void myTestMethod1()
    {
       User u=[select name ,id from User where alias ='ntamb' and IsActive=true limit 1];
        Deal__c testDeal1= new Deal__c();
        testDeal1.name='TestDeal1';
        insert  testDeal1;
         
         Acquisition_Split__c ct= new Acquisition_Split__c();
         ct.Acquisition__c=u.id;
         ct.Acquisition_Split__c=30;
         ct.Deal__c=testDeal1.id;
         insert ct;
        
        Acquisition_Split__c ct1= new Acquisition_Split__c();
         ct1.Acquisition__c=u.id;
         ct1.Acquisition_Split__c=70;
         ct1.Deal__c=testDeal1.id;
         insert ct1;
         
         ct1.Acquisition_Split__c=50;
         update ct1;
         
         delete ct1;
        
         system.assertequals(ct.Deal__c,testDeal1.id);

     }
     
     
 }