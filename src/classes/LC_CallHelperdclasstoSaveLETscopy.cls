global class LC_CallHelperdclasstoSaveLETscopy
{
    @InvocableMethod
    public static void CalltheCentralisedClass(list<String>lcpid)
    {
        String LCid=lcpid[0];
        LC_LeaseOpprtunity__c lcp=[select id, LC_Already_Submitted__c from LC_LeaseOpprtunity__c where id=:LCid];
        decimal letsformcount=lcp.LC_Already_Submitted__c ;
        string mystringvariable='Called from Process Builder';
        LC_LeaseCentralHelper.SaveLETSFormCopy(LCid,letsformcount,mystringvariable);
        
        
        
    }      
    
  }