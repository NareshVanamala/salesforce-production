public  with sharing class NationalAccountTerritories {
  
  // Constructor
 public NationalAccountTerritories(ApexPages.StandardController controller)
 {
 this.Acc = (Account)controller.getSubject();
 
 this.nat = [SELECT 
 d.AllTerr_ofAllBdReps__c,d.AllTerr_TotalterrVirtuals__c,d.AllTerrCYBdsales__c,d.AllTerrCyytd__c,d.AllTerrlastyr__c,
 d.AllTerrnoofReps__c,d.AllTerrpercentTotalTerrcalls__c,d.AllTerrPerecentTotalterrVisits__c,d.AllTerrSalesInception_c__c,d.AllTerrTotalCalls__c,
 d.AllTerrTotalVirtuals__c,d.AllTerrtotalVisits__c,d.Terr1_ofAllBdReps__c,d.Terr1_TotalterrVirtuals__c,d.Terr1CYBdsales__c,
 d.Terr1Cyytd__c,d.Terr1lastyr__c,d.Terr1noofReps__c,d.Terr1percentTotalTerrcalls__c,d.Terr1PerecentTotalterrVisits__c,d.Terr1SalesInception__c,
 d.Terr1TotalCalls__c,d.Terr1TotalVirtuals__c,d.Terr1totalVisits__c,
 d.Terr2_CY_BD_Sales__c,d.Terr2_ofAllBdReps__c,d.Terr2_Total_Terr_Calls__c,d.Terr2_Total__c,d.Terr2_Total_Terr_Visits__c,d.Terr2CYYTD__c,
 d.Terr2LastYr__c,d.Terr2NoOfReps__c,d.Terr2SalesInception__c,d.Terr2TotalCalls__c,d.Terr2TotalVirtual__c,
 d.Terr2TotalVisits__c,d.Terr3_CY_BD_Sales__c,d.Terr3_ofAllBdReps__c,d.Terr3_Total_Terr_Calls__c,d.Terr3_Total_Terr_Virtual__c,d.Terr3_Total_Terr_Visits__c,d.Terr3CYYTD__c,
 d.Terr3LastYr__c,d.Terr3NoOfReps__c,d.Terr3SalesInception__c,d.Terr3TotalCalls__c,d.Terr3TotalVirtual__c,d.Terr3TotalVisits__c,d.Terr4_CY_BD_Sales__c,d.Terr4_ofAllBdReps__c,
 d.Terr4_Total_Terr_Calls__c,d.Terr4_Total_Terr_Virtual__c,d.Terr4_Total_Terr_Visits__c,d.Terr4CYYTD__c,d.Terr4LastYr__c,d.Terr4NoOfReps__c,d.Terr4SalesInception__c,
 d.Terr4TotalCalls__c,d.Terr4TotalVirtual__c,d.Terr4TotalVisits__c,d.Terr5_ofAllBdReps__c,d.Terr5_TotalterrVirtuals__c,d.Terr5CYBdsales__c,
 d.Terr5Cyytd__c,d.Terr5noofReps__c,d.Terr5percentTotalTerrcalls__c,d.Terr5PerecentTotalterrVisits__c,d.Terr5SalesInception__c,d.Terr5TotalCalls__c,d.Terr5TotalVirtuals__c,
 d.Terr5totalVisits__c,d.Terr5LastYr1__c,d.TerritoryName1__c,d.TerritoryName2__c,d.TerritoryName3__c,d.TerritoryName4__c,d.TerritoryName5__c
 
  FROM 
  National_Account_Helper__c d 
  WHERE 
  d.Account__c = :Acc.id ];
 }
 
 // Action Method called from page button
 public pagereference saveChanges() { 
  update this.nat;
 PageReference pageRef1 = new PageReference('/apex/TerritoryOutput');
 return pageRef1;
 
 }
 public pagereference Edit1() { 
  PageReference pageRef = new PageReference('/apex/Territory');
 return pageRef;
 }
 // public Getter to provide table headers 
 //public string[] getheaders() { return new string [] 
 // {'LastName','FirstName','Phone' } ; }
 
 // public Getter to list project deliverables
 public National_Account_Helper__c getrecs() { 
  return this.nat; 
 } 
 
 // class variables
 Account Acc = new Account();
 National_Account_Helper__c nat = new National_Account_Helper__c();
  
}