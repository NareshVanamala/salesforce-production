@isTest(seealldata=true)
public with sharing class UpdateContactStatusField_Test {
 
  static testMethod void testUpdateContactStatusField()
  {
  
    
    Campaign cm1=new Campaign();
    cm1.name='test';
    insert cm1;
    
    Account a = new Account(Name='Test Account Name');
    insert a;

    Contact c = new Contact(LastName = 'Contact Last Name',Internal_Wholesaler__c='test',Wholesaler__c='test',Regional_Territory_Manager__c='test', AccountId = a.id);
    c.Email = 'testguy309@testyourcode.com';
    insert c;
    system.assertequals( c.AccountId,a.id);

    CampaignMember cm=new CampaignMember();
    cm.contactid=c.id;
    cm.Campaignid=cm1.id;
    cm.status='RSVP';
    insert cm;
    
    cm.status='Responded';
    update cm;
  } 
 }