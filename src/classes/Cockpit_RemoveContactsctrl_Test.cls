@isTest
(SeeAllData=true)
public class Cockpit_RemoveContactsctrl_Test{
    
   static testmethod void myTestMethod(){

     //Test.StartTest();       
    // System.assert(cl != null);
    
     Contact c = new Contact();
     c.Firstname='Test';
     c.Lastname='record';
     insert c;
     System.assert(c != null);
     
     Contact c1 = new Contact();
     c1.Firstname='Test';
     c1.Lastname='record';
     insert c1;
     System.assert(c1 != null);
     
         
     List<Contact> scope1 = new List<Contact>();
     scope1.add(c); 
     
     Campaign cn = new Campaign();
     cn.name='Blast Email';
     insert cn;
     System.assert(cn != null);
 
     CampaignMember cr = new CampaignMember();
     cr.campaignid = cn.id;
     cr.contactid = c.id;
     insert cr;
     System.assert(cr != null);
  
     
     Automated_Call_List__c ACL = new Automated_Call_List__c();
     ACL.name ='TestingCallList'+c.id;
     ACL.Campaign__c =cn.id;
     ACL.Dynamic_Query_Filters__c='Select Campaign Status&&&&Alternative_Investments_Firm_Provided__c,equals,null-->X1031_Relationship__c,equals,null-->Account.HNW_Accounts_2M__c,equals,null-->Account.Analyst_and_Research_Role__c,equals,False-->Account.Name,equals,null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c,starts with,null-->Account.Active__c,starts with,null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c,contains,null-->Account.BusinessProfileTrackPercent__c,equals,null-->Account.CapleaseNumberofLocations__c,equals,null';
     //ACL.Last_Activity_Date__c=Date.today()-30;
     ACL.Archived__c=false;
     insert ACL;
     System.assert(ACL != null);
     
     ACL.TempLock__c = 'General Lock';
     update ACL;
    
     ACL.TempLock__c = '';
     ACL.Internal_Flag__c  =true;
     update ACL;
     
     Automated_Call_List__c ACL1 = new Automated_Call_List__c();
     ACL1.name ='TestingCallList1'+c.id;
     //ACL1.Campaign__c =cn.id;
     ACL1.Dynamic_Query_Filters__c='Select Campaign Status&&&&Alternative_Investments_Firm_Provided__c&&!!equals&&!!null-->X1031_Relationship__c&&!!equals&&!!null-->Account.HNW_Accounts_2M__c&&!!equals&&!!null-->Account.Analyst_and_Research_Role__c&&!!equals&&!!False-->Account.Name&&!!equals&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!starts with&&!!null-->Account.Active__c&&!!starts with&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!contains&&!!null-->Account.BusinessProfileTrackPercent__c&&!!equals&&!!null-->Account.CapleaseNumberofLocations__c&&!!equals&&!!null';
     ACL1.Custom_Logic__c = 'AND';
    // ACL1.Last_Activity_Date__c=Date.today()-30;
     ACL1.Archived__c=false;
     ACL1.Type_Of_Call_List__c='Campaign Call List';
     ACL1.IsDeleted__c=false;
     ACL1.Visiblity__c='Visible to all users';
     ACL1.TempLock__c='';
     ACL1.Activity_In_Last_X_Days__c= 7;
     ACL1.Open_Task_Due_In_X_Days__c = 7;
     ACL1.Description__c = 'testing';
     ACL1.Priority__c = true;              
     ACL1.Dynamic_Query__c='select Id,Name,Internal_Wholesaler1__c,Territory__c,RIA_Territory__c from Contact where (First_Name__c = \'Naresh\')';
     insert ACL1;
     
     System.assert(ACL1 != null);
     
     Automated_Call_List__c ACL2 = new Automated_Call_List__c();
     ACL2.name ='TestingCallList2'+c.id;
     //ACL1.Campaign__c =cn.id;
     ACL2.Dynamic_Query_Filters__c='Select Campaign Status&&&&Alternative_Investments_Firm_Provided__c&&!!equals&&!!null-->X1031_Relationship__c&&!!equals&&!!null-->Account.HNW_Accounts_2M__c&&!!equals&&!!null-->Account.Analyst_and_Research_Role__c&&!!equals&&!!False-->Account.Name&&!!equals&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!starts with&&!!null-->Account.Active__c&&!!starts with&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!contains&&!!null-->Account.BusinessProfileTrackPercent__c&&!!equals&&!!null-->Account.CapleaseNumberofLocations__c&&!!equals&&!!null';
     ACL2.Custom_Logic__c = 'AND';
    // ACL2.Last_Activity_Date__c=Date.today()-30;
     ACL2.Archived__c=false;
     ACL2.Type_Of_Call_List__c='Campaign Call List';
     ACL2.Visiblity__c='Visible to all users';
     ACL2.IsDeleted__c=false;
     ACL2.TempLock__c='';
     ACL2.Activity_In_Last_X_Days__c= 7;
     ACL2.Open_Task_Due_In_X_Days__c = 7;
     ACL2.Description__c = 'testing';
     ACL2.Priority__c = true;              
     ACL2.Dynamic_Query__c='select Id,Name,Internal_Wholesaler1__c,Territory__c,RIA_Territory__c from Contact where (First_Name__c = \'Naresh\')';
     insert ACL2;
     System.assert(ACL2 != null);
          
     contact_call_list__c clist = new contact_call_list__c();
     clist.Call_Complete__c = false;
     clist.Automated_Call_List__c = ACL.id;
     clist.CallList_Name__c = ACL.Name;
     clist.Description__c = 'Testing';
     clist.ContactCalllist_UniqueConbination__c=ACL.Name+c.Id;
     clist.Contact__c = c.id;
     //clist.Archived__c=false;
     //clist.campaign__c = cn.id;
     insert clist;
     System.assert(clist != null);
     
     contact_call_list__c clist1 = new contact_call_list__c();
     clist1.Call_Complete__c = false;
     clist1.Automated_Call_List__c = ACL1.id;
     clist1.CallList_Name__c = ACL1.Name;
     clist1.Description__c = 'Testing';
     clist1.ContactCalllist_UniqueConbination__c=ACL1.Name+c1.Id;
     clist1.Contact__c = c1.id;
     //clist1.Archived__c=false;
     //clist.campaign__c = cn.id;
     insert clist1;

     System.assert(clist1 != null);   
     
     Id MngCallListRecId = Schema.SObjectType.Manage_Cockpit__c.getRecordTypeInfosByName().get('Manage Call Lists').getRecordTypeId();
     Manage_Cockpit__c ManageCallListRec = new Manage_Cockpit__c();
     ManageCallListRec.recordtypeid = MngCallListRecId;
     ManageCallListRec.Archive_Call_List_in_X_days__c = '7';
     insert ManageCallListRec;
     Test.StartTest(); 
   
     Cockpit_RemoveContactsctrl removeContactsCtrl = new Cockpit_RemoveContactsctrl();
     removeContactsCtrl.removeFieldName1='FirstName';
     removeContactsCtrl.removeOperator1='equals';
     removeContactsCtrl.removeValue1=c.FirstName;
     removeContactsCtrl.removeFieldName2='FirstName';
     removeContactsCtrl.removeOperator2='equals';
     removeContactsCtrl.removeValue2=c.FirstName;
     removeContactsCtrl.removeFieldName3='FirstName';
     removeContactsCtrl.removeOperator3='equals';
     removeContactsCtrl.removeValue3=c.FirstName;
     removeContactsCtrl.removeFieldName4='FirstName';
     removeContactsCtrl.removeOperator4='equals';
     removeContactsCtrl.removeValue4=c.FirstName;
     removeContactsCtrl.removeFieldName5='FirstName';
     removeContactsCtrl.removeOperator5='equals';
     removeContactsCtrl.removeValue5=c.FirstName;
     removeContactsCtrl.removeFieldName6='FirstName';
     removeContactsCtrl.removeOperator6='equals';
     removeContactsCtrl.removeValue6=c.FirstName;
     removeContactsCtrl.removeFieldName7='FirstName';
     removeContactsCtrl.removeOperator7='equals';
     removeContactsCtrl.removeValue7=c.FirstName; 
     removeContactsCtrl.removeFieldName8='FirstName';
     removeContactsCtrl.removeOperator8='equals';
     removeContactsCtrl.removeValue8=c.FirstName;
     removeContactsCtrl.removeFieldName9='FirstName';
     removeContactsCtrl.removeOperator9='equals';
     removeContactsCtrl.removeValue9=c.FirstName;
     removeContactsCtrl.removeFieldName10='FirstName';
     removeContactsCtrl.removeOperator10='equals';
     removeContactsCtrl.removeValue10=c.FirstName;
     removeContactsCtrl.removeContactCallList=ACL2.Name;
     removeContactsCtrl.start=15;
     removeContactsCtrl.index=0;
     removeContactsCtrl.getContactCallListsSize();   
     removeContactsCtrl.getDisablePrevious();
     removeContactsCtrl.getDisableNext();
     removeContactsCtrl.getTotalPages();
     removeContactsCtrl.getPageNumber();
     removeContactsCtrl.go();
     removeContactsCtrl.previous();
     removeContactsCtrl.next();
     removeContactsCtrl.getRemoveOperatorList1();
     removeContactsCtrl.getRemoveOperatorList2();
     removeContactsCtrl.getRemoveOperatorList3();
     removeContactsCtrl.getRemoveOperatorList4();
     removeContactsCtrl.getRemoveOperatorList5();
     removeContactsCtrl.getRemoveOperatorList6();
     removeContactsCtrl.getRemoveOperatorList7();
     removeContactsCtrl.getRemoveOperatorList8();
     removeContactsCtrl.getRemoveOperatorList9();
     removeContactsCtrl.getRemoveOperatorList10();
     removeContactsCtrl.getContactCallLists();

     Test.StopTest();      
     }
 }