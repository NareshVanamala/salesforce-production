public with sharing class updatecontact
{
public boolean subscribe{get;set;}
public string hide{get;set;}
public id urlid {get;set;}
    public updatecontact(ApexPages.StandardController controller) 
    {        
        
        urlid = apexpages.currentpage().getparameters().get('id');
        if(urlid!=null)
        {
        contact c2=[select id,lastname,subscribed__c from contact where id=:urlid];
        subscribe=c2.subscribed__c;
        if(c2.subscribed__c==false)
        hide='<script> disable(); </script>';
        }
    }
public pagereference updatesubscribed()
{
if(urlid!=null)
{
contact c1=[select id,lastname,subscribed__c from contact where id=:urlid];
c1.subscribed__c=subscribe;
if (Schema.sObjectType.contact.isUpdateable()) 
update c1;
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Selection Saved')); 
//pagereference pageRef = new PageReference('/apex/subscribedpage?id'+'='+urlid);
         //pageRef.setRedirect(true);
         return null;
}
return null;
}
}