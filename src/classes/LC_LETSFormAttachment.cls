public with sharing class LC_LETSFormAttachment
{

    public String quoteId1{get;set;} 
    public String myValue{get;set;}
    public attachment LCattachment{get;set;}
    public list<attachment>lst{get;set;}
    
   
    public String getquoteId1() {
        system.debug('>>>>>>>>>>>>>> '+quoteId1);
        return quoteId1;
    }
    
     public void setquoteId1 (String s) {
         myValue=s;
       
     } 
     
      public list<Attachment> LCattachmentlist{get{
        lst= new list<Attachment>();
        system.debug('-------------NewhireNdcontroller-------------->'+quoteId1);
        lst=[select id,name,parentid,body from Attachment where parentid=:quoteId1 and name!='LETS Form.pdf'];
        LCattachmentlist= lst;
        return LCattachmentlist;
    } set;
    }
    
     public boolean showAttachlist{get{
        lst= new list<Attachment>();
        system.debug('-------------NewhireNdcontroller-------------->'+quoteId1);
        lst=[select id,name,parentid,body from Attachment where parentid=:quoteId1 and name!='LETS Form.pdf'];
        if(lst.size()>0)
          showAttachlist=true;
       else if(lst.size()==0)
         showAttachlist= false;
       return  showAttachlist; 
    } set;
    }
    
     public boolean showOutputText{get{
        lst= new list<Attachment>();
        system.debug('-------------NewhireNdcontroller-------------->'+quoteId1);
        lst=[select id,name,parentid,body from Attachment where parentid=:quoteId1 and name!='LETS Form.pdf'];
       
       if(lst.size()>0)
        showOutputText=false;
       else if(lst.size()==0)
         showOutputText=true;
        return showOutputText;
        
    } set;
    }
    
    
    
    
     
   }