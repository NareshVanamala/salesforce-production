@isTest(seealldata=true)
public with sharing class test_ocms_ColeCapitalContact{
 
static testMethod void testocmsColeCapitalContact() {

 profile pf=[select id,name from profile where name='Cole Capital Community'];
       user u=[select id,username,contactid,firstname,lastname from user limit 1];
             
       
        
        account a=new account();
        a.name='test';
        insert a;
       
        list<contact> clist=new list<contact>();
        for(integer i=0;i<3;i++) {
        Contact c= new Contact();
        c.firstName='sandeep';
        c.lastName='m';
        c.HomePhone='9999999999';
        c.title='Director';
        c.phone='9999999999';
        c.accountid=a.id;
        c.email='test@test'+i+'email1.com';
        c.Image_URL__c='/01p500000001k7V';
        c.Internal_Wholesaler__c='sandeep m';
        c.Wholesaler__c='sandeep m';
        c.Regional_Territory_Manager__c='sandeep m';
        c.Internal_Wholesaler1__c=u.id;
        c.Sales_Director__c=u.id;
        c.Regional_Territory_Manager1__c=u.id;
        clist.add(c);
        }
        insert clist;
        
        User user = new User();
        user.FirstName = 'Test222';
        user.LastName = 'Guy222';
        user.Phone = '0123456789';
        user.Email = 'testguy2@testyourcode1.com';
        user.CommunityNickname = 'testguy222@testyourcode.com';
        user.Alias = 'HelloMan';
        user.UserName = 'testguy1111@testvereitorg.com';
        user.ProfileId =  pf.Id;
        user.contactid=clist[0].id;
        user.TimeZoneSidKey = 'GMT';
        user.LocaleSidKey = 'en_US';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.LanguageLocaleKey = 'en_US';
        user.title='mr';
        user.Image_URL__c='test';
        user.MobilePhone='123123123';
        insert user;  
        
        System.assertEquals(user.ProfileId,pf.Id);

     system.runas(user)
        {
     
    ocms_ColeCapitalContact sc=new ocms_ColeCapitalContact();
    
    
    
  try
  {  
     
     list<sobject> ProductList=sc.getProductList();
     sc.writeControls();
     sc.getHTML();
     sc.writeListView();
  }
  catch(exception e){} 
  }
   }
 static testMethod void testocmsColeCapitalContact1() {

 profile pf=[select id,name from profile where name='Cole Capital Community'];
       user u=[select id,username,contactid,firstname,lastname from user limit 1];
               
         

        
        account a=new account();
        a.name='test';
        insert a;
       
     
        Contact c= new Contact();
        c.firstName='sandeep';
        c.lastName='m';
        c.HomePhone='9999999999';
        c.title='Director';
        c.phone='9999999999';
        c.accountid=a.id;
        c.email='test@test1.com';
        c.Image_URL__c='/01p500000001k7V';
        c.Internal_Wholesaler__c='sandeep m';
        c.Wholesaler__c='sandeep m';
        c.Regional_Territory_Manager__c='sandeep m';
        c.Internal_Wholesaler1__c=u.id;
        c.Sales_Director__c=u.id;
        c.Regional_Territory_Manager1__c=u.id;
        
        insert c;
    
         User user = new User();
         user.FirstName = 'Test222';
         user.LastName = 'Guy222';
         user.Phone = '0123456789';
         user.Email = 'testguy114201623@testyourcode4.com';
         user.CommunityNickname = 'testguy222@testyourcode.com';
         user.Alias = 'HelloMan';
         user.UserName = 'testguy222@testvereitorg.com';
         user.ProfileId =  pf.Id;
         user.contactid=c.id;
         user.TimeZoneSidKey = 'GMT';
         user.LocaleSidKey = 'en_US';
         user.EmailEncodingKey = 'ISO-8859-1';
         user.LanguageLocaleKey = 'en_US';
         user.title='mr';
         user.Image_URL__c='test';
         user.MobilePhone='123123123';
         insert user; 
         
          System.assertEquals(user.ProfileId,pf.Id);

     system.runas(user)
        {
     
    ocms_ColeCapitalContact sc=new ocms_ColeCapitalContact();
    
    
    
  try
  {  
     
     list<sobject> ProductList=sc.getProductList();
     sc.writeControls();
     sc.getHTML();
     sc.writeListView();
  }
  catch(exception e){} 
  }
   }  
}