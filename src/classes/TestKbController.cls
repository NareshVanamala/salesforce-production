@isTest
private class TestKbController {
    
   static testMethod void ValidateMethods(){
       
       //User u3 = [SELECT Id FROM User WHERE UserName='ntambe@colecapital.com.stage'];
      User u3 = [SELECT Id FROM User WHERE UserName LIKE 'ntambe@colecapital.com%'];
      System.runAs(u3){
      Kbcontoller kbc =  new Kbcontoller();
      kbc.callback = 'test';
      String paramcat =  'News_Updates__c';
      String SearhcParm = 'someky';
      String sObjectType  = 'KnowledgeArticleVersion' ;
      String reuslt   =  Kbcontoller.GetArticles(paramcat);
      String catResult = Kbcontoller.getCategoriesJson(sObjectType);
      String JSonResult =  JSON.serialize(Kbcontoller.getCategories(sObjectType));
      String LatestArticles = Kbcontoller.GetLatestArticles();
      String SearchResult =  Kbcontoller.SearchArticles(SearhcParm);
     
      //System.assertEquals('',reuslt);
      //
      
      List<Kbcontoller.HomeArticles> HomepageArticles =  new List<Kbcontoller.HomeArticles>();
         //create the kav instance 
        News_Updates__kav a = new News_Updates__kav(
            Title = 'test apex news',
            Summary = 'test from apex',           
            URLName = 'testclassNewsupdatearticle',
            Home_Main_Article__c = true
        );
 
        insert a;
        system.assert(a!=null);
       //in order to get the KnowledgeArticleId
        a = [SELECT KnowledgeArticleId FROM News_Updates__kav WHERE Id = :a.Id];
        
        
        //publish it
        KbManagement.PublishingService.publishArticle(a.KnowledgeArticleId, true);
        
        String qryString = 'SELECT Title,UrlName,Summary,Home_Main_Article__c,VideoUrl__c,LastPublishedDate,LastModifiedBy.name FROM News_Updates__Kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') and Home_Main_Article__c = TRUE  order by LastPublishedDate DESC Limit 1'; 
        
         News_Updates__kav articleList= Database.query(qryString); 
                     
                   
            if(articleList != null){
            
              Kbcontoller.HomeArticles ha = new Kbcontoller.HomeArticles();
              ha.ArtilceTitle = articleList.Title;
              ha.ArticleCategory = 'MainhomeArticle';
              ha.ArticleSummary = articleList.Summary;
              ha.ArticleCreatedBy = articleList.LastModifiedBy.name;
              ha.Artilcepublisheddate = articleList.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = articleList.VideoUrl__c;
              ha.Articleurl = 'News_Updates#'+articleList.UrlName;
              HomepageArticles.Add(ha); 
            }
        
            News_Updates__kav videoart = new News_Updates__kav(
            Title = 'test apex news videotest',
            Summary = 'test from apex',           
            URLName = 'testvideofromtestclassvideos',
            Home_Page_Article_Category__c = 'Videos'
        );
        insert videoart;
        system.assert(videoart!=null);
       //in order to get the KnowledgeArticleId
        videoart = [SELECT KnowledgeArticleId FROM News_Updates__kav WHERE Id = :videoart.Id];
        
        
        //publish it
        KbManagement.PublishingService.publishArticle(videoart.KnowledgeArticleId, true);
          qryString = 'SELECT Title,UrlName,Summary,Home_Main_Article__c,VideoUrl__c,LastPublishedDate,LastModifiedBy.name FROM News_Updates__Kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') and Home_Page_Article_Category__c = \'Videos\'  order by LastPublishedDate DESC Limit 1'; 
            articleList= Database.query(qryString);      
            if(articleList != null){
            
              Kbcontoller.HomeArticles ha = new Kbcontoller.HomeArticles();
              ha.ArtilceTitle = articleList.Title;
              ha.ArticleCategory = 'Videos';
              ha.ArticleSummary = articleList.Summary;
              ha.ArticleCreatedBy = articleList.LastModifiedBy.name;
              ha.Artilcepublisheddate = articleList.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = articleList.VideoUrl__c;
              ha.Articleurl = 'News_Updates#'+articleList.UrlName;
              HomepageArticles.Add(ha); 
            }
            
            
//////////////////////////
            News_Updates__kav vereitmission = new News_Updates__kav(
            Title = 'test apex news videotest',
            Summary = 'test from apex',           
            URLName = 'testvideofromtestclassvideos1234',
            Home_Page_Article_Category__c = 'VEREIT Mission'
        );
        insert vereitmission;
        
        system.assert(vereitmission!=null);
       //in order to get the KnowledgeArticleId
        vereitmission = [SELECT KnowledgeArticleId FROM News_Updates__kav WHERE Id = :vereitmission.Id];
        
        
        //publish it
        KbManagement.PublishingService.publishArticle(vereitmission.KnowledgeArticleId, true);
          qryString = 'SELECT Title,UrlName,Summary,Home_Main_Article__c,VideoUrl__c,LastPublishedDate,LastModifiedBy.name FROM News_Updates__Kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') and Home_Page_Article_Category__c = \'VEREIT Mission\'  order by LastPublishedDate DESC Limit 1'; 
            articleList= Database.query(qryString);      
            if(articleList != null){
            
              Kbcontoller.HomeArticles ha = new Kbcontoller.HomeArticles();
              ha.ArtilceTitle = articleList.Title;
              ha.ArticleCategory = 'VEREIT Mission';
              ha.ArticleSummary = articleList.Summary;
              ha.ArticleCreatedBy = articleList.LastModifiedBy.name;
              ha.Artilcepublisheddate = articleList.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = articleList.VideoUrl__c;
              ha.Articleurl = 'News_Updates#'+articleList.UrlName;
              HomepageArticles.Add(ha); 
            }
        
        
            News_Updates__kav CareerOpportunities = new News_Updates__kav(
            Title = 'test apex news videotest',
            Summary = 'test from apex',           
            URLName = 'testvideosCareerOpportunities1234',
            Home_Page_Article_Category__c = 'Career Opportunities'
        );
        insert CareerOpportunities;
        
        system.assert(CareerOpportunities!=null);
       //in order to get the KnowledgeArticleId
        CareerOpportunities = [SELECT KnowledgeArticleId FROM News_Updates__kav WHERE Id = :CareerOpportunities.Id];
        
        
        //publish it
        KbManagement.PublishingService.publishArticle(CareerOpportunities.KnowledgeArticleId, true);
          qryString = 'SELECT Title,UrlName,Summary,Home_Main_Article__c,VideoUrl__c,LastPublishedDate,LastModifiedBy.name FROM News_Updates__Kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') and Home_Page_Article_Category__c = \'Career Opportunities\'  order by LastPublishedDate DESC Limit 1'; 
            articleList= Database.query(qryString);      
            if(articleList != null){
            
              Kbcontoller.HomeArticles ha = new Kbcontoller.HomeArticles();
              ha.ArtilceTitle = articleList.Title;
              ha.ArticleCategory = 'Career Opportunities';
              ha.ArticleSummary = articleList.Summary;
              ha.ArticleCreatedBy = articleList.LastModifiedBy.name;
              ha.Artilcepublisheddate = articleList.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = articleList.VideoUrl__c;
              ha.Articleurl = 'News_Updates#'+articleList.UrlName;
              HomepageArticles.Add(ha); 
            }
////////////
        
         News_Updates__kav RESpotlightart = new News_Updates__kav(
            Title = 'test apex news videotest',
            Summary = 'test from apex',           
            URLName = 'testRESpotlighttestfromclass',
            Home_Page_Article_Category__c = 'RESpotlight'
        );
        insert RESpotlightart;
        system.assert(RESpotlightart!=null);
       //in order to get the KnowledgeArticleId
        RESpotlightart = [SELECT KnowledgeArticleId FROM News_Updates__kav WHERE Id = :RESpotlightart.Id];
                //publish it
        KbManagement.PublishingService.publishArticle(RESpotlightart.KnowledgeArticleId, true);
        qryString = 'SELECT Title,UrlName,Summary,Home_Main_Article__c,VideoUrl__c,LastPublishedDate,LastModifiedBy.name FROM News_Updates__Kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') and Home_Page_Article_Category__c = \'RESpotlight\'  order by LastPublishedDate DESC Limit 1'; 
        articleList= Database.query(qryString);      
            if(articleList != null){
             Kbcontoller.HomeArticles ha = new Kbcontoller.HomeArticles();
              ha.ArtilceTitle = articleList.Title;
              ha.ArticleCategory = 'RESpotlight';
              ha.ArticleSummary = articleList.Summary;
              ha.ArticleCreatedBy = articleList.LastModifiedBy.name;
              ha.Artilcepublisheddate = articleList.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = articleList.VideoUrl__c;
              ha.Articleurl = 'News_Updates#'+articleList.UrlName;
              HomepageArticles.Add(ha); 
            }
 
            News_Updates__kav Eventsart = new News_Updates__kav(
            Title = 'test apex news videotest',
            Summary = 'test from apex',           
            URLName = 'testREEventsarthometest',
            Home_Page_Article_Category__c = 'VEREIT Beat'
            );
            insert Eventsart;
            system.assert(Eventsart!=null);
            Eventsart = [SELECT KnowledgeArticleId FROM News_Updates__kav WHERE Id = :Eventsart.Id];
            KbManagement.PublishingService.publishArticle(Eventsart.KnowledgeArticleId, true);
        
            qryString = 'SELECT Title,UrlName,Summary,Home_Main_Article__c,VideoUrl__c,LastPublishedDate,LastModifiedBy.name FROM News_Updates__Kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') and  Home_Page_Article_Category__c = \'VEREIT Beat\'  order by LastPublishedDate DESC Limit 1'; 
            articleList= Database.query(qryString);      
            if(articleList != null){
              Kbcontoller.HomeArticles ha = new Kbcontoller.HomeArticles();
              ha.ArtilceTitle = articleList.Title;
              ha.ArticleCategory = 'VEREIT Beat';
              ha.ArticleSummary = articleList.Summary;
              ha.ArticleCreatedBy = articleList.LastModifiedBy.name;
              ha.Artilcepublisheddate = articleList.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = articleList.VideoUrl__c;
              ha.Articleurl = 'News_Updates#'+articleList.UrlName;
              HomepageArticles.Add(ha); 
            }
            
            VERITAS_Award__kav GGarticle = new VERITAS_Award__kav(
            Title = 'test apex news videotest',
            Summary = 'test from apex',           
            URLName = 'TestGGarticlehomtest'   
            );
            insert GGarticle;
            system.assert(GGarticle!=null);
            
            GGarticle = [SELECT KnowledgeArticleId FROM VERITAS_Award__kav WHERE Id = :GGarticle.Id];
            KbManagement.PublishingService.publishArticle(GGarticle.KnowledgeArticleId, true);
            qryString = 'SELECT Title,UrlName,Summary,LastPublishedDate,LastModifiedBy.name FROM VERITAS_Award__kav  WHERE (PublishStatus = \'online\' and Language = \'en_US\') order by LastPublishedDate DESC Limit 1'; 
            VERITAS_Award__kav GGArtilces  = Database.query(qryString);      
            if(GGArtilces != null){
            
              Kbcontoller.HomeArticles ha = new Kbcontoller.HomeArticles();
              ha.ArtilceTitle = GGArtilces.Title;
              ha.ArticleCategory = 'VERITAS_Award';
              ha.ArticleSummary = GGArtilces.Summary;
              ha.ArticleCreatedBy = GGArtilces.LastModifiedBy.name;
              ha.Artilcepublisheddate = GGArtilces.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = '';
              ha.Articleurl = 'VERITAS_Award#'+GGArtilces.UrlName;
              HomepageArticles.Add(ha); 
            }
        
            Miscellaneous__kav  MiscArticle = new Miscellaneous__kav (
            Title = 'test apex miscellaneues art',
            Summary = 'test from apex',           
            URLName = 'TestMiscarticletest',
            Article_Category__c  = 'External Links'
            );
            insert MiscArticle;
            system.assert(MiscArticle!=null);
            MiscArticle = [SELECT KnowledgeArticleId FROM Miscellaneous__kav WHERE Id = :MiscArticle.Id];
            KbManagement.PublishingService.publishArticle(MiscArticle.KnowledgeArticleId, true);
            qryString = 'SELECT Title,UrlName,Summary,LastPublishedDate,LastModifiedBy.name FROM Miscellaneous__kav WHERE (PublishStatus = \'online\' and Language = \'en_US\')and  Article_Category__c = \'External Links\'  order by LastPublishedDate DESC Limit 1'; 
            Miscellaneous__kav mmArtilces  = Database.query(qryString);      
            if(mmArtilces  != null){
            
              Kbcontoller.HomeArticles ha = new Kbcontoller.HomeArticles();
              ha.ArtilceTitle = mmArtilces.Title;
              ha.ArticleCategory = 'ExtLink';
              ha.ArticleSummary = mmArtilces.Summary;
              ha.ArticleCreatedBy = mmArtilces.LastModifiedBy.name;
              ha.Artilcepublisheddate = mmArtilces.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = '';
              ha.Articleurl = 'Miscellaneous#'+mmArtilces.UrlName;
              HomepageArticles.Add(ha); 
            }
            News_Updates__kav Lunch = new News_Updates__kav(
            Title = 'test apex news Lunchtest',
            Summary = 'test from apex Lunch',           
            URLName = 'testvideofromtestclassLunch1234',
            Home_Page_Article_Category__c = 'Lunch'
            );
            insert Lunch;
            system.assert(Lunch!=null);
            Lunch = [SELECT KnowledgeArticleId FROM News_Updates__kav WHERE Id = :Lunch.Id];
            KbManagement.PublishingService.publishArticle(Lunch.KnowledgeArticleId, true);
            qryString = 'SELECT Title,UrlName,Summary,Home_Main_Article__c,VideoUrl__c,LastPublishedDate,LastModifiedBy.name FROM News_Updates__Kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') and  Home_Page_Article_Category__c = \'Lunch\'  order by LastPublishedDate DESC Limit 1'; 
            articleList= Database.query(qryString);      
            if(articleList != null){
              Kbcontoller.HomeArticles ha = new Kbcontoller.HomeArticles();
              ha.ArtilceTitle = articleList.Title;
              ha.ArticleCategory = 'Lunch';
              ha.ArticleSummary = articleList.Summary;
              ha.ArticleCreatedBy = articleList.LastModifiedBy.name;
              ha.Artilcepublisheddate = articleList.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl = articleList.VideoUrl__c;
              ha.Articleurl = 'News_Updates#'+articleList.UrlName;
              HomepageArticles.Add(ha); 
            }
            
            
              String homeartiresult =  Kbcontoller.GetHomePageArtilce();
              
         }     
    }
}