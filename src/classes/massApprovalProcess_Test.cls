@isTest(SeeAllData=true)
private class massApprovalProcess_Test
{
    static testMethod void massapprovaltest()
    {
        MRI_Property__c m = new MRI_Property__c();
        m.Name = 'Sample';
        m.Web_Ready__c = 'Pending Approval';
        m.Property_Id__c = '12345';
        insert m;
        
        System.assertEquals(m.Name,'Sample');

        MassApprovalProcess  massAppr = new MassApprovalProcess();
        massAppr.mripropertyDetails(false,false,5,0,null);
        massAppr.processSelected();
        massAppr.massRejection();
        massAppr.previous();
        massAppr.next();
        massAppr.SearchByProperty();
    }
}