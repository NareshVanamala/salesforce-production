@IsTest
private class ApproveThroughEmail_Test  {
        static testmethod void ApprovalThrough() {
        // Create a new email and envelope object
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.plainTextBody ='Sample Email Body';
        email.fromAddress ='test@test.com';
        String contactEmail = 'jsmith@salesforce.com';
        email.ccAddresses = new String[] {'Jon Smith <' + contactEmail + '>'};
        email.subject = 'Dummy Account Name 123';
        
        Approve_Through_Email  edr = new Approve_Through_Email ();
       
        Test.startTest();
        Messaging.InboundEmailResult result = edr.handleInboundEmail(email, env);
        System.assertEquals(email.subject,'Dummy Account Name 123' );
       
         DataBase.executeBatch(new Approval_Job(null),20);
                
        Test.stopTest();
    } 
    }