@isTest
Public class REIT_ProxyVotes_Test
{
  static testmethod void testProxyVotes()
  {
      
       Account a = new Account();
       a.name = 'testaccount';
       insert a;
  
    
       Contact c1= new Contact();
       c1.accountid=a.id;
       c1.firstname='xxx';
       c1.lastname='yyy';
       insert c1;
  
      REIT_Proxy_Votes__c prv= new REIT_Proxy_Votes__c();
      prv.Investor__c='NinadTambe';
      prv.Shares__c=576879.89;
      prv.Fund__c='3374';
      prv.Amount__c=899900.99;
      prv.DST_Account_Number__c='7799400016';
      prv.Contact__c=c1.id;
      insert prv;
      
       
      REIT_Proxy_Votes__c prv2= new REIT_Proxy_Votes__c();
      prv2.Investor__c='NinadTambe';
      prv2.Shares__c=19000.89;
      prv2.Fund__c='3376';
      prv2.Amount__c=899900.99;
      prv2.DST_Account_Number__c='7799400016';
      prv2.Contact__c=c1.id;
      insert prv2;
      
      REIT_Proxy_Votes__c prv3= new REIT_Proxy_Votes__c();
      prv3.Investor__c='NinadTambe';
      prv3.Shares__c=19000.89;
      prv3.Fund__c='3376';
      prv3.Amount__c=899900.99;
      prv3.DST_Account_Number__c='7799400016';
      prv3.Contact__c=c1.id;
      insert prv3;
      
     System.assertEquals(prv2.Investor__c,prv3.Investor__c);

  
      REIT_ProxyVotes  tyu= new REIT_ProxyVotes ();
      tyu.contactid=c1.id;
      tyu.myValue=c1.id;
      tyu.getContactid();
      REIT_ProxyVotes.REITProxyShareWrappercls testwrapper  = new REIT_ProxyVotes.REITProxyShareWrappercls ('NinadTambe','CCPTV','CCPTIV','CCITII','INAVA','INAVW','INAVI');
      tyu.getWrapperCls();
      tyu.toggleSort();
      tyu.ShowMore();
    
      tyu.sortField='CCPT5fund';
      tyu.showArrowCCPT4fund =true; 
     // REIT_ProxyVotes.REITProxyShareWrappercls testwrapper  = new REIT_ProxyVotes.REITProxyShareWrappercls ('NinadTambe','CCPTV','CCPTIV','CCITII','INAVA','INAVW','INAVI');
      tyu.toggleSort();
    
      tyu.sortField='CCIT2fund';
      tyu.showArrowCCIT2fund=true; 
      //REIT_ProxyVotes.REITProxyShareWrappercls testwrapper  = new REIT_ProxyVotes.REITProxyShareWrappercls ('NinadTambe','CCPTV','CCPTIV','CCITII','INAVA','INAVW','INAVI');
      tyu.toggleSort();
    
    
      tyu.sortField='INAVafund';
      tyu.showArrowINAVafund=true; 
      //REIT_ProxyVotes.REITProxyShareWrappercls testwrapper  = new REIT_ProxyVotes.REITProxyShareWrappercls ('NinadTambe','CCPTV','CCPTIV','CCITII','INAVA','INAVW','INAVI');
      tyu.toggleSort();
      
      tyu.sortField='INAVwfund';
      tyu.showArrowINAVwfund=true; 
      //REIT_ProxyVotes.REITProxyShareWrappercls testwrapper  = new REIT_ProxyVotes.REITProxyShareWrappercls ('NinadTambe','CCPTV','CCPTIV','CCITII','INAVA','INAVW','INAVI');
      tyu.toggleSort();
      
       tyu.sortField='INAVifund';
      tyu.showArrowINAVifund=true; 
      //REIT_ProxyVotes.REITProxyShareWrappercls testwrapper  = new REIT_ProxyVotes.REITProxyShareWrappercls ('NinadTambe','CCPTV','CCPTIV','CCITII','INAVA','INAVW','INAVI');
      tyu.toggleSort();
    
     tyu.sortField='Investor1';
      tyu.showArrowInvsetorName=true; 
      //REIT_ProxyVotes.REITProxyShareWrappercls testwrapper  = new REIT_ProxyVotes.REITProxyShareWrappercls ('NinadTambe','CCPTV','CCPTIV','CCITII','INAVA','INAVW','INAVI');
      tyu.toggleSort();
  
  
  }
  
}