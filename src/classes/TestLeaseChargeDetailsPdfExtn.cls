@isTest(SeeAllData = true)
private class TestLeaseChargeDetailsPdfExtn {

    static testMethod void testDoGet() {
        Lease__c leaseobj =new Lease__c();
        leaseobj =[Select id from Lease__c limit 1];
        Lease_Charges_Request__c leaseCharReqobj = new  Lease_Charges_Request__c();
        leaseCharReqobj.Lease__c=leaseobj.id;
        leaseCharReqobj.Source_Code__c='test';
        leaseCharReqobj.Approval_Status__c='Not Submitted for Approval';
        //leaseobj.LeaseChargesRequest__c='test';
        insert leaseCharReqobj;
        System.assertEquals(leaseCharReqobj.Lease__c,leaseobj.id);
        
        ApexPages.CurrentPage().getparameters().put('id', leaseCharReqobj.id);
       ApexPages.StandardController StandardsObjectController =  new ApexPages.StandardController(leaseCharReqobj);
       LeaseChargeDetailsPdfExtn coninvattctrl  = new LeaseChargeDetailsPdfExtn(StandardsObjectController);
        LeaseChargeDetailsPdfExtn.createAttachment(leaseCharReqobj.id);
    }
  }