public class WebPropertitesLocationCallouts {
      public static boolean inFutureContext = false;
      @future (callout=true)  // future method needed to run callouts from Triggers
      static public void getLocation(set<id> webids){
       // static public void getLocation(id webids){
        // gather account info
        List<Web_Properties__c> updatewplist = new List<Web_Properties__c>();
        List<Web_Properties__c> wplist = [SELECT Address__c,City__c ,State__c ,Zip_Code__c,Country__c,Location__Latitude__s,Location__Longitude__s,Latitude_del__c,Longitude_del__c FROM Web_Properties__c WHERE id IN: webids];
        for(Web_Properties__c wp:wplist)
        {
        if(wp.Address__c != null && wp.Address__c != '') {  
         String streetName ='';
        if(wp.Address__c != null && wp.Address__c != '') {
            streetName += wp.Address__c + ',';
        }
        streetName = streetName.replace(' ', '+');
        //string streetname =string.replace()
        system.debug('streetname'+streetName);
        String cityName = '';
        if(wp.City__c != null && wp.City__c != '') {
            cityName += '+'+ wp.City__c + ',';
        }
         cityName = cityName.replace(' ', '+');
         system.debug('cityname'+cityName );
        String stateName = '';
        if(wp.State__c != null && wp.State__c != '') {
            stateName += '+' + wp.State__c + ',';
        }
         stateName = stateName.replace(' ', '+');
        system.debug('statename'+stateName );
        String ZipCode = '';
         if(wp.Zip_Code__c != null && wp.Zip_Code__c != '') {
            ZipCode += '+' + wp.Zip_Code__c + ',';
        }
        ZipCode = ZipCode.replace(' ', '+');
        system.debug('ZipCode'+ZipCode );
        String countryName = '';
        
        if(wp.Country__c != null && wp.Country__c != '') {
            countryName += '+' + wp.Country__c;
        }
        countryName = countryName.replace(' ', '+');
        system.debug('countryname'+countryName );
        // create an address string
        string address = '';
                // address += GetStreetNumber(Loc);
             address += streetName;
             address += cityName;
             address += stateName;
             address += ZipCode ;
             address += countryName;
          address = address.replace('&', 'and');
 
       // address = EncodingUtil.urlEncode(address, 'UTF-8');
           /*
            Genrating Signature.
            */
        system.debug('Address is ' +address);
        string url = '/maps/api/geocode/json?address='+address+'&client=gme-colecapitalpartners';
        
        string privateKey = Label.CryptoKey;
        privateKey = privateKey.replace('-', '+');
        privateKey = privateKey.replace('_', '/');
        
        Blob privateKeyBlob = EncodingUtil.base64Decode(privateKey);
        //Blob privateKeyBlob = Blob.valueOf(privateKey);
        Blob urlBlob = Blob.valueOf(url);
        Blob signatureBlob = Crypto.generateMac('hmacSHA1', urlBlob, privateKeyBlob);
        
        String signature =EncodingUtil.base64Encode(signatureBlob);
        signature = signature.replace('+', '-');
        signature = signature.replace('/', '_');
        
        system.debug('signature is ' +signature);
        
        string url1 = 'http://maps.googleapis.com'+url;
        system.debug('url protocol'+url1);
        url1 += '&signature=' + signature;
         system.debug('url protocol1'+url1 );
             Http h = new Http();
             HttpRequest req = new HttpRequest();
             req.setEndpoint(url1);
             req.setMethod('GET');
            req.setTimeout(60000);
            double lat = null;
            double lon = null;
        try{
            // callout
            HttpResponse res = h.send(req);
            
            // parse coordinates from response
              JSONParser parser = JSON.createParser(res.getBody());
              system.debug('********* Resquest Body ********' + res.getBody());
            List<String> splitAddress;
            String formattedAddress = '';
            Boolean addressFlag = false;
            Boolean latlngFlag = false;
             splitAddress = new List<String>();
              while(parser.nextToken() != null) {
                    
                     if(((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'formatted_address') && addressFlag == false)) {
                        parser.nextToken();
                        formattedAddress = parser.getText();
                        addressFlag = true;
                     }
                   if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                    (parser.getText() == 'location')&& latlngFlag == false){
                       parser.nextToken(); // object start
                       while (parser.nextToken() != JSONToken.END_OBJECT){
                           String txt = parser.getText();
                           parser.nextToken();
                           if (txt == 'lat')
                               lat = parser.getDoubleValue();
                           else if (txt == 'lng')
                               lon = parser.getDoubleValue();
                       }
                  system.debug('NO web propertites'+lon);
                }
                 }// End of while loop
                
                 splitAddress = address.split(',');
                 // If Lat and Lng is not Null and Formatted address is same as Provided, populate Google Address, Address_Not_Found__c and Approximate_Address__c on Risk Location
                 if( lat != null && lon != null ) {
                 system.debug('if lat and lng is not equal to');
                     // If Formatted address from Google is same as Address on Risk Location, populate Lat, Lng, Google Address and Uncheck Approximate_Address__c and Address_Not_Found__c
                     if((formattedAddress.toUpperCase()).contains((splitAddress[1].toUpperCase())) && (formattedAddress.toUpperCase()).contains((splitAddress[2].toUpperCase())) &&
                        ((formattedAddress.toUpperCase()).contains(('AU')) || (formattedAddress.toUpperCase()).contains(('AUS')) || (formattedAddress.toUpperCase()).contains(('AUSTRALIA')))) {
                        wp.location__Latitude__s = lat;
                        wp.location__Longitude__s = lon;
                        wp.Latitude_del__c= lat;
                        wp.Longitude_del__c= lon;
                        //Loc.Google_Address__c = formattedAddress;
                        wp.Approximate_Address__c = false;
                        wp.Address_Not_Found__c = false;
                     }
                     // If Formatted Address is not same as Address on Risk Location then Polpulate Lat,Lng,Google Address and Check Approximate_Address true and Address_Not_Found = false
                     else {
                        wp.location__Latitude__s = lat;
                        wp.location__Longitude__s = lon;
                        wp.Latitude_del__c= lat;
                        wp.Longitude_del__c= lon;
                        wp.Approximate_Address__c = true;
                        wp.Address_Not_Found__c = false;
                        //Loc.Google_Address__c = formattedAddress;
                     }
                 }
                 // If Lat Lng is Null then Clear Google Address, Lat, Lng and Check Address_Not_Found = true and Approximate_Address = false
                 else {
                    wp.location__Latitude__s = lat;
                    wp.location__Longitude__s = lon;
                    wp.Latitude_del__c= lat;
                    wp.Longitude_del__c= lon;
                    //Loc.Google_Address__c = '';
                    wp.Approximate_Address__c = false;
                    wp.Address_Not_Found__c = true;
                 }
                system.debug('web propertites'+wp.location__Latitude__s);
                system.debug('web propertites1'+wp.location__Longitude__s);
                system.debug('web propertitesdel'+wp.Latitude_del__c);
                system.debug('web propertitesdel1'+wp.Longitude_del__c);
                
             } catch( Exception e) {
                 system.debug('== Exception Occured ====' + e.getMessage());
             }
             address = '';
             //  update coordinates if we get back
            if (lat != null){
              
                system.debug('web propertites'+lat);
                system.debug('web propertites1'+lon);
                //WebPropertitesLocationCallouts.inFutureContext = true;
                
                updatewplist.add(wp);
            }
            }
            else{
             wp.Address_Not_Found__c = true;
             wp.Approximate_Address__c = false;
             wp.location__Latitude__s = null;
             wp.location__Longitude__s = null;
             wp.Latitude_del__c= null;
             wp.Longitude_del__c= null;
           
                updatewplist.add(wp);
            }
           } 
            ProcessorControl.inFutureContext = true;
        if (Schema.sObjectType.Web_Properties__c.isUpdateable()) 
 
            update updatewplist;
           }
}