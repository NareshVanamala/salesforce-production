public with sharing class NearByPropertiesFinal

{

    public PageReference Geopointe_DataSet() {
        string contactID = ApexPages.currentPage().getParameters().get('id');
        string URL='/apex/geopointe__Map?id='+contactID+'&wbRecordId='+contactID+'&wbField=web_properties__c&wbLookupObject=web_properties__c&wbButtonText=URLENCODE("Add to contact")&units=m'+'&range=100&ds='+system.label.Geopointe_DataSet+'&runRadialSearch=true';
        pagereference redirect = new PageReference(URL);
        redirect.setRedirect(true);
        system.debug('Hello@@'+redirect);
        system.debug('Geopointe_DataSet'+Geopointe_DataSet);
        return redirect;
    }


   /* public pag refernceGeopointe_DataSet() {
   
        return Geopointe_DataSet;
    }*/

    
     public list<Web_Properties__c>totalproperties{get;set;}
     public list<Web_Properties__c>totalproperties1{get;set;}
     public list<Web_Properties__c>totalproperties2{get;set;}
     public list<Web_Properties__c>totalproperties3{get;set;}
     public list<Web_Properties__c>totalproperties4{get;set;}
     public list<Web_Properties__c>totalproperties5{get;set;}
     public list<Web_Properties__c>totalproperties6{get;set;}
     public string Geopointe_DataSet{get;set;}
    
    
     public list<Web_Properties__c>CCPT3properties{get;set;}
     public list<Web_Properties__c>CCPT2properties{get;set;}
     public list<Web_Properties__c>CINAVproperties{get;set;}
     Public list<web_Properties__C>CCPT4properties{get;set;}
     Public list<web_Properties__C>CCPT5properties{get;set;}
     Public list<web_Properties__C>CCITproperties{get;set;}
     Public list<web_Properties__C>CCIT2properties{get;set;}
     
     
     public NearByPropertiesFinal(ApexPages.StandardController controller)
     {
       
       CCPT3properties=new list<Web_Properties__c>(); 
       CCPT2properties=new list<Web_Properties__c> ();
       CINAVproperties=new list<Web_Properties__c> ();
       CCPT4properties=new list<Web_Properties__c> ();
       CCPT5properties=new list<Web_Properties__c> ();
       CCITproperties =new list<Web_Properties__c> ();
       CCIT2properties=new list<Web_Properties__c> ();
      contact c = [select Id, Name, Geolocation__Latitude__s, Geolocation__Longitude__s from Contact where id = :ApexPages.currentPage().getParameters().get('id')];    
      
        decimal lat;
        decimal lang;
        if(c.Geolocation__Latitude__s!=null)
         lat =c.Geolocation__Latitude__s;
        else
        lat=0.00;
       
        if(c.Geolocation__Longitude__s!=null)
         lang =c.Geolocation__Longitude__s;
         else
         lang=-1.00;
       String  q = 'select Id, Name,status__c, Property_Id__c, Location__latitude__s,Location__longitude__s,Program_Id__c,State__c,City__c from Web_Properties__c ';
      //  q+= ' where DISTANCE(location__c, GEOLOCATION(';
      //  q+= String.valueof(lat) + ', ' + String.valueof(lang );
      //  q+= ' ), \'mi\') < 100';
      
       // q+=' AND ';
       q+= ' where';
        q+=' Program_Id__c';
        q+='=';
        q+=' \'CCPT3\'';
        q+= ' AND ';
        q+= ' status__c';
        q+= '=';
        q+= ' \'Owned\'';
        q+= ' AND ';
        q+= ' Location__latitude__s';
        q+= '!=null';
        q+= ' And ';
        q+= ' Location__longitude__s';
        q+= '!=null';
        q+= ' order by';
        q+= ' DISTANCE(location__c, GEOLOCATION(';
        q+= String.valueof(lat) + ', ' + String.valueof(lang);
        q+= ' ), \'mi\')';
        q+= ' Limit 3';
       system.debug('@@@@@@@@@@@'+q);
       // system.debug(database.query(q));
      totalproperties = database.query(q);
      system.debug('check'+totalproperties);
       for(Web_Properties__c ct:totalproperties)
       {
       
        if(ct.Program_Id__c=='CCPT3')
        {
        
            CCPT3properties.add(ct);
         }
           
       }
        String  q1 = 'select Id, Name,status__c,Location__latitude__s,Location__longitude__s, Property_Id__c,Program_Id__c,State__c,City__c from Web_Properties__c ';
     //   q1+= ' where DISTANCE(location__c, GEOLOCATION(';
     //   q1+= String.valueof(lat) + ', ' + String.valueof(lang );
     //   q1+= ' ), \'mi\') < 100';
     //   q1+=' AND ';
          q1+= ' where ';
          
         q1+=' Program_Id__c';
        q1+='=';
        q1+=' \'CCPT2\'';
        q1+= ' AND ';
        q1+= ' status__c';
        q1+= '=';
        q1+= ' \'Owned\'';
        q1+= ' AND ';
        q1+= ' Location__latitude__s';
        q1+= '!=null';
        q1+= ' And ';
        q1+= ' Location__longitude__s';
        q1+= '!=null';
        q1+= ' order by';
        q1+= ' DISTANCE(location__c, GEOLOCATION(';
        q1+= String.valueof(lat) + ', ' + String.valueof(lang);
        q1+= ' ), \'mi\')';
        q1+= ' Limit 3';
       system.debug('@@@@@@@@@@@'+q1);
       // system.debug(database.query(q));
      totalproperties1 = database.query(q1);
      system.debug('check'+totalproperties1);
       for(Web_Properties__c ct1:totalproperties1)
       {
       
        if(ct1.Program_Id__c=='CCPT2')
        {
        
            CCPT2properties.add(ct1);
         }
           
       }
       
        String  q2 = 'select Id, Name,status__c,Location__latitude__s,Location__longitude__s, Property_Id__c,Program_Id__c,State__c,City__c from Web_Properties__c ';
       // q2+= ' where DISTANCE(location__c, GEOLOCATION(';
      //  q2+= String.valueof(lat) + ', ' + String.valueof(lang );
      //  q2+= ' ), \'mi\') < 100';
      //  q2+=' AND ';
          q2+= ' where ';
        q2+=' Program_Id__c';
        q2+='=';
        q2+=' \'CCPT4\'';
        q2+= ' AND ';
        q2+= ' status__c';
        q2+= '=';
        q2+= ' \'Owned\'';
        q2+= ' AND ';
        q2+= ' Location__latitude__s';
        q2+= '!=null';
        q2+= ' And ';
        q2+= ' Location__longitude__s';
        q2+= '!=null';
        q2+= ' order by';
        q2+= ' DISTANCE(location__c, GEOLOCATION(';
        q2+= String.valueof(lat) + ', ' + String.valueof(lang);
        q2+= ' ), \'mi\')';
        q2+= ' Limit 3';
       system.debug('@@@@@@@@@@@'+q2);
       // system.debug(database.query(q));
      totalproperties2 = database.query(q2);
      system.debug('check'+totalproperties2);
       for(Web_Properties__c ct2:totalproperties2)
       {
       
        if(ct2.Program_Id__c=='CCPT4')
        {
        
            CCPT4properties.add(ct2);
         }
           
       }
       
        String  q3 = 'select Id, Name,status__c,Location__latitude__s,Location__longitude__s, Property_Id__c,Program_Id__c,State__c,City__c from Web_Properties__c ';
       // q3+= ' where DISTANCE(location__c, GEOLOCATION(';
       // q3+= String.valueof(lat) + ', ' + String.valueof(lang );
      //  q3+= ' ), \'mi\') < 100';
      //  q3+=' AND ';
         q3+= ' where ';
        q3+=' Program_Id__c';
        q3+='=';
        q3+=' \'CCPT5\'';
        q3+= ' AND ';
        q3+= ' status__c';
        q3+= '=';
        q3+= ' \'Owned\'';
        q3+= ' AND ';
        q3+= ' Location__latitude__s';
        q3+= '!=null';
        q3+= ' And ';
        q3+= ' Location__longitude__s';
        q3+= '!=null';
        q3+= ' order by';
        q3+= ' DISTANCE(location__c, GEOLOCATION(';
        q3+= String.valueof(lat) + ', ' + String.valueof(lang);
        q3+= ' ), \'mi\')';
        q3+= ' Limit 3';
       system.debug('@@@@@@@@@@@'+q3);
       // system.debug(database.query(q));
      totalproperties3 = database.query(q3);
      system.debug('check'+totalproperties3);
       for(Web_Properties__c ct3:totalproperties3)
       {
       
        if(ct3.Program_Id__c=='CCPT5')
        {
        
            CCPT5properties.add(ct3);
         }
           
       }
       
        String  q4 = 'select Id, Name,status__c,Location__latitude__s,Location__longitude__s, Property_Id__c,Program_Id__c,State__c,City__c from Web_Properties__c ';
      //  q4+= ' where DISTANCE(location__c, GEOLOCATION(';
      //  q4+= String.valueof(lat) + ', ' + String.valueof(lang );
     //   q4+= ' ), \'mi\') < 100';
     //   q4+=' AND ';
         q4+= ' where ';
        q4+=' Program_Id__c';
        q4+='=';
        q4+=' \'CINAV\'';
        q4+= ' AND ';
        q4+= ' status__c';
        q4+= '=';
        q4+= ' \'Owned\'';
        q4+= ' AND ';
        q4+= ' Location__latitude__s';
        q4+= '!=null';
        q4+= ' And ';
        q4+= ' Location__longitude__s';
        q4+= '!=null';
        q4+= ' order by';
        q4+= ' DISTANCE(location__c, GEOLOCATION(';
        q4+= String.valueof(lat) + ', ' + String.valueof(lang);
        q4+= ' ), \'mi\')';
        q4+= ' Limit 3';
       system.debug('@@@@@@@@@@@'+q4);
       // system.debug(database.query(q));
      totalproperties4 = database.query(q4);
      system.debug('check'+totalproperties4);
       for(Web_Properties__c ct4:totalproperties4)
       {
       
        if(ct4.Program_Id__c=='CINAV')
        {
        
            CINAVproperties.add(ct4);
         }
           
       }
   
     String  q5 = 'select Id, Name,status__c,Location__latitude__s,Location__longitude__s, Property_Id__c,Location__c,Program_Id__c,State__c,City__c from Web_Properties__c ';
      //  q4+= ' where DISTANCE(location__c, GEOLOCATION(';
      //  q4+= String.valueof(lat) + ', ' + String.valueof(lang );
     //   q4+= ' ), \'mi\') < 100';
     //   q4+=' AND ';
         q5+= ' where';
        q5+=' Program_Id__c';
        q5+='=';
        q5+=' \'CCIT\'';
        q5+= ' AND ';
        q5+= ' status__c';
        q5+= '=';
        q5+= ' \'Owned\'';
        q5+= ' AND ';
        q5+= ' Location__latitude__s';
        q5+= '!=null';
        q5+= ' And ';
        q5+= ' Location__longitude__s';
        q5+= '!=null';
        q5+= ' order by';
        q5+= ' DISTANCE(location__c, GEOLOCATION(';
        q5+= String.valueof(lat) + ', ' + String.valueof(lang);
        q5+= ' ), \'mi\')';
        q5+= ' Limit 3';
       system.debug('@@@@@@@@@@@'+q5);
      totalproperties5 = database.query(q5);
      system.debug('check CCIT'+totalproperties5);
      
       for(Web_Properties__c ct5:totalproperties5)
       {
       system.debug('&&&&&&&&&&&&&&&&');
        if(ct5.Program_Id__c=='CCIT')
        {
        system.debug('*****************');
            CCITproperties.add(ct5);
         }
           
       }
          
       String  q6 = 'select Id, Name,status__c,Location__latitude__s,Location__longitude__s, Property_Id__c,Program_Id__c,State__c,City__c from Web_Properties__c ';
     //   q6+= ' where DISTANCE(location__c, GEOLOCATION(';
     //   q6+= String.valueof(lat) + ', ' + String.valueof(lang );
     //   q6+= ' ), \'mi\') < 100';
     //   q6+=' AND ';
         q6+= ' where ';
        q6+=' Program_Id__c';
        q6+='=';
        q6+=' \'CCIT2\'';
        q6+= ' AND ';
        q6+= ' status__c';
        q6+= '=';
        q6+= ' \'Owned\'';
        q6+= ' AND ';
        q6+= ' Location__latitude__s';
        q6+= '!=null';
        q6+= ' And ';
        q6+= ' Location__longitude__s';
        q6+= '!=null';
        q6+= ' order by';
        q6+= ' DISTANCE(location__c, GEOLOCATION(';
        q6+= String.valueof(lat) + ', ' + String.valueof(lang);
        q6+= ' ), \'mi\')';
        q6+= ' Limit 3';
       system.debug('@@@@@@@@@@@'+q6);
       // system.debug(database.query(q));
      totalproperties6 = database.query(q6);
      system.debug('check'+totalproperties6);
       for(Web_Properties__c ct6:totalproperties6)
       {
       
        if(ct6.Program_Id__c=='CCIT2')
        {
        
            CCIT2properties.add(ct6);
         }
           
       }
       
      
     }

}