@ISTest
private class Reittest
{

static testMethod void myUnitTest3771(){
Test.startTest();
        Account Acc = new Account(Name='Acc');
        Insert Acc;
        Contact con =new Contact(LastName='LastName',AccountId=Acc.Id);
        Insert con;  
          System.assertEquals(con.AccountId,Acc.Id);
     
        REIT_Investment__c Reit1 = new REIT_Investment__c(Rep_Contact__c=con.Id,Fund__c ='3771',Current_Capital__c=10000,Deposit_Date__c=system.today());
        Insert Reit1;
Test.stopTest();
 }
 
 static testMethod void myUnitTest3774(){
 Test.startTest();
        Account Acc2 = new Account(Name='Acc2');
        Insert Acc2;
       // Contact con2 =new Contact(LastName='LastName2',AccountId=Acc2.Id,Income_NAV_A__c=0);
       Contact con2 =new Contact(LastName='LastName2',AccountId=Acc2.Id);
        Insert con2;
        
        System.assertEquals(con2.AccountId,Acc2.Id);

        REIT_Investment__c Reit2 = new REIT_Investment__c(Rep_Contact__c=con2.Id,Fund__c ='3774',Current_Capital__c=10000,Deposit_Date__c=system.today());
        Insert Reit2;
        Test.stopTest();
 }
static testMethod void myUnitTest3775(){
Test.startTest();
        Account Acc3 = new Account(Name='Acc3');
        Insert Acc3;
       // Contact con3 =new Contact(LastName='LastName3',AccountId=Acc3.Id,Income_NAV_I__c=0);
       Contact con3 =new Contact(LastName='LastName3',AccountId=Acc3.Id);
        Insert con3;
       System.assertEquals(con3.AccountId,Acc3.Id);
       
        REIT_Investment__c Reit3 = new REIT_Investment__c(Rep_Contact__c=con3.Id,Fund__c ='3775',Current_Capital__c=10000,Deposit_Date__c=system.today());
        Insert Reit3;
        Test.stopTest();
 } 

}