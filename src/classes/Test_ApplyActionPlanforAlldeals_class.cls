@isTest(SeeAlldata = true)
public class Test_ApplyActionPlanforAlldeals_class
{

static testMethod void TestApplyActionPlanforAlldeals()
{
Portfolio__c pc= new Portfolio__c();
pc.name='Testing1';
Insert pc; 
 
 
String Query; 
Deal__c d = new Deal__c();
d.Name ='Test12';  
d.Portfolio_Deal__c=pc.id;  
d.Build_to_Suit_Type__c ='Current';
d.Estimated_COE_Date__c = date.today();
d.HVAC_Warranty_Status__c ='Received';
d.Ownership_Interest__c = 'Fee Simple';
d.Roof_Warranty_Status__c = 'Received';
d.Zip_Code__c = '98033';
d.Create_Workspace__c =true; 
insert d ;


Deal__c d1 = new Deal__c();
d1.Name ='Test13'; 
d1.Portfolio_Deal__c=pc.id;  
d1.Build_to_Suit_Type__c ='Current';
d1.Estimated_COE_Date__c = date.today();
d1.HVAC_Warranty_Status__c ='Received';
d1.Ownership_Interest__c = 'Fee Simple';
d1.Roof_Warranty_Status__c = 'Received';
d1.Zip_Code__c = '98033';
d1.Create_Workspace__c =true; 
insert d1 ;
System.assertEquals(d1.Portfolio_Deal__c,pc.id );

Template__c temprec = new Template__c();
temprec.Name = 'Template';
temprec.deal__c=d.id;

insert temprec;

System.assertEquals(d1.Portfolio_Deal__c,pc.id );

Template__c temp = new Template__c();
Task_Plan__c tp=new Task_Plan__c();
tp.Date_Received__c=date.today();

Query='select id, name from Deal__c where id=:d1.id';
ApexPages.StandardController sc = new ApexPages.standardController(temp);
ApexPages.currentPage().getParameters().put('id', null);
apexpages.currentpage().getparameters().put('objectId', d.id);
ApplyActionPlanforAlldeals APD = new ApplyActionPlanforAlldeals(sc);
APD.getRelatedObjectPicklist();
APD.checkDuplicateTasks();
APD.getdateFields();
APD.CalldateFields();
APD.getarrangetask(); 
APD.addTask();
APD.actionPlansubTasks = new List<Task_Plan__c>();
apexpages.currentpage().getparameters().put('parentindex', '1');
APD.actionPlansubTasks.add(tp);
APD.actionPlanTasks=new List<Task_Plan__c>();
APD.actionPlanTasks.add(tp);
APD.addSubTask();
APD.templateSelected='Template';
APD.actionPlanTasks=new List<Task_Plan__c>();
APD.actionPlanTasks.add(tp);
APD.mapTask = new Map<Integer,List<Task_Plan__c>>();
APD.mapTask.put(1,APD.actionPlanTasks);

apexpages.currentpage().getparameters().put('removeRec', '1');
APD.removetask();
apexpages.currentpage().getparameters().put('SubtaskParent', '1');
apexpages.currentpage().getparameters().put('SubtaskIndex', '1');
List<Task_Plan__c> TPL = new List<Task_Plan__c>();
TPL.add(tp);
APD.mapTask = new Map<Integer,List<Task_Plan__c>>();
APD.mapTask.put(1,TPL); 
APD.removesubtask();

apexpages.currentpage().getparameters().put('id', temprec.id);
APD.NutralAct();
APD.nutralexists();
APD.gettemplatesOptions();
APD.changeTemplate();
APD.Cancel1();

temprec = new Template__c();
temprec.Name = 'Template';
temprec.deal__c=d.id;
insert temprec;

tp = new Task_Plan__c ();
tp.Name='test';
tp.priority__c = 'Medium';
tp.Index__c = 1;
tp.Assigned_To__c = Userinfo.getUserId();
tp.Template__c =temprec.Id ; 
tp.Date_Received__c=date.today();
tp.Field_to_update_Date_Received__c=string.valueof(date.today());

APD.actionPlan=temp; 

APD.templateSelected = temprec.Name;
TPL = new List<Task_Plan__c>();
tp.index__c=1;
TPL=new List<Task_Plan__c>();
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Dead_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Site_Visit_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Appraisal_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Passed_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Accenture_Due_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_COE_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Go-Hard Date', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Hard_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Rep_Burn_Off__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_SP_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Site_Visit_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Purchase_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Query_Sorted_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_Hard_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Passed_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Contract_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_COE_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Closing_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Hard_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='LOI_Signed_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_SP_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='LOI_Sent_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Purchase_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Lease_Abstract_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_Hard_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Lease_Expiration__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Contract_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Open_Escrow_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Closing_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Dead_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='LOI_Signed_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Tenant_ROFR_Waiver__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='LOI_Sent_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Date_Audit_Completed__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Lease_Abstract_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Date_Identified__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Lease_Expiration__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Investment_Committee_Approval__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Open_Escrow_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Part_1_Start_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Purchase_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Tenant_ROFR_Waiver__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Date_Audit_Completed__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Date_Identified__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Investment_Committee_Approval__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Part_1_Start_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estoppel__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);
tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Go_Hard_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
TPL.add(tp);

APD.mapTask = new Map<Integer,List<Task_Plan__c>>();
APD.mapTask.put(1,TPL); 
APD.actionPlanTasks = TPL;

APD.mapTask = new Map<Integer,List<Task_Plan__c>>();
APD.mapTask.put(0,TPL);
APD.save();
APD.changeTemplate();
tp.Template__c=temprec.id;
insert tp;
apexpages.currentpage().getparameters().put('id', tp.id);
APD.nutralexists();

MassApplyActionPlanforAlldeals batch = new MassApplyActionPlanforAlldeals(d.id,temprec.id);
Id batchId = Database.executeBatch(batch,5);   

}
    

}