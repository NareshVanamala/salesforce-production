Public with sharing class PropertystatusControllerforParalegal
{
    public map<String,list<Deal__c>> dealListMap { get; set; }
    public map<id,list<Task>> TaskListMap { get; set; }
    public List<String> sortedParalegals { get; set; }
    public List<User> sortedParalegalsid { get; set; }
    public String ids {get;set;}
    public List<String> selectedparalegalids = new List<String>();
    public list<Deal__c>paralegaldeals= new list<Deal__c>();
    public list<MyWrapper>Paralegals;
    public list<Task>tasklist;
    public list<id>dealids; 
    
    public  PropertystatusControllerforParalegal() 
   {
       sortedParalegalsid =new list<User>();
       Paralegals=new list<MyWrapper>();
       dealListMap=new map<String,list<Deal__c>>();
       TaskListMap= new map<id,list<Task>>(); 
       tasklist= new list<Task>();
       dealids= new list<id>();
       //sortedParalegals= new list<String>();
       ids = ApexPages.currentPage().getParameters().get('paralegalIds');
       if(ids!=null)
       {
         selectedparalegalids = ids.split(',');
       }
      system.debug('get ne the ids..'+selectedparalegalids );
      paralegaldeals=[select id, name,Deal_Status__c,Paralegal__c,Contract_Price__c,Attorney__c,Attorney__r.name,Fund__c,Paralegal__r.name from Deal__c where (Deal_Status__c='Signed LOI' or Deal_Status__c='Under Contract/ Signed PSA' or Deal_Status__c='Escrow Pending' or Deal_Status__c='Escrow Study' or Deal_Status__c='Escrow Hard') and Paralegal__c in:selectedparalegalids order by Paralegal__r.name,name ];
      sortedParalegalsid =[select id, name from user where id in:selectedparalegalids order by name];
      //for(User u:sortedParalegalsid )
      //sortedParalegals.add(u.name); 
      
      for(Deal__c ri :paralegaldeals)
      { 
         dealids.add(ri.id);
           if(dealListMap.containsKey(ri.Paralegal__r.name))
             dealListMap .get(ri.Paralegal__r.name).add(ri);
          else
            dealListMap.put(ri.Paralegal__r.name, new List<Deal__c >{ri});
       } 
       
      tasklist=[select id,whatid,status, subject,Date_Ordered__c,ActivityDate,Description,Date_Received__c from Task where whatid in:dealids order by subject ASC];
      For(Task tsk:tasklist)
      {
            
           if(TaskListMap.containsKey(tsk.whatid))
             TaskListMap.get(tsk.whatid).add(tsk);
          else
            TaskListMap.put(tsk.whatid, new List<Task>{tsk});
 
      }

   }
 
   public list<MyWrapper>getParalegals()
   {
       /*for(User u:sortedParalegalsid )
       {    
             MyWrapper w= new MyWrapper();
             w.name=u.name;
             w.deallist=dealListMap.get(u.name);
             for(Deal__c dc:w.deallist)
             {
               w.tasks= taskListMap.get(dc.id); 
             }
              Paralegals.add(w);
          } */
          
      for(Deal__c dc:paralegaldeals)
      {
        MyWrapper w= new MyWrapper();
        w.name=dc.Paralegal__r.name; 
        w.status=dc.Deal_Status__c;
        w.fundname=dc.Fund__c;
        w.ContractPrice=dc.Contract_Price__c;
        w.Attorny=dc.Attorney__r.name;
        w.dealname=dc.name;
        w.tasks= TaskListMap.get(dc.id); 
        Paralegals.add(w);      
      }
       return Paralegals;
   }
   
  public class MyWrapper
  {
       Public string name{get;set;}
        Public string fundname{get;set;}  
        Public string dealname{get;set;} 
        Public decimal ContractPrice{get;set;}
       Public string Attorny{get;set;}
       Public string status{get;set;}
       //public list<Deal__c>deallist{get;set;}
      public list<Task>tasks{get;set;}
    
    Public  MyWrapper()
   { 
     
     
     //deallist= new list<Deal__c>(); 
      tasks= new list<Task>();
   }
       
  }

}