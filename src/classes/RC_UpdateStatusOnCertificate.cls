global class RC_UpdateStatusOnCertificate
{
    @InvocableMethod
    public static void UpdateCertificateStatus(list<String> propid )
    {
         list<id> certreturnlist=new list<id>(); 
         list<RC_Property__c> proplist =new list<RC_Property__c>();
         list<RC_Certificate_of_Insurance__c> myCert=new list<RC_Certificate_of_Insurance__c>();
         list<RC_Certificate_of_Insurance__c >updatedCertlist=new list<RC_Certificate_of_Insurance__c>();
       
       
        //get the property id
         proplist =[select id,Name,Ownership_Type__c from RC_Property__c where id in:propid]; 
         Id propid1=proplist[0].Id;
         
         //get the locations of the property
          list<RC_Location__c > LocList =new list<RC_Location__c >();
          LocList=[SELECT Id,Status__c,Property__c from RC_Location__c where Property__c=:propid1 ];
           if(LocList.size()>0)
           {
              for(RC_Location__c rct:LocList)
                certreturnlist.add(rct.id);
           }   
         
         //get the certificate of the locations
        myCert=[SELECT Id,Location__c,Status__c FROM RC_Certificate_of_Insurance__c where Location__c in:certreturnlist ];
           
           if(myCert.size()>0)
           {
              for(RC_Certificate_of_Insurance__c certins:myCert)
              {
                 certins.Status__c='InActive';
                 updatedCertlist.add(certins);
              }
              
           }   
           //update the certificate
           if (Schema.sObjectType.RC_Certificate_of_Insurance__c.isUpdateable()) 

          update  updatedCertlist;
   
      }

 }