global class NewProducerNewDesign implements Database.Batchable<SObject>
{
     
    Map<ID,Task> ContactTaskMap = new Map<ID,Task>();
    Map<ID,Task> ContactVirtualTaskMap = new Map<ID,Task>();

    global Database.QueryLocator Start(Database.BatchableContext BC)
    {
        Date d = system.today();
        d = d.addDays(-167);
        string name='Virtual Meeting Scheduled';
        string name1='Webcast date';
        string name2='External Appointment';
        string name3='virtual presentation';
        string name4='completed';
        Id rrrecordtype = [select Id, Name,DeveloperName from RecordType where DeveloperName ='NonInvestorRepContact' and SobjectType = 'Contact'].id;
  //**********Fetch all the RRContacts with first producer date satisfying upper and lower limits*********/   
        string query='select id,mailingstate,name,Territory__c,Last_Successful_Contact__c,Newproducer_onboarding_status__c,Last_Producer_Date__c,first_producer_date__c,Email ,(select type,id,subject,whoid,ownerid,lastmodifieddate,status from tasks WHERE (subject=:name OR subject=:name1 OR subject=:name2 OR subject=:name3 ) AND status=:name4) from contact WHERE recordtypeid=\''+rrrecordtype+'\' AND first_producer_date__c >=:d and Control_Group__c=true and Subscribed__c=true';
        return database.getQueryLocator(query);
    }   
    global void Execute(Database.BatchableContext BC,List<Contact> Scope)
    { 
                
      profile p = [select id,name from profile where name='Internal Sales'];  
    //*************Fetching 'Internal Sales' users*********************     
      list<User> ulist=[select id,name,Territory__c,isactive from User where Profileid=:p.id];
      Map<String,id> Usermap= new Map<String,id>();
   
    //We need usermap while creating CCL record . We can not write SOQL inside for loop.
      for(User u:ulist)
      {
      Usermap.put(u.Territory__c,u.id);
      }
    //**************Fetching Emial Templates*****************************  
      list<EmailTemplate> lstEmailTemplates = [select Id,Name,DeveloperName from EmailTemplate where( Name=: 'Webcast Link' OR Name=: 'Brain Shark Link' OR Name=:'Thank You' OR Name=:'Welcome kit for NY and Ohio Territory Producers')];
      map<string,string> emailmap = new map<string,string>();
      
      if(lstEmailTemplates!=null && lstEmailTemplates.size()>0)
        {
            for(emailtemplate e: lstEmailTemplates)
            emailmap.put(e.name,e.id);
        }
      for(Contact C:Scope)
      {
        for(task t:C.tasks)
        {
           if((t.subject=='External Appointment')&&(t.type=='Field'))
            ContactTaskMap.put(t.whoid,t);
           else if(((t.subject=='virtual presentation')&&(t.type=='Virtual'))||((t.subject=='virtual Meeting Scheduled')&&(t.type=='Virtual'))) 
            ContactVirtualTaskMap.put(t.whoid,t);
 
        } 
        
        if(c.first_producer_date__c==c.last_producer_date__c)
        {
          if(((c.First_Producer_Date__c==system.today())&&(c.Newproducer_onboarding_status__c==null))||((c.First_Producer_Date__c>=system.today()-3)&&(c.Newproducer_onboarding_status__c==null)))  
          {
                LifeCycleHelper.insertccl(c.id,usermap.get(c.territory__c),'New Producer Thank You & Schedule Virtual');
           if(c.email!=null)
           {
             if(c.mailingstate=='OH'||c.mailingstate=='MA')
             {
                LifeCycleHelper.sendEmail(c.id,emailmap.get('Thank You'));  
             } 
             else 
              {  
                LifeCycleHelper.sendEmail(c.id,emailmap.get('Thank You')); 
              } 
           }      
                LifeCycleHelper.newproducerstatus('New Producer Thank You & Schedule Virtual',c.id);
          }   
                  
          if((ContactVirtualTaskMap.get(c.id)!=null))
          {
          //This is  Responded  track for Virtual Appointment where it already have completed 
          //This is for BRD scetion # 1.4. Touch 3 
                if((ContactVirtualTaskMap.get(c.id).lastmodifieddate>=system.today()-1)&&(c.First_Producer_Date__c>=system.today()-90)&&((c.Newproducer_onboarding_status__c=='New Producer Thank You & Schedule Virtual')||(c.Newproducer_onboarding_status__c=='New Producer Follow Up & Schedule Virtual 1')||(c.Newproducer_onboarding_status__c=='New Producer Follow Up & Schedule Virtual 2')||(c.Newproducer_onboarding_status__c=='New Producer Follow Up & Schedule Virtual 3')))    
                {
                    system.debug('//////////////////////////I am in the  Responded  track ....');
                    LifeCycleHelper.insertccl(c.id,usermap.get(c.territory__c),'New Producer Virtual Follow Up & Schedule External');
                    LifeCycleHelper.newproducerstatus('New Producer Virtual Follow Up & Schedule External',c.id);
                } 
                if((ContactVirtualTaskMap.get(c.id).lastmodifieddate>=system.today()-7)&&(c.First_Producer_Date__c>=system.today()-97)&&(c.Last_Successful_Contact__c>=system.today()-7)&&(c.Newproducer_onboarding_status__c=='New Producer Virtual Follow Up & Schedule External'))    
                {
                        LifeCycleHelper.insertccl(c.id,usermap.get(c.territory__c),'New Producer Follow Up & Schedule External 1');
                        LifeCycleHelper.newproducerstatus('New Producer Follow Up & Schedule External 1',c.id);
                } 
                    //This is for BRD scetion #3.2 Touch2
                if((ContactVirtualTaskMap.get(c.id).lastmodifieddate>=system.today()-17)&&(c.First_Producer_Date__c>=system.today()-107)&&(c.Last_Successful_Contact__c>=system.today()-17)&&(c.Last_Successful_Contact__c<=system.today()-8)&&(c.Newproducer_onboarding_status__c=='New Producer Follow Up & Schedule External 1'))    
                {
                        LifeCycleHelper.insertccl(c.id,usermap.get(c.territory__c),'New Producer Follow Up & Schedule External 2');
                        LifeCycleHelper.newproducerstatus('New Producer Follow Up & Schedule External 2',c.id);
                }
          }
          if(ContactTaskMap.get(c.id)!=null) 
          {     
          //This is   Responded   track for External Appointment where it already have completed 
          //This is Merged BRD section # 1.6 & 1.7. Touch 6   
                if((ContactTaskMap.get(c.id).lastmodifieddate>=system.today()-2)&&(c.First_Producer_Date__c>=system.today()-150)&&((c.Newproducer_onboarding_status__c=='New Producer Virtual Follow Up & Schedule External')||(c.Newproducer_onboarding_status__c=='New Producer Follow Up & Schedule External 1')||(c.Newproducer_onboarding_status__c=='New Producer Follow Up & Schedule External 2')||(c.Newproducer_onboarding_status__c=='New Producer Thank You & Schedule Virtual')))    
                {
                    LifeCycleHelper.insertccl(c.id,usermap.get(c.territory__c),'New Producer External Appt Follow Up Email & Call');
                    LifeCycleHelper.newproducerstatus('New Producer External Appt Follow Up Email & Call',c.id);
                }
                   
          //This is for BRD scetion #4.1 Touch 1
                if((ContactTaskMap.get(c.id).lastmodifieddate>=system.today()-7)&&(c.First_Producer_Date__c>=system.today()-157)&&(c.Last_Successful_Contact__c>=system.today()-7)&&(c.Newproducer_onboarding_status__c=='New Producer External Appt Follow Up Email & Call'))    
                {
                    LifeCycleHelper.insertccl(c.id,usermap.get(c.territory__c),'New Producer External Appointment Follow Up 1');
                    LifeCycleHelper.newproducerstatus('New Producer External Appointment Follow Up 1',c.id);
                }
                
          //This is for BRD scetion #4.2 Touch 2
                if((ContactTaskMap.get(c.id).lastmodifieddate>=system.today()-17)&&(c.First_Producer_Date__c>=system.today()-167)&&(c.Last_Successful_Contact__c>=system.today()-17)&&(c.Last_Successful_Contact__c<=system.today()-8)&&(c.Newproducer_onboarding_status__c=='New Producer External Appointment Follow Up 1'))    
                {
                    LifeCycleHelper.insertccl(c.id,usermap.get(c.territory__c),'New Producer External Appointment Follow Up 2');
                    LifeCycleHelper.newproducerstatus('New Producer External Appointment Follow Up 2',c.id);
                }  
          
          }
          // This will be Executed when Virtual or External hasn't been scheduled yet...
          //This is for BRD section #2.1 Touch 1
                 if((c.First_Producer_Date__c==system.today()-5)&&(c.Last_Successful_Contact__c>=system.today()-5)&&(c.Newproducer_onboarding_status__c=='New Producer Thank You & Schedule Virtual'))    
                  {
                      /*  if(emailmap.containsKey('Brain Shark Link'))
                        {
                            LifeCycleHelper.sendEmail(c.id,emailmap.get('Brain Shark Link'));
                        }
                        */ 
                            LifeCycleHelper.insertccl(c.id,usermap.get(c.territory__c),'New Producer Brainshark Was Sent');   
                  }


//**************Below is Non Responded Track where virtual appointmnet did not get scheduled and so there is no task in the ContactVirtualTaskMap.        
          
          if(ContactVirtualTaskMap.get(c.id)==null)
            {
                //This is for BRD scetion #2.2 Touch 2
                 if((c.First_Producer_Date__c==system.today()-6)&&(c.Last_Successful_Contact__c>=system.today()-6)&&(c.Newproducer_onboarding_status__c=='New Producer Thank You & Schedule Virtual'))    
                  {
                        LifeCycleHelper.insertccl(c.id,usermap.get(c.territory__c),'New Producer Follow Up & Schedule Virtual 1');
                        LifeCycleHelper.newproducerstatus('New Producer Follow Up & Schedule Virtual 1',c.id);
                  } 
                //This is for BRD scetion #2.3 Touch3 
                  if((c.First_Producer_Date__c>=system.today()-15)&&(c.first_producer_date__c<=system.today()-7)&&(c.Last_Successful_Contact__c>=system.today()-15)&&(c.Newproducer_onboarding_status__c=='New Producer Follow Up & Schedule Virtual 1'))    
                  {
                        LifeCycleHelper.insertccl(c.id,usermap.get(c.territory__c),'New Producer Follow Up & Schedule Virtual 2');
                        LifeCycleHelper.newproducerstatus('New Producer Follow Up & Schedule Virtual 2',c.id);
                  } 
                //This is for BRD scetion #2.4 Touch4 
                 if((c.First_Producer_Date__c>=system.today()-30)&&(c.first_producer_date__c<=system.today()-16)&&(c.Last_Successful_Contact__c>=system.today()-30)&&(c.Newproducer_onboarding_status__c=='New Producer Follow Up & Schedule Virtual 2'))    
                  {
                        LifeCycleHelper.insertccl(c.id,usermap.get(c.territory__c),'New Producer Follow Up & Schedule Virtual 3');
                        LifeCycleHelper.newproducerstatus('New Producer Follow Up & Schedule Virtual 3',c.id);
                       /* if(emailmap.containsKey('Webcast Link'))
                        {
                            LifeCycleHelper.sendEmail(c.id,emailmap.get('webcast Link'));
                        }*/   
                  } 
            }
        }
          
          if(c.first_producer_date__c!=c.last_producer_date__c)
          {
           LifeCycleHelper.newproducerstatus('Repeat Producer',c.id);
          }
          if(c.first_producer_date__c==c.last_producer_date__c && c.Newproducer_onboarding_status__c !='One Time Producer'&& c.first_producer_date__c==system.today()-167 && ContactVirtualTaskMap.get(c.id)!=null && ContactTaskMap.get(c.id)!=null)
          {
           LifeCycleHelper.newproducerstatus('One Time Producer',c.id);
          }
          if(c.first_producer_date__c==c.last_producer_date__c && c.first_producer_date__c==system.today()-33 && ContactVirtualTaskMap.get(c.id)==null && ContactTaskMap.get(c.id)==null)
          {
           LifeCycleHelper.newproducerstatus('One Time Producer',c.id);
          }
      }
    }

    global void finish(Database.BatchableContext BC)
    {
       user u=[select id,name,email from user where id=:userinfo.getuserid()];
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {u.email};
       //String[] bccAddresses = new String[] {'sandeep.mariyala@polarisft.com'};
       mail.setToAddresses(toAddresses);
       mail.setBccSender(true);
       //mail.setBccAddresses(bccAddresses);
       mail.setPlainTextBody('Done');
       mail.setSubject('Your Batch class processing has been finished.Please verify the changes in contact');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    } 
        
}