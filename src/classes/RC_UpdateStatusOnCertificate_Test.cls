@isTest
 Public class RC_UpdateStatusOnCertificate_Test
{
  static testmethod void testUpdateStatusOnCertificate()
  {  
      Account ac= new Account();
      ac.name='Test-Account';
      insert ac;
    
     RC_Property__c PRT=new RC_Property__c();
     PRT.name='Amazon DC 2014 USAA~A11123';
     PRT.Occupancy__c='Single-Tenant';
     insert PRT;
     
     RC_Location__c LC=new RC_Location__c();
     LC.name='Amazon DC 2014 USAA~A11123';
     LC.Entity_Tenant_Code__c='12345';
     LC.Property__c=PRT.id;
     LC.Account__c=ac.id;
     insert LC;
     
          System.assertEquals(PRT.name,LC.name);

     RC_Certificate_of_Insurance__c certific=new RC_Certificate_of_Insurance__c();
     certific.Location__c=LC.id;
      insert certific;
      
    PRT.Ownership_Type__c='Sold';
      update PRT;
   }
   
 }