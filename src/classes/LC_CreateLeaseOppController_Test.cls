@isTest
Public class LC_CreateLeaseOppController_Test
{
  static testmethod void testCreatelease()
  {
        
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         System.assertEquals( mc.name,'Test_MRI_Property');

         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
     
    
        
    //********************with record type New**********************    
     
         RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'New_Lease_Opportunity' limit 1]; 
        //created New lease with look up to lease
         string recid=NewRecType.id;
         LC_LeaseOpprtunity__c lc1= new LC_LeaseOpprtunity__c();
         lc1.name='TestNEWOpportunity';
         lc1.Lease_Opportunity_Type__c='Lease - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.LC_OtherOpportunity_Description__c='Thisis my description';
         lc1.Base_Rent__c=12345;
         lc1.recordtypeid=recid;
         insert lc1;
         
         System.assertEquals( lc1.name,'TestNEWOpportunity');

         LC_Clauses__c cls1 = New LC_Clauses__c();
         cls1.Name='125PEU';
         cls1.Clause_Type__c= 'Permitted Use';
         insert cls1 ;
         
         LC_ClauseType__c clt1 =new LC_ClauseType__c();    
         clt1.Lease_Opportunity__c=lc1.Id;
         clt1.Clauses__c=cls1.Id;
         clt1.Name = cls1.Name;  
         clt1.ClauseType_Name__c=cls1.Clause_Type__c;
         clt1.Clause_Description__c='Test desc';
         insert clt1;
         
         LC_Clauses__c cls2 = New LC_Clauses__c();
         cls2.Name='302COM';
         cls2.Clause_Type__c= 'Commissions';
         insert cls2 ;
         
         LC_ClauseType__c clt2 =new LC_ClauseType__c();    
         clt2 .Lease_Opportunity__c=lc1.Id;
         clt2 .Clauses__c=cls2 .Id;
         clt2 .Name = cls2 .Name;  
         clt2 .ClauseType_Name__c=cls2 .Clause_Type__c;
         clt2 .Clause_Description__c='Test desc';
         insert clt2; 
         
         LC_Clauses__c cls3 = New LC_Clauses__c();
         cls3.Name='305FIX';
         cls3.Clause_Type__c= 'Fixturization Period';
         insert cls3 ;
         
         LC_ClauseType__c clt3 =new LC_ClauseType__c();    
        clt3 .Lease_Opportunity__c=lc1.Id;
        clt3 .Clauses__c=cls3 .Id;
        clt3 .Name = cls3 .Name;  
        clt3 .ClauseType_Name__c=cls3 .Clause_Type__c;
        clt3 .Clause_Description__c='Test desc';
        insert clt3 ; 
         
         LC_Clauses__c cls4 = New LC_Clauses__c();
         cls4.Name='306NNN';
         cls4.Clause_Type__c= 'NNN';
         insert cls4;
         
          LC_ClauseType__c  clt4 =new LC_ClauseType__c();    
        clt4 .Lease_Opportunity__c=lc1.Id;
        clt4 .Clauses__c=cls4.Id;
        clt4 .Name = cls3 .Name;  
        clt4 .ClauseType_Name__c=cls4.Clause_Type__c;
        clt4 .Clause_Description__c='Test desc';
        insert clt4 ; 
        
         LC_Clauses__c cls5 = New LC_Clauses__c();
         cls5.Name='108SECD';
         cls5.Clause_Type__c= 'Secutity Deposite';
         insert cls5 ;
         
         LC_ClauseType__c clt5 =new LC_ClauseType__c();    
         clt5 .Lease_Opportunity__c=lc1.Id;
         clt5 .Clauses__c=cls5 .Id;
         clt5 .Name = cls5 .Name;  
         clt5 .ClauseType_Name__c=cls5 .Clause_Type__c;
         clt5 .Clause_Description__c='Test desc';
         insert clt5 ; 
        PageReference pref = Page.LC_LeaseOppLandingPage;
    pref.getParameters().put('id', lc1.id);
    Test.setCurrentPage(pref);
        
         test.starttest();    
         ApexPages.StandardController stdcon = new ApexPages.StandardController(lc1);
         LC_CreateLeaseOppController LCcontroller = new LC_CreateLeaseOppController (stdcon);
         LCcontroller.LeaseOppid = lc1.Id;
         LCcontroller.LeaseOpportunity=lc1;
         LCcontroller.myprop=lc1.MRI_Property__c;
         LCcontroller.leaseOrSuit = 'L';
         LCcontroller.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontroller.IdtoLeasevsuitmap=MYMAp;
         LCcontroller.AdviserCallList();
         LCcontroller.LeaseOppid=lc1.id; 
         PageReference pageRef = LCcontroller.save();
         //LeaseOppid=lc1.Id;     

         
        // LCcontroller.createCluases(lc1.id);
         LCcontroller.Cancel();

          
  
        //New Lease Opportunity without lease 
        
         LC_LeaseOpprtunity__c lc12= new LC_LeaseOpprtunity__c();
         lc12.name='TestNEWOpportunity';
         lc12.Lease_Opportunity_Type__c='Lease - New';
         lc12.MRI_Property__c=mc.id;
         lc12.Current_Date__c=system.today();
         lc12.LC_OtherOpportunity_Description__c='Thisis my description';
         lc12.Base_Rent__c=12345;
         lc12.recordtypeid=recid;
         insert lc12;
         
          ApexPages.StandardController stdcon011 = new ApexPages.StandardController(lc12);
         LC_CreateLeaseOppController LCcontroller011 = new LC_CreateLeaseOppController (stdcon011  );
         LCcontroller011.LeaseOpportunity=lc12;
         LCcontroller011.leaseOrSuit = 'None';
         LCcontroller011.calllist='--None--';
         LCcontroller011.myprop=lc1.MRI_Property__c;
         LCcontroller011.IdtoLeasevsuitmap=MYMAp;
         LCcontroller011.AdviserCallList();
         LCcontroller011.save();
         // LCcontroller.createCluases(lc12.id);
         LCcontroller011.Cancel();
         LCcontroller011.hideSectionOnChange();      

    
         test.stoptest();
 
    
  }  
  
   static testmethod void testCreatelease1()
  {
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
        
         System.assertEquals( mc.name,'Test_MRI_Property');

         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
     
  
        

     //********************with record type Amendent**********************   
    
         
     RecordType NewRecType1 = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Amendment_LeaseOpportunity' limit 1]; 
         string recid1=NewRecType1.id;
     //created Amendent leae with lookup to lease
         LC_LeaseOpprtunity__c lc2= new LC_LeaseOpprtunity__c();
         lc2.name='TestAmmOpportunity';
         lc2.Lease_Opportunity_Type__c='Lease - Amendment';
         lc2.MRI_Property__c=mc.id;
         lc2.Lease__c=mylease.id;
         lc2.Current_Date__c=system.today();
         lc2.LC_OtherOpportunity_Description__c='Thisis my description';
         lc2.Base_Rent__c=12345;
         lc2.recordtypeid=recid1;
         insert lc2;
         System.assertEquals( lc2.name,'TestAmmOpportunity');

         test.starttest();    
         
           PageReference pageRef = Page.LC_LeaseOppLandingPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',lc2.id);
        
        
                  
        
         ApexPages.StandardController stdcon1 = new ApexPages.StandardController(lc2);
         LC_CreateLeaseOppController LCcontroller1 = new LC_CreateLeaseOppController (stdcon1);
         LCcontroller1.LeaseOppid=lc2.id;
         LCcontroller1.LeaseOpportunity=lc2;
         LCcontroller1.myprop=lc2.MRI_Property__c;
         LCcontroller1.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontroller1.IdtoLeasevsuitmap=MYMAp;
         LCcontroller1.AdviserCallList();
         LCcontroller1.save();
         //LCcontroller1.createCluases(lc2.id);
         LCcontroller1.Cancel();
   

    test.stoptest();
 
    
  } 
  
     static testmethod void testCreatelease3()
  { 
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
         System.assertEquals( mc.name,'Test_MRI_Property');

         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
   
     
     
    
     //********************with record type Renewal**********************   
     
     RecordType NewRecType2 = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Renewal_Lease_Opportunity' limit 1]; 
         string recid2=NewRecType2.id;
     //created Amendent leae with lookup to lease
         LC_LeaseOpprtunity__c lc3= new LC_LeaseOpprtunity__c();
         lc3.name='TestAmmOpportunity';
         lc3.Lease_Opportunity_Type__c='Lease - Renewal';
         lc3.MRI_Property__c=mc.id;
         lc3.Lease__c=mylease.id;
         lc3.Current_Date__c=system.today();
         lc3.LC_OtherOpportunity_Description__c='Thisis my description';
         lc3.Base_Rent__c=12345;
         lc3.recordtypeid=recid2;
         insert lc3;
         
         System.assertEquals( lc3.name,'TestAmmOpportunity');
         PageReference pageRef = Page.LC_LeaseOppLandingPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',lc3.id);
        

         test.starttest();    
         ApexPages.StandardController stdcon2 = new ApexPages.StandardController(lc3);
         LC_CreateLeaseOppController LCcontroller2 = new LC_CreateLeaseOppController (stdcon2 );
         LCcontroller2.LeaseOppid=lc3.id;
         LCcontroller2.LeaseOpportunity=lc3;
         LCcontroller2.myprop=lc3.MRI_Property__c;
         LCcontroller2.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontroller2.IdtoLeasevsuitmap=MYMAp;
         LCcontroller2.AdviserCallList();
         LCcontroller2.save();
        //  LCcontroller2.createCluases(lc3.id);
         LCcontroller2.Cancel();
  
    test.stoptest();
 
    
  }  
   
      static testmethod void testCreatelease4()
  { 
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
         System.assertEquals(mc.name,'Test_MRI_Property');

         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
   
 
    
    
     //********************with record type Other**********************   
     
    RecordType NewRecTypeOther = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Other']; 
         string recidother=NewRecTypeOther.id;
     
         LC_LeaseOpprtunity__c lcOther= new LC_LeaseOpprtunity__c();
         lcOther.name='TestAmmOpportunity';
         lcOther.Lease_Opportunity_Type__c='Other';
         lcOther.MRI_Property__c=mc.id;
         lcOther.Lease__c=mylease.id;
         lcOther.Current_Date__c=system.today();
         lcOther.LC_OtherOpportunity_Description__c='Thisis my description';
         lcOther.Base_Rent__c=12345;
         lcOther.recordtypeid=recidother;
         insert lcOther;
         
         System.assertEquals(lcOther.name,'TestAmmOpportunity');
         PageReference pageRef = Page.LC_LeaseOppLandingPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',lcOther.id);
        
  
         test.starttest();  
        
         ApexPages.StandardController stdconother = new ApexPages.StandardController(lcOther);
         LC_CreateLeaseOppController LCcontrollerOther = new LC_CreateLeaseOppController (stdconother );
         LCcontrollerOther.LeaseOppid=lcOther.id;
         LCcontrollerOther.LeaseOpportunity=lcOther;
         LCcontrollerOther.myprop=lcOther.MRI_Property__c;
         LCcontrollerOther.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontrollerOther.IdtoLeasevsuitmap=MYMAp;
         LCcontrollerOther.AdviserCallList();
         LCcontrollerOther.save();
        // LCcontrollerOther.createCluases(lcOther.id);
         LCcontrollerOther.Cancel();
   
  
        LC_CreateLeaseOppController LCcontrollertest = new LC_CreateLeaseOppController();
    
 
    
    test.stoptest();
 
    
  }  
  
  
     //********************with record type Master Lease**********************  
     
         static testmethod void testCreatelease5()
  { 
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
         System.assertEquals(mc.name,'Test_MRI_Property');

         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
   
 
    
    RecordType NewRecTypeOther = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Master_Lease']; 
         string recidother=NewRecTypeOther.id;
     
         LC_LeaseOpprtunity__c lcOther= new LC_LeaseOpprtunity__c();
         lcOther.name='TestMasterNewOpportunity';
         lcOther.Lease_Opportunity_Type__c='Master Lease - New';
         lcOther.MRI_Property__c=mc.id;
         lcOther.Lease__c=mylease.id;
         lcOther.Current_Date__c=system.today();
         lcOther.LC_OtherOpportunity_Description__c='Thisis my description';
         lcOther.Base_Rent__c=12345;
         lcOther.recordtypeid=recidother;
         insert lcOther;
          
         System.assertEquals(lcOther.name,'TestMasterNewOpportunity');
          PageReference pageRef = Page.LC_LeaseOppLandingPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',lcOther.id);
    
     test.starttest();  
        
         ApexPages.StandardController stdconother = new ApexPages.StandardController(lcOther);
         LC_CreateLeaseOppController LCcontrollerOther = new LC_CreateLeaseOppController (stdconother );
         LCcontrollerOther.LeaseOppid=lcOther.id;
         LCcontrollerOther.LeaseOpportunity=lcOther;
         LCcontrollerOther.myprop=lcOther.MRI_Property__c;
         LCcontrollerOther.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontrollerOther.IdtoLeasevsuitmap=MYMAp;
         LCcontrollerOther.AdviserCallList();
         LCcontrollerOther.save();
        // LCcontrollerOther.createCluases(lcOther.id);
         LCcontrollerOther.Cancel();
   
  
        LC_CreateLeaseOppController LCcontrollertest = new LC_CreateLeaseOppController();
    
 
    
    test.stoptest();
 
    
  } 
  
     //********************with record type Master Lease Other **********************  
     
         static testmethod void testCreatelease6()
  { 
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
        System.assertEquals(mc.name,'Test_MRI_Property');

         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
   
 
    
    RecordType NewRecTypeOther = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Master_Lease_Other']; 
         string recidother=NewRecTypeOther.id;
     
         LC_LeaseOpprtunity__c lcOther= new LC_LeaseOpprtunity__c();
         lcOther.name='TestMasterNewOpportunity';
         lcOther.Lease_Opportunity_Type__c='Master Lease - Assignment';
         lcOther.MRI_Property__c=mc.id;
         lcOther.Lease__c=mylease.id;
         lcOther.Current_Date__c=system.today();
         lcOther.LC_OtherOpportunity_Description__c='Thisis my description';
         lcOther.Base_Rent__c=12345;
         lcOther.recordtypeid=recidother;
         insert lcOther;    
         
         System.assertEquals(lcOther.name,'TestMasterNewOpportunity');
  PageReference pageRef = Page.LC_LeaseOppLandingPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',lcOther.id);
 
     test.starttest();  
        
         ApexPages.StandardController stdconother = new ApexPages.StandardController(lcOther);
         LC_CreateLeaseOppController LCcontrollerOther = new LC_CreateLeaseOppController (stdconother );
         LCcontrollerOther.LeaseOppid=lcOther.id;
         LCcontrollerOther.LeaseOpportunity=lcOther;
         LCcontrollerOther.myprop=lcOther.MRI_Property__c;
         LCcontrollerOther.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontrollerOther.IdtoLeasevsuitmap=MYMAp;
         LCcontrollerOther.AdviserCallList();
         LCcontrollerOther.save();
        // LCcontrollerOther.createCluases(lcOther.id);
         LCcontrollerOther.Cancel();
   
  
        LC_CreateLeaseOppController LCcontrollertest = new LC_CreateLeaseOppController();
    
 
    
    test.stoptest();
 
    
  }       
    
     //********************with record type Master Lease Other **********************  
     
         static testmethod void testCreatelease8()
  { 
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
              System.assertNotEquals(mc.name,myLease.name);
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
   
 
    
    RecordType NewRecTypeOther = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Master_Lease_Other']; 
         string recidother=NewRecTypeOther.id;
     
         LC_LeaseOpprtunity__c lcOther= new LC_LeaseOpprtunity__c();
         lcOther.name='TestMasterNewOpportunity';
         lcOther.Lease_Opportunity_Type__c='Master Lease - Rent Restructure & Extension ';
         lcOther.MRI_Property__c=mc.id;
         lcOther.Lease__c=mylease.id;
         lcOther.Current_Date__c=system.today();
         lcOther.LC_OtherOpportunity_Description__c='Thisis my description';
         lcOther.Base_Rent__c=12345;
         lcOther.recordtypeid=recidother;
         insert lcOther;
         
     PageReference pageRef = Page.LC_LeaseOppLandingPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',lcOther.id);
               
     test.starttest();  
        
         ApexPages.StandardController stdconother = new ApexPages.StandardController(lcOther);
         LC_CreateLeaseOppController LCcontrollerOther = new LC_CreateLeaseOppController (stdconother );
         LCcontrollerOther.LeaseOppid=lcOther.id;
         LCcontrollerOther.LeaseOpportunity=lcOther;
         LCcontrollerOther.myprop=lcOther.MRI_Property__c;
         LCcontrollerOther.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontrollerOther.IdtoLeasevsuitmap=MYMAp;
         LCcontrollerOther.AdviserCallList();
         LCcontrollerOther.save();
        // LCcontrollerOther.createCluases(lcOther.id);
         LCcontrollerOther.Cancel();
   
  
        LC_CreateLeaseOppController LCcontrollertest = new LC_CreateLeaseOppController();
    
 
    
    test.stoptest();
 
    
  }   
  
    //********************with record type Master Lease Amendment **********************  
     
         static testmethod void testCreatelease7()
  { 
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         System.assertNotEquals(mc.name,myLease.name);
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
   
  
    
    RecordType NewRecTypeOther = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Master_Lease_Amendment']; 
         string recidother=NewRecTypeOther.id;
     
         LC_LeaseOpprtunity__c lcOther= new LC_LeaseOpprtunity__c();
         lcOther.name='TestMasterNewOpportunity';
         lcOther.Lease_Opportunity_Type__c='Master Lease - Amendment';
         lcOther.MRI_Property__c=mc.id;
         lcOther.Lease__c=mylease.id;
         lcOther.Current_Date__c=system.today();
         lcOther.LC_OtherOpportunity_Description__c='Thisis my description';
         lcOther.Base_Rent__c=12345;
         lcOther.recordtypeid=recidother;
         insert lcOther;
    PageReference pageRef = Page.LC_LeaseOppLandingPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',lcOther.id);
               
     test.starttest();  
        
         ApexPages.StandardController stdconother = new ApexPages.StandardController(lcOther);
         LC_CreateLeaseOppController LCcontrollerOther = new LC_CreateLeaseOppController (stdconother );
         LCcontrollerOther.LeaseOppid=lcOther.id;
         LCcontrollerOther.LeaseOpportunity=lcOther;
         LCcontrollerOther.myprop=lcOther.MRI_Property__c;
         LCcontrollerOther.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontrollerOther.IdtoLeasevsuitmap=MYMAp;
         LCcontrollerOther.AdviserCallList();
         LCcontrollerOther.save();
        // LCcontrollerOther.createCluases(lcOther.id);
         LCcontrollerOther.Cancel();
   
  
        LC_CreateLeaseOppController LCcontrollertest = new LC_CreateLeaseOppController();
    
 
    
    test.stoptest();
 
    
  }       
   
     //********************lease opportunity Expansion creation **********************  
     
         static testmethod void testCreatelease9()
  { 
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         System.assertNotEquals(mc.name,myLease.name);
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
   
 
    
    RecordType NewRecTypeexp = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Expansion_Lease_Opportunity']; 
         string recidother=NewRecTypeexp.id;
     
         LC_LeaseOpprtunity__c lcOther= new LC_LeaseOpprtunity__c();
         lcOther.name='TestMasterNewOpportunity';
         lcOther.Lease_Opportunity_Type__c='Lease Expansion - Renewal';
         lcOther.MRI_Property__c=mc.id;
         lcOther.Lease__c=mylease.id;
         lcOther.Current_Date__c=system.today();
         lcOther.LC_OtherOpportunity_Description__c='Thisis my description';
         lcOther.Base_Rent__c=12345;
         lcOther.recordtypeid=recidother;
         insert lcOther;
    
          PageReference pageRef = Page.LC_LeaseOppLandingPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',lcOther.id);      
     test.starttest();  
        
         ApexPages.StandardController stdconother = new ApexPages.StandardController(lcOther);
         LC_CreateLeaseOppController LCcontrollerOther = new LC_CreateLeaseOppController (stdconother );
         LCcontrollerOther.LeaseOppid=lcOther.id;
         LCcontrollerOther.LeaseOpportunity=lcOther;
         LCcontrollerOther.myprop=lcOther.MRI_Property__c;
         LCcontrollerOther.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontrollerOther.IdtoLeasevsuitmap=MYMAp;
         LCcontrollerOther.AdviserCallList();
         LCcontrollerOther.save();
        // LCcontrollerOther.createCluases(lcOther.id);
         LCcontrollerOther.Cancel();
   
  
        LC_CreateLeaseOppController LCcontrollertest = new LC_CreateLeaseOppController();
    
 
    
    test.stoptest();
 
    
  } 
   //******************** Outparcel/Pad Sale creation **********************  
     
         static testmethod void testCreatelease10()
  { 
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         System.assertNotEquals(mc.name,myLease.name);
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
   
 
    
    RecordType NewRecTypeexp = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Lease_Outparcel_Pad_Sale']; 
         string recidother=NewRecTypeexp.id;
     
         LC_LeaseOpprtunity__c lcOther= new LC_LeaseOpprtunity__c();
         lcOther.name='TestMasterNewOpportunity';
         lcOther.Lease_Opportunity_Type__c='Outparcel/Pad Sale or New';
         lcOther.MRI_Property__c=mc.id;
         lcOther.Lease__c=mylease.id;
         lcOther.Current_Date__c=system.today();
         lcOther.LC_OtherOpportunity_Description__c='Thisis my description';
         lcOther.Base_Rent__c=12345;
         lcOther.recordtypeid=recidother;
         insert lcOther;
     PageReference pageRef = Page.LC_LeaseOppLandingPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',lcOther.id);
               
     test.starttest();  
        
         ApexPages.StandardController stdconother = new ApexPages.StandardController(lcOther);
         LC_CreateLeaseOppController LCcontrollerOther = new LC_CreateLeaseOppController (stdconother );
         LCcontrollerOther.LeaseOppid=lcOther.id;
         LCcontrollerOther.LeaseOpportunity=lcOther;
         LCcontrollerOther.myprop=lcOther.MRI_Property__c;
         LCcontrollerOther.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontrollerOther.IdtoLeasevsuitmap=MYMAp;
         LCcontrollerOther.AdviserCallList();
         LCcontrollerOther.save();
        // LCcontrollerOther.createCluases(lcOther.id);
         LCcontrollerOther.Cancel();
   
  
        LC_CreateLeaseOppController LCcontrollertest = new LC_CreateLeaseOppController();
    
 
    
    test.stoptest();
 
    
  }             
    //******************** License/Vendor Agreement creation **********************  
     
         static testmethod void testCreatelease11()
  { 
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         System.assertNotEquals(mc.name,myLease.name);
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
   
 
    
         RecordType NewRecTypeexp = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Other']; 
         string recidother=NewRecTypeexp.id;
     
         LC_LeaseOpprtunity__c lcOther= new LC_LeaseOpprtunity__c();
         lcOther.name='TestMasterNewOpportunity';
         lcOther.Lease_Opportunity_Type__c='License/Vendor Agreement';
         lcOther.MRI_Property__c=mc.id;
         lcOther.Lease__c=mylease.id;
         lcOther.Current_Date__c=system.today();
         lcOther.LC_OtherOpportunity_Description__c='Thisis my description';
         lcOther.Base_Rent__c=12345;
         lcOther.recordtypeid=recidother;
         insert lcOther;
    
           PageReference pageRef = Page.LC_LeaseOppLandingPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',lcOther.id); 
        
     test.starttest();  
        
         ApexPages.StandardController stdconother = new ApexPages.StandardController(lcOther);
         LC_CreateLeaseOppController LCcontrollerOther = new LC_CreateLeaseOppController (stdconother );
         LCcontrollerOther.LeaseOppid=lcOther.id;
         LCcontrollerOther.LeaseOpportunity=lcOther;
         LCcontrollerOther.myprop=lcOther.MRI_Property__c;
         LCcontrollerOther.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontrollerOther.IdtoLeasevsuitmap=MYMAp;
         LCcontrollerOther.AdviserCallList();
          PageReference   objPageRef = LCcontrollerOther.save();
        // LCcontrollerOther.createCluases(lcOther.id);
         LCcontrollerOther.Cancel();
   
  
        LC_CreateLeaseOppController LCcontrollertest = new LC_CreateLeaseOppController();
    
 
    
    test.stoptest();
 
    
  }             
}