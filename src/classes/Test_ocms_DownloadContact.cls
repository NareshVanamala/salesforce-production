@isTest
public class Test_ocms_DownloadContact 
{
    
    static testmethod void testocms_DownloadContact ()
    {
   Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];   
   user cn1= new user();
   cn1.FirstName='test';
   cn1.lastName='record';
   //cn1.Territory__c='HeartLand';
   cn1.Phone='8473361717';
   cn1.mobilePhone='8473361717';
   cn1.email='test@colecapital.com';
   cn1.street='test';
   cn1.city='test';
   cn1.state='test';
   cn1.country='test';
   //cn1.MailingPostalCode='test';
   cn1.title='mr';
   cn1.department='test';
   cn1.EmailEncodingKey='UTF-8';
   cn1.LastName='Testing';
   cn1.LanguageLocaleKey='en_US';
   cn1.LocaleSidKey='en_US';
   cn1.ProfileId = p.Id;
   cn1.TimeZoneSidKey='America/Los_Angeles';
   cn1.UserName='standarduser1132016@vereitorg.com';
   cn1.Alias='alias';
   
   insert cn1;
   
   
   System.assertEquals(cn1.ProfileId,p.Id);

   
   System.currentPageReference().getParameters().put('id',cn1.id);
   
   ApexPages.currentPage().getHeaders().put('Content-Disposition','mail.vcf');
   
   ocms_DownloadContact sc=new ocms_DownloadContact();
   }
}