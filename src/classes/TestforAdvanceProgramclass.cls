@isTest
private class TestforAdvanceProgramclass 
 {
      public static testMethod void TestforAdvanceProgramclass () 
      {
          Account a= new Account();
          a.name='Testaccount';
          //RecordType rtypes = [Select Name, Id From RecordType where isActive=true and sobjecttype='Account'and name='National_Accounts'];
          //a.recordtypeid=rtypes.id;
          insert a;  
          
          Program__c pt= new Program__c();
          pt.Account__c=a.id;
          pt.Assets__c='Testingthe Program';
          pt.Category__c='Rep as Advisor';
          pt.Cole_Product__c='INCNAV';
          insert pt;
          System.assertEquals(pt.Account__c,a.id);

          Program__c pt1= new Program__c();
          pt1.Account__c=a.id;
          pt1.Assets__c='Testingthe Program';
          pt1.Category__c='Rep as Advisor';
          pt1.Cole_Product__c='INCNAV';
          insert pt1;
          System.assertEquals(pt1.Account__c,a.id);
 
          Program__c pt2= new Program__c();
          pt2.Account__c=a.id;
          pt2.Assets__c='TestingTseingthe Program';
          pt2.Category__c='Rep as Advisor';
          pt2.Cole_Product__c='INCNAV';
          insert pt2;
          System.assertEquals(pt2.Account__c,a.id);


         ApexPages.currentPage().getParameters().put('id',a.id);
         ApexPages.StandardController controller = new ApexPages.StandardController(a); 
         AdvancedProgramController1 x = new AdvancedProgramController1 (controller);  
         x.newProgram();
         //x.SaveRecord();
         x.savenewProg();
         //x.removecon();
         x.cancelnewProg();          

       }
         
          
           
 }