@isTest (SeeAllData=true)
private class ocms_WebPropertyList_test {
    
    @isTest static void test_getResultTotal() {
        
        ocms_WebPropertyList propertyList = new ocms_WebPropertyList();

        String portfolio = 'Cole Credit Property Trust III';
        String state = 'TX';
        String city = 'Houston';
        String tenantIndustry = '';
        String tenant = '';
        String[] filterArray = new List<String>();
        filterArray.add('Office');

        propertyList.getResultTotal(portfolio,state,city, tenantIndustry, tenant, filterArray);

        state = '';
        city = '';
        tenantIndustry = 'Office';
        
      System.assert(propertyList != Null);


        propertyList.getResultTotal(portfolio,state,city,tenantIndustry,tenant,filterArray);

        tenantIndustry = '';
        tenant = '**VACANT**';

        propertyList.getResultTotal(portfolio,state,city,tenantIndustry,tenant,filterArray);


    }
    
    @isTest static void test_getPropertyList() {

        ocms_WebPropertyList propertyList = new ocms_WebPropertyList();

        String portfolio = 'Cole Credit Property Trust III';
        String state = 'TX';
        String city = 'Houston';
        String tenantIndustry = '';
        String tenant = '';
        String[] filterArray = new List<String>();
        filterArray.add('Office');

        propertyList.getPropertyList(portfolio,state,city,tenantIndustry,tenant,filterArray,'Date_Acquired__c', '25', '0');

        state = '';
        city = '';
        tenantIndustry = 'Office';
        filterArray.add('Industrial');

              System.assert(filterArray!= Null);

        propertyList.getPropertyList(portfolio,state,city,tenantIndustry,tenant,filterArray,'Name', '25', '0');

        tenantIndustry = '';
        tenant = '**VACANT**';

        propertyList.getPropertyList(portfolio,state,city,tenantIndustry,tenant,filterArray,'location', '25', '0');

        try{
        propertyList.getHTML();
        }
        catch(Exception e){}
    }

    @isTest static void test_getMethods() {

        ocms_WebPropertyList propertyList = new ocms_WebPropertyList();

        propertyList.getPortfolioList();

        propertyList.getStateList('Past Programs');

        propertyList.getCityList('TX','Past Programs');

        propertyList.getIndustryList('Past Programs');

        propertyList.getTenantList('Past Programs');
        
              System.assert(propertyList != Null);

        
        try{
        propertyList.getHTML();
        }
        catch(Exception e){}
    }

    @isTest static void test_getHTML() {
        ocms_WebPropertyList propertyList = new ocms_WebPropertyList();
              System.assert(propertyList != Null);


        //Internal,a2BP0000000AtoZMAS,NewWindow,Web-Property-Detail,,,

        //propertyList.webPropertyTargetPage = 'Internal,a2BP0000000AtoZMAS,NewWindow,Web-Property-Detail,,,';

        //propertyList.getHTML();

    }
    
}