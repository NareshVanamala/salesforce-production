@isTest
private class RC_UpdateLoanRelationshipwithPropId_Test {
    static testMethod void updatepropId() {
         Deal__c dealObj = new Deal__c();
       dealObj.name='test deal';
       dealObj.Cole_Initials__c='test cole';
       dealObj.Property_Type__c='Land';
       dealObj.Tenancy__c='Single-Tenant';
       dealObj.Primary_Use__c='Retail';
       dealObj.Deal_Status__c = 'Reviewing';
      insert dealObj;
      

      
      Loan__c loanObj = new Loan__c();
      loanObj.Name ='test loan';
      loanObj.Status__c ='Prospective Loan';
      insert loanObj;
      
      System.assertNotEquals(dealObj.Deal_Status__c,loanObj.Status__c );

      
      Loan_Relationship__c loanRelObj = new Loan_Relationship__c();
      loanRelObj.name = 'test loan relationship';
      loanRelObj.Loan__c = loanObj.id;
      loanRelObj.Deal__c = dealObj.id;
      loanRelObj.Loan_Amount__c = 200000;
      insert loanRelObj;
      
      RC_Property__c rcpObj = new RC_Property__c();
      rcpObj.name = 'test Property';
      rcpObj.Related_Deal__c = dealObj.id;
      insert rcpObj;
      loanRelObj.Property__c = rcpObj.id;
      update loanRelObj;
      
    }
}