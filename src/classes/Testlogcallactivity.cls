/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Testlogcallactivity {

    static testMethod void myUnitTest() {

        // Create a Contact         
        Contact c = new Contact();             
        c.FirstName = 'Test';             
        c.LastName = 'Contact';             
        //c.AccountId = a.Id;             
        c.Email = 'contactemail@example.com';         
        insert c;         

        /* Create Tasks for Test Cases */         
        list<Task> l_Tasks = new list<Task>();           
        // Task associated to Lead, not Contact         
        Task t1 = new Task();             
        t1.Subject = 'Email: something';             
        t1.Status = 'Completed';             
        t1.WhoId = c.id;           
        t1.ActivityDate = Date.today();         
        l_Tasks.add(t1); 
        insert l_Tasks;
        System.assertEquals(t1.WhoId,c.id);

        
    }
}