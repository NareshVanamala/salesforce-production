public with sharing class IdDisplay
{    
    public string Id{get;set;}    
     
    public IdDisplay(ApexPages.StandardController controller)    
    {        
        Id=controller.getRecord().Id;        
    }                     
}