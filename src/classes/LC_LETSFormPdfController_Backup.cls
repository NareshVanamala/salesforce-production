public with sharing class LC_LETSFormPdfController_Backup
 {
 
    public string LseOprtyid{get;set;} 
    public LC_LeaseOpprtunity__c LseOpty{get;set;} 
    public List<LC_ClauseType__c>cTypeList{set;get;}
    public List<LC_ClauseType__c>cTypeList1{set;get;}
    public list<LC_RentSchedule__c>LCrentlist{set;get;}
    public list<LC_RentSchedule__c>LCrentlist1{set;get;}
    public List<Attachment> attList{get;set;}
   
    public List<Note> noteList{get;set;}
    public boolean showRentschedule{set;get;}
    public boolean showAddlClauses{set;get;}
    public boolean showEstimatedRent{set;get;}
    public boolean showRenewalEffectiveDate{set;get;}
    public list<LC_OptionSchedule__c>LCoptionlist{set;get;}
    public boolean showOptionSchedule{set;get;}
    public boolean showRevisedSection{set;get;}
    //public boolean showRentReliefInfo{set;get;}
    transient public String oneTwoFivePEU {get; set;}
   // transient public String threeZeroTwoCOM {get; set;}
    transient public String threeZeroFiveFix {get; set;}
    //transient public String threeZeroSixNNN {get; set;}
    transient public String oneZeroEightSECD {get; set;}
    transient public String threeZeroSvnPPR {get; set;}
    //transient public String oneFourZeroLLM {get; set;}
    transient public String oneFiveZeroLLW {get; set;}
    transient public String oneOneTwoEXC{get; set;}
   // transient public String onethreeSvnCON {get; set;}
    transient public String oneFourEitKCKOUT {get; set;}
    transient public String oneOneThreeCONT {get; set;}
    transient public String oneOneFourRAD {get; set;}
    transient public String oneTwoSvnDRK {get; set;}
    transient public String oneTwoThreeROFO {get; set;}
    transient public String oneFourSevenTIA {get; set;}
    transient public String oneFourNineCOTEN{get; set;}
    //*******Created for signature block***********
    transient  public boolean Showsignature{set;get;}
    transient  public boolean showRenewalInfo{set;get;}
    //*******Created for signature block finished***********
    // create a map for clase type records ..name and description.
    transient public map<String,String>leaseNametoDesriptionMap{get;set;}
    transient public set<id>Subcaseid;
    transient public list<ProcessInstanceStep >plist;
    transient public list<ProcessInstance>plist1;
    transient public set<id>ProcessCaseid;
    transient public map<id, list<ProcessInstanceHistory>> processinstancestepmap;
    transient public map<id,string>StepToComment;
    transient public map<id,String>subjectCaseMap; 
    transient public map<id,Datetime>approvaltimeMap{set;get;}
    
    public Datetime firstapprovaltime{set;get;}
    public Datetime Secondapprovaltime{set;get;}
    public Datetime Thirdapprovaltime{set;get;}
    public Datetime Fourthapprovaltime{set;get;}
    public Datetime fiveapprovaltime{set;get;}
    public Datetime sixapprovaltime{set;get;}
    
    public list<LC_Clauses__c>standardClauseList{set;get;}
    public set<string>clauseNameSet{get;set;}
    Public Centralized_Helper_Object__c helperObject{get;set;}
    public set<string>clauseNameSet1{get;set;}
    transient public map<String,String>clauseNametoDesriptionMap{get;set;}
  
    public LC_LETSFormPdfController_Backup(ApexPages.StandardController controller)
    {
        helperObject=new Centralized_Helper_Object__c ();
       approvaltimeMap=new map<id,Datetime>();
       Subcaseid=new set<id>();
       plist=new list<ProcessInstanceStep >();
       plist1=new list<ProcessInstance>();
       ProcessCaseid=new set<id>();  
       subjectCaseMap=new map<id,String>();
       processinstancestepmap=new map<id, list<ProcessInstanceHistory>>();
       StepToComment=new map<id,string>();
       firstapprovaltime=null;
       Secondapprovaltime=null;
       Thirdapprovaltime=null;
       Fourthapprovaltime=null;
       fiveapprovaltime=null; 
       sixapprovaltime=null; 
       
       
      LseOpty = new LC_LeaseOpprtunity__c ();
      leaseNametoDesriptionMap=new map<String,String>();
          LseOprtyid = ApexPages.currentPage().getParameters().get('id');
          LseOpty=[select id,Name,National_Tenant__c,Actual_NER__c,Additional_Comments__c,Add_l_Options__c,Tenant_Address__c,Alternate_Property_Name__c,Average_base_rent_over_renewal_period__c,Anticipated_Delivery_Date__c,Average_Option_Rents_over_Renewal_Period__c,Suite_Id__c,Leasing_Commissions__c,Percent_Rent__c,Break_Point__c,Reimbursable_Type__c,Percent_Rent1__c,Was_space_purchased_vacant_New__c,NNN_Base_Year__c,SystemModStamp,Renewal_at_option__c,
                   Budgeted_NER__c,Building_Id__c,Common_Name__c,Contiguous_Tenant_For_Possible_Expansion__c,Current_Date__c,Current_Expiration_Date__c,Annual_Base_Rent_PSF__c,Annual_Base_Rent_at_expiration__c,Summary_of_Terms_Rich__c,LC_Already_Submitted__c,Suite_Address__c,Tenant_Improvement_Cost__c,Tenant_Improvement_PSF__c,Date_Executed__c,Estimated_Rent_Start_Date__c,Estimated_rent_start_date_New__c,Next_Contract_Option_Rents_per_Lease__c,
                   Entity_Id__c,Financials_if_requested__c, OwnerEnitty_Fund__c,Guarantor__c,Lease_Opportunity_Type__c,Market_Rents__c,Milestone__c,MRI_Property__c,MRIProperty_Address__c, Tenant_Email_Contact_Number__c,By__c,Prepared_by__c,Deal_Maker__c,Avg_option_Rents_over_initial_period__c,Landlord_Broker__c,Tenant_Broker__c,Leasing_Commissions_PSF__c ,Renewal_Effective_date__c ,Date_last_tenant_vacated_New__c,Record_ID__c,Base_Rent_PSF__c,
                   Renewalterm__c,Sales_Reports_if_requested__c,Suite_Sqft__c,Status__c,Tenant_Contact_Name__c,Tenant_DBA__c,LC_OtherOpportunity_Description__c,Percent_Rent_Breakpoint__c,Lease_Category__c,Tenant_Email__c,Gross_Sales_Reporting__c,Tenant_Notice_Address__c,Lease__r.Suite_Address__c,Tenant_Contact_Number__c,Property_Type__c ,Recommendations_Narrative_Rich__c,Base_Rent__c,Is_there_a_lender__c,Is_this_a_TIC_property__c,
                   Tenant_Legal_Name__c,Tenant_payment_performance_history__c,Initialterm__c,Waivers__c,Remaining_Options_Intact__c,RemainingOptions_Description__c,First_Approver__c,First_Approver__r.title,Second_Approver__c,Second_Approver__r.title,CreatedDate,Lastmodifieddate,MRI_Property__r.City__c,MRI_Property__r.State__c,MRI_Property__r.Zip_code__c,Average_Base_Rent_Over_Initial_Term__c ,TIC_and_Lender_Approval__c ,SF_excluding__c,
                   Third_Approver__c,Third_Approver__r.title,Fourth_Approver__c,Fourth_Approver__r.title,Fifth_Approver__c,Fifth_Approver__r.title,Sixth_Approver__c,Sixth_Approver__r.title,Date_First_level_Approval__c,Date_Second_level_Approval__c,Date_Third_Level_Approval__c,Date_Fourth_level_Approval__c,Date_Fifth_Level_Approval__c,Date_Sixth_Level_Approval__c from LC_LeaseOpprtunity__c where id=:LseOprtyid];  
                   
          system.debug('----------------'+LseOpty.Remaining_Options_Intact__c);
          system.debug('----------------'+LseOpty.RemainingOptions_Description__c);
       
            if(LseOpty.LC_Already_Submitted__c>1)
         showRevisedSection=true;
       else
        showRevisedSection=false;
       helperObject=[select id,LeaseCentral_Approval_Instructions__c,name from Centralized_Helper_Object__c where name=:'Lease Central-I'];
        //*******Created for signature block***********
        if((LseOpty.Milestone__c=='Approval Received')||(LseOpty.Milestone__c=='Lease Executed'))
           Showsignature=true;
        else
         Showsignature=false;
        //*******Created for signature block finished***********
      
        
         if(LseOpty.Lease_Opportunity_Type__c=='Lease - Renewal' )
         showRenewalEffectiveDate=true;
         else
         showRenewalEffectiveDate=false;
 
      
         if((LseOpty.Lease_Opportunity_Type__c=='Master Lease - New')||(LseOpty.Lease_Opportunity_Type__c=='Lease - New'))
         {
        showRenewalInfo =false;
        
        }
        else
        {
        showRenewalInfo =True;
        
        }
        if(LseOpty.Lease_Opportunity_Type__c=='Lease - New')
        {
       //showRenewalInfo =false;
        showEstimatedRent =True;
        }
        else
        {
        //showRenewalInfo =True;
        showEstimatedRent =False;
        }
 
              
      LCrentlist=[select id,Name,Increase_in_rent__c,Increase_in_rent_n__c ,Annual_PSF__c,Average_Base_Rent_over_Initial_Term__c,Base_Rent_Additional_Info__c,Base_Rent_PSF__c,Income_Category_1__c,Monthly_PSF__c,Lease__c,Lease_Opportunity__c,From_In_Months__c,To_In_Months__c,Months_Years__c,Formula_Number__c,Break_Point__c  from LC_RentSchedule__c where Lease_Opportunity__c=:LseOprtyid Order By Income_Category_1__c,From_In_Months__c ASC ];
           
          if(LCrentlist.size()>0)
           showRentschedule=true;
         else
           showRentschedule=false;
           
          LCoptionlist=[select id,Name,Additional_Notes__c,Amount__c,Currency__c,Amount_Description__c,Formula_Number__c,Frequency__c,Lease_Opportunity__c,Notice_Days__c,To_In_Months__c,From_In_Months__c,Months_Years__c,Option_Type__c,sqft__c,Base_Rent_PSF__c,Break_Point__c,Increase_in_rent__c,Increase_in_rent_n__c,Monthly_PSF__c from LC_OptionSchedule__c where Lease_Opportunity__c=:LseOprtyid Order By Option_Type__c,From_In_Months__c ASC];
          if(LCoptionlist.size()>0)
           showOptionSchedule=true;
         else
           showOptionSchedule=false;
//*************************adding the stanadrd clause types**************************


      standardClauseList=new list<LC_Clauses__c>(); 
      clauseNameSet=new set<string>();
      clauseNameSet1=new set<string>();
      clauseNametoDesriptionMap=new map<String, String>();
      standardClauseList=[select id, name,Clause_Body__c,Clause_Type__c,Order_On_LETS_form__c,Show_On_LETs_Form__c from LC_Clauses__c where Show_On_LETs_Form__c=True order by Order_On_LETS_form__c];
           system.debug('get the complete list'+standardClauseList.size());
         
             for(LC_Clauses__c  ct:standardClauseList)
               clauseNameSet.add(ct.Clause_Type__c);
             system.debug('This is my set'+clauseNameSet);
             cTypeList=[select id,Name,ClauseType_Name__c,Clauses__c,Clause_Description__c,Lease_Opportunity__c from  LC_ClauseType__c  where Lease_Opportunity__c=:LseOprtyid and ClauseType_Name__c in :clauseNameSet ];
          
          for(LC_ClauseType__c  ct:cTypeList)
          {
             
               leaseNametoDesriptionMap.put(ct.ClauseType_Name__c, ct.Clause_Description__c);
          }
          system.debug('show me the map'+leaseNametoDesriptionMap);
          
         for(LC_Clauses__c ct:standardClauseList)
         {
          if(leaseNametoDesriptionMap.get(ct.Clause_Type__c)!=null)
          {
           string mydescription=leaseNametoDesriptionMap.get(ct.Clause_Type__c);
           system.debug('get me the description'+mydescription);
           clauseNametoDesriptionMap.put(ct.Clause_Type__c,mydescription);
          
          }
           else if((leaseNametoDesriptionMap.get(ct.Clause_Type__c)==null)&&(leaseNametoDesriptionMap.containskey(ct.Clause_Type__c)==false))   
           {
             clauseNametoDesriptionMap.put(ct.Clause_Type__c,'Clause not Selected');
           }     
         else if((leaseNametoDesriptionMap.get(ct.Clause_Type__c)==null)&&(leaseNametoDesriptionMap.containskey(ct.Clause_Type__c)==true))   
           {
             clauseNametoDesriptionMap.put(ct.Clause_Type__c,'');
           }  
        
        
         } 
            system.debug('show me the map'+clauseNametoDesriptionMap);     
             system.debug('----------------'+cTypeList);




//**********************************finshed adding the stanadrd clauses**************************

// comment the below code so that we so not have to hard code the values


          /* cTypeList=[select id,Name,ClauseType_Name__c,Clauses__c,Clause_Description__c,Lease_Opportunity__c from  LC_ClauseType__c  where Lease_Opportunity__c=:LseOprtyid  ];
           system.debug('----------------'+cTypeList);
            if(cTypeList.size()>0)
           {
             for(LC_ClauseType__c lc: cTypeList)
             {
             if((lc.Name!=Null && lc.Clause_Description__c!=Null)||(lc.Name!=Null))
              leaseNametoDesriptionMap.put(lc.name,lc.Clause_Description__c);
                               
             }
           }
             system.debug('&&&&&&&&&&&&&&&&&&&&&&&&&'+leaseNametoDesriptionMap);

             if(leaseNametoDesriptionMap!=Null)  
              {
            
               if( leaseNametoDesriptionMap.containsKey('125PEU'))
               {
                oneTwoFivePEU = leaseNametoDesriptionMap.get('125PEU');
                
               } 
               else
               {
                  oneTwoFivePEU ='Clause Not Selected';
               }
               system.debug('2222222222222222222222'+oneTwoFivePEU);
               
              
               if(leaseNametoDesriptionMap.containsKey('305FIX'))
               {        
               threeZeroFiveFix = leaseNametoDesriptionMap.get('305FIX');
               }
               else
               {
                  threeZeroFiveFix ='Clause Not Selected';
               }
              
               if(leaseNametoDesriptionMap.containsKey('108SECD'))
               {
               oneZeroEightSECD = leaseNametoDesriptionMap.get('108SECD');
               }
               else
               {
                  oneZeroEightSECD ='Clause Not Selected';
               }
               
               if(leaseNametoDesriptionMap.containsKey('307PPR'))
               {
               threeZeroSvnPPR = leaseNametoDesriptionMap.get('307PPR');
               }
               else
               {
                  threeZeroSvnPPR ='Clause Not Selected';
               }
                if(leaseNametoDesriptionMap.containsKey('147TIA'))
               { 
               oneFourSevenTIA= leaseNametoDesriptionMap.get('147TIA');
               }
                 else
               {
                  oneFourSevenTIA ='Clause Not Selected';
               }
              
               if(leaseNametoDesriptionMap.containsKey('150LLW'))
               {
               oneFiveZeroLLW = leaseNametoDesriptionMap.get('150LLW');
               }
                else
               {
                  oneFiveZeroLLW ='Clause Not Selected';
               }
               if(leaseNametoDesriptionMap.containsKey('112EXC'))
               {
               oneOneTwoEXC = leaseNametoDesriptionMap.get('112EXC');
               }
                else
               {
                  oneOneTwoEXC ='Clause Not Selected';
               }
               
                if(leaseNametoDesriptionMap.containsKey('148KCKOUT'))
               {
               oneFourEitKCKOUT = leaseNametoDesriptionMap.get('148KCKOUT');
               }
                else
               {
                  oneFourEitKCKOUT ='Clause Not Selected';
               }
               
               if(leaseNametoDesriptionMap.containsKey('149COTEN'))
               {
               oneFourNineCOTEN= leaseNametoDesriptionMap.get('149COTEN');
               }
                 else
               {
                  oneFourNineCOTEN='Clause Not Selected';
               }
                if(leaseNametoDesriptionMap.containsKey('113CONT'))
               {
               oneOneThreeCONT = leaseNametoDesriptionMap.get('113CONT');
               }
                 else
               {
                  oneOneThreeCONT ='Clause Not Selected';
               }
               
               
               if(leaseNametoDesriptionMap.containsKey('114RAD'))
               {
               oneOneFourRAD = leaseNametoDesriptionMap.get('114RAD');
               }
                 else
               {
                  oneOneFourRAD ='Clause Not Selected';
               }
               if(leaseNametoDesriptionMap.containsKey('127DRK'))
               {
               oneTwoSvnDRK = leaseNametoDesriptionMap.get('127DRK');
               }
                 else
               {
                  oneTwoSvnDRK ='Clause Not Selected';
               }
               if(leaseNametoDesriptionMap.containsKey('123ROFO'))
               {
               oneTwoThreeROFO = leaseNametoDesriptionMap.get('123ROFO');
               }
                 else
               {
                  oneTwoThreeROFO ='Clause Not Selected';
               }*/
               
           //finished commenting*************************************   
              
              
              
               //******NEW AND ADDTIONAL CLAUSES*******
             // }
             
           //}  
           
          cTypeList1=[select id,Name,ClauseType_Name__c,Clause_Description__c,Clauses__c,Lease_Opportunity__c from  LC_ClauseType__c where Clauses__r.Show_On_LETs_Form__c=false and Lease_Opportunity__c=:LseOprtyid];
          system.debug('&&&&&&&&&&&&&&&&&&&&&&&&&'+cTypeList1);
           if(cTypeList1.size()>0)
           showAddlClauses=true;
         else
           showAddlClauses=false;
           
          attList = [SELECT Id, Name, Body, ContentType FROM Attachment  WHERE Parentid =:LseOprtyid];
          noteList = [SELECT Id,Title FROM Note  WHERE Parentid =:LseOprtyid];
          system.debug('--------------attachments-------------- '+attList );
     //********************approval timing ********************     
       plist1=[SELECT ProcessDefinitionID,CreatedById,CreatedDate,Id,IsDeleted,LastModifiedById,LastModifiedDate,Status,SystemModstamp,TargetObjectId,TargetObject.Name,(SELECT SystemModstamp, StepStatus, ProcessInstanceId, OriginalActorId,OriginalActor.name, Id, CreatedDate, CreatedById, Comments, ActorId,Actor.name FROM StepsAndWorkitems ORDER BY CreatedDate DESC ) FROM ProcessInstance where TargetObjectId =:LseOprtyid ORDER BY CreatedDate ASC];
        for(processinstance plist2:plist1)
        {
           for(ProcessInstanceHistory ri:plist2.StepsAndWorkitems)
           {  
               if((ri.stepstatus=='Started'))
                 StepToComment.put(ri.ProcessInstanceId,ri.Comments);
             if(processinstancestepmap.containsKey(ri.ProcessInstanceId))
                 processinstancestepmap.get(ri.ProcessInstanceId).add(ri);
               else
               processinstancestepmap.put(ri.ProcessInstanceId, new List<ProcessInstanceHistory>{ri});
             
            }
         }
      system.debug('Check the map...'+processinstancestepmap); 
      
      list<ProcessInstanceHistory >conlist=new list<ProcessInstanceHistory >();
       for(ProcessInstance acc:plist1)
       {
           list<ProcessInstanceHistory >conlist1=new list<ProcessInstanceHistory >();
           conlist1=processinstancestepmap.get(acc.id);
           //conlist=processinstancestepmap.get(acc.id);
           conlist.addAll(conlist1);
       }
      system.debug('check the list'+conlist);  
          for(ProcessInstanceHistory  ct:conlist)
           {
             if((ct.StepStatus=='Approved')||(ct.StepStatus=='Rejected')||(ct.StepStatus=='Removed'))
             
          
             approvaltimeMap.put(ct.OriginalActorId,ct.CreatedDate);
             
       
            }
       system.debug('This is the final Map'+approvaltimeMap);   
       system.debug('This is the '+LseOpty.First_Approver__c);
       
        
       if(approvaltimeMap.get(LseOpty.First_Approver__c)!=null)
       {
       firstapprovaltime=approvaltimeMap.get(LseOpty.First_Approver__c);
       //firstapprovaltime=firstapprovaltime-0.291666666;
       }
       
      if(approvaltimeMap.get(LseOpty.Second_Approver__c)!=null)
       {
       Secondapprovaltime=approvaltimeMap.get(LseOpty.Second_Approver__c);
       }
       
       if(approvaltimeMap.get(LseOpty.Third_Approver__c)!=null)
       {
       
       Thirdapprovaltime=approvaltimeMap.get(LseOpty.Third_Approver__c);       
       }
       
       if(approvaltimeMap.get(LseOpty.Fourth_Approver__c)!=null)
       {
       Fourthapprovaltime=approvaltimeMap.get(LseOpty.Fourth_Approver__c);
       }
       
       if(approvaltimeMap.get(LseOpty.Fifth_Approver__c)!=null)
       {
       fiveapprovaltime=approvaltimeMap.get(LseOpty.Fifth_Approver__c);
       }
       
       if(approvaltimeMap.get(LseOpty.Sixth_Approver__c)!=null)
       {
       sixapprovaltime=approvaltimeMap.get(LseOpty.Sixth_Approver__c);
       }
       
     
                                         
}


}