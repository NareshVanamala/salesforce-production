@isTest
public with sharing class ocms_PortalMessage_test{
    
    public ocms_PortalMessage_test() {
        
    }
   static cms__Content__c txc {get; set;}

     static private cms.GenerateContent setUpController() {
        Map <String, String> contextProperties = new Map<String, String>{'page_mode' => 'prev'};    
        cms__Sites__c mySite = cms.TestExtensionFixtures.InitiateTest('Public', contextProperties);
        
        cms__Content_Type__c ct = new cms__Content_Type__c(cms__Name__c = 'ARSelfRegistration', cms__Site_Name__c = 'Public', cms__Label__c = 'ARSelfRegistration');
        insert ct;

        txc = new cms__Content__c(
                cms__Content_Type__c         = ct.Id,
                cms__Name__c                 = 'ARSelfRegistrationCon',
                cms__Description__c          = 'Testing',
                cms__Preview__c              = true,
                cms__Published__c            = false,
                cms__Published_Start_Date__c = System.now(),
                cms__Site_Name__c            = 'Public',
                cms__Revision_Number__c      = 0,
                cms__Revision_Origin__c      = null,
                cms__Version_Number__c       = 1,
                cms__Version_Origin__c       = null,
                cms__Version_Original__c     = true,
                cms__Version_Parent__c       = null,
                cms__Depth__c                = 0
        );
        insert txc;
        
        cms__Content_Layout__c cl = new cms__Content_Layout__c(cms__Name__c = 'ARSelfRegistrationCL');
        insert cl;
        
        cms__Page__c page = new cms__Page__c(cms__Name__c = 'ARSelfRegistrationPage', cms__Site_Name__c = 'Public');
        insert page;
        
        cms__Content_Layout_Instance__c cli = new cms__Content_Layout_Instance__c(cms__Content__c = txc.Id, cms__Content_Layout__c = cl.Id);
        insert cli;

       System.assertEquals(cli.cms__Content_Layout__c,cl.Id);

        cms__Page_Content_Layout_Instance__c pcli = new cms__Page_Content_Layout_Instance__c(cms__Content_Layout_Instance__c=cli.Id,cms__Page__c=page.Id);
        insert pcli;

        cms.API anAPI = new cms.API(null, 'prev');
        anAPI.site_name = 'Public';
        
        System.currentPageReference().getParameters().put('ecms', anAPI.getSerialize());
        System.currentPageReference().getParameters().put('content_id', txc.Id);
        System.currentPageReference().getParameters().put('cli_id', cli.Id);
        System.currentPageReference().getParameters().put('pcli_id', pcli.Id);

        cms.GenerateContent gc = new cms.GenerateContent();

        gc.content = txc;
        gc.cli = cli;
        gc.pcli = pcli;

       return gc;
    }
    
    static testmethod void testmethod1() {
    
    cms.GenerateContent gc = setUpController();
    
    cms.Link link=new cms.Link('test');
    link.Targetpage='test#tab';
    
    user u=[select id,username from user where name='IT Service Account'];
    ocms_PortalMessage op=new ocms_PortalMessage(gc);
    
    list<Portal_Message__c> pmlist=new list<Portal_Message__c>();
    for(integer i=0;i<5;i++)
    {
    Portal_Message__c pm=new Portal_Message__c();
  //  pm.name='test';
    pm.Message_Subject__c='test';
    pm.Message_Body__c='test';
    pm.New_Message__c=true;
    pm.Message_Date__c=system.today();
    pm.Message_User__c=u.id;
    pmlist.add(pm);
    }
    insert pmlist;
    
    Portal_Message__c pm1=new Portal_Message__c();
  //  pm1.name='test';
    pm1.Message_Subject__c='test';
    pm1.Message_Body__c='test';
    pm1.New_Message__c=true;
    pm1.Message_Date__c=system.today();
    pm1.Message_User__c=u.id;
    insert pm1;
    
    System.assertEquals(pm1.Message_User__c,u.id);

    
    cms__Content_Layout__c cl = new cms__Content_Layout__c(cms__Name__c = 'ARSelfRegistrationCL');
        insert cl;
        
    ApexPages.currentPage().getParameters().put('msgid',pm1.id);
     
    ocms_PortalMessage op1=new ocms_PortalMessage();
    op1.getHTML();
    op1.getType();
    map<string,string> exr=new map<string,string>();
    exr.put('action','deleteCurrentMessage');
    exr.put('action','deleteMessages');
    exr.put('action','getNewMessageCount');
   exr.put('msgid',pmlist[0].id);
  /*  exr.put('msgid',pmlist[1].id);
    exr.put('msgid',pmlist[2].id);
    exr.put('msgid',pmlist[3].id);
    exr.put('msgid',pmlist[4].id);
    
    String formattedJson = '{"msgIds":' + exr.get('msgIds') + '}';
    
    messageIds abc=new messageIds();
    
    messageIds messageIdList = (messageIds)JSON.deserialize(formattedJson, messageIds.class);
   */
            
    string response =op1.executeRequest(exr);
    
    
    op1.getContentLayoutNameByContentLayoutId(cl.id);
    op1.setDetailMessage();
    op1.pm=null;
    op1.detailPageString='test';
    op1.dPage=link.targetpage;
    op1.messagePageString='test';
    op1.mPage=link.targetpage;
    op1.updateNewMsgStatus();
    op1.deleteCurrentMessage(pm1.id);
    op1.messagePageCMSLink=link;
   // op1.deleteMessages();
    }
    
}