public with sharing class Deal_AcquisitionDispositionInvoke {

    static Map<String, String> BAD_ACQ_STATUS= new Map<String, String>{'On Hold'=>'On Hold', 'Lost'=>'Lost', 'Passed'=>'Passed'};
    
    @InvocableMethod(label='invokeDeal' description='invokeDeal')
    public static void invokeDeal (List<string> inputParams) {
        
        //system.debug(logginglevel.info, '--------ids: '+inputParams);

        Map<Id, Deal__c> deals= new Map<Id, Deal__c>([select id, MRIProperty__c, Owned_Id__c,Property_Owner_SPEL__c,Deal_Status__c,RecordType.Name from Deal__c
                                where id in :inputParams and RecordType.Name IN ('Acquisition', 'Disposition','Sale Leaseback') and Deal_Status__c Not In ('Dead','Dead-Legal') 
                                order by Owned_Id__c,RecordType.Name,createddate asc]);
        system.debug('deals'+deals);

        
        Set<String> ownedIds= new Set<String>();

        for(Deal__c deal: deals.values()){
            ownedIds.add(deal.Owned_Id__c);
        }

        Map<Id, MRI_PROPERTY__c> props= new Map<Id, MRI_PROPERTY__c>([select id, Property_ID__c,Property_Owner_SPE__c, Related_Deal__c, Disposition_in_DC__c 
                                        from MRI_PROPERTY__c 
                                        where Related_Deal__c IN :deals.keySet() or Disposition_in_DC__c IN :deals.keySet() or Property_ID__c IN :ownedIds]);
        
        Map<Id, MRI_PROPERTY__c> updateMap= new Map<Id, MRI_PROPERTY__c>();

        //system.debug(logginglevel.info, '--------props: '+props.keySet());

        for(Deal__c deal: deals.values()){

            for(MRI_PROPERTY__c prop: props.values()){

                // existing mri_properties will get effected. This is the case where reference needs to be removed. type acquisition
                if(prop.Related_Deal__c== deal.Id && !prop.Property_ID__c.equalsIgnoreCase(deal.Owned_Id__c)){
                    prop.Related_Deal__c= null;
                    if(updateMap.get(prop.Id)!=null){
                        updateMap.get(prop.Id).Related_Deal__c= null;
                        updateMap.get(prop.Id).Property_Owner_SPE__c=null;
                        continue;
                    }
                    
                    // existing mri_properties will get effected. This is the case where reference needs to be removed. type disposition
                }else if (prop.Disposition_in_DC__c== deal.Id && !prop.Property_ID__c.equalsIgnoreCase(deal.Owned_Id__c)) {
                    prop.Disposition_in_DC__c= null;
                    if(updateMap.get(prop.Id)!=null){
                        updateMap.get(prop.Id).Disposition_in_DC__c= null;
                        continue;
                    }
                    
                // newly created or ownedId modified on deal and new mri_property satisfies the criteria. type acquisition
                }else if (prop.Property_ID__c.equalsIgnoreCase(deal.Owned_Id__c) && (deal.RecordType.Name=='Acquisition' || deal.RecordType.Name=='Sale Leaseback') && BAD_ACQ_STATUS.get(deal.Deal_Status__c)==null) {
                    prop.Related_Deal__c= deal.Id;
                    if(updateMap.get(prop.Id)!=null){
                        updateMap.get(prop.Id).Related_Deal__c= deal.Id;
                        updateMap.get(prop.Id).Property_Owner_SPE__c= deal.Property_Owner_SPEL__c;
                        continue;
                    }

                // newly created or ownedId modified on deal and new mri_property satisfies the criteria. type disposition
                }else if (prop.Property_ID__c.equalsIgnoreCase(deal.Owned_Id__c) && deal.RecordType.Name=='Disposition') {
                    prop.Disposition_in_DC__c= deal.Id;
                    if(updateMap.get(prop.Id)!=null){
                        updateMap.get(prop.Id).Disposition_in_DC__c= deal.Id;
                        continue;
                    }
                    
                }else {
                    continue;
                }

                updateMap.put(prop.Id, prop);
            }

        }

        //system.debug(logginglevel.info, '--------------updateMap: \n'+updateMap.keySet());

        if(updateMap.isEmpty())
            return;

        try{
            update updateMap.values();
        }catch(Exception e){}

    }
}