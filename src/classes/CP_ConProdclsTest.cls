@isTest
public class CP_ConProdclsTest {
    static testMethod void CP_ConProdclsTest () {
        CP_HelperAccConProdcls hp = new CP_HelperAccConProdcls ();
        account acc = new account();
        acc.name='Test acc con';
        insert acc;

        Contact con= new Contact();
        con.FirstName='Test First Name';
        con.LastName='Test Last Name';
        con.AccountId=acc.id;
        insert con;
        
        New_Competitor__c nc = new New_Competitor__c();
        nc.name = 'test test';
        insert nc;

        List<New_Product__c> npList = new List<New_Product__c>();
        
        New_Product__c np = new New_Product__c();
        np.Average_Lease_term__c='lease';
        np.Competitor__c = nc.id;
        insert np;
        //npList.add(np);
        New_Product__c np1 = new New_Product__c();
        np1.Average_Lease_term__c='lease1';
        insert np1;
        //npList.add(np1);
        //insert npList; 

        Contact_Product__c cp = new Contact_Product__c();
        cp.Contact__c=con.id;
        cp.Product__c=np.id;
        insert cp;
        
        PageReference pageRef = Page.CP_ConProd;  
        ApexPages.currentPage().getParameters().put('Id',con.id);
        Test.setCurrentPage(pageRef);       
        Boolean selected;
        string str;
        test.startTest();
        Test.setCurrentPage(Page.CP_ConProd);
        ApexPages.StandardController stdcon = new ApexPages.StandardController(np);
        CP_ConProdcls cpc = new CP_ConProdcls (stdcon);
        //cpc.add('ABC');

        cpc.search();
        System.assertEquals(null,str);
        cpc.str='Test';
        cpc.selected =true;
        cpc.search();
        cpc.cls.isFailed=true;
        cpc.curid = con.id;
        cpc.cls.selectprod.add(np);
        cpc.cls.Searchpro(str);
        cpc.cls.pwcList[0].selectedValue = true;
        cpc.addcont();
        cpc.cls.pwcList[0].selectedValue = false;
        cpc.addcont();
        //hp.isFailed=true;
        hp.selectprod.add(np);
        hp.selectprod.add(np1);
        System.assertEquals(true,hp.selectprod.size()>0);
        cpc.Clear();
        test.stopTest();
    }
}