@isTest
private class TestEventAutomationApprovalController {
   static testmethod void TestEventAutomation() {
   // User usr = [Select id from User where firstname=:'test' Limit 1];     
    Id eventrec=[Select id,name,DeveloperName from RecordType where DeveloperName='EventAutomationRecordType' and SobjectType = 'Event_Automation_Request__c'].id;
    Test.StartTest();
   // system.runAs(usr)
//{
   Event_Automation_Request__c  e = new Event_Automation_Request__c ();
        e.recordtypeid  = eventrec;
         e.Owner_Department__c = 'test';
         e.Name_of_the_Entity__c = 'test';
         e.Start_Date__c = system.today();
         insert e;
          ApexPages.currentPage().getParameters().put('id',e.id); 
    ApexPages.StandardController sc = new ApexPages.StandardController(e);
    EventAutomationApprovalHistoryController ea = new EventAutomationApprovalHistoryController(sc);
    
    ea.getApprovalSteps();
       
        list<ProcessInstance> processInstances  = [select id from ProcessInstance where TargetObjectId =:e.id];
         system.assertequals(processInstances.size(),0);
       //  }
     Test.StopTest();
     }
  
  }