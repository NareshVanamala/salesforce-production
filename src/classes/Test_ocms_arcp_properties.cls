@isTest (SeeAllData=true)
private class Test_ocms_arcp_properties {
    
    @isTest static void test_getResultTotal() {
        
        ocms_arcp_properties propertyList = new ocms_arcp_properties();

        String state = 'TX';
        String city = 'Houston';
        String tenantIndustry = '';
        String tenant = '';
        String[] filterArray = new List<String>();
        filterArray.add('Office');
        
        system.assert(filterArray != Null);

        propertyList.getResultTotal(state,city, tenantIndustry, tenant, filterArray);

        state = '';
        city = '';
        tenantIndustry = 'Office';

        propertyList.getResultTotal(state,city,tenantIndustry,tenant,filterArray);

        tenantIndustry = '';
        tenant = '**VACANT**';

        propertyList.getResultTotal(state,city,tenantIndustry,tenant,filterArray);


    }
    
    @isTest static void test_getPropertyList() {

       ocms_arcp_properties propertyList = new ocms_arcp_properties();

        String state = 'TX';
        String city = 'Houston';
        String tenantIndustry = '';
        String tenant = '';
        String[] filterArray = new List<String>();
        filterArray.add('Office');
        
        system.assert(filterArray != Null);

        propertyList.getPropertyList(state,city,tenantIndustry,tenant,filterArray,'SqFt__c', '25', '0');

        state = '';
        city = '';
        tenantIndustry = 'Office';
        filterArray.add('Industrial');


        propertyList.getPropertyList(state,city,tenantIndustry,tenant,filterArray,'Name', '25', '0');

        tenantIndustry = '';
        tenant = '**VACANT**';

        propertyList.getPropertyList(state,city,tenantIndustry,tenant,filterArray,'State__c', '25', '0');

        try{
        propertyList.getHTML();
        }
        catch(Exception e){}
    }

    @isTest static void test_getMethods() {

        ocms_arcp_properties propertyList = new ocms_arcp_properties();
         
        propertyList.getStateList();

        propertyList.getCityList('TX');

        propertyList.getIndustryList();

        propertyList.getTenantList();
        
        system.assert(propertyList != Null);

        try{
        propertyList.getHTML();
        }
        catch(Exception e){}
    }

    
}