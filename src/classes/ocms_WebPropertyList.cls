global virtual without sharing class ocms_WebPropertyList extends cms.ContentTemplateController{

    private List<SObject> propertyList;
    private String html;
    public string webready='Approved';
    public Map<String, String> stateMap = new Map<String, String>(); // Used to store an indexed list of state names

    // Stores the target page set by editing the content
    public String webPropertyTargetPage{
        get {
            return getProperty('webPropertyTargetPage');
        }
        set;
    }

    // This object is created from the csv set in webPropertyTargetPage
    public cms.Link webPropertyTargetPageObj {
        get { 
            if(this.webPropertyTargetPageObj == null){
                if(this.webPropertyTargetPage != null){
                    this.webPropertyTargetPageObj = new cms.Link(this.webPropertyTargetPage);
                }
            }
            return this.webPropertyTargetPageObj;
        }   
        set;
    }

    // ocms_WebPropertyList() - Constructor - on creation, populate stateMap with a list of state names indexed by the 2 character short form.
    global ocms_WebPropertyList() {
        stateMap.put('AL','Alabama');
        stateMap.put('AK','Alaska');
        stateMap.put('AZ','Arizona');
        stateMap.put('AR','Arkansas');
        stateMap.put('CA','California');
        stateMap.put('CO','Colorado');
        stateMap.put('CT','Connecticut');
        stateMap.put('DE','Delaware');
        stateMap.put('FL','Florida');
        stateMap.put('GA','Georgia');
        stateMap.put('HI','Hawaii');
        stateMap.put('ID','Idaho');
        stateMap.put('IL','Illinois');
        stateMap.put('IN','Indiana');
        stateMap.put('IA','Iowa');
        stateMap.put('KS','Kansas');
        stateMap.put('KY','Kentucky');
        stateMap.put('LA','Louisiana');
        stateMap.put('ME','Maine');
        stateMap.put('MD','Maryland');
        stateMap.put('MA','Massachusetts');
        stateMap.put('MI','Michigan');
        stateMap.put('MN','Minnesota');
        stateMap.put('MS','Mississippi');
        stateMap.put('MO','Missouri');
        stateMap.put('MT','Montana');
        stateMap.put('NE','Nebraska');
        stateMap.put('NV','Nevada');
        stateMap.put('NH','New Hampshire');
        stateMap.put('NJ','New Jersey');
        stateMap.put('NM','New Mexico');
        stateMap.put('NY','New York');
        stateMap.put('NC','North Carolina');
        stateMap.put('ND','North Dakota');
        stateMap.put('OH','Ohio');
        stateMap.put('OK','Oklahoma');
        stateMap.put('OR','Oregon');
        stateMap.put('PA','Pennsylvania');
        stateMap.put('RI','Rhode Island');
        stateMap.put('SC','South Carolina');
        stateMap.put('SD','South Dakota');
        stateMap.put('TN','Tennessee');
        stateMap.put('TX','Texas');
        stateMap.put('UT','Utah');
        stateMap.put('VT','Vermont');
        stateMap.put('VA','Virginia');
        stateMap.put('WA','Washington');
        stateMap.put('WV','West Virginia');
        stateMap.put('WI','Wisconsin');
        stateMap.put('WY','Wyoming');
    } 
    //ocms_WebPropertyList()

    // getResultTotal(String, String, String, String, String, String[]) - Used to get the count of the query created from the values supplied.
    public Integer getResultTotal(String portfolio, String state, String city, String tenantIndustry, String tenant, String[] filters){
        
        String query;
        set<string> ab = getSelectedPortfolio(portfolio);
        
        query = 'SELECT COUNT() FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Program_Id__c in:ab and Status__c=\'Owned\'';
                
        if(state != null && state != '' && state != 'all'){
            query += ' AND State__c = \'' + state + '\'';
        }

        if(city != null && city != '' && state != 'all' && city != 'all'){
            query += ' AND City__c = \'' + city + '\'';
        }

        if(tenantIndustry != null && tenantIndustry != '' && tenantIndustry != 'all'){
            query += ' AND Sector__c = \'' + tenantIndustry + '\'';
        }

        if(tenant != null && tenant != '' && tenant != 'all'){
            query += ' AND Website_Tenant_Name__c = \'' + tenant + '\'';
        }

        if(filters != null && filters.size() > 0){
            query += ' AND ( ';
            Boolean first = true;
            for(String f: filters){
                if(first){
                    first=false;
                }else{
                    query += ' OR ';
                }

                query += 'Property_Type__c = \'' + f + '\'';

            }
            query += ' )';
        }

        return Database.countQuery(query);

    } // getResultTotal

    // getPropertyList(String, String, String, String, String, String[], String, String, String) - Main utility class, used to return a list of properties affected by the parameters passed.
    public List<SObject> getPropertyList(String portfolio, String state, String city, String tenantIndustry, String tenant, String[] filters, String sortBy, String rLimit, String offset ){

        List<SObject> propertyList;
        String query;
        set<string> ab = getSelectedPortfolio(portfolio);
        query = 'SELECT Id, Name,Common_Name__C,Web_Name__c, Is_Featured_Property__c, City__c, State__c, Property_type__c, Date_Acquired__c, Location__c, Image__c FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Program_Id__c in:ab and Status__c=\'Owned\'';
        
        if(state != null && state != '' && state != 'all'){
            query += ' AND State__c = \'' + state + '\'';
        }

        if(city != null && city != '' && state != 'all' && city != 'all'){
            query += ' AND City__c = \'' + city + '\'';
        }

        if(tenantIndustry != null && tenantIndustry != '' && tenantIndustry != 'all'){
            query += ' AND Sector__c = \'' + tenantIndustry + '\'';
        }

        if(tenant != null && tenant != '' && tenant != 'all'){
            query += ' AND Website_Tenant_Name__c = \'' + tenant + '\'';
        }

        if(filters != null && filters.size() > 0){
            query += ' AND ( ';
            Boolean first = true;
            for(String f: filters){
                if(first){
                    first=false;
                }else{
                    query += ' OR ';
                }

                query += 'Property_Type__c = \'' + f + '\'';

            }
            query += ' )';
        }

        if(sortBy != null && sortBy != ''){
          
           if( sortBy == 'location' )
           {
                query += ' ORDER BY State__c ASC, City__c ASC';
           }
           else if(sortBy == 'Is_Featured_Property__c')
           {
               query += ' ORDER BY Is_Featured_Property__c DESC, Date_Acquired__c DESC';
           }
           else if(sortBy == 'Date_Acquired__c')
           {
               query += ' ORDER BY Date_Acquired__c DESC';
           }
           else 
           {
               query += ' ORDER BY ' + sortBy + ' ASC ';
           }
        }

        if(rLimit != null && rLimit != ''){
            query += ' LIMIT ' + rLimit;
        }

        if(offset != null && offset != ''){
            query += ' OFFSET ' + offset;
        }
        system.debug('propertyList123'+query);
        propertyList = Database.query(query);
        return propertyList;
    }// getPropertyList


    // Service methods - Simple methods used to return dropdown values mostly.

    // getPortfolioList() - returns list of portfolios
    public List<SObject> getPortfolioList(){
        //List<SObject> portfolioList = new ;
       Map<String,Property_Program_Codes__c> allCodes = Property_Program_Codes__c.getAll();
       list<Property_Program_Codes__c> code = allCodes.values();
       list<string> ab=new list<string>();
       for(Property_Program_Codes__c code1:code)
       {
           ab.add(code1.Program_Code__c);
       }
        
        String query = 'SELECT ProgramName__c FROM Web_Properties__c where ProgramName__c in:ab and Status__c=\'Owned\' and Web_Ready__c=\'' + webready + '\'  GROUP BY ProgramName__c';
        List<SObject> portfolioList = Database.query(query);

        return sanitizeList(portfolioList, 'ProgramName__c');
    }
    // getPortfolioList()


    // getStateList() - returns a JSON formatted string that contains both the short form and long
    //                  form for the states that contain property's, if a state does not contain any
    //                  property's it will be excluded from the list.
  
  
    public String getStateList(String portfolio){

        List<SObject> stateList;
        List<String> outList = new List<String>();
        set<string> ab = getSelectedPortfolio(portfolio);
        String response = '';
        Boolean first = true;

        String query = 'SELECT State__c FROM  Web_Properties__c where Status__c=\'Owned\' and Web_Ready__c=\'' + webready + '\' and Program_Id__c in:ab GROUP BY State__c ORDER BY State__c ASC';
        stateList = Database.query(query);
        stateList = sanitizeList(stateList, 'State__c');

        response+='[';

        for(SOBject s : stateList){
            if(first){
                first=false;
            }else{
                response+=', ';
            }
            response+='{"short":"' + String.valueOf(s.get('State__c')) + '", ';
            if(stateMap.get(String.valueOf(s.get('State__c'))) != null)
                response+='"long":"' + stateMap.get(String.valueOf(s.get('State__c'))) + '"}';
            else
                response+='"long":"' + String.valueOf(s.get('State__c')) + '"}';
        }

        response+=']';

        return response;

    }
    // getStateList()

    // getCityList(String) - return a list of all city's that have property's from the specified state. Refreshes
    //                       whenever the state dropdown is changed, only loads cities from the specified state
    //                       and only cities that contain property's.
    public List<SObject> getCityList(String state,String portfolio){

        List<SObject> cityList;
        set<string> ab = getSelectedPortfolio(portfolio);
        String query = 'SELECT City__c FROM Web_Properties__c WHERE State__c = \'' + state + '\' and Status__c=\'Owned\' and Web_Ready__c=\'' + webready + '\' and Program_Id__c in:ab GROUP BY City__c ORDER BY City__c ASC';
        cityList = Database.query(query);

        return sanitizeList(cityList, 'City__c');

    }
    // getCityList()

    // getIndustryList() - returns a list of each unique industry.
    public List<SObject> getIndustryList(String portfolio){

        List<SObject> industryList;
        set<string> ab = getSelectedPortfolio(portfolio);
        String query = 'SELECT Sector__c FROM Web_Properties__c where Status__c=\'Owned\' and Web_Ready__c=\'' + webready + '\' and Program_Id__c in:ab GROUP BY Sector__c ORDER BY Sector__c ASC';
        industryList = Database.query(query);

        return sanitizeList(industryList, 'Sector__c');

    }
    // getIndustryList()

public set<string> getSelectedPortfolio(String StrPortfolio)
{
       set<string> ListPortfolio=new set<string>();
       List<SObject> Portfolio;
       String Portfolioquery='';
       
       if(StrPortfolio == 'Current Programs')
       {
           Portfolioquery = 'select Name from Property_Program_Codes__c';
       }
       else if(StrPortfolio == 'Past Programs')
       {     
           Portfolioquery = 'select Name from Property_Program_Codes_Past__c';
       }
       else
       {
           ListPortfolio.add(StrPortfolio);
           return ListPortfolio;
       }
       
       if(Portfolioquery != null || Portfolioquery != '')
       {
           Portfolio= Database.query(Portfolioquery);
           if(Portfolio.size()>0)
           {
                for(SObject c: Portfolio)
                {
                     ListPortfolio.add(String.valueOf(c.get('Name')));
                }
           }
       }

       return ListPortfolio;
}

    // getTenantList() - return a complete list of unique tenants.
    public List<SObject> getTenantList(string portfolio)
    {
        List<SObject> tenantList;
        set<string> ab = getSelectedPortfolio(portfolio);
        string query;
        query = 'SELECT Website_Tenant_Name__c FROM Web_Properties__c WHERE Program_Id__c in:ab and Status__c=\'Owned\' and Web_Ready__c=\'' + webready + '\' GROUP BY Website_Tenant_Name__c';
        tenantList = Database.query(query);
        return sanitizeList(tenantList, 'Website_Tenant_Name__c');
    }
    // getTenantList()

    // sanitizeList(List<SObject>, String) - used to remove any null values from a list passed in.
    private List<SObject> sanitizeList(List<SObject> inList, String field){

        Integer ctr = 0;

        while(ctr < inList.size()){
            if(inList[ctr].get(field) == null){
                inList.remove(ctr);
            } else {
                ctr++;
            }
        }
        return inList;

    }
    //sanitizeList()

    // writeControls() - 
    private String writeControls(){
        String html = '';
        //List<SObject> portfolioList;
        //List<SObject> stateList;

        //found that 'html += ' on every line was creating a performance issue, replaced with one long assignment broken up with +'s improved load time.

    /*   Map<String,Property_Program_Codes__c> allCodes = Property_Program_Codes__c.getAll();
       list<Property_Program_Codes__c> code = allCodes.values();
       list<string> ab=new list<string>();
       for(Property_Program_Codes__c code1:code)
       {
           ab.add(code1.Program_Code__c);
       }
       
       Map<String,Property_Program_Codes_Past__c> allCodes1 = Property_Program_Codes_Past__c.getAll();
       list<Property_Program_Codes_Past__c> code11 = allCodes1.values();
       list<string> cd=new list<string>();
       for(Property_Program_Codes_Past__c code1:code11)
       {
           cd.add(code1.Program_Code__c);
       }*/
       
        html += '<input type="hidden" id="targetPage" value="' + String.valueOf(webPropertyTargetPageObj.targetPage).replaceAll('#','') + '" />';

        html += '<div class="ocms_WebPropertyList">' +
                '   <div class="ocms_WP_Controls">' +
                '       <div class="ocms_WP_label"><strong>Portfolio:</strong></div>' +
                '       <div class="ocms_WP_dropdown">' +
                '           <select class="ocms_WP_portfolioDropdown">' ;
                
                List<SObject> CurrentPortfolio= Database.query('select Name,Program_Code__c from Property_Program_Codes__c');
                if(CurrentPortfolio.size()>0)
                {
                    html += '<option value="Current Programs">Current Programs</option>';
                    for(SObject c: CurrentPortfolio)
                    {
                        html += '<option value="' + c.get('Name') + '">' + c.get('Program_Code__c') + '</option>';
                    }
                }
                List<SObject> PastPortfolio= Database.query('select Name,Program_Code__c from Property_Program_Codes_Past__c');
                if(PastPortfolio.size()>0)
                {
                    html += '<option value="Past Programs">Past Programs</option>';
                    for(SObject p: PastPortfolio)
                    {
                        html += '<option value="' + p.get('Name') + '">' + p.get('Program_Code__c') + '</option>';
                    }
                }
                
        html += '           </select>' +
                '   </div>' +
                '   <div class="ocms_WP_narrowResultsArea">' +
                '       <div class="ocms_WP_narrowResults">' +
                '           <a href="javascript:void(0)">Narrow Results</a>' +
                '       </div>' +
                '       <div class="ocms_WP_searchForm">' +
                '           <div class="ocms_WP_leftColumn">' +
                '               <div class="ocms_WP_narrowRadios">' +
                '                   <input class="byLocationRadio" type="radio" name="narrowBy" value="byLocation">By Location</input>' +
                '                   <input class="byIndustryRadio" type="radio" name="narrowBy" value="byIndustry">By Industry</input>' +
                '                   <input class="byTenantRadio" type="radio" name="narrowBy" value="byTenant">By Tenant</input>' +
                '               </div>' +
                '               <div class="byLocationForm">' +
                '                   <div class="ocms_WP_label"><strong>State</strong></div>' +
                '                   <select class="stateDropdown">' +
                '                       <option value="all">All States</option>' +
                '                   </select>' +
                '                   <div class="ocms_WP_label"><strong>City</strong>' +
                '                   <select class="cityDropdown">' +
                '                       <option value="all">All Cities</option>' +
                '                   </select>' +
                '                   </div>' +
                '               </div>' +
                '               <div class="byIndustryForm">' +
                '                   <div class="ocms_WP_label"><strong>Tenant Industry</strong></div>' +
                '                   <select class="industryDropdown">' +
                '                       <option value="all">All Industries</option>' +
                '                   </select>' +
                '               </div>' +
                '               <div class="byTenantForm">' +
                '                   <div class="ocms_WP_label"><strong>Tenant Name</strong></div>' +
                '                   <select class="tenantDropdown">' +
                '                       <option value="all">All Tenants</option>' +
                '                   </select>' +
                '               </div>' +
                '           </div>' +
                '           <div class="filterBy">' +
                '               <div class="ocms_WP_label"><strong>Filter By Property Type</strong></div>' +
                '               <div class="sector">' +
                '                   <div class="leftColumn">' +
                '                       <div class="filterContainer">' +
                '                           <input type="checkbox" class="filterRetail" name="propertyCheckbox" value="Freestanding Retail">Freestanding Retail</input>' +
                '                       </div>' +
                '                       <div class="filterContainer">' +
                '                           <input type="checkbox" class="filterMultiTenant" name="propertyCheckbox" value="Multi-tenant Retail">Multi-tenant Retail</input>' +
                '                       </div>' +
                '                   </div>' +
                '                   <div class="rightColumn">' +
                '                       <div class="filterContainer">' +
                '                           <input type="checkbox" class="filterOffice" name="propertyCheckbox" value="Office">Office</input>' +
                '                       </div>' +
                '                       <div class="filterContainer">' +
                '                           <input type="checkbox" class="filterIndustrial" name="propertyCheckbox" value="Industrial">Industrial</input>' +
                '                       </div>' +
                '                   </div>' +
                '               </div>' +
                '           </div>' +
                '       </div>' +
                '   </div>' +
                '   <div class="tabSwitchBottom">' +
                '       <ul class="heroContentNav">' +
                '           <li class="listViewControl">List View</li>' +
                '           <li class="mapViewControl">Map View</li>' +
                '       </ul>' +
                '   </div>' +
                '   <div class="resultBar">' +
                '       <div class="resultBarCount">' +
                '           <div class="propertyCountContainer"><span class="propertyCount"></span> <span class="propertyText"></span></div>' +
                '           <div class="displayingResults">Displaying <span class="lower"></span> - <span class="upper"></span> of <span class="propertyCount"></span></div>' +
                '       </div>' +
                '       <div class="display">' +
                '           <div class="ocms_WP_label"><strong>Display</strong></div>' +
                '           <div class="uniformDropdown">' +
                '               <select class="displayDropdown">' +
                '                   <option value="25">25 Results</option>' +
                '                   <option value="50">50 Results</option>' +
                '                   <option value="100">100 Results</option>' +
                '                   <option value="200">200 Results</option>' +
                '               </select>' +
                '           </div>' +
                '       </div>' +
                '       <div class="sort">' +
                '           <div class="ocms_WP_label"><strong>Sort By</strong></div>' +
                '           <div class="uniformDropdown">' +
                '               <select class="sortByDropdown">' +
                '                   <option value="Is_Featured_Property__c">Featured Properties</option>' +
                '                   <option value="Date_Acquired__c">Acquisition Date</option>' +
                '                   <option value="Name">Property Name</option>' +
                '                   <option value="location">Location</option>' +
                '               </select>' +
                '           </div>' +
                '       </div>' +
                '       <div class="resultBarPaginationTop">' +
                '           <div class="leftArrow"></div>' +
                '           <div class="paginateText">Page <span class="pageLower"></span> of <span class="pageUpper"></span></div>' +
                '           <div class="rightArrow"></div>' +
                '       </div>' +
                '   </div>' +
                '   </div>' +
                '   <div class="propertyListView"></div>' +
                '   <div class="propertyMapView">' +
                '       <div class="propertyList"></div>' +
                '       <div id="map-canvas"></div>' +
                '   </div>' +
                '   <div class="resultBarPaginationBottom">' +
                '       <div class="leftArrow"></div>' +
                '       <div class="paginateText">Page <span class="pageLower"></span> of <span class="pageUpper"></span></div>' +
                '       <div class="rightArrow"></div>' +
                '   </div>' +
                '</div>';

        return html;
    }
    // writeControls()

    //This method is capable of returning a url to a static resource. This is left in and commented in case Salesforce requires a timestamp
    //in the future, this class is put in a managed package, or for whatever reason hardcoding the url doesn't work anymore.
    //private String getStaticResourceURL(String resourceName){
    //  List<StaticResource> resourceList = [SELECT Name, NamespacePrefix, SystemModStamp FROM StaticResource WHERE Name = :resourceName];
    //  if(resourceList.size() == 1){
    //      String namespace = resourceList[0].NamespacePrefix;
    //      return '/resource/' + resourceList[0].SystemModStamp.getTime() + '/' + (namespace != null && namespace != '' ? namespace + '__' : '') + resourceName;
    //  } else {
    //      return '';
    //  }
    //}

    // getHTML() - required method - This is the method that gets called automatically upon content instantiation.
    global override virtual String getHTML(){
        String html = '';

        html += writeControls();
        html += '<script src="resource/1468981717000/ocms_WebPropertylistjs" type="text/javascript"></script>';

        return html;
    }
    // getHTML()

}