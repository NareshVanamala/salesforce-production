public with sharing class CompetativeProducts1{

    Public List<Competiive_Product__c> memberList{get;set;}
    public Competiive_Product__c deletelist1;
    public Competiive_Product__c updatedlist1;
    Public List<Competiive_Product__c> updatemelist;
    Public List<Competiive_Product__c> deleteemelist;
    public integer removepos{get;set;}
    public string conid{get;set;}
    public Id recId{get;set;} 
    
    public string fileName{get;set;} 
    public Blob fileBody{get;set;}
    
    public Attachment attachment{get;set;}
    public Attachment myatt{get;set;}
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    public Boolean showInputFile{get;set;}
     
    public Account a;  
        public CompetativeProducts1(ApexPages.StandardController controller) {
        addrow = new Competiive_Product__c();
        //myAttachment = new Attachment();

        a =(Account)controller.getrecord();
        showInputFile = false;
        //a =[select id from Account where id=:ApexPages.currentPage().getParameters().get('id')];
        //a=[select id from Account where id= '001P000000adMsB'];
        memberList =[select name, id,(Select Id,Name from Attachments) Attmts,Account__c,Assets_Sales__c,Close_Date__c,Offering_Date__c,Product__c,Rank__c,Sponsor__c from Competiive_Product__c where Account__c=:a.id order by LastModifiedDate DESC];  
        deletelist1= new Competiive_Product__c();  
        updatedlist1=new Competiive_Product__c();
        updatemelist=new list<Competiive_Product__c>();
        deleteemelist=new list<Competiive_Product__c>();
        recId= controller.getRecord().id;
        myatt=new attachment();
    }
    public Pagereference removecon(){
        if(memberList.size()>0)
        {
        
            string paramname= Apexpages.currentPage().getParameters().get('node');
            system.debug('Check the paramname'+paramname);
            for(Integer i=0;i<memberList.size();i++)
            {
                if(memberList[i].id==conid)
                {
                    removepos=i;
                    //deletelist1.id=memberList[i].id;
                    deleteemelist.add(memberList[i]);
                  
                }
            }
            if(removepos!=null)
            memberList.remove(removepos);
            system.debug('check the deletelist..'+deleteemelist);
           if(deleteemelist.size()>0)
             delete  deleteemelist;          
        }
       return ApexPages.currentPage();
   }   
   // Edit Record method ....
   Pagereference p;
   public Id recordId {set;get;}
   public Boolean edit {set;get;}   
   public Pagereference editRecord(){
       recordId = conid;
       edit = true;
       //p = new Pagereference('/apex/AdvancedProgrampage?id='+ApexPages.currentPage().getParameters().get('id'));
       return null;
   }
   
   /*public Pagereference viewRecord(){
       recordId = conid;   
      myatt=[select id, name, parentid from Attachment where parentid=:recordId ];
        system.debug('check the attachement..'+myatt);
       p = new Pagereference('/'+myatt.id);
      p.setRedirect(true);
       return p;
   }*/
   
   public Pagereference SaveRecord(){
      if(memberList.size()>0)
        {
             for(Integer i=0;i<memberList.size();i++)
            {
                if(memberList[i].id==conid)
                {
                   /*updatedlist1.id= memberList[i].id;
                   updatedlist1.name=memberList[i].name;

                   updatedlist1.Sponsor__c=memberList[i].Sponsor__c;
                   updatedlist1.Product__c=memberList[i].Product__c;
                   updatedlist1.Assets_Sales__c=memberList[i].Assets_Sales__c;
                   updatedlist1.Rank__c=memberList[i].Rank__c;
                   updatedlist1.Offering_Date__c=memberList[i].Offering_Date__c;
                   updatedlist1.Close_Date__c=memberList[i].Close_Date__c;*/
                   // updatedlist1.add(memberList[i]);
                   updatemelist.add(memberList[i]);                 
                }
            }
            if(updatemelist.size()>0)
            update updatemelist; 
             }
        system.debug('Please let me know you gets updated/not...'+updatedlist1);     
       edit = false;
       return null;
       
   }
    //adding new record
    public Boolean addnew{set;get;}
    public Competiive_Product__c addrow {set;get;}

    public Pagereference newProgram(){
        attachment =  new Attachment();
        addrow= new Competiive_Product__c();
        addnew=true;        
        showInputFile = true;
        return null;    
    }
    public Pagereference savenewProg(){
        addnew=false;
        try{
            addrow.Account__c = a.id;
            if (Schema.sObjectType.Competiive_Product__c.isCreateable())
            insert addrow;
            system.debug('Added --->'+addrow.Id);
           // memberList = [select name, id,(Select Id,Name from Attachments) Attmts,Account__c,Assets__c,Category__c,Cole_Product__c from Program__c where Account__c=:a.id order by LastModifiedDate DESC]; 
                        
            attachment.OwnerId = UserInfo.getUserId();
            attachment.ParentId = addrow.Id;// the record the file is attached to
            attachment.Name = nameFile;
            attachment.Body = contentFile;   
            //attachment.IsPrivate = true;
            
            system.debug('\n myAttachment.name--->'+attachment.name);
            system.debug('check the attachement..'+ attachment);
            try {
                if((attachment.Name!=null) &&(attachment.Body!=null))
                insert attachment;
          memberList =[select name, id,(Select Id,Name from Attachments) Attmts,Account__c,Assets_Sales__c,Close_Date__c,Offering_Date__c,Product__c,Rank__c,Sponsor__c from Competiive_Product__c where Account__c=:a.id order by LastModifiedDate DESC]; 
            } catch (DMLException e) {
                system.debug('check the attachement..'+ attachment);
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
            }
       
        }catch(Exception e){
            system.debug('Exception --->'+e);
        } 
          
         return ApexPages.currentPage();
           
    }
    public Pagereference cancelnewProg(){
        addnew=false;
        return null;    
    }  
   

    
 }