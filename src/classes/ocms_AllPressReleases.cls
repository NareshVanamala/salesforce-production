global virtual without sharing class ocms_AllPressReleases extends cms.ContentTemplateController{

    private List<SObject> propertyList;
    private String html;
       // public String getValue()
    global ocms_AllPressReleases() { 
                 
        } 
   
     public Integer getResultTotal(String year, String pressRelease){
        
        String query;
        
        query = 'SELECT COUNT() FROM Press_Releases__c where';
        
        
        if((year == 'all'  && pressRelease == 'all') || (year == ''  && pressRelease == '') || (year == null   && pressRelease == null )){
              year = year.escapeEcmaScript();
              pressRelease = pressRelease.escapeEcmaScript();
              query += ' Year__c != \'' + '' + '\'';
              query += ' AND Sytem_Type_Of_Release__c != \'' + '' + '\'';
        }
        else if((year != null && year != '' && year != 'all') && (pressRelease != null && pressRelease != '' && pressRelease != 'all' )){
              year = year.escapeEcmaScript();
              pressRelease = pressRelease.escapeEcmaScript();
              query += ' Year__c = \'' + year + '\'';
              query += ' AND Sytem_Type_Of_Release__c like  \'%' + pressRelease + '%\'';
   
        }
        else if((year == 'all'  && pressRelease != 'all') ){
              year = year.escapeEcmaScript();
              pressRelease = pressRelease.escapeEcmaScript();
              query += ' Year__c != \'' + '' + '\'';
              query += ' AND Sytem_Type_Of_Release__c like  \'%' + pressRelease + '%\'';
        }
        else if((year != 'all'  && pressRelease == 'all') || (year == ''  && pressRelease == '') || (year == null   && pressRelease == null )){
              year = year.escapeEcmaScript();
              pressRelease = pressRelease.escapeEcmaScript();
              query += ' Year__c = \'' + year + '\'';
              query += ' AND Sytem_Type_Of_Release__c != \'' + '' + '\'';
        }
        return Database.countQuery(query);

    } 
    
       public List<SObject> getPropertyList(String year, String pressRelease){

        List<SObject> propertyList;
        String query;
        system.debug('year123'+year ); 
        system.debug('pressRelease123'+pressRelease); 
        query = 'SELECT Id, Press_Release_Name__c,Sytem_Type_Of_Release__c,Type_Of_Release__c,Date__c,Published_Date__c, Article_Link__c,Description__c, Year__c FROM Press_Releases__c where';

       if((year == 'all'  && pressRelease == 'all') || (year == ''  && pressRelease == '') || (year == null   && pressRelease == null )){
              year = year.escapeEcmaScript();
              pressRelease = pressRelease.escapeEcmaScript();
              query += ' Year__c != \'' + '' + '\'';
              query += ' AND Sytem_Type_Of_Release__c!= \'' + '' + '\'';
        }
        else if((year != null && year != '' && year != 'all') && (pressRelease != null && pressRelease != '' && pressRelease != 'all' )){
              year = year.escapeEcmaScript();
              pressRelease = pressRelease.escapeEcmaScript();
              query += ' Year__c = \'' + year + '\'';
              query += ' AND Sytem_Type_Of_Release__c like  \'%' + pressRelease + '%\'';
   
        }
        else if((year == 'all'  && pressRelease != 'all') ){
              year = year.escapeEcmaScript();
              pressRelease = pressRelease.escapeEcmaScript();
              query += ' Year__c != \'' + '' + '\'';
              query += ' AND Sytem_Type_Of_Release__c like  \'%' + pressRelease + '%\'';
        }
        else if((year != 'all'  && pressRelease == 'all') || (year == ''  && pressRelease == '') || (year == null   && pressRelease == null )){
              year = year.escapeEcmaScript();
              pressRelease = pressRelease.escapeEcmaScript();
              query += ' Year__c = \'' + year + '\'';
              query += ' AND Sytem_Type_Of_Release__c != \'' + '' + '\'';
        }
        query += 'ORDER BY Published_Date__c DESC';
        system.debug('query123'+query); 
        propertyList = Database.query(query);
       
        return propertyList;
    }
    
  
    public List<SObject> getYearList(){

        List<SObject> yearList;

        String query = 'SELECT Year__c FROM Press_Releases__c Group by Year__c ORDER BY Year__c DESC';
        yearList = Database.query(query);

        return sanitizeList(yearList, 'Year__c');

    }
    public list<string> getpressReleaseList()
        {
          List<string> options = new List<string>();
                
           Schema.DescribeFieldResult fieldResult =
         Press_Releases__c.Type_Of_Release__c.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
           for( Schema.PicklistEntry f : ple)
           {
              options.add((f.getLabel()));
           }       
           return options;
        }

   
    private List<SObject> sanitizeList(List<SObject> inList, String field){

        Integer ctr = 0;

        while(ctr < inList.size()){
            if(inList[ctr].get(field) == null){
                inList.remove(ctr);
            } else {
                ctr++;
            }
        }
        return inList;

    }
 
    private String writeControls(){
        String html = '';
  html += '<div class="col-md-9 row "><div class="ocms_WebPropertyList"><h3>News & Press Releases</h3>' +
              
                '   <div>'+
                '   </div>'+
                '   <div class="ocms_WP_Controls">' +
                '   <div class="ocms_WP_narrowResultsArea">' +
                '   <div>'+
                '   <div class="ocms_WP_label" style="width:100%;"><strong>Display: </strong><select class="pressReleaseDropdown"> ' +
                '        <option value="all">All Press Releases</option>' +    
                '                   </select><strong style="margin-left:10px;">Year: </strong><select class="yearDropdown">' +
                '                       <option value="all">All Years</option>' +
                '                   </select></div>' +
                '                   <div class="ocms_WP_label"></div>' +         
                '    </div>'+
                '   <div class="resultBar" style="border-bottom: medium none;">' +
                '       <div class="resultBarCount">' +
                '           <div class="propertyCountContainer"><span class="propertyCount"></span> <span class="propertyText"></span></div>' +
                '           <div class="displayingResults">Displaying <span class="lower"></span> - <span class="upper"></span> of <span class="propertyCount"></span></div>' +
                '       </div>' +
                '       <div class="resultBarPaginationTop">' +
                '           <div class="leftArrow"></div>' +
                '           <div class="paginateText">Page <span class="pageLower"></span> of <span class="pageUpper"></span></div>' +
                '           <div class="rightArrow"></div>' +
                '       </div>' +
                '   </div>' +
                '   </div>' +
                '   <div class="propertyListView"></div>' +
                '   <div class="resultBarPaginationBottom">' +
                '       <div class="leftArrow"></div>' +
                '       <div class="paginateText">Page <span class="pageLower"></span> of <span class="pageUpper"></span></div>' +
                '       <div class="rightArrow"></div>' +
                '   </div>' +
                '</div></div></div>';
           return html;
    }
global override virtual String getHTML(){
       String html = '';

        html += writeControls();
        //html +='<script src="resource/1430197568000/ocms_PressRelease" type="text/javascript"></script>';       
       html +='<script src="http://www.vereit.com/resource/1431242396000/PressReleasesJS" type="text/javascript"></script>';
        //html +='<script src="http://www.arcpreit.com/resource/1430474644000/ocms_PressReleaseJS" type="text/javascript"></script>';
         //html +='<script src="{!URLFOR($Resource.arcpreitjs,"js/ocms_PressRelease.js")}"></script>';
         
       return html;
    }
  
  }