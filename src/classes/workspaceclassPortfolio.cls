Public with sharing class workspaceclassPortfolio{
public Portfolio__c myportfolio {get;set;}
private Apexpages.StandardController controller; 
public workspaceclassPortfolio(ApexPages.StandardController controller) {
this.controller = controller;
myportfolio = [select id from Portfolio__c where id =:apexpages.currentpage().getparameters().get('id')];
}
Public pagereference createworkspace()
{
myportfolio.Create_Workspace__c =true;
//myportfolio.workspace_Date__c =System.Now();

if (Schema.sObjectType.Portfolio__c.isUpdateable()) 
update myportfolio;
If(myportfolio.Create_Workspace__c=true)
{
ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Workspace is being created and indexed in Autonomy, please wait for an email confirmation');
apexpages.addmessage(myMsg);
}
return null;
}
}