@isTest
public class Test_ocms_WebCastlist
{
    public static testmethod void Test_OcmsHomePage() 
    { 
        
               
        profile pf=[select id,name from profile where name='Cole Capital Community'];
               
        account a=new account();
        a.name='test';
        insert a;
        
        Contact C= new Contact();
        C.firstName='Ninad';
        C.lastName='Tambe';
        c.accountid=a.id;
        c.email='test1234@noemail.com';
        insert C;
        
        Contact C11= new Contact();
        C11.firstName='Ninad1';
        C11.lastName='Tambe1';
        c11.accountid=a.id;
        c11.email='test1234@Testnoemail.com';
        insert C11;
        
        User u = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = pf.id, country='United States',IsActive =true,
                ContactId = c.Id,
                timezonesidkey='America/Los_Angeles', username='tester123@noemail.com');
       
        insert u;
        
        User u1 = new User(alias = 'test123', email='test123@testnoemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = pf.id, country='United States',IsActive =true,
                ContactId = c11.Id,
                timezonesidkey='America/Los_Angeles', username='tester678@noemail.com');
       
        insert u1;
        
        System.assertEquals(u1.profileid,pf.id);

        
        list<user> ulist=new list<user>();
        ulist.add(u);
        ulist.add(u1);
        
        Campaign cmp= new Campaign ();
        cmp.name='Test-Webcast';
        cmp.IsActive=True;
        cmp.Status='Completed';
        cmp.Type='Webcast';
        insert cmp;
        
              
       list<CampaignMember> clist=new list<CampaignMember>();
       for(integer i=0;i<=1;i++) {
        CampaignMember cp=new CampaignMember();
        cp.CampaignId=cmp.id;
        cp.contactid=ulist[i].contactid;
        cp.status='Registered';
        clist.add(cp);
        }
        insert clist;
        
         
       list<id> ab=new list<id>();
        for(CampaignMember cp11:clist) {
         ab.add(cp11.CampaignId);
        }
          
        datetime dt=system.now()-10;
        
        
         list<WebCast__c> wblist=new list<WebCast__c>();
       for(integer i=0;i<=1;i++) {
        WebCast__c wb= new WebCast__c();
        wb.name='Test';
        wb.Contacts__c= C.id;
        wb.Campaigns__c= ab[i];
        wb.Start_Date_Time__c=dt;
        wb.Description__c='Testing cole capital';
        wb.Status__c='Registered';
        wb.Email_Address__c='test@test.com';
        wblist.add(wb);
        }
         insert wblist;  
         
       system.runas(u)
        {  
        ocms_WebCastListmodified rc=new ocms_WebCastListmodified();
        try
        {
         map<string,string> p=new map<string,string>();
            p.put('action','getUpcomingWebcastCount');
            
        
           list<sobject> ProductList1=rc.getProductList1(); 
           
       
           list<sobject> ProductList=rc.getProductList(); 
            rc.sanitizeList(wblist,'name');
            rc.writeMapView();
            rc.executeRequest(p);
            rc.getUpcomingWebcastCount();
            rc.gettype();
            rc.writeControls();
            rc.getHtml();
            rc.writeListView();
         }
          catch(Exception e)  {        }       
          }
  }
  public static testmethod void Test_OcmsHomePage1() 
    { 
        
               
         profile pf=[select id,name from profile where name='Cole Capital Community'];
               
        account a=new account();
        a.name='test';
        insert a;
        
        Contact C= new Contact();
        C.firstName='sandeep';
        C.lastName='m';
        c.accountid=a.id;
        c.email='test123@noemail.com';
        insert C;
        
        Contact C11= new Contact();
        C11.firstName='Ninad1';
        C11.lastName='Tambe1';
        c11.accountid=a.id;
        c11.email='test123@testnoemail.com';
        insert C11;
        
        User u = new User(alias = 'test123', email='test1234@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = pf.id, country='United States',IsActive =true,
                ContactId = c.Id,
                timezonesidkey='America/Los_Angeles', username='tester321@noemail.com');
       
        insert u;
        
       System.assertEquals(u.profileid,pf.id);

        
        User u1 = new User(alias = 'test123', email='test1234@testnoemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = pf.id, country='United States',IsActive =true,
                ContactId = c11.Id,
                timezonesidkey='America/Los_Angeles', username='tester345@noemail.com');
       
        insert u1;
        
         System.assertEquals(u1.profileid,pf.id);

        
        list<user> ulist=new list<user>();
        ulist.add(u);
        ulist.add(u1);
        
        
      
        Campaign cmp1= new Campaign ();
        cmp1.name='Test-Webcast';
        cmp1.IsActive=True;
        cmp1.Status='In Progress';
        cmp1.Type='Webcast';
        insert cmp1;
        
       list<CampaignMember> clist=new list<CampaignMember>();
       for(integer i=0;i<=1;i++) {
        CampaignMember cp=new CampaignMember();
        cp.CampaignId=cmp1.id;
        cp.contactid=ulist[i].contactid;
        cp.status='Registered';
        clist.add(cp);
        }
        insert clist;
        
       list<id> ab=new list<id>();
        for(CampaignMember cp11:clist) {
         ab.add(cp11.CampaignId);
        }
        
       Datetime dt= Datetime.newInstance(2014, 10, 10);
       Datetime newDateTime = dt.addDays(200);
        
        
       list<WebCast__c> wblist=new list<WebCast__c>();
       for(integer i=0;i<=1;i++) {
        WebCast__c wb= new WebCast__c();
        wb.name='Test';
        wb.Contacts__c= C.id;
        wb.Campaigns__c= ab[0];
        wb.Start_Date_Time__c=newDateTime;
        wb.Description__c='Testing cole capital';
        wb.Status__c='Registered';
        wb.Email_Address__c='test@test.com';
         wblist.add(wb);
        }
         insert wblist;  
         update wblist;
    
         system.debug('@@@'+wblist);
       system.runas(u)
        {  
        ocms_WebCastListmodified rc=new ocms_WebCastListmodified();
        try
        {
         map<string,string> p=new map<string,string>();
            p.put('action','getUpcomingWebcastCount');
            
            list<sobject> ProductList=rc.getProductList(); 
            system.debug('***'+ProductList.size());
            system.debug('!!!'+ProductList);
            rc.sanitizeList(wblist,null);
            rc.writeMapView();
            rc.executeRequest(p);
            rc.getUpcomingWebcastCount();
            rc.gettype();
            rc.writeControls();
            rc.getHtml();
            rc.writeListView();
         }
          catch(Exception e)  {        }       
        }
  }
  
}