public class LC_CreateLeaseOppController {


   public boolean flag{get;set;}
   public List<SelectOption> CallListValues {set;get;}
   public String CallList {set;get;}
   public String myRecid{set;get;}
   public String LeaseOppid{set;get;}
   public String myprop{set;get;}
   public String leaseOrSuit{set;get;}
   public map<string, string>IdtoLeasevsuitmap{set;get;}
  

    public LC_CreateLeaseOppController()
    {
       CallListValues = new List<SelectOption>();
       CallListValues.add(new SelectOption('--None--','--None--'));
      
      
    }
    
    public void AdviserCallList()
    {
        CallListValues = new List<SelectOption>();
        system.debug('This is my proprty'+LeaseOpportunity.MRI_Property__c);
        string myprop=LeaseOpportunity.MRI_Property__c;
        list<Lease__c>Activeleaslist= new list<Lease__c>();
        list<Lease__c>Inactiveleaslist= new list<Lease__c>();
        list<Lease__c>Inactiveleaslist1= new list<Lease__c>();
        set<id>Vacantleaseid=new set<id>();
        list<Vacant_Lease__c>vacantleases=new list<Vacant_Lease__c>();
        CallListValues.add(new SelectOption('--None--','--None--'));
        set<string> Activesuiteids = new set<string>();
        set<string> InActivesuiteids = new set<string>();
        set<string> InActivesuiteids1 = new set<string>();
        String myvalue1;
        Activeleaslist=[select id, name ,Lease_ID__c,Lease_Status__c,MRI_PROPERTY__c,SuitVacancyIndicator__c ,Tenant_Name__c,SuiteId__c,Base_Rent_at_expiration__c from Lease__c where MRI_PROPERTY__c =:myprop and Lease_Status__c = 'Active' order by Generation__c desc];
        if(Activeleaslist!= null && Activeleaslist.size()>0){
            for(Lease__C leaseobj : Activeleaslist){
                Activesuiteids.add(leaseobj.SuiteId__c);
            }
        }
       if(Activeleaslist!= null && Activeleaslist.size()>0)
        {     
        list<string> suiteidnames = new list<string>();
        suiteidnames.addall(Activesuiteids);
        list<Lease__c> Activelist =[select id,name,Lease_Status__c,Lease_ID__c,Lease_Status_Time_Stamp__c,SuiteId__c from Lease__c where SuiteId__c in :Activesuiteids and MRI_PROPERTY__c=:myprop and Lease_Status__c = 'Active' order by Generation__c desc];
        for(integer i=0;i<Activesuiteids.size();i++){
        for(Lease__c  lc : Activelist)
                {   if(lc.SuiteId__c == suiteidnames[i]){
                       String myvalue=lc.Lease_ID__c+':'+lc.Lease_Status__c;
                       string firstvalue='L'+'-'+lc.id;
                       IdtoLeasevsuitmap.put(myvalue,firstvalue);
                       CallListValues.add(new SelectOption(myvalue,myvalue));
                       break;
                   }
               }     
        }
        }
       Inactiveleaslist=[select id, name ,Lease_ID__c,Lease_Status__c,MRI_PROPERTY__c,SuitVacancyIndicator__c ,Tenant_Name__c,SuiteId__c,Base_Rent_at_expiration__c from Lease__c where MRI_PROPERTY__c =:myprop and Lease_Status__c != 'Active' and SuitVacancyIndicator__c = TRUE order by Generation__c desc];
       system.debug('Inactiveleaslist123'+Inactiveleaslist);
       if(Inactiveleaslist!= null && Inactiveleaslist.size()>0){
            for(Lease__C leaseobj : Inactiveleaslist){
                InActivesuiteids.add(leaseobj.SuiteId__c);
            }
        }
        if(Inactiveleaslist!= null && Inactiveleaslist.size()>0)
        {
        list<string> inactivesuiteidnames1 = new list<string>();
        inactivesuiteidnames1.addall(InActivesuiteids );
        list<Lease__c> lclist = [select id,name,Lease_Status__c,Lease_ID__c,PropertyId_SuiteId_Generation__c,SuiteId__c from Lease__c where SuiteId__c in :InActivesuiteids and MRI_PROPERTY__c=:myprop and Lease_Status__c != 'Active' order by Generation__c desc NULLS LAST];
        for(integer i=0;i<InActivesuiteids .size();i++){
        for(Lease__c  lc1:lclist)
               
           {
               if(lc1.SuiteId__c == inactivesuiteidnames1 [i]){
               InActivesuiteids1.add(lc1.PropertyId_SuiteId_Generation__c);
               system.debug('InActivesuiteids1'+InActivesuiteids1);
               break;
               }

            }
        }
     }

      if(Inactiveleaslist!= null && Inactiveleaslist.size()>0)
        {   
        system.debug('entered');
        list<string> inactivesuiteidnames = new list<string>();
        inactivesuiteidnames.addall(InActivesuiteids1);
        list<Lease__c> leaseList = [select id,name,Lease_Status__c,Lease_ID__c,Lease_Status_Time_Stamp__c,PropertyId_SuiteId_Generation__c,SuiteId__c,Exec_Date__c from Lease__c where PropertyId_SuiteId_Generation__c in :InActivesuiteids1 and MRI_PROPERTY__c=:myprop and Lease_Status__c != 'Active' order by Exec_Date__c desc NULLS LAST];
        for(integer i=0;i<InActivesuiteids1 .size();i++){
        for(Lease__c  lc:leaseList)
               
               {   if(lc.PropertyId_SuiteId_Generation__c == inactivesuiteidnames [i]){
                       String myvalue=lc.Lease_ID__c+':'+lc.Lease_Status__c;
                       string firstvalue='L'+'-'+lc.id;
                       IdtoLeasevsuitmap.put(myvalue,firstvalue);
                       CallListValues.add(new SelectOption(myvalue,myvalue));
                       system.debug('myvalue123'+myvalue);
                       break;
                       
                   }
               }     
        }
        }       
        if((Inactiveleaslist.size()==0)&&(Activeleaslist.size()==0))
         {
              CallListValues.add(new SelectOption('--None--','--None--'));
        
         } 
      }

  // The Lease Opportunity record you are adding values to
   public LC_LeaseOpprtunity__c LeaseOpportunity
   {
    get {
      if (LeaseOpportunity == null)
        LeaseOpportunity = new LC_LeaseOpprtunity__c();
      return LeaseOpportunity ;
    }
    set;
  }

  public LC_CreateLeaseOppController(ApexPages.StandardController controller)
  { 
  CallListValues = new List<SelectOption>();
       CallListValues.add(new SelectOption('--None--','--None--'));
  
  flag = True;
  IdtoLeasevsuitmap = new map<string, String>();
    // blank constructor
  }
  
     public void hideSectionOnChange()
    {
        if(LeaseOpportunity.Lease_Opportunity_Type__c == 'Lease - New')
            flag = true;
        if(LeaseOpportunity.Lease_Opportunity_Type__c == 'Lease - Ammendment')   
            flag = true;
        if(LeaseOpportunity.Lease_Opportunity_Type__c == 'Lease - Renewal')   
            flag = true;   
        if(LeaseOpportunity.Lease_Opportunity_Type__c == 'Other')
            flag = false;
    }

  // save button is clicked
    public PageReference save() 
  {
     list<string> myLease=new list<String>();
     list<string>mylleasesuitid=new list<String>();
     string check1;
     list<string> check=new list<string>();
     String leaseName='';
     String tenantName='';
      String LeaseId='';
      Lease__c myLease1=new Lease__c ();
    if((calllist!='--None--'))
    {
         myLease=calllist.split(':');
         check1=IdtoLeasevsuitmap.get(calllist);
         system.debug('<<<This is calllist>>>>>>>>>'+calllist);
         system.debug('<<<This is Map>>>>>>>>>'+IdtoLeasevsuitmap);
         system.debug('<<<This is check1>>>>>>>>>'+check1); 
         check=check1.split('-');
         myRecid=check[1];
         leaseOrSuit=check[0];
         system.debug('<<<<<<<<<>>>>>>>>'+check1);
         system.debug('<<<<<<<<<>>>>>>>>'+myLease);
        if(myLease.size()>0)
        {
          //leaseName=myLease[0];
          LeaseId=myLease[0];
          leaseName=myLease[1];
          //tenantName=myLease[2];
         // name=myLease[3];
         // leasestatus=myLease[4];
        }
     }   
   else if((calllist=='--None--'))  
    {
     leaseOrSuit='None';
    
    }
     
     
    //create a lease Opportunity with particular record type based on Type picklist selection
        try 
        {
        
                if(LeaseOpportunity.Lease_Opportunity_Type__c=='--None--')
                           {
                           
                             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a Lease'));

                           
                           
                           }   
               if(LeaseOpportunity.Lease_Opportunity_Type__c=='Lease - New')
               {
                        RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'New_Lease_Opportunity']; 
                        string recid=NewRecType.id;
                            if(leaseOrSuit=='L')
                           {
                              myLease1=[select id,Lease_ID__c, name,Tenant_Name__c,SuiteId__c,Base_Rent_at_expiration__c  from Lease__c where id=:myRecid]; 
                              string myid= myLease1.id;         
                              LeaseOpportunity.recordtypeid=recid;
                              LeaseOpportunity.Lease__c=myid;
                              
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())
                              try{
                                insert LeaseOpportunity ;  
                              }catch(DMLException e){}
                            }
                            
                             if(leaseOrSuit=='None')
                           {      
                              LeaseOpportunity.recordtypeid=recid;
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())
                              try{
                                insert LeaseOpportunity ;  
                              }catch(DMLException e){}

                            } 
                            
                            LeaseOppid=LeaseOpportunity.id; 
                            PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
                            LseOptyPage .setRedirect(true);
                            return LseOptyPage ;
                }
                
                if(LeaseOpportunity.Lease_Opportunity_Type__c=='Lease - Amendment')
               {
                           
                           RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Amendment_LeaseOpportunity']; 
                            string recid=NewRecType.id;
              
                            if(leaseOrSuit=='L')
                           {         
                              LeaseOpportunity.recordtypeid=recid;
                              LeaseOpportunity.Lease__c=myRecid;
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())
                              try{
                              insert LeaseOpportunity ; 
                              }catch(DMLException e){}
                              LeaseOppid=LeaseOpportunity.id; 
                            PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
                            LseOptyPage .setRedirect(true);
                            return LseOptyPage ; 
                            }
                            if(leaseOrSuit=='None')
                           {
                             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a Lease'));
                           }
                }
                
                if(LeaseOpportunity.Lease_Opportunity_Type__c=='Lease - Renewal' || LeaseOpportunity.Lease_Opportunity_Type__c=='Contraction & Extension')
               {
                           RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Renewal_Lease_Opportunity']; 
                            string recid=NewRecType.id;  
                            if(leaseOrSuit=='L')
                           { 
                              LeaseOpportunity.recordtypeid=recid;
                              LeaseOpportunity.Lease__c=myRecid;
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())
                              try{
                                insert LeaseOpportunity ;  
                              }catch(DMLException e){}
                               LeaseOppid=LeaseOpportunity.id;                            
                            PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
                            LseOptyPage .setRedirect(true);
                            return LseOptyPage ;
                            }                         
                           
                            if(leaseOrSuit=='None')
                           {      
                              LeaseOpportunity.recordtypeid=recid;
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())
                              try{
                              insert LeaseOpportunity ;  
                              }catch(DMLException e){}
                            } 
                            
                            LeaseOppid=LeaseOpportunity.id; 
                            PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
                            LseOptyPage .setRedirect(true);
                            return LseOptyPage ;          
                       
                }
                  //88888888888888888888888888888 Master Lease Code8888888888888888888888888888888      
                  if(LeaseOpportunity.Lease_Opportunity_Type__c=='Master Lease - New')
               {
                           RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Master_Lease']; 
                            string recid=NewRecType.id;  
                            
                            if(leaseOrSuit=='L')
                           {        
                              LeaseOpportunity.recordtypeid=recid;
                              LeaseOpportunity.Lease__c=myRecid;
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())
                              try{
                                insert LeaseOpportunity ;  
                              }catch(DMLException e){} 
                               LeaseOppid=LeaseOpportunity.id;                             
                            PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
                            LseOptyPage.setRedirect(true);
                            return LseOptyPage ;
                            }
                          if(leaseOrSuit=='None')
                           {
                             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a Lease'));
                           }         
                }
                
                    if(LeaseOpportunity.Lease_Opportunity_Type__c=='Master Lease - Amendment')
               {
                           RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Master_Lease_Amendment']; 
                            string recid=NewRecType.id;  
                            
                            if(leaseOrSuit=='L')
                           {        
                              LeaseOpportunity.recordtypeid=recid;
                              LeaseOpportunity.Lease__c=myRecid;
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())

                              try{
                                insert LeaseOpportunity ;  
                              }catch(DMLException e){} 
                               LeaseOppid=LeaseOpportunity.id;                             
                            PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
                            LseOptyPage.setRedirect(true);
                            return LseOptyPage ;
                            }
                          if(leaseOrSuit=='None')
                           {
                             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a Lease'));
                           
                            }
                }
                       
                         if((LeaseOpportunity.Lease_Opportunity_Type__c=='Master Lease - Assignment')||(LeaseOpportunity.Lease_Opportunity_Type__c=='Master Lease - Rent Restructure & Extension')||(LeaseOpportunity.Lease_Opportunity_Type__c=='Master Lease - Sublease')||LeaseOpportunity.Lease_Opportunity_Type__c=='Master Lease - Renewal')
                      {
                           RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Master_Lease_Other']; 
                            string recid=NewRecType.id;  
                            
                            if(leaseOrSuit=='L')
                           {        
                              LeaseOpportunity.recordtypeid=recid;
                              LeaseOpportunity.Lease__c=myRecid;
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())

                              try{
                                insert LeaseOpportunity ;  
                              }catch(DMLException e){} 
                               LeaseOppid=LeaseOpportunity.id;                             
                            PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
                            LseOptyPage.setRedirect(true);
                            return LseOptyPage ;
                            }
                          if(leaseOrSuit=='None')
                           {
                             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a Lease'));
                           
                            }
                }
          //Added for lease opportunity Expansion creation*********************************
                if(LeaseOpportunity.Lease_Opportunity_Type__c=='Lease Expansion - Renewal')
               {
                           RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Expansion_Lease_Opportunity']; 
                            string recid=NewRecType.id;  
                            
                            if(leaseOrSuit=='L')
                           {        
                              LeaseOpportunity.recordtypeid=recid;
                              LeaseOpportunity.Lease__c=myRecid;
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())
                              try{
                                insert LeaseOpportunity ;  
                              }catch(DMLException e){}
                              LeaseOppid=LeaseOpportunity.id;                             
                            PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
                            LseOptyPage.setRedirect(true);
                            return LseOptyPage ;
                            }
                          if(leaseOrSuit=='None')
                           {      
                              LeaseOpportunity.recordtypeid=recid;
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())
                              try{
                                insert LeaseOpportunity ;  
                              }catch(DMLException e){}
                            } 
                            
                            LeaseOppid=LeaseOpportunity.id; 
                            PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
                            LseOptyPage .setRedirect(true);
                            return LseOptyPage ;
                }       
                 
          //   finished adding for lease opportunity Expansion*********************   
          //Added for lease opportunity Outparcel/Pad Sale creation*********************************
                if(LeaseOpportunity.Lease_Opportunity_Type__c=='Outparcel/Pad Sale or New')
               {
                           RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Lease_Outparcel_Pad_Sale']; 
                            string recid=NewRecType.id;  
                            
                            if(leaseOrSuit=='L')
                           {        
                              LeaseOpportunity.recordtypeid=recid;
                              LeaseOpportunity.Lease__c=myRecid;
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())

                              try{
                                insert LeaseOpportunity ;  
                              }catch(DMLException e){}
                               LeaseOppid=LeaseOpportunity.id;                             
                            PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
                            LseOptyPage.setRedirect(true);
                            return LseOptyPage ;
                            }
                          if(leaseOrSuit=='None')
                           {      
                              LeaseOpportunity.recordtypeid=recid;
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())

                              try{
                                insert LeaseOpportunity ;  
                              }catch(DMLException e){}
                            } 
                            
                            LeaseOppid=LeaseOpportunity.id; 
                            PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
                            LseOptyPage .setRedirect(true);
                            return LseOptyPage ;
                }       
                 
          //   finished adding Outparcel/Pad Sale creation*********************   
                
                
                
     //88888888888888888888888888888 Master Lease Code    8888888888888888888888888888888      
                     if((LeaseOpportunity.Lease_Opportunity_Type__c=='CAM Only Agreement - New')||(LeaseOpportunity.Lease_Opportunity_Type__c=='Contraction')||(LeaseOpportunity.Lease_Opportunity_Type__c=='Lease - Assignment')||(LeaseOpportunity.Lease_Opportunity_Type__c=='Other')||(LeaseOpportunity.Lease_Opportunity_Type__c=='Rent Restructure & Extension')||(LeaseOpportunity.Lease_Opportunity_Type__c=='Sublease')||(LeaseOpportunity.Lease_Opportunity_Type__c=='Other'))
                   {
                                                          
                            RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Other']; 
                       
                            string recid=NewRecType.id;
                            LeaseOpportunity.recordtypeid=recid;
                            if(leaseOrSuit=='L')
                           {        
                              LeaseOpportunity.recordtypeid=recid;
                              LeaseOpportunity.Lease__c=myRecid;
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())
                              try{
                                insert LeaseOpportunity ;  
                              }catch(DMLException e){}
                              LeaseOppid=LeaseOpportunity.id; 
                             PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
                             LseOptyPage .setRedirect(true);
                             return LseOptyPage ;
                            
                            }
                           
                             if(leaseOrSuit=='None')
                           {
                           
                             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a Lease'));

                           } 
   
                      } 
                    if(LeaseOpportunity.Lease_Opportunity_Type__c=='License/Vendor Agreement')
                   {
                                                          
                            RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'Other']; 
                       
                            string recid=NewRecType.id;
                            LeaseOpportunity.recordtypeid=recid;
                            if(leaseOrSuit=='L')
                           {        
                              LeaseOpportunity.recordtypeid=recid;
                              LeaseOpportunity.Lease__c=myRecid;
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())

                              try{
                                insert LeaseOpportunity ;  
                              }catch(DMLException e){}
                              LeaseOppid=LeaseOpportunity.id; 
                             PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
                             LseOptyPage .setRedirect(true);
                             return LseOptyPage ;
                            
                            }
                           
                            if(leaseOrSuit=='None')
                           {      
                              LeaseOpportunity.recordtypeid=recid;
                              if (Schema.sObjectType.LC_LeaseOpprtunity__c.isCreateable())

                              try{
                                insert LeaseOpportunity ;  
                              }catch(DMLException e){}
                            } 
                            
                            LeaseOppid=LeaseOpportunity.id; 
                            PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
                            LseOptyPage .setRedirect(true);
                            return LseOptyPage ;
   
                      } 

                
        }        
        catch (DMLException e)
        {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating new Lease Opportunity .'));
          return null;
        }
        return null;
  }

  
    public PageReference Cancel() 
  {
    string URl = system.label.Lopp_home;
      PageReference HomePage= new PageReference(URl );
    Return HomePage;
  }

}