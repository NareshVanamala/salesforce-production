public with sharing class NewHireFormProcess {

    public boolean MRIRequired{get;set;}
    public boolean NameRequired{get;set;}
    NewHireFormProcess__c c;
    Apexpages.StandardController controller;
    PageReference pageRef;
    public boolean showerror{set;get;}
    public NewHireFormProcess__c objCase=null;
        
     public NewHireFormProcess(ApexPages.StandardController Controller) {
    
        this.controller = controller;
        NewHireFormProcess__c c = (NewHireFormProcess__c)controller.getRecord();
        Id rtp=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormParent' and SobjectType = 'NewHireFormProcess__c'].id;
        objCase = c;
        c.recordtypeid = rtp;
        c.HR_ManagerApprovalStatus__c= 'Submitted For Approval';
        
    }
    public PageReference submitCase() {       
       
      if(objCase.MRI_Access_Requested__c==true && (objCase.If_MRI_Access_is_Needed_Indicate_Secur__c==null || objCase.If_MRI_Access_is_Needed_Indicate_Secur__c==''))
        {
            objCase.addError('Please enter Security Class');
            MRIRequired=true;
            return null;
        }
      if(objCase.First_Name__c==null || objCase.First_Name__c=='')
        {
            objCase.First_Name__c.addError('Please enter Name of the Employee');
            NameRequired=true;
            return null;
        }  
        controller.save();         
        pageRef = new PageReference('/apex/Submitedsuccess');
        pageRef.setRedirect(true);
        return pageRef; 
    }
    
    /*public List<SelectOption> getSecurityGroups()
    {
       List<SelectOption> options = new List<SelectOption>();
            
       Schema.DescribeFieldResult fieldResult = Case.Active_Directory_Group_1__c.getDescribe();
     
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
       for( Schema.PicklistEntry f : ple)
       {
          options.add(new SelectOption(f.getLabel(), f.getValue()));
       }       
       return options;
    }*/
 }