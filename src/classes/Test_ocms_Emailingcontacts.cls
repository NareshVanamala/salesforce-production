@isTest(SeeAllData=true)
public class Test_ocms_Emailingcontacts  
{
    
    @isTest static void testocms_Emailingcontacts1()
    {

   account a =new account();
   a.name='ARCP';
   insert a;
   
   Contact c1= new Contact();
   c1.FirstName='sandeep';
   c1.lastName='m';
   c1.accountid=a.id;
   c1.email='test@colecapital.com';
   c1.Phone='8473361717';
   c1.mailingcity='hyd';
   c1.mailingstate='TA';
   
   insert c1;

   
   System.currentPageReference().getParameters().put('contactid',c1.id);
   System.currentPageReference().getParameters().put('Template','Request a call');
   System.currentPageReference().getParameters().put('com','hi');
   System.currentPageReference().getParameters().put('IN','sandeep m');
    
   
   
   Contact_Us_Response__c ct=new Contact_Us_Response__c();
    ct.name=c1.firstname+' '+c1.lastname+' '+' Request';
    ct.First_Name__c=c1.firstname;
    ct.last_name__c=c1.lastname;
    ct.email__c=c1.email;
    ct.phone_number__c=c1.phone;
    ct.category__c='Request a call';
    ct.Internal_wholesaler_email__c=c1.email;
    ct.Internal_wholesaler__c=c1.firstname+' '+c1.lastname;
    ct.Last_Mailed_Date__c=system.today();
    
    insert ct;
    
    System.assertEquals(ct.last_name__c,c1.lastname);


    ocms_Emailingcontacts sc=new ocms_Emailingcontacts();
    sc.sendemail(); 
    
 }  
  
    @isTest static void testocms_Emailingcontacts2()
    {

   account a =new account();
   a.name='ARCP';
   insert a;
   
   Contact c1= new Contact();
   c1.FirstName='sandeep';
   c1.lastName='m';
   c1.accountid=a.id;
   c1.email='test@colecapital.com';
   c1.Phone='8473361717';
   c1.mailingcity='hyd';
   c1.mailingstate='TA';
   
   insert c1;

   
   System.currentPageReference().getParameters().put('contactid',c1.id);
   System.currentPageReference().getParameters().put('Template','Request a call');
   System.currentPageReference().getParameters().put('com',null);
   System.currentPageReference().getParameters().put('IN','sandeep m');
    
   
   
   Contact_Us_Response__c ct=new Contact_Us_Response__c();
    ct.name='hi';
    ct.First_Name__c=c1.firstname;
    ct.last_name__c=c1.lastname;
    ct.email__c=c1.email;
    ct.phone_number__c=c1.phone;
    ct.category__c='Request';
    ct.Internal_wholesaler_email__c=c1.email;
    ct.Internal_wholesaler__c=c1.firstname+' '+c1.lastname;
    ct.Last_Mailed_Date__c=system.today();
    
    insert ct;
    
    System.assertEquals(ct.last_name__c,c1.lastname);


    ocms_Emailingcontacts sc=new ocms_Emailingcontacts();
    sc.sendemail(); 
    
 }  
  @isTest static void testocms_Emailingcontacts3()
    {

   account a =new account();
   a.name='ARCP';
   insert a;
   
   Contact c1= new Contact();
   c1.FirstName='sandeep';
   c1.lastName='m';
   c1.accountid=a.id;
   c1.email='test@colecapital.com';
   c1.Phone='8473361717';
   c1.mailingcity='hyd';
   c1.mailingstate='TA';
   
   insert c1;

   
   System.currentPageReference().getParameters().put('contactid',c1.id);
   System.currentPageReference().getParameters().put('Template','Request a call');
   System.currentPageReference().getParameters().put('com','hi');
   System.currentPageReference().getParameters().put('IN',null);
    
   Map<String,Alternate_Email_in_absenece_of_IW__c> allCodes = Alternate_Email_in_absenece_of_IW__c.getAll();
   Alternate_Email_in_absenece_of_IW__c code = allCodes.values();

   
   Contact_Us_Response__c ct=new Contact_Us_Response__c();
    ct.name=c1.firstname+' '+c1.lastname+' '+' Request';
    ct.First_Name__c=c1.firstname;
    ct.last_name__c=c1.lastname;
    ct.email__c=c1.email;
    ct.phone_number__c=c1.phone;
    ct.category__c='Request a call';
    ct.Internal_wholesaler_email__c=c1.email;
    ct.Internal_wholesaler__c=c1.firstname+' '+c1.lastname;
    ct.Last_Mailed_Date__c=system.today();
    
    insert ct;

    System.assertEquals(ct.last_name__c,c1.lastname);
    ocms_Emailingcontacts sc=new ocms_Emailingcontacts();
    sc.sendemail(); 
    
 }  
 @isTest static void testocms_Emailingcontacts4()
    {

   account a =new account();
   a.name='ARCP';
   insert a;
   
   Contact c1= new Contact();
   c1.FirstName='sandeep';
   c1.lastName='m';
   c1.accountid=a.id;
   c1.email='test@colecapital.com';
   c1.Phone='8473361717';
   c1.mailingcity='hyd';
   c1.mailingstate='TA';
   
   insert c1;

   
   System.currentPageReference().getParameters().put('contactid',c1.id);
   System.currentPageReference().getParameters().put('Template','Request a call');
   System.currentPageReference().getParameters().put('com',null);
   System.currentPageReference().getParameters().put('IN',null);
    
  Map<String,Alternate_Email_in_absenece_of_IW__c> allCodes = Alternate_Email_in_absenece_of_IW__c.getAll();
   Alternate_Email_in_absenece_of_IW__c code = allCodes.values();

 
   
   Contact_Us_Response__c ct=new Contact_Us_Response__c();
    ct.name='hi';
    ct.First_Name__c=c1.firstname;
    ct.last_name__c=c1.lastname;
    ct.email__c=c1.email;
    ct.phone_number__c=c1.phone;
    ct.category__c='Request';
    ct.Internal_wholesaler_email__c=c1.email;
    ct.Internal_wholesaler__c=c1.firstname+' '+c1.lastname;
    ct.Last_Mailed_Date__c=system.today();
    
    insert ct;
    System.assertEquals(ct.last_name__c,c1.lastname);
    ocms_Emailingcontacts sc=new ocms_Emailingcontacts();
    sc.sendemail(); 
    
 }  
 @isTest static void testocms_Emailingcontacts5()
    {

   account a =new account();
   a.name='ARCP';
   insert a;
   
   Contact c1= new Contact();
   c1.FirstName='sandeep';
   c1.lastName='m';
   c1.accountid=a.id;
   c1.email='test@colecapital.com';
   c1.Phone='8473361717';
   c1.mailingcity='hyd';
   c1.mailingstate='TA';
   
   insert c1;

   
   System.currentPageReference().getParameters().put('contactid',c1.id);
   System.currentPageReference().getParameters().put('Template','Request a call');
   System.currentPageReference().getParameters().put('com','hii');
   System.currentPageReference().getParameters().put('IN','sandeep m');
    
   
   
   Contact_Us_Response__c ct=new Contact_Us_Response__c();
    ct.name='hi';
    ct.First_Name__c=c1.firstname;
    ct.last_name__c=c1.lastname;
    ct.email__c=c1.email;
    ct.phone_number__c=c1.phone;
    ct.category__c='Request';
    ct.Internal_wholesaler_email__c=c1.email;
    ct.Internal_wholesaler__c=c1.firstname+' '+c1.lastname;
    ct.Last_Mailed_Date__c=system.today();
    
    insert ct;
    System.assertEquals(ct.last_name__c,c1.lastname);
    ocms_Emailingcontacts sc=new ocms_Emailingcontacts();
    sc.sendemail(); 
    
 }  
  @isTest static void testocms_Emailingcontacts6()
    {

   account a =new account();
   a.name='ARCP';
   insert a;
   
   Contact c1= new Contact();
   c1.FirstName='sandeep';
   c1.lastName='m';
   c1.accountid=a.id;
   c1.email='test@colecapital.com';
   c1.Phone='8473361717';
   c1.mailingcity='hyd';
   c1.mailingstate='TA';
   
   insert c1;

   
   System.currentPageReference().getParameters().put('contactid',c1.id);
   System.currentPageReference().getParameters().put('Template','Request a call');
   System.currentPageReference().getParameters().put('com','hiii');
   System.currentPageReference().getParameters().put('IN',null);
    
   Map<String,Alternate_Email_in_absenece_of_IW__c> allCodes = Alternate_Email_in_absenece_of_IW__c.getAll();
   Alternate_Email_in_absenece_of_IW__c code = allCodes.values();
   
    Contact_Us_Response__c ct=new Contact_Us_Response__c();
    ct.name='hi';
    ct.First_Name__c=c1.firstname;
    ct.last_name__c=c1.lastname;
    ct.email__c=c1.email;
    ct.phone_number__c=c1.phone;
    ct.category__c='Request';
    ct.Internal_wholesaler_email__c=c1.email;
    ct.Internal_wholesaler__c=c1.firstname+' '+c1.lastname;
    ct.Last_Mailed_Date__c=system.today();
    insert ct;
    System.assertEquals(ct.last_name__c,c1.lastname);
    ocms_Emailingcontacts sc=new ocms_Emailingcontacts();
    sc.sendemail(); 
    
 }  
}