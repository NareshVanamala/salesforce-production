global with sharing class ReportCentralforcoleintrahome
{

    public ReportCentralforcoleintrahome() 
    {

    }

    public List<string> wList{get;set;}
    public static list<string> Combinedlist{get; set;}
    public static list<string> SiteNameList{get; set;}
    public static list<string> ReportviewURLlist{get; set;}
    public static list<string> reportlist{get; set;}
    public static list<string> WorkBooklist {get; set;}
  
    
    public string testreport{get;set;}
    public string nickname{get;set;}
    Public String FName{get;set;}
    public String u {get; set;}
    public boolean isPMaccessible {get;set;}
    public boolean isISRGaccessible {get;set;}    
    public boolean isTableauUser {get;set;}
    
    public static list<Calender_Event__c> ARCPCorporateEvent{get;set;}
    public static list<Calender_Event__c> ARCPCorporateBirthday{get;set;}
    public static list<Calender_Event__c> ARCPCorporateHoliday{get;set;}
    public Static list<String>Startdatetimelist{get;set;}
    public Static list<String>Enddatetimelist{get;set;}
    public Static list<String>Eventlist{get;set;} 
    public Static list<calEvent>events{get;set;}
    public Static list<calEvent>eventsBirthday{get;set;}
    public Static list<calEvent>eventsHoliday{get;set;}
    public Static String dtFormat = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'' ;
    public Static String dtFormat2 = 'yyyy-MM-dd';
    public ReportCentralforcoleintrahome(cms.CoreController controller) 
   {

     PopulateCorporateEvent();
     //populateBirthdays();
     birthdayData();
     populateHolidayEvent();
     User Usr1  = [SELECT FirstName FROM User WHERE Id = : UserInfo.getUserId()][0];
     FName = Usr1.FirstName;
     //populateReportCentralData();
 
    }
    
    
    public static void PopulateCorporateEvent()
    {
      ARCPCorporateEvent=new list<Calender_Event__c>();
      
      Map<String,Schema.RecordTypeInfo> recordTypesadjustment = Calender_Event__c.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id RecTypeIdArcp = recordTypesadjustment.get('VEREIT & Cole Corporate Events').getRecordTypeId();
      ARCPCorporateEvent=[select id,Start_Date_Time__c,End_Time__c,All_day_Event__c,Event_Agenga_Location__c,Intranet_End_Date__c,Event_Description__c,Event_Date__c,name,Corporate_Event_Type__c from Calender_Event__c where RecordTypeId = :RecTypeIdArcp order by Event_Date__c desc limit 1000];
      
     events = new list<calEvent>();
        for(Calender_Event__c evnt:ARCPCorporateEvent)
        {
        Date eventdt = evnt.Event_Date__c;
        DateTime startDT = evnt.Start_Date_Time__c;
        DateTime endDT = evnt.Intranet_End_Date__c;
        calEvent myEvent = new calEvent();
        myEvent.title = evnt.name;
        myEvent.allDay = evnt.All_day_Event__c;
        myEvent.eventDate = eventdt.format();
             
            if(eventdt.format() == date.today().format()) 
             {
              if(evnt.Event_Agenga_Location__c != NULL)
               myEvent.EventAgendaLocation = evnt.Event_Agenga_Location__c.trim();
             }
             
            if(startDT!= NULL)
            {
               myEvent.startString = startDT.format(dtFormat);
            }
          
            if(endDT != NULL)
            {
               myEvent.endString = endDT.format(dtFormat);
            }
          
      //    myEvent.url = '/' + evnt.Id;
            myEvent.url = evnt.Event_Description__c;
            myEvent.className = 'event-personal';
            myEvent.eventDescription = evnt.Event_Description__c;    
            myEvent.eventType   = evnt.Corporate_Event_Type__c;    
            
            events.add(myEvent);
            system.debug('My  events'+events);
        }    
    }
    
   /* public static void populateBirthdays()
    {
        ARCPCorporateBirthday=new list<Calender_Event__c>();
        Map<String,Schema.RecordTypeInfo> recordTypesadjustment = Calender_Event__c.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id RecTypeIdBrthday = recordTypesadjustment.get('Birthday & Anniversary Event').getRecordTypeId();
        ARCPCorporateBirthday=[select id, Start_Date_Time__c, End_Time__c, All_day_Event__c, Event_Description__c, Event_Date__c, name,Event_Type__c from Calender_Event__c where RecordTypeId = :RecTypeIdBrthday ];
      
         eventsBirthday = new list<calEvent>();
        for(Calender_Event__c evnt:ARCPCorporateBirthday)
        { 
            DateTime eventdt = evnt.Event_Date__c;
            DateTime startDT = evnt.Start_Date_Time__c;
            DateTime endDT = evnt.End_Time__c;
            calEvent myEvent = new calEvent();
            myEvent.title = evnt.name;
            myEvent.allDay = true;
             
            
            myEvent.eventDate = eventdt.format();
            
             if(startDT!= NULL)
            {
               myEvent.startString = startDT.format(dtFormat);
            }
          
            if(endDT != NULL)
            {
               myEvent.endString = endDT.format(dtFormat);
            }
    //      myEvent.url = '/' + evnt.Id;
            myEvent.url = evnt.Event_Description__c;
            myEvent.className = 'event-personal';
            myEvent.eventDescription = evnt.Event_Description__c;    
            myEvent.eventType   = evnt.Event_Type__c;    
            eventsBirthday.add(myEvent);
            system.debug('My  events'+ eventsBirthday);
               
        }
    }*/
    
    public static void populateHolidayEvent()
    {
     ARCPCorporateHoliday=new list<Calender_Event__c>();
     Map<String,Schema.RecordTypeInfo> recordTypesadjustment = Calender_Event__c.sObjectType.getDescribe().getRecordTypeInfosByName();
     Id RecTypeIdholi = recordTypesadjustment.get('Holiday Event').getRecordTypeId();
     ARCPCorporateHoliday=[select id, Start_Date_Time__c, End_Time__c, All_day_Event__c, Event_Description__c, Event_Date__c, name from Calender_Event__c where RecordTypeId = :RecTypeIdholi order by Event_Date__c desc limit 1000];
      
        eventsHoliday = new list<calEvent>();
        for(Calender_Event__c evnt:ARCPCorporateHoliday)
        {
            DateTime eventdt = evnt.Event_Date__c;
            DateTime startDT = evnt.Start_Date_Time__c;
            DateTime endDT = evnt.End_Time__c;
            calEvent myEvent = new calEvent();
            myEvent.title = evnt.name;
            myEvent.eventDate = eventdt.format();//.format(dtFormat2);
            myEvent.allDay = true;
             if(startDT != NULL)
            {
               myEvent.startString = startDT.format(dtFormat);
            }
          
            if(endDT != NULL)
            {
               myEvent.endString = endDT.format(dtFormat);
            }
            //myEvent.url = '/' + evnt.Id;
            myEvent.url = evnt.Event_Description__c;
            myEvent.className = 'event-personal';
            myEvent.eventDescription = evnt.Event_Description__c;
            eventsHoliday.add(myEvent);
            system.debug('My  events'+ eventsHoliday);
         }                   
    }

        public class calEvent{
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public String startString {get;set;}
        public String endString {get;set;}
        public String url {get;set;}
        public String className {get;set;}
        public String eventDescription {get;set;}
        public String EventAgendaLocation{get;set;}
        public String eventDate{get;set;}
        public String eventType{get;set;}
        public string backgroundColor {get;set;}
        public string textColor {get;set;}
        public calEvent()
            {
                
            }
    }


 @RemoteAction
    global static string GetCoraporateEventforToday()
    {
      ARCPCorporateEvent=new list<Calender_Event__c>();
      Map<String,Schema.RecordTypeInfo> recordTypesadjustment = Calender_Event__c.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id RecTypeIdArcp1 = recordTypesadjustment.get('VEREIT & Cole Corporate Events').getRecordTypeId();
    
      ARCPCorporateEvent=[select id,Start_Date_Time__c,End_Time__c,All_day_Event__c,Event_Agenga_Location__c,Event_Description__c,Event_Date__c,name from Calender_Event__c where RecordTypeId = :RecTypeIdArcp1 order by Event_Date__c desc limit 1000];
      
      list<calEvent> cevents = new list<calEvent>();
    
        for(Calender_Event__c evnt:ARCPCorporateEvent)
        {
            Date eventdt = evnt.Event_Date__c;
            Datetime yourDate = evnt.End_Time__c;
            Datetime startDate = evnt.Start_Date_Time__c;
            String dateOutput ='';
            String startdateOutput ='';
            String evntdateOutput ='';
            
            if(yourDate != NULL)
            {
              // Datetime yourDate = evnt.End_Time__c;
               dateOutput = yourDate.format('MM/dd/yyyy');
               //myDateTime =  DateTime.parse(evnt.End_Time__c);

            }
            if(startDate != NULL)
            {
               startdateOutput = startDate.format('MM/dd/yyyy');
             }
             if(eventdt != NULL)
            {   DateTime eventdteconvt = datetime.newInstance(eventdt.year(), eventdt.month(),eventdt.day());
               evntdateOutput = eventdteconvt.format('MM/dd/yyyy');
             }
           //system.debug('****Event Date*****'+eventdt.format());
           //system.debug('****Start Date*****'+startDate.format('MM/dd/yyyy'));
           //system.debug('****EndDate*****'+dateOutput);
            for(DateTime i=startDate;i<=yourDate;i = i.AddDays(1)){
                string evntstartdte = i.format('MM/dd/yyyy');
            if(evntdateOutput>=evntstartdte && evntdateOutput<=dateOutput){
                  system.debug('****Entering*****');
                  string todayDate = Datetime.now().format('MM/dd/yyyy');
            if((eventdt.format() == date.today().format()) ||( dateOutput == Datetime.now().format('MM/dd/yyyy')) || (todayDate>=evntstartdte && todayDate<=dateOutput))
             {  
               system.debug('****inner Entering*****');
              // system.debug('enddate'+dateInStringString);
             calEvent myEvent = new calEvent();
              if(evnt.Event_Agenga_Location__c != NULL)
              {
                  myEvent.EventAgendaLocation = evnt.Event_Agenga_Location__c.trim();
               }
           
             
       
             DateTime startDT = evnt.Start_Date_Time__c;
            DateTime endDT = evnt.End_Time__c;
            
            myEvent.title = evnt.name;
            myEvent.allDay = evnt.All_day_Event__c;
            myEvent.eventDate = eventdt.format();
             
            if(eventdt.format() == date.today().format()) 
             {
              if(evnt.Event_Agenga_Location__c != NULL)
               myEvent.EventAgendaLocation = evnt.Event_Agenga_Location__c.trim();
             }
             
            if(startDT!= NULL)
            {
               myEvent.startString = startDT.format(dtFormat);
            }
          
            if(endDT != NULL)
            {
               myEvent.endString = endDT.format(dtFormat);
            }
          
      //    myEvent.url = '/' + evnt.Id;
            myEvent.url = evnt.Event_Description__c;
            myEvent.className = 'event-personal';
            myEvent.eventDescription = evnt.Event_Description__c;       
            cevents.add(myEvent);
            }
            }
           break;
            }
           
        }    
         return JSON.serialize(cevents);
    }

  @RemoteAction
   global static String getCalendarEvent() 
   {
     
       // String dtFormat = 'EEE, d MMM yyyy HH:mm:ss z';
     
       ARCPCorporateEvent=[select id,Start_Date_Time__c,End_Time__c,All_day_Event__c,Event_Description__c,Event_Date__c,name from Calender_Event__c ];
       Eventlist=new list<String>();
        events = new list<calEvent>();
       Startdatetimelist=new list<String>();
       Enddatetimelist=new list<String>();
       integer i=1;
         string ARCPEvent='' ;
       for(Calender_Event__c evnt:ARCPCorporateEvent)
            {
            
                JSONGenerator gen = JSON.createGenerator(true); 
                gen.writeStartObject(); 
                String s1=string.valueof(i);
              
                DateTime startDT = evnt.Start_Date_Time__c;
                DateTime endDT = evnt.End_Time__c;
                calEvent myEvent = new calEvent();
                myEvent.title = evnt.name;
                myEvent.allDay = evnt.All_day_Event__c;
                gen.writeStringField('id',s1);
                gen.writeStringField('title',evnt.name); 
              
               
                if(startDT != NULL)
                {
                  myEvent.startString = startDT.format(dtFormat);
                  gen.writeStringField('start',startDT.format(dtFormat));
                }
                
                if(endDT != NULL)
                {
                  myEvent.endString = endDT.format(dtFormat);
                  gen.writeStringField('end',endDT.format(dtFormat));
                }
                gen.writeEndObject(); 
                myEvent.url = '/' + evnt.Id;
                myEvent.className = 'event-personal';
                events.add(myEvent);
                
              
              if(ARCPEvent!='')
                ARCPEvent =ARCPEvent+','+gen.getAsString();
              else
                 ARCPEvent =gen.getAsString();
                
                
                i++;
                system.debug('My  events'+events);
                
                 
             }      
           
             
         
       return ARCPEvent ;
   } 
  
   @RemoteAction
    global static string birthdayData(){
        calEvent[] birthdayevents = new calEvent[]{};
         Map<String,Schema.RecordTypeInfo> recordTypesadjustment = Calender_Event__c.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id RecTypeIdArcp1 = recordTypesadjustment.get('Birthday & Anniversary Event').getRecordTypeId();
    
      ARCPCorporateEvent=[select id,Event_Type__c,Start_Date_Time__c,End_Time__c,All_day_Event__c,Event_Agenga_Location__c,Event_Description__c,Event_Date__c,name from Calender_Event__c where RecordTypeId = :RecTypeIdArcp1];
      
        for(Calender_Event__c evnt: ARCPCorporateEvent){
            DateTime startDT = evnt.Start_Date_Time__c;
            DateTime endDT = evnt.End_Time__c;
            calEvent myEvent = new calEvent();
            myEvent.title = evnt.Name;
            myEvent.allDay = true;
            myEvent.startString = startDT.format(dtFormat);
            myEvent.endString = endDT.format(dtFormat);
            myEvent.url = evnt.Event_Description__c;
            myEvent.className = 'event-personal';
            myEvent.eventDescription = evnt.Event_Description__c;    
            myEvent.eventType   = evnt.Event_Type__c;  
            myEvent.backgroundColor = getcolor(evnt.Event_Type__c);
            myEvent.textColor = 'white!important';
            birthdayevents.add(myEvent);
        }

        string jsonEvents = JSON.serialize(birthdayevents);
        jsonEvents = jsonEvents.replace('startString','start');
        jsonEvents = jsonEvents.replace('endString','end');

        return jsonEvents;
    }


   global static string getcolor(string eventtype)
    {
       string color ='';
             if(eventtype == 'Birthday' || eventtype == 'REIT Industry')
             {
                color = '#f37021!important';
             }
             else if(eventtype == 'Anniversary' || eventtype=='VEREIT Corporate')
             {
                color = '#6e9934!important' ;

             }
             else if(eventtype == 'Cole Capital Corporate')
             {
                color = '#6d6e71!important' ;

             }
             else if(eventtype == 'Real Estate Event')
             {
                color = '#0088a4!important' ;

             }
             
             else
               {
                color = '#7d0c0e!important' ;
               }
             return color;
    }
}