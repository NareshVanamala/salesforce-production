public class LifeCycleHelper
{
 // method to send email
    Public static void sendEmail(ID cnt, ID tmpt)
    {
      contact c1 =[select id,name,email,Last_Contact_Date__c,Newproducer_onboarding_status__c,Last_Producer_Date__c,first_producer_date__c from contact where ID=:cnt];
 
       if(c1.email!=null)
       {       
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        //String[] fortest = new String[] {'ninad.tambe@colereit.com','skalamkar@colecapital.com','ravivarma.mekala@polarisft.com','kdharman@colecapital.com'};  
        String[] fortest = new String[] {c1.email};
        //String[] fortest1 = new String[] {'smariyala@arcpreit.com'};  
        email.setTargetObjectId(cnt);
        email.setTemplateId(tmpt);
        email.setSaveAsActivity(false);
        email.setToAddresses(fortest);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
       }
    }
 // method to insert Contact Call Lists Records
    public static void insertccl(ID cnct, ID usr, string clname)
    {
       Contact_Call_List__c ccllist = new Contact_Call_List__c();
              ccllist.contact__c = cnct ;
              ccllist.owner__c  = usr;
              ccllist.CallList_Name__c = clname;
              ccllist.CSV_Generated__c=true;
              insert ccllist;             
    }
  // Method to update status of New Producer  
    public static void newproducerstatus(string npo,ID cn)
    {
     contact co =[select id,name,Territory__c,Last_Contact_Date__c,Newproducer_onboarding_status__c,Last_Producer_Date__c,first_producer_date__c from contact where ID=:cn];
         co.Newproducer_onboarding_status__c = npo; 
         update co;
    }
}