global with sharing class Cockpit_GenerateCSV implements Database.Batchable<SObject>,Database.Stateful{
    global String Query;
    //global String automatedCallListName;  
    global Automated_Call_List__c acl;
    global String finalstr;
    string que ;
 global Cockpit_GenerateCSV(Id clId){  
   system.debug('Welcome to Batch apex method...');
   acl = [Select id, name from Automated_Call_List__c Where Id =: clId];
  // automatedCallListName = acl.Name;
 }  
 global Database.QueryLocator start(Database.BatchableContext BC)  {  
   que = 'Select Id, Contact__c,Contact__r.Name, Contact__r.Territory__c, Owner__c ,CallList_Name__c , Automated_Call_List__c from Contact_call_list__c where Automated_Call_List__c = ' ;
   Query = que + '\''+acl.Id +'\'' ;
   system.debug('Query' + Query);
   finalstr = 'CallListName , ContactIds , Territory , Name\n';
   return Database.getQueryLocator(query);  
 }
global void execute(Database.BatchableContext BC,List<Contact_call_list__c> scope){  
  List<contact_call_list__c> callist = new List<contact_call_list__c>();

  for(Contact_call_list__c c : scope) {
  string recordString = acl.Name +','+c.Contact__c+','+ c.contact__r.Territory__c +','+c.Contact__r.Name+'\n';
  finalstr = finalstr + recordString;

  }
 }
global void finish(Database.BatchableContext BC){ 
    System.debug('!!!!!!!finishfinal' + finalstr);
    AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
    Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
    System.debug('!!!!!!!a.CreatedBy.Email' + a.CreatedBy.Email);
    blob csvBlob = Blob.valueOf(finalstr);
    string csvname= acl.Name+'.csv';
    csvAttc.setFileName(csvname);
    csvAttc.setBody(csvBlob);
    Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
    String[] toAddresses = new String[] {a.CreatedBy.Email};
    String[] bccAddresses = new String[] {'nvanamala@colecapital.com'};
    String subject =acl.Name+' CSV';
    email.setSubject(subject);
    email.setBccSender(true);
    email.setToAddresses(toAddresses);
    email.setBccAddresses(bccAddresses);
    email.setPlainTextBody('Attached is the '+acl.Name+' CSV ');
    email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
    Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
 }
}