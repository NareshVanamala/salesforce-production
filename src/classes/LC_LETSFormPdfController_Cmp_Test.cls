@isTest
Public class LC_LETSFormPdfController_Cmp_Test
{
  static testmethod void testCreatelease()
  {
         test.starttest();
         map<string,string> leaseNametoDesriptionMap = new map<string,string>(); 
         
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.name='Test_MRI_Property';
         mc.Property_ID__c='A1234';
         insert mc; 
         system.debug('This is my MRI Property'+mc);
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU';
         mylease.Suite_Sqft__c=1234;
         mylease.Lease_ID__c='A046677';
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         system.debug('This is my Lease'+mylease);

         System.assertNotEquals(mc.name,myLease.name);
         
         LC_LeaseOpprtunity__c lc1= new LC_LeaseOpprtunity__c();
         lc1.name='TestOpportunity';
         lc1.Lease_Opportunity_Type__c='Lease - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.LC_OtherOpportunity_Description__c='Thisis my description';
         lc1.Base_Rent__c=12345;
         insert lc1;
         system.debug('This is my lease Opportunity'+lc1);
         
         
         list<LC_OptionSchedule__c >lclist=new list<LC_OptionSchedule__c >();
         LC_OptionSchedule__c ops = new LC_OptionSchedule__c();
         ops.Lease_Opportunity__c= lc1.id;
         ops.Months_Years__c='Months';
         ops.From_In_Months__c='1';
         ops.To_In_Months__c='3';
         ops.Amount__c=12345;
         lclist.add(ops) ;

         LC_OptionSchedule__c ops1 = new LC_OptionSchedule__c();
         ops1.Lease_Opportunity__c= lc1.id;
         ops1.Months_Years__c='Months';
         ops1.From_In_Months__c='7';
         ops1.To_In_Months__c='9';
         ops1.Amount__c=12345;
         lclist.add(ops1) ;
         
         insert lclist;
         
         list<LC_RentSchedule__c >myRentlist= new list<LC_RentSchedule__c >();
         LC_RentSchedule__c rs= new LC_RentSchedule__c();
         rs.Lease_Opportunity__c= lc1.id; 
         rs.Lease__c= mylease.id;   
         rs.Months_Years__c='Months';
         rs.From_In_Months__c='1';
         rs.To_In_Months__c='3';
         rs.Annual_PSF__c=12345;
         myRentlist.add(rs);
         insert myRentlist;
         
         LC_Clauses__c c1 = new LC_Clauses__c ();
         c1.Clause_Type__c = '125PEU';
         c1.Clause_Body__c = 'Permitted Use';
         insert c1;
         
         LC_Clauses__c c2 = new LC_Clauses__c ();
         c2.Clause_Type__c = '47TIA';
         c2.Clause_Body__c = 'TI Allowance';
         insert c2;
         LC_Clauses__c c3 = new LC_Clauses__c ();
         c3.Clause_Type__c = '305FIX';
         c3.Clause_Body__c = 'Fixturization Period';
         insert c3;
         
         LC_ClauseType__c ct2 = new LC_ClauseType__c ();
         ct2.Clauses__c=c2.id;
         ct2.Lease_Opportunity__c= lc1.id;
         ct2.name='47TIA';
         ct2.Clause_Description__c='TI Allowance';
         insert ct2;
         
         
         LC_ClauseType__c ct3 = new LC_ClauseType__c ();
         ct3.Clauses__c=c3.id;
         ct3.Lease_Opportunity__c= lc1.id;
         ct3.name='305FIX';
         ct3.Clause_Description__c='Fixturization Period';
         insert ct3;
 
          LC_LETSFormPdfController_Cmp LCcontroller = new  LC_LETSFormPdfController_Cmp();
          LCcontroller.myValue=lc1.id;
          LCcontroller.setquoteId1(lc1.id);
          string myval=LCcontroller.getquoteId1();
          LCcontroller.quoteId1=lc1.id;
          lc1= LCcontroller.LseOpty;
        
        
         LC_ClauseType__c ct1 = new LC_ClauseType__c ();
         ct1.Clauses__c=c1.id;
         ct1.Lease_Opportunity__c=lc1.id;
         ct1.name='125PEU';
         ct1.Clause_Description__c='Permitted Use';
         insert ct1;
        
        
        /* LC_Clauses__c c4 = new LC_Clauses__c ();
         c4.Clause_Type__c = '306NNN';
         c4.Clause_Body__c = 'NNNDescription';
         insert c4;*/
         LC_Clauses__c c5 = new LC_Clauses__c ();
         c5.Clause_Type__c = '108SECD';
         c5.Clause_Body__c = 'SecurityDepositSecurityDeposit';
         insert c5;
         LC_Clauses__c c6 = new LC_Clauses__c ();
         c6.Clause_Type__c = '307PPR';
         c6.Clause_Body__c = 'PrepaidRent ';
         insert c6;
         LC_Clauses__c c15 = new LC_Clauses__c ();
         c15.Clause_Type__c = '147TIA';
         c15.Clause_Body__c = 'TI Allowance';
         insert c15 ;
         LC_Clauses__c c16 = new LC_Clauses__c ();
         c16.Clause_Type__c = '148KCKOUT';
         c16.Clause_Body__c = 'Terminationright';
         insert c16;
          LC_Clauses__c c17 = new LC_Clauses__c ();
         c17.Clause_Type__c = '149COTEN';
         c17.Clause_Body__c = 'CoTenancy';
         insert c17;
        
        
        /* LC_ClauseType__c ct4 = new LC_ClauseType__c ();
         ct4.Clauses__c=c4.id;
         ct4.Lease_Opportunity__c=lc1.id;
         ct4.name='306NNN';
         ct4.Clause_Description__c='NNNDescription';
         insert ct4;*/
         LC_ClauseType__c ct5 = new LC_ClauseType__c ();
         ct5.Clauses__c=c5.id;
         ct5.Lease_Opportunity__c=lc1.id;
         ct5.name='108SECD';
         ct5.Clause_Description__c='SecurityDeposit';
         insert ct5;
         
         LC_ClauseType__c ct6 = new LC_ClauseType__c ();
         ct6.Clauses__c=c6.id;
         ct6.Lease_Opportunity__c=lc1.id;
         ct6.name='307PPR';
         ct6.Clause_Description__c='PrepaidRent';
         insert ct6;
         LC_ClauseType__c ct15= new LC_ClauseType__c ();
         ct15.Clauses__c=c15.id;
         ct15.Lease_Opportunity__c=lc1.id;
         ct15.name='147TIA';
         ct15.Clause_Description__c='TI Allowance';
         insert ct15;
          LC_ClauseType__c ct16= new LC_ClauseType__c ();
         ct16.Clauses__c=c16.id;
         ct16.Lease_Opportunity__c=lc1.id;
         ct16.name='148KCKOUT';
         ct16.Clause_Description__c='Terminationright';
         insert ct16;
         
          LC_ClauseType__c ct17= new LC_ClauseType__c ();
         ct17.Clauses__c=c17.id;
         ct17.Lease_Opportunity__c=lc1.id;
         ct17.name='149COTEN';
         //ct17.Clause_Description__c='CoTenancy';
         insert ct17;

            // LLwork*********************//
        //add remaning clauss.
       //insert clause  
         LC_Clauses__c c7 = new LC_Clauses__c ();
         c7.Clause_Type__c = '150LLW';
         c7.Clause_Body__c = 'LL Work';
         insert c7;
       //insert clause type  
         LC_ClauseType__c ct7 = new LC_ClauseType__c ();
         ct7.Clauses__c=c7.id;
         ct7.Lease_Opportunity__c=lc1.id;
         ct7.name='150LLW';
         ct7.Clause_Description__c='LL Work';
         insert ct7;
        //add it in controller 
          
     //*****************finished LLwork***************************   
         
        // LCcontroller.CoTenancy='CoTenancy';
         // string CoTenancydesc= LCcontroller.CoTenancy;
        // string mydescr=LCcontroller.permittedUse;
         // LCcontroller.TIAllowance='TI Allowance';
         // string BRKCommisionDeceription= LCcontroller.BrokerCommission;
          //system.assertEquals(false,a.isEmpty());
        // LCcontroller.BrokerCommission='BrokerCommision';
         // string BRDECr1= LCcontroller.BrokerCommission;
         // LCcontroller.FixturationPeriod=ct3.Clause_Description__c;
         // string FixturationPeriod= LCcontroller.FixturationPeriod;
       //   LCcontroller.NNNs=ct4.Clause_Description__c;
         // string NNNs1= LCcontroller.NNNs;
        //  LCcontroller.SecurityDeposit=ct5.Clause_Description__c;
         // string SecurityDeposit12= LCcontroller.SecurityDeposit;
         // LCcontroller.PrepaidRent =ct4.Clause_Description__c;
        //  string PrepaidRent123 = LCcontroller.PrepaidRent ;
       //  LCcontroller.TIAllowance =ct15.Clause_Description__c;
         // string TIAll = LCcontroller.TIAllowance ;
         // LCcontroller.LLWork=ct7.Clause_Description__c;
         // string LLWork123= LCcontroller.LLWork; 
         // LCcontroller.Terminationright=ct16.Clause_Description__c;
        //  string term= LCcontroller.Terminationright; 
          
          LCcontroller.LCoptionlist=lclist;
          list<LC_OptionSchedule__c>mylist= LCcontroller.LCoptionlist;
           LCcontroller.LCrentlist=myRentlist;
         list<LC_RentSchedule__c >rentlit56=LCcontroller.LCrentlist;
  
    //************************Exclusivity
            //add remaning clauss.
       //insert clause  
         LC_Clauses__c c8 = new LC_Clauses__c ();
         c8.Clause_Type__c = '112EXC';
         c8.Clause_Body__c = 'Exclusivity';
         insert c8;
       //insert clause type  
         LC_ClauseType__c ct8 = new LC_ClauseType__c ();
         ct8.Clauses__c=c8.id;
         ct8.Lease_Opportunity__c=lc1.id;
         ct8.name='112EXC';
         ct8.Clause_Description__c='Exclusivity';
         insert ct8;
        //add it in controller 
        //  LCcontroller.Exclusivity=ct8.Clause_Description__c;
        //  string Exclusivity1= LCcontroller.Exclusivity;  
         //*****************finished Exclusivity***************************      
         //************************Terminationright
            //add remaning clauss.
       //insert clause  
         LC_Clauses__c c9 = new LC_Clauses__c ();
         c9.Clause_Type__c = '137CON';
         c9.Clause_Body__c = 'Terminationright';
         insert c9;
       //insert clause type  
         LC_ClauseType__c ct9 = new LC_ClauseType__c ();
         ct9.Clauses__c=c9.id;
         ct9.Lease_Opportunity__c=lc1.id;
         ct9.name='137CON';
         ct9.Clause_Description__c='Terminationright';
         insert ct9;
        //add it in controller 
        //  LCcontroller.Terminationright=ct9.Clause_Description__c;
         // string Terminationrigh123t= LCcontroller.Terminationright;  
         //*****************finished Terminationright***************************     
         
         
         //************************ContinuousOps******************************
            //add remaning clauss.
       //insert clause  
         LC_Clauses__c c10 = new LC_Clauses__c ();
         c10.Clause_Type__c = '113CONT';
         c10.Clause_Body__c = 'ContinuousOps';
         insert c10;
       //insert clause type  
         LC_ClauseType__c ct10 = new LC_ClauseType__c ();
         ct10.Clauses__c=c10.id;
         ct10.Lease_Opportunity__c=lc1.id;
         ct10.name='113CONT';
         ct10.Clause_Description__c='ContinuousOps';
         insert ct10;
        //add it in controller 
         // LCcontroller.ContinuousOps=ct10.Clause_Description__c;
         // string ContinuousOps123= LCcontroller.ContinuousOps;  
         //*****************finished ContinuousOps***************************     
         
        //************************RadiusRestriction******************************
            //add remaning clauss.
       //insert clause  
         LC_Clauses__c c11 = new LC_Clauses__c ();
         c11.Clause_Type__c = '114RAD';
         c11.Clause_Body__c = 'RadiusRestriction';
         insert c11;
       //insert clause type  
         LC_ClauseType__c ct11 = new LC_ClauseType__c ();
         ct11.Clauses__c=c11.id;
         ct11.Lease_Opportunity__c=lc1.id;
         ct11.name='114RAD';
         ct11.Clause_Description__c='RadiusRestriction';
         insert ct11;
        //add it in controller 
        //  LCcontroller.RadiusRestriction=ct11.Clause_Description__c;
        //  string RadiusRestriction1= LCcontroller.RadiusRestriction;  
         //*****************finished RadiusRestriction***************************   
         
           
        //************************GoDarkRigh******************************
            //add remaning clauss.
       //insert clause  
         LC_Clauses__c c12 = new LC_Clauses__c ();
         c12.Clause_Type__c = '127DRK';
         c12.Clause_Body__c = 'RadiusRestriction';
         insert c12;
       //insert clause type  
         LC_ClauseType__c ct12 = new LC_ClauseType__c ();
         ct12.Clauses__c=c12.id;
         ct12.Lease_Opportunity__c=lc1.id;
         ct12.name='127DRK';
         ct12.Clause_Description__c='RadiusRestriction';
         insert ct12;
        //add it in controller 
        //  LCcontroller.GoDarkRigh=ct12.Clause_Description__c;
        //  string GoDarkRigh1= LCcontroller.GoDarkRigh;  
         //*****************finished GoDarkRigh***************************   
         
          //************************FRORORROFO******************************
            //add remaning clauss.
       //insert clause  
         LC_Clauses__c c13 = new LC_Clauses__c ();
         c13.Clause_Type__c = '123ROFO';
         c13.Clause_Body__c = 'RadiusRestriction';
         insert c13;
       //insert clause type  
         LC_ClauseType__c ct13 = new LC_ClauseType__c ();
         ct13.Clauses__c=c13.id;
         ct13.Lease_Opportunity__c=lc1.id;
         ct13.name='123ROFO';
         ct13.Clause_Description__c='RadiusRestriction';
         insert ct13;
        //add it in controller 
      //   LCcontroller.FRORORROFO=ct13.Clause_Description__c;
        //  string FRORORROFO1= LCcontroller.FRORORROFO;  
         //*****************finished FRORORROFO***************************  
         
          LC_Clauses__c c14 = new LC_Clauses__c ();
         c14.Clause_Type__c = '5678NK';
         c14.Clause_Body__c = 'NewClause';
         insert c14;
       //insert clause type  
       
       list<LC_ClauseType__c >otherclausetypelist=new list<LC_ClauseType__c>();
         LC_ClauseType__c ct14 = new LC_ClauseType__c ();
         ct14.Clauses__c=c14.id;
         ct14.Lease_Opportunity__c=lc1.id;
         ct14.name='5678NK';
         ct14.Clause_Description__c='RadiusRestriction';
         otherclausetypelist.add(ct14);
         insert otherclausetypelist;
       
         LCcontroller.cTypeList1=otherclausetypelist;
         list<LC_ClauseType__c >testlit=LCcontroller.cTypeList1;
         
        list<attachment>myattalist= new list<attachment>(); 
         Blob b = Blob.valueOf('Test Data');  
         Attachment attachment = new Attachment();  
    attachment.ParentId = lc1.id;  
    attachment.Name = 'Test Attachment for Parent';  
    attachment.Body = b;  
     myattalist.add(attachment );
     insert myattalist;
     
      LCcontroller.attList=myattalist;
      list<attachment>myattalist12=LCcontroller.attList;
      
        list<Note>notelist= new list<Note>(); 
          
         Note n = new Note();  
        n.ParentId = lc1.id;  
       n.Title= 'Test Attachment for Parent';  
        n.Body ='Note Body';  
       notelist.add(n );
       insert notelist;
       
     LCcontroller.noteList =notelist;
      list<Note>nlst1=LCcontroller.noteList ;
    test.stoptest();
 
    
  }  
  static testmethod void testCreatelease1()
  {
         test.starttest();
         map<string,string> leaseNametoDesriptionMap = new map<string,string>(); 
         
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
         system.debug('This is my MRI Property'+mc);
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU';
         mylease.Suite_Sqft__c=1234;
         mylease.Lease_ID__c='A046677';
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         system.debug('This is my Lease'+mylease);

         System.assertNotEquals(mc.name,myLease.name);

        
         LC_LeaseOpprtunity__c lc1= new LC_LeaseOpprtunity__c();
         lc1.name='TestOpportunity';
         lc1.Lease_Opportunity_Type__c='Lease - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.LC_OtherOpportunity_Description__c='Thisis my description';
         lc1.Base_Rent__c=12345;
         insert lc1;
         system.debug('This is my lease Opportunity'+lc1);
         
         
         list<LC_OptionSchedule__c >lclist=new list<LC_OptionSchedule__c >();
         LC_OptionSchedule__c ops = new LC_OptionSchedule__c();
         ops.Lease_Opportunity__c= lc1.id;
         ops.Months_Years__c='Months';
         ops.From_In_Months__c='1';
         ops.To_In_Months__c='3';
         ops.Amount__c=12345;
         lclist.add(ops) ;

         LC_OptionSchedule__c ops1 = new LC_OptionSchedule__c();
         ops1.Lease_Opportunity__c= lc1.id;
         ops1.Months_Years__c='Months';
         ops1.From_In_Months__c='7';
         ops1.To_In_Months__c='9';
         ops1.Amount__c=12345;
         lclist.add(ops1) ;
         
         insert lclist;
         
         list<LC_RentSchedule__c >myRentlist= new list<LC_RentSchedule__c >();
         LC_RentSchedule__c rs= new LC_RentSchedule__c();
         rs.Lease_Opportunity__c= lc1.id; 
         rs.Lease__c= mylease.id;   
         rs.Months_Years__c='Months';
         rs.From_In_Months__c='1';
         rs.To_In_Months__c='3';
         rs.Annual_PSF__c=12345;
         myRentlist.add(rs);
         insert myRentlist;
         
         LC_Clauses__c c1 = new LC_Clauses__c ();
         c1.Clause_Type__c = '125PEU';
         c1.Clause_Body__c = 'Permitted Use';
         insert c1;
         
         LC_Clauses__c c2 = new LC_Clauses__c ();
         c2.Clause_Type__c = '147TIA';
         c2.Clause_Body__c = 'BTI Allowance';
         insert c2;
         LC_Clauses__c c3 = new LC_Clauses__c ();
         c3.Clause_Type__c = '305FIX';
         c3.Clause_Body__c = 'Fixturization Period';
         insert c3;
         
         LC_ClauseType__c ct2 = new LC_ClauseType__c ();
         ct2.Clauses__c=c2.id;
         ct2.Lease_Opportunity__c= lc1.id;
         ct2.name='147TIA';
        ct2.Clause_Description__c='TI Allowance';
         insert ct2;
         
         
         LC_ClauseType__c ct3 = new LC_ClauseType__c ();
         ct3.Clauses__c=c3.id;
         ct3.Lease_Opportunity__c= lc1.id;
         ct3.name='305FIX';
        // ct3.Clause_Description__c='Fixturization Period';
         insert ct3;
 
          LC_LETSFormPdfController_Cmp LCcontroller = new  LC_LETSFormPdfController_Cmp();
          LCcontroller.myValue=lc1.id;
          LCcontroller.setquoteId1(lc1.id);
          string myval=LCcontroller.getquoteId1();
          LCcontroller.quoteId1=lc1.id;
          lc1= LCcontroller.LseOpty;
        
        
         LC_ClauseType__c ct1 = new LC_ClauseType__c ();
         ct1.Clauses__c=c1.id;
         ct1.Lease_Opportunity__c=lc1.id;
         ct1.name='125PEU';
        // ct1.Clause_Description__c='Permitted Use';
         insert ct1;
        
        
         /*LC_Clauses__c c4 = new LC_Clauses__c ();
         c4.Clause_Type__c = '306NNN';
         c4.Clause_Body__c = 'NNNDescription';
         insert c4;*/
         LC_Clauses__c c5 = new LC_Clauses__c ();
         c5.Clause_Type__c = '108SECD';
         c5.Clause_Body__c = 'SecurityDepositSecurityDeposit';
         insert c5;
         LC_Clauses__c c6 = new LC_Clauses__c ();
         c6.Clause_Type__c = '307PPR';
         c6.Clause_Body__c = 'PrepaidRent ';
         insert c6;
         LC_Clauses__c c15 = new LC_Clauses__c ();
         c15.Clause_Type__c = '147TIA';
         c15.Clause_Body__c = 'TI Allowance';
         insert c15 ;
         LC_Clauses__c c16 = new LC_Clauses__c ();
         c16.Clause_Type__c = '148KCKOUT';
         c16.Clause_Body__c = 'Terminationright';
         insert c16;
          LC_Clauses__c c17 = new LC_Clauses__c ();
         c17.Clause_Type__c = '149COTEN';
         c17.Clause_Body__c = 'CoTenancy';
         insert c17;
        
        
         /*LC_ClauseType__c ct4 = new LC_ClauseType__c ();
         ct4.Clauses__c=c4.id;
         ct4.Lease_Opportunity__c=lc1.id;
         ct4.name='306NNN';
        ct4.Clause_Description__c='NNNDescription';
         insert ct4;*/
         LC_ClauseType__c ct5 = new LC_ClauseType__c ();
         ct5.Clauses__c=c5.id;
         ct5.Lease_Opportunity__c=lc1.id;
         ct5.name='108SECD';
        // ct5.Clause_Description__c='SecurityDeposit';
         insert ct5;
         
         LC_ClauseType__c ct6 = new LC_ClauseType__c ();
         ct6.Clauses__c=c6.id;
         ct6.Lease_Opportunity__c=lc1.id;
         ct6.name='307PPR';
       //  ct6.Clause_Description__c='PrepaidRent';
         insert ct6;
         LC_ClauseType__c ct15= new LC_ClauseType__c ();
         ct15.Clauses__c=c15.id;
         ct15.Lease_Opportunity__c=lc1.id;
         ct15.name='147TIA';
        // ct15.Clause_Description__c='TI Allowance';
         insert ct15;
          LC_ClauseType__c ct16= new LC_ClauseType__c ();
         ct16.Clauses__c=c16.id;
         ct16.Lease_Opportunity__c=lc1.id;
         ct16.name='148KCKOUT';
        // ct16.Clause_Description__c='Terminationright';
         insert ct16;
         
          LC_ClauseType__c ct17= new LC_ClauseType__c ();
         ct17.Clauses__c=c17.id;
         ct17.Lease_Opportunity__c=lc1.id;
         ct17.name='149COTEN';
         //ct17.Clause_Description__c='CoTenancy';
         insert ct17;

            // LLwork*********************//
        //add remaning clauss.
       //insert clause  
         LC_Clauses__c c7 = new LC_Clauses__c ();
         c7.Clause_Type__c = '150LLW';
         c7.Clause_Body__c = 'LL Work';
         insert c7;
       //insert clause type  
         LC_ClauseType__c ct7 = new LC_ClauseType__c ();
         ct7.Clauses__c=c7.id;
         ct7.Lease_Opportunity__c=lc1.id;
         ct7.name='150LLW';
        // ct7.Clause_Description__c='LL Work';
         insert ct7;
        //add it in controller 
          
     //*****************finished LLwork***************************   
         
        // LCcontroller.CoTenancy='CoTenancy';
         // string CoTenancydesc= LCcontroller.CoTenancy;
        // string mydescr=LCcontroller.permittedUse;
         //LCcontroller.BrokerCommission='BrokerCommision';
        //  string BRKCommisionDeceription= LCcontroller.BrokerCommission;
          //system.assertEquals(false,a.isEmpty());
        // LCcontroller.BrokerCommission='BrokerCommision';
         // string BRDECr1= LCcontroller.BrokerCommission;
       //   LCcontroller.FixturationPeriod=ct3.Clause_Description__c;
        //  string FixturationPeriod= LCcontroller.FixturationPeriod;
          //LCcontroller.NNNs=ct4.Clause_Description__c;
         // string NNNs1= LCcontroller.NNNs;
       //   LCcontroller.SecurityDeposit=ct5.Clause_Description__c;
       //   string SecurityDeposit12= LCcontroller.SecurityDeposit;
         // LCcontroller.PrepaidRent =ct4.Clause_Description__c;
       //   string PrepaidRent123 = LCcontroller.PrepaidRent ;
        //  LCcontroller.TIAllowance =ct15.Clause_Description__c;
        //  string TIAll = LCcontroller.TIAllowance ;
        //  LCcontroller.LLWork=ct7.Clause_Description__c;
        //  string LLWork123= LCcontroller.LLWork; 
        //  LCcontroller.Terminationright=ct16.Clause_Description__c;
       //   string term= LCcontroller.Terminationright; 
          
          LCcontroller.LCoptionlist=lclist;
          list<LC_OptionSchedule__c>mylist= LCcontroller.LCoptionlist;
           LCcontroller.LCrentlist=myRentlist;
         list<LC_RentSchedule__c >rentlit56=LCcontroller.LCrentlist;
  
    //************************Exclusivity
            //add remaning clauss.
       //insert clause  
         LC_Clauses__c c8 = new LC_Clauses__c ();
         c8.Clause_Type__c = '112EXC';
         c8.Clause_Body__c = 'Exclusivity';
         insert c8;
       //insert clause type  
         LC_ClauseType__c ct8 = new LC_ClauseType__c ();
         ct8.Clauses__c=c8.id;
         ct8.Lease_Opportunity__c=lc1.id;
         ct8.name='112EXC';
       //  ct8.Clause_Description__c='Exclusivity';
         insert ct8;
        //add it in controller 
        //  LCcontroller.Exclusivity=ct8.Clause_Description__c;
        //  string Exclusivity1= LCcontroller.Exclusivity;  
         //*****************finished Exclusivity***************************      
         //************************Terminationright
            //add remaning clauss.
       //insert clause  
         LC_Clauses__c c9 = new LC_Clauses__c ();
         c9.Clause_Type__c = '137CON';
         c9.Clause_Body__c = 'Terminationright';
         insert c9;
       //insert clause type  
         LC_ClauseType__c ct9 = new LC_ClauseType__c ();
         ct9.Clauses__c=c9.id;
         ct9.Lease_Opportunity__c=lc1.id;
         ct9.name='137CON';
        // ct9.Clause_Description__c='Terminationright';
         insert ct9;
        //add it in controller 
         // LCcontroller.Terminationright=ct9.Clause_Description__c;
        //  string Terminationrigh123t= LCcontroller.Terminationright;  
         //*****************finished Terminationright***************************     
         
         
         //************************ContinuousOps******************************
            //add remaning clauss.
       //insert clause  
         LC_Clauses__c c10 = new LC_Clauses__c ();
         c10.Clause_Type__c = '113CONT';
         c10.Clause_Body__c = 'ContinuousOps';
         insert c10;
       //insert clause type  
         LC_ClauseType__c ct10 = new LC_ClauseType__c ();
         ct10.Clauses__c=c10.id;
         ct10.Lease_Opportunity__c=lc1.id;
         ct10.name='113CONT';
        // ct10.Clause_Description__c='ContinuousOps';
         insert ct10;
        //add it in controller 
       //   LCcontroller.ContinuousOps=ct10.Clause_Description__c;
       //   string ContinuousOps123= LCcontroller.ContinuousOps;  
         //*****************finished ContinuousOps***************************     
         
        //************************RadiusRestriction******************************
            //add remaning clauss.
       //insert clause  
         LC_Clauses__c c11 = new LC_Clauses__c ();
         c11.Clause_Type__c = '114RAD';
         c11.Clause_Body__c = 'RadiusRestriction';
         insert c11;
       //insert clause type  
         LC_ClauseType__c ct11 = new LC_ClauseType__c ();
         ct11.Clauses__c=c11.id;
         ct11.Lease_Opportunity__c=lc1.id;
         ct11.name='114RAD';
        // ct11.Clause_Description__c='RadiusRestriction';
         insert ct11;
        //add it in controller 
        //  LCcontroller.RadiusRestriction=ct11.Clause_Description__c;
        //  string RadiusRestriction1= LCcontroller.RadiusRestriction;  
         //*****************finished RadiusRestriction***************************   
         
           
        //************************GoDarkRigh******************************
            //add remaning clauss.
       //insert clause  
         LC_Clauses__c c12 = new LC_Clauses__c ();
         c12.Clause_Type__c = '127DRK';
         c12.Clause_Body__c = 'RadiusRestriction';
         insert c12;
       //insert clause type  
         LC_ClauseType__c ct12 = new LC_ClauseType__c ();
         ct12.Clauses__c=c12.id;
         ct12.Lease_Opportunity__c=lc1.id;
         ct12.name='127DRK';
        // ct12.Clause_Description__c='RadiusRestriction';
         insert ct12;
        //add it in controller 
          //LCcontroller.GoDarkRigh=ct12.Clause_Description__c;
         // string GoDarkRigh1= LCcontroller.GoDarkRigh;  
         //*****************finished GoDarkRigh***************************   
         
          //************************FRORORROFO******************************
            //add remaning clauss.
       //insert clause  
         LC_Clauses__c c13 = new LC_Clauses__c ();
         c13.Clause_Type__c = '123ROFO';
         c13.Clause_Body__c = 'RadiusRestriction';
         insert c13;
       //insert clause type  
         LC_ClauseType__c ct13 = new LC_ClauseType__c ();
         ct13.Clauses__c=c13.id;
         ct13.Lease_Opportunity__c=lc1.id;
         ct13.name='123ROFO';
       //  ct13.Clause_Description__c='RadiusRestriction';
         insert ct13;
        //add it in controller 
         // LCcontroller.FRORORROFO=ct13.Clause_Description__c;
        //  string FRORORROFO1= LCcontroller.FRORORROFO;  
         //*****************finished FRORORROFO***************************  
         
          LC_Clauses__c c14 = new LC_Clauses__c ();
         c14.Clause_Type__c = '5678NK';
         c14.Clause_Body__c = 'NewClause';
         insert c14;
       //insert clause type  
       
       list<LC_ClauseType__c >otherclausetypelist=new list<LC_ClauseType__c>();
         LC_ClauseType__c ct14 = new LC_ClauseType__c ();
         ct14.Clauses__c=c14.id;
         ct14.Lease_Opportunity__c=lc1.id;
         ct14.name='5678NK';
        // ct14.Clause_Description__c='RadiusRestriction';
         otherclausetypelist.add(ct14);
         insert otherclausetypelist;
       
         LCcontroller.cTypeList1=otherclausetypelist;
         list<LC_ClauseType__c >testlit=LCcontroller.cTypeList1;
         
        list<attachment>myattalist= new list<attachment>(); 
         Blob b = Blob.valueOf('Test Data');  
         Attachment attachment = new Attachment();  
    attachment.ParentId = lc1.id;  
    attachment.Name = 'Test Attachment for Parent';  
    attachment.Body = b;  
     myattalist.add(attachment );
     insert myattalist;
     
      LCcontroller.attList=myattalist;
      list<attachment>myattalist12=LCcontroller.attList;
      
        list<Note>notelist= new list<Note>(); 
          
         Note n = new Note();  
        n.ParentId = lc1.id;  
       n.Title= 'Test Attachment for Parent';  
        n.Body ='Note Body';  
       notelist.add(n );
       insert notelist;
       
     LCcontroller.noteList =notelist;
      list<Note>nlst1=LCcontroller.noteList ;
    test.stoptest();
 
  }  
}