Public with sharing class checkAllcolumnDemocontroller 
{
    Public List<wrapperclass> wrapList {get;set;}
    private ApexPages.StandardController Controller;
    private Portfolio__c port;
    public String strURL {get;set;}
    public String strURL1 {get;set;} 
    public String strURL6 {get;set;}
    public String redirectUrl{Public get;private set;}
    Public list<Deal__c> deallist {get;set;}
    public String ids{get;set;}
    public string url;
    
   // public wrapperclass wcls {get; set;}
      
    Public checkAllcolumnDemocontroller(ApexPages.StandardController controller) 
    {
        this.port= (Portfolio__c)controller.getRecord();
        redirectUrl='';
        deallist = [select id from Deal__c where Portfolio_Deal__c =: port.id];
        system.debug('My deal'+deallist[0].id);
        ids= deallist[0].id;
        
    }
    Public List<wrapperclass> getWrapperList()
    {
       wrapList = New List<wrapperclass>();
       Portfolio__c  p1 = [Select id FROM Portfolio__c where id = :port.id];
       for(Deal__c acc:[Select id, Name,AcquisitionDepartment__c,Deal_Status__c,Fund__c,Contract_Price__c,CAP_Rate__c,Total_SF__c,Closing_Date__c,Create_Workspace__c from Deal__c where Portfolio_Deal__c= :p1.id ORDER BY Create_Workspace__c,Name asc])
       {
           wrapList.add(New wrapperclass(acc,false));
       
       }
       return wrapList;
    }
    Public void getWrapperList(List<wrapperclass> w)
    {
       wrapList = w;
    }
    
    public PageReference doRedirect()
    {
    
       String ids='';
       for(wrapperclass wObj:wrapList)
       {
           System.debug('********* '+wObj);
           if(wObj.checkFlag)
           {
               if(ids.length()>0)
               {
                   ids +=','+wObj.accRec.id;
               }
               else{
                   ids +=wObj.accRec.id;
               }
           }
       }
       System.debug('********* '+ids);
       system.debug('Length of string'+ids.length());
       if(ids.length()>2048)
       {
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select less than 100 records')); 
       }
       if(ids.length()<1)
       {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select atleast one record')); 
       }
       If(ids.length()>1 && ids.length()<2048)
       {
       String url='/apex/MassUpdateActionPlan1?ids='+ids+'&object=Deal__c';
       redirectUrl =url;
       System.debug('********* '+redirectUrl);
       strURL = url;
       PageReference pref = new PageReference(url);
       pref.setRedirect(true);
       return pref ;
       }
    
       return null;
       
    }
    
    public PageReference doRedirect1()
    {
    
      String ids='';
       for(wrapperclass wObj:wrapList)
       {
           System.debug('********* '+wObj);
           if(wObj.checkFlag)
           {
               if(ids.length()>0)
               {
                   ids +=','+wObj.accRec.id;
               }
               else
               {
                   ids +=wObj.accRec.id;
               }
           }
       }
       System.debug('********* '+ids);
       system.debug('Length of string'+ids.length());
       if(ids.length()>2048)
       {
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select less than 100 records')); 
       }
       if(ids.length()<1)
       {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select atleast one record')); 
       }
       If(ids.length()>1 && ids.length()<2048)
       {
       String url='/apex/MassApplyActionPlan?ids='+ids+'&object=Deal__c';
       redirectUrl =url;
       System.debug('********* '+redirectUrl);
       strURL = url;
       PageReference pref = new PageReference(url);
       pref.setRedirect(true);
       return pref ;
       }
       return null;
    }
    public PageReference doRedirect3()
    {         
      system.debug('#####################'+ids);
      String url1='/apex/MassUpdateActionPlanForAllDeals?ids='+ids+'&object=Deal__c'+'&createtask=true';
      System.debug('********* '+url1);
      PageReference pref = new PageReference(url1);
      pref.setRedirect(true);
      return pref ;
    }
    public PageReference doRedirect4()
    {        
      system.debug('#####################'+ids);
      String url1='/apex/ActionPlanforAlldeal?objectId='+ids+'&object=Deal__c'+'&createtask=true';
      System.debug('********* '+url1);
      PageReference pref = new PageReference(url1);
      pref.setRedirect(true);
      return pref ;
    }
    
    public pagereference doRedirect5()
    { 
       String ids='';
       Boolean hasupdate = false;
       Boolean hasinsert = false;
       set<Id> idset = new set<ID>();
       for(wrapperclass wObj:wrapList)
       {
           System.debug('********* '+wObj);
           if(wObj.checkFlag)
           {
               if(ids.length()>0)
               {
                   ids +=','+wObj.accRec.id;                 
               }
               else
               {
                  ids +=wObj.accRec.id;
                   
               }
               
               idset.add(wObj.accrec.id);
            }
            
         }
                          
         
                   
       System.debug('********* '+ids);
       system.debug('Length of string'+ids.length());
       
       if(idset.isempty())
       {
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select atleast one record')); 
       }
       if(!idset.isEmpty())
            {   
                           
                   url='/apex/MassManager__MassUpdate?selectedRecords='+ids+'&retURL=%2F'+port.id+'&selectedObject=Deal__c';
                  
                   redirectUrl =url;
                   System.debug('********* '+redirectUrl);
                   strURL = url;
                   PageReference pref = new PageReference(url);
                   pref.setRedirect(true);
                   return pref ;             
           } 
    
       return null;
       
   }
       
   
    public void doRedirect6()
    {   string id ='';
        
        boolean hasinsert = false;
        boolean hasupdate = false;
        set<Id> idset = new set<ID>();
        for(wrapperclass wc : wrapList)
        {
          if(wc.checkFlag)
          {
          
            if(ids.length()>0)
               {
                   ids +=','+wc.accRec.id;
               }                
               else
               {
                   ids +=wc.accRec.id;                  
               }
                        
              if(wc.accRec.Create_Workspace__c)
              {
                  hasupdate = true;
              }
              else
              {
                  hasinsert = true;
              }
              idset.add(wc.accrec.id);
          }                   
        }
        
        if(!idset.isEmpty())
        {     
            if(hasinsert && !hasupdate)
                       
            {                             
                List<Deal__c> deallst = new List<Deal__c>();
                
                deallst = [select id,Create_Workspace__c from Deal__c where id IN: idset];
                           
                for(deal__c dc1 : deallst)
                {          
                    dc1.Create_Workspace__c = true;
                              
                }
               
                Savepoint sp1 = Database.setSavepoint();//Save point for rollback if DML fails
                try
                {                 
                    update deallst;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Workspace create request is sent to Autonomy, Please wait for email confirmation')); 
                                      
                }
                catch(Exception ex)
                {          
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Update failed :'+ex));
                   
                   Database.rollback(sp1);  
                }
                                 

               pagereference p = apexpages.Currentpage();
               p.setredirect(true);
                             
           } 
           
            else if((!hasinsert && hasupdate)||(hasinsert && hasupdate))
                    
            {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Please select deals without workspace'));
            }     
       }              
        else
        {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Please select atleast one record'));
        }         
    }
        
         Public  pagereference backMethod()
            {   
                
                Pagereference pg =  new Pagereference(system.Label.Domain_Name+port.id); 
                pg.setRedirect(true);
                return pg;
            }
    public PageReference relateToLoan()
    {
    
      String ids='';
       for(wrapperclass wObj:wrapList)
       {
           System.debug('********* '+wObj);
           if(wObj.checkFlag)
           {
               if(ids.length()>0)
               {
                   ids +=','+wObj.accRec.id;
               }
               else
               {
                   ids +=wObj.accRec.id;
               }
           }
       }
       System.debug('********* '+ids);
       system.debug('Length of string'+ids.length());
      /*if(ids.length()>2048)
       {
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select less than 100 records')); 
       } */
       if(ids.length()<1)
       {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select atleast one record')); 
       }
       //If(ids.length()>1 && ids.length()<2048)
       If(ids.length()>1)
       {
       String url='/apex/massApplyLoanPage?selectedRecords='+ids+'&selectedObject=Deal__c';
       redirectUrl =url;
       System.debug('********* '+redirectUrl);
       strURL = url;
       PageReference pref = new PageReference(url);
       pref.setRedirect(true);
       return pref ;
       }
       return null;
    }
            
        public class wrapperclass
        
        {
            Public Deal__c accRec{get;set;}
            Public boolean checkFlag{get;set;}
            
            Public wrapperclass(Deal__c acc,boolean flag)
            {
                accRec = new Deal__c();
                accRec = acc;
                checkFlag = flag;
            }
        }   
 }