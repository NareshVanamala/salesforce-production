global with sharing class FGSPushToWebservice {
    webService static String sendDoc(String cID, String kId) {
        try {
            FGSPushToWebservice cd = new FGSPushToWebservice();
    
            // Creating the Order Document
            String inputXML = cd.createDoc(cId, kId);
            try {
                // Create FGS Webservice - Not being used due to FGS SOAP Based Webservice not using CDATA elements.
                //tempuriOrg.fgshotlinkSoap ws = new tempuriOrg.fgshotlinkSoap();
                //String responseXML = ws.sendOrderData(inputXML);
                                
                // HTTP Post Method
                Http h = new Http();
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint('http://FGSHotlink.fgsfulfillment.com/fgshotlink.asmx/sendOrderData?inputXML='+inputXML);
                    req.setMethod('GET');
                    HttpResponse res = h.send(req);
                    System.debug(res.getStatus());
                    System.debug(res.getBody());
                
                // Recording the Task if WS Call Successful, otherwise deleting first Kit Request
                if (res.getBody().contains('http://cole.fgsfulfillment.com/badinquiry.aspx')){
                    Kit_Request__c kitToDelete = new Kit_Request__C(id = kId);
                    delete kitToDelete;
                    return inputXML;
                } else {
                    Task t = new Task();
                    t.Description = 'FGS Order Submitted'; // This will need the FGS Order Number added.
                    t.WhoId = cId;
                    t.Type = 'FGS Request';
                    t.Status = 'Completed';
                    t.Subject = 'Kit Sent';
                    t.ActivityDate = date.today();
                    insert t;
                    Kit_Request__c kitToUpdate = new Kit_Request__C(id = kId);
                    kitToUpdate.Debug_FGS_Message__c = inputXML;
                    update kitToUpdate;
                }
                
                // Create and Return URL from FGS Webservice
                String returnString = res.getBody();
                returnString = returnString.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                returnString = returnString.replace('<string xmlns="http://www.ASCSoftware.com/">', '');
                returnString = returnString.replace('</string>', '');
                return returnString;
            } catch (Exception e) {
                return e.getMessage();
            }
            return 'Problem.';
        } catch (Exception e) {
            return e.getMessage();
        }
    }
    public String createDoc(Id cID, Id oId) {
        try {
            Contact contactRecord = [SELECT ID
                                            , FullName__c
                                            , Email
                                            , mailingStreet
                                            , mailingCity
                                            , mailingState
                                            , mailingPostalCode
                                            , Company_Name__c
                                            , Account.Name
                                    FROM Contact
                                    WHERE ID = :cID LIMIT 1];
                                    
            User currentUser = [SELECT ID
                                , Name
                                , PostalCode
                                , Email
                                FROM User
                                WHERE ID = :Userinfo.getUserId()];
            
            
            // URL Encoding All Values.......*sigh*
            Datetime oDate = datetime.now();
            String dt = oDate.format('MM/dd/yyyy');
            String userIdEncoded = EncodingUtil.urlEncode(currentUser.Id,'UTF-8');
            String userNameEncoded = EncodingUtil.urlEncode(currentUser.Name,'UTF-8');
            String cRecordIdEncoded = EncodingUtil.urlEncode(contactRecord.id,'UTF-8');
            String orderIdEncoded = EncodingUtil.urlEncode(oId,'UTF-8');
            String cRecordCompanyEncoded = '';
            String cRecordAccountEncoded = '';
            if (contactRecord.Company_Name__c != null){
                cRecordCompanyEncoded = EncodingUtil.urlEncode(contactRecord.Company_Name__c,'UTF-8');
            } else if (contactRecord.Account.Name != null) {
                cRecordCompanyEncoded = EncodingUtil.urlEncode(contactRecord.Account.Name,'UTF-8');
            } else {
                cRecordCompanyEncoded = 'Not%20Specified';
            }
            String attentionOfEncoded = '';
            if (contactRecord.FullName__c != null){
                attentionOfEncoded = EncodingUtil.urlEncode(contactRecord.FullName__c,'UTF-8');
            } else {
                attentionOfEncoded = 'Not%20Specified';
            }
            if(contactRecord.Account.Name != null)            
            {                
                cRecordAccountEncoded = EncodingUtil.urlEncode(contactRecord.Account.Name,'UTF-8');            
            }
            String cRecordMailingStreetEncoded = '';
            if (contactRecord.MailingStreet != null){
                cRecordMailingStreetEncoded = EncodingUtil.urlEncode(contactRecord.MailingStreet,'UTF-8');
            } else {
                cRecordMailingStreetEncoded = 'Not%20Specified';
            }
            String cRecordMailingCityEncoded = '';
            if (contactRecord.MailingCity != null){
                cRecordMailingCityEncoded = EncodingUtil.urlEncode(contactRecord.MailingCity,'UTF-8');
            } else {
                cRecordMailingCityEncoded = 'Not%20Specified';
            }
            String cRecordMailingStateEncoded = '';
            if (contactRecord.MailingState != null){
                cRecordMailingStateEncoded = EncodingUtil.urlEncode(contactRecord.MailingState,'UTF-8');
            } else {
                cRecordMailingStateEncoded = 'Not%20Specified';
            }
            String cRecordPostalCodeEncoded = '';
            if (contactRecord.mailingPostalCode != null){
                cRecordPostalCodeEncoded = EncodingUtil.urlEncode(contactRecord.mailingPostalCode,'UTF-8');
            } else {
                cRecordPostalCodeEncoded = 'Not%20Specified';
            }
            String cRecordEmailEncoded = '';
            if (contactRecord.email != null){
                cRecordEmailEncoded = EncodingUtil.urlEncode(contactRecord.email,'UTF-8');
            } else {
                cRecordEmailEncoded = 'Not%20Specified';
            }
            String orgNameEncoded = EncodingUtil.urlEncode(Userinfo.getOrganizationName(),'UTF-8');
            String postCodeEncoded = '';
            if (currentUser.postalCode != null){
                postCodeEncoded = EncodingUtil.urlEncode(currentUser.postalCode,'UTF-8');
            } else {
                postCodeEncoded = 'Not%20Specified';
            }
            String emailEncoded = EncodingUtil.urlEncode(currentUser.Email,'UTF-8');
            
            String xmlDoc; // Creating the main XML Doc Container.
            xmlDoc = '<?xml%20version="1.0"%20encoding="utf-8"?>';
            xmlDoc = xmlDoc + '<NewOrder>';
                xmlDoc = xmlDoc + '<User>';
                    xmlDoc = xmlDoc + '<UserId>'+emailEncoded+'</UserId>';
                    //xmlDoc = xmlDoc + '<UserId>ASC15</UserId>';
                    xmlDoc = xmlDoc + '<ClientUserID>'+userIdEncoded+'</ClientUserID>';
                    xmlDoc = xmlDoc + '<Company>COLE</Company>';  // <<
                    xmlDoc = xmlDoc + '<Name>'+userNameEncoded+'</Name>';
                    xmlDoc = xmlDoc + '<Email>'+emailEncoded+'</Email>';
                xmlDoc = xmlDoc + '</User>';
                xmlDoc = xmlDoc + '<Order>';
                    xmlDoc = xmlDoc + '<RequestNumber%20/>';
                    xmlDoc = xmlDoc + '<CustomerOrderDate>'+dt+'</CustomerOrderDate>';
                    xmlDoc = xmlDoc + '<ClientOrderID>'+orderIdEncoded+'</ClientOrderID>';  // <<  Put my order ID here.
                    xmlDoc = xmlDoc + '<ShipComplete%20/>';
                    xmlDoc = xmlDoc + '<CostCenter%20/>';
                    xmlDoc = xmlDoc + '<FreightBillCode%20/>';
                    xmlDoc = xmlDoc + '<FreightBillAccount%20/>';
                    xmlDoc = xmlDoc + '<PartyID%20/>';  // <<
                    xmlDoc = xmlDoc + '<Producer%20/>';  // <<
                    xmlDoc = xmlDoc + '<DepartmentId%20/>';
                    xmlDoc = xmlDoc + '<IREP%20/>';  // <<
                    xmlDoc = xmlDoc + '<SendEmail%20/>';  // <<
                    xmlDoc = xmlDoc + '<OrderType%20/>';
                    xmlDoc = xmlDoc + '<Recipient>';
                        xmlDoc = xmlDoc + '<ClientRecipientID>'+cRecordIdEncoded+'</ClientRecipientID>';  // <<
                        xmlDoc = xmlDoc + '<ShipToCompany>'+cRecordCompanyEncoded+'</ShipToCompany>';  // <<
                        xmlDoc = xmlDoc + '<FullFirmName>'+cRecordAccountEncoded+'</FullFirmName>';  // <<       
                        xmlDoc = xmlDoc + '<AttentionOf>'+attentionOfEncoded+'</AttentionOf>';  // <<
                        xmlDoc = xmlDoc + '<ShippingAddress>';
                            xmlDoc = xmlDoc + '<Address1>'+cRecordMailingStreetEncoded+'</Address1>';  // <<
                            xmlDoc = xmlDoc + '<Address2%20/>';
                            xmlDoc = xmlDoc + '<Address3%20/>';
                            xmlDoc = xmlDoc + '<Address4%20/>';
                            xmlDoc = xmlDoc + '<City>'+cRecordMailingCityEncoded+'</City>';  // <<
                            xmlDoc = xmlDoc + '<State>'+cRecordMailingStateEncoded+'</State>';  // <<
                            xmlDoc = xmlDoc + '<ZipCode>'+cRecordPostalCodeEncoded+'</ZipCode>';  // <<
                            xmlDoc = xmlDoc + '<ShipToEmail>'+cRecordEmailEncoded+'</ShipToEmail>';
                            xmlDoc = xmlDoc + '<ShipToNotes%20/>';
                        xmlDoc = xmlDoc + '</ShippingAddress>';
                    xmlDoc = xmlDoc + '</Recipient>';
                    xmlDoc = xmlDoc + '<FinancialProfessional>';
                        xmlDoc = xmlDoc + '<Name>'+userNameEncoded+'</Name>';  // <<
                        xmlDoc = xmlDoc + '<ClientFPRecipientID%20/>';  // <<
                        xmlDoc = xmlDoc + '<Territory%20/>';  // <<
                        xmlDoc = xmlDoc + '<Series7%20/>';  // <<
                        xmlDoc = xmlDoc + '<Company>'+orgNameEncoded+'</Company>';  // <<
                        xmlDoc = xmlDoc + '<ZipCode>'+postCodeEncoded+'</ZipCode>';  // <<
                        xmlDoc = xmlDoc + '<FPEmailAddress>'+emailEncoded+'</FPEmailAddress>';  // <<
                        xmlDoc = xmlDoc + '<Type%20/>';  // <<
                    xmlDoc = xmlDoc + '</FinancialProfessional>';
                xmlDoc = xmlDoc + '</Order>';
            xmlDoc = xmlDoc + '</NewOrder>';
            
            //  The following is a 'known good' xml we can comment out in order to test FGS with a record they provided to us.  - TAL
            //xmldoc = '<?xml%20version="1.0"%20encoding="utf-8"?><NewOrder><User><UserId>ASC15</UserId><ClientUserID>191362</ClientUserID><Company>COLE</Company><Name>Rebecca%20Caudill</Name><Email>RCaudill@ascsoftware.com</Email></User><Order><RequestNumber>12345</RequestNumber><CustomerOrderDate>04/27/2009</CustomerOrderDate><ClientOrderID>e0683434-1As3-db11-998f-000d566f3979</ClientOrderID><ShipComplete>T</ShipComplete><CostCenter>000%20-%20General</CostCenter><FreightBillCode>04</FreightBillCode><FreightBillAccount>3R7-0W7</FreightBillAccount><PartyID%20/><Producer%20/><DepartmentId%20/><IREP%20/><SendEmail%20/><OrderType%20/><Recipient><ClientRecipientID>191354</ClientRecipientID><ShipToCompany>ASC%20Software</ShipToCompany><AttentionOf>Rebecca%20Caudill</AttentionOf><ShippingAddress><Address1>4074%20E%20Patterson%20Rd</Address1><Address2%20/><Address3%20/><Address4%20/><City>Dayton</City><State>OH</State><ZipCode>45430</ZipCode><ShipToEmail>RCaudill@ascsoftware.com</ShipToEmail><ShipToNotes>These%20are%20shipping%20notes,%20which%20are%20limited%20to%20160%20characters.</ShipToNotes></ShippingAddress></Recipient><FinancialProfessional><Name>Joe%20Schmoely</Name><ClientFPRecipientID%20/><Territory>WEA160</Territory><Series7%20/><Company>Smith%20Barney</Company><ZipCode>07701</ZipCode><FPEmailAddress%20/><Type>UIT</Type></FinancialProfessional></Order></NewOrder>';
            system.debug('XML Document: --------------- '+xmlDoc);
            return xmlDoc;
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}