@isTest
public class Test_Geo_TaskUpdate
{  
    static testmethod void myTest_MethodTask()
    {
        Account a= new Account();
        a.name='TestGeoAccount';
        a.recordtypeid='012500000004uvF';
        a.type='Broker Dealer' ;
        insert a; 
        
        
        Contact c= new Contact();
        c.firstname='TestGeo'; 
        c.lastname='Contact';
        c.accountid=a.id;
        c.recordtypeid='012300000004rLn';
        c.Contact_Type__c='S & E Contact';
        c.Relationship__c='Attorney';
        insert c; 
        
        system.assertequals(c.accountid,a.id);

                  
        Task t = new Task();
        t.OwnerId = '00550000001j5tyAAA';
        t.Type = 'Outbound';
        t.Status = 'Not Started';
        t.ActivityDate = Date.TODAY();
        t.Priority = 'Normal';
        t.Subject = 'Check-In:Test Geo 007';
        //t.WhoId = '0035000001ckImYAAU';
        t.WhoId = c.id;
        insert t;
    }         
}