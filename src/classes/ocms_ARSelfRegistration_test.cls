@isTest
public class ocms_ARSelfRegistration_test {
    
    public ocms_ARSelfRegistration_test() {
        
    }
    //Global vars
    static Contact con1;
    static cms__Content__c txc {get; set;}

    /**
    * @description initialize all test data
    **/
    static void init() {
        con1 = new Contact();
        con1.FirstName = 'Test';
        con1.LastName = 'Guy1';
        con1.Phone = '0123456789';
        con1.Email = 'testguy1@testyourcode.com';
        
        insert con1;

        Contact con2 = new Contact();
        con2.FirstName = 'Test3';
        con2.LastName = 'Guy3';
        con2.Phone = '0123456789';
        con2.Email = 'testguy3@testyourcode.com';
        
        insert con2;

        Profile pro = [select Id from Profile where Name = 'Chatter Free User'];

        
        User user = new User();
        user.FirstName = 'Test3';
        user.LastName = 'Guy3';
        user.Phone = '0123456789';
        user.Email = 'testguy3@testyourcode.com';
        user.CommunityNickname = 'testguy3@testyourcode.com';
        user.Alias = 'HelloMan';
        user.UserName = 'testguy3@testyourcode.com';
        user.Contact = con2;
        user.ProfileId =  pro.Id;
        user.TimeZoneSidKey = 'GMT';
        user.LocaleSidKey = 'en_US';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.LanguageLocaleKey = 'en_US';

        insert user;
        
        System.assertEquals(user.ProfileId,pro.Id);

        

        Account acc = new Account();
        acc.Name = 'New Community Users';

        insert acc;

    }

    /**
    * @description setup the page
    **/
    static private cms.GenerateContent setUpController() {
                
        Map <String, String> contextProperties = new Map<String, String>{'page_mode' => 'prev'};    
        cms__Sites__c mySite = cms.TestExtensionFixtures.InitiateTest('Public', contextProperties);
        
        cms__Content_Type__c ct = new cms__Content_Type__c(cms__Name__c = 'ARSelfRegistration', cms__Site_Name__c = 'Public', cms__Label__c = 'ARSelfRegistration');
        insert ct;

        txc = new cms__Content__c(
                cms__Content_Type__c         = ct.Id,
                cms__Name__c                 = 'ARSelfRegistrationCon',
                cms__Description__c          = 'Testing',
                cms__Preview__c              = true,
                cms__Published__c            = false,
                cms__Published_Start_Date__c = System.now(),
                cms__Site_Name__c            = 'Public',
                cms__Revision_Number__c      = 0,
                cms__Revision_Origin__c      = null,
                cms__Version_Number__c       = 1,
                cms__Version_Origin__c       = null,
                cms__Version_Original__c     = true,
                cms__Version_Parent__c       = null,
                cms__Depth__c                = 0
        );
        insert txc;
        
        cms__Content_Layout__c cl = new cms__Content_Layout__c(cms__Name__c = 'ARSelfRegistrationCL');
        insert cl;
        
        cms__Page__c page = new cms__Page__c(cms__Name__c = 'ARSelfRegistrationPage', cms__Site_Name__c = 'Public');
        insert page;
        
        cms__Content_Layout_Instance__c cli = new cms__Content_Layout_Instance__c(cms__Content__c = txc.Id, cms__Content_Layout__c = cl.Id);
        insert cli;
        
        System.assertEquals(cli.cms__Content__c,txc.Id);


        cms__Page_Content_Layout_Instance__c pcli = new cms__Page_Content_Layout_Instance__c(cms__Content_Layout_Instance__c=cli.Id,cms__Page__c=page.Id);
        insert pcli;

        cms.API anAPI = new cms.API(null, 'prev');
        anAPI.site_name = 'Public';
        
        System.currentPageReference().getParameters().put('ecms', anAPI.getSerialize());
        System.currentPageReference().getParameters().put('content_id', txc.Id);
        System.currentPageReference().getParameters().put('cli_id', cli.Id);
        System.currentPageReference().getParameters().put('pcli_id', pcli.Id);

        cms.GenerateContent gc = new cms.GenerateContent();

        gc.content = txc;
        gc.cli = cli;
        gc.pcli = pcli;

        //cms.CreateContentController cc = new cms.CreateContentController();
        //cc.content = sitePreferencesContent;
        //cc.content_properties = content_properties;

        return gc;
    }


    /**
    * @description Main test function
    *
    **/
    static testMethod void testARRegForm() {
        //Call the init function
        init();
        
        //Setip page controller
        cms.GenerateContent gc = setUpController();

        //Create new instance of the SELF REG page
        ocms_ARSelfRegistration selfRegPage = new ocms_ARSelfRegistration();
        selfRegPage.sPage = 'pagelink';

        System.Type type = selfRegPage.getType();

        //Test the getHTML function
        String testGetUrl = selfRegPage.getHTML();
        
        Map<String, String> serviceParams = new Map<String, String>();
        serviceParams.put('action', 'registerUser');
        serviceParams.put('firstname', 'Test');
        serviceParams.put('lastname', 'Guy1');
        serviceParams.put('email', 'testguy1@testyourcode.com');
        serviceParams.put('phone', '1234567890');

        String resp1 = selfRegPage.executeRequest(serviceParams);

        serviceParams = new Map<String, String>();
        serviceParams.put('action', 'registerUser');
        serviceParams.put('firstname', '');
        serviceParams.put('lastname', 'Guy1');
        serviceParams.put('email', 'testguy1@testyourcode.com');
        serviceParams.put('phone', '1234567890');

        String resp2 = selfRegPage.executeRequest(serviceParams);

        serviceParams = new Map<String, String>();
        serviceParams.put('action', 'registerUser');
        serviceParams.put('firstname', 'Test');
        serviceParams.put('lastname', '');
        serviceParams.put('email', 'testguy1@testyourcode.com');
        serviceParams.put('phone', '1234567890');
        
        System.assert(serviceParams != Null);


        String resp3 = selfRegPage.executeRequest(serviceParams);

        serviceParams = new Map<String, String>();
        serviceParams.put('action', 'registerUser');
        serviceParams.put('firstname', 'Test');
        serviceParams.put('lastname', 'Guy1');
        serviceParams.put('email', 'testguy');
        serviceParams.put('phone', '1234567890');

      /*  String resp4 = selfRegPage.executeRequest(serviceParams);

        serviceParams = new Map<String, String>();
        serviceParams.put('action', 'registerUser');
        serviceParams.put('firstname', 'Test');
        serviceParams.put('lastname', 'Guy1');
        serviceParams.put('email', 'testguy1@testyourcode.com');
        serviceParams.put('phone', 'sdfgdsgfdgfdgsdfgdfgdfg');

        String resp5 = selfRegPage.executeRequest(serviceParams);

        serviceParams = new Map<String, String>();
        serviceParams.put('action', 'registerUser');
        serviceParams.put('firstname', 'Test');
        serviceParams.put('lastname', 'Guy1');
        serviceParams.put('email', 'testguy1@tesode.com');
        serviceParams.put('phone', '1234567890');

        String resp6 = selfRegPage.executeRequest(serviceParams);*/

        //selfRegPage.successPageString = 'Doesthiswork?';
        //String ssspage = selfRegPage.successPageString;
        //cms.Link link = selfRegPage.successPageCMSLink;
        //String sPageString = selfRegPage.sPage;
        //String srLink = selfRegPage.sPage;

        //String resp2 = selfRegPage.registerPortalUser(String firstname, String lastname, String email, String phone);

    }

}