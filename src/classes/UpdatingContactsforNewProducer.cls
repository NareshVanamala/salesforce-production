global class UpdatingContactsforNewProducer implements Database.Batchable<SObject>
{
    global Database.QueryLocator Start(Database.BatchableContext BC)
    {
        Id rrrecordtype = [select Id, Name,DeveloperName from RecordType where DeveloperName ='NonInvestorRepContact' and SobjectType = 'Contact'].id;
        string query='select id,mailingstate,name,subscribed__c,control_group__c,Territory__c,Last_Successful_Contact__c,Newproducer_onboarding_status__c,Last_Producer_Date__c,first_producer_date__c,Email from contact WHERE recordtypeid=\''+rrrecordtype+'\' AND first_producer_date__c=null';
        return database.getQueryLocator(query);
    }
    global void Execute(Database.BatchableContext BC,List<Contact> Scope)
    { 
      list<contact> clist=new list<contact>();
       for(contact c:Scope)
       {
         c.subscribed__c=true;
         c.control_group__c=true;
         clist.add(c);  
       }
         update clist;
    }
    global void finish(Database.BatchableContext BC)
    {
       user u=[select id,name,email from user where id=:userinfo.getuserid()];
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {u.email};
       String[] bccAddresses = new String[] {'skalamkar@colecapital.com'};
       mail.setToAddresses(toAddresses);
       mail.setBccSender(true);
       mail.setBccAddresses(bccAddresses);
       mail.setPlainTextBody('Done');
       mail.setSubject('Your Batch class processing has been finished.Please verify the changes in contact');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }    
}