public with sharing class Websites_DrupalInvocableOnAccount implements Database.AllowsCallouts{
    
    @InvocableMethod(label='invokeAccountContacts' description='')
    public static void invokeAccountContacts (List<Id> accounts) {
        if(Test.isRunningTest() && (Websites_Drupal_Endpoint_URLs__c.getAll().Values()==null || Websites_Drupal_Endpoint_URLs__c.getAll().Values().isEmpty())){
			Websites_HttpClass.insertDrupalEnpointURLsForTest();
    	}
    	if(Test.isRunningTest() && !isFromTestClass){
            Test.setMock(HttpCalloutMock.class, new Websites_HttpClass.Mock('{"status": "SUCCESS"}', 200));
        }
        
        Websites_CCDrupalContactJob batchClass= new Websites_CCDrupalContactJob (accounts);
        
        
        if(!Test.isRunningTest())
        	database.executebatch(batchClass, 200);
    	
    }
    public static  boolean isFromTestClass=false;
	public static void invokeAccountContactsTest (List<Id> accounts) {
		isFromTestClass= true;
		invokeAccountContacts(accounts);
	}

}