public with sharing class EmployeeUserChangeClass {

    public PageReference BacktoAccessCentral() 
    {
         PageReference pageRef1 = new PageReference('/apex/home/showAllTabs.jsp');
          pageRef1.setRedirect(true);
          return pageRef1;    
     }



    public boolean addvalues{set;get;}
    public id edt{set;get;}
    public list<Employee__c>emplist1{set; get;}
    public boolean isHRManager{set; get;}
    public string departmentName{get; set;}
    public string ApplicationImplementor{get; set;}
    public static boolean rec = false;
    public Employee__c emprec;
    public Employee__c emp{get;set;}
    public string contextItem1{get; set;}
    public list<Employee__c>emplist{set; get;}
    //public Employee__c objEmp=null;
    Apexpages.StandardController controller;
    PageReference pageRef;
    public NewHireFormProcess__c nh;
    public string searchString{get;set;}
    public List<Employee__c> autoresults{get;set;}
   
    public EmployeeUserChangeClass()
    {
        addvalues = true;
        User U=[select id, HR_Manager__c from User where id=:userinfo.getUserid()];
        isHRManager=U.HR_Manager__c;
        searchString = System.currentPageReference().getParameters().get('lksrch');
        
    }
    public String getSearchString()
    {
        return searchString;
    }  
    public void setSearchString(String SearchString) 
    {
        this.SearchString= SearchString;
    }
    public PageReference Selected(){
        
        id id1 = ApexPages.currentPage().getParameters().get('edt');
        system.debug('--------editrow-------------->'+id1);
        if(id1 != null ){
            emprec = [select id,Full_Name__c ,Name,NewHireFormProcess__c,Department__c,Employee_Status__c from Employee__c where id =:id1];
            //nh = [select id,Updating__c from NewHireFormProcess__c where id=:emprec.NewHireFormProcess__c];
            //nh.Updating__c = true;
            //update nh;
            pageRef = new PageReference('/apex/userchangerequestform?id='+emprec.id);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }
     public PageReference SelectedforTermination(){
        
        id id1 = ApexPages.currentPage().getParameters().get('edt');
        system.debug('--------editrow-------------->'+id1);
        if(id1 != null ){
            emprec = [select id,Name,Full_Name__c ,NewHireFormProcess__c,Department__c,Employee_Status__c from Employee__c where id =:id1];
            //nh = [select id,Updating__c from NewHireFormProcess__c where id=:emprec.NewHireFormProcess__c];
            //nh.Updating__c = true;
            //update nh;
            pageRef = new PageReference('/apex/Employeeterminationform?id='+emprec.id);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }
    
    //Search related methods --- Start 
    public void search() 
    {
       
        runSearch();
    }
      
    public void runSearch() 
    {
        
        autoresults = performSearch(searchString);   
        If(autoresults.size()==0)
        {
        system.debug('@@@'+autoresults.size());
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No Results Found');
         apexpages.addmessage(myMsg);
        }
           
    }

   public List<Employee__c> performSearch(string searchString) 
    {
         string soql;
         String Ravan='Ravan';
         String terminated = 'Terminated';
         string Approved='Approved';
         String HRStatus = 'Submitted for Approval';
         soql = 'select id, name,Employee_Status__c,Full_Name__c ,Department__c from Employee__c';
           if(searchString != '' && searchString != null)
                soql = soql + '  where ((name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\')'+'OR'+'(Full_Name__c LIKE \'%' + String.escapeSingleQuotes(searchString)+'%\'))'+ ' And' + ' Employee_Status__c !=:' + terminated + ' And' + ' HR_Manager_Status__c =:' + Approved;
           else 
               soql = soql +  ' where name=:'+ Ravan ; 
         // soql=  soql + ' and ' + searchString  + '!='+'null';
          //soql = soql + ' limit 200';
           System.debug(soql);
            return database.query(soql);
    }
    
    
    /*public pagereference submit(){
        
        emplist= [select id,Name,NewHireFormProcess__c,Department__c from Employee__c where Name =:contextItem1];
        String firstName = Apexpages.currentPage().getParameters().get('firstName');
        system.debug('Can I see my firstName'+firstName);
        if(emplist.size()>1)
        {   
            //addvalues = true;
            emplist1 = emplist; 
            return null;
        }  
       else if(emplist.size()==1)    
        {
          emprec = [select id,Name,NewHireFormProcess__c,Department__c from Employee__c where id =:emplist[0].id];  
          nh = [select id,Updating__c from NewHireFormProcess__c where id=:emprec.NewHireFormProcess__c];
          nh.Updating__c = true;
          update nh;
          pageRef = new PageReference('/apex/userchangerequestform?id='+emprec.NewHireFormProcess__c);
          pageRef.setRedirect(true);
          return pageRef;
        } 
     return null;  
    }*/
    
    
    public PageReference CreateEmp()
    {
          PageReference pageRef1 = new PageReference('/apex/EmployeeCreationForm');
          pageRef1.setRedirect(true);
          return pageRef1;
    } 
    
    
    
}