@isTest
public class Cockpit_AdminReportClass_Test 
{

   public static testMethod void AdminReportClass()
    {
    //CalllistStatusclskunal rtsk= new CalllistStatusclskunal();
     User u = [Select id,name from user where isactive= true limit 1];
          
     Contact c = new Contact();
     c.Firstname='Test';
     c.Lastname='record';
     insert c;
     
     System.assert(c != null);
     
     //System.assert(cl != null);
     Automated_Call_List__c ACL1 = new Automated_Call_List__c();
     ACL1.name ='TestingCallList1'+c.id;
     //ACL1.Campaign__c =cn.id;
     ACL1.Dynamic_Query_Filters__c='Select Campaign Status&&&&First_Name__c&&!!equals&&!!Naresh';
     ACL1.Custom_Logic__c = 'AND';
     //ACL1.Last_Activity_Date__c=Date.today()-30;
     ACL1.Archived__c=false;
     ACL1.IsDeleted__c=false;
     ACL1.Type_Of_Call_List__c='Campaign Call List';
     ACL1.Visiblity__c='Visible to all users';
     ACL1.TempLock__c='General Lock';
     ACL1.Activity_In_Last_X_Days__c= 7;
     ACL1.Open_Task_Due_In_X_Days__c = 7;
     ACL1.Description__c = 'testing';
     ACL1.Priority__c = true;              
     ACL1.Dynamic_Query__c='select Id,Name,Internal_Wholesaler1__c,Territory__c,RIA_Territory__c from Contact where (First_Name__c = \'Naresh\')';
     insert ACL1;
     System.assert(ACL1 != null);

     Contact c1 = new Contact();
     c1.Firstname='Test1';
     c1.Lastname='record';
     insert c1;
     system.assert(c1 != null);
     
     Contact c3 = new Contact();
     c3.Firstname='Test11';
     c3.Lastname='record';
     insert c3;
     system.assert(c3 != null);
     
     contact_call_list__c clist = new contact_call_list__c();
     clist.Call_Complete__c = false;
     clist.Automated_Call_List__c = ACL1.id;
     clist.Contact__c = c.id;
     clist.CallList_Name__c=ACL1.name;
     clist.IsDeleted__c=false;
     clist.Contact_Territory__c='Atlantic';
     insert clist;
     System.assert(clist != null);
    
     contact_call_list__c clist1 = new contact_call_list__c();
     clist1.Call_Complete__c = true;
     clist1.Automated_Call_List__c = ACL1.id;
     clist1.Contact__c = c1.id;
     clist1.CallList_Name__c=ACL1.name;
     clist1.IsDeleted__c=false;
     clist1.Contact_Territory__c='Atlantic';
     insert clist1;
     system.assert(clist1 != null);
     
     contact_call_list__c clist11 = new contact_call_list__c();
     clist11.Call_Complete__c = true;
     clist11.Automated_Call_List__c = ACL1.id;
     clist11.Contact__c = c3.id;
     clist11.Owner__c = UserInfo.getUserId();
     clist11.CallList_Name__c=ACL1.name;
     clist11.Skipped_Reason__c='already tasked';
     clist11.IsDeleted__c=false;
     clist11.Contact_Territory__c='Atlantic';
     insert clist11;
     system.assert(clist11 != null);
             
     Cockpit_AdminReportClass test = new Cockpit_AdminReportClass();  
     test.AdminReportCallListName =ACL1.name; 
     test.Territorylist='Atlantic';
     test.exportToExcel2();
     date d=system.today();
     test.exportToExcel();
     test.exportToExcel1();
     test.Results1forCallandInternal();
     test.getItems();
     test.getCallListReport();
     test.getTerritoryReport();
     test.getReportOptions();
     test.setReportOptions('None');
     Cockpit_AdminReportClass.ContactCountWrappercls120  testwrapper12= new Cockpit_AdminReportClass.ContactCountWrappercls120('String1',2,2,2,2,2); 
     Cockpit_AdminReportClass.ContactCountWrapperclsforCallandInternal testwrapper456= new Cockpit_AdminReportClass.ContactCountWrapperclsforCallandInternal('String1','String1',2,2,2,2);
         
    }
   
    
    
}