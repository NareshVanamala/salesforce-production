public with sharing class Marektingsupportfee {

    public list<Event_Automation_Request__c> eventlist{get;set;}
    public boolean temp{get;set;}
    public string contactid {get;set;}
    public contact con1{get;set;}
    public Marektingsupportfee(ApexPages.StandardController controller)
    {
         id conid=apexpages.currentpage().getparameters().get('id');
         contactid =conid;
         if(conid!=null)
         {
         eventlist=[select id,name,Amount_Requested__c,Date_Approved_by_Compliance__c,Date_Check_Mailed_to_Broker_Dealer__c,Date_Compliance_Received__c,Date_Forwarded_to_Accounting__c,Comments__c,Rep_Name_FBO__c,Event_Date__c,Date_Submitted__c from Event_Automation_Request__c where Rep_Name_FBO__c=:conid];
         }
    }
   public pagereference editrow()
   {
       id id1 = ApexPages.currentPage().getParameters().get('edt');
       id conid=apexpages.currentpage().getparameters().get('id');
       
       pagereference pageRef = new PageReference('/'+id1+'/e?retURL=%2F'+conid);
       pageRef.setRedirect(true);
       return pageRef;
       
   } 
   public void deleterow()
   {
       id id2 = ApexPages.currentPage().getParameters().get('dlt');
       Event_Automation_Request__c e =[select id,name,Amount_Requested__c,Date_Approved_by_Compliance__c,Date_Check_Mailed_to_Broker_Dealer__c,Date_Compliance_Received__c,Date_Forwarded_to_Accounting__c,Comments__c,Rep_Name_FBO__c,Event_Date__c,Date_Submitted__c from Event_Automation_Request__c where id=:id2];
        
         if (Event_Automation_Request__c.sObjectType.getDescribe().isDeletable()) 
        {
            delete e;
        }
       id conid=apexpages.currentpage().getparameters().get('id');
       if(conid!=null)
       {
       eventlist=[select id,name,Amount_Requested__c,Date_Approved_by_Compliance__c,Date_Check_Mailed_to_Broker_Dealer__c,Date_Compliance_Received__c,Date_Forwarded_to_Accounting__c,Comments__c,Rep_Name_FBO__c,Event_Date__c,Date_Submitted__c from Event_Automation_Request__c where Rep_Name_FBO__c=:conid];
       }
   }
   public pagereference newevent()
   {
        String samp = System.Label.Current_Org_Url;
       con1=[select id,firstname, lastname from contact where id=:contactid];
      system.debug('check nycon'+con1);
       Id rtype = [select Id, Name,DeveloperName from RecordType where DeveloperName ='ReimbursementRecordType' and SobjectType = 'Event_Automation_Request__c'].id;
       string firstname=con1.firstname;
       string lastname=con1.lastname;
       pagereference pageRef = new PageReference(''+samp+'/a37/e?retURL=%2Fa37%2Fo&RecordType=012500000005VGd&ent=01I5000000035c1');
        //pagereference pageRef1=new PageReference('https://cole--stage.cs4.my.salesforce.com/a38/e?CF00NP0000000ghvP=firstname+lastname&CF00NP0000000ghvP_lkid=0035000000X6Gvn&retURL=%2F0035000000X6Gvn&RecordType=012P00000000OXp&ent=01IP00000000XuM');
       pageRef.setRedirect(true);
       return pageRef;
   } 
}