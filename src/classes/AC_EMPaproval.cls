global with sharing class AC_EMPaproval implements Database.Batchable<sObject>
{
     global  List<Employee__c> scope{get;set;}
     
     global list<Employee__c>SubmitForApprovalemplist{get;set;}
     //global  map<string,id>UserNameToidMap{get;set;}
     global Database.QueryLocator start(Database.BatchableContext BC)
    {
        
        scope=new list<Employee__c>();
        AC_Contractor_Extension__c ec=AC_Contractor_Extension__c.getvalues('AC_EmpType');
        string Emptypelist=ec.Employee_Type__c;
        system.debug(Emptypelist);
         List<String> emptypevalues= Emptypelist.split(';');
          system.debug(emptypevalues);

/*date d= system.today();
   list<Employee__c >emplist = [SELECT Employee_Status__c,HR_Manager_Status__c,AC_Send_reminder_email__c,Id,Send_Termination_Email__c,MGR__c,MGR__r.name,Name,Testing_SystemToday__c,Supervisor__c,Supervisor_Lookup__r.name,Employee_Approval__c,Reminder_Email_Date__c,Termination_Email_Date__c,Employee_Type__c,Contractor_renewal_Status__c,Start_Date__c from Employee__c where Employee_Type__c in:emptypevalues and HR_Manager_Status__c='Approved'and Employee_Status__c!='Terminated' and Start_Date__c=:d];
system.debug(emplist);*/
        
        
        
        
        
        set<string>Emptype=new set<String>();
        string empid='a3PR000000008gN';
        Emptype.add('Internal Contractor');
        Emptype.add('External Contractor');
        Emptype.add('Third Party Manager');
        Emptype.add('External Auditor');
        string HRStatus='Approved';
        string empstatus='Terminated';
       
       
       
        date d=system.today();
        String query = 'SELECT MGR__r.Last_Name__c,MGR__r.First_Name__c,MGR__r.Employee_Status__c,Employee_Status__c,HR_Manager_Status__c,AC_Send_reminder_email__c,Id,Send_Termination_Email__c,MGR__c,MGR__r.name,Name,Testing_SystemToday__c,Supervisor__c,Supervisor_Lookup__r.name,Employee_Approval__c,Reminder_Email_Date__c,Termination_Email_Date__c,Employee_Type__c,Contractor_renewal_Status__c,Start_Date__c from Employee__c where Employee_Type__c in:emptypevalues and HR_Manager_Status__c=:HRStatus and Employee_Status__c!=:empstatus ';
       if(Test.isRunningTest())
        {
          query += ' Limit 1';
        }
       
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Employee__c> scope)
    {
           List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();
           set<String>empNameSet=new set<String>(); 
           list<User>Userlist=new list<User>();
          map<string,id>UserNameToidMap=new map<String,id>();
            UserNameToidMap=new map<String,id>();
           list<Employee__c>emplist= new list<Employee__c> ();
           list<Employee__c>UpdateEmpliet=new list<Employee__c>();
           list<Employee__c>SubmitForApprovalemplist=new list<Employee__c>();
           list<Employee__c>ReminderEmailemplist=new list<Employee__c>();
           list<Employee__c>UpdateReminderEmaillist=new list<Employee__c>();
           list<Employee__c>TerminationEmailemplist=new list<Employee__c>();
           list<Employee__c>TerminationupdationEmailemplist=new list<Employee__c>();
           emplist.AddAll(scope);
           system.debug('check the emplist'+emplist);
          
           for(Employee__c epc:scope)
           {
              if(epc.MGR__r.Employee_Status__c!='Terminated')
              {
               string empName=epc.MGR__r.First_Name__c+' '+epc.MGR__r.Last_Name__c;
               empNameSet.add(empName);
               
              }            
          
           }
           
          system.debug('This is the set'+empNameSet);
          system.debug('This is the list of the scope'+emplist);
          
          Userlist=[select id,FirstName,lastName,name,email from User where name  in:empNameSet and isactive=true];
          system.debug('get the userlist'+Userlist);
          
           for(User Upc:Userlist)
                 UserNameToidMap.put(Upc.FirstName+' '+Upc.lastName,Upc.id);   
            
           date d=system.today();
         
           for(Employee__c ct:emplist)
          {
             
             
             if(UserNameToidMap.get(ct.MGR__r.name)!=null)
             {
                ct.Supervisor_Lookup__c=UserNameToidMap.get(ct.MGR__r.name);
                 UpdateEmpliet.add(ct);
              }   
              
              
              if((ct.Reminder_Email_Date__c==d)&&(ct.Reminder_Email_Date__c!=null)&&(ct.Contractor_renewal_Status__c!='Submit for Approval')&&(ct.Supervisor_Lookup__c!=null))
               SubmitForApprovalemplist.add(ct);
               
            if((ct.Contractor_renewal_Status__c=='Submit for Approval')&&(ct.Reminder_Email_Date__c<system.today())&&(ct.Termination_Email_Date__c>system.today())&&(ct.Supervisor_Lookup__c!=null))  
                ReminderEmailemplist.add(ct);
                
           if(((ct.Contractor_renewal_Status__c=='Submit for Approval')||(ct.Contractor_renewal_Status__c=='Rejected'))&&(ct.Termination_Email_Date__c==system.today())&&(ct.Supervisor_Lookup__c!=null))
               TerminationEmailemplist.add(ct); 
          }
           update UpdateEmpliet;
           system.debug('check the list'+SubmitForApprovalemplist);
           System.debug('Check the reminderemaillist'+ReminderEmailemplist);
           System.debug('Check the TerminationEmailemplist'+TerminationEmailemplist);
            if(SubmitForApprovalemplist.size()>0)
            {   
                 for(Employee__c ec:SubmitForApprovalemplist)
                 {
                        system.debug('I am in the loop');
                        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest(); 
                        req.setObjectId(ec.Id);
                        approvalReqList.add(req); 
                        system.debug('check the approvalrequestlist'+approvalReqList);  
                }
             
            List<Approval.ProcessResult> resultList = Approval.process(approvalReqList);  
            system.debug('Check the resultlist'+resultList ) ; 
            }
            if(TerminationEmailemplist.size()>0)
            {
                  for(Employee__c cry:TerminationEmailemplist)
                  {
                    cry.Send_Termination_Email__c=true;
                    TerminationupdationEmailemplist.add(cry);
                  }
            
            }
            if(ReminderEmailemplist.size()>0)
            {
              for(Employee__c cry:ReminderEmailemplist)
              {
                cry.AC_Send_reminder_email__c=true;
                UpdateReminderEmaillist.add(cry);
              
              }
            
            }
            
          if(TerminationupdationEmailemplist.size()>0)  
         update TerminationupdationEmailemplist;
         
         if(UpdateReminderEmaillist.size()>0)  
         update UpdateReminderEmaillist;
    }  
    
    global void finish(Database.BatchableContext BC)
    {
       User current_user=[SELECT Email FROM User WHERE Id= :UserInfo.getUserId()] ;
       string emailadd = current_user.Email; 
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {emailadd};
       //String[] bccAddresses = new String[] {'ntambe@vereit.com','skalamkar@vereit.com','pcooper@vereit.com'};
       mail.setToAddresses(toAddresses);
       //mail.setBccSender(true);
       //mail.setBccAddresses(bccAddresses);
       mail.setSubject('Access Central batch class is finished' );
       mail.setPlainTextBody('Please verify the changes in the Employee record');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    
    }
    
    }