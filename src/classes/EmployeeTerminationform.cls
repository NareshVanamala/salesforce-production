public with sharing class EmployeeTerminationform {
    
    public Employee__c e1{get; set;}
    public List<Asset__c> assets{get;set;}
    Apexpages.StandardController controller;
    PageReference pageRef;
    public String hireId{get;set;}
    
    public EmployeeTerminationform(ApexPages.StandardController con) {
  
       con = controller;
       hireId = ApexPages.CurrentPage().getParameters().get('id');
       e1 = [SELECT Id,Name,Terminated__c,Termination_date__c FROM Employee__c WHERE id =:hireId];
       System.debug('----Employee Id------>' + hireId);
       
    }
    
    public PageReference submit(){
        list<Asset__c> alist = new list<Asset__c>();
        System.debug('----Employee Id------>' + hireId);
        if (Schema.sObjectType.Asset__c.isUpdateable()) 

        update e1;
        assets = [SELECT Access_Name__c,Employee__c,Id,Application_Implementor__c,Asset_Status__c,Termination_Status__c FROM Asset__c WHERE Employee__c =:hireId];
        for(Asset__c a:assets){
          System.debug('---------------Assets-------------->' + a.Termination_Status__c);    
          if(((a.Asset_Status__c == 'Active')&&(a.Access_Name__c!='ADP'))||((a.Asset_Status__c == 'Added')&&(a.Access_Name__c!='ADP')))
          {  
            a.Asset_Status__c = 'Removal In Process';
            a.Terminating__c = true;
            alist.add(a);
          }
         else if(a.Access_Name__c=='ADP') 
         {
          a.Asset_Status__c = 'Removed';
          a.Removed_On__c=system.today();
           alist.add(a);
         }
          
          
        }
        if (Schema.sObjectType.Asset__c.isUpdateable()) 

        update alist;
        
        pageRef = new PageReference('/apex/EmployeeterminationSubmited');
        pageRef.setRedirect(true);
        return pageRef;
        //return null;
    }
    
    
}