global with sharing class MediaRoomService implements cms.ServiceInterface {

    public String JSONResponse {get; set;}
    private String action {get; set;}
    private Map<String, String> parameters {get; set;}
    private MediaRoom  wpl = new MediaRoom();

    global MediaRoomService() {
        
    }

    global System.Type getType(){
        return MediaRoomService.class;
    }

    global String executeRequest(Map<String, String> p) {
        this.action = p.get('action');
        this.parameters = p; 
        this.LoadResponse(); 
        system.debug('this.parameters'+this.parameters);   
        return this.JSONResponse;
    }

    global void loadResponse(){
       /* if (this.action == 'getYearList'){
            this.getYearList();
           // system.debug('this.getYearList()'+this.getYearList() ); 
        }*/ 
        if (this.action == 'getPressReleaseList'){
            this.getPressReleaseList();
        } else if (this.action == 'getResultTotal'){
            //String year = parameters.get('year');
            String pressRelease = parameters.get('pressRelease');
             
            this.getResultTotal(pressRelease);

        } else if (this.action == 'getPropertyList'){

            //String year = parameters.get('year');
            String pressRelease = parameters.get('pressRelease');
           // system.debug('year'+year );    
            system.debug('pressRelease '+pressRelease );   
            this.getPropertyList(pressRelease);
            
        }
    }

    private void getResultTotal(String pressRelease){
        this.JSONResponse = JSON.serialize(wpl.getResultTotal(pressRelease));
    }

    private void getPropertyList(String pressRelease){
        this.JSONResponse = JSON.serialize(wpl.getPropertyList(pressRelease));
    }

    /*private void getYearList(){
        this.JSONResponse = JSON.serialize(wpl.getYearList());
        system.debug('this.JSONResponse'+this.JSONResponse );  
    }*/

    private void getPressReleaseList(){
        this.JSONResponse = JSON.serialize(wpl.getPressReleaseList());
    }
}