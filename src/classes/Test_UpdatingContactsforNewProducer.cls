@isTest
private class Test_UpdatingContactsforNewProducer {

    static testmethod void test() {
   
    Id rrrecordtype = [select Id, Name,DeveloperName from RecordType where DeveloperName ='NonInvestorRepContact' and SobjectType = 'Contact'].id;
    profile p = [select id,name from profile where name='Internal Sales'];  
    list<User> ulist=[select id,name,Territory__c,isactive from User where isactive=true and Profileid=:p.id];  
 
   Account a = new Account();
       a.name = 'Test';
       insert a; 
   
   list<contact> clist = new list<contact>();
   list<string> olist = new list<string>{'null','New Producer Thank You & Schedule Virtual','New Producer Follow Up & Schedule Virtual 1','New Producer Follow Up & Schedule Virtual 2','New Producer Follow Up & Schedule Virtual 3','New Producer Virtual Follow Up & Schedule External','New Producer Follow Up & Schedule External 1','New Producer Follow Up & Schedule External 2','New Producer External Appt Follow Up Email & Call','New Producer External Appointment Follow Up 1','New Producer External Appointment Follow Up 2'};
    for(integer i=0;i<10;i++)
    {
    Contact c1 = new Contact();
       c1.lastname = 'Cockpit';
       c1.Accountid = a.id;
       c1.Relationship__c ='Accounting';
       c1.Contact_Type__c = 'Cole';
       c1.Wholesaler_Management__c = 'Producer A';
       c1.Territory__c ='Chicago';
       c1.RIA_Territory__c ='Southeast Region';
       c1.RIA_Consultant_Picklist__c ='Brian Mackin';
       c1.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c1.Wholesaler__c='Andrew Garten';
       c1.Regional_Territory_Manager__c ='Andrew Garten';
       c1.Internal_Wholesaler__c ='Keith Severson';
       c1.Territory_Zone__c = 'NV-50';
       c1.Next_Planned_External_Appointment__c = system.now();
       c1.Newproducer_onboarding_status__c=olist[i];
       c1.recordtypeid = rrrecordtype;
       c1.mailingstate='OH';
       c1.email='sandeep'+i+'m.mariyala@polarisft.com';
       c1.subscribed__c=false;
       c1.control_group__c=false;
       clist.add(c1);
       
      }
       insert clist;
       
     REIT_Investment__c rt = new REIT_Investment__c();
     rt.Deposit_Date__c=system.today();
     rt.Admit_Date__c=system.today();
     rt.Original_Capital__c=5000;
     rt.Current_Units__c=100;
     rt.Current_Capital__c=1000;
     rt.Investor_Contact__c=clist[0].id;
     rt.Rep_Contact__c=clist[0].id;
     insert rt;
     update clist;
    
     
    Manual_or_Automatic__c m1 = new Manual_or_Automatic__c();
      m1.name='test';
      m1.Manual_Assignment__c=true;
      insert m1;
    Test.startTest();
    UpdatingContactsforNewProducer np = new UpdatingContactsforNewProducer();
    Database.BatchableContext bc;
    
    np.start(bc);
    np.execute(bc,clist);
    np.finish(bc);
    Test.stopTest(); 
      }

}