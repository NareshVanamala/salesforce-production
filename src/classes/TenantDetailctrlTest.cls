@isTest(SeeAllData=true)
private class TenantDetailctrlTest {
    static testMethod void updateLenTest(){
        try{    MRI_Property__c  mri = [select id from MRI_Property__c limit 1];
        Property_Contacts__c propCon = new Property_Contacts__c();
        propCon.Name = 'Sample PropCon';
        propCon.Email_Address__c ='SamplePropCon@company.com';
        propCon.MRI_Property__c = mri.Id;
        propCon.OwnerId =UserInfo.getuserid();
        insert propCon;
        
        Lease__c leaseobj=[Select id from Lease__c where Lease_Status__c = 'Active' and Sales_Report_Required__c = TRUE limit 1];
        Property_Contact_Leases__c propleases = new Property_Contact_Leases__c();
        propleases.Lease__c=leaseobj.id;
        propleases.Property_Contacts__c=propCon.id;
        propleases.Contact_Type__c='Financials;Gross Sales';
        insert propleases;
                     System.assertEquals(propleases.Lease__c,leaseobj.id);

        
        
        String propconid = apexpages.currentpage().getparameters().put('id' , propCon.id);
        TenantDetailctrl updateTenClass = new TenantDetailctrl();
        updateTenClass.getFunds();
        updateTenClass.getAvgLeaseTerm();
        updateTenClass.getPropManagers();
        updateTenClass.doSearch();
        updateTenClass.PropCronSearch();
        updateTenClass.tosave();
        updateTenClass.generateExcel();
        updateTenClass.CurrentPage();
        updateTenClass.exportToExcel();
        updateTenClass.next();
        updateTenClass.previous();
        updateTenClass.propcontactsfilter(true,false,1
                                          ,0,'ARCP',null,propconid,null,null,null,null);
           }
         catch(Exception e){
        
    }
    }
   
}