public with sharing class newpifcontroller{
public List<Project_Initiation__c> pif {get; set;} 
//Picklist of tnteger values to hold file count
public string fileName{get;set;}  
public Blob fileBody{get;set;}
public string radio{get;set;}
public string radio1{get;set;}
public string radio2{get;set;}
public String datename {get; set;}
Apexpages.StandardController controller;
public List<Attachment> attachments;
public newpifcontroller(ApexPages.StandardController Controller) 
    {
        this.controller= controller;
        Project_Initiation__c c = (Project_Initiation__c)controller.getRecord();
        c.recordtypeid = '012P00000000PK6';
        c.PMO_Group_Status__c= 'Submitted For Approval';
        Id urlid = apexpages.currentpage().getparameters().get('id');
        if(urlid==null)
         radio2='Yes';
        if(urlid!=null)
        {
         system.debug('-----------Urlid-----------'+urlid);
         Project_Initiation__c pi=[select id,name,ARCP_Integration_Project__c,Was_the_Project_Included_in_the_Original__c,Projecr_Requires_Compliance_Approval__c from Project_Initiation__c where id=:urlid];
         if(pi.ARCP_Integration_Project__c==True)
            radio='Yes';
         else
            radio='No'; 
         if(pi.Was_the_Project_Included_in_the_Original__c==True)
            radio1='Yes';
         else
            radio1='No';
         if(pi.Projecr_Requires_Compliance_Approval__c==True)
            radio2='Yes';
         else
            radio2='No';         
        }
        pif = new List<Project_Initiation__c>(); 
        pif.add(new Project_Initiation__c());
       
       
    }
   
public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Yes','Yes')); 
        options.add(new SelectOption('No','No')); 
        return options; 
    }   
public List<SelectOption> getItems1() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Yes','Yes')); 
        options.add(new SelectOption('No','No')); 
        return options; 
    } 
public List<SelectOption> getItems2() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Yes','Yes')); 
        options.add(new SelectOption('No','No')); 
        return options; 
    }     

public PageReference submit()
  {
        Project_Initiation__c c1 = (Project_Initiation__c)controller.getRecord();
         if(radio=='Yes')
         c1.ARCP_Integration_Project__c = True;        
         else
         c1.ARCP_Integration_Project__c = False; 
         if(radio1=='Yes')
         c1.Was_the_Project_Included_in_the_Original__c = True;        
         else
         c1.Was_the_Project_Included_in_the_Original__c = False; 
         if(radio2=='Yes')
         c1.Projecr_Requires_Compliance_Approval__c = True;        
         else
         c1.Projecr_Requires_Compliance_Approval__c = False; 
         controller.save();
         Id i=controller.getrecord().id;
         system.debug('----------ID------------'+i); 
                   
         pagereference pageRef = new PageReference('/apex/projectinitiationform?id'+'='+i);
         pageRef.setRedirect(true);
         return pageRef;
         
     
 }
public pagereference saveandsubmit()
{
         
         Project_Initiation__c c1 = (Project_Initiation__c)controller.getRecord();
         controller.save();
         Id i=controller.getrecord().id;
         system.debug('----------ID------------'+i); 
         Id urlid = apexpages.currentpage().getparameters().get('id');
         if(urlid==null)
         {
         pagereference pageRef = new PageReference('/apex/projectinitiationform?id'+'='+i);
         pageRef.setRedirect(true);
         return pageRef;
         }
         if(urlid!=null)
         {
         pagereference pageRef = new PageReference('/apex/pifhomepage');
         pageRef.setRedirect(true);
         return pageRef; 
         }
         return null;
}       
public List<Attachment> getAttachments()
    {
        // only execute the SOQL if the list hasn't been initialised
        if (null==attachments)
        {
            attachments=[select Id, ParentId, Name, Description from Attachment where parentId=:apexpages.currentpage().getparameters().get('id')];
        }
        
        return attachments;
    }

}