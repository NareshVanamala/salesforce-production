@isTest
private class CommunityUserDetailsTest{
    static testMethod void CommunityUserTest (){
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
        Account acc = new Account (
        Name = 'newAcc1'
        );  
        insert acc;
        Contact con = new Contact (
        AccountId = acc.id,
        LastName = 'portalTestUser'
        );
        insert con;
        Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
         
        User newUser = new User(
        profileId = p.id,
        username = 'newUser06032015604PM@gmail.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = con.id
        );
        insert newUser;
        System.assertNotEquals(con.lastname,newUser.lastname);

        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        CommunityUserDetails commUserClass = new CommunityUserDetails(sc);
    }
     static testMethod void NoncommunityUserTest (){
                    
        Account acc = new Account (
        Name = 'newAcc1'
        );  
        insert acc;
        Contact con = new Contact (
        AccountId = acc.id,
        LastName = 'portalTestUser'
        );
        insert con;
        System.assertNotEquals(acc.name,con.lastname);

         ApexPages.StandardController sc = new ApexPages.StandardController(con);
        CommunityUserDetails commUserClass = new CommunityUserDetails(sc);
    }
}