@isTest
Public class  LC_LeaseCentralHelper_Test
{
  static testmethod void testCreatelease()
  {
       
    MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
        RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LeaseOpprtunity__c' and DeveloperName = 'New_Lease_Opportunity' limit 1]; 
         string recid=NewRecType.id;
         
         LC_LeaseOpprtunity__c lc1= new LC_LeaseOpprtunity__c();
         lc1.name='TestNEWOpportunity';
         lc1.Lease_Opportunity_Type__c='Lease - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.LC_OtherOpportunity_Description__c='Thisis my description';
         lc1.Base_Rent__c=12345;
         lc1.recordtypeid=recid;
         lc1.Actual_NER__c= String.valueOf(2);
         lc1.Budgeted_NER__c =String.valueOf(4);
         lc1.Milestone__c='Approval Received';
         lc1.LC_Already_Submitted__c=2.0;
         insert lc1;     
          
        test.starttest();
        
        System.assertEquals(mc.Id,  myLease.MRI_PROPERTY__c);
        System.assertEquals(myLease.MRI_PROPERTY__c, lc1.MRI_Property__c);
        
        LC_LeaseCentralHelper lc= new LC_LeaseCentralHelper();
        LC_LeaseCentralHelper.SaveLETSFormCopy(lc1.id,lc1.LC_Already_Submitted__c,'Called from Process Builder');
        
        test.stoptest();
  }  
  
  }