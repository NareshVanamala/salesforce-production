@isTest
public class Test_createpif
{
    static testmethod void createpifTest()
    {  
       string  profileid= [Select id from profile where name='System Administrator' limit 1].id;
       User userToCreate = new User();
       userToCreate.FirstName = 'David123';
       userToCreate.LastName  = 'Liu';
       userToCreate.Email     = 'dvdkliu+sfdc99@gmail.com';
       userToCreate.Username  = 'sfdc-dreamer01312016@vereit.com';
       userToCreate.Alias     = 'fatty';
       userToCreate.ProfileId = profileid;
       userToCreate.TimeZoneSidKey    = 'America/Denver';
       userToCreate.LocaleSidKey      = 'en_US';
       userToCreate.EmailEncodingKey  = 'UTF-8';
       userToCreate.LanguageLocaleKey = 'en_US';      
       insert userToCreate; 
       system.assert(userToCreate!=null);
       Id uid=[SELECT Alias,Id,LastName FROM User LIMIT 1].id;
        
        Project_Initiation__c  p =new Project_Initiation__c();
         
        p.name = 'Test';
        p.Code__c = 'PRJ123';
        p.ARCP_Integration_Project__c = true;
        p.CapEx__c = 100.00;
        p.OpEx__c = 100.00;
        p.PMO_Group_Status__c = 'Submitted for Approval';
        p.CFO_GRoup__c = userToCreate.id;
        p.Champion_group__c = userToCreate.id;
        //p.Compliance_group__c = userToCreate.id;
        p.PIF_Group__c = userToCreate.id;
        p.Sponsor__c = userToCreate.id;
        insert p;
        
       
        
        PIF_Child__c  objectchild = new PIF_Child__c();
        objectchild.Name='PMO Approval Process';
        objectchild.PMO_Group_Status__c = 'Submitted for Approval';
        objectchild.PIF_parent__c = p.id;
      
         p.PMO_Group_Status__c = 'Approved';
         p.PIF_Group_Status__c =  'Submitted for Approval';
      try{
     update p;}
     catch(Exception e){}
       
      system.assertequals(objectchild.PIF_parent__c,p.id);

       
      PIF_Child__c  objectchild1 = new PIF_Child__c();
      objectchild1.PMO_Group_Status__c = 'Approved';
      objectchild1.PIF_Group_Status__c = 'Submitted for Approval';
      objectchild1.Name='PIF Review Group Approval Process';  
      objectchild1.PIF_parent__c = p.id;
       
      p.PMO_Group_Status__c = 'Approved';
       p.PIF_Group_Status__c =  'Approved';
       p.Champion_Status__c = 'Submitted for Approval';  
       
        try{
     update p;}
     catch(Exception e){}
     
       PIF_Child__c  objectchild2 = new PIF_Child__c();
       objectchild2.PMO_Group_Status__c = 'Approved';
       objectchild2.PIF_Group_Status__c = 'Approved';
       objectchild2.Champion_Group_Status__c = 'Submitted for Approval';  
       objectchild2.Name='Champion Approval Process';
       objectchild2.PIF_parent__c = p.id;     
        
      p.PMO_Group_Status__c = 'Approved';
       p.PIF_Group_Status__c =  'Approved';
       p.Champion_Status__c = 'Approved';  
        p.Sponsor_status__c = 'Submitted for Approval';
         p.Sponsor__c = uid;
       
        try{
     update p;}
     catch(Exception e){}
     
           system.assertequals(objectchild2.PIF_parent__c,p.id);

       
         PIF_Child__c  objectchild3 = new PIF_Child__c();
         objectchild3.PMO_Group_Status__c = 'Approved';
         objectchild3.PIF_Group_Status__c = 'Approved';
         objectchild3.Name='Sponsor Approval Process';
         objectchild3.Champion_Group_Status__c = 'Approved';
         objectchild3.Sponsor_Status__c ='Submitted for Approval'; 
         objectchild3.Sponsor__c = p.Sponsor__c;
        objectchild3.PIF_parent__c = p.id;     
          
         p.PMO_Group_Status__c = 'Approved';
       p.PIF_Group_Status__c =  'Approved';
       p.Champion_Status__c = 'Approved';  
        p.Sponsor_status__c = 'Approved';
        p.Projecr_Requires_Compliance_Approval__c = true;
        p.compliance_Approval_Status__c ='Submitted for Approval' ;
       
        try{
     update p;}
     catch(Exception e){}
       
       PIF_Child__c  objectchild4 = new PIF_Child__c();
         objectchild4.PMO_Group_Status__c = 'Approved';
         objectchild4.PIF_Group_Status__c = 'Approved';
         objectchild4.Name='Sponsor Approval Process';
         objectchild4.Champion_Group_Status__c = 'Approved';
         objectchild4.Sponsor_Status__c ='Approved'; 
         objectchild4.Project_Requires_Compliance_Approval__c=true;
         objectchild4.Compliance_Group_Status__c= 'Submitted for Approval';
        objectchild4.PIF_parent__c = p.id;     
         
          p.PMO_Group_Status__c = 'Approved';
       p.PIF_Group_Status__c =  'Approved';
       p.Champion_Status__c = 'Approved';  
        p.Sponsor_status__c = 'Approved';
        p.compliance_Approval_Status__c ='Approved' ;
        p.CFO_Status__c= 'Submitted for Approval';
        
        try{
     update p;}
     catch(Exception e){}
       
          PIF_Child__c  objectchild5 = new PIF_Child__c();
          objectchild5.PMO_Group_Status__c = 'Approved';
          objectchild5.PIF_Group_Status__c = 'Approved';
          objectchild5.Champion_Group_Status__c = 'Approved';
          objectchild5.Sponsor_Status__c ='Approved';
          objectchild5.Compliance_Group_Status__c= 'Approved';
          objectchild5.CFO_Group_Status__c ='Submitted for Approval';
          objectchild5.Name='CFO Approval Process';
          objectchild5.PIF_parent__c = p.id;
                     
   
} 
    static testmethod void createpifTest1()
    {   
       Id uid=[SELECT Alias,Id,LastName FROM User LIMIT 1].id;
       string  profileid= [Select id from profile where name='System Administrator' limit 1].id;
       User userToCreate = new User();
       userToCreate.FirstName = 'David123';
       userToCreate.LastName  = 'Liu';
       userToCreate.Email     = 'dvdkliu+sfdc99@gmail.com';
       userToCreate.Username  = 'sfdc-dreamer013122017@vereit.com';
       userToCreate.Alias     = 'fatty';
       userToCreate.ProfileId = profileid;
       userToCreate.TimeZoneSidKey    = 'America/Denver';
       userToCreate.LocaleSidKey      = 'en_US';
       userToCreate.EmailEncodingKey  = 'UTF-8';
       userToCreate.LanguageLocaleKey = 'en_US';      
       insert userToCreate; 
       system.assert(userToCreate!=null);
        
        Project_Initiation__c  p1 =new Project_Initiation__c();
         
        p1.name = 'Test';
        p1.Code__c = 'PRJ123';
        p1.ARCP_Integration_Project__c = false;
        p1.CapEx__c = 100.00;
        p1.OpEx__c = 100.00;
        p1.PMO_Group_Status__c = 'Submitted for Approval';
        p1.CFO_GRoup__c = userToCreate.id;
        p1.Champion_group__c = userToCreate.id;
        //p.Compliance_group__c = userToCreate.id;
        p1.PIF_Group__c = userToCreate.id;
        p1.Sponsor__c = userToCreate.id;
        insert p1; 
        
        p1.PMO_Group_Status__c = 'Rejected' ;
        
         try{
     update p1;}
     catch(Exception e){}
     
    system.assertequals(p1.name,'Test');

          
}

}