@isTest
public class NationalAccountRepProfiletestclass{
static testMethod void  NationalAccountRepProfiletestmethod() {
  
  Account a = new Account();
  a.name = 'testaccount';
  insert a;
    System.assertEquals(a.name,'testaccount');

  National_Account_Helper__c c = new  National_Account_Helper__c();
  c.Platform_A_SOW__c ='350'; 
  c.Platform_B_SOW__c='350';
  c.Platform_A_Total_Assets__c='121'; 
  c.Platform_B_Total_Assets__c='345';
  c.Platform_C_SOW__c='123';
  c.Platform_C_Total_Assets__c='456';
  c.Account__c=a.id;
  c.Platform_D_SOW__c='234';
  c.Platform_D_total_Assets__c='67';
  c.TotalReps__c='78';
  c.Reps_With_7__c='5';
  c.Total_Selling_Reps__c='45';
  c.Total_Sale__c='78';
  c.Sales_Per_Rep__c='89';
  insert c;
  
  System.assertEquals(c.Account__c,a.id);

  
  //c.Total_Cole_Tickets_Only_Funds_in_SF__c = 1;
  //c.Total_Cole_Production_Funds_in_SF__c = 1000;
  //c.Last_Producer_Date__c = System.Today();
  
  
  
    //PageReference pref = new PageReference();

    ApexPages.currentPage().getParameters().put('id',a.id);
    //Test.setCurrentPage(pref);

  
  ApexPages.StandardController controller = new ApexPages.StandardController(a); 
  NationalAccountRepProfile x = new NationalAccountRepProfile(controller);
  x.saveChanges();
  x.Edit1();
  x.getrecs();

}
}