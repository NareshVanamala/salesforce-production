global virtual without sharing class ocms_DistributionList extends cms.ContentTemplateController{

    private List<SObject> DistributionList;
    private String html;
   
       
    public List<SObject> getDistributionList(String sharetype, String year){

        List<SObject> DistributionList;
        String query;
        string Distributions='Distributions';
        query = 'SELECT Id, Name,Type_of_Share__c, Date_Paid__c, Daily_Distribution__c, Net_Asset_Value__c, Annualized_Yield__c FROM Distributions__c where Category__c=\'' + Distributions + '\'';
        
       
      if(sharetype!= null && sharetype!= '' && year!='Since Inception'){
            query += ' AND Type_of_Share__c = \'' + sharetype + '\'';
        }

         if(sharetype!= null && sharetype!= '' && year=='Since Inception'){
            query += ' AND Type_of_Share__c= \'' + sharetype + '\'';
        } 
        
         if(year!= null && year!= '' && year!= 'Since Inception'){
            query += ' AND  Date_Paid_Year__c = \'' + year + '\'';
        }

            query +=' order by Date_Paid__c DESC'; 
            
        DistributionList= Database.query(query);
        return DistributionList;
    }
     
    @TestVisible private String writeControls(){
        String html = '';
          
        html+='<article class="topMar7 wrapper teer3"><h1>Distributions</h1>'+
              '<div class="clear">'+
              '<select class="ocms_sharetype">'+
              '<option selected="selected" value="Class W Shares">Class W Shares</option>'+
              '<option value="Class I Shares">Class I Shares</option>'+
              '<option value="Class A Shares">Class A Shares</option>'+
              '</select></div>';
        html+='<div class="ocms_selectedshare" style="display:block"></div>';   
           
        html+='<div><select class="ocms_year">'+
              '<option selected="selected" value="Since Inception">Since Inception</option>'+
              '<option value="2014">2014</option>'+
              '<option value="2013">2013</option>'+
              '<option value="2012">2012</option>'+
              '</select></div>';
                 
        html+='<div class="panel" style="padding:10px; display:block"></div>'; 
        
        html+=' <p>The Company&#39;s board of directors has authorized a daily distribution based on 365 days in the calendar year. The monthly distribution amount represents the sum of all daily distributions. The daily distribution amount for each class of outstanding common stock will be adjusted based on the relative net asset value of the various classes each day so that, from day to day, distributions constitute a uniform percentage of the net asset value per share of all classes. As a result, from day to day, the per share daily distribution for each outstanding class of common stock may be higher or lower based on the relative net asset value of each class of common stock on that day. During the distribution period, the distributions will be payable to stockholders of record as of the close of business on each day of the period and the distributions will be paid monthly in arrears.<br />'+
                '<br />' +
                'We may pay distributions from sources other than cash flow from operations, including from the proceeds of this offering, from borrowings or from the sale of properties or other investments, among others, and we have no limit on the amounts we may pay from such sources. We expect that our cash flow from operations available for distribution will be lower in the initial stages of this offering until we have raised significant capital and made substantial investments. Contrary to traditional non-exchange traded REITs, and as a result of our common stock being sold and redeemed on a daily basis at NAV per share, distributions that represent a return of capital or exceed our operating cash flow will be reflected in our daily calculation of NAV.<br />'+
                '<br />' +
                'During the nine months ended September 30, 2013, we paid distributions of $1.2 million, including $330,000 through the issuance of shares pursuant to the DRIP. The distributions paid during the nine months ended September 30, 2013 were funded by cash flows from operations, including cash flows in excess of distributions from the prior year, of $705,000 and proceeds from the Offering of $542,000.<br />'+
                '<br />'+
                '<strong>There is no guarantee that investors will receive any specified amount of distributions.</strong><br />'+
                '<br />'+
                '* Cole Income NAV is designed to provide daily liquidity to investors under most investing conditions. Each calendar quarter, net redemptions are generally limited to 5% of total net assets at the end of the previous quarter. If net redemptions reach the five percent limit, no further redemptions will be processed that quarter. We will begin accepting redemption requests again on the first business day of the next calendar quarter, but will apply the five percent quarterly limitation on redemptions on a per-stockholder basis, instead of a first-come, first-served basis. The Board of Directors has discretion to further limit or suspend redemptions, if in the best interests of shareholders of Cole Income NAV.<br />'+
                '<br />'+
                'The annualized yield is calculated as the monthly distribution amount shown for a given month, divided by the number of days in that month, multiplied by the number of days in the current year, and divided by the NAV per share on the date the distributions were paid. Past performance is no guarantee of future results.</p>';
               
               
         html+='</article>';   
                     
        return html;
    }
    
    global override virtual String getHTML(){
        String html = '';
        
        

        html += writeControls();
        html += '<script src="/resource/ocms_Distributionlist" type="text/javascript"></script>';

        return html;
    }
   

}