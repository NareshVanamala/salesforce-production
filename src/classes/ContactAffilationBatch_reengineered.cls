global class ContactAffilationBatch_reengineered implements Database.Batchable<SObject>,Database.Stateful
{
    global String Query;
    global String strFinal;
    global String strDupe;
    global List<String> reps = new List<string>() ;
   global   Map<String, Contact> mapRepID = new Map<String, Contact>(); 
   global   List<Contact>dupelist = new List<Contact>();
   
   
    
    
   global ContactAffilationBatch_reengineered()  
{  
date fourweeks= system.Today();
       fourweeks= fourweeks.adddays(-10);
List<Contact> ThreeMonths = new List<Contact>();
 ThreeMonths = [Select ID , Rep_CRD__c , RecordTypeID from Contact where Rep_CRD__c != null and RecordTypeId IN ('012300000004rLn','012500000005Qqz') AND LastModifieddate > :fourweeks];

for(Contact cn : ThreeMonths)
{
string a = cn.Rep_CRD__c;
reps.add(a);
}

} 
    
    

    global Database.QueryLocator start(Database.BatchableContext BC)  
    {
        Query = 'Select ID, AccountId, Rep_CRD__c, Name,Contact.Account.Name, RecordTypeID from Contact where RecordTypeId IN (\'012300000004rLn\',\'012500000005Qqz\') AND Rep_CRD__c  in';
       
       // Query = 'Select ID, AccountId, Rep_CRD__c, Name,Contact.Account.Name, RecordTypeID from Contact where RecordTypeId IN (\'012500000005Qqz\') AND Rep_CRD__c  in';
        Query = query + ' : reps ' ;
        
        query = query + ' order by recordtypeID desc  ' ;
        
        System.debug('=================================> Query : '+Query);  
        strFinal = 'Account ID, ContactID, Contact Name, AccountName \n';
        strDupe = 'ContactID, Rep CRD, RecordTypeID , Account ID, Contact Name , Account Name  \n';
        return Database.getQueryLocator(Query); 
    }

    global void execute(Database.BatchableContext BC,List<Contact> scope)  
    { 
    
        List<Contact_Affiliation__c> lstNewContactAffil = new List<Contact_Affiliation__c>();
        system.debug('check the size of the list scope..'+ scope.size());
        //Recordtype rec=[select id, name from Recordtype where sobjecttype='Contact' and name='Registered_Investment_Advisor_RIA_Contact'];
        for(Contact objCon : scope) 
        {
            
            Contact check =  mapRepId.get(objCon.Rep_CRD__c);
            system.debug('Check Check check....'+ check );
            if(check != null)
            
             {
                      string esccomma = check.Account.Name;
                      If(esccomma!=null)
                     esccomma = esccomma.escapeCsv();
                     
                       string esccomma1 = ObjCon.Account.Name;
                       
                        If(esccomma1!=null)
                      esccomma1 = esccomma1.escapeCsv();
                      
                     String DupeCons = check.Id +','+  check.Rep_CRD__c + ',' + check.RecordTypeID + ',' + check.AccountID +','+ check.Name + ',' + esccomma + '\n';
                     If((objCon.RecordTypeID=='012500000005Qqz')&& (check.RecordTypeID=='012300000004rLn') )
                      {
                          Contact_Affiliation__c caf = new Contact_Affiliation__c();
                          caf.Account__c = objCon.AccountId;
                          caf.Contact_Name__c = check.ID;
                          system.debug('Check the contacts......'+caf.Contact_Name__c);
                          lstNewContactAffil.add(caf); 
                          String AffsCreated = objCon.AccountID + ',' +  check.ID +'\n';
                          strFinal = strFinal + Affscreated ; 
                          
                            dupeCons = DupeCons +  ObjCon.Id +','+  ObjCon.Rep_CRD__c + ',' + ObjCon.RecordTypeID + ',' + objCon.AccountID + ',' + objCon.Name + ',' + esccomma1  +'\n'; 
                          strDupe =  strDupe + dupeCons; 
                      }
                     //seperating the if condition for all the cases.  
                       else If((objCon.RecordTypeID=='012300000004rLn')&& (check.RecordTypeID=='012500000005Qqz') )
                      {
                          Contact_Affiliation__c caf = new Contact_Affiliation__c();
                          caf.Account__c = check.AccountId;
                          caf.Contact_Name__c = objCon.ID;
                          system.debug('Check the contacts......'+caf.Contact_Name__c);
                          lstNewContactAffil.add(caf); 
                          String AffsCreated = check.AccountId + ',' +  objCon.ID + ',' + ObjCon.Name + ',' +  esccomma + '\n';
                          strFinal = strFinal + Affscreated ; 
                            dupeCons = DupeCons +  ObjCon.Id +','+  ObjCon.Rep_CRD__c + ',' + ObjCon.RecordTypeID +  ',' + objCon.AccountID +','+ objCon.Name + ',' + esccomma1  +'\n'; 
                         strDupe =  strDupe + dupeCons; 
                          
                      }
                     else If((objCon.RecordTypeID=='012300000004rLn')&& (check.RecordTypeID=='012300000004rLn') )
                      {
                         // String DupeCons = check.Id +','+  check.Rep_CRD__c + ',' + check.RecordTypeID +'\n';
                          dupeCons = DupeCons +  ObjCon.Id +','+  ObjCon.Rep_CRD__c + ',' + ObjCon.RecordTypeID  + ',' + objCon.AccountID +','+ objCon.Name + ',' + esccomma1 + '\n'; 
                          strDupe =  strDupe + dupeCons; 
                      }
                      
                      else If((objCon.RecordTypeID=='012500000005Qqz')&& (check.RecordTypeID=='012500000005Qqz') )
                      {
                         // String DupeCons = check.Id +','+  check.Rep_CRD__c + ',' + check.RecordTypeID +'\n';
                          dupeCons = dupeCons + ObjCon.Id +','+  ObjCon.Rep_CRD__c + ',' + ObjCon.RecordTypeID + ',' + objCon.AccountID +','+ objCon.Name +
                           ',' + esccomma +'\n'; 
                          strDupe =  strDupe + dupeCons; 
                      } 
                  
                  }
           else
             {
             mapRepID.put(objCon.Rep_CRD__c, objCon);
             system.debug('Check the map entry...'+ mapRepID);
             }
        
            
        }
        
       
        
        
        try {
           
           if (Schema.sObjectType.Contact_Affiliation__c.isCreateable())
           insert lstNewContactAffil;
           system.debug('Check the size of the affiliation...'+ lstNewContactAffil.size());
        } catch (system.dmlexception e) {
            System.debug('Affiliations not inserted: ' + e);
        } 
        
    }

    global void finish(Database.BatchableContext BC)  
    { 
    AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];

        Messaging.EmailFileAttachment csvAttc1 = new Messaging.EmailFileAttachment();

        blob csvBlob = Blob.valueOf(strFinal);
        string csvname= 'Affiliations.csv';
        csvAttc1.setFileName(csvname);
        csvAttc1.setBody(csvBlob);


       Messaging.EmailFileAttachment csvAttc2 = new Messaging.EmailFileAttachment();
       blob csvBlob1 = Blob.valueOf(strDupe);
       string csvname1= 'Duplicate RR Contacts.csv';
       csvAttc2.setFileName(csvname1);
     csvAttc2.setBody(csvBlob1);
        
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'skalamkar@colecapital.com', 'ksingh@colecapital.com','nvyas@colecapital.com'};
        String[] bccAddresses = new String[] {'ntambe@colecapital.com','ajhunjhunwala@colecapital.com'};
        String subject ='Contact Affilation Records and DupeList';
        email.setSubject(subject);
        email.setBccSender(true);
        email.setToAddresses( toAddresses );
        email.setBccAddresses(bccAddresses);
        email.setPlainTextBody('Please find attached doc which contains list of newly created Affilations and Duplicate RR contacts');
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc1,csvAttc2});
        //email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc2});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});                   
        
       
    }        
}