@RestResource(urlMapping='/ContactinvestorsPDF/*')
global with sharing class ContactInvestorAttachmentCtrl
{
public list<Contact_InvestorList__c> coninvList{get;set;}
    public ContactInvestorAttachmentCtrl(ApexPages.StandardController controller) {
        coninvList = new list<Contact_InvestorList__c>();
        string contactid   =controller.getId();
            for(Contact_InvestorList__c coninv : [select id,Advisor_Name__r.Email,Advisor_Name__r.Phone,Advisor_Name__r.Name,Advisor_Name__c,Advisor_Name__r.DST_Contact_Id__c,internal_sales_agent__r.LastName,internal_sales_agent__r.firstName,internal_sales_agent__r.User_Id__c,CCPT_5__c,CCIT2__c,CCIT_Amount__c,investor_account__c,
            Investor_Last_Name__c,state__c,Address__c,Investor_State__c,City__c,Zipcode__c from Contact_InvestorList__c where Advisor_Name__c=: contactid]){
                    coninvList.add(coninv);
            }
    }

@HttpGet
        global static void createconinvPDFREST()
{           id conid;
            RestRequest req = RestContext.request;
            if(Test.isRunningTest()){
            contact conobj =new contact();
            conobj =[select id ,Contactinvestorscount__c from contact where Contactinvestorscount__c!=0 limit 1];
            conid =conobj.id;
            }else{
              conId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
            }
    
            /*Pdf Body Generation - Start*/
            PageReference pdfPage = new PageReference('/apex/ContactInvestorPDFAttachment?Id='+conId);
            pdfPage.getParameters().put('id',conId);
            Blob coninvbody;
            if(Test.isRunningTest()){
                coninvbody = Blob.ValueOf('dummy text');
            }else{
                coninvbody = pdfPage.getContentAsPdf();
            }
            //Blob coninvbody = !Test.isRunningTest() ? pdfPage.getContentAsPdf() : Blob.ValueOf('dummy text');
            /*Pdf Body Generation - End*/
            /*CSV Body Generation - Start*/
            PageReference csvPage = new PageReference('/apex/ContactInvestorCSVAttachment?Id='+conId);
            csvPage.getParameters().put('id',conId);
            Blob coninvCsvbody =!Test.isRunningTest() ? csvPage.getContent() : Blob.ValueOf('dummy text');
            /*CSV Body Generation - End*/
            Contact_InvestorList__c coninvObj = [select id,Advisor_Name__r.Email,Advisor_Name__r.Phone,Advisor_Name__r.Name,Advisor_Name__c,Advisor_Name__r.DST_Contact_Id__c,internal_sales_agent__r.LastName,internal_sales_agent__r.firstName,internal_sales_agent__r.User_Id__c,CCPT_5__c,CCIT2__c,CCIT_Amount__c,investor_account__c,
            Investor_Last_Name__c,state__c,Address__c,Investor_State__c,City__c,Zipcode__c from Contact_InvestorList__c where Advisor_Name__c=: conId limit 1];
          List<Attachment> attachList = new list<Attachment>();
        set<id> attachmentids = new set<id>();
        set<id> attachments = new set<id>();
        List<Investor_Attachment__c> invesAttachList = new List<Investor_Attachment__c>();
        for(Investor_Attachment__c attachobj : [select id,Contact__c ,Name from Investor_Attachment__c where Contact__c =:conId]){
            attachmentids.add(attachobj.id);
        }
        attachList =[select id,parentId,Name from Attachment where parentId in :attachmentids];
        for(Attachment attchobj : attachList ){
            attachments.add(attchobj.id);
        }
        if(attachList!=null && attachList.size()>0){
            delete attachList;
        }
        invesAttachList = [select id,Attachment_Id__c,Name from Investor_Attachment__c where Attachment_Id__c in :attachments];
        if(invesAttachList!=null && invesAttachList.size()>0){
            delete invesAttachList;
        }
        /*CSV attachment preparation - Start*/    
        Investor_Attachment__c cAttcsv = new Investor_Attachment__c();
        cAttcsv.Contact__c = conId;
        cAttcsv.Type__c = 'Investor List';
        cAttcsv.Sub_Type__c = 'CSV File';
        insert cAttcsv;
        Attachment attachObjcsv = new Attachment();
        attachObjcsv.Body = coninvCsvbody ;
        attachObjcsv.ParentId = cAttcsv.id;
        attachObjcsv.Name = 'contactInvestor.csv';
        try
        {
            insert attachObjcsv;  
            cAttcsv.Attachment_Id__c =   attachObjcsv.Id;
            update cAttcsv;
            
        }
        catch(Exception Ex)
        {
            system.debug('csv error'+ Ex.getMessage());
        }
        /*CSV attachment preparation - End*/ 
        /*Pdf attachment preparation - Start*/   
        Investor_Attachment__c cAttPDF = new Investor_Attachment__c();
        cAttPDF.Contact__c = conId;
        cAttPDF.Type__c = 'Investor List';
        cAttPDF.Sub_Type__c = 'PDF File';
        insert cAttPDF;
        Attachment attachObjPDF = new Attachment();
        attachObjPDF.Body =coninvbody;
        attachObjPDF.ParentId = cAttPDF.id;
        attachObjPDF.Name = 'contactInvestor.pdf';
        try
        {
            insert attachObjPDF;  
            cAttPDF.Attachment_Id__c =   attachObjPDF.Id;
            update cAttPDF;
        }
        catch(Exception Ex)
        {
            system.debug('pdf error message'+Ex.getMessage());
        }
        /*Pdf attachment preparation - End*/  
        }

        // call this method from your Batch Apex
        global static void CreateContactInvAttachment( Id conId, String sessionId )
        {
         System.debug('contactId==>'+conId);
         System.debug('sessionId==>'+sessionId);
         System.debug('Iam in REST API Call method');
         string currentOrgUrl='https://'+URL.getSalesforceBaseUrl().getHost();
           String addr = currentOrgUrl+'/services/apexrest/ContactinvestorsPDF/' + conId;
            HttpRequest req = new HttpRequest();
           req.setEndpoint(addr);
            req.setMethod('GET');
            req.setTimeout(60000);
            req.setHeader('Authorization', 'OAuth ' + sessionId);

            Http http = new Http();
            HttpResponse response = http.send(req);  
        }
      
}