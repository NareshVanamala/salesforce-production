public with sharing class massApplyLoanCtrl{
   List<loanwrapper> loanList = new List<loanwrapper>();
    public List<Loan__c> selectedLoan {get;set;}
   public list<Loan_Relationship__c> loanRelList {get;set;}
   public list <Loan__C> searchresult {get;set;}  
   public list<Deal__c> dealList{get;set;}
   public decimal SumOfPurchasePrice {get;set;}
   public string portfolioId{get;set;}
   public string LoanName{get;set;}
   public decimal TotalLoanAmount{get;set;}
   public string searchstring {get;set;}  
   public string dealId{get;set;}
   public list<string> dealIds = new list<string>();
   public massApplyLoanCtrl(){
      selectedLoan = new List<Loan__c>();
       dealId = ApexPages.CurrentPage().getParameters().get('selectedRecords') ;
      if(dealId!=null){
           dealIds = dealId.split(',');
           system.debug('deal Ids List'+dealIds);
      }
     
       if(loanList!=null)
           loanList.clear();
       
       list<string> portfolioIds  = new list<string>();
    for(Deal__c dealObj :[Select id,Portfolio_Deal__c from Deal__c where id in :dealIds]){
        portfolioIds.add(dealObj.Portfolio_Deal__c);
    }
    portfolioId = portfolioIds[0];
   }
    
    public PageReference dealPage(){
        set<string> loanids = new set<string> ();
        system.debug('selected loan'+selectedLoan);
        if(selectedLoan!=null && selectedLoan.size()>0){
             for(Loan__c lonObj : selectedLoan){
             loanids.add(lonObj.id);
        }
      
       
        dealList = new list<Deal__c>();
        dealList = [Select id,name,Report_Deal_Name__c,Purchase_Price__c from Deal__c where id in :dealIds];
        list<Loan__c> loanlist =new list<Loan__c>();
        loanlist = [Select id ,name,Total_Purchase_Price__c,Total_Loan_Amount__c from Loan__c where id in :loanids];
        string loanid = loanlist[0].id;
        LoanName=loanlist[0].Name;
        TotalLoanAmount=loanlist[0].Total_Loan_Amount__c ;
        decimal Totalpurchaseprice = loanlist[0].Total_Purchase_Price__c;
        system.debug('deal list'+dealList);
        system.debug('loan list'+loanlist);
        loanRelList = new list<Loan_Relationship__c>();
        decimal Ratiovalue ; 
        decimal allocatedLoanAmount ;
        decimal totalDealPurchasePrice =0.00;
        if(dealList!=null && dealList.size()>0){
            for(integer i =0;i<dealList.size();i++){
                totalDealPurchasePrice+=dealList[i].Purchase_Price__c;
            }
        }
        system.debug('totalDealPurchasePrice******'+totalDealPurchasePrice);
        SumOfPurchasePrice = totalDealPurchasePrice+loanlist[0].Total_Purchase_Price__c;
        system.debug('SumOfPurchasePrice******'+SumOfPurchasePrice);
        for(integer i =0;i<dealList.size();i++){
            system.debug('selected deal'+dealList[i]);
            if(loanlist!=null && loanlist.size()>0){
                Ratiovalue = 0.00;
                allocatedLoanAmount = 0.00;
                Ratiovalue =  dealList[i].Purchase_Price__c /SumOfPurchasePrice;
                Ratiovalue=Ratiovalue.setScale(6, RoundingMode.HALF_UP);
                if(TotalLoanAmount != null)
                {
                allocatedLoanAmount = Ratiovalue*TotalLoanAmount;
                }
                else{
                TotalLoanAmount=0.0;
                allocatedLoanAmount = Ratiovalue*TotalLoanAmount;
                }
                String LoanRelName = dealList[i].Report_Deal_Name__c+'-'+loanlist[0].name;
                if(LoanRelName.length() > 80)
                {
                LoanRelName = dealList[i].Report_Deal_Name__c;
                }
                 loanRelList.add(new Loan_Relationship__c(Loan_Amount__c=allocatedLoanAmount,name=LoanRelName,deal__c=dealIds[i],Loan__c=loanlist[0].id,Purchase_Price__c=dealList[i].Purchase_Price__c,Ratio__c=Ratiovalue,External_ID__c=loanlist[0].id+'-'+dealIds[i]));
                 system.debug('loanRelList'+loanRelList);
            }
        }
         if (Schema.sObjectType.Loan_Relationship__c.isCreateable())

         upsert loanRelList External_ID__c;
        
         PageReference page = new PageReference('/apex/MassApplyLoanDeal?selectedLoan='+selectedLoan[0].id);
         system.debug('entered'+page);
        //page.setRedirect(true);
        return page;
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select atleast one loan'));
            return null;
        }
       
    }
    public List<loanwrapper> getLoans()
        {   
        loanList.clear();
        System.debug('search string'+searchstring);
        string searchquery='select Id, Name,Loan_Amount__c,Status__c,Legal_Secretary__c,Lender__c,Fund__c from Loan__c';  
            if(searchstring !=null){
                searchquery +=' where name like \'%'+String.escapeSingleQuotes(searchString)+'%\' ';
                    System.debug('enter search string'+searchstring);
            }
            searchquery +=' Limit 1000';
            System.debug('search query'+searchquery);
            searchresult= Database.query(searchquery);
            for(Loan__c a : searchresult)
            loanList.add(new loanwrapper(a));
            return loanList;
        }

     public PageReference search(){  
      
         return null;
    } 
    
 public PageReference getSelected()
    {
        selectedLoan.clear();
        for(loanwrapper loanwrapper : loanList)
        if(loanwrapper.selected == true)
        selectedLoan.add(loanwrapper.loanObj);
        return null;
    }
    
    public List<Loan__c> GetSelectedLoans()
    {
        if(selectedLoan.size()>0)
        return selectedLoan;
        else
        return null;
    }   
 public class loanwrapper
    {
        public Loan__c loanObj{get; set;}
        public Boolean selected {get; set;}
        public loanwrapper(Loan__c a)
        {
            loanObj = a;
            selected = false;
        }
    }
public pageReference cancel(){
     PageReference page = new PageReference('/'+portfolioId);
        page.setRedirect(true);
        return page;
     
}
}