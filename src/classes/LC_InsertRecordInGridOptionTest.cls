@isTest
Public class LC_InsertRecordInGridOptionTest
{
    static testmethod void testunit1()
    {
         test.starttest();
         Entity__c ent = new Entity__c();
         ent.name=' Test entity1';
         ent.Owned_ID__c = 'P001';
         insert ent;
         
         MRI_PROPERTY__c MRC= new MRI_PROPERTY__c();
         MRC.Property_ID__c='A1234';
         MRC.name='Test_MRI_Property';
         MRC.Enity_Id__c = 'P001';
         insert MRC; 
       
          System.assertNotEquals(ent.name,MRC.name);

         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU';
         mylease.Suite_Sqft__c=1234;
         mylease.Lease_ID__c='A046677';
         myLease.MRI_PROPERTY__c=MRC.id;
         insert mylease;
         
         LC_Helper_for_Grid__c mvc=new LC_Helper_for_Grid__c();
         mvc.name='Update Now';
         insert  mvc;
  
          LC_LeaseOpprtunity__c OLC=new LC_LeaseOpprtunity__c();
          OLC.name='OptionTypeCheckTest';
          OLC.Lease_Opportunity_Type__c='Lease - New';
          OLC.MRI_Property__c=MRC.id;
          OLC.Lease__c =mylease.id;
          OLC.Suite_Sqft__c=mylease.Suite_Sqft__c;
          OLC.Base_Rent__c=Null;
          insert OLC;
             
          LC_OptionSchedule__c LRSC= new LC_OptionSchedule__c();
          LRSC.Lease_Opportunity__c=OLC.id;
          LRSC.Option_Type__c='Option-1';
          LRSC.Amount__c=0;
          LRSC.sqft__c= OLC.Suite_Sqft__c;
          insert LRSC;
      
          LC_OptionSchedule__c LRSC0= new LC_OptionSchedule__c();
          LRSC0.Lease_Opportunity__c=OLC.id;
          LRSC0.Amount__c=200;
          LRSC0.Option_Type__c='Option-1';
          LRSC0.sqft__c= OLC.Suite_Sqft__c;
          LRSC0.From_In_Months__c='1';
          LRSC0.To_In_Months__c='2';
          insert LRSC0;
          
          
        LC_OptionSchedule__c  LRSC00= new  LC_OptionSchedule__c();
          LRSC00.Lease_Opportunity__c=OLC.id;
          LRSC00.Amount__c=250;
          LRSC00.sqft__c= OLC.Suite_Sqft__c;
          LRSC00.Option_Type__c='Option-1';
          LRSC00.From_In_Months__c='3';
          LRSC00.To_In_Months__c='4';
          insert LRSC00;
 
           LC_OptionSchedule__c LRSC01= new  LC_OptionSchedule__c();
          LRSC01.Lease_Opportunity__c=OLC.id;
          LRSC01.Option_Type__c='Option-1';
          LRSC01.sqft__c= OLC.Suite_Sqft__c;
          LRSC01.Amount__c=250;
         // LRSC01.Increase_in_rent__c=23;
          LRSC01.From_In_Months__c='5';
          LRSC01.To_In_Months__c='6';
          insert LRSC01;
        test.stoptest();  
      
            LRSC.Amount__c=100;
          LRSC.sqft__c= OLC.Suite_Sqft__c;
          update LRSC;
           mvc.Helper__c =true;
          update mvc;
          
         
           LRSC0.Amount__c=250;  
           LRSC0.sqft__c= OLC.Suite_Sqft__c; 
                
           update LRSC0;
           mvc.Helper__c =true;
          update mvc;
          
            LRSC00.Amount__c=300;  
           LRSC00.sqft__c= OLC.Suite_Sqft__c;      
           update LRSC0;
           mvc.Helper__c =true;
          update mvc;
           
         }
         static testmethod void testunit01()
    {
         test.starttest();
         Entity__c ent = new Entity__c();
         ent.name=' Test entity1';
         ent.Owned_ID__c = 'P001';
         insert ent;
         
         MRI_PROPERTY__c MRC= new MRI_PROPERTY__c();
         MRC.Property_ID__c='A1234';
         MRC.name='Test_MRI_Property';
         MRC.Enity_Id__c = 'P001';
         insert MRC; 
          System.assertNotEquals(ent.name,MRC.name);
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU';
         mylease.Suite_Sqft__c=1234;
         mylease.Lease_ID__c='A046677';
         myLease.MRI_PROPERTY__c=MRC.id;
         insert mylease;
         
         LC_Helper_for_Grid__c mvc=new LC_Helper_for_Grid__c();
         mvc.name='Update Now';
         insert  mvc;
  
          LC_LeaseOpprtunity__c OLC=new LC_LeaseOpprtunity__c();
          OLC.name='OptionTypeCheckTest';
          OLC.Lease_Opportunity_Type__c='Lease - New';
          OLC.MRI_Property__c=MRC.id;
          OLC.Lease__c =mylease.id;
          OLC.Suite_Sqft__c=mylease.Suite_Sqft__c;
          OLC.Base_Rent__c=Null;
          insert OLC;
             
           LC_OptionSchedule__c LRSC= new  LC_OptionSchedule__c();
          LRSC.Lease_Opportunity__c=OLC.id;
          LRSC.Option_Type__c='Option-1';
          LRSC.Amount__c=100;
          LRSC.sqft__c= OLC.Suite_Sqft__c;
          insert LRSC;
          
           LC_OptionSchedule__c LRSC00= new  LC_OptionSchedule__c();
          LRSC00.Lease_Opportunity__c=OLC.id;
          LRSC00.Amount__c=250;
          LRSC00.sqft__c= OLC.Suite_Sqft__c;
          LRSC00.Option_Type__c='Option-1';
          LRSC00.From_In_Months__c='3';
          LRSC00.To_In_Months__c='4';
          insert LRSC00;

       
       test.stoptest();
          mvc.Helper__c =False;
           update mvc;
            LRSC.Amount__c=0;
          LRSC.sqft__c= OLC.Suite_Sqft__c;
          update LRSC;
           mvc.Helper__c =true;
          update mvc;
        
        
         }
         
          static testmethod void testunit2()
    {
     
       test.starttest();
       Entity__c ent = new Entity__c();
         ent.name=' Test entity1';
         ent.Owned_ID__c = 'P001';
         insert ent;
         
         MRI_PROPERTY__c MRC= new MRI_PROPERTY__c();
         MRC.Property_ID__c='A1234';
         MRC.name='Test_MRI_Property';
         MRC.Enity_Id__c = 'P001';
         insert MRC; 
          System.assertNotEquals(ent.name,MRC.name);
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU';
         mylease.Suite_Sqft__c=1234;
         mylease.Lease_ID__c='A046677';
         myLease.MRI_PROPERTY__c=MRC.id;
         insert mylease;
         
         LC_Helper_for_Grid__c mvc=new LC_Helper_for_Grid__c();
         mvc.name='Update Now';
         insert  mvc;
  
          LC_LeaseOpprtunity__c OLC=new LC_LeaseOpprtunity__c();
          OLC.name='OptionTypeCheckTest';
          OLC.Lease_Opportunity_Type__c='Lease - New';
          OLC.MRI_Property__c=MRC.id;
          OLC.Lease__c =mylease.id;
          OLC.Suite_Sqft__c=mylease.Suite_Sqft__c;
          OLC.Base_Rent__c=100;
          insert OLC;
             
           LC_OptionSchedule__c LRSC= new  LC_OptionSchedule__c();
          LRSC.Lease_Opportunity__c=OLC.id;
          LRSC.Option_Type__c='Option-1';
          LRSC.Amount__c=OLC.Base_Rent__c;
          LRSC.sqft__c= OLC.Suite_Sqft__c;
          insert LRSC;
      
           LC_OptionSchedule__c LRSC0= new  LC_OptionSchedule__c();
          LRSC0.Lease_Opportunity__c=OLC.id;
          LRSC0.Amount__c=200;
          LRSC0.Option_Type__c='Option-1';
          LRSC0.sqft__c= OLC.Suite_Sqft__c;
          LRSC0.From_In_Months__c='1';
          LRSC0.To_In_Months__c='2';
          insert LRSC0;
          
          
           LC_OptionSchedule__c LRSC00= new  LC_OptionSchedule__c();
          LRSC00.Lease_Opportunity__c=OLC.id;
          LRSC00.Amount__c=250;
          LRSC00.sqft__c= OLC.Suite_Sqft__c;
          LRSC00.Option_Type__c='Option-1';
          LRSC00.From_In_Months__c='3';
          LRSC00.To_In_Months__c='4';
          insert LRSC00;
 
           LC_OptionSchedule__c LRSC01= new  LC_OptionSchedule__c();
          LRSC01.Lease_Opportunity__c=OLC.id;
          LRSC01.Option_Type__c='Option-1';
          LRSC01.sqft__c= OLC.Suite_Sqft__c;
          LRSC01.Increase_in_rent__c=23;
          LRSC01.From_In_Months__c='5';
          LRSC01.To_In_Months__c='6';
          insert LRSC01;
          
        test.stoptest(); 
         
           
          LRSC.Amount__c=0;
          LRSC.sqft__c= OLC.Suite_Sqft__c;
          update LRSC;
           mvc.Helper__c =true;
          update mvc;
        
            LRSC.Amount__c=20;
          LRSC.sqft__c= OLC.Suite_Sqft__c;
          update LRSC;
           mvc.Helper__c =true;
          update mvc;
           
           LRSC.Amount__c=0;
          LRSC.sqft__c= OLC.Suite_Sqft__c;
          update LRSC;
           mvc.Helper__c =true;
          update mvc;
          
           LRSC00.Amount__c=250;  
           LRSC00.sqft__c= OLC.Suite_Sqft__c;      
           update LRSC00;
           mvc.Helper__c =true;
          update mvc;
          
           LRSC01.Amount__c=250;  
           LRSC01.sqft__c= OLC.Suite_Sqft__c;      
           update LRSC01;
           mvc.Helper__c =true;
          update mvc;
          
        
        
          
    }
    
}