@isTest
private class RC_New_ClaimButtonClassTest
{
    static testmethod void test1() 
    {
       RC_Claim__c  mp=new RC_Claim__c ();
       RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'RC_Claim__c' and DeveloperName = 'PROP']; 
       mp.RecordTypeId=NewRecType.id;
       String Url=System.Label.Current_Org_Url+'/a5C/e?nooverride=1&retURL=%2Fa5C%2Fo&RecordType='+mp.RecordTypeId+'&ent=01IR00000005';
       PageReference acctPage = new PageReference(Url);
       Test.setCurrentPage(acctPage); 
       ApexPages.StandardController controller = new ApexPages.StandardController(mp);
       RC_New_ClaimButtonClass csr = new RC_New_ClaimButtonClass(controller);
       
       csr.Gobacktostandardpage();
       
            
        RC_Property__c PRT=new RC_Property__c();
         PRT.name='Amazon DC 2014 USAA~A11123';
         PRT.City__c='Hyderabad';
         insert PRT;
         
         Account a= new Account();
         a.name='TestAccount';
         insert a;
        
         
         RC_Location__c LC=new RC_Location__c();
         LC.name='Amazon DC 2014 USAA~A11123';
         LC.Entity_Tenant_Code__c='12345';
         //LC.City__c='Hyderabad';
         LC.Property__c=PRT.id;
         LC.Account__c=a.id;
         insert LC;
            
        System.assertEquals(PRT.name,LC.name);

         string bid=LC.id;
         string bid1=LC.Name;
         string accountid=LC.Account__c;
         String accountName=LC.Account__r.name;
         //accountName=accountName.replaceAll('&','%26');
         String s2 = bid1.replaceAll('&', '%26');
         RC_Claim__c ct1=new RC_Claim__c();
         ct1.RecordTypeId=NewRecType.id;
         String Url1=System.Label.Current_Org_Url+'/a5D/e?nooverride=1&CF00NR0000001JRV9_lkold='+bid+'&CF00NR0000001JRV9='+s2+'&CF00NR0000001JRST_lkold='+accountid+'&CF00NR0000001JRST='+accountName+'&retURL=%2F'+LC.id+'&RecordType='+ct1.RecordTypeId+'&ent=01IR00000005onJ';
  
       
        ApexPages.StandardController controller1 = new ApexPages.StandardController(ct1);
       RC_New_ClaimButtonClass csr1=new RC_New_ClaimButtonClass(controller1);
       csr1.ppc=LC;
       csr1.mp=ct1;
        PageReference acctPage1 = new PageReference(Url1);
       Test.setCurrentPage(acctPage1); 
       csr1.mp.Location__c=LC.id;
       csr1.Gobacktostandardpage();
    
    
    
    }
    
    
    
}