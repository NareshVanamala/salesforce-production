Public with sharing class PropertystatusController
{

  public String labelMap { get; set; }
  Id dealid;
  public list<Task>Tasks;
  //Deal__c mydeal{get;set;}
 public  PropertystatusController() 
 {
  dealid=ApexPages.currentPage().getParameters().get('id');
  system.debug('check the id..'+dealid);
  mydeal=[select Lease_Abstract_Notes__c,Lease_Abstract_Date__c ,Site_Visit_By__c,Site_Visit_Date__c,Site_Visit_By__r.name,Lease_Abstract_By__r.name,Site_Visit_Notes__c,id,Name,Fund__c,Paralegal__r.name,Contract_Price__c,Attorney__r.name,Deal_Status__c from Deal__c where id=:dealid];
  system.debug('check the mydeal..'+mydeal);
 }

 public Deal__c mydeal
 { 
  get
 {     
 if (mydeal == null) 
   mydeal= new Deal__c ();   
   return mydeal; 
  }   
set;  
 }

    
    public List<Task>getTasks() {
    Tasks=[select id,whatid,status, subject,Date_Ordered__c,ActivityDate,Description,Date_Received__c from Task where whatid=:mydeal.id order by subject ASC];
        //return SObjectType.Deal__c.FieldSets.Purchase_Report_Property_Status.getFields();
       // System.debug('*****' + Tasks[0].Date_Ordered__c + Tasks[0].Date_Received__c + Tasks[0].subject);
    return Tasks;
    
    }
    
   /* private Deal__c getdeal() {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id, Name,Deal_Status__c FROM Deal__c LIMIT 1';
        return Database.query(query);
    }*/
}