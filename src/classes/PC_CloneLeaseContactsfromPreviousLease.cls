public with sharing class PC_CloneLeaseContactsfromPreviousLease
{
    @InvocableMethod
    
    public static void CloneLeaseContacts(list<string>Lseid)
    {
        String LeaseId;
        if(Lseid.size()>0)
        LeaseId=Lseid[0];
        Lease__c ObjLease = [select id,Previous_Lease__c from LEase__c where Id=:LeaseId];
        List<Property_Contact_Leases__c> OldPCList = new list<Property_Contact_Leases__c>();
        OldPCList = [select id,Building_ID__c,Common_Name__c,Contact_Type__c,External_ID__c,Lease__c,Lease__r.MasterOccupant_ID__c,Lease_ID__c,Lease_Status__c,Local_Tenant_Concept__c,Local_tenant_Name__c,Property_Contacts__c,Property_ID__c from Property_Contact_Leases__c where Lease__c=:ObjLease.Previous_Lease__c] ;
        List< Property_Contact_Leases__c>  NewPCList  = new List<Property_Contact_Leases__c>();
        for(Property_Contact_Leases__c a: OldPCList )
        {
            Property_Contact_Leases__c att = new Property_Contact_Leases__c(Property_Contacts__c= a.Property_Contacts__c,Contact_Type__c=a.Contact_Type__c, Lease__c = ObjLease.id);
            NewPCList  .add(att);
        }
        if(NewPCList.size() > 0)
        {
            insert NewPCList;
        }
    }
}