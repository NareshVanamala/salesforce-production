@isTest (SeeAllData=true)
private class PropertyforLeaseDetail_Test
{
    
    @isTest static void test_getHTML() {

        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        
        Web_Properties__c wp=new Web_Properties__c();
        wp.name='test';
        wp.Long_Description__c='hi';
        wp.Tenant_Profile__c='hii';
        wp.Property_Type__c='hi';
        wp.sector__c='hi';
        wp.ProgramName__c='lowe';
        wp.Date_Acquired__c=system.today();
        wp.sqFt__c=2000;
        wp.Related_Resources__c='test';
        wp.status__c='approved for tenant';
        wp.Gross_Leaseable_Area__c=1800;
        wp.ImagesforLease__c='test';
        wp.Web_Ready__c='approved';
        wp.lease_status__c='sold';
        wp.Address__c = '1600 Amphitheatre Parkway';
        wp.City__c = 'Mountain View';
        wp.State__c    = 'CA';
        wp.Country__c ='USA';
        wp.Brochure__c ='test';
        wp.Aerial__c ='test';
        wp.Demographics__c ='test';
        wp.Main_Image__c='test';
        wp.Image1__c ='test';
        wp.Image2__c ='test';
        wp.Image3__c = 'test';
        wp.Image4__c = 'test';
        wp.Image5__c = 'test'; 
        try{
        insert wp;

        
        System.currentPageReference().getParameters().put('propertyId', wp.id); //Modify this value to point to a web_property__c record with values.
        
       
        PropertyforLeaseDetail detail = new PropertyforLeaseDetail ();
        detail.getHTML();} 
        catch(exception e){}
         system.assert(wp.Date_Acquired__c == system.today());

    }
    
    @isTest static void test_getHTML1() {

        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        
        Web_Properties__c wp=new Web_Properties__c();
        wp.name='test';
        wp.Long_Description__c='';
        wp.Tenant_Profile__c='';
        wp.Property_Type__c='hi';
        wp.sector__c='hi';
        wp.ProgramName__c='lowe';
        wp.Date_Acquired__c=system.today();
        wp.sqFt__c=2000;
        wp.Related_Resources__c='test';
        wp.status__c='approved for tenant';
        wp.Gross_Leaseable_Area__c=1800;
        wp.ImagesforLease__c='test';
        wp.Web_Ready__c='approved';
        wp.lease_status__c='sold';
        wp.Address__c = '1600 Amphitheatre Parkway';
        wp.City__c = 'Mountain View';
        wp.State__c    = 'CA';
        wp.Country__c ='USA';
        wp.Brochure__c ='test';
        wp.Aerial__c ='test';
        wp.Demographics__c ='test';
        wp.Main_Image__c='test';
        wp.Image1__c ='test';
        wp.Image2__c ='test';
        wp.Image3__c = 'test';
        wp.Image4__c = 'test';
        wp.Image5__c = 'test'; 
        try{
        insert wp;
                
        System.currentPageReference().getParameters().put('propertyId', wp.id); //Modify this value to point to a web_property__c record with values.

        PropertyforLeaseDetail detail = new PropertyforLeaseDetail ();
        detail.getHTML();} catch(exception e){}
        
        system.assert(wp.Date_Acquired__c == system.today());

    }
    
}