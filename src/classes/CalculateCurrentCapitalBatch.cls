global class CalculateCurrentCapitalBatch implements Database.Batchable<SObject>
{
global Database.QueryLocator start(Database.BatchableContext BC)  
{
    String Query;
    system.debug('This is start method');  
    Query = 'SELECT Id,Rep_Contact__c, Deposit_Date__c, Current_Capital__c, Fund__c, Original_Capital__c FROM REIT_Investment__c WHERE Deposit_Date__c = THIS_YEAR' ;
    system.debug('Myquery is......' + Query);
    return Database.getQueryLocator(Query);
}
global void execute(Database.BatchableContext BC, List<REIT_Investment__c> scope)  
{
   Map<Id,List<REIT_Investment__c>> conREITS = new Map<Id,List<REIT_Investment__c>>();
   List<Contact> conlist = new List<Contact>();
   Map<Id,Contact> contactMap = new Map<Id,Contact>();
   
   for(REIT_Investment__c ri : scope)
    {
        if(conREITS.containsKey(ri.Rep_Contact__c))
        {
            conREITS.get(ri.Rep_Contact__c).add(ri);
        }
        else
        {
            conREITS.put(ri.Rep_Contact__c, new List<REIT_Investment__c>{ri});
        }
    } 
    
    conlist = [select id, Year_To_Date__c from Contact WHERE Id IN :conREITS.keyset()]; 
    
    for(Contact n : conlist)
    {
        contactMap.put(n.Id, n);
    } 
    date firstdaythisyear = date.newInstance(Date.today().Year(),1,1);
    date lastdaythisyear = date.newInstance(Date.today().Year(),12,31);  
    
     for(Id rep : conREITS.keyset())
     {
       decimal currentcapitalthisyear= 0;
       
      if(contactMap.containsKey(rep))
        {
         for(REIT_Investment__c rct : conREITS.get(rep))
          {
           if((rct.Deposit_Date__c >= firstdaythisyear) && (rct.Deposit_Date__c<=lastdaythisyear))
             {
               currentcapitalthisyear += rct.Current_Capital__c;
             }
             System.debug('****************' + currentcapitalthisyear);
          }
          
          if(contactMap.get(rep).Year_To_Date__c == null)
          {
            contactMap.get(rep).Year_To_Date__c = 0;
            
          }
          contactMap.get(rep).Year_To_Date__c += currentcapitalthisyear; 
     }
     
    }
    update contactMap.values();
 }
global void finish(Database.BatchableContext BC)  
{
   
}
}