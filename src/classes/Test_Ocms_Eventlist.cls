@isTest
public class Test_Ocms_Eventlist
{
    public static testmethod void Test_Ocms_Eventlist() 
    { 
        profile pf=[select id,name from profile where name='Cole Capital Community'];
              
        account a=new account();
        a.name='test';
        insert a;
        
        Contact C= new Contact();
        C.firstName='Ninad';
        C.lastName='Tambe';
        c.accountid=a.id;
        insert C;
        
        User u = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = pf.id, country='United States',IsActive =true,
                ContactId = c.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert u;
        
        System.assertEquals(u.profileid,pf.id);
        
        Campaign cmp= new Campaign ();
        cmp.name='Test-Webcast';
        cmp.IsActive=True;
        cmp.Status='Completed';
        cmp.Type='CE Event';
        insert cmp;
        
        Campaign cmp1= new Campaign ();
        cmp1.name='Test-Webcast';
        cmp1.IsActive=True;
        cmp1.Status='In Process';
        cmp1.Type='CE Event';
        insert cmp1;
        
        list<CampaignMember> clist=new list<CampaignMember>();
       for(integer i=0;i<1;i++) {
        CampaignMember cp=new CampaignMember();
        cp.CampaignId=cmp.id;
        cp.contactid=u.contactid;
        cp.status='RSVP';
        clist.add(cp);
        }
        insert clist;
        
        CampaignMember cp1=new CampaignMember();
        cp1.CampaignId=cmp1.id;
        cp1.contactid=u.contactid;
        cp1.status='RSVP';
        insert cp1;
        
        
        list<id> ab=new list<id>();
        for(CampaignMember cp11:clist) {
         ab.add(cp11.CampaignId);
        }
        
       
        datetime dt=system.now()+10;
        
        
        list<WebCast__c> wblist=new list<WebCast__c>();
       for(integer i=0;i<1;i++) {
        WebCast__c wb= new WebCast__c();
        wb.name='Test';
        wb.Contacts__c= C.id;
        wb.Campaigns__c= ab[i];
        
        wb.Start_Date_Time__c=dt;
        wb.Description__c='Testing cole capital';
        wb.Status__c='Registered';
        wb.iCalendar_URL__c='hi';
        wb.meeting_ID__c='test';
        wb.name__c='test';
        wb.Viewble_on_website__c=true;
        wb.Webcast_Replay_URL__c=true;
        wb.Meeting_Date_Time__c=dt;
        wb.Email_Address__c='test@test.com';
        wblist.add(wb);
        }
         insert wblist;  
        
    
      system.runas(u)
        {        
     
       ocms_Event_List rc=new ocms_Event_List();
        try
        {
            map<string,string> p=new map<string,string>();
            p.put('action','getUpcomingEventsCount');
            
            list<sobject> ProductList=rc.getProductList(); 
            
            rc.writeMapView();
            rc.executeRequest(p);
            rc.getUpcomingEventsCount();
            rc.gettype();
            rc.getHtml(); 
            rc.writeListView();
            
         }
          catch(Exception e)  {        }       
          }
  }
  
   public static testmethod void Test_Ocms_Eventlist1() 
    { 
        profile pf=[select id,name from profile where name='Cole Capital Community'];
              
        account a=new account();
        a.name='test';
        insert a;
        
        Contact C= new Contact();
        C.firstName='Ninad';
        C.lastName='Tambe';
        c.accountid=a.id;
        insert C;
        
        User u = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = pf.id, country='United States',IsActive =true,
                ContactId = c.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert u;
        
        System.assertEquals(u.profileid,pf.id);

        Campaign cmp= new Campaign ();
        cmp.name='Test-Webcast';
        cmp.IsActive=True;
        cmp.Status='Completed';
        cmp.Type='CE Event';
        insert cmp;
        
        Campaign cmp1= new Campaign ();
        cmp1.name='Test-Webcast';
        cmp1.IsActive=True;
        cmp1.Status='In Process';
        cmp1.Type='CE Event';
        insert cmp1;
        
        list<CampaignMember> clist=new list<CampaignMember>();
       for(integer i=0;i<1;i++) {
        CampaignMember cp=new CampaignMember();
        cp.CampaignId=cmp.id;
        cp.contactid=u.contactid;
        cp.status='Completed';
        clist.add(cp);
        }
        insert clist;
        
        CampaignMember cp1=new CampaignMember();
        cp1.CampaignId=cmp1.id;
        cp1.contactid=u.contactid;
        cp1.status='Completed';
        insert cp1;
        
        
        list<id> ab=new list<id>();
        for(CampaignMember cp11:clist) {
         ab.add(cp11.CampaignId);
        }
        
       
        datetime dt=system.now()+10;
        
        
        list<WebCast__c> wblist=new list<WebCast__c>();
       for(integer i=0;i<1;i++) {
        WebCast__c wb= new WebCast__c();
        wb.name='Test';
        wb.Contacts__c= C.id;
        wb.Campaigns__c= ab[i];
        
        wb.Start_Date_Time__c=dt;
        wb.Description__c='Testing cole capital';
        wb.Status__c='Registered';
        wb.iCalendar_URL__c='hi';
        wb.meeting_ID__c='test';
        wb.name__c='test';
        wb.Viewble_on_website__c=true;
        wb.Webcast_Replay_URL__c=true;
        wb.Meeting_Date_Time__c=dt;
        wb.Email_Address__c='test@test.com';
        wblist.add(wb);
        }
         insert wblist;  
        
       system.runas(u)
        {        
     
       ocms_Event_List rc=new ocms_Event_List();
        try
        {
            map<string,string> p=new map<string,string>();
            p.put('action','getUpcomingEventsCount');
            
            list<sobject> ProductList=rc.getProductList(); 
            
            rc.writeMapView();
            rc.executeRequest(p);
            rc.getUpcomingEventsCount();
            rc.gettype();
            rc.getHtml(); 
            rc.writeListView();
            
         }
          catch(Exception e)  {        }       
          }
  }
}