@isTest
public class Cockpit_GenerateCSV_Test  
{
    
   static testmethod void myTestMethod(){

     Test.StartTest();       
    // System.assert(cl != null);
    
     Contact c = new Contact();
     c.Firstname='Test';
     c.Lastname='record';
     insert c;
     
     System.assert(c != null);
     
     Campaign cn = new Campaign();
     cn.name='Blast Email';
     insert cn;
     
     System.assert(cn != null);
 
     CampaignMember cr = new CampaignMember();
     cr.campaignid = cn.id;
     cr.contactid = c.id;
     insert cr;
     
     System.assert(cr != null);
  
     
     Automated_Call_List__c ACL = new Automated_Call_List__c();
     ACL.name ='TestingList';
     ACL.Campaign__c =cn.id;
     ACL.Dynamic_Query_Filters__c='Select Status&&&&';
     //ACL.Last_Activity_Date__c=Date.today()-30;
     ACL.Archived__c=true;
     insert ACL;
     
     System.assert(ACL != null);
     
     ACL.TempLock__c = 'General Lock';
     update ACL;
    
     ACL.TempLock__c = '';
     ACL.Internal_Flag__c  =true;
     update ACL; 
  
     
     contact_call_list__c clist = new contact_call_list__c();
     clist.Call_Complete__c = false;
     clist.Automated_Call_List__c = ACL.id;
     clist.CallList_Name__c = ACL.Name;
     clist.Description__c = 'Testing';
     clist.ContactCalllist_UniqueConbination__c=ACL.Name+c.Id;
     clist.Contact__c = c.id;
     //clist.Archived__c=false;
     //clist.campaign__c = cn.id;
     insert clist;
     
     System.assert(clist != null);   
      
     List<contact_call_list__c> scope = new List<contact_call_list__c>();
     scope.add(clist);    
      
     Cockpit_GenerateCSV  batch = new Cockpit_GenerateCSV(ACL.id);
     Id batchId = Database.executeBatch(batch);
     Database.BatchableContext BC;
     batch.execute(BC,scope);
    

  }
}