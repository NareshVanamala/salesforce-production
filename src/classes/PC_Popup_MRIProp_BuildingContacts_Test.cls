@isTest(seeAlldata =true)
public class PC_Popup_MRIProp_BuildingContacts_Test{
    static testMethod void testmethod1(){
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
        Account acc = new Account (
        Name = 'newAcc1'
        );  
        insert acc;
        Contact con = new Contact (
        AccountId = acc.id,
        LastName = 'portalTestUser'
        );
        insert con;
        Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
         
        User newUser = new User(
        profileId = p.id,
        username = 'newUser0615201720511@arcpreit.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = con.id
        );
        insert newUser;
        
        MRI_Property__c mrpObj = new MRI_Property__c();
        mrpObj.Name = 'testmrp';
        mrpObj.Property_Manager__c = newUser.id;
        mrpObj.Fund1__c = 'ARCP';
        mrpObj.Date_Acquired__c = Date.today();
        mrpObj.Common_Name__c = 'cname';
        mrpObj.Property_Id__c = '1234';
        insert mrpObj;
        
        list<Lease__c> leaseLst = new list<Lease__c>();
        
        Lease__c lObj1 = new Lease__c();
        lObj1.MRI_Property__c = mrpObj.Id;
        lObj1.Tenant_Name__c = 'testTenent1';
        lObj1.Lease_Status__c = 'Active';
        leaseLst.add(lObj1);
        
        Lease__c lObj2 = new Lease__c();
        lObj2.MRI_Property__c = mrpObj.Id;
        lObj2.Tenant_Name__c = 'testTenent1';
        lObj2.Lease_Status__c = 'Active';
        leaseLst.add(lObj2);
        
        
        Lease__c lObj3 = new Lease__c();
        lObj3.MRI_Property__c = mrpObj.Id;
        lObj3.Tenant_Name__c = 'testTenent1';
        lObj3.Lease_Status__c = 'Active';
        leaseLst.add(lObj3);
        
        insert leaseLst;
        
        Property_Contacts__c pcObj1 = new Property_Contacts__c();
        pcObj1.Name = 'test1';
      //  pcObj1.Lease__c = lObj1.Id;
        pcObj1.MRI_Property__c = mrpObj.Id;
        insert pcObj1;
        
        Property_Contacts__c pcObj2 = new Property_Contacts__c();
        pcObj2.Name = 'test2';
        //pcObj2.Lease__c = lObj2.Id;
        pcObj2.MRI_Property__c = mrpObj.Id;
        insert pcObj2;
        
       Property_Contact_Buildings__c pclObj1 = new Property_Contact_Buildings__c();
        pclObj1.Center_Name__c= mrpObj.Id;
        pclObj1.Property_Contacts__c = pcObj1.Id;
        pclObj1.Contact_Type__c = 'Dining Services;Electricity;Elevators;Facilities;HVAC;Janitorial;';
        insert pclObj1;
        
        Property_Contact_Buildings__c pclObj2 = new Property_Contact_Buildings__c();
        pclObj2.Center_Name__c= mrpObj.Id;
        pclObj2.Property_Contacts__c = pcObj2.Id;
        pclObj2.Contact_Type__c = 'Dining Services;Electricity;Elevators;Facilities;HVAC;Janitorial;';
        insert pclObj2;
        
        ApexPages.currentPage().getParameters().put('cid', mrpObj.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(mrpObj);
        PC_Popup_MRIProp_BuildingContacts classObj = new PC_Popup_MRIProp_BuildingContacts(sc);
        List<PC_Popup_MRIProp_BuildingContacts.cLease> cLeaselist = new List<PC_Popup_MRIProp_BuildingContacts.cLease>();
        cLeaselist.add(new PC_Popup_MRIProp_BuildingContacts.cLease(mrpObj));
       // cLeaselist.add(new PC_Popup_MRIProp_BuildingContacts.cLease(lObj2));
        classObj.leaseList = cLeaselist;
        List<PC_Popup_MRIProp_BuildingContacts.mywrapper> mywrapperlist = new List<PC_Popup_MRIProp_BuildingContacts.mywrapper>();
        mywrapperlist.add(new PC_Popup_MRIProp_BuildingContacts.mywrapper(pclObj1));
       /*  mywrapperlist.add(new PC_Popup_MRIProp_BuildingContacts.mywrapper(pclObj2)); */
        classObj.getmylist();
        classObj.getmassapplylist();
        classObj.closePopup();
        for(PC_Popup_MRIProp_BuildingContacts.cContact ct:classObj.PropertyContactList)
              {
                 ct.selected=true;
                  
              }
        classObj.ApplyPropertyContacts();
        classObj.createNewRecord();
        classObj.enable();
        classObj.getContacts();
        classObj.getleaseList();
        classObj.getmassapplylist();
        classObj.getmylist();
        classObj.getSelected();
        classObj.getshownewcon();
        classObj.getSnoozeradio();
        classObj.massapply();
        //classObj.getmassapplylist();
        classObj.Save1();
        for(PC_Popup_MRIProp_BuildingContacts.cLease llist:classObj.leaseList){
            llist.selected=true;
        }
        classObj.prconl.Contact_Type__c = 'Dining Services;Electricity';
        classObj.Save();
        classObj.Save1();
        for(PC_Popup_MRIProp_BuildingContacts.cContact ct: classObj.PropertyContactList){
                 ct.selected=true;
                  
        }
        PC_Popup_MRIProp_BuildingContacts.cContact pc = new PC_Popup_MRIProp_BuildingContacts.cContact(pcObj1);
        pc.selected = true;
        classObj.selectedPropertycontactList.add(pc);
        classObj.SearchRecord();
        classObj.showPopup();
      //**************added by snehal***************//
      PC_Popup_MRIProp_BuildingContacts classObj1 = new PC_Popup_MRIProp_BuildingContacts(sc);
        List<PC_Popup_MRIProp_BuildingContacts.cLease> cLeaselist1 = new List<PC_Popup_MRIProp_BuildingContacts.cLease>();
        cLeaselist.add(new PC_Popup_MRIProp_BuildingContacts.cLease(mrpObj));
        //cLeaselist.add(new PC_Popup_MRIProp_BuildingContacts.cLease(lObj2));
        classObj1.leaseList = cLeaselist;
        List<PC_Popup_MRIProp_BuildingContacts.mywrapper> mywrapperlist1 = new List<PC_Popup_MRIProp_BuildingContacts.mywrapper>();
        cLeaselist.add(new PC_Popup_MRIProp_BuildingContacts.cLease(mrpObj));
       /*  mywrapperlist.add(new PC_Popup_MRIProp_BuildingContacts.mywrapper(pclObj2)); */
       
        classObj1.getmylist();
        classObj1.getmassapplylist();
        classObj1.closePopup();
        for(PC_Popup_MRIProp_BuildingContacts.cContact ct:classObj1.PropertyContactList)
              {
                 ct.selected=true;
                  
              }
        classObj1.ApplyPropertyContacts();
        classObj1.createNewRecord();
        classObj1.enable();
        classObj1.getContacts();
        classObj1.getleaseList();
        classObj1.getmassapplylist();
        classObj1.getmylist();
        classObj1.getSelected();
        classObj1.getshownewcon();
        classObj1.getSnoozeradio();
        classObj1.massapply();
        //classObj.getmassapplylist();
        classObj1.Save1();
        for(PC_Popup_MRIProp_BuildingContacts.cLease llist:classObj1.leaseList){
            llist.selected=true;
        }
        classObj1.prconl.Contact_Type__c = 'Electricity';
        classObj1.Save();
        classObj1.Save1();
        for(PC_Popup_MRIProp_BuildingContacts.cContact ct: classObj.PropertyContactList){
                 ct.selected=true;
                  
        }
        //PC_Popup_MRIProp_BuildingContacts.cContact pc = new PC_Popup_MRIProp_BuildingContacts.cContact(pcObj1);
        pc.selected = true;
        classObj1.selectedPropertycontactList.add(pc);
        classObj1.SearchRecord();
        classObj1.showPopup();  
        
  //***************this is second  try******************      
        Property_Contact_Buildings__c pclObj11 = new Property_Contact_Buildings__c ();
        pclObj11.center_Name__c= mrpObj.Id;
        pclObj11.Property_Contacts__c = pcObj1.Id;
        pclObj11.Contact_Type__c = 'Dining Services;Electricity;Elevators;Facilities;HVAC;Janitorial;';
        insert pclObj11;
        
        
         PC_Popup_MRIProp_BuildingContacts classObj11 = new PC_Popup_MRIProp_BuildingContacts(sc);
       List<PC_Popup_MRIProp_BuildingContacts.cLease> cLeaselist2 = new List<PC_Popup_MRIProp_BuildingContacts.cLease>();
        cLeaselist.add(new PC_Popup_MRIProp_BuildingContacts.cLease(mrpObj));
        //cLeaselist.add(new PC_Popup_MRIProp_BuildingContacts.cLease(lObj2));
        classObj11.ShowJustContactType =True;
        classObj11.showContactType=True;
        classObj11.leaseList = cLeaselist;
        classObj11.processSelectedcons();
        List<PC_Popup_MRIProp_BuildingContacts.mywrapper> mywrapperlist2 = new List<PC_Popup_MRIProp_BuildingContacts.mywrapper>();
       mywrapperlist1.add(new PC_Popup_MRIProp_BuildingContacts.mywrapper(pclObj11));
       /*  mywrapperlist.add(new PC_Popup_MRIProp_BuildingContacts.mywrapper(pclObj2)); */
       
        classObj11.getmylist();
        classObj11.getmassapplylist();
        classObj11.closePopup();
        for(PC_Popup_MRIProp_BuildingContacts.cContact ct:classObj11.PropertyContactList)
              {
                 ct.selected=true;
                  
              }
        classObj11.ApplyPropertyContacts();
        classObj11.createNewRecord();
        classObj11.enable();
        classObj11.getContacts();
        classObj11.getleaseList();
        classObj11.getmassapplylist();
        classObj11.getmylist();
        classObj11.getSelected();
        classObj11.getshownewcon();
        classObj11.getSnoozeradio();
        classObj11.massapply();
        //classObj.getmassapplylist();
        classObj11.Save1();
        for(PC_Popup_MRIProp_BuildingContacts.cLease llist:classObj11.leaseList){
            llist.selected=true;
        }
        classObj11.prconl.Contact_Type__c = 'Dining Services';
        classObj11.Save();
        classObj11.Save1();
        for(PC_Popup_MRIProp_BuildingContacts.cContact ct: classObj11.PropertyContactList){
                 ct.selected=true;
                  
         }
        PC_Popup_MRIProp_BuildingContacts.cContact pc1 = new PC_Popup_MRIProp_BuildingContacts.cContact(pcObj1);
        
        pc.selected = true;
        classObj11.selectedPropertycontactList.add(pc);
        classObj11.SearchRecord();
        classObj11.showPopup();  
        
        
        
        
        
    }
     
}