global virtual without sharing class ocms_WebPropertyDetail extends cms.ContentTemplateController{
    
    GetURL getURLObj = new GetURL();

    public ocms_WebPropertyDetail() {
        
    }

    global override virtual String getHTML(){

        SObject property;
        String html;
        String propertyID = System.currentPageReference().getParameters().get('propertyId');

        String query = 'SELECT Name,Common_Name__C,Web_Name__c, Address__c, City__c, State__c, Zip_Code__c, Date_Acquired__c, Property_Type__c, Sector__c, sqFt__c, ProgramName__c, Long_Description__c, Location__c, Location__latitude__s, Location__longitude__s, Tenant_Profile__c, Photo__c FROM Web_Properties__c WHERE Id=\'' + propertyID + '\' LIMIT 1';

        String overview;
        String tenantProfile;
        String title;
        DateTime acquired;

        property = Database.query(query);

        Date d = (Date) property.get('Date_Acquired__c');
        acquired = (DateTime) d;

        if(property.get('Long_Description__c') != null){
            overview = String.valueOf(property.get('Long_Description__c'));
        } else {
            overview = '';
        }
        title= String.valueOf(property.get('Web_Name__c'));

        html =  '   <div class="ocms_WebPropertyDetail">' +
                '       <input type="hidden" id="latitude" value="' + property.get('Location__latitude__s') + '" /> ' +
                '       <input type="hidden" id="longitude" value="' + property.get('Location__longitude__s') + '" /> ' +
                '       <div class="ocms_title">' + title + '</div>' +
                '       <div class="ocms_infoSection">' +
                '           <div class="ocms_address">' +
                '               <div class="ocms_streetAddress">' + property.get('Address__c') + '</div>' +
                '               <div class="ocms_cityStateZip">' + property.get('City__c') + ', ' + property.get('State__c') + ' ' + property.get('Zip_Code__c') + '</div>' +
                '           </div>' +
                '           <table class="ocms_propertyData">' +
                '               <tr>' +
                '                   <td>Acquired:</td>' +
                '                   <td>' + acquired.format('MMMM d, Y') + '</td>' +
                '               </tr>' +
                '               <tr>' +
                '                   <td>Property Type:</td>' +
                '                   <td>' + property.get('Property_Type__c') + '</td>' +
                '               </tr>' +
                '               <tr>' +
                '                   <td>Industry:</td>' +
                '                   <td>' + property.get('Sector__c') + '</td>' +
                '               </tr>' +
                '               <tr>' +
                '                   <td>Square Feet:</td>' +
                '                   <td>' + property.get('sqFt__c') + '</td>' +
                '               </tr>' +
                '               <tr>' +
                '                   <td>Portfolio:</td>' +
                '                   <td>' + property.get('ProgramName__c') + '</td>' +
                '               </tr>' +
                '           </table>' +
                '           <div class="ocms_photo"><img src="' + property.get('Photo__c') + '" /></div>' +
                '       </div>' +
                '       <div class="ocms_controls">' ;
                
                if(property.get('Long_Description__c') != null)
                {
                
                html+=' <div class="ocms_overview">Overview</div>';
                }
                
                if(property.get('Tenant_Profile__c') != null)
                {
                html+= '<div class="ocms_tenantProfile">Tenant Profile</div>'; 
                }
                
                html+=' <div class="ocms_map">Map</div>' +
                '       </div>';
                
                if(property.get('Long_Description__c') != null)
                {
                html+='       <div class="ocms_overviewTab" id="overviewtab">' +
                '           <div class="ocms_title">Overview</div>' +
                '           <div class="ocms_body">' + overview + '</div>' +
                '       </div>' ;
                }
        
                if(property.get('Tenant_Profile__c') != null)
                {
                html+='       <div class="ocms_tenantTab" id="tenanttab">' +
                '           <div class="ocms_title">Tenant Profile</div>' +
                '           <div class="ocms_body">' + tenantProfile + '</div>' +
                '       </div>';
                }
        
                html+='       <div class="ocms_mapTab" id="maptab">' +
                '           <div id="map-canvas"></div>' +
                '       </div>' +
                '   </div>' +
                '<script src="/resource/1412950432000/ocms_WebPropertyList/js/ocms_WebPropertyDetail.js" type="text/javascript"></script>';
        
            /*    if(property.get('Long_Description__c') != null)
                {
                html+='<script>$("#maptab").hide();$("#tenanttab").hide();$("#overviewtab").show();</script>';
                }
                else if(property.get('Tenant_Profile__c') == null && property.get('Long_Description__c') == null)
                {
                html+='<script>$("#overviewtab").hide();$("#tenanttab").hide();$("#maptab").show();</script>';
                }*/
        
        return html;
    }

}