@isTest
private class Websites_CCDrupalContactJobTest{
	

	@isTest(SeeAllData=true)
	static void callJob(){
		String body= '{"status": "SUCCESS"}';
    	Test.setMock(HttpCalloutMock.class, new Websites_HttpClass.Mock(body, 200));

		Map<Id, Account> accounts= new Map<Id, Account>([select id from Account limit 2]);
		Set<Id> ids= accounts.keySet();
		List<Id> lid= new List<Id>();
		lid.addAll(ids);
		Websites_CCDrupalContactJob j= new Websites_CCDrupalContactJob(lid);
		j.isFromTestClass=true;
		database.executeBatch(j,200);

	}
}