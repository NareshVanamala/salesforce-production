public with sharing class LC_LETSFormPdfController_Cmp
{


    public String quoteId1{get;set;} 
    public String myValue{get;set;}
    public list<LC_ClauseType__c>cTypeList{get;set;}
   // public list<LC_ClauseType__c>StandardClauseList{get;set;}
    //public list<LC_ClauseType__c>cTypeList1{get;set;}
    //public LC_LeaseOpprtunity__c LseOpty{get;set;}
    //public boolean showRentschedule{set;get;}
    //public boolean showOptionSchedule{set;get;} 
    //public boolean showRevisedSection{set;get;}
    public map<String,String>leaseNametoDesriptionMap{get;set;}
   // public boolean showRentschedule{set;get;}
    public boolean showAddlClauses{set;get;}
    
    
    public LC_LeaseOpprtunity__c LseOptys{get;set;}
   // Public Centralized_Helper_Object__c helperObject{get;set;}
   
   public Centralized_Helper_Object__c  helperObject{get{
       system.debug('>>>>>>>>>>>>>> '+quoteId1);
    
       try
       {
             Centralized_Helper_Object__c  myobj= [select id,LeaseCentral_Approval_Instructions__c,name from Centralized_Helper_Object__c where name=:'Lease Central-I'];
             helperObject=myobj;
           
      }
          Catch(Exception e)
       {
       
       }
         return helperObject;
        
    } set;
    }    
    
    
    
  
     
    public String getquoteId1() 
    {
        system.debug('>>>>>>>>>>>>>> '+quoteId1);
        return quoteId1;
    }
    
     public void setquoteId1 (String s) 
     {
         myValue=s;
     } 
  
       //**********LseOpty*****************************
   public LC_LeaseOpprtunity__c  LseOpty{get{
       system.debug('>>>>>>>>>>>>>> '+quoteId1);
    
       try
       {
             LC_LeaseOpprtunity__c   myCT= [select Average_Option_Rents_over_Renewal_Period__c,Gross_Sales_Reporting__c,Average_base_rent_over_renewal_period__c,id,Name,National_Tenant__c,Actual_NER__c,Add_l_Options__c,Additional_Comments__c,Tenant_Address__c,Alternate_Property_Name__c,Suite_Id__c,Leasing_Commissions__c,Reimbursable_Type__c,Landlord_Broker__c,Tenant_Broker__c,Date_last_tenant_vacated_New__c,
                   Budgeted_NER__c,Building_Id__c,Common_Name__c,Contiguous_Tenant_For_Possible_Expansion__c,Current_Date__c,Current_Expiration_Date__c,Tenant_Email_Contact_Number__c,Lease_Category__c,LC_Already_Submitted__c,Average_Base_Rent_Over_Initial_Term__c ,Prepared_by__c,Deal_Maker__c,Percent_Rent__c,Break_Point__c,Percent_Rent1__c,Leasing_Commissions_PSF__c ,Was_space_purchased_vacant_New__c,Base_Rent_PSF__c,
                   Entity_Id__c,Financials_if_requested__c, OwnerEnitty_Fund__c,Guarantor__c,Lease_Opportunity_Type__c,Market_Rents__c,Milestone__c,MRI_Property__c,MRIProperty_Address__c,Summary_of_Terms_Rich__c,Tenant_Notice_Address__c,Avg_option_Rents_over_initial_period__c,Suite_Address__c,Tenant_Improvement_Cost__c,Tenant_Improvement_PSF__c,Initialterm__c,Is_there_a_lender__c,Is_this_a_TIC_property__c,
                   Property_Name__c,Property_Type__c,Recommendations_Narrative_Rich__c,Renewal_Effective_date__c,Option_Rents_Contractual__c,TIC_and_Lender_Approval__c,Supplemental_Attachments__c, Annual_Base_Rent_PSF__c,Annual_Base_Rent_at_expiration__c,Tenant_Contact_Number__c,Date_Executed__c,Estimated_Rent_Start_Date__c,Estimated_rent_start_date_New__c,Next_Contract_Option_Rents_per_Lease__c,NNN_Base_Year__c,
                   Renewalterm__c,Sales_Reports_if_requested__c,Suite_Sqft__c,Status__c,Tenant_Contact_Name__c,Tenant_DBA__c,Percent_Rent_Breakpoint__c, Base_Rent__c ,Tenant_Email__c,Anticipated_Delivery_Date__c,MRI_Property__r.City__c,MRI_Property__r.State__c,Lease__r.Suite_Address__c,MRI_Property__r.Zip_code__c,SF_excluding__c,Record_ID__c,Lease__r.Master_Lease_ID__c,
                   Tenant_Legal_Name__c,Tenant_payment_performance_history__c,Waivers__c,Remaining_Options_Intact__c,RemainingOptions_Description__c,LC_OtherOpportunity_Description__c,lease__r.State__c,lease__r.City__c,Budgeted_LCs__c,Budgeted_LCs_PSF__c,Budgeted_LL_Work__c,Budgeted_LL_Work_PSF__c,Budgeted_TIs__c,Budgeted_TIs_PSF__c,Landlord_Work__c,Net_Book_Value__c,Administrative_Fees__c from LC_LeaseOpprtunity__c where id=:quoteId1];
             LseOpty=myCT;
           
      }
          Catch(Exception e)
       {
       
       }
         return LseOpty;
        
    } set;
    }    
  
  //**********LseOpty*****************************
  //***************showRenewalInfo*******************
   public boolean showRenewalInfo{get{
       system.debug('>>>>>>>>>>>>>> '+quoteId1);
      
       try
       {
          LC_LeaseOpprtunity__c   myCT= [select id,Name,National_Tenant__c,Actual_NER__c,Add_l_Options__c,Additional_Comments__c,Tenant_Address__c,Alternate_Property_Name__c,Average_base_rent_over_renewal_period__c,Average_Option_Rents_over_Renewal_Period__c,Suite_Address__c,Average_Base_Rent_Over_Initial_Term__c,Reimbursable_Type__c,Landlord_Broker__c,Tenant_Broker__c,Date_last_tenant_vacated_New__c,Base_Rent_PSF__c,
                   Budgeted_NER__c,Building_Id__c,Common_Name__c,Contiguous_Tenant_For_Possible_Expansion__c,Current_Date__c,Current_Expiration_Date__c,Tenant_Email_Contact_Number__c,Lease_Category__c,LC_Already_Submitted__c,Avg_option_Rents_over_initial_period__c,Suite_Id__c,Leasing_Commissions__c,Percent_Rent1__c,Estimated_Rent_Start_Date__c,Estimated_rent_start_date_New__c,Leasing_Commissions_PSF__c ,Was_space_purchased_vacant_New__c,
                   Entity_Id__c,Financials_if_requested__c, OwnerEnitty_Fund__c,Guarantor__c,Lease_Opportunity_Type__c,Market_Rents__c,Milestone__c,MRI_Property__c,MRIProperty_Address__c,Summary_of_Terms_Rich__c,Lease__r.Suite_Address__c,Tenant_Contact_Number__c,Percent_Rent__c,Break_Point__c,Tenant_Improvement_Cost__c,Tenant_Improvement_PSF__c,Initialterm__c,Is_there_a_lender__c,Is_this_a_TIC_property__c,
                   Property_Name__c,Property_Type__c,Recommendations_Narrative_Rich__c,Renewal_Effective_date__c,Option_Rents_Contractual__c,TIC_and_Lender_Approval__c,Supplemental_Attachments__c, Annual_Base_Rent_PSF__c,Annual_Base_Rent_at_expiration__c,Prepared_by__c,Deal_Maker__c,Date_Executed__c,Next_Contract_Option_Rents_per_Lease__c,NNN_Base_Year__c,
                   Renewalterm__c,Sales_Reports_if_requested__c,Suite_Sqft__c,Status__c,Tenant_Contact_Name__c,Tenant_DBA__c,Percent_Rent_Breakpoint__c, Base_Rent__c ,Tenant_Email__c,Anticipated_Delivery_Date__c,MRI_Property__r.City__c,MRI_Property__r.State__c,MRI_Property__r.Zip_code__c,SF_excluding__c,Record_ID__c,Lease__r.Master_Lease_ID__c,
                   Tenant_Legal_Name__c,Tenant_payment_performance_history__c,Waivers__c,Remaining_Options_Intact__c,RemainingOptions_Description__c,LC_OtherOpportunity_Description__c,Budgeted_LCs__c,Budgeted_LCs_PSF__c,Budgeted_LL_Work__c,Budgeted_LL_Work_PSF__c,Budgeted_TIs__c,Budgeted_TIs_PSF__c,Landlord_Work__c,Net_Book_Value__c,Administrative_Fees__c  from LC_LeaseOpprtunity__c where id=:quoteId1];
          //if(myCT.Lease_Opportunity_Type__c=='Lease - New')
         if((myCT.Lease_Opportunity_Type__c=='Lease - New') ||(myCT.Lease_Opportunity_Type__c=='Lease-New Master Lease'))
         {
             showRenewalInfo=False;
             
        }
       else 
        {
            showRenewalInfo=True;
           
        }
       } 
       Catch(Exception e)
       {
       
       }
         return showRenewalInfo;
        
    } set;
    }
     //***************showRenewalInfo*******************
     
       //***************ShowEstimatedRentStartDate*******************

    
    
     public boolean showEstimatedRent{get{
       system.debug('>>>>>>>>>>>>>> '+quoteId1);
      
       try
       {
          LC_LeaseOpprtunity__c   myCT= [select id,Name,National_Tenant__c,Actual_NER__c,Add_l_Options__c,Additional_Comments__c,Tenant_Address__c,Alternate_Property_Name__c,Average_base_rent_over_renewal_period__c,Average_Option_Rents_over_Renewal_Period__c,Suite_Address__c,Average_Base_Rent_Over_Initial_Term__c,Reimbursable_Type__c,Landlord_Broker__c,Tenant_Broker__c,Date_last_tenant_vacated_New__c,Base_Rent_PSF__c,
                   Budgeted_NER__c,Building_Id__c,Common_Name__c,Contiguous_Tenant_For_Possible_Expansion__c,Current_Date__c,Current_Expiration_Date__c,Tenant_Email_Contact_Number__c,Lease_Category__c,LC_Already_Submitted__c,Avg_option_Rents_over_initial_period__c,Suite_Id__c,Leasing_Commissions__c,Percent_Rent1__c,Estimated_Rent_Start_Date__c,Estimated_rent_start_date_New__c,Leasing_Commissions_PSF__c ,Was_space_purchased_vacant_New__c,
                   Entity_Id__c,Financials_if_requested__c, OwnerEnitty_Fund__c,Guarantor__c,Lease_Opportunity_Type__c,Market_Rents__c,Milestone__c,MRI_Property__c,MRIProperty_Address__c,Summary_of_Terms_Rich__c,Lease__r.Suite_Address__c,Tenant_Contact_Number__c,Percent_Rent__c,Break_Point__c,Tenant_Improvement_Cost__c,Tenant_Improvement_PSF__c,Initialterm__c,Is_there_a_lender__c,Is_this_a_TIC_property__c,
                   Property_Name__c,Property_Type__c,Recommendations_Narrative_Rich__c,Renewal_Effective_date__c,Option_Rents_Contractual__c,TIC_and_Lender_Approval__c,Supplemental_Attachments__c, Annual_Base_Rent_PSF__c,Annual_Base_Rent_at_expiration__c,Prepared_by__c,Deal_Maker__c,Date_Executed__c,Next_Contract_Option_Rents_per_Lease__c,NNN_Base_Year__c,Lease__r.Master_Lease_ID__c,
                   Renewalterm__c,Sales_Reports_if_requested__c,Suite_Sqft__c,Status__c,Tenant_Contact_Name__c,Tenant_DBA__c,Percent_Rent_Breakpoint__c, Base_Rent__c ,Tenant_Email__c,Anticipated_Delivery_Date__c,MRI_Property__r.City__c,MRI_Property__r.State__c,MRI_Property__r.Zip_code__c,SF_excluding__c,Record_ID__c,
                   Tenant_Legal_Name__c,Tenant_payment_performance_history__c,Waivers__c,Remaining_Options_Intact__c,RemainingOptions_Description__c,LC_OtherOpportunity_Description__c,Budgeted_LCs__c,Budgeted_LCs_PSF__c,Budgeted_LL_Work__c,Budgeted_LL_Work_PSF__c,Budgeted_TIs__c,Budgeted_TIs_PSF__c,Landlord_Work__c,Net_Book_Value__c,Administrative_Fees__c  from LC_LeaseOpprtunity__c where id=:quoteId1];
          if(myCT.Lease_Opportunity_Type__c=='Lease - New')
         {
             showEstimatedRent=True;
             
        }
       else 
        {
            showEstimatedRent=False;
           
        }
       } 
       Catch(Exception e)
       {
       
       }
         return showEstimatedRent;
        
    } set;
    }
     
         //***************ShowEstimatedRentStartDate*******************

          //***************showRenewalEffectiveDate*******************
          
   
              
               public boolean showRenewalEffectiveDate{get{
       system.debug('>>>>>>>>>>>>>> '+quoteId1);
      
       try
       {
          LC_LeaseOpprtunity__c   myCT= [select id,Name,National_Tenant__c,Actual_NER__c,Add_l_Options__c,Additional_Comments__c,Tenant_Address__c,Alternate_Property_Name__c,Average_base_rent_over_renewal_period__c,Average_Option_Rents_over_Renewal_Period__c,Suite_Address__c,Average_Base_Rent_Over_Initial_Term__c,Reimbursable_Type__c,Landlord_Broker__c,Tenant_Broker__c,Date_last_tenant_vacated_New__c,Base_Rent_PSF__c,
                   Budgeted_NER__c,Building_Id__c,Common_Name__c,Contiguous_Tenant_For_Possible_Expansion__c,Current_Date__c,Current_Expiration_Date__c,Tenant_Email_Contact_Number__c,Lease_Category__c,LC_Already_Submitted__c,Avg_option_Rents_over_initial_period__c,Suite_Id__c,Leasing_Commissions__c,Percent_Rent1__c,Estimated_Rent_Start_Date__c,Estimated_rent_start_date_New__c,Leasing_Commissions_PSF__c ,Was_space_purchased_vacant_New__c,
                   Entity_Id__c,Financials_if_requested__c, OwnerEnitty_Fund__c,Guarantor__c,Lease_Opportunity_Type__c,Market_Rents__c,Milestone__c,MRI_Property__c,MRIProperty_Address__c,Summary_of_Terms_Rich__c,Lease__r.Suite_Address__c,Tenant_Contact_Number__c,Percent_Rent__c,Break_Point__c,Tenant_Improvement_Cost__c,Tenant_Improvement_PSF__c,Initialterm__c,Is_there_a_lender__c,Is_this_a_TIC_property__c,
                   Property_Name__c,Property_Type__c,Recommendations_Narrative_Rich__c,Renewal_Effective_date__c,Option_Rents_Contractual__c,TIC_and_Lender_Approval__c,Supplemental_Attachments__c, Annual_Base_Rent_PSF__c,Annual_Base_Rent_at_expiration__c,Prepared_by__c,Deal_Maker__c,Date_Executed__c,Next_Contract_Option_Rents_per_Lease__c,NNN_Base_Year__c,Lease__r.Master_Lease_ID__c,
                   Renewalterm__c,Sales_Reports_if_requested__c,Suite_Sqft__c,Status__c,Tenant_Contact_Name__c,Tenant_DBA__c,Percent_Rent_Breakpoint__c, Base_Rent__c ,Tenant_Email__c,Anticipated_Delivery_Date__c,MRI_Property__r.City__c,MRI_Property__r.State__c,MRI_Property__r.Zip_code__c,SF_excluding__c,Record_ID__c,
                   Tenant_Legal_Name__c,Tenant_payment_performance_history__c,Waivers__c,Remaining_Options_Intact__c,RemainingOptions_Description__c,LC_OtherOpportunity_Description__c,Budgeted_LCs__c,Budgeted_LCs_PSF__c,Budgeted_LL_Work__c,Budgeted_LL_Work_PSF__c,Budgeted_TIs__c,Budgeted_TIs_PSF__c,Landlord_Work__c,Net_Book_Value__c,Administrative_Fees__c  from LC_LeaseOpprtunity__c where id=:quoteId1];
          if(myCT.Lease_Opportunity_Type__c=='Lease - Renewal')
         {
             showRenewalEffectiveDate=True;
             
        }
       else 
        {
            showRenewalEffectiveDate=False;
           
        }
       } 
       Catch(Exception e)
       {
       
       }
         return showRenewalEffectiveDate;
        
    } set;
    }
          
          //***************showRenewalEffectiveDate*******************
     
     //***************showRentReliefInfo*******************
    
     /*public boolean showRentReliefInfo{get{
       system.debug('>>>>>>>>>>>>>> '+quoteId1);
      
       try
       {
          LC_LeaseOpprtunity__c   myCT= [select id,Name,National_Tenant__c,Actual_NER__c,Add_l_Options__c,Additional_Comments__c,Tenant_Address__c,Alternate_Property_Name__c,Average_base_rent_over_renewal_period__c,Average_Option_Rents_over_Renewal_Period__c,Suite_Address__c,Average_Base_Rent_Over_Initial_Term__c,Reimbursable_Type__c,Landlord_Broker__c,Tenant_Broker__c,Date_last_tenant_vacated_New__c,Base_Rent_PSF__c,
                   Budgeted_NER__c,Building_Id__c,Common_Name__c,Contiguous_Tenant_For_Possible_Expansion__c,Current_Date__c,Current_Expiration_Date__c,Tenant_Email_Contact_Number__c,Lease_Category__c,LC_Already_Submitted__c,Avg_option_Rents_over_initial_period__c,Suite_Id__c,Leasing_Commissions__c,Percent_Rent1__c,Estimated_Rent_Start_Date__c,Estimated_rent_start_date_New__c,Leasing_Commissions_PSF__c ,Was_space_purchased_vacant_New__c,
                   Entity_Id__c,Financials_if_requested__c, OwnerEnitty_Fund__c,Guarantor__c,Lease_Opportunity_Type__c,Market_Rents__c,Milestone__c,MRI_Property__c,MRIProperty_Address__c,Summary_of_Terms_Rich__c,Lease__r.Suite_Address__c,Tenant_Contact_Number__c,Percent_Rent__c,Break_Point__c,Tenant_Improvement_Cost__c,Tenant_Improvement_PSF__c,Initialterm__c,Is_there_a_lender__c,Is_this_a_TIC_property__c,
                   Property_Name__c,Property_Type__c,Recommendations_Narrative_Rich__c,Renewal_Effective_date__c,Option_Rents_Contractual__c,TIC_and_Lender_Approval__c,Supplemental_Attachments__c, Annual_Base_Rent_PSF__c,Annual_Base_Rent_at_expiration__c,Prepared_by__c,Deal_Maker__c,Date_Executed__c,Next_Contract_Option_Rents_per_Lease__c,NNN_Base_Year__c,Record_ID__c,
                   Renewalterm__c,Sales_Reports_if_requested__c,Suite_Sqft__c,Status__c,Tenant_Contact_Name__c,Tenant_DBA__c,Percent_Rent_Breakpoint__c, Base_Rent__c ,Tenant_Email__c,Anticipated_Delivery_Date__c,MRI_Property__r.City__c,MRI_Property__r.State__c,MRI_Property__r.Zip_code__c,SF_excluding__c,
                   Tenant_Legal_Name__c,Tenant_payment_performance_history__c,Waivers__c,Remaining_Options_Intact__c,RemainingOptions_Description__c,LC_OtherOpportunity_Description__c  from LC_LeaseOpprtunity__c where id=:quoteId1];
          if(myCT.Lease_Opportunity_Type__c=='Lease - New')
         {
             showRentReliefInfo=false;
            
        }
       else 
        {
            showRentReliefInfo=true;
           
        }
       } 
       Catch(Exception e)
       {
       
       }
         return showRentReliefInfo;
        
    } set;
    }*/

    //***************showRentReliefInfo******************* 
   
    //***************Revised section*******************    
        
    
    
     public boolean showRevisedSection{get{
       system.debug('>>>>>>>>>>>>>> '+quoteId1);
      
       try
       {
          LC_LeaseOpprtunity__c   myCT= [select id,Name,National_Tenant__c,Actual_NER__c,Add_l_Options__c,Additional_Comments__c,Tenant_Address__c,Alternate_Property_Name__c,Average_base_rent_over_renewal_period__c,Average_Option_Rents_over_Renewal_Period__c,Suite_Address__c,Suite_Id__c,Leasing_Commissions__c,Reimbursable_Type__c,Landlord_Broker__c,Tenant_Broker__c,Date_last_tenant_vacated_New__c,Base_Rent_PSF__c,
                   Budgeted_NER__c,Building_Id__c,Common_Name__c,Contiguous_Tenant_For_Possible_Expansion__c,Current_Date__c,Current_Expiration_Date__c,Tenant_Email_Contact_Number__c,Lease_Category__c,LC_Already_Submitted__c,Avg_option_Rents_over_initial_period__c,Average_Base_Rent_Over_Initial_Term__c,Percent_Rent1__c,Estimated_Rent_Start_Date__c,Estimated_rent_start_date_New__c,Leasing_Commissions_PSF__c ,Was_space_purchased_vacant_New__c,
                   Entity_Id__c,Financials_if_requested__c, OwnerEnitty_Fund__c,Guarantor__c,Lease_Opportunity_Type__c,Market_Rents__c,Milestone__c,MRI_Property__c,MRIProperty_Address__c,Summary_of_Terms_Rich__c,Lease__r.Suite_Address__c,Tenant_Contact_Number__c,Percent_Rent__c,Break_Point__c,Tenant_Improvement_Cost__c,Tenant_Improvement_PSF__c,Initialterm__c,Is_there_a_lender__c,Is_this_a_TIC_property__c,
                   Property_Name__c,Property_Type__c,Recommendations_Narrative_Rich__c,Renewal_Effective_date__c,Option_Rents_Contractual__c,TIC_and_Lender_Approval__c,Supplemental_Attachments__c, Annual_Base_Rent_PSF__c,Annual_Base_Rent_at_expiration__c,Prepared_by__c,Deal_Maker__c,Date_Executed__c,Next_Contract_Option_Rents_per_Lease__c,NNN_Base_Year__c,Record_ID__c,Lease__r.Master_Lease_ID__c,
                   Renewalterm__c,Sales_Reports_if_requested__c,Suite_Sqft__c,Status__c,Tenant_Contact_Name__c,Tenant_DBA__c,Percent_Rent_Breakpoint__c, Base_Rent__c ,Tenant_Email__c,Anticipated_Delivery_Date__c,MRI_Property__r.City__c,MRI_Property__r.State__c,MRI_Property__r.Zip_code__c,SF_excluding__c,
                   Tenant_Legal_Name__c,Tenant_payment_performance_history__c,Waivers__c,Remaining_Options_Intact__c,RemainingOptions_Description__c,LC_OtherOpportunity_Description__c,Budgeted_LCs__c,Budgeted_LCs_PSF__c,Budgeted_LL_Work__c,Budgeted_LL_Work_PSF__c,Budgeted_TIs__c,Budgeted_TIs_PSF__c,Landlord_Work__c,Net_Book_Value__c,Administrative_Fees__c  from LC_LeaseOpprtunity__c where id=:quoteId1];
          if(myCT.LC_Already_Submitted__c>1)
         {
             showRevisedSection=true;
            
        }
       else if(myCT.LC_Already_Submitted__c<=1)
        {
            showRevisedSection=false;
           
        }
       } 
       Catch(Exception e)
       {
       
       }
         return showRevisedSection;
        
    } set;
    }
   //***************Revised section*******************   
   
    
    //**************showRentSchedule***************
       public Boolean showRentSchedule{get{
       system.debug('>>>>>>>>>>>>>> '+quoteId1);
      
       try
       {
        list<LC_RentSchedule__c> lst = new list<LC_RentSchedule__c>();
        system.debug('-------------NewhireNdcontroller-------------->'+quoteId1);
       lst=[select Increase_in_rent__c,Annual_PSF__c,Average_Base_Rent_over_Initial_Term__c,Base_Rent_Additional_Info__c,Break_Point__c,Base_Rent_PSF__c,From_In_Months__c,Months_Years__c,Income_Category_1__c,Lease__c,Lease_Opportunity__c,Monthly_PSF__c,To_In_Months__c,sqft__c  from LC_RentSchedule__c where Lease_Opportunity__c=:quoteId1 Order By Income_Category_1__c,From_In_Months__c ASC ];
       
       if(lst.size()>0)
       {
        showRentSchedule=true;
       
       }
       else if(lst.size()==0)
        {
          showRentSchedule=false;
           
        }
       } 
       Catch(Exception e)
       {
       
       }
         return showRentSchedule;
        
    } set;
    } 
    
    //*******************showRentSchedule**********************
    //**************showOptionSchedule***************
       public Boolean showOptionSchedule{get{
       system.debug('>>>>>>>>>>>>>> '+quoteId1);
       
       try
       {
        // list<LC_OptionSchedule__c> lst = new list<LC_OptionSchedule__c>();
        //system.debug('-------------NewhireNdcontroller-------------->'+quoteId1);
        list<LC_OptionSchedule__c>lst=[select id,Name,Additional_Notes__c,Amount__c,Currency__c,Amount_Description__c,Formula_Number__c,Frequency__c,Lease_Opportunity__c,Notice_Days__c,To_In_Months__c,From_In_Months__c,Months_Years__c,Option_Type__c,sqft__c,Base_Rent_PSF__c,Break_Point__c,Increase_in_rent__c,Increase_in_rent_n__c,Monthly_PSF__c from LC_OptionSchedule__c where Lease_Opportunity__c=:quoteId1 Order By Option_Type__c,From_In_Months__c ASC ];
        
       if(lst.size()>0)
       {
        showOptionSchedule=true;
       
       }
       else if(lst.size()==0)
        {
          showOptionSchedule=false;
           
        }
       } 
       Catch(Exception e)
       {
       
       }
         return showOptionSchedule;
        
    } set;
    } 
    
    //*******************showOptionSchedule**********************
    
   
  //********** LCoptionlist*****************************
   public list<LC_OptionSchedule__c> LCoptionlist{get{
       list<LC_OptionSchedule__c> lst = new list<LC_OptionSchedule__c>();
        system.debug('-------------NewhireNdcontroller-------------->'+quoteId1);
        lst=[select id,Name,Additional_Notes__c,Amount__c,Currency__c,Amount_Description__c,Formula_Number__c,Frequency__c,Lease_Opportunity__c,Notice_Days__c,To_In_Months__c,From_In_Months__c,Months_Years__c,Option_Type__c,sqft__c,Base_Rent_PSF__c,Break_Point__c,Increase_in_rent__c,Increase_in_rent_n__c,Monthly_PSF__c from LC_OptionSchedule__c where Lease_Opportunity__c=:quoteId1 order by From_In_Months__c ];
        LCoptionlist= lst;
        return LCoptionlist;
    } set;
    }
 //**********LCoptionlist******************************
 
 //********** LCrentlist*****************************
   public list<LC_RentSchedule__c> LCrentlist{get{
       list<LC_RentSchedule__c> lst = new list<LC_RentSchedule__c>();
        system.debug('-------------NewhireNdcontroller-------------->'+quoteId1);
        lst=[select Base_Rent_Additional_Info__c,Increase_in_rent__c,Annual_PSF__c,Average_Base_Rent_over_Initial_Term__c,Break_Point__c,Increase_in_rent_n__c,Base_Rent_PSF__c,From_In_Months__c,Months_Years__c,Income_Category_1__c,Lease__c,Lease_Opportunity__c,Monthly_PSF__c,To_In_Months__c,sqft__c  from LC_RentSchedule__c where Lease_Opportunity__c=:quoteId1 order by From_In_Months__c ];
        LCrentlist= lst;
        return LCrentlist;
    } set;
    }
 //**********LCrentlist******************************
 //********** otherclauselist*****************************
     public list<LC_ClauseType__c> cTypeList1{get{
       list<LC_ClauseType__c> lst = new list<LC_ClauseType__c>();
        system.debug('-------------NewhireNdcontroller-------------->'+quoteId1);
       lst= [select id,Name,ClauseType_Name__c,Clause_Description__c,Clauses__c,Lease_Opportunity__c from  LC_ClauseType__c where Clauses__r.Show_On_LETs_Form__c=false and Lease_Opportunity__c=:quoteId1];
        
        
       // lst=[select id,Name,ClauseType_Name__c,Clause_Description__c,Clauses__c,Lease_Opportunity__c from  LC_ClauseType__c where (Name!='125PEU' and Name!='302COM' and Name!='305FIX' and Name!='306NNN'and 
                      // Name!='108SECD'and Name!='307PPR' and Name!='140LLM' and Name!='112EXC' and Name!='137CON' and Name!='113CONT' and Name!='114RAD' and Name!='127DRK' and Name!='123ROFO' and Lease_Opportunity__c =:quoteId1)];
          system.debug('&&&&&&&&&&&&&&&&&&&&&&&&&'+cTypeList1);
        cTypeList1= lst;
        return cTypeList1;
    } set;
    }
 //**********otherclauselist******************************
 //**************standard caluse list*****************
  public list<LC_ClauseType__c> StandardClauseList{get{
       list<LC_ClauseType__c> lst = new list<LC_ClauseType__c>();
        system.debug('-------------NewhireNdcontroller-------------->'+quoteId1);
       lst= [select id,Name,ClauseType_Name__c,Clause_Description__c,Clauses__c,Lease_Opportunity__c from  LC_ClauseType__c where Clauses__r.Show_On_LETs_Form__c=true and Lease_Opportunity__c=:quoteId1];
        
        
       // lst=[select id,Name,ClauseType_Name__c,Clause_Description__c,Clauses__c,Lease_Opportunity__c from  LC_ClauseType__c where (Name!='125PEU' and Name!='302COM' and Name!='305FIX' and Name!='306NNN'and 
                      // Name!='108SECD'and Name!='307PPR' and Name!='140LLM' and Name!='112EXC' and Name!='137CON' and Name!='113CONT' and Name!='114RAD' and Name!='127DRK' and Name!='123ROFO' and Lease_Opportunity__c =:quoteId1)];
          system.debug('&&&&&&&&&&&&&&&&&&&&&&&&&'+cTypeList1);
        StandardClauseList= lst;
        return StandardClauseList;
    } set;
    }
 
 //***********************************************
 
  //**********Master Grid List******************************
   public list<Lease__c> wrapLeaseList{get{
       list<Lease__c> Lselst = new list<Lease__c>();
        system.debug('-------------NewhireNdcontroller-------------->'+quoteId1);
        Lselst =[select id,Name,SuiteId__c,MRI_PROPERTY__c,Property_ID__c,LeaseId__c,MRI_PROPERTY__r.Name,MRI_PROPERTY__r.Common_Name__c,Master_Lease__c,Master_Lease_ID__c,MRI_Lease_ID__c,Center_Name__c,Tenant_Contact_Name__c,
        Tenant_Name__c,Suite_Sqft__c,MRI_PROPERTY__r.NOI__c,MRI_PROPERTY__r.Property_ID__c,MRI_PROPERTY__r.Property_Owner_SPE__c,MRI_PROPERTY__r.Property_Owner_SPE__r.name,Expiration_Date__c from Lease__c where  Master_Lease__c=true and Master_Lease_ID__c=:LseOpty.Lease__r.Master_Lease_ID__c ];
        wrapLeaseList= Lselst ;
        return wrapLeaseList;
    } set;
    } 
  
  //**********Master Grid List******************************
  //**********Sub  Grid List******************************
   public list<Helper_for_Master_Lease__c> hmlList {get{
       list<Helper_for_Master_Lease__c > Lselst = new list<Helper_for_Master_Lease__c >();
        system.debug('-------------NewhireNdcontroller-------------->'+quoteId1);
       Lselst = [Select id,Name__c,Lease__c,Lease_Opp_ID__c,Lease_Opportunity__c,Master_Lease_Id__c,MRI_Property__c,MRI_PROPERTY__r.Name,Lease__r.Master_Lease_ID__c,MRI_Property__r.Property_ID__c,Lease__r.Suite_Sqft__c,Going_on_NOI1__c,GLA_Suite_SQFT__c,Lease_Expiration_Date__c,LTC_Name__c,Property_Owner_SPE__c,Lease_Id__c,Building_Id__c,Additional_Information__c,
            Lease__r.LeaseId__c,Lease__r.Center_Name__c,Lease__r.Tenant_Name__c,MRI_Property__r.Common_Name__c,MRI_Property__r.NOI__c,Lease__r.Expiration_Date__c,MRI_Property__r.Property_Owner_SPE__c,MRI_PROPERTY__r.Property_Owner_SPE__r.Name, Lease__r.Name
            From Helper_for_Master_Lease__c where  Lease_Opportunity__c=:LseOpty.Id];
        if(Lselst.size()>0)
          hmlList = Lselst ;
        return hmlList ;
    } set;
    } 
  
  //**********Sub Grid List******************************
  //***************show Master Grid*******************
          
    
       public boolean showMasterGrid{get{
       system.debug('>>>>>>>>>>>>>> '+quoteId1);
      
       try
       {
          LC_LeaseOpprtunity__c   myCT= [select id,Name,National_Tenant__c,Actual_NER__c,Add_l_Options__c,Additional_Comments__c,Tenant_Address__c,Alternate_Property_Name__c,Average_base_rent_over_renewal_period__c,Average_Option_Rents_over_Renewal_Period__c,Suite_Address__c,Average_Base_Rent_Over_Initial_Term__c,Reimbursable_Type__c,Landlord_Broker__c,Tenant_Broker__c,Date_last_tenant_vacated_New__c,Base_Rent_PSF__c,
                   Budgeted_NER__c,Building_Id__c,Common_Name__c,Contiguous_Tenant_For_Possible_Expansion__c,Current_Date__c,Current_Expiration_Date__c,Tenant_Email_Contact_Number__c,Lease_Category__c,LC_Already_Submitted__c,Avg_option_Rents_over_initial_period__c,Suite_Id__c,Leasing_Commissions__c,Percent_Rent1__c,Estimated_Rent_Start_Date__c,Estimated_rent_start_date_New__c,Leasing_Commissions_PSF__c ,Was_space_purchased_vacant_New__c,
                   Entity_Id__c,Financials_if_requested__c, OwnerEnitty_Fund__c,Guarantor__c,Lease_Opportunity_Type__c,Market_Rents__c,Milestone__c,MRI_Property__c,MRIProperty_Address__c,Summary_of_Terms_Rich__c,Lease__r.Suite_Address__c,Tenant_Contact_Number__c,Percent_Rent__c,Break_Point__c,Tenant_Improvement_Cost__c,Tenant_Improvement_PSF__c,Initialterm__c,Is_there_a_lender__c,Is_this_a_TIC_property__c,
                   Property_Name__c,Property_Type__c,Recommendations_Narrative_Rich__c,Renewal_Effective_date__c,Option_Rents_Contractual__c,TIC_and_Lender_Approval__c,Supplemental_Attachments__c, Annual_Base_Rent_PSF__c,Annual_Base_Rent_at_expiration__c,Prepared_by__c,Deal_Maker__c,Date_Executed__c,Next_Contract_Option_Rents_per_Lease__c,NNN_Base_Year__c,Lease__r.Master_Lease_ID__c,
                   Renewalterm__c,Sales_Reports_if_requested__c,Suite_Sqft__c,Status__c,Tenant_Contact_Name__c,Tenant_DBA__c,Percent_Rent_Breakpoint__c, Base_Rent__c ,Tenant_Email__c,Anticipated_Delivery_Date__c,MRI_Property__r.City__c,MRI_Property__r.State__c,MRI_Property__r.Zip_code__c,SF_excluding__c,Record_ID__c,
                   Tenant_Legal_Name__c,Tenant_payment_performance_history__c,Waivers__c,Remaining_Options_Intact__c,RemainingOptions_Description__c,LC_OtherOpportunity_Description__c,Budgeted_LCs__c,Budgeted_LCs_PSF__c,Budgeted_LL_Work__c,Budgeted_LL_Work_PSF__c,Budgeted_TIs__c,Budgeted_TIs_PSF__c,Landlord_Work__c,Net_Book_Value__c,Administrative_Fees__c  from LC_LeaseOpprtunity__c where id=:quoteId1];
        

          if((myCT.Lease_Opportunity_Type__c=='Master Lease - New')||(myCT.Lease_Opportunity_Type__c=='Master Lease - Amendment')||(myCT.Lease_Opportunity_Type__c =='Master Lease - Assignment')||(myCT.Lease_Opportunity_Type__c =='Master Lease - Rent Restructure & Extension')||(myCT.Lease_Opportunity_Type__c =='Master Lease - Sublease'))
         {
             showMasterGrid=True;
             
        }
       else 
        {
            showMasterGrid=False;
           
        }
       } 
       Catch(Exception e)
       {
       
       }
         return showMasterGrid;
        
    } set;
    }
          
          //***************show Master Grid*******************
 
 
    
  //**********Attachmentslist****************************** 
   
     public list<Attachment> attList{get{
       list<Attachment> alst = new list<Attachment>();
        system.debug('-------------NewhireNdcontroller-------------->'+quoteId1);
        alst =[SELECT Id, Name, Body, ContentType FROM Attachment  WHERE Parentid =:quoteId1 ];
        attList= alst ;
        return attList;
    } set;
    } 
    
    
 //**********Attachmentslist******************************
 

 //**********Notesslist****************************** 
   
     public list<Note> noteList {get{
       list<Note> nlst = new list<Note>();
      
        nlst =[SELECT Id,Title FROM Note  WHERE Parentid =:quoteId1 ];
        noteList = nlst ;
        return noteList ;
    } set;
    } 
    
    
 //**********Notesslist******************************
 
 
  public List<Schema.FieldSetMember> getfirstHeadersectionfields() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.First_Header_section_field_set.getFields();
    }
    
    public List<Schema.FieldSetMember> getfirstHeadersectionFSfields() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.First_Header_section_field_set.getFields();
    }
    
    public List<Schema.FieldSetMember> getsecondHeadersectionfields() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.Second_Header_Section.getFields();
    }
    
     public List<Schema.FieldSetMember> getthirdheadersectionfields() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.Third_Field_set_for_Header_Section.getFields();
    }
     public List<Schema.FieldSetMember> getfourthheadersectionfields() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.Fourth_Field_set_for_Header.getFields();
    }
  
        
     public List<Schema.FieldSetMember> getfifthheadersectionfields() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.Fifth_header_field_set.getFields();
    }
     public List<Schema.FieldSetMember> getbasicinfofields() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.Basic_Info.getFields();
    }
     public List<Schema.FieldSetMember> getNewexistingoppfields() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.New_Or_existing_lease_info.getFields();
    }
     public List<Schema.FieldSetMember> getRenewalExistingoppfields() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.Renewal_Existing_Lease_Info.getFields();
    }
    public List<Schema.FieldSetMember> getRenewalDatefields() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.RenewalDate.getFields();
    }
    public List<Schema.FieldSetMember> getRenewalExistingLeasefields1() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.Renewal_ExistingLease1.getFields();
    }
     public List<Schema.FieldSetMember> getAdditionalInfofields() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.Additional_Information.getFields();
    }
    public List<Schema.FieldSetMember> getBVAInfofields1() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.BVA_information_1.getFields();
    }
     public List<Schema.FieldSetMember> getBVAInfofields2() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.BVA_information_2.getFields();
    }
    
    public List<Schema.FieldSetMember> getReportingOnlyfields() {
        return SObjectType.LC_LeaseOpprtunity__c.FieldSets.Reporting_Purpose_Only.getFields();
    }
   
 public LC_LeaseOpprtunity__c  LCopp {get{
       system.debug('>>>>>>>>>>>>>> '+quoteId1);
    
       try
       {
             String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getfirstHeadersectionfields()) {
            query += f.getFieldPath() + ', ';
        }
        
         for(Schema.FieldSetMember f : this.getsecondHeadersectionfields()) {
            query += f.getFieldPath() + ', ';
        }
          for(Schema.FieldSetMember f : this.getthirdheadersectionfields()) {
            query += f.getFieldPath() + ', ';
        }
        for(Schema.FieldSetMember f : this.getfourthheadersectionfields()) {
            query += f.getFieldPath() + ', ';
        }
         for(Schema.FieldSetMember f : this.getfifthheadersectionfields()) {
            query += f.getFieldPath() + ', ';
        }
        for(Schema.FieldSetMember f : this.getbasicinfofields()) {
            query += f.getFieldPath() + ', ';
        }
        for(Schema.FieldSetMember f : this.getNewexistingoppfields()) {
            query += f.getFieldPath() + ', ';
        }
        
        for(Schema.FieldSetMember f : this.getRenewalExistingoppfields()) {
            query += f.getFieldPath() + ', ';
        } 
        
        for(Schema.FieldSetMember f : this.getRenewalDatefields()) {
            query += f.getFieldPath() + ', ';
        } 
         for(Schema.FieldSetMember f : this.getRenewalExistingLeasefields1()) {
            query += f.getFieldPath() + ', ';
        }  
        
        
        for(Schema.FieldSetMember f : this.getAdditionalInfofields()) {
            query += f.getFieldPath() + ', ';
        }
         
         for(Schema.FieldSetMember f : this.getBVAInfofields1()) {
            query += f.getFieldPath() + ', ';
        } 
        for(Schema.FieldSetMember f : this.getBVAInfofields2()) {
            query += f.getFieldPath() + ', ';
        }       
        
        for(Schema.FieldSetMember f : this.getReportingOnlyfields()) {
            query += f.getFieldPath() + ', ';
        } 
        
        query += 'Id FROM LC_LeaseOpprtunity__c where id=:quoteId1 LIMIT 1';
        system.debug('check the quoteid'+quoteId1 );
        system.debug('check the query'+query );
        
        return Database.query(query);
      }
          Catch(Exception e)
       {
       
       }
         return LCopp ;
        
    } set;
    }     
 
 
 
 
    
}