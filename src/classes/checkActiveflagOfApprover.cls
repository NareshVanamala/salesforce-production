global class checkActiveflagOfApprover implements Database.Batchable<SObject>,Database.Stateful
{
    global String Query;  
    global String finalstr;
    string que ;
    global list<Asset__c>InActiveApprerAssetlist;
    global list<Asset__c>InActiveImplementerAssetlist;
    global Database.QueryLocator start(Database.BatchableContext BC)  
    {  
        set<id>empidset=new set<id>();
        list<Employee__c>emplist=[select id from Employee__c where Employee_Status__c!='Terminated'];
        for(Employee__c ecp:emplist)
        {
            empidset.add(ecp.id);   
        
        }
        Query= 'select Application_Implementor__r.IsActive,Application_Owner__r.IsActive,id,Application_Owner__c,Application_Implementor__c,Access_Name__c,name,Employee__c from Asset__c where Employee__c in:empidset ';     
        system.debug('Query' + Query);
        finalstr = 'Name ,AssetName , Assetids , InActive Approver/Implementer , Employee\n';
        return Database.getQueryLocator(Query);  
    }
    global void execute(Database.BatchableContext BC,List<Asset__c> scope)  
    {  
          InActiveApprerAssetlist= new list<Asset__c>();
          InActiveImplementerAssetlist=new list<Asset__c>();
          String header = 'Name ,AssetName , AssetId , InActive Approver/Implementer , Employee\n';
          //finalstr = header ;
         for(Asset__c Acl : scope)
         {
              if(Acl.Application_Owner__r.IsActive==false)
              {
                  string recordString = Acl.Name +','+Acl.Access_Name__c+','+ Acl.id +','+Acl.Application_Owner__c+','+ Acl.Employee__c +'\n';
                  finalstr = finalstr + recordString;
                  InActiveApprerAssetlist.add(Acl);
               }   
              /*if(Acl.Application_Implementor__r.IsActive=false)
              {
                  string recordString = acl.Name +','+Acl.Access_Name__c+','+ Acl.id +','+Acl.Application_Implementor__c+','+ Acl.Employee__c +'\n';
                  finalstr = finalstr + recordString;
                  InActiveImplementerAssetlist.add(Acl);
              } */   
        }
    }
    global void finish(Database.BatchableContext BC)  
    { 
        System.debug('!!!!!!!finishfinal' + finalstr);
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string csvname= 'InActiveApproverAssets.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        //String[] toAddresses = new String[] {'skalamkar@colecapital.com'};
        String[] toAddresses = new String[] {'skalamkar@colecapital.com','ntambe@arcpreit.com','pcooper@arcpreit.com','RAnkaiah@arcpreit.com','VKumar@vereit.com'};
        //String[] bccAddresses = new String[] {'ntambe@colecapital.com','ksingh@colecapital.com'};
        String subject ='InActiveApproverAssets CSV';
        email.setSubject(subject);
        email.setBccSender(true);
        email.setToAddresses( toAddresses );
        //email.setBccAddresses(bccAddresses);
        email.setPlainTextBody('InActiveApproverAssets CSV ');
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }
 }