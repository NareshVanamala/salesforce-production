global class BDApprvalupdate implements Database.Batchable<SObject>
{
    global Database.QueryLocator Start(Database.BatchableContext BC)
     {
         string qry='select id,name,Account_name__c,CampaignMemeberId__c,Contact_Name__c from BDApprovalHelper__c where account_name__c=null and CampaignMemeberId__c!=null';
         return database.getQueryLocator(qry);
 }
 global void Execute(Database.BatchableContext BC,List<BDApprovalHelper__c > Scope)
 {
    set<string> clist=new set<string>();
   list<BDApprovalHelper__c> alist = new list<BDApprovalHelper__c>();
    //list<>
       for(BDApprovalHelper__c a1:Scope)
    {
       clist.add(a1.CampaignMemeberId__c);
    }    
    system.debug(clist.size());
    list<CampaignMember> c=[select id,ContactId,Account__c from CampaignMember where id in:clist];
    map<string,string>campaignmembertoAccount=new map<string,string>();
    for(CampaignMember ct:c)
     campaignmembertoAccount.put(ct.id,ct.Account__c);
   
    //system.debug(c.size());
    for(BDApprovalHelper__c a2:Scope)
    {
         if(a2.account_name__c==null)
          {
           a2.account_name__c = campaignmembertoAccount.get(a2.CampaignMemeberId__c);
            alist.add(a2);    
            }
      }    
       
    update alist; 
 }
global void finish(Database.BatchableContext BC)
    {
       user u=[select id,name,email from user where id=:userinfo.getuserid()];
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {u.email};
       String[] bccAddresses = new String[] {'sandeep.mariyala@polarisft.com'};
       mail.setToAddresses(toAddresses);
       mail.setBccSender(true);
       mail.setBccAddresses(bccAddresses);
       mail.setPlainTextBody('Done');
       mail.setSubject('Your Batch class processing has been finished.Please verify the changes in contact');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    } 
}