@isTest
public class NationalAccountTerritoriestestclass{
static testMethod void NationalAccountTerritoriestestmethod() {
  
  Account a = new Account();
  a.name = 'testaccount';
  insert a;
  System.assertEquals(a.name,'testaccount');

  National_Account_Helper__c c = new  National_Account_Helper__c();
  c.AllTerr_ofAllBdReps__c ='350'; 
  c.AllTerr_TotalterrVirtuals__c='350';
  c.AllTerrCYBdsales__c='121'; 
  c.AllTerrCyytd__c='345';
  c.AllTerrlastyr__c='123';
  c.AllTerrnoofReps__c='456';
  c.AllTerrpercentTotalTerrcalls__c='456';
  c.AllTerrPerecentTotalterrVisits__c='234';
  c.AllTerrSalesInception_c__c='67';
  c.AllTerrTotalCalls__c='78';
  c.AllTerrTotalVirtuals__c='5';
  c.AllTerrtotalVisits__c='45';
  c.Terr1_ofAllBdReps__c='78';
  c.Terr1_TotalterrVirtuals__c='89';
  c.Terr1CYBdsales__c='456';
  c.Terr1Cyytd__c='456';
  c.Terr1lastyr__c='456';
  c.Terr1noofReps__c='456';
  c.Terr1percentTotalTerrcalls__c='456';
  c.Terr1PerecentTotalterrVisits__c='456';
  c.Terr1SalesInception__c='456';
  c.Terr1TotalCalls__c='456';
  c.Terr1TotalVirtuals__c='456';
  c.Terr1totalVisits__c='456';
  c.Terr2_CY_BD_Sales__c='456';
  c.Terr2_ofAllBdReps__c='456';
  c.Terr2_Total_Terr_Calls__c='456';
  c.Terr2_Total__c='456';
  c.Terr2_Total_Terr_Visits__c='456';
  c.Terr2CYYTD__c='456';
  c.Terr2_CY_BD_Sales__c='456';
  c.Terr2_ofAllBdReps__c='456';
  c.Terr2_Total_Terr_Calls__c='456';
  c.Terr2_Total__c='456';
  c.Terr2_Total_Terr_Visits__c='456';
  c.Terr2CYYTD__c='456';
  c.Terr2LastYr__c='456';
  c.Terr2NoOfReps__c='456';
  c.Terr2SalesInception__c='456';
  c.Terr2TotalCalls__c='456';
  c.Terr2TotalVirtual__c='456';
  c.Terr2TotalVisits__c='456';
  c.Terr3_CY_BD_Sales__c='456';
  c.Terr3_ofAllBdReps__c='456';
  c.Terr3_Total_Terr_Calls__c='456';
  c.Terr3_Total_Terr_Virtual__c='456';
  c.Terr3_Total_Terr_Visits__c='456';
  c.Terr3CYYTD__c='456';
  c.Terr3LastYr__c='456';
  c.Terr3NoOfReps__c='456';
  c.Terr3SalesInception__c='456';
  c.Terr3TotalCalls__c='456';
  c.Terr3TotalVirtual__c='456';
  c.Terr3TotalVisits__c='456';
  c.Terr4_CY_BD_Sales__c='456';
  c.Terr4_ofAllBdReps__c='456';
  c.Terr4_Total_Terr_Virtual__c='456';
  c.Account__c=a.id;
  insert c;
  
  //c.Total_Cole_Tickets_Only_Funds_in_SF__c = 1;
  //c.Total_Cole_Production_Funds_in_SF__c = 1000;
  //c.Last_Producer_Date__c = System.Today();
  
  
  
    //PageReference pref = new PageReference();

    ApexPages.currentPage().getParameters().put('id',a.id);
    //Test.setCurrentPage(pref);

  
  ApexPages.StandardController controller = new ApexPages.StandardController(a); 
  NationalAccountTerritories x = new NationalAccountTerritories(controller);
  x.saveChanges();
  x.Edit1();
  x.getrecs();

}
}