@isTest
Public class LC_submitforApproval_Test
{
   static testmethod void testCreatelease()
  {
     
      MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.name='Test_MRI_Property';
         mc.Property_ID__c='b-00001';
         insert mc; 
         system.debug('This is my MRI Property'+mc);
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU';
         mylease.Suite_Sqft__c=1234;
         mylease.Lease_ID__c='A046677';
         mylease.MRI_Lease_ID__c ='A046677';
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         system.debug('This is my Lease'+mylease);
 
 
 
         User u=[select id,name,Territory__c,isactive from User where isactive=true limit 1];
         u.firstName='Testing';
         u.lastname='Approval';
         u.email='skalamkar@colecapital.com';
         update u;
         
         Centralized_Helper_Object__c  chb = new Centralized_Helper_Object__c() ;
         chb.Name ='Lease Central-I';
         chb.LeaseCentral_Approval_Instructions__c= ' Test Instructions';
         insert CHB;
        
         LC_LeaseOpprtunity__c lc1= new LC_LeaseOpprtunity__c();
         lc1.name='TestOpportunity';
         lc1.Lease_Opportunity_Type__c='Lease - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.LC_OtherOpportunity_Description__c='This is my description';
         lc1.First_Approver__c=u.id;
         lc1.Base_Rent__c=12345;
         lc1.LC_Already_Submitted__c=0;
         insert lc1;   
         
         
          list<attachment> myattalist= new list<attachment>(); 
         Blob b = Blob.valueOf('Test Data');  
         Attachment attachment = new Attachment();  
         attachment.ParentId = lc1.id;  
         attachment.Name = 'LETS From';  
         attachment.Body = b;  
         myattalist.add(attachment);
         insert myattalist;
 
      test.starttest();
 
        System.assertEquals(myLease.MRI_PROPERTY__c, lc1.MRI_Property__c);
      System.assertEquals(mc.Id,  myLease.MRI_PROPERTY__c);
      System.assertEquals( u.firstName, 'Testing');
      System.assertEquals( attachment.Name,'LETS From');
      

         ApexPages.currentPage().getParameters().put('ids',lc1.id);
         ApexPages.StandardController stdcon1 = new ApexPages.StandardController(lc1);
         LC_submitforApproval LCcontroller1 = new LC_submitforApproval (stdcon1);
         
       PageReference pref = Page.LC_LETSFormPdfPage;
         pref.getParameters().put('id',lc1.id);
         Test.setCurrentPage(pref);
                 Blob content;
           if (Test.IsRunningTest())
           {
                content=Blob.valueOf('UNIT.TEST');
           }
           else
           {
                content=pref.getContentAsPdf();
           }
                 
         LCcontroller1.LCOP=lc1;
         LCcontroller1.parentId=lc1.id;
         LCcontroller1.ids=lc1.id;
  
        /* try
         {
         pref = LCcontroller1.attachfile();
         
        }
         
        catch(Exception e)
        {
         
         }*/
         LCcontroller1.SubmitForApproval();
         LCcontroller1.Cancel();
 
      test.stoptest();
 
 }
}