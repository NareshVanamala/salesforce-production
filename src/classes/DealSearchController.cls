public with sharing class DealSearchController{
   
    public String sMail{get;set;}
    public String dealid{get;set;}
    public String dealcloneid{get;set;}
    public String cloneddealid{get;set;}
    public List<Deal__c> dealresults{get;set;}
    public List<cContact> contactList {get; set;}
    public List<cContact> selectedcontactList {get; set;}
    Deal__c selecteddealrec;
    List<Deal__c> selectedDeal = new List<Deal__c>();
    public DealSearchController() 
    {
       sMail=ApexPages.CurrentPage().getParameters().get('emid');
       system.debug('Check the value..'+sMail);
       dealresults=performSearch(sMail);
       selectedcontactList=new list<cContact>();
    } 
    
    public List<Deal__c> performSearch(string searchString) 
    {
       
        String soql = 'select id, name,AcquisitionDepartment__c,Property_Type__c,Contract_Price__c,Tenancy__c,Deal_Status__c,Deal_Type__c,Fund__c from Deal__c';
        if(searchString != '' && searchString != null)
        soql = soql +  ' where name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\'';
        soql = soql + ' limit 200';
        System.debug('***Query' + soql);     
       // if (Schema.sObjectType.Deal__c.isAccessible())

        return database.query(soql); 
    }
    
    public List<cContact> getContacts()
    {
        if(contactList == null) {
            contactList = new List<cContact>();
            for(Deal__c c:dealresults) {
            // As each contact is processed we create a new cContact object and add it to the contactList
                contactList.add(new cContact(c));
            }
        }
        return contactList;
    }
    
    public PageReference Clonedeal()
    {
      
         for(cContact dealwrapper1 : contactList){
          if(dealwrapper1.selected == true)
             selectedDeal.add(dealwrapper1.con);
          }
        
        System.debug('%%%%' + selectedDeal[0].id); 
        dealcloneid = selectedDeal[0].id;
        
       Deal__c newclonedeal;
       try
        {
          selecteddealrec = [Select id, name,Number_Of_Properties__c,Parking_Spaces__c,Land_Acres__c,Vacant_SF__c,Shop_SF__c,Population__c,Total_SF__c,Households__c,Year_Renovated__c,Median_Income__c,Year_Built__c,Range__c,Region__c,Ownership_Type__c,Tenancy__c,Zip_Code__c,Class__c,State__c,City__c,Primary_Use__c,Property_Type__c,Address__c,AcquisitionDepartment__c,Tenant_s__c,Cole_Initials__c,Portfolio_Deal__c,Source_Notes__c,Source_Company__c,Source_Contact__c ,Source_Type__c,Source__c,Deal_Status__c,Deal_Type__c,Fund__c,Contract_Price__c,Total_Price__c from Deal__c where id =: dealcloneid];
          newclonedeal = selecteddealrec.clone(false,true);
          insert newclonedeal;
          
          system.debug('&&&&newclone' + newclonedeal);
          
           //Copy related list - Rent Roll
            List<Rent_Roll__c> rentrolllist = new List<Rent_Roll__c>();
            for(Rent_Roll__c roll : [Select id,name,Name_of_Tenant__c,Reimb_Type__c,Lease_Type__c,Deal__c from Rent_Roll__c where Deal__c =: dealcloneid])
            {
                Rent_Roll__c newRentrollclone = roll.clone(false,true);
                newRentrollclone.Deal__c = newclonedeal.id;
                rentrolllist.add(newRentrollclone);
            }
            if (Schema.sObjectType.Rent_Roll__c.isCreateable())

            insert rentrolllist;
            system.debug('&&&&rentclone' + rentrolllist);
            //cloneddealid = newclonedeal.id;
            //System.debug('@@@@@@' + cloneddealid);
            
            //Copy related list - Acquisition Split
            List<Acquisition_Split__c> acqsplitlist = new List<Acquisition_Split__c>();
            for(Acquisition_Split__c split : [Select id,name,Acquisition__c,Acquisition_Split__c,Deal__c from Acquisition_Split__c where Deal__c =: dealcloneid])
            {
                Acquisition_Split__c newacqsplitclone = split.clone(false,true);
                newacqsplitclone.Deal__c = newclonedeal.id;
                acqsplitlist.add(newacqsplitclone);
            }
            if (Schema.sObjectType.Acquisition_Split__c.isCreateable())

            insert acqsplitlist;
            system.debug('&&&&acqsplit' + acqsplitlist);
            cloneddealid = newclonedeal.id;
            System.debug('@@@@@@' + cloneddealid);
        
         List<Loan_Relationship__c> loanrelationshiplist = new List<Loan_Relationship__c>();
            for(Loan_Relationship__c split : [Select id,name,Amort_I_O__c,Fixed_Floating__c,Deal__c,Index__c,Lender__c,Loan__c,Loan_Amount__c,LTV__c,Old_Loan_Relationship_DC_External_ID__c,Rate_Spread__c from Loan_Relationship__c where Deal__c =: dealcloneid])
            {
                Loan_Relationship__c newacqsplitclone = split.clone(false,true);
                newacqsplitclone.Deal__c = newclonedeal.id;
                loanrelationshiplist.add(newacqsplitclone);
            }
            if (Schema.sObjectType.Loan_Relationship__c.isCreateable())

            insert loanrelationshiplist;
            system.debug('&&&&acqsplit' + loanrelationshiplist);
            cloneddealid = newclonedeal.id;
            System.debug('@@@@@@' + cloneddealid);
        }  
        
        catch(Exception e){} 
        PageReference clonepage = new PageReference('/'+ cloneddealid+ '/e?retURL=%2F'+ cloneddealid);  
      return clonepage;
    }
    
    public PageReference Mergedeal()
    { 
         for(cContact dealwrapper : contactList){
           if(dealwrapper.selected == true)
             selectedDeal.add(dealwrapper.con);
          }
        
        //System.debug('%%%%' + selectedDeal[0].id);
        dealid = selectedDeal[0].id;
        //PageReference page = new PageReference('/apex/DealMergepage?eid='+dealid);
        PageReference page = new PageReference('/' + dealid+ '/e?retURL=%2F' + dealid);
        
        return page;
        
     }
     public PageReference createNewDeal()
     {
       String samp = System.Label.Current_Org_Url;
       PageReference ref = new PageReference(''+samp+'/setup/ui/recordtypeselect.jsp?ent=01I500000003M60&retURL=%2Fa3H%2Fo&save_new_url=%2Fa3H%2Fe%3FretURL%3D%252Fa3H%252Fo');
       return ref;
     }
   
     public class cContact
     {
        public Deal__c con {get; set;}
        public Boolean selected {get; set;}

        //This is the contructor method. When we create a new cContact object we pass a Contact that is set to the con property. We also set the selected value to false
        public cContact(Deal__c c) {
            con = c;
            selected = false;
       }
    }

   
   
   
   
   
     
}