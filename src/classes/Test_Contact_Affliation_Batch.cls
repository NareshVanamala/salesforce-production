@isTest
public class Test_Contact_Affliation_Batch
{  
    static testmethod void myTest_Method1()
    {
        Account a= new Account();
        a.name='TestBrokerDealerAccount';
        a.recordtypeid='012500000004uvF';
        a.type='Broker Dealer' ;
        insert a; 
        
        Recordtype rr1= [select id, name from Recordtype where name='RIA' and sobjecttype='Account'];   
        Account a1= new Account();
        a1.name='TestRIAAccount';
        a1.recordtypeid=rr1.id;    
        insert a1;
        
        //Recordtype RRcnt=[select id, name from Recordtype where name='NonInvestorRepContact' and sobjecttype='Contact'];        
        Contact c= new Contact();
        c.firstname='TestBrokerDealerContact'; 
        c.lastname='testing';
        c.accountid=a.id;
        c.Rep_CRD__c='988120';
        c.recordtypeid='012300000004rLn';
        c.Contact_Type__c='S & E Contact';
        c.Relationship__c='Attorney';
        insert c; 
        
         system.assertequals(c.accountid,a.id);

        
        //Recordtype Riacnt=[select id, name from Recordtype where name='Registered_Investment_Advisor_RIA_Contact' and sobjecttype='Contact'];            
        Contact c1= new Contact();
        c1.firstname='TestRiaContact'; 
        c1.lastname='testing11';
        c1.accountid=a1.id;
        c1.Rep_CRD__c='988120';
        c1.recordtypeid='012500000005Qqz';
        insert c1; 
          
       system.assertequals(c1.accountid,a1.id);
      
        
        ContactAffilationBatch_reengineered batch = new ContactAffilationBatch_reengineered();
         Id batchId = Database.executeBatch(batch);
                
       
    }
     
      
}