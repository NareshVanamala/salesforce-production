public with sharing class Cockpit_AdminReportClass
{
    //varible declarations.
    String ReportOptions= 'None';
    public List<SelectOption> CallListValues {set;get;}
    public String AdminReportCallListName {get;set;}
    public List<SelectOption> TerritoryValues {set;get;}
    public String Territorylist{get;set;}
    public integer totalcontactsCreated{set;get;}
    public integer totalcontactsRemaining{set;get;}
    public integer totalcontactscompleted{set;get;}
    public integer totalcontactsCreated1{set;get;}
    public integer totalcontactsRemaining1{set;get;}
    public integer totalcontactscompleted1{set;get;}
    public integer totalcontactsskipped{set;get;}
    public List<ContactCountWrappercls> ContactListCls {set;get;}
    public List<ContactCountWrappercls120> ContactListCls1 {set;get;}
    public List<ContactCountWrapperclsforCallandInternal> ContactListClsforCallandInternal{set;get;}

    public Cockpit_AdminReportClass(){
        set<String> call = new Set<String>();
        List<String> lscall = new List<String>();
        ContactListCls =new list<ContactCountWrappercls>();
        ContactListCls1=new list<ContactCountWrappercls120>(); 
        ContactListClsforCallandInternal=new list<ContactCountWrapperclsforCallandInternal>();
        //get all the Call list values
        try{
            TerritoryValues = new List<SelectOption>();
            list<string>contactterritorylist=new list<string>();
            string territoryTypes = system.label.Cockpit_Territory_Types;
            contactterritorylist = territoryTypes.split(';');          
            string objName='Contact';
            //Call the  helper class method to get the territory and riaterritory drop dwon values;
            string territoryValuesstring='';
            for(integer i=0;i<contactterritorylist.Size();i++){
                if(territoryValuesstring != null && territoryValuesstring != '')
                   territoryValuesstring=territoryValuesstring+';'+SobjectFieldSetHelperClass.getpicklistValuesfromtheschema(objName,contactterritorylist[i]);
                else
                   territoryValuesstring=territoryValuesstring+SobjectFieldSetHelperClass.getpicklistValuesfromtheschema(objName,contactterritorylist[i]);
            }
            system.debug('territoryValuesstring'+territoryValuesstring);
            system.debug('This is my Territory list'+territoryValuesstring);
            list<string>tlist=new list<String>();
            list<string>territorylist=new list<String>();
            Set<string>tlistset = new Set<string>();
            tlist =territoryValuesstring.split(';');
            tlistset.addAll(tlist);
            territorylist.addAll(tlistset);
            TerritoryValues.add(new SelectOption('Select Territory','Select Territory'));
            for(String s : territorylist)
                TerritoryValues.add(new SelectOption(s,s));
          
            if(TerritoryValues.size()==0)
              TerritoryValues.add(new SelectOption('None','None'));   
           system.debug('this is the<<<>>>>>'+territorylist );     
                   
        }
        catch(Exception e){}
        
       
        
    }
    
    public List<SelectOption> getItems()
    {
         List<SelectOption> options = new List<SelectOption>();
         options.add(new SelectOption('None','None'));
         options.add(new SelectOption('Territory','Territory'));
         options.add(new SelectOption('Call Lists','Call Lists'));
         //options.add(new SelectOption('All Call Lists','All Call Lists'));
         return options;
     }
     
     public void getCallListReport()
     {
         system.debug('AdminReportCallListName'+AdminReportCallListName);
          ContactListCls =new list<ContactCountWrappercls>();
          //ContactListCls = new List<ContactCountWrappercls>();
          system.debug('This is the Admin report based on Call list');
          //get the selected automated call list record and get all the roll up summary fields.
          Automated_Call_List__c actty=[select id,name,Total_Remaining_Calls__c,Contact_Call_Lists_Count__c,Total_Skipped_Calls__c,Total_Snoozed_Calls__c from Automated_Call_List__c  where name=:AdminReportCallListName];
          totalcontactsCreated=integer.valueOF(actty.Contact_Call_Lists_Count__c);
          totalcontactsRemaining=integer.valueOF(actty.Total_Remaining_Calls__c); 
          totalcontactscompleted=totalcontactsCreated-totalcontactsRemaining;
         //totalcontactsCreated=56678;
          System.debug('Heap Size--->0: ' + Limits.getHeapSize() + ' <---> ' + Limits.getLimitHeapSize());
          System.debug('Heap Size--->1: ' + Limits.getHeapSize() + ' <---> ' + Limits.getLimitHeapSize());
          List<AggregateResult> groupedResults=[select CallList_Name__c,Call_Complete__c, Skipped_Reason__c,IsDeleted__c,Contact_Territory__c, COUNT(id) cnt from Contact_Call_List__c where (CallList_Name__c=:AdminReportCallListName and IsDeleted__c=false and Contact_Territory__c!=null) GROUP BY Contact_Territory__c, Call_Complete__c, Skipped_Reason__c, CallList_Name__c,IsDeleted__c order by Contact_Territory__c ASC, CallList_Name__c ASC ];
          system.debug('Here are the grouped result'+groupedResults);
          String prevTerr;
          String prevCallList;
          integer prevRemaining =0;
          integer prevContacted =0;
          integer prevSkipped=0;
          Integer prevCount=0;
          integer contacted =0;
          integer remaining =0;
          integer skipped=0;
          integer firstEntry = 0;
          integer totalcreated=0;
          
    
        if(groupedResults.size()>0)
        {
            for(AggregateResult ar : groupedResults)
            {
               String terr= (String)ar.get('Contact_Territory__c');
               system.debug('This is my Territory'+terr);
               system.debug('This is my previous Territory'+prevTerr);
               
               Integer cnt = (Integer)ar.get('cnt');
               Boolean Status=(Boolean)ar.get('Call_Complete__c');
               totalcreated=cnt;
                if((Boolean.valueof(ar.get('Call_Complete__c')) == True)&&(String.valueof(ar.get('Skipped_Reason__c')) ==null ))
                {
                    contacted = integer.valueof(ar.get('cnt'));
                    remaining = 0;
                    skipped=0;
                }
                if(Boolean.valueof(ar.get('Call_Complete__c')) == False)
                {
                    remaining = integer.valueof(ar.get('cnt'));
                    contacted = 0;
                    skipped=0;
                }
                if((Boolean.valueof(ar.get('Call_Complete__c')) == True)&&(String.valueof(ar.get('Skipped_Reason__c'))!=null))
                {
                    skipped=integer.valueof(ar.get('cnt'));
                    contacted =0;
                    remaining = 0;
                }
                if((terr!=null) && (prevTerr!=null))
                {
                  
                      if (prevTerr== terr) 
                      {
                           prevCount= prevCount+ totalcreated;
                           totalcreated= prevCount; 
                            prevContacted=prevContacted+contacted ;
                           if (prevContacted > 0)  
                            contacted = prevContacted;
                           prevRemaining=prevRemaining+remaining;
                            if (prevRemaining > 0)
                           remaining = prevRemaining; 
                            prevSkipped= prevSkipped+ skipped;
                            if(prevSkipped>0)
                             skipped=prevSkipped;
                            
                       }
                       else if(prevTerr!= terr)
                       {                        
                            
                            system.debug('Thisismy previous territory'+prevTerr);
                            ContactCountWrappercls  tskcls = new ContactCountWrappercls (prevTerr, prevCount, prevContacted ,prevRemaining,prevSkipped);
                            ContactListCls.add(tskcls);
                            system.debug('<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>'+tskcls );
                            
                      }                   
                }
                prevRemaining = remaining;
                prevContacted = contacted;
                prevSkipped = skipped;     
                
                prevTerr= terr;
                prevCount= totalcreated;
                prevSkipped= skipped;
                
            }
            System.debug('******END********\n'+System.now());            
            ContactCountWrappercls tskcls = new ContactCountWrappercls(prevTerr, totalcreated, prevContacted ,prevRemaining,prevSkipped);
            ContactListCls.add(tskcls);
            system.debug('<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>'+tskcls );
            
        }
          
     }
     public void getTerritoryReport()
     {
         ContactListCls1=new list<ContactCountWrappercls120>(); 
         system.debug('Check the selected territory'+Territorylist);
         List<AggregateResult>TotalcreatedRec1 =[select  Contact_Territory__c,IsDeleted__c, COUNT(id) cnt from Contact_Call_List__c where  Contact_Territory__c =:Territorylist and IsDeleted__c=false and  Automated_Call_List__r.IsDeleted__c=false and Automated_Call_List__r.Archived__c=false group by  Contact_Territory__c,IsDeleted__c];
         List<AggregateResult>ComplatedContacts=[select  Contact_Territory__c,IsDeleted__c, COUNT(id) cnt1 from Contact_Call_List__c where(Contact_Territory__c =:Territorylist and Call_Complete__c=true and Skipped_Reason__c=null and IsDeleted__c=false and Automated_Call_List__r.IsDeleted__c=false and Automated_Call_List__r.Archived__c=false)group by  Contact_Territory__c,IsDeleted__c ];
         List<AggregateResult>RemainingContacts=[select  Contact_Territory__c,IsDeleted__c, COUNT(id) cnt2 from Contact_Call_List__c where(Contact_Territory__c =:Territorylist and Call_Complete__c=false and  IsDeleted__c=false and Automated_Call_List__r.IsDeleted__c=false and Automated_Call_List__r.Archived__c=false)group by  Contact_Territory__c,IsDeleted__c ];
         List<AggregateResult>skippedContacts=[select  Contact_Territory__c,IsDeleted__c, COUNT(id) cnt2 from Contact_Call_List__c where(Contact_Territory__c =:Territorylist and Call_Complete__c=true and Skipped_Reason__c!=null and  IsDeleted__c=false and Automated_Call_List__r.IsDeleted__c=false and Automated_Call_List__r.Archived__c=false)group by  Contact_Territory__c,IsDeleted__c ];
        
         If(TotalcreatedRec1.size()>0)
             totalcontactsCreated1=(integer)TotalcreatedRec1[0].get('cnt');
         else 
         totalcontactsCreated1= 0;
         
         If(ComplatedContacts.size()>0)
            totalcontactscompleted1=(integer)ComplatedContacts[0].get('cnt1');
        else 
           totalcontactscompleted1= 0;
           
        If(RemainingContacts.size()>0)
            totalcontactsRemaining1=(integer)RemainingContacts[0].get('cnt2');
        else 
           totalcontactsRemaining1= 0;
           
        If(skippedContacts.size()>0)
            totalcontactsskipped=(integer)skippedContacts[0].get('cnt2');
        else 
           totalcontactsskipped= 0;
           
       List<AggregateResult>groupbyCalllistNamesResult1=[select CallList_Name__c,Contact_Territory__c,Call_Complete__c,Snoozed__c,Skipped_Reason__c,IsDeleted__c, COUNT(id) cnt from Contact_Call_List__c where (CallList_Name__c!=null and Contact_Territory__c=:Territorylist and IsDeleted__c=false  and Automated_Call_List__r.IsDeleted__c=false and Automated_Call_List__r.Archived__c=false ) GROUP BY CallList_Name__c , Call_Complete__c, Skipped_Reason__c,Snoozed__c,Contact_Territory__c,IsDeleted__c order by  CallList_Name__c,Contact_Territory__c ASC];
       system.debug('<<<>>>GroupbyCalllistResult'+groupbyCalllistNamesResult1);
       
       String prevTerr;
        String prevCallList1;
        integer prevRemaining1 =0;
        integer prevContacted1 =0;
        integer prevSkipped1=0;
        Integer prevCount1=0;
        Integer prevSnoozed=0;
        integer contacted1 =0;
        integer remaining1 =0;
        integer skipped1=0;
        Integer Snoozed=0;
        integer firstEntry1 = 0;
        integer totalcreated1=0;
        //Date previousassigneddate=null;
        //Date assignedDate1;
        if(groupbyCalllistNamesResult1.size()>0)
        {
            for(AggregateResult ar : groupbyCalllistNamesResult1)
            {
               String terr= (String)ar.get('Contact_Territory__c');
                String call1= (String)ar.get('CallList_Name__c');
                Integer cnt1 = (Integer)ar.get('cnt');
                Boolean Status1=(Boolean)ar.get('Call_Complete__c');
               //assignedDate1=(date)ar.get('Assigned_Date__c');
                String name1=null;
                totalcreated1=cnt1;
               //if(asgned1!=null)
                 //name1 = usermap1.get(asgned1).Name;
               
                if((Boolean.valueof(ar.get('Call_Complete__c')) == True)&&(String.valueof(ar.get('Skipped_Reason__c')) ==null ))
                {
                    contacted1 = integer.valueof(ar.get('cnt'));
                    remaining1 = 0;
                    skipped1=0;
                    Snoozed=0;
                }
                
                if((Boolean.valueof(ar.get('Call_Complete__c')) == False)&&(Boolean.valueof(ar.get('Snoozed__c')) == False ))
                {
                    remaining1 = integer.valueof(ar.get('cnt'));
                    Snoozed= 0;
                    skipped1=0;
                    contacted1=0; 
                }
                 if((Boolean.valueof(ar.get('Call_Complete__c')) == False)&&(Boolean.valueof(ar.get('Snoozed__c')) == True ))
                {
                    Snoozed= integer.valueof(ar.get('cnt'));
                    remaining1 = 0;
                    skipped1=0;
                    contacted1=0;
                }
                if((Boolean.valueof(ar.get('Call_Complete__c')) == True)&&(String.valueof(ar.get('Skipped_Reason__c'))!=null))
                {
                    skipped1=integer.valueof(ar.get('cnt'));
                    contacted1 =0;
                    remaining1 = 0;
                    Snoozed=0;
                }
                
                if(call1!=null && prevCallList1!=null)
                {
                  // Check to see if we are still dealing with same name and call list
                  if (prevCallList1 == call1) 
                  {
                   // we are still same call list
                   // Check the status
                   // remaining = prevRemaining;
                   // contacted= prevcontacted;
                   prevCount1= prevCount1+ totalcreated1;
                   //previousassigneddate=assignedDate1;
                   totalcreated1= prevCount1;
                   prevContacted1 =prevContacted1 +contacted1 ;
                   if (prevContacted1 > 0)  
                    contacted1 = prevContacted1;
                   prevRemaining1 =prevRemaining1 +remaining1 ;
                    if (prevRemaining1 > 0)
                    remaining1 = prevRemaining1; 
                  
                    prevSkipped1= prevSkipped1+ skipped1;
                    if(prevSkipped1>0)
                     skipped1=prevSkipped1;
                     
                    prevSnoozed= prevSnoozed+Snoozed;
                    if(prevSnoozed>0)
                     Snoozed= prevSnoozed;  
                   }
                  else{                        
                        // dealing with new call list
                        system.debug('We are inserting in 1st else  loop inside for.. '+ prevCallList1 + prevCount1 + prevContacted1+ prevRemaining1);
                        ContactCountWrappercls120  tskcls11 = new ContactCountWrappercls120 (prevCallList1 , prevCount1, prevContacted1 ,prevRemaining1,prevSkipped1,prevSnoozed);
                        ContactListCls1.add(tskcls11);
                        //system.debug('Check the list priya'+ ContactListCls1);
                    }                   
                }
                prevRemaining1 = remaining1;
                prevContacted1 = contacted1;
                prevSkipped1 = skipped1; 
                prevSnoozed=Snoozed;    
                System.debug('name : ' + name1 +' call list ' + call1 + ' prevName ' + prevTerr + ' prev call list ' + prevCallList1 + ' contacted:' + contacted1 + 'remaining ' + remaining1 + ' prevcontacted ' + prevContacted1 + 'prevremaining ' + prevremaining1);
                prevTerr = terr;
                prevCount1= totalcreated1;
                prevSkipped1= skipped1;
                prevCallList1 = call1;
                //previousassigneddate=assignedDate1;
            }
            System.debug('******END********\n'+System.now());            
            // For the last entry, log the record   
            //system.debug('We are inserting outside for.loop. '+ prevName1+ totalcreated1+ prevContacted1+ prevRemaining1);
            ContactCountWrappercls120 tskcls11 = new ContactCountWrappercls120 (prevCallList1 , totalcreated1, prevContacted1 ,prevRemaining1,prevSkipped1,prevSnoozed);
            ContactListCls1.add(tskcls11);
            system.debug('Lets check the list.....'+ ContactListCls1);
        }
           
      
     }

    public String getReportOptions(){
            return ReportOptions;
        }
            
    public void setReportOptions(String ReportOptions){
            this.ReportOptions= ReportOptions;
        }
    public class ContactCountWrappercls 
    {  
        public String Territory{set;get;}
        public Integer completed1{set;get;}
        public Integer remaining {set;get;}
        public Integer count {set;get;} 
        public Integer skipped1 {set;get;}
        //public Date assinedToday {set;get;}
        //public String status1{set;get;}
        public ContactCountWrappercls (String Territorycheck,Integer CreatedContacts ,Integer contacted1 ,Integer remaining1,Integer skipped ){
            Territory= Territorycheck;
            count= CreatedContacts ;
            //count = cnt;
            completed1=contacted1;
            remaining=remaining1;
            skipped1 =skipped ;
            //assinedToday =assigneddate;
        }
    } 
    public class ContactCountWrappercls120 
    {  
        public String CallName {set;get;}
        public Integer completed120{set;get;}
        public Integer remaining120 {set;get;}
        public Integer count120 {set;get;} 
        public Integer skipped120 {set;get;}
        public Integer TotalSnoozed{set;get;}
        //public date assignedDate{set;get;}
        //public String status1{set;get;}
        public ContactCountWrappercls120 (String CallListName,Integer CreatedContacts ,integer contacted1 ,Integer remaining1,Integer skipped, Integer Snoozed){
            CallName = CallListName;
            count120= CreatedContacts ;
            //count = cnt;
            completed120=contacted1;
            remaining120=remaining1;
            skipped120 =skipped ;
            TotalSnoozed=Snoozed;
           // assignedDate=assignedDate1;
            
        }
      } 
      public void Results1forCallandInternal()
    {
      ContactListClsforCallandInternal = new List<ContactCountWrapperclsforCallandInternal>();
     // Map<Id,User> usermap = new Map<Id,User>([select Id,Name,Territory__c from User]);
      List<AggregateResult> groupedResultsforCallandInternal=[select CallList_Name__c,Contact_Territory__c,Call_Complete__c,Skipped_Reason__c,IsDeleted__c, COUNT(id) cnt from Contact_Call_List__c where (CallList_Name__c!=null and Contact_Territory__c!=null and IsDeleted__c=false and Automated_Call_List__r.Archived__c=false) GROUP BY CallList_Name__c, Call_Complete__c,Skipped_Reason__c, Contact_Territory__c,IsDeleted__c order by Contact_Territory__c ASC , CallList_Name__c ASC];
        //List<AggregateResult> groupedResultsforCallandInternal =[select Call_list__c,OwnerId,status,COUNT(id) cnt from task where (Call_list__c!=null and OwnerId!=null and (status='Not Started' or status='Open')) GROUP BY ROLLUP(Call_list__c ,OwnerId,status)];
       
       system.debug('groupedResultsgroupedResultsgroupedResultsgroupedResultsgroupedResults'+groupedResultsforCallandInternal.size());
        system.debug('groupedResultsgroupedResultsgroupedResultsgroupedResultsgroupedResults'+groupedResultsforCallandInternal);
        String prevNameforCallandInternal;
        String prevCallListforCallandInternal;
        integer prevRemainingforCallandInternal =0;
        integer prevContactedforCallandInternal =0;
        integer prevSkipped1forCallandInternal=0;
        integer contactedforCallandInternal =0;
        integer remainingforCallandInternal =0;
        integer firstEntryforCallandInternal = 0;
        integer skippedforCallandInternal=0;
        integer totalcreatedforCallandInternal=0;
        Integer prevCount1forCallandInternal=0;
        //date prevassigneddateforCallandInternal=null;
        //date assigneddateforCallandInternal;
       
        for (AggregateResult ar : groupedResultsforCallandInternal)
        {
          String nameforCallandInternal = (String)ar.get('Contact_Territory__c');
          String callforCallandInternal= (String)ar.get('CallList_Name__c');
          Integer cntforCallandInternal = (Integer)ar.get('cnt');  
          Boolean StatusforCallandInternal=(Boolean)ar.get('Call_Complete__c');
          //assigneddateforCallandInternal=(date)ar.get('Assigned_Date__c');
          totalcreatedforCallandInternal=cntforCallandInternal;
         
         // String nameforCallandInternal=null;
         // if(asgnedforCallandInternal!=null)
          //nameforCallandInternal = usermap.get(asgnedforCallandInternal).Name;
         
          if(Boolean.valueof(ar.get('Call_Complete__c')) == True)
           {
            contactedforCallandInternal = integer.valueof(ar.get('cnt'));
            remainingforCallandInternal = 0;
            skippedforCallandInternal=0;
            }
            if(Boolean.valueof(ar.get('Call_Complete__c')) == False)
            {
                remainingforCallandInternal = integer.valueof(ar.get('cnt'));
                contactedforCallandInternal = 0;
                skippedforCallandInternal=0;
            }
            if((Boolean.valueof(ar.get('Call_Complete__c')) == True)&&(String.valueof(ar.get('Skipped_Reason__c'))!=null))
                {
                    skippedforCallandInternal=integer.valueof(ar.get('cnt'));
                    contactedforCallandInternal =0;
                    remainingforCallandInternal = 0;
                }
            
            if(nameforCallandInternal!=null && prevNameforCallandInternal !=null)
            {
              // Check to see if we are still dealing with same name and call list
              if (prevNameforCallandInternal == nameforCallandInternal && prevCallListforCallandInternal == callforCallandInternal) {
               // we are still same call list
               // Check the status
               // remaining = prevRemaining;
               // contacted= prevcontacted;
               prevCount1forCallandInternal= prevCount1forCallandInternal+ totalcreatedforCallandInternal;
                   totalcreatedforCallandInternal= prevCount1forCallandInternal;
               if (prevContactedforCallandInternal > 0)  
                contactedforCallandInternal = prevContactedforCallandInternal;
                if (prevRemainingforCallandInternal > 0)
                remainingforCallandInternal = prevRemainingforCallandInternal;
                prevSkipped1forCallandInternal=prevSkipped1forCallandInternal+skippedforCallandInternal;
                if(prevSkipped1forCallandInternal>0)
                 skippedforCallandInternal=prevSkipped1forCallandInternal;    
               }
              else
                 {
                    // dealing with new call list
                    ContactCountWrapperclsforCallandInternal  tskclsforCallandInternal = new ContactCountWrapperclsforCallandInternal (prevNameforCallandInternal, prevCallListforCallandInternal,prevCount1forCallandInternal, prevContactedforCallandInternal ,prevRemainingforCallandInternal,prevSkipped1forCallandInternal);
                    ContactListClsforCallandInternal.add(tskclsforCallandInternal);
                }
               
            }
            prevRemainingforCallandInternal = remainingforCallandInternal;
            prevContactedforCallandInternal = contactedforCallandInternal;  
            prevSkipped1forCallandInternal= skippedforCallandInternal; 
            prevCount1forCallandInternal=totalcreatedforCallandInternal;
            //prevassigneddateforCallandInternal=assigneddateforCallandInternal;
              
            //System.debug('name : ' + name +' call list ' + call + ' prevName ' + prevName + ' prev call list ' + prevCallList + ' contacted:' + contacted + 'remaining ' + remaining + ' prevcontacted ' + prevContacted + 'prevremaining ' + prevremaining);
            prevNameforCallandInternal = nameforCallandInternal;
            prevCallListforCallandInternal = callforCallandInternal;
        }
        
        // For the last entry, log the record        
       ContactCountWrapperclsforCallandInternal tskclsforCallandInternal = new ContactCountWrapperclsforCallandInternal (prevNameforCallandInternal, prevCallListforCallandInternal,prevCount1forCallandInternal, prevContactedforCallandInternal ,prevRemainingforCallandInternal,prevSkipped1forCallandInternal);
        ContactListClsforCallandInternal.add(tskclsforCallandInternal);
        
 
        System.debug('NINAD NINAD NINAD NINAD Test'+ContactListClsforCallandInternal);
        // List<AggregateResult> groupedResults=[select Call_list__c,OwnerId,status, COUNT(id) cnt from task where (Call_list__c!=null and OwnerId!=null ) GROUP BY Call_list__c, status, ownerid  order by name, Call_list__c];
        
        //return groupedResults;
    }
     
    public class ContactCountWrapperclsforCallandInternal 
    {
    
        public String AssignedToforCallandInternal {set;get;}
        public String Call1forCallandInternal {set;get;}
        public Integer completed1forCallandInternal{set;get;}
        public Integer remainingforCallandInternal {set;get;}
        public Integer SkippedforCallandInternal {set;get;}
        public Integer countforCallandInternal {set;get;} 
        public String status1forCallandInternal{set;get;}
        public date assignedDate2forcallandInternal{set;get;}
        
      public ContactCountWrapperclsforCallandInternal (String assignforCallandInternal,String callforCallandInternal, integer TotalContacts,integer contacted1forCallandInternal ,Integer remaining1forCallandInternal,Integer SkippedContacts )
      {        
      AssignedToforCallandInternal  = assignforCallandInternal;
      Call1forCallandInternal =  callforCallandInternal;
      countforCallandInternal =TotalContacts;
     // Status1=status;
      //count = cnt;
      completed1forCallandInternal=contacted1forCallandInternal;
      remainingforCallandInternal=remaining1forCallandInternal;
      SkippedforCallandInternal =SkippedContacts;
      //assignedDate2forcallandInternal=assigneddateforcall; 
     }
       
        
        
        }
        
     public PageReference exportToExcel() {
        return page.Cockpit_CallListReportExcel;
      }
      
        
      
     public PageReference exportToExcel1() {
        return page.Cockpit_TerritoryReportExcel;
      }
      
     public PageReference exportToExcel2() {
        return page.Cockpit_AllCallListReportExcel;
      }
       
        
}