global virtual with sharing class ocms_Event_List extends cms.ContentTemplateController implements cms.ServiceInterface
{
    private List<SObject> ProductList;
    private List<SObject> ProductList1;
    private String html;
    
    //private String Firstname;
    //private String lastname;
    //public Map<String, String> stateMap = new Map<String, String>();
/* Service methods */
    public List<SObject> getProductList()
    {
        List<SObject> ProductList;
        set<id> cmpid=new set<id>();
        user usr = [Select Id, firstname, lastname, contactId from user where id=:UserInfo.getUserId()];
        list<CampaignMember> cm=[select id,CampaignId,ContactId,Status from CampaignMember where ContactId=:usr.contactid];                
        for(CampaignMember cm1:cm)
        {
        cmpid.add(cm1.CampaignId);
        } 
        String query = 'select Is_Webcast_Replay__c,title__c,id,Campaigns__c,Contacts__c,Description__c,Email_Address__c,iCalendar_URL__c,meeting_ID__c,Name__c,Start_Date_Time__c,Status__c,Viewble_on_website__c,Webcast_Replay_URL__c,Add_to_Calender__c from WebCast__c where Is_Event__c=true';
       query+= ' and Campaigns__c IN:cmpid order by Start_Date_Time__c desc';
        ProductList= Database.query(query );
        return ProductList;
    }
    
    global String executeRequest (Map <String, String> p) {
      String response = '{"success": false}';
      if (p.get('action') == 'getUpcomingEventsCount') {
        Integer newMessagesCount = 0;
        newMessagesCount = getUpcomingEventsCount();
        response = '{"success": true, "count":' + newMessagesCount + ' }';
      }
      return response;
    }
    
   @TestVisible private Integer getUpcomingEventsCount(){
      ProductList= getProductList();
      Integer count = ProductList.size();
      return count;
    }
    
    global System.Type getType() { return ocms_PortalMessage.class;}
    
      /* Create Content */
   @TestVisible private String writeControls()
    {
        String html = '';
        return html;
     }
    @TestVisible private String writeListView()
     {
         String html = '';
         html += '<article class="topMar7 wrapper teer3">'+
          '<div class="container">'+
          '<div class="tab_container">'+
          '<div class="tab_content" id="tab1">'+
          '<h1>Upcoming Events</h1>'+
                '<table class="table table-border clear web-table" width="100%">'+
                '<tbody>'+
                '<tr>'+
                '<td width="20%">'+
                 '<strong>Date / Time<strong>'+
                  '</td>'+
                   '<td width="20%">'+
                    '<strong>Title<strong>'+
                     '</td>'+
                    '<td width="50%">'+
                    '<strong>Description<strong>'+
                     '</td>'+
                      '<td width="15%">'+
                       '<strong>Status<strong>'+
                        '</td>'+
                        '<td width="20%">'+
                        '<strong>&nbsp;<strong>'+
                        '</td>'+
                        '<td width="20%">'+
                        '<strong>&nbsp;<strong>'+
                        '</td>'+
                        '</tr>';
         if (ProductList!= null && ProductList.size() > 0)
         {
         Map<string,string> cmmemstatus=new map<string,string>(); 
         set<id> cmpid=new set<id>();
         user usr = [Select Id, firstname, lastname, contactId from user where id=:UserInfo.getUserId()];
         list<CampaignMember> cm=[select id,CampaignId,ContactId,Status from CampaignMember where ContactId=:usr.contactid];                
         for(CampaignMember cm1:cm)
         {
         cmpid.add(cm1.CampaignId);
         cmmemstatus.put(cm1.CampaignId,cm1.status);
         } 
         for(SObject pl : ProductList)
           {
                       String myid= String.valueOf(pl.get('id'));
                       string reg=cmmemstatus.get(String.valueOf(pl.get('Campaigns__c')));
                       String myid123= String.valueOf(pl.get('Campaigns__c'));
                       
                       html += '<tr>'+
                            '<td>'+pl.get('Start_Date_Time__c')+'</td>'+
                            '<td>'+pl.get('Title__c')+'</td>'+
                            '<td>'+pl.get('Description__c') + '</br><h3><a href="/Webcast_detail?str=' + myid + '">View Details</a></h3></td>'+
                            '<td>'+cmmemstatus.get(String.valueOf(pl.get('Campaigns__c')))+'</td>';
                            if(reg!='RSVP')
                            {
                                html += '<td>'+'<h3><input class="webcast-button" type="button" value="RSVP" name="RSVP" onclick="RSVP(\''+myid+'\');"/></h3>'+'</td>';
                            }
                                                        
                            else if((reg=='RSVP')&&(pl.get('Add_to_Calender__c')==true))
                            {
                               html += '<td>'+'<h3><a class="webcast-button" href="/ocmsicsattachmenttest?id='+myid123+'" target="_blank"> Add to Calendar </a></h3>'+'</td>';
                            
                            }
                            html += ' </tr>';
           }

         }
                  html += ' </tbody>'+
                ' </table>'+
                ' </div>'+
                ' </div>'+
                ' <p>&nbsp;</p>'+
                '</div>'+
                '</article>';
                         
         return html;
        }
        
      @TestVisible private String writeMapView(){
         return '';
        }
        
               
        global override virtual String getHTML()
        {
            String html = '';
            
            html += writeControls();
            ProductList= getProductList();
            html += writeListView();
            html += '<script src="/resource/ocms_WebPropertyList/js/ocms_Eventlist.JS" type="text/javascript"></script>';
            return html;
        }

}