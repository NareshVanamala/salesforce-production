@isTest
public class Test_ocms_WebCast_DetailPage 
{
    public static testmethod void Test_ocms_WebCast_DetailPage () 
    { 
        
        string html='';
               
         profile pf=[select id,name from profile where name='Cole Capital Community'];
               
        account a=new account();
        a.name='test';
        insert a;
        
        Contact C= new Contact();
        C.firstName='Ninad';
        C.lastName='Tambe';
        c.accountid=a.id;
        insert C;
        
        User u = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = pf.id, country='United States',IsActive =true,
                ContactId = c.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert u;
        
        System.assertEquals(c.accountid,a.id);

        Campaign cmp= new Campaign ();
        cmp.name='Test-Webcast';
        cmp.IsActive=True;
        cmp.Status='Completed';
        insert cmp;
        
       
        list<WebCast__c> wblist=new list<WebCast__c>();
        for(integer i=0;i<1;i++) {
        WebCast__c wb= new WebCast__c();
        wb.name='Test';
        wb.Contacts__c= C.id;
        wb.Campaigns__c= cmp.id;
        wb.Start_Date_Time__c=system.now();
        wb.Description__c='Testing cole capital';
        wb.Status__c='Registered';
        wb.title__c='mr';
        wb.meeting_ID__c='test';
        wblist.add(wb);
        
         }
         insert wblist;  
         
       system.runas(u)
        {  
        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        System.currentPageReference().getParameters().put('id', wblist[0].id); 
        
        ocms_WebCast_DetailPage rc=new ocms_WebCast_DetailPage ();
        try
        {
            rc.writeControls();
            list<sobject> ProductList=rc.getProductList(); 
            rc.getHtml(); 
            rc.writeListView();
            
         }
          catch(Exception e)  {        }    
        }   
  }
  public static testmethod void Test_ocms_WebCast_DetailPage1() 
    { 
        string html='';
               
          profile pf=[select id,name from profile where name='Cole Capital Community'];
               
        account a=new account();
        a.name='test';
        insert a;
        
        Contact C= new Contact();
        C.firstName='Ninad';
        C.lastName='Tambe';
        c.accountid=a.id;
        insert C;
        
        User u = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = pf.id, country='United States',IsActive =true,
                ContactId = c.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert u;
        
        System.assertEquals(u.profileid,pf.id);
        
        Campaign cmp= new Campaign ();
        cmp.name='Test-Webcast';
        cmp.IsActive=True;
        cmp.Status='Completed';
        insert cmp;
        
       list<WebCast__c> wblist=new list<WebCast__c>();
       for(integer i=0;i<1;i++) {         
        WebCast__c wb= new WebCast__c();
        wb.name='Test';
        wb.Contacts__c= C.id;
        wb.Campaigns__c= cmp.id;
        wb.Start_Date_Time__c=system.now();
        wb.Description__c='Testing cole capital';
        wb.Status__c='Registered';
        wb.title__c='mr';
        wb.meeting_ID__c='test';
        wblist.add(wb);
        
         }
         insert wblist;  
         
       system.runas(u)
        {                 
      
       Test.setCurrentPageReference(new PageReference('Page.myPage'));
        System.currentPageReference().getParameters().put('str', wblist[0].id); 
        
       ocms_WebCast_DetailPage rc=new ocms_WebCast_DetailPage();
        try
        { 
            rc.writeControls();
            list<sobject> ProductList=rc.getProductList1();
            rc.getHtml(); 
            rc.writeListView();
         }
          catch(Exception e)  {        }     
        }  
  }
}