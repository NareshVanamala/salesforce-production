@isTest
private class Test_YearToDate
{
   public static testMethod void TestYeartodate() 
   {
     Test.StartTest();
     
     Account a = new Account();
     a.name = 'Test Account';
     insert a;
     
     
     Contact c = new Contact();
     c.firstname = 'xxx';
     c.lastname = 'yyy';
     c.accountid = a.id;     
     insert c;
     
              system.assertequals( c.accountid,a.id);

     REIT_Investment__c reit = new REIT_Investment__c();
     reit.Deposit_Date__c = System.Today();
     reit.Current_Capital__c = 10000;
     reit.Rep_Contact__c = c.id;
     reit.Fund__c = '3771';
     insert reit;
     
     REIT_Investment__c reit1 = new REIT_Investment__c();
     reit1.Deposit_Date__c = System.Today();
     reit1.Current_Capital__c = 10000;
     reit1.Rep_Contact__c = c.id;
     reit1.Fund__c = '3774';
     insert reit1;
     
     REIT_Investment__c reit2 = new REIT_Investment__c();
     reit2.Deposit_Date__c = System.Today();
     reit2.Current_Capital__c = 10000;
     reit2.Rep_Contact__c = c.id;
     reit2.Fund__c = '3775';
     insert reit2;
     
     REIT_Investment__c reit3 = new REIT_Investment__c();
     reit3.Deposit_Date__c = System.Today();
     reit3.Current_Capital__c = 10000;
     reit3.Rep_Contact__c = c.id;
     reit3.Fund__c = '3776';
     insert reit3;
     
     Contact c1 = new Contact();
     c1.firstname = 'xxx1';
     c1.lastname = 'yyy1';
     c1.accountid = a.id;  
    // c1.Income_NAV_A__c  = reit1.Current_Capital__c;
     c1.Income_NAV_A_First_Investment_Date__c = reit1.Deposit_Date__c;
     //Income_NAV_A_Tickets__c   
     insert c1;
     
    // c1.Income_NAV_A__c = c1.Income_NAV_A__c + reit1.Current_Capital__c;
     update c1;
     
     
     
     
     c.Year_To_Date__c = reit.Current_Capital__c + reit1.Current_Capital__c;
     update c;
     
  
   }
     
   }