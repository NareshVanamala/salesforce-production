global with sharing class batchEventdelete implements Database.Batchable<sObject>
{

 global List<Event> Eventlist= new List<Event>();
    
      global  Set<Id> Ids = new Set<Id>();
       global  Set<Id> dIds = new Set<Id>();
    global  List<PublicCalendar__c> Pc = new List<PublicCalendar__c>();
     
    

global batchEventdelete(){
   
   
                // Batch Constructor
   pc= [select id__c from PublicCalendar__c];
    Ids.add(Pc[0].Id__c);
   system.debug('$$'+ids);
   
     }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
    
  //  string ids='a3DW00000008ewp';
    
        String query = 'SELECT Id,Deal_Status__c  FROM Deal__c Where Deal_status__c =\'Dead-Legal' + '\'';
                         
                          
        system.debug('##'+query);
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Deal__c> scope)
    {
     
         for(Deal__c d : scope)
         {
         dIDs.add(d.id);
         }
         
             EventList = [select id,whatid,Ownerid,Appointment_Status__c from Event where whatid in:dids and OwnerId =:Ids];
           System.debug('@@'+EventList.size());
         
    If(EventList.size()!=null)
    {
     system.debug('++++++'+EventList);
     
      if (Event.sObjectType.getDescribe().isDeletable()) 
        {
            Delete [select id from Event where id in:EventList];  
        }
    }       
    }   
    global void finish(Database.BatchableContext BC)
    {
    }
}