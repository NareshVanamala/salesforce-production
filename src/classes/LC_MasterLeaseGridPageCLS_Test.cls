@isTest(SeeAllData=True)
public class LC_MasterLeaseGridPageCLS_Test
{
    static testMethod void LC_MasterLeaseGridPageCLS() {
        
        Mri_property__c mri= new Mri_property__c();
        mri.name='Test';
        mri.Property_ID__c='1234';
        mri.NOI__c=123.4;
        insert  mri;
        
        Lease__c Le = new Lease__c();
        Le.Name='Test';
        Le.Mri_property__c =mri.id;
        le.Master_Lease__c=True;
        le.Expiration_Date__c=System.today();
        le.MRI_Lease_ID__c='E1234';
        le.Tenant_Name__c='Test Tenant';
        le.Suite_Sqft__c=234;
        insert Le;
        
        Lease__c Le1 = new Lease__c();
        Le1.Name='Test1';
        Le1.Mri_property__c =mri.id;
        Le1.Master_Lease__c=True;
        Le1.Expiration_Date__c=System.today();
        Le1.MRI_Lease_ID__c='E1234';
        Le1.Tenant_Name__c='Test Tenant';
        Le1.Suite_Sqft__c=234;
        insert Le1;
        
        
        Lease__c Le2 = new Lease__c();
        Le2.Name='Test2';
        Le2.Mri_property__c =mri.id;
        Le2.Master_Lease__c= True;
        Le2.Expiration_Date__c=System.today();
        Le2.MRI_Lease_ID__c='E1234';
        Le2.Tenant_Name__c='Test Tenant';
        Le2.Suite_Sqft__c=234;
        insert Le2;


        LC_LeaseOpprtunity__c lo= new LC_LeaseOpprtunity__c();
        lo.Name='Test Lease Opp';
        lo.MRI_Property__c=mri.id;
        lo.Lease__c=le.id;
        lo.Lease_Opportunity_Type__c=' Master Lease - New';
        insert lo;  

        Helper_for_Master_Lease__c hml = new Helper_for_Master_Lease__c();
        hml.Lease__c=hml.Lease__c;
        hml.MRI_Property__c=hml.MRI_Property__c;
        hml.Lease_Opportunity__c=lo.id;
        hml.Additional_Information__c='Test Information';
        hml.Building_Id__c=mri.Property_ID__c;
        hml.Name__c=mri.name;
        hml.GLA_Suite_SQFT__c=le1.Suite_Sqft__c;
        hml.Going_on_NOI1__c=mri.NOI__c;
        hml.Lease_Expiration_Date__c= Le1.Expiration_Date__c;
        hml.Master_Lease_Id__c= Le1.MRI_Lease_ID__c;
        insert hml;

        Helper_for_Master_Lease__c hml1 = new Helper_for_Master_Lease__c();
        hml1.Lease__c=hml.Lease__c;
        hml1.MRI_Property__c=hml.MRI_Property__c;
        hml1.Lease_Opportunity__c=lo.id;
        hml1.Additional_Information__c='Test Information';
        hml1.Building_Id__c=mri.Property_ID__c;
        hml1.Name__c=mri.name;
        hml1.GLA_Suite_SQFT__c=le1.Suite_Sqft__c;
        hml1.Going_on_NOI1__c=mri.NOI__c;
        hml1.Lease_Expiration_Date__c= Le1.Expiration_Date__c;
        hml1.Master_Lease_Id__c= Le1.MRI_Lease_ID__c;
        insert hml1;
      
        Test.startTest();
        
        System.assertEquals(le.MRI_PROPERTY__c , mri.id);
        System.assertNotEquals(le1.name,le2.name);        
        System.assertEquals( hml.Lease_Opportunity__c,lo.id);
        System.assertEquals( hml1.Lease_Opportunity__c,lo.id);

        
        Id MRID = lo.MRI_Property__r.id;
        ApexPages.StandardController stdcon = new ApexPages.StandardController(lo);
        ApexPages.currentPage().getParameters().put('id',lo.id);
        LC_MasterLeaseGridPageCLS lmg=new LC_MasterLeaseGridPageCLS(stdcon);
        lmg.ShowMasterGrid=True;
        lmg.Selected=True;
        lmg.recId = Lo.Id; 
       for (LC_MasterLeaseGridPageCLS.wrapLease  wc : lmg.wrapLeaseList) 
        {
            wc.selected  = true;

        lmg.Selectrec(); 
        }
            
        Test.stopTest(); 
    }
}