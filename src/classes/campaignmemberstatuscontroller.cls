public with sharing class campaignmemberstatuscontroller {
public boolean abc{get;set;}
    public PageReference updatestatus() {
    
    Id id1=ApexPages.currentPage().getParameters().get('str');
    if(id1!=null){
    try
    {
    user usr = [Select Id, firstname, lastname, contactId from user where id=:UserInfo.getUserId()];
    WebCast__c wc=[select Is_Webcast_Replay__c,id,Campaigns__c,Contacts__c,Description__c,Email_Address__c,iCalendar_URL__c,meeting_ID__c,Name__c,Start_Date_Time__c,Status__c,Viewble_on_website__c,Webcast_Replay_URL__c,Add_to_Calender__c from WebCast__c where id=:ApexPages.currentPage().getParameters().get('str')];
    CampaignMember cm=[select id,CampaignId,ContactId,Status from CampaignMember where ContactId=:usr.contactid and CampaignId=:wc.Campaigns__c];                
    cm.status='RSVP'; 
    if (Schema.sObjectType.CampaignMember.isUpdateable()) 
    update cm; 
     }
     
     catch(Exception e)
     {
      abc=false;
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Pleae make sure that you have a contact on your name!'));
     }
    }
    
    Id id2=ApexPages.currentPage().getParameters().get('id');
    if(id2!=null){
    try
    {
    user usr = [Select Id, firstname, lastname, contactId from user where id=:UserInfo.getUserId()];
    WebCast__c wc=[select Is_Webcast_Replay__c,id,Campaigns__c,Contacts__c,Description__c,Email_Address__c,iCalendar_URL__c,meeting_ID__c,Name__c,Start_Date_Time__c,Status__c,Viewble_on_website__c,Webcast_Replay_URL__c,Add_to_Calender__c from WebCast__c where id=:ApexPages.currentPage().getParameters().get('id')];
    CampaignMember cm=[select id,CampaignId,ContactId,Status from CampaignMember where ContactId=:usr.contactid and CampaignId=:wc.Campaigns__c];                
    cm.status='Registered'; 
    if (Schema.sObjectType.CampaignMember.isUpdateable()) 
    update cm; 
     }
     
     catch(Exception e)
     {
      abc=false;
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Pleae make sure that you are an user of ARCP!'));
     }
    }
     
    if((id1==null) && (id2==null))
    {
    abc=false;
    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Pleae make sure that you are an user of ARCP!'));

    } 
        return null;
    }

public campaignmemberstatuscontroller()
{
abc=true;
}

}