@isTest
private class Test_Marektingsupportfee 
{
   public static testMethod void TestforMarektingsupportfee() 
   {
    Id conid=null;
    Id id1=null;
    Id id2=null;
    Profile p = [select id from profile where name='System Administrator'];
    User us=new User(alias = 'sandym1', email='sandym1@noemail.com',
            emailencodingkey='UTF-8', lastname='Sandymar', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id, country='India',
            timezonesidkey='America/Los_Angeles', username='sandym1@noemail.com');
        insert us;
   Id rrrecordtype = [select Id, Name,DeveloperName from RecordType where DeveloperName ='NonInvestorRepContact' and SobjectType = 'Contact'].id;
        
    Account acc = new Account();
        acc.Name = 'Test Prospect Account';
        Insert acc;
        
    Contact c = new Contact();
       c.lastname = 'Cockpit';
       c.Accountid = acc.id;
       c.Relationship__c ='Accounting';
       c.Contact_Type__c = 'Cole';
       c.Wholesaler_Management__c = 'Producer A';
       c.Territory__c ='Chicago';
       c.RIA_Territory__c ='Southeast Region';
       c.RIA_Consultant_Picklist__c ='Brian Mackin';
       c.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c.Wholesaler__c='Andrew Garten';
       c.Regional_Territory_Manager__c ='Andrew Garten';
       c.Internal_Wholesaler__c ='Aaron Williams';
       c.Territory_Zone__c = 'NV-50';
       c.recordtypeid = rrrecordtype;
       c.Priority__c='1';
       c.Next_Planned_External_Appointment__c = null; 
       c.Next_External_Appointment__c = null; 
       insert c;
                  
   conid=c.id;
    
    Event_Automation_Request__c ear = new Event_Automation_Request__c();
        ear.Name='BDE13006';
        ear.Event_Name__c='testeventname';
        ear.Start_Date__c=System.Today();
        ear.End_Date__c=System.Today();
        ear.Broker_Dealer_Name__c=acc.id;
        ear.Event_Location__c='testlocation';
        ear.Key_Account_Manager_Name_Requestor__c=us.id;
        ear.Event_Venue__c='testvenue';
        ear.testingfield__c='';
        ear.Sponsor_Cost__c=50000;
        ear.Rep_Name_FBO__c=c.id;
        insert ear;
        system.assert(ear!=Null);
   
    id1=ear.id;    
    id2=ear.id; 
        
    Event_Automation_Request__c ear1 = new Event_Automation_Request__c();
        ear1.Name='BDE13006';
        ear1.Event_Name__c='testeventname';
        ear1.Start_Date__c=System.Today();
        ear1.End_Date__c=System.Today();
        ear1.Broker_Dealer_Name__c=acc.id;
        ear1.Event_Location__c='testlocation';
        ear1.Key_Account_Manager_Name_Requestor__c=us.id;
        ear1.Event_Venue__c='testvenue';
        ear1.testingfield__c='';
        ear1.Sponsor_Cost__c=50000;
        ear1.Rep_Name_FBO__c=c.id;
        insert ear1;
        system.assert(ear1 != Null);

        delete ear1;

    pagereference pageref = system.Currentpagereference();
        pageref.getParameters().put('id',c.id); 
    
    pagereference pageref1 = system.Currentpagereference();
        pageref1.getParameters().put('edt',ear.id); 
        
    pagereference pageref2 = system.Currentpagereference();
        pageref2.getParameters().put('dlt',ear.id); 
        
    ApexPages.StandardController controller = new ApexPages.StandardController(c);
        Marektingsupportfee sc = new Marektingsupportfee(controller);
        sc.editrow();
        sc.deleterow();
        sc.newevent(); 
   }
}