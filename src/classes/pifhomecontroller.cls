public with sharing class pifhomecontroller {

public PageReference newpifform1() {
        
        PageReference NewHireFormPage = new PageReference ('/apex/newpifform');
        NewHireFormPage .setRedirect(true);
        return NewHireFormPage ;
        
    }

}