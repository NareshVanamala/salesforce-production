public with sharing class ContactCheckBox
 {
public Contact theSObject {get;set;}
public Boolean ren {get;set;}
private Apexpages.StandardController controller; 
public List<String> toUsers = new List<String>();
public List<String> ccUsers = new List<String>();
public List<String> toAddress = new List<String>();
public List<String> ccAddress = new List<String>();
String DisplayNameUsers;
String DisplayNameAddress;

String contactType='';
public ContactCheckBox(ApexPages.StandardController controller) {

    this.controller = controller;
      //  theSObject = new Contact();
      //  ren =true;
    //contact conn = new contact();
     
 
  theSObject = [select id,Inav_Referal__c,Reason_To_Uncheck__c,RIA_Territory__c,Dual_channel_Focus__c,Internal_Consultant_Picklilst__c,RIA_Consultant_Picklist__c,Account.Name,Account.X1031_Selling_Agreement__c,Internal_Consultant__c,Internal_Wholesaler__c,Wholesaler__c from contact where id=:apexpages.currentpage().getparameters().get('id')];
 If(theSObject.Account.X1031_Selling_Agreement__c!=null)
 {
 
If(theSObject.RIA_Consultant_Picklist__c!=null && theSObject.RIA_Consultant_Picklist__c!='Not Assigned')
{
    toUsers.add(theSObject.RIA_Consultant_Picklist__c);
    System.debug('toUsers======>>>'+toUsers);
}
  
if(theSObject.Internal_Consultant_Picklilst__c!='Not Assigned')
{
    ccUsers.add(theSObject.Internal_Consultant_Picklilst__c);
}  
If(theSObject.Internal_Wholesaler__c!=null && theSObject.Internal_Wholesaler__c!='Not Assigned')
{
    ccUsers.add(theSObject.Internal_Wholesaler__c);
}  
If(theSObject.Wholesaler__c!='Not Assigned' && theSObject.Wholesaler__c!='N/A' && theSObject.Wholesaler__c!='North Central')
{
    ccUsers.add(theSObject.Wholesaler__c);
}  
if(theSObject.Wholesaler__c!='Not Assigned' && theSObject.Wholesaler__c!='N/A' && theSObject.Wholesaler__c!='North Central')

{
   DisplayNameUsers= theSObject.Wholesaler__c;
}

}
  
  
System.debug('ccUsers======>>>'+ccUsers);
    

List<User> us = [Select id , Name,Email from User where Name IN :toUsers ] ; 
    
    for(User u :us)
    {
        toAddress.add(u.Email);
    }
    
List<User> usr = [Select id , Name,Email from User where Name IN :ccUsers ] ; 
    
    for(User u :usr)
    {
        ccAddress.add(u.Email);
      
    }
       ccAddress.add('Mike.Bacon@colecapital.com');
        ccAddress.add('Brian.Rivera@colecapital.com');
        ccAddress.add('Sean.Morris@colecapital.com');
        ccAddress.add('John.Sabey@colecapital.com');
        ccAddress.add('Marketintelligencestrategy@colecapital.com');

User usr1 =null;

if(DisplayNameUsers !=null){
 usr1  =[Select id ,Name,Email from User where Name =:DisplayNameUsers Limit 1] ; 
 }
   
    if(usr1!=null){
    DisplayNameAddress=usr1.Email;
    }
    
    System.debug('>>>>>>>>'+DisplayNameUsers);

    
System.debug('##########'+contactType);
System.debug('toAddress======>>>'+toAddress);
System.debug('ccAddress======>>>'+ccAddress);
System.debug('Dual channel Focus'+theSObject.Dual_channel_Focus__c);
If(theSObject.Dual_channel_Focus__c)
{
ContactType = 'This Contact is Dual Channel Focus.';
}
else{
ContactType = 'This Contact is not Dual Channel Focus.';
}

If(!theSObject.Inav_Referal__c)
    {
    //ren =true;
    }else
    {
   // ren = false ;
    }
 }
    
Public Pagereference save()
{
update theSObject ;
if(theSObject.Inav_Referal__c){
string str;
if(theSObject.Account.X1031_Selling_Agreement__c!=null)
try{
str = theSObject.Account.X1031_Selling_Agreement__c;
List<string> ref = str.split(';');
Map<string,string> m = new map<string,string>();

if(ref.size()>0){
for(integer i=0;i<ref.size();i++)
{
    m.put(ref.get(i),ref.get(i));
}
}

If(m.containskey('Income NAV - W')||m.containskey('Income NAV - A')||m.containskey('Income NAV - I')||m.containskey('Income NAV')) {
List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
   Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
email.setToAddresses(toAddress);
// Email.setTOAddresses(new String[]{'Pratyush.reddy@colereit.com'});
//  Email.setCCAddresses(new String[]{'Ninad.Tambe@colereit.com'});
 Email.setCCAddresses(ccAddress);
 Email.setBccAddresses(new String[]{'Ninad.Tambe@colereit.com'});
  email.setReplyTo(DisplayNameAddress);
  String samp = System.Label.Current_Org_Url;
   if(theSobject.RIA_Territory__c!=null && theSObject.RIA_Territory__c!='Not Assigned' )
   {
     
    email.setSubject('Inav Referral');
   email.setPlainTextBody(theSObject.Wholesaler__c+','+'has referred a contact for his/her interest in INAV product.'+ '\r\n'+'\r\n'+ContactType+'\r\n'+'\r\n'+'Link to profile:'+ '\r\n'+'\r\n'+''+samp+'/'+theSObject.id+'\r\n'+'\r\n'+ 'Thank you');
   }
  else
   {
   email.setSubject('Inav Referral-No RIA Territory Assigned');
   email.setPlainTextBody(theSObject.Wholesaler__c+','+'has referred a contact for his/her interest in INAV product.'+ '\r\n'+'\r\n'+ContactType+'\r\n'+'\r\n'+'The following profile has no RIA territory Assigned.'+'\r\n'+'\r\n'+'Link to profile:'+ '\r\n'+'\r\n'+''+samp+'/'+theSObject.id+'\r\n'+'\r\n'+ 'Thank you');
   }
   emails.add(email);


   Messaging.sendEmail(emails);
    }
 }
 catch(exception e){}
}
if(!theSObject.Inav_Referal__c){
string str;
if(theSObject.Account.X1031_Selling_Agreement__c!=null)
try{
str = theSObject.Account.X1031_Selling_Agreement__c;
List<string> ref = str.split(';');
Map<string,string> m = new map<string,string>();

System.debug('str ======>>>'+str );
System.debug('ref ======>>>'+ref );
System.debug('m======>>>'+m);


if(ref.size()>0){
for(integer i=0;i<ref.size();i++)
{
    m.put(ref.get(i),ref.get(i));
}
}
If(m.containskey('Income NAV - W')|| m.containskey('Income NAV - A')|| m.containskey('Income NAV - I') || m.containskey('Income NAV')) {
List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
   Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
  //  Email.setTOAddresses(new String[]{'Pratyush.reddy@colereit.com'});
   //  Email.setCCAddresses(new String[]{'Ninad.Tambe@colereit.com'});
   email.setToAddresses(toAddress);
  Email.setCCAddresses(ccAddress);
 Email.setBccAddresses(new String[]{'Ninad.Tambe@colereit.com'});
   email.setReplyTo(DisplayNameAddress);
   String samp = System.Label.Current_Org_Url;
  if(theSobject.RIA_Territory__c!=null && theSObject.RIA_Territory__c!='Not Assigned' )
   {
    email.setSubject('Inav Referral Cancelled');
   email.setPlainTextBody(theSObject.Wholesaler__c+','+'has cancelled a referral.'+ '\r\n'+'\r\n'+'Reason:'+theSObject.Reason_TO_uncheck__c+'\r\n'+'\r\n'+ContactType+'\r\n'+'\r\n'+'Link to profile:'+ '\r\n'+'\r\n'+''+samp+'/'+theSObject.id+'\r\n'+'\r\n'+ 'Thank you');
   }
  else
   {
   email.setSubject('Inav Referral Cancelled-No RIA Territory Assigned');
   email.setPlainTextBody(theSObject.Wholesaler__c+','+'has cancelled a referral.'+ '\r\n'+'\r\n'+'Reason:'+theSObject.Reason_TO_uncheck__c+'\r\n'+'\r\n'+ContactType+'\r\n'+'\r\n'+'The following profile has no RIA territory Assigned.'+'\r\n'+'\r\n'+'Link to profile:'+ '\r\n'+'\r\n'+''+samp+'/'+theSObject.id+'\r\n'+'\r\n'+ 'Thank you');
   }
   emails.add(email);


   Messaging.sendEmail(emails);
    }
    }
    catch(exception e){}
}
/*if(theSObject.Inav_Referal__c){
string str;
if(theSObject.Account.X1031_Selling_Agreement__c!=null)
try{
str = theSObject.Account.X1031_Selling_Agreement__c;
List<string> ref = str.split(';');
Map<string,string> m = new map<string,string>();

System.debug('str ======>>>'+str );
System.debug('ref ======>>>'+ref );
System.debug('m======>>>'+m);


if(ref.size()>0){
for(integer i=0;i<ref.size();i++)
{
    m.put(ref.get(i),ref.get(i));
}
}
If((m.containskey('Income NAV – W')||m.containskey('Income NAV – A')||m.containskey('Income NAV – I') ||m.containskey('Income NAV')) && (theSobject.RIA_Territory__c==null || theSObject.RIA_Territory__c=='Not Assigned')){
List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
   Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
 //Email.setTOAddresses(new String[]{'Pratyush.reddy@colereit.com'});
  Email.setTOAddresses(new String[]{'Gene.Patent@colecapital.com'});
 // Email.setCCAddresses(ccAddress);
  email.setReplyTo(DisplayNameAddress);
   email.setSubject('Inav Referral No Territory Assigned');
   email.setPlainTextBody('The following profile has no territory assigned.'+ '\r\n'+'\r\n'+'Link to profile:'+ '\r\n'+'\r\n'+'https://cole--stage.cs4.my.salesforce.com/'+theSObject.id+'\r\n'+'\r\n'+ 'Thank you');
   emails.add(email);


   Messaging.sendEmail(emails);
    }
    }
    catch(exception e){}
}*/
if(theSObject.Account.X1031_Selling_Agreement__c!=null)
{
try{
string str = theSObject.Account.X1031_Selling_Agreement__c;
List<string> ref = str.split(';');
Map<string,string> m = new map<string,string>();

System.debug('str ======>>>'+str );
System.debug('ref ======>>>'+ref );
System.debug('m======>>>'+m);


if(ref.size()>0){
for(integer i=0;i<ref.size();i++)
{
    m.put(ref.get(i),ref.get(i));
}
}
if(theSObject.Inav_Referal__c && (m.containskey('Income NAV - W')|| m.containskey('Income NAV - A')|| m.containskey('Income NAV - I')||m.containskey('Income NAV')))
{

   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Inav Referral Email has been sent.');
         apexpages.addmessage(myMsg);
}
if(!theSObject.Inav_Referal__c && (m.containskey('Income NAV - W')|| m.containskey('Income NAV - A')|| m.containskey('Income NAV - I')||m.containskey('Income NAV')))
{

   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Inav Cancellation Email has been sent.');
         apexpages.addmessage(myMsg);
}
if(theSObject.Inav_Referal__c && !(m.containskey('Income NAV - W')|| m.containskey('Income NAV - A')|| m.containskey('Income NAV - I')||m.containskey('Income NAV')))
{

   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Inav Referral Email has not been sent because the contact is part of an Account without an Inav SGA.');
         apexpages.addmessage(myMsg);
}
if(!theSObject.Inav_Referal__c && !(m.containskey('Income NAV - W')|| m.containskey('Income NAV - A')|| m.containskey('Income NAV - I')||m.containskey('Income NAV')))
{

   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Inav cancellation Email has not been sent because the contact is part of an Account without an Inav SGA.');
         apexpages.addmessage(myMsg);
}
}
catch(exception e){}
}
if(theSObject.Account.X1031_Selling_Agreement__c==null)
{
ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Inav Email cannot be sent because the contact is part of an Account without an Inav SGA.');
         apexpages.addmessage(myMsg);
}
return null ; 

}
Public Pagereference cancel()
{
return null;
}    
 
public Pagereference changeSection(){

   If(!theSObject.Inav_referal__c == true){
     // ren =true;
   
    
    ren = true;
      
   }
If(theSObject.Inav_referal__c == true){

     ren =false;
     
   }
   
   
   return  null;

 }

}