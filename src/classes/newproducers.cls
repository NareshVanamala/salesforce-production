public with sharing class newproducers {

    public list<Contact> conList{get;set;}
    public String name{get;set;}
    public String address{get;set;}
    public String city{get;set;}
    public String state{get;set;}
    public String zip{get;set;}
    public Decimal totalcole{get;set;}
    
    public newproducers(ApexPages.StandardController controller) {
      
       Date d1 = Date.Today();
       Date d2 = Date.Today()-30;
       
      // Account a = [select id, name from Account where id='001R000000eNKON'];
       Account a=[select id, name from Account where id = :ApexPages.currentPage().getParameters().get('id')];
       //Account a=[select id, name from Account where id = '001P000000adMsB'];
       conList=[select id,name,MailingStreet,MailingCity,MailingState,MailingPostalCode,Total_Cole_Tickets_Only_Funds_in_SF__c,Total_Cole_Production_Funds_in_SF__c,Last_Producer_Date__c from Contact where accountid =: a.id and Last_Producer_Date__c >=:d2 and Last_Producer_Date__c <=:d1 and Total_Cole_Tickets_Only_Funds_in_SF__c=1 order by Total_Cole_Production_Funds_in_SF__c desc];
       System.debug('*****consize' + conList.size());
       
       /*for(Contact c : conList){
         name = c.name; 
         totalcole = c.Total_Cole_Production_Funds_in_SF__c;  
         address = c.MailingStreet;
         city = c.MailingCity;  
         state = c.MailingState;
         zip = c.MailingPostalCode;    
       }*/
    }

}