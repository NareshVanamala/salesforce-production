@isTest
Public class LC_LETSFormAttachment_Test
{
   static testmethod void testCreatelease()
  {
     test.starttest();
        MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.name='Test_MRI_Property';
         insert mc; 
         system.debug('This is my MRI Property'+mc);
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU';
         mylease.Suite_Sqft__c=1234;
         mylease.Lease_ID__c='A046677';
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         system.debug('This is my Lease'+mylease);
        
         LC_LeaseOpprtunity__c lc1= new LC_LeaseOpprtunity__c();
         lc1.name='TestOpportunity';
         lc1.Lease_Opportunity_Type__c='Lease - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.LC_OtherOpportunity_Description__c='Thisis my description';
         lc1.Base_Rent__c=12345;
         insert lc1;
         system.debug('This is my lease Opportunity'+lc1);   
      
         list<attachment>myattalist= new list<attachment>(); 
         Blob b = Blob.valueOf('Test Data');  
         Attachment attachment = new Attachment();  
         attachment.ParentId = lc1.id;  
         attachment.Name = 'Test Attachment for Parent';  
         attachment.Body = b;  
         myattalist.add(attachment );
          insert myattalist;
         
            System.assertEquals(mc.Id,  myLease.MRI_PROPERTY__c);
            System.assertEquals(myLease.MRI_PROPERTY__c, lc1.MRI_Property__c);
            System.assertEquals( attachment.Name,'Test Attachment for Parent');
      
          LC_LETSFormAttachment LCcontroller = new  LC_LETSFormAttachment();
          LCcontroller.myValue=lc1.id;
          LCcontroller.setquoteId1(lc1.id);
          string myval=LCcontroller.getquoteId1();
          LCcontroller.quoteId1=lc1.id;
          
         LCcontroller.LCattachmentlist=myattalist;
         list<attachment>myattalist12=LCcontroller.LCattachmentlist;
        
      test.stoptest();
  
  
  
  
  
  
  
  
 }
}