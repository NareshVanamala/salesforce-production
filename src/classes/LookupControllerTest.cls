@isTest(SeeAllData=true)
public class LookupControllerTest
{ 
  public static testmethod void lookupControllerTest(){
    Account acc = new Account();
     acc.name='West cockpitcontact1FirstName';
     insert acc;
     
    System.assertEquals(acc.name,'West cockpitcontact1FirstName');

    Test.startTest();
      Id [] fixedSearchResults= new Id[1];
      fixedSearchResults[0] = acc.id;
      Test.setFixedSearchResults(fixedSearchResults);
      LookupController.searchString('West', 'Account');
    Test.stopTest();

  }
}