global class updatepropertygrossleaseableareabatch implements Database.Batchable<SObject>
{
  global list<web_properties__c> wblistnew=new list<web_properties__c>();
  map<string,web_properties__c> wbupdatedlist=new map<string,web_properties__c>();
  set<string> cd=new set<string>();  
   global updatepropertygrossleaseableareabatch(list<web_properties__c> wblist)
   {
   wblistnew=wblist;
   }

    global Database.QueryLocator Start(Database.BatchableContext BC)
    {
        return DataBase.getQueryLocator([select id,name,Gross_Leaseable_Area__c,property_id__c from web_properties__c where id in:wblistnew]);
    }   
    global void Execute(Database.BatchableContext BC,List<web_properties__c> Scope)
    { 
     for(web_properties__c wb:Scope)
     {
     cd.add(wb.property_id__c);
     }
     list<Vacant_Lease__c> vllist=[select id,name,Suite_SQFT__c from Vacant_Lease__c where name in:cd];           
    
     for(web_properties__c wb:Scope)
     {
       integer squareft=0;
       for(Vacant_Lease__c vl:vllist)
       {
        if(wb.property_id__c==vl.name)
        {
         if(vl.Suite_SQFT__c!=null)
         {        
         squareft=squareft+integer.valueof(vl.Suite_SQFT__c);
         }
        }
       }
       wb.Gross_Leaseable_Area__c=squareft;
       wbupdatedlist.put(wb.id,wb);
     }
       update wbupdatedlist.values();
    }

    global void finish(Database.BatchableContext BC)
    {
      
    } 
        
}