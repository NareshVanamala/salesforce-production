@isTest(seeAllData=true)
private class CMProjectsEmailTemplateCtrl_Test{
static testMethod void CMProjectsEmailTemplateCtrlTest(){
    User usr = [select id from User where IsActive = True limit 1];
    MRI_PROPERTY__c  mri = [select id from MRI_PROPERTY__c limit 1];
    Project__c cmproject = new Project__c();
    cmproject.Name = 'Testing';
    cmproject.Approval_Status__c='Approved';
    cmproject.Building__c = mri.id;
    //cmproject.Vendor__c
    cmproject.Maintenance_Manager__c=usr.id;
    insert cmproject;
    System.assertEquals(cmproject.name,'Testing');

    Bid__c bid= new Bid__c();
    bid.name = 'Bid';
    bid.CM_Project__c=cmproject.id;
    bid.Recommended_Vendor__c = TRUE;
    insert bid;
    
    Bid__c bid1= new Bid__c();
    bid1.name = 'Bid';
    bid1.CM_Project__c=cmproject.id;
    bid1.Recommended_Vendor__c = FALSE;
    insert bid1;
    
    System.assertEquals(bid.name,bid1.name);

    CMProjectsEmailTemplateCtrl CMProjects = new CMProjectsEmailTemplateCtrl();
    CMProjects.CMProjId = cmproject.id;
    CMProjects.getbidsList();
    CMProjects.getAllattList();
    
    //CMProjectsEmailTemplateCtrl CMProjectctrl = new CMProjectsEmailTemplateCtrl(sc);
    
}
}