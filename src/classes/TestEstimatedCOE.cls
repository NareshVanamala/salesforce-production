@isTest(seeAllData=True)
private class TestEstimatedCOE {
 
    static testMethod void testCOE() {
    Test.StartTest();
     PublicCalendar__c pc = new PublicCalendar__c();
     pc.Name='1222'; 
     insert pc;
    
    Deal__c D = new Deal__c();
    D.Name = 'Test';
    D.Estimated_COE_Date__c = Date.newInstance(2012,12,12);
     insert D;
    
    System.assertEquals(D.Estimated_COE_Date__c,Date.newInstance(2012,12,12));

    Event e= new Event();
    datetime dtstart = Datetime.newInstance(2013, 20 , 12, 00, 00, 00);
    e.StartDateTime = dtstart;
    e.Location = 'Phoenix'; 
   e.IsAllDayEvent=true; 
   Insert e;
   Test.StopTest();
   }
   }