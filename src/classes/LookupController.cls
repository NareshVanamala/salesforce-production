public with sharing virtual class LookupController {
    private static Map<String, Map<String, String>> obj_Field_apiName_Map= new Map<String, Map<String, String>>();
    
    private static Map<String, String> sobjectMap= new Map<String, String>();
    
    //private static String resolveSobjectName(String objName){
    //    if(objName.toLowerCase().endsWith('__c')){
    //        // is custom object
    //        return objName.toLowerCase().remove('__c')+'_c';
    //    }

    //    return objName.toLowerCase();
        
    //}
    public static void generateFieldsFromFieldSets(String objName){
        system.debug(logginglevel.info, 'objName: '+objName);
        try{
            sobjectMap.put(objName,objName);
            Schema.DescribeSObjectResult objectMeta = Schema.describeSObjects(new String[]{objName})[0];
            Schema.FieldSet objFields = objectMeta.fieldSets.getMap().get('CompassLookupFields');
            
            Map<String, String> field_ApiName_Label_Map= new Map<String, String>();

            for(Schema.FieldSetMember f : objFields.getFields()) {
                field_ApiName_Label_Map.put((f.getFieldPath().contains('__r')?f.getFieldPath().substring(f.getFieldPath().indexOf('__r.')+4, f.getFieldPath().length()) : f.getFieldPath()), f.getLabel());
            }

            obj_Field_apiName_Map.put(objName, field_ApiName_Label_Map);
            
        }catch(Exception e){
            system.debug(logginglevel.info, 'StackTrace:'+e.getStackTraceString()+'\nMessage: '+e.getMessage());
        }
    }
    public static String searchString(String searchString, String sObjectName){
        
        Results finalResults;
        try{
            String str= String.escapeSingleQuotes(searchString);
            
            generateFieldsFromFieldSets(sObjectName);

            String query= generateQuery(str, sObjectName);
            
            List<List<SObject>> searchList=search.query(query);    
            Map<String, String> account_field_label_map= obj_Field_apiName_Map.get(sObjectName);

            finalResults= new Results();
            finalResults.objName= sObjectName;
            finalResults.label= sobjectMap.get(sObjectName);


            for(Sobject obj: searchList.get(0)){
                Result result= new Result();
                
                for(String field: account_field_label_map.keySet()){
                    //if(obj.get(field)!=null && String.valueOf(obj.get(field))!=''){
                        if(field.equalsIgnoreCase('id')){
                            result.id= String.valueof(obj.get('Id'));
                        }
                        Column col= new Column(field, 
                                    account_field_label_map.get(field), 
                                    (obj.get(field)==null || obj.get(field)==''? '': String.valueOf(obj.get(field))));
                        result.columns.add(col);
                    //}
                }

                finalResults.results.add(result);
            }
        }catch(Exception e){
            if(finalResults==null){
                finalResults= new Results();
            }
            finalResults.hasError=true;
            finalResults.error='line: '+e.getLineNumber()+', '+e.getMessage()+', stack trace: '+e.getStackTraceString();
            system.debug(logginglevel.info, '-----Exception-----: ' +finalResults.error);
            return JSON.serialize(finalResults);
        }
        system.debug(logginglevel.info, '----- '+JSON.serializePretty(finalResults));
        return JSON.serialize(finalResults);
    }
    
    private static String generateQuery(String search, String sobj){
        if(sobj == null || sobj =='')
            return null;

        String query='FIND \''+search+'*\' ' +' IN ALL FIELDS RETURNING '+sobj;

        query += '(';
        for(String field: obj_Field_apiName_Map.get(sobj).keyset()){
            query += field + ',';
        }
        query = query.substring(0, query.length()-1);
        query += ')';
        
        return query;
    } 
    public class Results{
        public List<Result> results= new List<Result>();
        public String objName{get; set;}
        public String label{get; set;}
        public String error{get; set;}
        public Boolean hasError{get; set;}

        public Results(){
            objName='';
            label='';
            error='';
            hasError= false;
        }
    }
    public class Result{
        public List<Column> columns{get; set;}
        public String id{get; set;}

        public Result(){
            this.columns= new List<Column>();
        }
    }
    
    public class Column{
        public String apiName{get; set;}
        public String label{get; set;}
        public String value{get; set;}
        
        public Column(String apiName, String label, String value){
            this.apiName= apiName;
            this.label= label;
            this.value= value;
        }
    }
    public class SearchRequest{
        public String searchString;
        public String objectName;
    }
}