global with sharing class PortfolioMapService implements cms.ServiceInterface {

    public String JSONResponse {get; set;}
    private String action {get; set;}
    private Map<String, String> parameters {get; set;}
     // cms.CoreController stdController = new cms.CoreController();
    //private PortfolioMapctrl wpl = new PortfolioMapctrl(stdController);
    private PortfolioMapctrl wpl = new PortfolioMapctrl();
    private System.JSONGenerator response;

    global PortfolioMapService() {
        
    }

    global System.Type getType(){
        return PortfolioMapService.class;
    }

    global String executeRequest(Map<String, String> p) {
        this.action = p.get('action');
        this.parameters = p; 
        this.LoadResponse();    
        return this.JSONResponse;
    }

    global void loadResponse(){
        if (this.action == 'getIndustryList'){
            this.getIndustryList(parameters.get('region'));
        } else if (this.action == 'getTenantList'){
            this.getTenantList(parameters.get('region'));
        } else if (this.action == 'getStateList'){
            this.getStateList(parameters.get('region'));
        }
        /* else if (this.action == 'getCityList'){
            this.getCityList(parameters.get('state'));
        } 
        */
        else if (this.action == 'getResultTotal'){
            String region = parameters.get('region');
            String state = parameters.get('state');
            //String city = parameters.get('city');
            String tenantIndustry = parameters.get('tenantIndustry');
            String tenant = parameters.get('tenant');
            String[] filters = new List<String>();

            if(parameters.get('filterRetail') != null && parameters.get('filterRetail') != ''){
                filters.add(parameters.get('filterRetail'));
            }
            if(parameters.get('filterMultiTenant') != null && parameters.get('filterMultiTenant') != ''){
                filters.add(parameters.get('filterMultiTenant'));
            }
            if(parameters.get('filterOffice') != null && parameters.get('filterOffice') != ''){
                filters.add(parameters.get('filterOffice'));
            }
            if(parameters.get('filterIndustrial') != null && parameters.get('filterIndustrial') != ''){
                filters.add(parameters.get('filterIndustrial'));
            }
            if(parameters.get('filterOther') != null && parameters.get('filterOther') != ''){
                filters.add(parameters.get('filterOther'));
            }

            this.getResultTotal(region,state,tenantIndustry, tenant, filters);

        }   else if (this.action == 'getgroupbyStates'){
            String region = parameters.get('region');
            String state = parameters.get('state');
            //String city = parameters.get('city');
            String tenantIndustry = parameters.get('tenantIndustry');
            String tenant = parameters.get('tenant');
            String[] filters = new List<String>();

            if(parameters.get('filterRetail') != null && parameters.get('filterRetail') != ''){
                filters.add(parameters.get('filterRetail'));
            }
            if(parameters.get('filterMultiTenant') != null && parameters.get('filterMultiTenant') != ''){
                filters.add(parameters.get('filterMultiTenant'));
            }
            if(parameters.get('filterOffice') != null && parameters.get('filterOffice') != ''){
                filters.add(parameters.get('filterOffice'));
            }
            if(parameters.get('filterIndustrial') != null && parameters.get('filterIndustrial') != ''){
                filters.add(parameters.get('filterIndustrial'));
            }
            if(parameters.get('filterOther') != null && parameters.get('filterOther') != ''){
                filters.add(parameters.get('filterOther'));
            }

            this.getgroupbyStates(region,state,tenantIndustry, tenant, filters);
            //system.debug('getgroupbyStates'+this.getgroupbyStates);
            

        }
        else if (this.action == 'getPropertyListByState'){

            String state = parameters.get('state');
            String region = parameters.get('region');
            //String city = parameters.get('city');
            String tenantIndustry = parameters.get('tenantIndustry');
            String tenant = parameters.get('tenant');
            String[] filters = new List<String>();
            String sortBy = parameters.get('sortBy');
            String rLimit = parameters.get('limit');
            String offset = parameters.get('offset');

            if(parameters.get('filterRetail') != null && parameters.get('filterRetail') != ''){
                filters.add(parameters.get('filterRetail'));
            }
            if(parameters.get('filterMultiTenant') != null && parameters.get('filterMultiTenant') != ''){
                filters.add(parameters.get('filterMultiTenant'));
            }
            if(parameters.get('filterOffice') != null && parameters.get('filterOffice') != ''){
                filters.add(parameters.get('filterOffice'));
            }
            if(parameters.get('filterIndustrial') != null && parameters.get('filterIndustrial') != ''){
                filters.add(parameters.get('filterIndustrial'));
            }
            if(parameters.get('filterOther') != null && parameters.get('filterOther') != ''){
                filters.add(parameters.get('filterOther'));
            }

            this.getPropertyList(region,state,tenantIndustry, tenant, filters);
            //system.debug('getPropertyList'+this.getPropertyList );
            
        } 
        /*else if (this.action == 'saveCMSSessionCache') {
            
            // From CMS, the content can be saved in user's session
            Cache.session.put('local.CMSCache.gridContent', parameters.get('gridContent'));
            System.debug('saving cache in CMSCache for content ' + parameters.get('gridContent'));

            // Write "success" back
            response.writeStartObject();                
            response.writeBooleanField('success', true);
            response.close();
            this.JSONResponse = response.getAsString();
        } else if (this.action == 'getCMSSessionCache') {
            
            if (Cache.Session.contains('local.CMSCache.gridContent')) {
                String strContent = (String)Cache.Session.get('local.CMSCache.gridContent');
                System.debug('retrieving cache in CMSCache for content ' + strContent);
                
                // Write the data back
                response.writeStartObject();                
                response.writeBooleanField('success', true);
                response.writeStringField('strContent', strContent);
                response.close();
                this.JSONResponse = response.getAsString();
            }
        }*/
    }

    private void getResultTotal(string region,String state,String tenantIndustry, String tenant, String[] filters){
    this.JSONResponse = JSON.serialize(wpl.getResultTotal(region,state,tenantIndustry, tenant, filters));
    system.debug('getResultTotal'+this.JSONResponse );
    }
     private void getgroupbyStates(string region,String state,String tenantIndustry, String tenant, String[] filters){
     
        this.JSONResponse = JSON.serialize(wpl.getgroupbyStates(region,state,tenantIndustry, tenant, filters));
        system.debug('getgroupbyStates'+this.JSONResponse );
    }
    private void getPropertyList(string region,String state, String tenantIndustry, String tenant, String[] filters){
        this.JSONResponse = JSON.serialize(wpl.getPropertyList(region,state, tenantIndustry, tenant, filters));
   system.debug('getPropertyList'+this.JSONResponse );
    }

    private void getIndustryList(string region){
        this.JSONResponse = JSON.serialize(wpl.getIndustryList(region));
        system.debug('getIndustryList'+this.JSONResponse );
    }

    private void getTenantList(string region){
        this.JSONResponse = JSON.serialize(wpl.getTenantList(region));
        system.debug('getTenantList'+this.JSONResponse );
    }

    private void getStateList(string region){
        this.JSONResponse = wpl.getStateList(region);
        system.debug('state12345618'+this.JSONResponse );
    }

   /* private void getCityList(String state){
        this.JSONResponse = JSON.serialize(wpl.getCityList(state));
        system.debug('getCityList'+this.JSONResponse );
    }*/

}