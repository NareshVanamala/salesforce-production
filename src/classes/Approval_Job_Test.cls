@isTest(seeAllData=false)
private class Approval_Job_Test   {

    static testmethod void ApprovalJob() {
         MRI_Property__c m = new MRI_Property__c();
        m.Name = 'Sample';
        m.Web_Ready__c = 'Pending Approval';
        m.Property_Id__c = '12345';
        m.AvgLeaseTerm__c=234567;
        m.Program_Id__c ='ARCP';
        insert m;
        
        System.assertnotEquals( m.Property_Id__c,m.Program_Id__c);
      /*  Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(m.Id);
      req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});

        Approval.ProcessResult result = Approval.process(req1);
  */

        // The query used by the batch job.
        String query = 'SELECT Id FROM MRI_Property__c where Web_Ready__c = \'Pending Approval\' OR Web_Ready__c = \'Pending Deleted\'';
             

        Test.startTest();
       Approval_Job   c = new Approval_Job(query);
       Database.executeBatch(c);
       Test.stopTest();

      
    }
}