@isTest
private class TestforMarketingthemesclass 
 {
   public static testMethod void TestforMarketingthemesclass () 
   {
          Account a= new Account();
          a.name='Testaccount1';
           
          //RecordType rtypes = [Select Name, Id From RecordType where isActive=true and sobjecttype='Account'and name='National_Accounts'];
          //a.recordtypeid=rtypes.id;
          insert a;  
          
          Marketing_themes__c ct= new Marketing_themes__c();
          ct.Account__c=a.id;
          ct.Description__c='TestingtheBDsystem';
          ct.name='Testingthe Program';
          insert ct;
          
          Marketing_themes__c ct2= new Marketing_themes__c();
          ct2.Account__c=a.id;
          ct2.Description__c='TestingtheBDsystem23';
          ct2.name='Testingthe Programyy';
          insert ct2;
          
          a.Marketing_Themes__c =ct2.Description__c;
          update a;
          
          System.assertEquals(ct2.Account__c,a.id);

           
          String nameFile = 'hello';
          Attachment attachment= new Attachment(); 
          attachment.OwnerId = UserInfo.getUserId();  
          attachment.ParentId = ct.Id;// the record the file is attached to   
          attachment.Name = nameFile;     
          Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');  
          attachment.body=bodyBlob;   
          insert attachment;  
          
          ApexPages.currentPage().getParameters().put('id',a.id);  
          ApexPages.StandardController controller = new ApexPages.StandardController(a);  
          Marketingthemesclass x = new Marketingthemesclass(controller);
          x.nameFile='Snehal';      
          Blob blb=Blob.valueOf('SnehalPiyaSnehal'); 
          x.contentFile=blb;  
           x.addrow.Name='B_D_Systems Name'; 
          List<Attachment> attachments=[select id, name from Attachment where parent.id=:ct.id];    
                       
          x.newProgram();
         x.savenewProg();
         x.editrecord();
         x.inserttochild();
         //x.viewRecord();
         x.SaveRecord();
         x.removecon();
         x.cancelnewProg();          

       }
         
          
           
 }