global virtual with sharing class MediaRoom{

    private List<SObject> propertyList;
       // public String getValue()
    global MediaRoom() { 
                 
        } 
   
     public Integer getResultTotal(String pressRelease){
        
        String query;
        
        query = 'SELECT COUNT() FROM Press_Releases__c where';
        
        
        if((pressRelease == 'all') || (pressRelease == '') || (pressRelease == null )){
              pressRelease = pressRelease.escapeEcmaScript();
              query += ' Sytem_Type_Of_Release__c != \'' + '' + '\'';
        }
        else if((pressRelease != null && pressRelease != '' && pressRelease != 'all' )){
              pressRelease = pressRelease.escapeEcmaScript();
              query += ' Sytem_Type_Of_Release__c like  \'%' + pressRelease + '%\'';
   
        }
         return Database.countQuery(query);

    } 
    
       public List<SObject> getPropertyList(String pressRelease){

        List<SObject> propertyList;
        String query;
        //system.debug('year123'+year ); 
        system.debug('pressRelease123'+pressRelease); 
        query = 'SELECT Id, Press_Release_Name__c,Sytem_Type_Of_Release__c,Type_Of_Release__c,Date__c,Published_Date__c, Article_Link__c,Description__c, Year__c FROM Press_Releases__c where';

       if((pressRelease == 'all') || (pressRelease == '') || ( pressRelease == null )){
              pressRelease = pressRelease.escapeEcmaScript();
              query += ' Sytem_Type_Of_Release__c!= \'' + '' + '\'';
        }
        else if((pressRelease != null && pressRelease != '' && pressRelease != 'all' )){
              pressRelease = pressRelease.escapeEcmaScript();
              query += ' Sytem_Type_Of_Release__c like  \'%' + pressRelease + '%\'';
   
        }
        query += 'ORDER BY Published_Date__c DESC';
        system.debug('query123'+query); 
        propertyList = Database.query(query);
       
        return propertyList;
    }
    
  
    /*public List<SObject> getYearList(){

        List<SObject> yearList;

        String query = 'SELECT Year__c FROM Press_Releases__c Group by Year__c ORDER BY Year__c DESC';
        yearList = Database.query(query);

        return sanitizeList(yearList, 'Year__c');

    }*/
    public list<string> getpressReleaseList()
        {
          List<string> options = new List<string>();
                
           Schema.DescribeFieldResult fieldResult =
         Press_Releases__c.Type_Of_Release__c.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
           for( Schema.PicklistEntry f : ple)
           {
              options.add((f.getLabel()));
           }       
           return options;
        }

   
    private List<SObject> sanitizeList(List<SObject> inList, String field){

        Integer ctr = 0;

        while(ctr < inList.size()){
            if(inList[ctr].get(field) == null){
                inList.remove(ctr);
            } else {
                ctr++;
            }
        }
        return inList;

    }
 
   
  
  }