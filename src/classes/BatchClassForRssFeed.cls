global with sharing class BatchClassForRssFeed implements Database.Batchable<sObject>, Database.AllowsCallouts{

global Database.QueryLocator start(Database.BatchableContext BC)
{
    String query = 'Select Id from user limit 1';
    return Database.getQueryLocator(query);
}

global void execute(Database.BatchableContext BC, List<user> scope)
{       List<Press_Releases__c> pressRelease= new List<Press_Releases__c>();
        string clientId =system.label.Rss_ClientId;
        string responseBody;
        string getStatus;
        String reqbody;
        string bodyurl = 'client_id={0}&client_secret={1}&scope=http://www.snl.com&grant_type=client_credentials';
        string clientSecret = system.label.RSS_ClientSecret;
        string[] arguments = new string[]{clientId,clientSecret};
        reqbody = String.Format(bodyurl, arguments);
        system.debug('REQUESTED_BODY'+reqbody);
        JSONParser parser;
        JSONParser parser1;        
        JSON2Apex Jap;
          Http h = new Http();
        if(!Test.isRunningTest()){        
           
            HttpRequest req = new HttpRequest();
            req.setBody(reqbody);
            req.setMethod('POST');
            req.setTimeout(120000);
            req.setEndpoint('https://security.snl.com/SNL.Services.Security.Service/oauth/token');
            HttpResponse res = h.send(req);            
            responseBody = res.getbody();
            getStatus = res.getStatus();
            system.debug('RESPONSE_BODY'+res.getbody());
        }
        else{ 
        
            if(Test.isRunningTest()){
              responseBody  ='{\"access_token\":\"<jwt0 xmlns=\\\"http:\\/\\/www.snl.com\\/security\\/tokens\\\">eyJ0eXAiOiJKV0UiLCJhbGciOiJSU0ExXzUiLCJ4NXQiOiI5NkMwMDY3NEU4RDRCNzdFNzExQjM2QTEyQkVBNDJBRTNFQzcyNEMyIiwiZW5jIjoiQTI1NkNCQyIsIml2IjoiMTQzcGl1cXBqWk9kTksybzlWODVuQSIsInppcCI6IlpJUCJ9.NqHxSoE_InBGLauYWNAGGE2SzivNjON8woH0sZkapoLcxu61BqzW51RD4sGPK8AzL0O026jQ9XObJw-inqtyBr85xH8smArS4tLZ1fBWMVAB7wBRkfBkNqjnLpnY6wOcp91gpPx_IjUg0WaiAS0mCIBs8yhzR6oQVhyRUqir-4pWCD68HAUiYe9_BeU_m_EB6aadoE8BIs6JTjDjkEcZQYdBjrzV7yLDdbg2pAytQoAkjk9Kl5FGeUI59ZrV510cSL8QQYXQKAhH_IHSpM_zuwKYvz5N8tYKhoEZ6B6cJ-6d5u5tCuc2-gguaVYCU_YJ1bC4szTM9isoCdUJgamY3A.6bg9SpbE73Xa07wMk5bXMaEdx-kA4y4LTwbwvMPp7ratY0Arc6jBfJQ_03ZnQHHsasxTn5lB2Cl7k63xbrzLJcwmH1NIBquY52HSH3hhLc_zA5IGNef3m11Etvv6BFjlbV7WpOOKCkTzp6WarYx5RCjzIjjY_UQd3KzfWnhKB_fHl7ed8m1uc1jdtHt-dlHNGpvLEpPB7KdAHD7tpHQGDQxpRASsMbttpk6Vlq-jn1rO1p4qsSKsl9nKUy-mN9lDspLi4OlEMVRrKDwGgSsUOTjgxKh-q60JOkFgJKF7igj_9658LMJXZljY0tuAzIei6FcqV8EIGfhHQiRRdrrdkLlYGPvbxHODOimWhGkgt5pwFeZR2w5Qst97Kr9tqqKiETZIdYU1bYRwMIvJk56p59G16gZBCI5Dr2A9MQzpD6Lr3c962BEp4ALkWGJkmUWC0myqYvZklFVrF99XbbdNIiRBiCQe8ogrfy1rqAXyDF05FX5o2-PLHgpEPqL86MLtcP900phLvHUWW759kvNRC2vQpxsszkOZAW7NGATbTgx_Mq5Ph2t6SFPKK8JefhkAhqJyg0tmwL2R57Q2okeYP8R990EhBwfarDqB5yoJ6sx9vD-G2wwqFkEvZuQmoo_FaEOGHCVQ99H9lptYZ2tPMmCPCC550cy2tvnfs_GaYftB_Ni3C0xYqqbho7hOjvapDe18z2pDtttpJ1r6e8dKg_beaYrJTpiXFvzYHENnSarACqT0bxHvkAewiiYvYMYwk_UI2WHhO3rCOo5Khn7gYVeDsid-ydud3ymdixYzPkIrgDtD2VCLcRHrF64TWQgYw9jksWQSpCkl8tK5fnf_fiaTybNbE1qD4RzuDv23s98L8jm-CX-7AENY-nvD3RQwVuJYPmOWv51hwZD8IP0l8diJD5XJGms8yh2oDiAjfcIMuVR3t33vjA8K7ha5FCDqcF6fcH5wXt-V2J3Y8h03Cxj6ni5X7tUFaQD4yCwob16pfpVJrAUVPkacQGF_c0t6DTcO0vzAxHfRlF18oWVHj15Rpd9dBKtNQDM5Q_o81Bd5LXjNxHO0OMtXr8vUI169OoULspv15QO-5xj9O78prA0I9lpkhD5-bV0MXPgFdrz0x8RvPnsg0yKuL3VctRbIzycMfFGpzB7NsnIsBTIzS_YqzXLm97Awe3CHATPyNkY<\\/jwt0>\",\"token_type\":\"bearer\",\"expires_in\":86400,\"scope\":\"http:\\/\\/www.snl.com\"}';
              }
             
        }
        
        parser = JSON.createParser(responseBody);        
        string accessToken ='';
        string refreshToken ='';
        while (parser.nextToken() != null) {
            if(parser.getCurrentToken() == JSONToken.FIELD_NAME){
                if(parser.getText() == 'access_token'){
                    parser.nextToken();
                    accessToken = parser.getText();
                }
                if(parser.getText() == 'refresh_token'){
                    parser.nextToken();
                    refreshToken = parser.getText();
                }
            }
            if(accessToken != '' && refreshToken != ''){
                break;
            }
        }
         if(!Test.isRunningTest()){    
         if(accessToken != null & accessToken != ''){
            HttpRequest req1 = new HttpRequest();
            string requestURL;
            String KeyInstance = system.label.RSS_KeyInstance;
            requestURL = 'https://data.snl.com/IRWebAPI/v1/PressReleases/PressReleases?Id='+system.label.RSS_KeyInstance+'&IncludeText=false&IncludeRelatedDocs=false';
            system.debug('requestURL'+requestURL );
            requestURL += '&$format=json';
            req1.setEndpoint(requestURL);
            req1.setHeader('Authorization', 'Bearer ' + accessToken);
            system.debug('header'+accessToken);
            req1.setMethod('GET');
            req1.setTimeout(120000);
            HttpResponse res1 = h.send(req1);
            //Now we could parse through the JSON again and get the values that we want
            string valuetoShow = 'Press Release Details: ' + res1.getBody();           
            parser1 = JSON.createParser(res1.getBody());
            Jap = new JSON2Apex(parser1);
             }
             }
              else if(test.isrunningtest()){
              
                    String responseBody1 = '{"d":{"results":[{"__metadata":{"id":"http://data.snl.com/IRWebAPI/V1/PressReleases/PressReleases(13281131)","uri":"http://data.snl.com/IRWebAPI/V1/PressReleases/PressReleases(13281131)","type":"SNL.SNLWebX.IRWebAPI.Common.Models.PressReleases.PressReleaseEntity"},"KeyDoc":13281131,"KeyFile":11711596,"FilingType":"Press Release","Title":"American Realty Capital Properties Executive Management Team to Ring the Bell on NASDAQ","ShortDescription":null,"FullText":null,"ReleaseDateTime":"2011-09-08T17:44:00","ReleaseDateTimeFormatted":"9/8/2011","ReleaseDay":8,"ReleaseMonth":9,"ReleaseYear":2011,"ReleaseTime":"17:44:00","ReleaseType":null,"Format":"XML","RelatedDocs":{"__metadata":{"type":"Collection(SNL.SNLWebX.IRWebAPI.Common.Models.Common.RelatedDoc)"},"results":[]}}]}}'; 
                    JSONParser parser2 = JSON.createParser(responseBody1);
                    Jap = new JSON2Apex(parser2);
                    
                }
             for(JSON2Apex.Results_Z jr: Jap.d.results)
             {
               Press_Releases__c  pressReleaseObj =new Press_Releases__c();
               pressReleaseObj.Press_Release_Name__c = jr.Title;
               pressReleaseObj.Article_Link__c=system.label.PressReleasesLink+jr.KeyFile;
               pressReleaseObj.Type_Of_Release__c=jr.FilingType;
               date mydate = date.parse(jr.ReleaseDateTimeFormatted);
               pressReleaseObj.Published_Date__c = mydate;
               pressReleaseObj.Year__c=string.valueof(jr.ReleaseYear);
               pressRelease.add(pressReleaseObj);  
             }
               
             if(pressRelease !=null && pressRelease.size()>0){
             
                 if (Schema.sObjectType.Press_Releases__c.isCreateable())


                  upsert pressRelease Article_Link__c;   
             }
             
                

}   
global void finish(Database.BatchableContext BC)
{
}
}