public with sharing class Marketingthemesclass{

    Public List<Marketing_themes__c> memberList{get;set;}
    private Marketing_themes__c deletelist1;
    private Marketing_themes__c updatedlist1;
    private List<Marketing_themes__c> updatemelist;
    private List<Marketing_themes__c> deleteemelist;
    private integer removepos{get;set;}
    public string conid{get;set;}
    private Id recId{get;set;} 
    public string fileName{get;set;} 
    public transient Blob fileBody{get;set;}
    public Attachment attachment{get;set;}
    public Attachment myatt{get;set;}
    public string nameFile{get;set;}
    public string decriptionOfFile{get;set;}
    public Blob contentFile{get;set;}
    public Boolean showInputFile{get;set;}
    private Account a;  
    
    public Marketingthemesclass(ApexPages.StandardController controller)
    {
        addrow = new Marketing_themes__c();
        //myAttachment = new Attachment();
        a =(Account)controller.getrecord();
        showInputFile = false;
        //a =[select id from Account where id=:ApexPages.currentPage().getParameters().get('id')];
        //a=[select id from Account where id= '001P000000adMsB'];
        memberList =[select name, id,(Select Id,Name,description from Attachments) Attmts,Account__c ,Description__c from Marketing_themes__c where Account__c=:a.id order by LastModifiedDate DESC];  
        deletelist1= new Marketing_themes__c();  
        updatedlist1= new Marketing_themes__c();
        updatemelist=new list<Marketing_themes__c>();
        deleteemelist=new list<Marketing_themes__c>();
        recId= controller.getRecord().id;
        myatt=new attachment();
        
       
    }
    public Pagereference removecon(){
        if(memberList.size()>0)
        {       
            string paramname= Apexpages.currentPage().getParameters().get('node');
            system.debug('Check the paramname'+paramname);
            for(Integer i=0;i<memberList.size();i++)
            {
                if(memberList[i].id==conid)
                {
                    removepos=i;
                    //deletelist1.id=memberList[i].id;
                    deleteemelist.add(memberList[i]);
                }
            }
            if(removepos!=null)
            memberList.remove(removepos);
            //system.debug('check the deletelist..'+deletelist1);
            if(deleteemelist.size()>0){
                if (Marketing_themes__c.sObjectType.getDescribe().isDeletable()) 
                {
                        delete deleteemelist;
                }
             deleteemelist.clear();
            }           
        }
        return null;
   }
   
   // Edit Record method ....
   Pagereference p;
   public Id recordId {set;get;}
   public Boolean edit {set;get;}   
   public Pagereference editRecord(){
       recordId = conid;
       edit = true;
       //p = new Pagereference('/apex/AdvancedProgrampage?id='+ApexPages.currentPage().getParameters().get('id'));
       return null;
   }
   
  /* public Pagereference viewRecord(){
       recordId = conid;   
      myatt=[select id, name, parentid from Attachment where parentid=:recordId ];
        system.debug('check the attachement..'+myatt);
       p = new Pagereference('/'+myatt.id);
      // p.setRedirect(true);
       return p;
   }*/
   
   public Pagereference SaveRecord(){
      if(memberList.size()>0)
        {
             for(Integer i=0;i<memberList.size();i++)
            {
                if(memberList[i].id==conid)
                {
                   /*updatedlist1.id= memberList[i].id;
                   updatedlist1.name= memberList[i].name;
                   updatedlist1.Description__c= memberList[i].Description__c;*/
                   updatemelist.add(memberList[i]);   
                }
            }
                 if(updatemelist.size()>0)
                 {
                 update updatemelist;
                 updatemelist.clear();
                 }
        }
       edit = false;
       return null;
       
   }
    //adding new record
    public Boolean addnew{set;get;}
    public Marketing_themes__c addrow {set;get;}

    public Pagereference newProgram(){
        attachment =  new Attachment();
        addrow= new Marketing_themes__c();
        addnew=true;        
        showInputFile = true;
        return null;    
    }
    public Pagereference savenewProg(){   
    system.debug('This is the Save new parogram');          
        system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>nameFile>>>>>>>>>>>>>>>>>>>>>>>>>>>'+nameFile);
        system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>contentFile>>>>>>>>>>>>>>>>>>>>>>>>>>>'+contentFile);        
        try{            
            addnew=true;
            addrow.Account__c = a.id;
            /*System.debug('********description' + addrow.Description__c);
            if(addrow.Description__c!= null){
               a.Marketing_Themes__c = addrow.Description__c;
               System.debug('********Marketing_Themes__c' +addrow.Description__c );
            } */          
            if((nameFile==null) && (contentFile==null))
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please add attachment'));
            else if(contentFile.size()>5242880)
            {
            system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> >5MB..');
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Attachment size is not greater than 5MB'));            
            }
            else
            {   
                addnew=false;
                if (Schema.sObjectType.Marketing_themes__c.isCreateable())

                insert addrow;
                //update a;
                
                attachment.OwnerId = UserInfo.getUserId();
                attachment.ParentId = addrow.Id;// the record the file is attached to
                attachment.Name = nameFile;
                attachment.Description =decriptionOfFile;
                attachment.Body = contentFile;   
                //attachment.IsPrivate = true;
            
                system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>contentFile.Size>>>>>>>>>>>>>>>>>>>>>>>>>>> if '+contentFile.size());
                system.debug('\n myAttachment.name--->'+attachment.name);
                system.debug('check the attachement..'+ attachment);
                
                try
                 {
                    if((attachment.Name!=null) &&(attachment.Body!=null)) 
                    {
                      if (Schema.sObjectType.Attachment.isCreateable())
                          
                    insert attachment;
                    }
                    inserttochild(); 
                    attachment.Body=null;
                    attachment.clear();
                     nameFile=null;
                     contentFile=null;                   
                     memberList =[select name, id,(Select Id,Name,Description from Attachments) Attmts,Account__c,Description__c from Marketing_themes__c where Account__c=:a.id order by LastModifiedDate DESC];                
                    
                      
                 }
                 catch (DMLException e)
                 {
                    system.debug('check the attachement..'+ attachment);
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
                  }
                return ApexPages.currentPage();
            }           
        }
        catch(Exception e){
            system.debug('Exception --->'+e);
        } 
          
       // pagereference   p = new Pagereference('/'+ApexPages.currentPage().getParameters().get('id')) ;
       contentFile=null;
       nameFile=null;
       system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> End');
       return null;
           
    }
    public Pagereference cancelnewProg(){
        addnew=false;
        return null;    
    }
    
    public void inserttochild()
    {
      system.debug('HI,welcome to this method');
         List<Account> acclist = [select id, parentid  from Account where parentid=:a.id];
         system.debug('\n myAttachment.name--->'+attachment.name);
         System.debug('Accountlist' + acclist);
         List<Attachment> attlist = new List<Attachment>();
         List<Marketing_Themes__c> mtlist = new List<Marketing_Themes__c >();
         if(acclist.size()>0){
         for(Account at : acclist){
            Marketing_Themes__c attach = new Marketing_Themes__c();
            
            attach.Account__c= at.Id;// the record the file is attached to
            //attach.Name = nameFile;
            attach.Description__c =addrow.Description__c ;
            //attach.Body = contentFile;
            
            mtlist.add(attach);
         }
         
         //System.debug('****' + attlist.size());
         if (Schema.sObjectType.Marketing_Themes__c.isCreateable())

         insert mtlist;
         }
         for(Marketing_Themes__c mts : mtlist){
           Attachment attach = new Attachment();
            attach.OwnerId = UserInfo.getUserId();
            attach.ParentId = mts.Id;// the record the file is attached to
            attach.Name = nameFile;
            attach.Description =mts.Description__c;
            attach.Body = contentFile;
            
            attlist.add(attach);

         }
         if (Schema.sObjectType.Attachment.isCreateable())

         insert attlist;
    }
      
   

    
 }