@isTest
Public class Test_CustomNewSplitTicketButton
{
static testmethod void testunit1()
{
test.starttest();

        Account acc = new Account();
        acc.Name = 'Test Prospect Account';      
        Insert acc;

        Contact c = new Contact();
        c.firstname='TFirst1/TFirst2';
        c.lastname='TLast1/TLast2';
        c.accountid=acc.id;
        c.MailingCity='testcity';
        c.MailingState='teststate';
        c.email='test@mail.com';
        c.Contact_Type__c = 'Registered Rep';
        insert c;
        
        system.assertequals(c.accountid,acc.id);
        
        REIT_Investment__c newreit = new REIT_Investment__c (Investor_Name__c = 'Test_Investor',
        Account__c= '1150221562',Rep_Contact__c=c.id,
        Rep_ID__c ='BE111',Fund__c='3772');
         
        insert newreit ;
         
         SplitTickets__c  spt = new SplitTickets__c(REIT_Investment__c= newreit.id,Rep__c=c.id );
        insert spt;
      
ApexPages.currentPage().getParameters().put('id',newreit .id);
ApexPages.StandardController stdcon = new ApexPages.StandardController(newreit );
CustomNewSplitTicketButton sptbutton= new CustomNewSplitTicketButton (stdcon);
sptbutton.saveAndNew();
sptbutton.CancelAndReturn();
sptbutton.lookupRedirect() ;
test.stoptest();
} 


}