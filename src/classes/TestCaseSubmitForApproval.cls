@isTest
private class TestCaseSubmitForApproval {
 
    static testMethod void testApprovalSuccess() {
    Id MISRequest=[Select id,name,DeveloperName from RecordType where DeveloperName='MI_S_Request' and SobjectType = 'Case'].id;
    Test.StartTest();
        Case cs = new Case();
       cs.recordtypeid = MISRequest;
        cs.status= 'Pending Approval';
        cs.Subject= 'Test';
        cs.Origin = 'Email';
       
         insert cs;
         
         System.assertEquals(cs.Origin,'Email');

       
 //List<ProcessInstance> processInstances = [select Id from ProcessInstance where TargetObjectId = :cs.id];
 //System.assertEquals(processInstances.size(),1);
 Test.stopTest();
    }
 
}