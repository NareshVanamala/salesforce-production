public with sharing class ProductsDetailPage{
    private List<Products__c> oppz;
    private Competitor__c cntact; 
    public ProductsDetailPage(ApexPages.StandardController controller) {
        this.cntact= (Competitor__c)controller.getRecord();
    }
    public List<Products__c> getOppz()
    {
        Competitor__c  con = [Select id FROM Competitor__c where id = :cntact.id];
        system.debug('Competitor Id'+con);
        oppz = [Select id, Name,Is_master_record__c,competitor__c,Diversification__c,Dividend__c,Portfolio_cap_rate__c,Talking_points__c ,strategy__c from Products__c where competitor__c = :con.id and Is_Master_record__c=true];
        system.debug('Get me the products list'+oppz);
        return oppz;
    }
}