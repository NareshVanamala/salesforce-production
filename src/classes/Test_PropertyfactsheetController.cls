@isTest
public class Test_PropertyfactsheetController{
static testMethod void PropertyfactsheetControllerTest()
{
  Test.StartTest();
  Account a= new Account();
  a.name='Testaccount1';
  insert a; 
  
  Contact c = new Contact();
  c.firstname = 'test';
  c.lastname='contact';
  insert c; 
  
          

  List<Deal__c> deals = new List<Deal__c>();
  
  Deal__c d = new Deal__c();
  d.name = 'TestDeal';
  d.Address__c = 'Northeast';
  d.City__C = 'Kansas';
  d.State__c = 'AZ';
  insert d;
  
  Loan__c lc= new Loan__c();
  lc.name='testloan';
  insert lc;
  
  Rent_Roll__c rent = new Rent_Roll__c();
  rent.Name_of_Tenant__c = 'Tenant';
  rent.Suite_ID__c = 'rtrt454545654';
  rent.Floor__c = '5';
  rent.SF__c = 10;
  rent.store__c ='rtre';
  rent.Lease_Expiration__c = System.today();
  rent.Tenant_Type_ID__c = 'fgfg45345345';
  rent.Lease_Code__c = 'fgfg';
  rent.Reimb_Code__c='tytyt';
  rent.S_P_Rating__c ='AAA';
  rent.Deal__c = d.id;
  insert rent;
  
  Loan_Relationship__c loan = new Loan_Relationship__c();
  loan.Deal__c = d.id;
  loan.Lender__c = a.id;
  loan.Loan_Amount__c = 10000;
  loan.LTV__c = 98989;
  loan.test__c = 'test';
  loan.Loan__c = lc.id;
  insert loan;
  
   System.assertEquals(loan.Loan__c,lc.id);

  ApexPages.currentPage().getParameters().put('id',d.id);
  
  GeneratePropertyFactsheetController fact = new GeneratePropertyFactsheetController();
  fact.getrentRolls();
  fact.getloans();
  Test.StopTest();
  
  }
  }