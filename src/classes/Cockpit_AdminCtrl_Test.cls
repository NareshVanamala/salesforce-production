@isTest
public class Cockpit_AdminCtrl_Test{
    
   static testmethod void myTestMethod(){

     Contact c = new Contact();
     c.Firstname='Test';
     c.Lastname='record';
     insert c;
     System.assert(c != null);
     
     Contact c1 = new Contact();
     c1.Firstname='Test';
     c1.Lastname='record';
     insert c1;
     System.assert(c1 != null);
     
         
     List<Contact> scope1 = new List<Contact>();
     scope1.add(c); 
     
     Campaign cn = new Campaign();
     cn.name='Blast Email';
     insert cn;
     System.assert(cn != null);
 
     CampaignMember cr = new CampaignMember();
     cr.campaignid = cn.id;
     cr.contactid = c.id;
     insert cr;
     System.assert(cr != null);
  
     
     Automated_Call_List__c ACL = new Automated_Call_List__c();
     ACL.name ='TestingCallList'+c.id;
     ACL.Campaign__c =cn.id;
     ACL.Type_Of_Call_List__c='Campaign Call List';
     ACL.Dynamic_Query_Filters__c='Select Campaign Status&&&&Alternative_Investments_Firm_Provided__c&&!!equals&&!!null-->X1031_Relationship__c&&!!equals&&!!null-->Account.HNW_Accounts_2M__c&&!!equals&&!!null-->Account.Analyst_and_Research_Role__c&&!!equals&&!!False-->Account.Name&&!!equals&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!starts with&&!!null-->Account.Active__c&&!!starts with&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!contains&&!!null-->Account.BusinessProfileTrackPercent__c&&!!equals&&!!null-->Account.CapleaseNumberofLocations__c&&!!equals&&!!null';
     //ACL.Last_Activity_Date__c=Date.today()-30;
     ACL.Archived__c=false;
     ACL.isAdvanceFormula__c=false;
     ACL.Visiblity__c='Visible to all users';
     ACL.TempLock__c='';
     ACL.Activity_In_Last_X_Days__c= 7;
     ACL.Open_Task_Due_In_X_Days__c = 7;
     ACL.Description__c = 'testing';
     ACL.Priority__c = true; 
     insert ACL;
     System.assert(ACL != null);
     
     ACL.TempLock__c = 'General Lock';
     update ACL;
    
     ACL.TempLock__c = '';
     ACL.Internal_Flag__c  =true;
     update ACL;
     
     Automated_Call_List__c ACL1 = new Automated_Call_List__c();
     ACL1.name ='TestingCallList1'+c.id;
     ACL1.Dynamic_Query_Filters__c='Select Campaign Status&&&&Alternative_Investments_Firm_Provided__c&&!!equals&&!!null-->X1031_Relationship__c&&!!equals&&!!null-->Account.HNW_Accounts_2M__c&&!!equals&&!!null-->Account.Analyst_and_Research_Role__c&&!!equals&&!!False-->Account.Name&&!!equals&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!starts with&&!!null-->Account.Active__c&&!!starts with&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!contains&&!!null-->Account.BusinessProfileTrackPercent__c&&!!equals&&!!null-->Account.CapleaseNumberofLocations__c&&!!equals&&!!null';
     ACL1.Custom_Logic__c = '1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8 AND 9 AND 10';
     //ACL1.Last_Activity_Date__c=Date.today()-30;
     ACL1.Archived__c=false;
     ACL1.Type_Of_Call_List__c='Campaign Call List';
     ACL1.IsDeleted__c=false;
     ACL1.Visiblity__c='Visible to all users';
     ACL1.TempLock__c='';
     ACL1.Activity_In_Last_X_Days__c= 7;
     ACL1.Open_Task_Due_In_X_Days__c = 7;
     ACL1.Description__c = 'testing';
     ACL1.Priority__c = true; 
     ACL.isAdvanceFormula__c=true;   
     ACL1.Dynamic_Query__c='select Id,Name,Internal_Wholesaler1__c,Territory__c,RIA_Territory__c from Contact where (First_Name__c = \'Naresh\')';
     insert ACL1;
     
     System.assert(ACL1 != null);
     
     Automated_Call_List__c ACL2 = new Automated_Call_List__c();
     ACL2.name ='TestingCallList2'+c.id;
     ACL2.Dynamic_Query_Filters__c='Select Campaign Status&&&&First_Name__c&&!!equals&&!!Naresh';
     ACL2.Custom_Logic__c = 'AND';
     //ACL2.Last_Activity_Date__c=Date.today()-30;
     ACL2.Archived__c=false;
     ACL2.Type_Of_Call_List__c='Campaign Call List';
     ACL2.Visiblity__c='Visible to all users';
     ACL2.IsDeleted__c=false;
     ACL2.TempLock__c='';
     ACL2.Activity_In_Last_X_Days__c= 7;
     ACL2.Open_Task_Due_In_X_Days__c = 7;
     ACL2.Description__c = 'testing';
     ACL2.Priority__c = true;              
     ACL2.Dynamic_Query__c='select Id,Name,Internal_Wholesaler1__c,Territory__c,RIA_Territory__c from Contact where (First_Name__c = \'Naresh\')';
     insert ACL2;
     System.assert(ACL2 != null);
          
     contact_call_list__c clist = new contact_call_list__c();
     clist.Call_Complete__c = false;
     clist.Automated_Call_List__c = ACL.id;
     clist.CallList_Name__c = ACL.Name;
     clist.Description__c = 'Testing';
     clist.ContactCalllist_UniqueConbination__c=ACL.Name+c.Id;
     clist.Contact__c = c.id;
     insert clist;
     System.assert(clist != null);
     
     contact_call_list__c clist1 = new contact_call_list__c();
     clist1.Call_Complete__c = false;
     clist1.Automated_Call_List__c = ACL1.id;
     clist1.CallList_Name__c = ACL1.Name;
     clist1.Description__c = 'Testing';
     clist1.ContactCalllist_UniqueConbination__c=ACL1.Name+c1.Id;
     clist1.Contact__c = c1.id;
     insert clist1;

     System.assert(clist1 != null);   
     
     Id MngCallListRecId = Schema.SObjectType.Manage_Cockpit__c.getRecordTypeInfosByName().get('Manage Call Lists').getRecordTypeId();
     Manage_Cockpit__c ManageCallListRec = new Manage_Cockpit__c();
     ManageCallListRec.recordtypeid = MngCallListRecId;
     ManageCallListRec.Archive_Call_List_in_X_days__c = '7';
     insert ManageCallListRec;
     
     System.assert(ManageCallListRec != null);
     
     Test.StartTest(); 

     Cockpit_AdminCtrl adminCtrl = new Cockpit_AdminCtrl();
     adminCtrl.automatedCallList.Name ='test ACL';
     adminCtrl.FieldName1='Firstname';
     adminCtrl.Operator1='equals';
     adminCtrl.Value1=c.Firstname;
     adminCtrl.FieldName2='Firstname';
     adminCtrl.Operator2='equals';
     adminCtrl.Value2=c.Firstname;
     adminCtrl.FieldName3='Firstname';
     adminCtrl.Operator3='equals';
     adminCtrl.Value3=c.Firstname;
     adminCtrl.FieldName4='Firstname';
     adminCtrl.Operator4='equals';
     adminCtrl.Value4=c.Firstname;
     adminCtrl.FieldName5='Firstname';
     adminCtrl.Operator5='equals';
     adminCtrl.Value5=c.Firstname;
     adminCtrl.FieldName6='Firstname';
     adminCtrl.Operator6='equals';
     adminCtrl.Value6=c.Firstname;
     adminCtrl.FieldName7='Firstname';
     adminCtrl.Operator7='equals';
     adminCtrl.Value7=c.Firstname; 
     adminCtrl.FieldName8='Firstname';
     adminCtrl.Operator8='equals';
     adminCtrl.Value8=c.Firstname;
     adminCtrl.FieldName9='Firstname';
     adminCtrl.Operator9='equals';
     adminCtrl.Value9=c.Firstname;
     adminCtrl.FieldName10='Firstname';
     adminCtrl.Operator10='equals';
     adminCtrl.Value10=c.Firstname;
     adminCtrl.showAdvanceFormula=true;
     adminCtrl.advanceFilterLogic='1 AND 2';
     adminCtrl.selectedCustomCallList=ACL.Name;
     adminCtrl.deleteMultipleCallLists=ACL2.Name;
     adminCtrl.generateCSVCallList=ACL1.Name;
     adminCtrl.visibilityCustomCallList=ACL.Name;
     adminCtrl.campaignStatus='Accepted';
     adminCtrl.create_ActivityInLastXDays=0;
     adminCtrl.create_openTaskDueInXdays=0;
     adminCtrl.showSuccMsg='success';
     adminCtrl.filterLogic='And';
     adminCtrl.closePopup();
     adminCtrl.getautomatedCallList();
     adminCtrl.save1();
     adminCtrl.deleteMultipleCallList();
     adminCtrl.getOperatorList1();
     adminCtrl.getOperatorList2();
     adminCtrl.getOperatorList3();
     adminCtrl.getOperatorList4();
     adminCtrl.getOperatorList5();
     adminCtrl.getOperatorList6();
     adminCtrl.getOperatorList7();
     adminCtrl.getOperatorList8();
     adminCtrl.getOperatorList9();
     adminCtrl.getOperatorList10();
     adminCtrl.validate('Create Call List',ACL.Name,'','FieldName1','FieldName2','FieldName3','FieldName4','FieldName5','FieldName6','FieldName7','FieldName8','FieldName9','FieldName10','Operator1','Operator2','Operator3','Operator4','Operator5','Operator6','Operator7','Operator8','Operator9','Operator10');
     adminCtrl.generateCSV();
     adminCtrl.getAdvisorAndRelatedListsDetails();
     adminCtrl.getmanageCallList();
     adminCtrl.updateCockpitSettingsValues();
     adminCtrl.updateVisibilityForCustomCallLists();
     
     Test.StopTest();      
   }

   static testmethod void advanceFilterLogic(){

     Contact c = new Contact();
     c.Firstname='Test';
     c.Lastname='record';
     insert c;
     System.assert(c != null);
     
     Contact c1 = new Contact();
     c1.Firstname='Test';
     c1.Lastname='record';
     insert c1;
     System.assert(c1 != null);
     
         
     List<Contact> scope1 = new List<Contact>();
     scope1.add(c); 
     
     Campaign cn = new Campaign();
     cn.name='Blast Email';
     insert cn;
     System.assert(cn != null);
 
     CampaignMember cr = new CampaignMember();
     cr.campaignid = cn.id;
     cr.contactid = c.id;
     insert cr;
     System.assert(cr != null);
  
     
     Automated_Call_List__c ACL = new Automated_Call_List__c();
     ACL.name ='TestingCallList'+c.id;
     ACL.Campaign__c =cn.id;
     ACL.Type_Of_Call_List__c='Campaign Call List';
     ACL.Dynamic_Query_Filters__c='Select Campaign Status&&&&Alternative_Investments_Firm_Provided__c&&!!equals&&!!null-->X1031_Relationship__c&&!!equals&&!!null-->Account.HNW_Accounts_2M__c&&!!equals&&!!null-->Account.Analyst_and_Research_Role__c&&!!equals&&!!False-->Account.Name&&!!equals&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!starts with&&!!null-->Account.Active__c&&!!starts with&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!contains&&!!null-->Account.BusinessProfileTrackPercent__c&&!!equals&&!!null-->Account.CapleaseNumberofLocations__c&&!!equals&&!!null';
     //ACL.Last_Activity_Date__c=Date.today()-30;
     ACL.Archived__c=false;
     ACL.isAdvanceFormula__c=false;
     ACL.Visiblity__c='Visible to all users';
     ACL.TempLock__c='';
     ACL.Activity_In_Last_X_Days__c= 7;
     ACL.Open_Task_Due_In_X_Days__c = 7;
     ACL.Description__c = 'testing';
     ACL.Priority__c = true; 
     insert ACL;
     System.assert(ACL != null);
     
     ACL.TempLock__c = 'General Lock';
     update ACL;
    
     ACL.TempLock__c = '';
     ACL.Internal_Flag__c  =true;
     update ACL;
     
     Automated_Call_List__c ACL1 = new Automated_Call_List__c();
     ACL1.name ='TestingCallList1'+c.id;
     ACL1.Dynamic_Query_Filters__c='Select Campaign Status&&&&Alternative_Investments_Firm_Provided__c&&!!equals&&!!null-->X1031_Relationship__c&&!!equals&&!!null-->Account.HNW_Accounts_2M__c&&!!equals&&!!null-->Account.Analyst_and_Research_Role__c&&!!equals&&!!False-->Account.Name&&!!equals&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!starts with&&!!null-->Account.Active__c&&!!starts with&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!contains&&!!null-->Account.BusinessProfileTrackPercent__c&&!!equals&&!!null-->Account.CapleaseNumberofLocations__c&&!!equals&&!!null';
     ACL1.Custom_Logic__c = '1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8 AND 9 AND 10';
     //ACL1.Last_Activity_Date__c=Date.today()-30;
     ACL1.Archived__c=false;
     ACL1.Type_Of_Call_List__c='Campaign Call List';
     ACL1.IsDeleted__c=false;
     ACL1.Visiblity__c='Visible to all users';
     ACL1.TempLock__c='';
     ACL1.Activity_In_Last_X_Days__c= 7;
     ACL1.Open_Task_Due_In_X_Days__c = 7;
     ACL1.Description__c = 'testing';
     ACL1.Priority__c = true; 
     ACL.isAdvanceFormula__c=true;   
     ACL1.Dynamic_Query__c='select Id,Name,Internal_Wholesaler1__c,Territory__c,RIA_Territory__c from Contact where (First_Name__c = \'Naresh\')';
     insert ACL1;
     
     System.assert(ACL1 != null);
     
     Automated_Call_List__c ACL2 = new Automated_Call_List__c();
     ACL2.name ='TestingCallList2'+c.id;
     ACL2.Dynamic_Query_Filters__c='Select Campaign Status&&&&First_Name__c&&!!equals&&!!Naresh';
     ACL2.Custom_Logic__c = 'AND';
     //ACL2.Last_Activity_Date__c=Date.today()-30;
     ACL2.Archived__c=false;
     ACL2.Type_Of_Call_List__c='Campaign Call List';
     ACL2.Visiblity__c='Visible to all users';
     ACL2.IsDeleted__c=false;
     ACL2.TempLock__c='';
     ACL2.Activity_In_Last_X_Days__c= 7;
     ACL2.Open_Task_Due_In_X_Days__c = 7;
     ACL2.Description__c = 'testing';
     ACL2.Priority__c = true;              
     ACL2.Dynamic_Query__c='select Id,Name,Internal_Wholesaler1__c,Territory__c,RIA_Territory__c from Contact where (First_Name__c = \'Naresh\')';
     insert ACL2;
     System.assert(ACL2 != null);
          
     contact_call_list__c clist = new contact_call_list__c();
     clist.Call_Complete__c = false;
     clist.Automated_Call_List__c = ACL.id;
     clist.CallList_Name__c = ACL.Name;
     clist.Description__c = 'Testing';
     clist.ContactCalllist_UniqueConbination__c=ACL.Name+c.Id;
     clist.Contact__c = c.id;
     insert clist;
     System.assert(clist != null);
     
     contact_call_list__c clist1 = new contact_call_list__c();
     clist1.Call_Complete__c = false;
     clist1.Automated_Call_List__c = ACL1.id;
     clist1.CallList_Name__c = ACL1.Name;
     clist1.Description__c = 'Testing';
     clist1.ContactCalllist_UniqueConbination__c=ACL1.Name+c1.Id;
     clist1.Contact__c = c1.id;
     insert clist1;

     System.assert(clist1 != null);   
     
     Id MngCallListRecId = Schema.SObjectType.Manage_Cockpit__c.getRecordTypeInfosByName().get('Manage Call Lists').getRecordTypeId();
     Manage_Cockpit__c ManageCallListRec = new Manage_Cockpit__c();
     ManageCallListRec.recordtypeid = MngCallListRecId;
     ManageCallListRec.Archive_Call_List_in_X_days__c = '7';
     insert ManageCallListRec;
     
     System.assert(ManageCallListRec != null);
     
     Test.StartTest(); 

     Cockpit_AdminCtrl adminCtrl = new Cockpit_AdminCtrl();
     adminCtrl.automatedCallList.Name ='test ACL';
     adminCtrl.FieldName1='Firstname';
     adminCtrl.Operator1='equals';
     adminCtrl.Value1=c.Firstname;
     adminCtrl.FieldName2='Firstname';
     adminCtrl.Operator2='equals';
     adminCtrl.Value2=c.Firstname;
     adminCtrl.FieldName3='Firstname';
     adminCtrl.Operator3='equals';
     adminCtrl.Value3=c.Firstname;
     adminCtrl.FieldName4='Firstname';
     adminCtrl.Operator4='equals';
     adminCtrl.Value4=c.Firstname;
     adminCtrl.showAdvanceFormula=true;
     adminCtrl.advanceFilterLogic='(1 AND 2 and (3 or 4))';
     adminCtrl.selectedCustomCallList=ACL.Name;
     adminCtrl.deleteMultipleCallLists=ACL2.Name;
     adminCtrl.generateCSVCallList=ACL1.Name;
     adminCtrl.visibilityCustomCallList=ACL.Name;
     adminCtrl.campaignStatus='Accepted';
     adminCtrl.create_ActivityInLastXDays=0;
     adminCtrl.create_openTaskDueInXdays=0;
     adminCtrl.showSuccMsg='success';
     adminCtrl.closePopup();
     adminCtrl.getautomatedCallList();
     adminCtrl.save1();
     adminCtrl.deleteMultipleCallList();
     adminCtrl.getOperatorList1();
     adminCtrl.getOperatorList2();
     adminCtrl.getOperatorList3();
     adminCtrl.getOperatorList4();
     adminCtrl.validate('Create Call List',ACL.Name,'','FieldName1','FieldName2','FieldName3','FieldName4','FieldName5','FieldName6','FieldName7','FieldName8','FieldName9','FieldName10','Operator1','Operator2','Operator3','Operator4','Operator5','Operator6','Operator7','Operator8','Operator9','Operator10');
     adminCtrl.generateCSV();
     adminCtrl.getAdvisorAndRelatedListsDetails();
     adminCtrl.getmanageCallList();
     adminCtrl.updateCockpitSettingsValues();
     adminCtrl.updateVisibilityForCustomCallLists();
     
     Test.StopTest();      
   }

   static testmethod void advanceFilterLogicNegative(){

     Contact c = new Contact();
     c.Firstname='Test';
     c.Lastname='record';
     insert c;
     System.assert(c != null);
     
     Contact c1 = new Contact();
     c1.Firstname='Test';
     c1.Lastname='record';
     insert c1;
     System.assert(c1 != null);
     
         
     List<Contact> scope1 = new List<Contact>();
     scope1.add(c); 
     
     Campaign cn = new Campaign();
     cn.name='Blast Email';
     insert cn;
     System.assert(cn != null);
 
     CampaignMember cr = new CampaignMember();
     cr.campaignid = cn.id;
     cr.contactid = c.id;
     insert cr;
     System.assert(cr != null);
  
     
     Automated_Call_List__c ACL = new Automated_Call_List__c();
     ACL.name ='TestingCallList'+c.id;
     ACL.Campaign__c =cn.id;
     ACL.Type_Of_Call_List__c='Campaign Call List';
     ACL.Dynamic_Query_Filters__c='Select Campaign Status&&&&Alternative_Investments_Firm_Provided__c&&!!equals&&!!null-->X1031_Relationship__c&&!!equals&&!!null-->Account.HNW_Accounts_2M__c&&!!equals&&!!null-->Account.Analyst_and_Research_Role__c&&!!equals&&!!False-->Account.Name&&!!equals&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!starts with&&!!null-->Account.Active__c&&!!starts with&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!contains&&!!null-->Account.BusinessProfileTrackPercent__c&&!!equals&&!!null-->Account.CapleaseNumberofLocations__c&&!!equals&&!!null';
     //ACL.Last_Activity_Date__c=Date.today()-30;
     ACL.Archived__c=false;
     ACL.isAdvanceFormula__c=false;
     ACL.Visiblity__c='Visible to all users';
     ACL.TempLock__c='';
     ACL.Activity_In_Last_X_Days__c= 7;
     ACL.Open_Task_Due_In_X_Days__c = 7;
     ACL.Description__c = 'testing';
     ACL.Priority__c = true; 
     insert ACL;
     System.assert(ACL != null);
     
     ACL.TempLock__c = 'General Lock';
     update ACL;
    
     ACL.TempLock__c = '';
     ACL.Internal_Flag__c  =true;
     update ACL;
     
     Automated_Call_List__c ACL1 = new Automated_Call_List__c();
     ACL1.name ='TestingCallList1'+c.id;
     ACL1.Dynamic_Query_Filters__c='Select Campaign Status&&&&Alternative_Investments_Firm_Provided__c&&!!equals&&!!null-->X1031_Relationship__c&&!!equals&&!!null-->Account.HNW_Accounts_2M__c&&!!equals&&!!null-->Account.Analyst_and_Research_Role__c&&!!equals&&!!False-->Account.Name&&!!equals&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!starts with&&!!null-->Account.Active__c&&!!starts with&&!!null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c&&!!contains&&!!null-->Account.BusinessProfileTrackPercent__c&&!!equals&&!!null-->Account.CapleaseNumberofLocations__c&&!!equals&&!!null';
     ACL1.Custom_Logic__c = '1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8 AND 9 AND 10';
     //ACL1.Last_Activity_Date__c=Date.today()-30;
     ACL1.Archived__c=false;
     ACL1.Type_Of_Call_List__c='Campaign Call List';
     ACL1.IsDeleted__c=false;
     ACL1.Visiblity__c='Visible to all users';
     ACL1.TempLock__c='';
     ACL1.Activity_In_Last_X_Days__c= 7;
     ACL1.Open_Task_Due_In_X_Days__c = 7;
     ACL1.Description__c = 'testing';
     ACL1.Priority__c = true; 
     ACL.isAdvanceFormula__c=true;   
     ACL1.Dynamic_Query__c='select Id,Name,Internal_Wholesaler1__c,Territory__c,RIA_Territory__c from Contact where (First_Name__c = \'Naresh\')';
     insert ACL1;
     
     System.assert(ACL1 != null);
     
     Automated_Call_List__c ACL2 = new Automated_Call_List__c();
     ACL2.name ='TestingCallList2'+c.id;
     ACL2.Dynamic_Query_Filters__c='Select Campaign Status&&&&First_Name__c&&!!equals&&!!Naresh';
     ACL2.Custom_Logic__c = 'AND';
     //ACL2.Last_Activity_Date__c=Date.today()-30;
     ACL2.Archived__c=false;
     ACL2.Type_Of_Call_List__c='Campaign Call List';
     ACL2.Visiblity__c='Visible to all users';
     ACL2.IsDeleted__c=false;
     ACL2.TempLock__c='';
     ACL2.Activity_In_Last_X_Days__c= 7;
     ACL2.Open_Task_Due_In_X_Days__c = 7;
     ACL2.Description__c = 'testing';
     ACL2.Priority__c = true;              
     ACL2.Dynamic_Query__c='select Id,Name,Internal_Wholesaler1__c,Territory__c,RIA_Territory__c from Contact where (First_Name__c = \'Naresh\')';
     insert ACL2;
     System.assert(ACL2 != null);
          
     contact_call_list__c clist = new contact_call_list__c();
     clist.Call_Complete__c = false;
     clist.Automated_Call_List__c = ACL.id;
     clist.CallList_Name__c = ACL.Name;
     clist.Description__c = 'Testing';
     clist.ContactCalllist_UniqueConbination__c=ACL.Name+c.Id;
     clist.Contact__c = c.id;
     insert clist;
     System.assert(clist != null);
     
     contact_call_list__c clist1 = new contact_call_list__c();
     clist1.Call_Complete__c = false;
     clist1.Automated_Call_List__c = ACL1.id;
     clist1.CallList_Name__c = ACL1.Name;
     clist1.Description__c = 'Testing';
     clist1.ContactCalllist_UniqueConbination__c=ACL1.Name+c1.Id;
     clist1.Contact__c = c1.id;
     insert clist1;

     System.assert(clist1 != null);   
     
     Id MngCallListRecId = Schema.SObjectType.Manage_Cockpit__c.getRecordTypeInfosByName().get('Manage Call Lists').getRecordTypeId();
     Manage_Cockpit__c ManageCallListRec = new Manage_Cockpit__c();
     ManageCallListRec.recordtypeid = MngCallListRecId;
     ManageCallListRec.Archive_Call_List_in_X_days__c = '7';
     insert ManageCallListRec;
     
     System.assert(ManageCallListRec != null);
     
     Test.StartTest(); 

     Cockpit_AdminCtrl adminCtrl = new Cockpit_AdminCtrl();
     adminCtrl.automatedCallList.Name ='test ACL';
     adminCtrl.FieldName1='Firstname';
     adminCtrl.Operator1='equals';
     adminCtrl.Value1=c.Firstname;
     adminCtrl.FieldName2='Firstname';
     adminCtrl.Operator2='equals';
     adminCtrl.Value2=c.Firstname;
     adminCtrl.FieldName3='Firstname';
     adminCtrl.Operator3='equals';
     adminCtrl.Value3=c.Firstname;
     adminCtrl.FieldName4='Firstname';
     adminCtrl.Operator4='equals';
     adminCtrl.Value4=c.Firstname;
     adminCtrl.showAdvanceFormula=true;
     adminCtrl.advanceFilterLogic='(1 (AND 2 and (3 or 4))';
     adminCtrl.selectedCustomCallList=ACL.Name;
     adminCtrl.deleteMultipleCallLists=ACL2.Name;
     adminCtrl.generateCSVCallList=ACL1.Name;
     adminCtrl.visibilityCustomCallList=ACL.Name;
     adminCtrl.campaignStatus='Accepted';
     adminCtrl.create_ActivityInLastXDays=0;
     adminCtrl.create_openTaskDueInXdays=0;
     adminCtrl.showSuccMsg='success';
     adminCtrl.closePopup();
     adminCtrl.getautomatedCallList();
     adminCtrl.save1();
     adminCtrl.deleteMultipleCallList();
     adminCtrl.getOperatorList1();
     adminCtrl.getOperatorList2();
     adminCtrl.getOperatorList3();
     adminCtrl.getOperatorList4();
     adminCtrl.validate('Create Call List',ACL.Name,'','FieldName1','FieldName2','FieldName3','FieldName4','FieldName5','FieldName6','FieldName7','FieldName8','FieldName9','FieldName10','Operator1','Operator2','Operator3','Operator4','Operator5','Operator6','Operator7','Operator8','Operator9','Operator10');
     adminCtrl.generateCSV();
     adminCtrl.getAdvisorAndRelatedListsDetails();
     adminCtrl.getmanageCallList();
     adminCtrl.updateCockpitSettingsValues();
     adminCtrl.updateVisibilityForCustomCallLists();
     
     Test.StopTest();      
   }
 }