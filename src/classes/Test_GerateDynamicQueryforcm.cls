@isTest
public class Test_GerateDynamicQueryforcm
{
    
    /*static testmethod void myTestMethod1()
    {
      Test.startTest();
      GerateDynamicQueryforcm g = new  GerateDynamicQueryforcm();
      
      g.conditionquery('Phone','Equals','123456');
      g.conditionquery('LastName','contains','s');
      g.conditionquery('LastName','contains','s,d');
      
      g.conditionquery('LastName','does not contain','s');
      g.conditionquery('LastName','does not contain','s,d');
      
      g.conditionquery('LastName','Starts With','s');
      g.conditionquery('LastName','Starts With','s,d');
      
      g.conditionquery('License__c','includes','7;21');
      g.conditionquery('License__c','includes','7,21');
      g.conditionquery('License__c','includes','7');
      
      g.conditionquery('License__c','excludes','7;21');
      g.conditionquery('License__c','excludes','7,21');
      g.conditionquery('License__c','excludes','21');
      
      g.conditionquery('Phone','Equals',null);
      g.conditionquery('Phone','not equal to',null);
      g.conditionquery('LastName','contains',null);
      
      //Equals operator
      
      g.conditionquery('REIT_Investments_Amount__c','Equals','5000');
      g.conditionquery('X1031_Kit_Received__c','Equals','True');
   
      
      g.conditionquery('Allocated_to_RE_Expired_On_Contact__c','Equals','17/11/1989');
      g.conditionquery('Allocated_to_RE_Expired_On_Contact__c','Equals','7/1/1989');
      
      g.conditionquery('LastName','Equals','s,d');
      g.conditionquery('TaskCreatedDate__c','Equals','10/14/2011 11:46 AM');
      g.conditionquery('TaskCreatedDate__c','Equals','2/4/2011 11:46 AM');
      g.conditionquery('TaskCreatedDate__c','Equals','2/4/2011 1:6 AM');
      
      
      g.conditionquery('TaskCreatedDate__c','Equals','10/14/2011 11:46 PM');
      g.conditionquery('TaskCreatedDate__c','Equals','2/4/2011 11:46 PM');
      g.conditionquery('TaskCreatedDate__c','Equals','2/4/2011 1:6 PM');
      
      //Not Equal To Operator
      
      g.conditionquery('REIT_Investments_Amount__c','not equal to','5000');
      g.conditionquery('X1031_Kit_Received__c','not equal to','True');
   
      
      g.conditionquery('Allocated_to_RE_Expired_On_Contact__c','not equal to','17/11/1989');
      g.conditionquery('Allocated_to_RE_Expired_On_Contact__c','not equal to','7/1/1989');
      
      g.conditionquery('LastName','not equal to','s,d');
      g.conditionquery('TaskCreatedDate__c','not equal to','10/14/2011 11:46 AM');
      g.conditionquery('TaskCreatedDate__c','not equal to','2/4/2011 11:46 AM');
      g.conditionquery('TaskCreatedDate__c','not equal to','2/4/2011 1:6 AM');
      
      
      g.conditionquery('TaskCreatedDate__c','not equal to','10/14/2011 11:46 PM');
      g.conditionquery('TaskCreatedDate__c','not equal to','2/4/2011 11:46 PM');
      g.conditionquery('TaskCreatedDate__c','not equal to','2/4/2011 1:6 PM');
      
      //Less than operator
      
      g.conditionquery('REIT_Investments_Amount__c','less than','5000');
      g.conditionquery('X1031_Kit_Received__c','less than','True');
   
      
      g.conditionquery('Allocated_to_RE_Expired_On_Contact__c','less than','17/11/1989');
      g.conditionquery('Allocated_to_RE_Expired_On_Contact__c','less than','7/1/1989');
      
      g.conditionquery('REIT_Investments_Amount__c','less than','100,200');
      g.conditionquery('TaskCreatedDate__c','less than','10/14/2011 11:46 AM');
      g.conditionquery('TaskCreatedDate__c','less than','2/4/2011 11:46 AM');
      g.conditionquery('TaskCreatedDate__c','less than','2/4/2011 1:6 AM');
      
      
      g.conditionquery('TaskCreatedDate__c','less than','10/14/2011 11:46 PM');
      g.conditionquery('TaskCreatedDate__c','less than','2/4/2011 11:46 PM');
      g.conditionquery('TaskCreatedDate__c','less than','2/4/2011 1:6 PM');
      
      //greater than operator
      
      g.conditionquery('REIT_Investments_Amount__c','greater than','5000');
      g.conditionquery('X1031_Kit_Received__c','greater than','True');
   
      
      g.conditionquery('Allocated_to_RE_Expired_On_Contact__c','greater than','17/11/1989');
      g.conditionquery('Allocated_to_RE_Expired_On_Contact__c','greater than','7/1/1989');
      
      g.conditionquery('REIT_Investments_Amount__c','greater than','100,200');
      g.conditionquery('TaskCreatedDate__c','greater than','10/14/2011 11:46 AM');
      g.conditionquery('TaskCreatedDate__c','greater than','2/4/2011 11:46 AM');
      g.conditionquery('TaskCreatedDate__c','greater than','2/4/2011 1:6 AM');
      
      
      g.conditionquery('TaskCreatedDate__c','greater than','10/14/2011 11:46 PM');
      g.conditionquery('TaskCreatedDate__c','greater than','2/4/2011 11:46 PM');
      g.conditionquery('TaskCreatedDate__c','greater than','2/4/2011 1:6 PM');
      
      //less or equal operator
      
      g.conditionquery('REIT_Investments_Amount__c','less or equal','5000');
      g.conditionquery('X1031_Kit_Received__c','less or equal','True');
   
      
      g.conditionquery('Allocated_to_RE_Expired_On_Contact__c','less or equal','17/11/1989');
      g.conditionquery('Allocated_to_RE_Expired_On_Contact__c','less or equal','7/1/1989');
      
      g.conditionquery('REIT_Investments_Amount__c','less or equal','100,200');
      g.conditionquery('TaskCreatedDate__c','less or equal','10/14/2011 11:46 AM');
      g.conditionquery('TaskCreatedDate__c','less or equal','2/4/2011 11:46 AM');
      g.conditionquery('TaskCreatedDate__c','less or equal','2/4/2011 1:6 AM');
      
      
      g.conditionquery('TaskCreatedDate__c','less or equal','10/14/2011 11:46 PM');
      g.conditionquery('TaskCreatedDate__c','less or equal','2/4/2011 11:46 PM');
      g.conditionquery('TaskCreatedDate__c','less or equal','2/4/2011 1:6 PM');
      
      //greater or equal operator
      
      g.conditionquery('REIT_Investments_Amount__c','greater or equal','5000');
      g.conditionquery('X1031_Kit_Received__c',' greater or equal','True');
   
      
      g.conditionquery('Allocated_to_RE_Expired_On_Contact__c','greater or equal','17/11/1989');
      g.conditionquery('Allocated_to_RE_Expired_On_Contact__c','greater or equal','7/1/1989');
      
      g.conditionquery('REIT_Investments_Amount__c','greater or equal','100,200');
      g.conditionquery('TaskCreatedDate__c','greater or equal','10/14/2011 11:46 AM');
      g.conditionquery('TaskCreatedDate__c','greater or equal','2/4/2011 11:46 AM');
      g.conditionquery('TaskCreatedDate__c','greater or equal','2/4/2011 1:6 AM');
      
      
      g.conditionquery('TaskCreatedDate__c','greater or equal','10/14/2011 11:46 PM');
      g.conditionquery('TaskCreatedDate__c','greater or equal','2/4/2011 11:46 PM');
      g.conditionquery('TaskCreatedDate__c','greater or equal','2/4/2011 1:6 PM');
      g.conditionquery('REIT_Investments_Amount__c','greater or equal','10000');
      
      
      
      
      Test.stopTest();
    }*/
    }