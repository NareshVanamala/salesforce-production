global with sharing class RC_UpdateAppraisaldateOFPropertyFinal
{
    public static void UpdateAppraisalDate1(list<Task_Plan__c> ataskid )
    {
         list<id>propreturnlist=new list<id>(); 
         list<id>dealidlist=new list<id>(); 
         list<Task_Plan__c> tasklist =ataskid ;
         //tasklist=[select id,name,Date_Needed__c,Date_Ordered__c,Date_Received__c,TaskRelatedto__c from Task_Plan__c where id in:ataskid ]; 
         system.debug('here is my list'+tasklist);
        
         list<Task_Plan__c>appraisaltasklist=new list<Task_Plan__c>();
         list<Task_Plan__c>PropertyConditiontsklist=new list<Task_Plan__c>();
         list<Task_Plan__c>RoofReporttsklist=new list<Task_Plan__c>();
         list<Task_Plan__c>CopyofCrs=new list<Task_Plan__c>();
         list<Task_Plan__c>EnvirornmentalList=new list<Task_Plan__c>(); 
         
            
           Map<Id, List<Task_Plan__c>>taskplanidmap  = new Map<Id, List<Task_Plan__c >>();
           for(Task_Plan__c tsp:tasklist )
          {
          
             dealidlist.add(tsp.TaskRelatedto__c);
             taskplanidmap.put(tsp.TaskRelatedto__c,new list<Task_Plan__c>{tsp});  
         } 
      
                 
       list<RC_Property__c> myProplist=[SELECT Related_Deal__c ,Property_Condition_Report_Date_Needed__c,Property_Condition_Report_Date_Ordered__c,Property_Condition_Report_Date_Received__c,Roof_Report_Date_Ordered__c,Roof_Report_Date_Received__c,Roof_Repot_Date_Needed__c,Environmental_Date_Needed__c,Environmental_Date_Ordered__c,Environmental_Date_Received__c,Copy_of_CC_R_s_Date_Needed__c,Copy_of_CC_R_s_Date_Ordered__c,Copy_of_CC_R_s_Date_Received__c,Id,Appraisal_Date_Needed__c,Appraisal_Date_Ordered__c,Appraisal_Date_Received__c FROM RC_Property__c WHERE Related_Deal__c in:dealidlist];
       list<RC_Property__c> UpdatedmyProplis=new list<RC_Property__c>();
        for(RC_Property__c myProp:myProplist) 
        {
          
            appraisaltasklist=new list<Task_Plan__c>();
            PropertyConditiontsklist=new list<Task_Plan__c>();
            RoofReporttsklist=new list<Task_Plan__c>();
            CopyofCrs=new list<Task_Plan__c>();
            EnvirornmentalList=new list<Task_Plan__c>(); 
           list<Task_Plan__c>tsklist=taskplanidmap.get(myProp.Related_Deal__c);
             for(Task_Plan__c tsk:tsklist)
             {
                      
                 if(tsk.name=='appraisal')
                 appraisaltasklist.add(tsk);
                if(tsk.name=='Property Condition Report')
             PropertyConditiontsklist.add(tsk);
            if(tsk.name=='Roof Report')
             RoofReporttsklist.add(tsk);
            if(tsk.name=='Copy of CC&Rs saved and distributed')
             CopyofCrs.add(tsk);  
            if(tsk.name=='Environmental Report')
             EnvirornmentalList.add(tsk);
           }
           
           system.debug('appraisaltasklist'+appraisaltasklist);
           system.debug('PropertyConditionReporlist'+PropertyConditiontsklist);
           system.debug(' RoofReporttasklist'+RoofReporttsklist);
           system.debug('CopyofCrstasklist'+CopyofCrs);
           system.debug(' Envirornmentaltasklist'+EnvirornmentalList);
              if(appraisaltasklist.size()>0)
           {
                  if(appraisaltasklist[0].Date_Needed__c!=null)
                myProp.Appraisal_Date_Needed__c=appraisaltasklist[0].Date_Needed__c;
                if(appraisaltasklist[0].Date_Received__c!=null)
                myProp.Appraisal_Date_Received__c=appraisaltasklist[0].Date_Received__c;
                if(appraisaltasklist[0].Date_Ordered__c!=null)
                myProp.Appraisal_Date_Ordered__c=appraisaltasklist[0].Date_Ordered__c;
           }
           
           if(PropertyConditiontsklist.size()>0)
           { 
             
             
                 if(PropertyConditiontsklist[0].Date_Needed__c!=null)
                myProp.Property_Condition_Report_Date_Needed__c=PropertyConditiontsklist[0].Date_Needed__c;
                 if(PropertyConditiontsklist[0].Date_Received__c!=null)
                myProp.Property_Condition_Report_Date_Received__c=PropertyConditiontsklist[0].Date_Received__c;
                if(PropertyConditiontsklist[0].Date_Ordered__c!=null)
                myProp.Property_Condition_Report_Date_Ordered__c=PropertyConditiontsklist[0].Date_Ordered__c;
           
           }
           if(RoofReporttsklist.size()>0)
           { 
                if(RoofReporttsklist[0].Date_Needed__c!=null)
                myProp.Roof_Repot_Date_Needed__c=RoofReporttsklist[0].Date_Needed__c;
                if(RoofReporttsklist[0].Date_Received__c!=null)
                myProp.Roof_Report_Date_Received__c=RoofReporttsklist[0].Date_Received__c;
                if(RoofReporttsklist[0].Date_Ordered__c!=null)
                myProp.Roof_Report_Date_Ordered__c=RoofReporttsklist[0].Date_Ordered__c;
                   
           }
           if(EnvirornmentalList.size()>0)
           { 
                 if(EnvirornmentalList[0].Date_Needed__c!=null)
                myProp.Environmental_Date_Needed__c=EnvirornmentalList[0].Date_Needed__c;
                  
                   if(EnvirornmentalList[0].Date_Received__c!=null)
                 myProp.Environmental_Date_Received__c=EnvirornmentalList[0].Date_Received__c;
                 
                 if(EnvirornmentalList[0].Date_Ordered__c!=null)
                myProp.Environmental_Date_Ordered__c=EnvirornmentalList[0].Date_Ordered__c;
                   
           }
           if(CopyofCrs.size()>0)
           { 
                 if(CopyofCrs[0].Date_Needed__c!=null)
                myProp.Copy_of_CC_R_s_Date_Needed__c=CopyofCrs[0].Date_Needed__c;
                
                 if(CopyofCrs[0].Date_Received__c!=null)
                myProp.Copy_of_CC_R_s_Date_Received__c=CopyofCrs[0].Date_Received__c;
                
                if(CopyofCrs[0].Date_Ordered__c!=null)
                myProp.Copy_of_CC_R_s_Date_Ordered__c=CopyofCrs[0].Date_Ordered__c;
                   
           }
             UpdatedmyProplis.add(myProp);
             
        
        }
    if (Schema.sObjectType.RC_Property__c.isUpdateable()) 

          update UpdatedmyProplis;
       
      }
 }