global class LeaseChargeDetailsMassUpdate
{
    List<Lease__c> Lease = new List<Lease__c>();       
    Public Lease_Charges_Request__c leaseChargeRequestObj {get;set;}
    public LeaseChargeDetailsMassUpdate(ApexPages.StandardController controller) {
        leaseChargeRequestObj = new Lease_Charges_Request__c ();
        leaseChargeRequestObj = (Lease_Charges_Request__c)controller.getRecord();
        
    } 
   
    webservice static void MassUpdate(Id leasechargeId)
    {
        
        List<Lease_Charges_Request__c> LCR1= new List<Lease_Charges_Request__c>();
        
        if(leasechargeId!=null || Test.isRunningTest())
        {
            List<Lease_Charges_Request__c> LCR = new List<Lease_Charges_Request__c>();
            LCR = [select id,Lease__r.MRI_PROPERTY__r.Property_ID__c ,Unique_ID__c,Approval_Status__c, MRI_Property__c,Asset_Manager__c,Director_Vice_President__c,Property_Manager__c,SVP__c,Source_Code__c,Lease__r.MRI_PROPERTY__r.Common_Name__c,Lease__r.Lease_ID__c,Pdf_Name__c from Lease_Charges_Request__c where Id =:leasechargeId];
            system.debug('LCR'+LCR);
            system.debug('LCR'+LCR[0].MRI_Property__c);
            List<Lease__c> Lease = new List<Lease__c>();
            Lease = [select id,Name,Lease_ID__c from Lease__c where MRI_PROPERTY__c=:LCR[0].MRI_Property__c and Lease_Status__c = 'Active'];
           
            for(Lease__c newlease:Lease){
                
                List<Lease_Charges_Request__c> eachLeaseChargeReq= new List<Lease_Charges_Request__c>();
                for( Lease_Charges_Request__c temp :LCR)
                   {
                
                       if(temp.Lease__c!=newlease.id || Test.isRunningTest())
                       {
                           system.debug('Inner loop Lease Id'+newlease.id);
                           Lease_Charges_Request__c NewLCR = new Lease_Charges_Request__c();
                           NewLCR.Unique_ID__c=temp.Unique_ID__c;
                           NewLCR.Approval_Status__c=temp.Approval_Status__c;
                           NewLCR.Asset_Manager__c=temp.Asset_Manager__c;
                           NewLCR.Director_Vice_President__c=temp.Director_Vice_President__c;
                           NewLCR.Lease__c=newlease.id;
                           NewLCR.Property_Manager__c=temp.Property_Manager__c;
                           NewLCR.Source_Code__c=temp.Source_Code__c;
                           NewLCR.SVP__c=temp.SVP__c;
                           //NewLCR.Total_Credit__c=temp.Total_Credit__c;
                           system.debug('NewLCR'+NewLCR);
                           eachLeaseChargeReq.add(NewLCR);
                           insert eachLeaseChargeReq;
                        
                            List<Lease_Charge_Details__c> leaseChargeDetailList = new List<Lease_Charge_Details__c>();
                            for(Lease_Charge_Details__c temp1 : [select id,Name,Charge_Code__c,Charge_Type__c,Mass_Update__c,Description__c,End_Date__c,Gross_Amount__c,MRI_Property__c,NetAmount__c,Notes__c,Source_Code__c,Start_Date__c,Support_Required__c,Type__c,LeaseChargesRequest__c from Lease_Charge_Details__c where Mass_Update__c = TRUE and LeaseChargesRequest__c=:leasechargeId]){
                             
                                                Lease_Charge_Details__c newlcrd = new Lease_Charge_Details__c();
                                                newlcrd.LeaseChargesRequest__c=eachLeaseChargeReq[0].id;
                                                newlcrd.Charge_Code__c=temp1.Charge_Code__c;
                                                newlcrd.Charge_Type__c=temp1.Charge_Type__c;
                                                newlcrd.Description__c=temp1.Description__c;
                                                newlcrd.End_Date__c=temp1.End_Date__c;
                                                newlcrd.Gross_Amount__c=temp1.Gross_Amount__c;
                                                newlcrd.NetAmount__c=temp1.NetAmount__c;
                                                newlcrd.Notes__c=temp1.Notes__c;
                                                newlcrd.Source_Code__c=temp1.Source_Code__c;
                                                newlcrd.Start_Date__c=temp1.Start_Date__c;
                                                newlcrd.Support_Required__c=temp1.Support_Required__c;
                                                newlcrd.Type__c=temp1.Type__c;
                                                newlcrd.Unique_ID__c=eachLeaseChargeReq[0].Unique_ID__c;
                                                system.debug('newlcrd'+newlcrd);
                                                leaseChargeDetailList.add(newlcrd);
                                } 
                            insert leaseChargeDetailList ; 
                            
                        }
                } 
                
            }  
             List<Lease_Charge_Details__c> leaseChargeDetailList1 = new List<Lease_Charge_Details__c>(); 
           for(Lease_Charge_Details__c temp2 : [select id,Name,Charge_Code__c,Unique_ID__c,Charge_Type__c,Mass_Update__c,Description__c,End_Date__c,Gross_Amount__c,MRI_Property__c,NetAmount__c,Notes__c,Source_Code__c,Start_Date__c,Support_Required__c,Type__c,LeaseChargesRequest__c from Lease_Charge_Details__c where Mass_Update__c = TRUE and LeaseChargesRequest__c=:leasechargeId]){
            
                           temp2.Mass_Update__c=FALSE;
                           temp2.Unique_ID__c=LCR[0].Unique_ID__c;
                           leaseChargeDetailList1.add(temp2);
                           
                           
            }
            update leaseChargeDetailList1;
        }
     }
 }