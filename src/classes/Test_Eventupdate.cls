@isTest
public class Test_Eventupdate
 {
    static testmethod void Eventupdate()
    {
       Database.QueryLocator QL;
       Database.BatchableContext BC;
        Database.QueryLocator QL1;
       Database.BatchableContext BC1;
       
       Id rrrecordtype = [select Id, Name,DeveloperName from RecordType where DeveloperName ='NonInvestorRepContact' and SobjectType = 'Contact'].id;
        Account a = new Account();
       a.name = 'Test';
       insert a;
       
       list<contact> clist = new list<contact>();
       list<contact> clist1 = new list<contact>();
       list<contact> clist2 = new list<contact>();
       list<contact> clist3 = new list<contact>();
       list<contact> clist4 = new list<contact>();
       list<contact> clist5 = new list<contact>();
       
       Contact c = new Contact();
       c.lastname = 'Cockpit';
       c.Accountid = a.id;
       c.Relationship__c ='Accounting';
       c.Contact_Type__c = 'Cole';
       c.Wholesaler_Management__c = 'Producer A';
       c.Territory__c ='Chicago';
       c.RIA_Territory__c ='Southeast Region';
       c.RIA_Consultant_Picklist__c ='Brian Mackin';
       c.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c.Wholesaler__c='Andrew Garten';
       c.Regional_Territory_Manager__c ='Andrew Garten';
       c.Internal_Wholesaler__c ='Aaron Williams';
       c.Territory_Zone__c = 'NV-50';
       c.recordtypeid = rrrecordtype;
       c.Priority__c='1';
       c.Next_Planned_External_Appointment__c = null; 
       c.Next_External_Appointment__c = null; 
       clist.add(c);
       insert clist;       
       system.assertequals(c.accountid,a.id);

       Contact c1 = new Contact();
       c1.lastname = 'Cockpit';
       c1.Accountid = a.id;
       c1.Relationship__c ='Accounting';
       c1.Contact_Type__c = 'Cole';
       c1.Wholesaler_Management__c = 'Producer A';
       c1.Territory__c ='Chicago';
       c1.RIA_Territory__c ='Southeast Region';
       c1.RIA_Consultant_Picklist__c ='Brian Mackin';
       c1.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c1.Wholesaler__c='Andrew Garten';
       c1.Regional_Territory_Manager__c ='Andrew Garten';
       c1.Internal_Wholesaler__c ='Aaron Williams';
       c1.Territory_Zone__c = 'NV-50';
       c1.recordtypeid = rrrecordtype;
       c1.Priority__c='2';
       c1.Next_Planned_External_Appointment__c = null; 
       c1.Next_External_Appointment__c = null;
       clist1.add(c1);
       insert clist1;  
       system.assertequals(c1.accountid,a.id);

       Contact c2 = new Contact();
       c2.lastname = 'Cockpit';
       c2.Accountid = a.id;
       c2.Relationship__c ='Accounting';
       c2.Contact_Type__c = 'Cole';
       c2.Wholesaler_Management__c = 'Producer A';
       c2.Territory__c ='Chicago';
       c2.RIA_Territory__c ='Southeast Region';
       c2.RIA_Consultant_Picklist__c ='Brian Mackin';
       c2.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c2.Wholesaler__c='Andrew Garten';
       c2.Regional_Territory_Manager__c ='Andrew Garten';
       c2.Internal_Wholesaler__c ='Aaron Williams';
       c2.Territory_Zone__c = 'NV-50';
       c2.recordtypeid = rrrecordtype;
       c2.Priority__c='3';
        c2.Next_Planned_External_Appointment__c = null; 
       c2.Next_External_Appointment__c = null;
       clist2.add(c2);
       insert clist2;
       
        Contact c3 = new Contact();
       c3.lastname = 'Cockpit';
       c3.Accountid = a.id;
       c3.Relationship__c ='Accounting';
       c3.Contact_Type__c = 'Cole';
       c3.Wholesaler_Management__c = 'Producer A';
       c3.Territory__c ='Chicago';
       c3.RIA_Territory__c ='Southeast Region';
       c3.RIA_Consultant_Picklist__c ='Brian Mackin';
       c3.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c3.Wholesaler__c='Andrew Garten';
       c3.Regional_Territory_Manager__c ='Andrew Garten';
       c3.Internal_Wholesaler__c ='Aaron Williams';
       c3.Territory_Zone__c = 'NV-50';
       c3.recordtypeid = rrrecordtype;
       c3.Priority__c='1';
        c3.Next_Planned_External_Appointment__c = null; 
       c3.Next_External_Appointment__c = System.today().adddays(-30);
       clist3.add(c3);
       insert clist3;
      
       Contact c4 = new Contact();
       c4.lastname = 'Cockpit';
       c4.Accountid = a.id;
       c4.Relationship__c ='Accounting';
       c4.Contact_Type__c = 'Cole';
       c4.Wholesaler_Management__c = 'Producer A';
       c4.Territory__c ='Chicago';
       c4.RIA_Territory__c ='Southeast Region';
       c4.RIA_Consultant_Picklist__c ='Brian Mackin';
       c4.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c4.Wholesaler__c='Andrew Garten';
       c4.Regional_Territory_Manager__c ='Andrew Garten';
       c4.Internal_Wholesaler__c ='Aaron Williams';
       c4.Territory_Zone__c = 'NV-50';
       c4.recordtypeid = rrrecordtype;
       c4.Priority__c='2';
        c4.Next_Planned_External_Appointment__c = null; 
       c4.Next_External_Appointment__c = System.today().adddays(-30);
       clist4.add(c4);
       insert clist4;    
       
       
       Contact c5 = new Contact();
       c5.lastname = 'Cockpit';
       c5.Accountid = a.id;
       c5.Relationship__c ='Accounting';
       c5.Contact_Type__c = 'Cole';
       c5.Wholesaler_Management__c = 'Producer A';
       c5.Territory__c ='Chicago';
       c5.RIA_Territory__c ='Southeast Region';
       c5.RIA_Consultant_Picklist__c ='Brian Mackin';
       c5.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c5.Wholesaler__c='Andrew Garten';
       c5.Regional_Territory_Manager__c ='Andrew Garten';
       c5.Internal_Wholesaler__c ='Aaron Williams';
       c5.Territory_Zone__c = 'NV-50';
       c5.recordtypeid = rrrecordtype;
       c5.Priority__c='3';
        c5.Next_Planned_External_Appointment__c = null; 
       c5.Next_External_Appointment__c = System.today().adddays(-30);
       clist5.add(c5);
       insert clist5;    
       
       REIT_Investment__c R = new REIT_Investment__c();
       R.Rep_Contact__c = c.id;
       R.Deposit_Date__c = System.today().adddays(90);
       insert R;
         
       system.assertequals(R.Rep_Contact__c,c.id);

       Eventupdate e= new Eventupdate();
       ID batchprocessid = Database.executeBatch(e);
       QL = e.start(bc);
       e.execute(BC,clist);
       e.finish(bc);
       
      
       Eventupdate e1= new Eventupdate();
       ID batchprocessid1 = Database.executeBatch(e1);
       QL1 = e1.start(bc1);
       e1.execute(BC1,clist1);
       e1.finish(bc1);
       
      /* Eventupdate e2= new Eventupdate();
       ID batchprocessid2 = Database.executeBatch(e2);
       QL = e2.start(bc);
       e2.execute(BC,clist2);
       e2.finish(bc);
      
       Eventupdate e3= new Eventupdate();
       ID batchprocessid3 = Database.executeBatch(e3);
       QL = e3.start(bc);
       e3.execute(BC,clist3);
       e3.finish(bc);
     
        Eventupdate e4= new Eventupdate();
       ID batchprocessid4 = Database.executeBatch(e4);
       QL = e4.start(bc);
       e4.execute(BC,clist4);
       e4.finish(bc);
       
        Eventupdate e5= new Eventupdate();
       ID batchprocessid5 = Database.executeBatch(e5);
       QL = e5.start(bc);
       e5.execute(BC,clist5);
       e5.finish(bc);*/
       
       
    }
 
 }