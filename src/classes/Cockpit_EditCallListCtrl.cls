public with sharing class Cockpit_EditCallListCtrl extends Cockpit_AdminHelperClass{
    Automated_Call_List__c editAutomatedcallList;
    public boolean admin{get;set;}
    public String editFieldname1{get;set;}
    public String editFieldname2{get;set;}
    public String editFieldname3{get;set;}
    public String editFieldname4{get;set;}
    public String editFieldname5{get;set;}
    public String editFieldname6{get;set;}
    public String editFieldname7{get;set;}
    public String editFieldname8{get;set;}
    public String editFieldname9{get;set;}
    public String editFieldname10{get;set;}
    public String editOperator1{get;set;}
    public String editOperator2{get;set;}
    public String editOperator3{get;set;}
    public String editOperator4{get;set;}
    public String editOperator5{get;set;}
    public String editOperator6{get;set;}
    public String editOperator7{get;set;}
    public String editOperator8{get;set;}
    public String editOperator9{get;set;}
    public String editOperator10{get;set;}
    public String editvalue1{get;set;}
    public String editvalue2{get;set;}
    public String editvalue3{get;set;}
    public String editvalue4{get;set;}
    public String editvalue5{get;set;}
    public String editvalue6{get;set;}
    public String editvalue7{get;set;}
    public String editvalue8{get;set;}
    public String editvalue9{get;set;}
    public String editvalue10{get;set;}
    public String editVisibility{get;set;}
    public String editFilterLogic{get;set;}
    public String editContactOption{get;set;}
    public String editCampaignStatus{get;set;}
    public String editSelectedCallList{get;set;}
    public boolean editAdmin{get;set;}
    public Automated_Call_List__c filter{get;set;}
    public Automated_Call_List__c updateAutomatedCallList{get;set;}
    public Map<String,String> mapfieldnames{get; set;}
    public Boolean showAdvanceFormula{get; set;}
    public String advanceFilterLogic{get; set;}
    Cockpit_AdminHelperClass AdminHelperClass=  new Cockpit_AdminHelperClass();
    
    //Constructor of the class  --Start--
    public Cockpit_EditCallListCtrl() {
        showAdvanceFormula=false;
        advanceFilterLogic='';
        editFieldname1='--None--';
        editFieldname2='--None--';
        editFieldname3='--None--';
        editFieldname4='--None--';
        editFieldname5='--None--';
        editFieldname6='--None--';
        editFieldname7='--None--';
        editFieldname8='--None--';
        editFieldname9='--None--';
        editFieldname10='--None--';
        mapfieldnames = new Map<String,String>();
        mapfieldnames.put('','');
        mapfieldnames.put('--None--','--None--');
        editAdmin=false;
        system.debug('editAdmin'+editAdmin);
        user usr = [Select id,name,profile.Name from user where id=:UserInfo.getUserId() limit 1];
        if(usr.profile.Name == 'Cole Sales Manager User' || usr.profile.Name == 'MI&S' || usr.profile.Name == 'System Administrator IT')
        editAdmin=true; 
        system.debug('editAdmin'+editAdmin);      
      
    } // --End of Constructor --
    // Get the Call List
    public Automated_Call_List__c getEditAutomatedCallList(){        
        if(editAutomatedcallList == null)
           editAutomatedcallList = new Automated_Call_List__c ();
        return editAutomatedcallList;
    }
    // Getting Operators in the operator drop down box in VF page --- Start ---
    public List<SelectOption> geteditOperatorList1() {
        List<SelectOption> OperatorList1 = new List<SelectOption>();
        OperatorList1=createOperatorList(editFieldname1); 
        return OperatorList1;
    }
    public List<SelectOption> geteditOperatorList2() {
        List<SelectOption> OperatorList2 = new List<SelectOption>();
        OperatorList2=createOperatorList(editFieldname2);
        return OperatorList2;
    }
    public List<SelectOption> geteditOperatorList3() {
        List<SelectOption> OperatorList3 = new List<SelectOption>();
        OperatorList3=createOperatorList(editFieldname3);
        return OperatorList3;
    }
    public List<SelectOption> geteditOperatorList4() {
        List<SelectOption> OperatorList4 = new List<SelectOption>();
        OperatorList4=createOperatorList(editFieldname4);
        return OperatorList4;
    }
    public List<SelectOption> geteditOperatorList5() {
        List<SelectOption> OperatorList5 = new List<SelectOption>();
        OperatorList5=createOperatorList(editFieldname5); 
        return OperatorList5;
    }
    public List<SelectOption> geteditOperatorList6() {
        List<SelectOption> OperatorList6 = new List<SelectOption>();
        OperatorList6=createOperatorList(editFieldname6); 
        return OperatorList6;
    }
    public List<SelectOption> geteditOperatorList7() {
        List<SelectOption> OperatorList7 = new List<SelectOption>();
        OperatorList7=createOperatorList(editFieldname7);
        return OperatorList7;
    }
    public List<SelectOption> geteditOperatorList8() {
        List<SelectOption> OperatorList8 = new List<SelectOption>();
        OperatorList8=createOperatorList(editFieldname8);
        return OperatorList8;
    }
    public List<SelectOption> geteditOperatorList9() {
        List<SelectOption> OperatorList9 = new List<SelectOption>();
        OperatorList9=createOperatorList(editFieldname9);
        return OperatorList9;
    }
    public List<SelectOption> geteditOperatorList10() {
        List<SelectOption> OperatorList10 = new List<SelectOption>();
        OperatorList10=createOperatorList(editFieldname10); 
        return OperatorList10;
    } 
 
    public void editFilterCriteria(){
        String dynamicQuery = '';
        mapfieldnames = getmapfieldnames();
        System.debug('****editSelectedCallList' +editSelectedCallList);
        filter = [Select id,Activity_In_Last_X_Days__c,TempLock__c,Dynamic_Query_Filters__c,Open_Task_Due_In_X_Days__c,Custom_Logic__c,Dynamic_Query__c,Campaign__c,Description__c,Priority__c,Visiblity__c, isAdvanceFormula__c from Automated_Call_List__c where name=:editSelectedCallList]; 
        if(filter.TempLock__c != null) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Some operation is being on this call list So please wait for sometime'));
                //return null;
        }
        else{
           editFilterLogic = filter.Custom_Logic__c;
           showAdvanceFormula= filter.isAdvanceFormula__c;
           advanceFilterLogic= filter.Custom_Logic__c;
           editVisibility  = filter.Visiblity__c;
           editFieldname1='--None--';
           editFieldname2='--None--';
           editFieldname3='--None--';
           editFieldname4='--None--';
           editFieldname5='--None--';
           editFieldname6='--None--';
           editFieldname7='--None--';
           editFieldname8='--None--';
           editFieldname9='--None--';
           editFieldname10='--None--';
           editOperator1='--None--';
           editOperator2='--None--';
           editOperator3='--None--';
           editOperator4='--None--';
           editOperator4='--None--';
           editOperator6='--None--';
           editOperator7='--None--';
           editOperator8='--None--';
           editOperator9='--None--';
           editOperator10='--None--';
           editValue1=' ';
           editValue2=' ';
           editValue3=' ';
           editValue4=' ';
           editValue5=' ';
           editValue6=' ';
           editValue7=' ';
           editValue8=' ';
           editValue9=' ';
           editValue10=' ';
           dynamicQuery = filter.Dynamic_Query_Filters__c;
           if(dynamicQuery != '' && dynamicQuery != null){
              dynamicQuery = dynamicQuery.replaceAll('null','');
              system.debug('**********dynamicQuery**********'+dynamicQuery);
              list<string> splitdynamicQuery = dynamicQuery.split('&&&&');
              system.debug('*****splitdynamicQuery*****'+splitdynamicQuery);
              system.debug('*****Selected Campaign Status*****'+splitdynamicQuery[0]);
              if(splitdynamicQuery.size() > 1&& splitdynamicQuery[1] != '' && splitdynamicQuery[1] != null){
                 list<string> fieldList = splitdynamicQuery[1].split('-->');
                 if(!fieldList.isEmpty()){
                    for(integer i=0;i<fieldList.size();i++){
                        if(i==0){
                           list<string> fieldNameList1 = fieldList[i].split('&&!!');
                           system.debug('fieldNameList1'+fieldNameList1);
                           editFieldName1 = (fieldNameList1[0]=='')?'--None--' : fieldNameList1[0];
                           editOperator1= fieldNameList1[1];
                           if(fieldNameList1.size()>2){
                              editValue1 = fieldNameList1[2];
                           }
                           else{
                              editValue1 = '';
                           }
                
                        }
                        else if(i==1){
                           list<string> fieldNameList2 = fieldList[i].split('&&!!');
                           system.debug('fieldNameList2'+fieldNameList2);
                           editFieldName2 = (fieldNameList2[0]=='')?'--None--' : fieldNameList2[0];
                           editOperator2=fieldNameList2[1];
                           if(fieldNameList2.size()>2){
                              editValue2 = fieldNameList2[2];
                           }   
                           else{
                              editValue2 = '';
                           }
                        }
                        else if(i==2){
                           list<string> fieldNameList3 = fieldList[i].split('&&!!');
                           editFieldName3 = (fieldNameList3[0]=='')?'--None--' : fieldNameList3[0];
                           editOperator3=fieldNameList3[1];
                           if(fieldNameList3.size()>2){
                              editValue3 = fieldNameList3[2];
                           }
                           else{
                              editValue3 = '';
                           }
                        }
                        else if(i==3){
                           list<string> fieldNameList4 = fieldList[i].split('&&!!');
                           editFieldName4 = (fieldNameList4[0]=='')?'--None--' : fieldNameList4[0];
                           editOperator4=fieldNameList4[1];
                           if(fieldNameList4.size()>2){
                              editValue4 = fieldNameList4[2];
                           }
                           else{
                              editValue4 = '';
                           }
                        }
                        else if(i==4){
                           list<string> fieldNameList5 = fieldList[i].split('&&!!');
                           editFieldName5 = (fieldNameList5[0]=='')?'--None--' : fieldNameList5[0];
                           editOperator5=fieldNameList5[1];
                           if(fieldNameList5.size()>2){
                              editValue5 = fieldNameList5[2];
                           }
                           else{
                              editValue5 = '';
                           }
                        }
                        else if(i==5){
                           list<string> fieldNameList6 = fieldList[i].split('&&!!');
                           editFieldName6 = (fieldNameList6[0]=='')?'--None--' : fieldNameList6[0];
                           editOperator6=fieldNameList6[1];
                           if(fieldNameList6.size()>2){
                              editValue6 = fieldNameList6[2];
                           }
                           else{
                              editValue6 = '';
                           }
                        }
                        else if(i==6){
                           list<string> fieldNameList7 = fieldList[i].split('&&!!');
                           editFieldName7 = (fieldNameList7[0]=='')?'--None--' : fieldNameList7[0];
                           editOperator7=fieldNameList7[1];
                           if(fieldNameList7.size()>2){
                              editValue7 = fieldNameList7[2];
                           }
                           else{
                              editValue7 = '';
                           }
                        }
                        else if(i==7){
                           list<string> fieldNameList8 = fieldList[i].split('&&!!');
                           editFieldName8 = (fieldNameList8[0]=='')?'--None--' : fieldNameList8[0];
                           editOperator8=fieldNameList8[1];
                           if(fieldNameList8.size()>2){
                              editValue8 = fieldNameList8[2];
                           }
                           else{
                              editValue8 = '';
                           }
                        }
                        else if(i==8){
                           list<string> fieldNameList9 = fieldList[i].split('&&!!');
                           editFieldName9 = (fieldNameList9[0]=='')?'--None--' : fieldNameList9[0];
                           editOperator9=fieldNameList9[1];
                           if(fieldNameList9.size()>2){
                              editValue9 = fieldNameList9[2];
                           }
                           else{
                              editValue9 = '';
                           }
                        }
                        else if(i==9){
                           list<string> fieldNameList10 = fieldList[i].split('&&!!');
                           editFieldName10 = (fieldNameList10[0]=='')?'--None--' : fieldNameList10[0];
                           editOperator10=fieldNameList10[1];
                           if(fieldNameList10.size()>2){
                              editValue10 = fieldNameList10[2];
                           }
                           else{
                              editValue10 = '';
                           }
                        }
                    }
            
                 }  
              }
           }
        } 
    }
    
    public PageReference updateCallList(){ 
        string returnURL ='';
        system.debug('editSelectedCallList'+editSelectedCallList);
        if(editSelectedCallList==''){
           editSelectedCallList = null;
        }
        string campaignId = '';
        if(filter!=null){
           if(filter.Campaign__c!=null){
              campaignId = filter.Campaign__c;
           }
        }
    
        if(validate('Edit Call List',editSelectedCallList,campaignId,editFieldName1,editFieldName2,editFieldName3,editFieldName4,editFieldName5,editFieldName6,editFieldName7,editFieldName8,editFieldName9,editFieldName10,editOperator1,editOperator2,editOperator3,editOperator4,editOperator5,editOperator6,editOperator7,editOperator8,editOperator9,editOperator10)){ 
           updateAutomatedCallList = [Select id,Activity_In_Last_X_Days__c,TempLock__c,Open_Task_Due_In_X_Days__c,Custom_Logic__c,Dynamic_Query__c,Campaign__c,Description__c,Priority__c,Visiblity__c, isAdvanceFormula__c from Automated_Call_List__c where name=:editSelectedCallList];
           if(updateAutomatedCallList!=null){

              if(showAdvanceFormula && (advanceFilterLogic==null || advanceFilterLogic=='')){
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Formula is Empty.'));
                  return null;     
               }
              updateAutomatedCallList.TempLock__c = 'General Lock';
              updateAutomatedCallList.Visiblity__c = editVisibility;
              updateAutomatedCallList.Activity_In_Last_X_Days__c= filter.Activity_In_Last_X_Days__c;
              updateAutomatedCallList.Open_Task_Due_In_X_Days__c = filter.Open_Task_Due_In_X_Days__c;
              updateAutomatedCallList.Description__c = filter.Description__c;
              updateAutomatedCallList.Custom_Logic__c = showAdvanceFormula?advanceFilterLogic : editFilterLogic;
              updateAutomatedCallList.isAdvanceFormula__c= showAdvanceFormula;
              updateAutomatedCallList.Campaign__c = filter.Campaign__c;
              updateAutomatedCallList.Priority__c = filter.Priority__c;

              Map<String, String> fieldsMap= new Map<String, String>();
              list<string> editFieldsList = new list<string>();
              string editFitlerConditions = editCampaignStatus+'&&&&';

              if(editFieldName1!=null && editFieldName1!='--None--'){
                 editFieldsList.add(String.escapeSingleQuotes(editFieldName1)+'&&!!'+String.escapeSingleQuotes(editOperator1)+'&&!!'+((editValue1!='')?String.escapeSingleQuotes(editValue1):'null'));
                 editFitlerConditions+=editFieldName1+'&&!!'+editOperator1+'&&!!'+((editValue1!='')?editValue1:'null');
                 if(showAdvanceFormula)
                      fieldsMap.put('0', String.escapeSingleQuotes(editFieldName1)+'&&!!'+String.escapeSingleQuotes(editOperator1)+'&&!!'+((editValue1!='')?String.escapeSingleQuotes(editValue1):'null'));
                    
              }
              if(editFieldName2!=null && editFieldName2!='--None--'){
                 editFieldsList.add(String.escapeSingleQuotes(editFieldName2)+'&&!!'+String.escapeSingleQuotes(editOperator2)+'&&!!'+((editValue2!='')?editValue2:'null'));
                 editFitlerConditions+='-->'+editFieldName2+'&&!!'+editOperator2+'&&!!'+((editValue2!='')?String.escapeSingleQuotes(editValue2):'null');
                  if(showAdvanceFormula)
                      fieldsMap.put('1', String.escapeSingleQuotes(editFieldName2)+'&&!!'+String.escapeSingleQuotes(editOperator2)+'&&!!'+((editValue2!='')?editValue2:'null'));
              }
              if(editFieldName3!=null && editFieldName3!='--None--'){
                   editFieldsList.add(String.escapeSingleQuotes(editFieldName3)+'&&!!'+String.escapeSingleQuotes(editOperator3)+'&&!!'+((editValue3!='')?editValue3:'null'));
                   editFitlerConditions+='-->'+editFieldName3+'&&!!'+editOperator3+'&&!!'+((editValue3!='')?String.escapeSingleQuotes(editValue3):'null');
                   if(showAdvanceFormula)
                      fieldsMap.put('2', String.escapeSingleQuotes(editFieldName3)+'&&!!'+String.escapeSingleQuotes(editOperator3)+'&&!!'+((editValue3!='')?editValue3:'null'));
              }
              if(editFieldName4!=null && editFieldName4!='--None--'){
                 editFieldsList.add(String.escapeSingleQuotes(editFieldName4)+'&&!!'+String.escapeSingleQuotes(editOperator4)+'&&!!'+((editValue4!='')?editValue4:'null'));
                 editFitlerConditions+='-->'+editFieldName4+'&&!!'+editOperator4+'&&!!'+((editValue4!='')?String.escapeSingleQuotes(editValue4):'null');
                 if(showAdvanceFormula)
                      fieldsMap.put('3', String.escapeSingleQuotes(editFieldName4)+'&&!!'+String.escapeSingleQuotes(editOperator4)+'&&!!'+((editValue4!='')?editValue4:'null'));
              }
              if(editFieldName5!=null && editFieldName5!='--None--'){
                 editFieldsList.add(String.escapeSingleQuotes(editFieldName5)+'&&!!'+String.escapeSingleQuotes(editOperator5)+'&&!!'+((editValue5!='')?editValue5:'null'));
                 editFitlerConditions+='-->'+editFieldName5+'&&!!'+editOperator5+'&&!!'+((editValue5!='')?String.escapeSingleQuotes(editValue5):'null');
                 if(showAdvanceFormula)
                      fieldsMap.put('4', String.escapeSingleQuotes(editFieldName5)+'&&!!'+String.escapeSingleQuotes(editOperator5)+'&&!!'+((editValue5!='')?editValue5:'null'));
              }
              if(editFieldName6!=null && editFieldName6!='--None--'){
                 editFieldsList.add(String.escapeSingleQuotes(editFieldName6)+'&&!!'+String.escapeSingleQuotes(editOperator6)+'&&!!'+((editValue6!='')?editValue6:'null'));
                 editFitlerConditions+='-->'+editFieldName6+'&&!!'+editOperator6+'&&!!'+((editValue6!='')?String.escapeSingleQuotes(editValue6):'null');
                 if(showAdvanceFormula)
                      fieldsMap.put('5', String.escapeSingleQuotes(editFieldName6)+'&&!!'+String.escapeSingleQuotes(editOperator6)+'&&!!'+((editValue6!='')?editValue6:'null'));
              }
              if(editFieldName7!=null && editFieldName7!='--None--'){
                 editFieldsList.add(String.escapeSingleQuotes(editFieldName7)+'&&!!'+String.escapeSingleQuotes(editOperator7)+'&&!!'+((editValue7!='')?editValue7:'null'));
                 editFitlerConditions+='-->'+editFieldName7+'&&!!'+editOperator7+'&&!!'+((editValue7!='')?String.escapeSingleQuotes(editValue7):'null');
                 if(showAdvanceFormula)
                      fieldsMap.put('6', String.escapeSingleQuotes(editFieldName7)+'&&!!'+String.escapeSingleQuotes(editOperator7)+'&&!!'+((editValue7!='')?editValue7:'null'));
              }
              if(editFieldName8!=null && editFieldName8!='--None--'){
                 editFieldsList.add(String.escapeSingleQuotes(editFieldName8)+'&&!!'+String.escapeSingleQuotes(editOperator8)+'&&!!'+((editValue8!='')?editValue8:'null'));
                 editFitlerConditions+='-->'+editFieldName8+'&&!!'+editOperator8+'&&!!'+((editValue8!='')?String.escapeSingleQuotes(editValue8):'null');
                 if(showAdvanceFormula)
                      fieldsMap.put('7', String.escapeSingleQuotes(editFieldName8)+'&&!!'+String.escapeSingleQuotes(editOperator8)+'&&!!'+((editValue8!='')?editValue8:'null'));
              }
              if(editFieldName9!=null && editFieldName9!='--None--'){
                 editFieldsList.add(String.escapeSingleQuotes(editFieldName9)+'&&!!'+String.escapeSingleQuotes(editOperator9)+'&&!!'+((editValue9!='')?editValue9:'null'));
                 editFitlerConditions+='-->'+editFieldName9+'&&!!'+editOperator9+'&&!!'+((editValue9!='')?String.escapeSingleQuotes(editValue9):'null');
                 if(showAdvanceFormula)
                      fieldsMap.put('8', String.escapeSingleQuotes(editFieldName9)+'&&!!'+String.escapeSingleQuotes(editOperator9)+'&&!!'+((editValue9!='')?editValue9:'null'));
              }
              if(editFieldName10!=null && editFieldName10!='--None--'){
                 editFieldsList.add(String.escapeSingleQuotes(editFieldName10)+'&&!!'+String.escapeSingleQuotes(editOperator10)+'&&!!'+((editValue10!='')?editValue10:'null'));
                 editFitlerConditions+='-->'+editFieldName10+'&&!!'+editOperator10+'&&!!'+((editValue10!='')?String.escapeSingleQuotes(editValue10):'null');
                if(showAdvanceFormula)
                      fieldsMap.put('9', String.escapeSingleQuotes(editFieldName10)+'&&!!'+String.escapeSingleQuotes(editOperator10)+'&&!!'+((editValue10!='')?editValue10:'null'));
              }
              
              if(showAdvanceFormula){
                  ValidateAndOrExpression v= new ValidateAndOrExpression(advanceFilterLogic, fieldsMap.keySet(), true);
                  if(v.hasError){
                      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Check Advance Filter Logic.\n'+v.message));
                      return null;
                  }
                  advanceFilterLogic= ValidateAndOrExpression.convertExpressionKeys(advanceFilterLogic);
               }

              updateAutomatedCallList.Dynamic_Query__c = 'select Id,Name,Internal_Wholesaler1__c,Territory__c,RIA_Territory__c from Contact'
                                  + (!showAdvanceFormula ? AdminHelperClass.generateDynamicQueryFilters('Edit Call List',editFieldsList,editFilterLogic,'All Contacts') 
                                                         : AdminHelperClass.generateAdvanceFormulaFilterCondition('Edit Call List', 'All Contacts', fieldsMap, advanceFilterLogic));
              updateAutomatedCallList.Dynamic_Query_Filters__c = editFitlerConditions;
              if(Schema.sObjectType.Automated_Call_List__c.isUpdateable())
                 update updateAutomatedCallList;
              System.debug('**Entering update callist'+updateAutomatedCallList);
              System.debug('**updateAutomatedCallList.Dynamic_Query_Filters__c'+updateAutomatedCallList.Dynamic_Query_Filters__c);
              System.debug('**filter.Dynamic_Query_Filters__c'+filter.Dynamic_Query_Filters__c);       
              Cockpit_DeleteContactCallLists  batch = new Cockpit_DeleteContactCallLists (editSelectedCallList);
              Id batchId = Database.executeBatch(batch);
           }
           returnURL=AdminHelperClass.updateVisibility(editSelectedCallList,editVisibility);
           system.debug('returnURL'+returnURL);
               
           PageReference pageref = new PageReference(returnURl);
           pageref.setRedirect(true);
           return pageref; 
       }
       else{
           return null;
       }
                  
    }
              

}