global with sharing class RC_LocationSearchClass 
{
   private integer totalRecs = 0;
   public integer OffsetSize{get;set;} 
   public integer LimitSize{get;set;}  
   //public String LocationId{get;set;} 
   //global string flowstring{get;set;}
    public String getValue() {
        return searchString;
    }


    public String getSearch() {
        return null;
    }

   public string searchString{get;set;} 
   public string searchby{get;set;}
   public List<RC_Location__c> locationresults{get;set;}
   

    public String getSearchString()
    {
        return searchString;
    }  
    public void setSearchString(String SearchString) 
    {
        this.SearchString= SearchString;
    }

    public PageReference Search()
    {
       //searchString = System.currentPageReference().getParameters().get('lksrch');
         OffsetSize=0;
         LimitSize=50;
         system.debug('This is my search'+searchString);
        searchby =searchString; 
        locationresults=performSearch(searchby );
            return null;
    }
    

   public RC_LocationSearchClass()
   {
         
         //searchby = System.currentPageReference().getParameters().get('myval');
       // system.debug('This is my search'+searchby );
        //locationresults=performSearch(searchby ); 
        OffsetSize=0;
         LimitSize=50;

   }
   public List<RC_Location__c> performSearch(string searchString) 
    {
       system.debug('This is performserach'+searchby );
       String Ravan='Ravan';
        String soql = 'select id,name,Address__c,Entity_Code__c,City__c,State__c,Known_Name__c, Tenant_Name__c from RC_Location__c ';
        if(searchby != '' && searchby != null)
        {
         soql = soql +  ' where Address__c LIKE \'%' +  String.escapeSingleQuotes(searchby) +'%\' ' + 'OR City__c Like \'%' +  String.escapeSingleQuotes(searchby)+'%\' ' + 'OR State__c Like \'%' +  String.escapeSingleQuotes(searchby)+'%\' ' + 'OR Entity_Code__c Like \'%' +  String.escapeSingleQuotes(searchby)+'%\' ' ;
          soql = soql + ' order by City__c' + ' LIMIT '+ LimitSize +' OFFSET '+ OffsetSize ;
        }
       else 
       {
          soql = soql +  ' where City__c=:'+ Ravan ;
       }
       System.debug('***Query' + soql);
       list<RC_Location__c>locationlist=new list<RC_Location__c>();
        locationlist= database.query(soql); 
        totalRecs =locationlist.size();
        system.debug('<<>>>'+totalRecs );
        return locationlist;
    }
    public void FirstPage()
        {
            OffsetSize = 0;
            locationresults=performSearch(searchby );
        }
        public void previous()
        {
              OffsetSize = OffsetSize-LimitSize;
              locationresults=performSearch(searchby );
        }
        public void next()
        {
               OffsetSize = OffsetSize + LimitSize;
               locationresults=performSearch(searchby );
        }
        public void LastPage()
        {
            OffsetSize = totalrecs-math.mod(totalRecs,LimitSize);
            locationresults=performSearch(searchby );
        }
        public boolean getprev()
        {
           if(OffsetSize == 0)
                return true;
            else
                return false;
        }
        public boolean getnxt()
        {
            if((OffsetSize + LimitSize) > totalRecs)
              return true;
            else
              return false;
        }

 
 
 }