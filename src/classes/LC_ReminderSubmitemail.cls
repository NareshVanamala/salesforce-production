global with sharing class LC_ReminderSubmitemail implements Database.Batchable<SObject>
{

    global String Query; 
    global String leaseOppstr; 
    global LC_LeaseOpprtunity__c call;
    global LC_LeaseOpprtunity__c LockedCL ;
    global integer size ;
    global string size1; 
    global List<LC_LeaseOpprtunity__c > callistname{get;set;}
    global List<LC_LeaseOpprtunity__c > callistname1  ;
   
    global Database.QueryLocator start(Database.BatchableContext BC)  
    {  
      //string lseopp='a4yR0000003Ulj7IAC';
      //get all the lease opportunity records where either of the approvers fields are not null
      leaseOppstr='Approval Received';
      Query='Select Current_Approver_1__c,Current_Approver_2__c,Current_Approver_3__c,Current_Approver_4__c,Current_Approver_5__c,Current_Approver_6__c,CurrentApprover__c,Tenant_Address__c,id,name,National_Tenant__c,Actual_NER__c,Add_l_Options__c,Additional_Comments__c,Alternate_Property_Name__c,Anticipated_Delivery_Date__c,Average_base_rent_over_renewal_period__c,Budgeted_NER__c,Building_Id__c,Common_Name__c,Contiguous_Tenant_For_Possible_Expansion__c,Current_Date__c,Current_Expiration_Date__c,Tenant_Email_Contact_Number__c,Lease_Category__c, Entity_Id__c,Financials_if_requested__c, OwnerEnitty_Fund__c,Guarantor__c,Lease_Opportunity_Type__c,Market_Rents__c,Milestone__c,MRI_Property__c,MRIProperty_Address__c,Summary_of_Terms_Rich__c,Property_Name__c,Property_Type__c,Recommendations_Narrative_Rich__c,Renewal_Effective_date__c,Option_Rents_Contractual__c,TIC_and_Lender_Approval__c,Supplemental_Attachments__c,Annual_Base_Rent_PSF__c,Annual_Base_Rent_at_expiration__c,Renewalterm__c,Sales_Reports_if_requested__c,Suite_Sqft__c,Status__c,Tenant_Contact_Name__c,Tenant_DBA__c,Percent_Rent_Breakpoint__c, Base_Rent__c,Tenant_Email__c,Tenant_Legal_Name__c,Tenant_payment_performance_history__c,Initialterm__c,Waivers__c,Remaining_Options_Intact__c,RemainingOptions_Description__c,LC_OtherOpportunity_Description__c,Current_Approver__c from LC_LeaseOpprtunity__c where((Current_Approver_1__c!=null)or(Current_Approver_2__c!=null)or(Current_Approver_3__c!=null)or(Current_Approver_4__c!=null)or(Current_Approver_5__c!=null)or(Current_Approver_6__c!=null))and Milestone__c!=:leaseOppstr';
      System.debug('*******:QUERY:********\n'+Query);
      system.debug('This is start method');
      system.debug('Myquery is......'+Query);
      return Database.getQueryLocator(query); 
    }
   global void execute(Database.BatchableContext BC,List<LC_LeaseOpprtunity__c> scope)  
   {  
        system.debug('Check the name of the call list name:'+scope);
        List<LC_LeaseOpprtunity__c > callist = new List<LC_LeaseOpprtunity__c >();
        
        callistname=new list<LC_LeaseOpprtunity__c>();
        //callistname.addAll(scope); 
        set<id> leaseOppIdSet = new set<id>();
        list<User>userlist=new list<User>();
        list<String>Namelist=new list<String>();
        set<String>NameSet=new set<String>();
        set<id>LeaseOpportunityid=new set<id>();
        map<String, String>NametoIdMap=new map<String, String>();
        If(scope.size()>0)
        {
            for(LC_LeaseOpprtunity__c c : scope)
            {
               callistname.add(c);
               LeaseOpportunityid.add(c.id);
               if(c.Current_Approver_1__c!=null)
               {
                 NameSet.add(c.Current_Approver_1__c);
               }
               if(c.Current_Approver_2__c!=null)
               {
                 NameSet.add(c.Current_Approver_2__c);
               } 
               if(c.Current_Approver_3__c!=null)
               {
                 NameSet.add(c.Current_Approver_3__c);
               }
               if(c.Current_Approver_4__c!=null)
               {
                 NameSet.add(c.Current_Approver_4__c);
               }
                if(c.Current_Approver_5__c!=null)
               {
                 NameSet.add(c.Current_Approver_5__c);
               }
               if(c.Current_Approver_6__c!=null)
               {
                 NameSet.add(c.Current_Approver_6__c);
               }
             }  
             system.debug('Thissis my map'+NameSet);          
        }
        
        list<ProcessInstance>plist=[SELECT CreatedById,CreatedDate,Id,ProcessDefinitionId,Status,SystemModstamp,TargetObjectId FROM ProcessInstance WHERE TargetObjectId in:LeaseOpportunityid ORDER BY CreatedDate DESC limit 1];
         system.debug('check the list'+plist);
         set<id>pidset=new set<id>();
         for(ProcessInstance  pct:plist)
              pidset.add(pct.id);
         list<ProcessInstanceStep>plist1=[SELECT Id,OriginalActorId,ProcessInstanceId,StepNodeId,StepStatus,SystemModstamp FROM ProcessInstanceStep where ProcessInstanceId in :pidset and ((StepStatus='Approved')or(StepStatus='Rejected')) ];
          system.debug('Check the list'+plist1.size());
          system.debug('Check the list'+plist1);
          set<id>useridset=new set<id>();
          for(ProcessInstanceStep stp:plist1)
            useridset.add(stp.OriginalActorId);
          system.debug('Check the useridset'+useridset);  
        userlist=[Select id, Name, email from User where Name in:NameSet and id not in:useridset];
          system.debug('Get me the user list'+userlist);
        if(Userlist.size()>0)
        {
          for(User Ut:userlist)
           NametoIdMap.put(Ut.name, Ut.id);
          
        }
        system.debug('Get me the Map'+NametoIdMap);
        If(scope.size()>0)
        {
             for(LC_LeaseOpprtunity__c c:scope)
             {
                
                if((c.Current_Approver_1__c!=null))
                {
                   if((NametoIdMap.get(c.Current_Approver_1__c)!=null))
                    {
                         String myid=NametoIdMap.get(c.Current_Approver_1__c); 
                         c.Current_Approver_User1__c=myid;
                    }
                   else if((NametoIdMap.get(c.Current_Approver_1__c)==null))
                    {
                         c.Current_Approver_User1__c=null;
                    }
                 }
                 if((c.Current_Approver_2__c!=null))
                 {
                   if((NametoIdMap.get(c.Current_Approver_2__c)!=null))
                   {
                     String myid=NametoIdMap.get(c.Current_Approver_2__c); 
                     c.Current_Approver_User2__c=myid;
                   } 
                   else if((NametoIdMap.get(c.Current_Approver_2__c)==null))
                    {
                         c.Current_Approver_User2__c=null;
                    } 
                 }
                 if((c.Current_Approver_3__c!=null))
                {
                   If(NametoIdMap.get(c.Current_Approver_3__c)!=null)
                  {
                     String myid=NametoIdMap.get(c.Current_Approver_3__c); 
                     c.Current_Approver_User3__c=myid;
                   }  
                   
                   If(NametoIdMap.get(c.Current_Approver_3__c)==null)
                  {
                      c.Current_Approver_User3__c=null;
                   }  
                }
                if((c.Current_Approver_4__c!=null))
                {
                  if(NametoIdMap.get(c.Current_Approver_4__c)!=null)
                   {
                     String myid=NametoIdMap.get(c.Current_Approver_4__c); 
                     c.Current_Approver_User4__c=myid;
                    } 
                    if(NametoIdMap.get(c.Current_Approver_4__c)==null)
                    {
                         c.Current_Approver_User4__c=null;
                    } 
                }
                if((c.Current_Approver_5__c!=null))
                 {
                 
                  if(NametoIdMap.get(c.Current_Approver_5__c)!=null)
                   {
                     String myid=NametoIdMap.get(c.Current_Approver_5__c); 
                     c.Current_Approver_User5__c=myid;
                   }  
                   if(NametoIdMap.get(c.Current_Approver_5__c)==null)
                   {
                       c.Current_Approver_User5__c=null;
                   }  
                     
                }
                 if((c.Current_Approver_6__c!=null))
                  {
                    if(NametoIdMap.get(c.Current_Approver_6__c)!=null)
                    {
                     String myid=NametoIdMap.get(c.Current_Approver_6__c); 
                     c.Current_Approver_User6__c=myid;
                     }
                    if(NametoIdMap.get(c.Current_Approver_6__c)==null)
                    {
                       c.Current_Approver_User6__c=null;
                     } 
                }
           callist.add(c);
           system.debug('Thisis my list'+callist) ;     
        }  
        
        update callist;       
  }
  
  }
        
      
        
       
  

  global void finish(Database.BatchableContext BC)  
  { 
      /*system.debug('check the leaseOpportunity list'+callistname);
      list<LC_LeaseOpprtunity__c>updatedlist=new list<LC_LeaseOpprtunity__c>();
      for(LC_LeaseOpprtunity__c  c:callistname)
      {
         c.Current_Approver_User1__c=null;
         c.Current_Approver_User2__c=null;
         c.Current_Approver_User3__c=null;
         c.Current_Approver_User4__c=null;
         c.Current_Approver_User5__c=null;
         c.Current_Approver_User6__c=null;
         updatedlist.add(c);
      }
      
       update updatedlist;*/
  }


}