global class Ops_Wholesaler_Reporting implements Messaging.InboundEmailHandler 
{

  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.Inboundenvelope envelope) 
  {
  
  Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
  set<String> emailset = new set<String>();
  Email_services__c  Emails = new Email_services__c() ;

try
{
      Emails.Name = email.subject;
      Emails.Email_Recieved__c = System.Now();
      Emails.To__c = string.valueof(email.toAddresses);
      Emails.from__c = email.fromAddress;
      Emails.CC__c = String.valueof(email.ccAddresses);
      Emails.Email_Services_category__c= 'Internal/External Sales';
      
      if(email.ccAddresses!= Null)
      {
      for(String s : email.ccAddresses)
      {
      emailset.add(s);
      }
      }
      
      if(!email.toAddresses.isEmpty())
      {
      for(String s : email.toAddresses)
      {
      emailset.add(s);
      }
      }
      
      insert Emails;
      
      if(!emailset.isEmpty())
      {
      List<Contact> conlst = new List<Contact>();
      List<ES_Contact_Junction__c> junlst = new List<ES_Contact_Junction__c>();      
      conlst = [Select ID,Email from Contact where Email in: emailset];
      
      if(!conlst.isEmpty())
      {
      for(Contact Con : conlst)
      {
      ES_Contact_Junction__c Jn = new ES_Contact_Junction__c(Contact__c = Con.id,Email_services__c = Emails.id);      
      junlst.add(Jn);
      }
      }
      if(!junlst.isEmpty())
      {
      insert junlst;
      }
      }

   List<Attachment> attLst = new List<Attachment>();
  if(email.textAttachments != null)
  {  
  for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments)  
  {

  Attachment attachment = new Attachment();
  
  attachment.Name = tAttachment.fileName;

  attachment.Body = Blob.valueOf(tAttachment.body);

  attachment.ParentId = Emails.Id;

  attLst.add(attachment);
 }
 }
if(email.binaryAttachments != null)
{
for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) 
{

  Attachment attachment = new Attachment();

  attachment.Name = bAttachment.fileName;

  attachment.Body = bAttachment.body;

  attachment.ParentId = Emails.Id;

  attLst.add(attachment);

}
}

if(!attLst.isEmpty())
{
insert attLst;
}
}
catch (System.DmlException e) 
        {
            for (Integer i = 0; i < e.getNumDml(); i++) 
            {
        // Process exception here 
    
        System.debug(e.getDmlMessage(i)); 
              }
        }   
          
        result.success = true;
           
return(null);
   
}
}