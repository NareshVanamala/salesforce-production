global with Sharing class EmployeeAseet implements Database.Batchable<SObject>
{
    global list<Employee__c> Employeelist = new list<Employee__c>();
    global list<Asset__c>assetlist= new list<Asset__c>();
    global list<Asset_Inventory__c>AllAssetInventorylist= new list<Asset_Inventory__c>();
   
     
    global Database.QueryLocator Start(Database.BatchableContext BC)
    {
        String Query;
        Query = 'Select id from Employee__c'; 
        System.debug('My Query Is.......'+Query);
        return Database.getQueryLocator(Query);
    }
    global void Execute(Database.BatchableContext BC,List<Employee__c> Scope)
    { 
    
        AllAssetInventorylist=[select id,name,Processed__c,Application_Implementor__c,Application_Owner__c,Additional_Descirption__c,Additional_Descirption_Required__c,Asset_Category__c,Asset_Type__c,Licence_Count__c from Asset_Inventory__c where Asset_Type__c!='Active Directory'];
        for(Employee__c c:scope)
         {
            for(Asset_Inventory__c cte:AllAssetInventorylist)
            {
                
                if((cte.Name=='General')&&(cte.Asset_Type__c=='Building'))
                  {
                      
                      Asset__c c1= new Asset__c();
                      c1.Access_Name__c=cte.name;
                      c1.Employee__c=c.id;
                      c1.Asset_Status__c='Added';
                      c1.Application_Owner__c=cte.Application_Owner__c;
                      c1.Application_Implementor__c=cte.Application_Implementor__c;
                      c1.Access_Type__c=cte.Asset_Type__c;
                      c1.Asset_Inventory__c=cte.id;
                      c1.Updating__c=true;
                      c1.Added_On__c=system.today(); 
                      assetlist.add(c1); 
                                    
                  }
                  else
                  {
                      Asset__c c1= new Asset__c();
                      c1.Access_Name__c=cte.name;
                      c1.Employee__c=c.id;
                      c1.Asset_Status__c='InActive';
                      c1.Application_Owner__c=cte.Application_Owner__c;
                      c1.Application_Implementor__c=cte.Application_Implementor__c;
                      c1.Access_Type__c=cte.Asset_Type__c;
                      c1.Asset_Inventory__c=cte.id;
                      c1.Updating__c=false;
                      assetlist.add(c1);        
                  }
              }
          
          }
          
          if (Schema.sObjectType.Asset__c.isCreateable())

          insert assetlist;
             
            
}          


 global void finish(Database.BatchableContext BC)
       {
       
       }
       


}