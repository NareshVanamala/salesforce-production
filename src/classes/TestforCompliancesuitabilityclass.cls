@isTest
private class TestforCompliancesuitabilityclass 
 {
      public static testMethod void  TestforCompliancesuitabilityclass() 
      {
          Account a= new Account();
          a.name='Testaccount1';
          //RecordType rtypes = [Select Name, Id From RecordType where isActive=true and sobjecttype='Account'and name='National_Accounts'];
          //a.recordtypeid=rtypes.id;
          insert a;  
          
          Compliance_Suitability__c ct= new Compliance_Suitability__c();
          ct.Account__c=a.id;
          ct.name='Testingthe Program';
          ct.Description__c='Testingthe Program12';
          insert ct;
          
          Compliance_Suitability__c ct2= new Compliance_Suitability__c();
          ct2.Account__c=a.id;
          ct2.name='Testingthe Programing';
          ct2.Description__c='Testingthe Programing12';
          insert ct2;
          
          String nameFile = 'hello';
                   
           Attachment attachment= new Attachment();
           attachment.OwnerId = UserInfo.getUserId();
           attachment.ParentId = ct.Id;// the record the file is attached to
           attachment.Name = nameFile;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
           attachment.body=bodyBlob;
           insert attachment;
           
              
         ApexPages.currentPage().getParameters().put('id',a.id);
         ApexPages.StandardController controller = new ApexPages.StandardController(a); 
         Compliancesuitabilityclass x = new  Compliancesuitabilityclass(controller);
          
         x.nameFile='Vasu_Snehal';
         Blob blb=Blob.valueOf('Vasu_SnehalVasu_SnehalVasu_Snehal');         
         x.contentFile=blb;
         x.addrow.Name='B_D_Systems Name';
         List<Attachment> attachments=[select id, name from Attachment where parent.id=:ct.id];
         System.assertEquals(1, attachments.size());
           
         x.newProgram();
         x.savenewProg();
         x.editrecord();
         //x.viewRecord();
         x.SaveRecord();
         x.removecon();
         x.cancelnewProg();          

       }
         
          
           
 }