@isTest
public class Test_Ocms_Business_Details
{
    public static testmethod void Test_Ocms_Business_Details() 
    { 
        profile pf=[select id,name from profile where name='Cole Capital Community'];
        account a=new account();
        a.name='test';
        insert a;
        
        Contact c1= new Contact();
        c1.firstName='Ninad';
        c1.lastName='Tambe';
        c1.accountid=a.id;
        c1.email='test123@noemail.com';
        insert c1;
        
        User u = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = pf.id, country='United States',IsActive =true,
                ContactId = c1.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert u;
         
        system.runas(u)
        {
         REIT_Investment__c rc= new REIT_Investment__c();
         rc.Current_Capital__c=346677;
         rc.Investor_Contact__c=c1.id;
         rc.Rep_Contact__c=c1.id;
         rc.Fund__c='3770';
         insert rc;
         
          REIT_Investment__c rcc= new REIT_Investment__c();
         rcc.Current_Capital__c=346677;
         rcc.Investor_Contact__c=c1.id;
         rcc.Rep_Contact__c=c1.id;
         rcc.Fund__c='3776';
         insert rcc;
         
         REIT_Investment__c rc2= new REIT_Investment__c();
         rc2.Current_Capital__c=346677;
         rc2.Investor_Contact__c=c1.id;
         rc2.Rep_Contact__c=c1.id;
         rc2.Fund__c='3777';
         insert rc2;
         
         REIT_Investment__c rc12= new REIT_Investment__c();
         rc12.Current_Capital__c=346677;
         rc12.Investor_Contact__c=c1.id;
         rc12.Rep_Contact__c=c1.id;
         rc12.Fund__c='3775';
         insert rc12;
         
         update c1;
         
     
        ocms_BusinessSumamry rc14=new ocms_BusinessSumamry();
        try
        {
            rc14.getProductList(); 
            rc14.writeControls();
            rc14.writeMapView();
            
            rc14.getHtml();
            rc14.writeListView();
        }
          catch(Exception e)  {        }    
         
        }
         }
         
   }