@isTest
public with sharing class ocms_PortalContactId_test {
    public ocms_PortalContactId_test() {
        
    }

    /**
    * @description this is the main function for testing portal message.
    **/
    static testMethod void testPortalContactId() {

     
        Account a = new Account(Name='Test Account Name');
        insert a;

        Contact c = new Contact(LastName = 'Contact Last Name', AccountId = a.id);
        c.Email = 'testguy3@testyourcode.com';
        insert c;
       
        Profile pro = [select Id from Profile where Name = 'Cole Capital Community'];
    
        User user = new User();
        user.FirstName = 'Test3';
        user.LastName = 'Guy3';
        user.Phone = '0123456789';
        user.Email = 'testguy3@testyourcode.com';
        user.CommunityNickname = 'testguy3@testyourcode.com';
        user.Alias = 'HelloMan';
        user.UserName = 'testguy3@testyourcode.com';
        user.ProfileId =  pro.Id;
       // user.ContactId = c.Id;
        user.TimeZoneSidKey = 'GMT';
        user.LocaleSidKey = 'en_US';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.contactid=c.id;
        user.LanguageLocaleKey = 'en_US';

        insert user;
        
        System.assertEquals(user.ProfileId,pro.Id);


        ocms_PortalContactId pci = new ocms_PortalContactId();
        pci.gettype();
        pci.userId = user.Id;

        Map<String, String> serviceParams = new Map<String, String>();

        serviceParams = new Map<String, String>();
        serviceParams.put('action', 'createContactCookie');
        pci.executeRequest(serviceParams);  

        Cookie contactCookie = new Cookie('ContactId',c.Id,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{contactCookie});
        pci.executeRequest(serviceParams);

    }
     static testMethod void testPortalContactId1() {

     
        Account a = new Account(Name='Test Account Name');
        insert a;

        Contact c = new Contact(LastName = 'Contact Last Name123', AccountId = a.id);
        c.Email = 'testguy3@testyourcode.com';
        insert c;

        Profile pro = [select Id from Profile where Name = 'Cole Capital Community'];
    
        User user = new User();
        user.FirstName = 'Test3';
        user.LastName = 'Guy3';
        user.Phone = '0123456789';
        user.Email = 'testguy3@testyourcode.com';
        user.CommunityNickname = 'testguy3@testyourcode.com';
        user.Alias = 'HelloMan';
        user.UserName = 'testguy3@testyourcode.com';
        user.ProfileId =  pro.Id;
       // user.ContactId = c.Id;
        user.TimeZoneSidKey = 'GMT';
        user.LocaleSidKey = 'en_US';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.contactid=c.id;
        user.LanguageLocaleKey = 'en_US';

        insert user;
        System.assertEquals(user.ProfileId,pro.Id);


        ocms_PortalContactId pci = new ocms_PortalContactId();
        pci.gettype();
        pci.userId = user.Id;

        Map<String, String> serviceParams = new Map<String, String>();

        serviceParams = new Map<String, String>();
        serviceParams.put('action', 'createContactCookie');
        pci.executeRequest(serviceParams);  

        Cookie contactCookie = new Cookie('ContactId',null,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{contactCookie});
        pci.executeRequest(serviceParams);

    }
}