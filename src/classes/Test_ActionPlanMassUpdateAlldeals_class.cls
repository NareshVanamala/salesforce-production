@isTest(SeeAlldata = true)
public class Test_ActionPlanMassUpdateAlldeals_class {


static testMethod void myUnitTest23() {
    
Portfolio__c pc= new Portfolio__c();
pc.name='Testing1';
Insert pc; 
String Query;
    
      //  PageReference pageRef = Page.MassUpdateActionplan1;
       // system.test.setCurrentPageReference(pageRef );  
    
        Deal__c d = new Deal__c();
        d.Name ='Test12';  
        d.Portfolio_Deal__c=pc.id;  
        d.Build_to_Suit_Type__c ='Current';
        d.Estimated_COE_Date__c = date.today();
        d.HVAC_Warranty_Status__c ='Received';
        d.Ownership_Interest__c = 'Fee Simple';
        d.Roof_Warranty_Status__c = 'Received';
        d.Zip_Code__c = '98033';
        d.Create_Workspace__c =true; 
        insert d ;
    
        Deal__c d1 = new Deal__c();
        d1.Name ='Test13';
        d1.Build_to_Suit_Type__c ='Current';
        d1.Estimated_COE_Date__c = date.today();
        d1.HVAC_Warranty_Status__c ='Received';
        d1.Ownership_Interest__c = 'Fee Simple';
        d1.Roof_Warranty_Status__c = 'Received';
        d1.Zip_Code__c = '98033';
        d1.Create_Workspace__c =true;
        d1.Portfolio_Deal__c=pc.id;      
        insert d1 ;
        
        System.assertNotEquals(d.Name,d1.Name );

        
        Template__c temp = new Template__c();
        temp.Name = 'Template';
        temp.deal__c=d.id;
        insert temp ;
        
        Template__c temp2 = new Template__c();
        temp2.Name = 'Template';
        temp2.deal__c=d.id;
        temp2.Template__c=temp.id;
        insert temp2;
        
        Template__c temp3 = new Template__c();
        temp3.Name = 'Template';
        temp3.deal__c=d1.id;
        temp3.Template__c=temp.id;
        insert temp3;
         
        temp.Template__c=temp2.id;
        update temp;
        
        Task_Plan__c tp = new Task_Plan__c ();
        tp.Name='test';
        tp.priority__c = 'Medium';
        tp.Index__c = 1;
        tp.Assigned_To__c = Userinfo.getUserId();
        tp.Template__c =temp.Id ; 
        tp.Date_Received__c=date.today();
        tp.Field_to_update_Date_Received__c=string.valueof(date.today());
        insert tp ; 
        
        tp = new Task_Plan__c ();
        tp.Name='test';
        tp.priority__c = 'Medium';
        tp.Index__c = 1;
        tp.Assigned_To__c = Userinfo.getUserId();
        tp.Template__c =temp2.Id ; 
        tp.Date_Received__c=date.today();
        tp.Field_to_update_Date_Received__c=string.valueof(date.today());
        insert tp ; 
        
        List<Task_Plan__c> TPL=new List<Task_Plan__c>();
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Dead_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Site_Visit_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Appraisal_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Passed_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Accenture_Due_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_COE_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Go-Hard Date', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Hard_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Rep_Burn_Off__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_SP_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Site_Visit_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Purchase_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Query_Sorted_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_Hard_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Passed_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Contract_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_COE_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Closing_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Hard_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='LOI_Signed_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_SP_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='LOI_Sent_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Purchase_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Lease_Abstract_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_Hard_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Lease_Expiration__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Contract_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Open_Escrow_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Closing_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Dead_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='LOI_Signed_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Tenant_ROFR_Waiver__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='LOI_Sent_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Date_Audit_Completed__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Lease_Abstract_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Date_Identified__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Lease_Expiration__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Investment_Committee_Approval__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Open_Escrow_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Part_1_Start_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Purchase_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Tenant_ROFR_Waiver__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Date_Audit_Completed__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Date_Identified__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Investment_Committee_Approval__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Part_1_Start_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estoppel__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =temp.Id);
        TPL.add(tp);
         
        insert TPL;
        Query='select id, name from Deal__c where id=:d.id';
        ApexPages.StandardController sc = new ApexPages.standardController(d);
        
        String key = d.Id+','+d1.Id;
        ApexPages.currentPage().getParameters().put('ids', key);
        
        ActionPlanMassUpdateForAlldeals_class1 taskPlans = new ActionPlanMassUpdateForAlldeals_class1(sc);
        
        taskPlans.cancel();
        //taskPlans.getDeals();
        taskPlans.getTemplete();
        taskPlans.gettempletName();  
        taskPlans.tempValues=temp2.Name;     
        taskPlans.search();        
        List<String> selectedDealIds = new List<String>();
        selectedDealIds.add(d.id);
        selectedDealIds.add(d1.id);
        taskPlans.temps=temp2;
        
        taskPlans.selectedDealIds = selectedDealIds;
        taskPlans.saveUpdateValues();
        
        MassUpdateActionPlanforAlldeals_class  batch = new MassUpdateActionPlanforAlldeals_class (d.id,temp.id);
Id batchId = Database.executeBatch(batch,1);   
       
    }
}