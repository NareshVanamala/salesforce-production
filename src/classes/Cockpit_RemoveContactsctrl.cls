public with sharing class Cockpit_RemoveContactsctrl extends Cockpit_AdminHelperClass{ 
    public Integer count {get;set;}
    public Integer index = 15;
    public Integer start = 0;  
    public boolean nextBool {get;set;}
    public boolean prevBool {get;set;}
    public list<Contact_Call_List__c> conlist{get;set;}
    public String removeContactCallList{get;set;}
    public String removeFilterLogic{get;set;}
    public String removeFieldName1{get;set;}
    public String removeFieldname2{get;set;}
    public String removeFieldname3{get;set;}
    public String removeFieldname4{get;set;}
    public String removeFieldname5{get;set;}
    public String removeFieldName6{get;set;}
    public String removeFieldname7{get;set;}
    public String removeFieldname8{get;set;}
    public String removeFieldname9{get;set;}
    public String removeFieldname10{get;set;}
    public String removeOperator1{get;set;}
    public String removeOperator2{get;set;}
    public String removeOperator3{get;set;}
    public String removeOperator4{get;set;}
    public String removeOperator5{get;set;}
    public String removeOperator6{get;set;}
    public String removeOperator7{get;set;}
    public String removeOperator8{get;set;}
    public String removeOperator9{get;set;}
    public String removeOperator10{get;set;}
    public String removeValue1{get;set;}
    public String removeValue2{get;set;}
    public String removeValue3{get;set;}
    public String removeValue4{get;set;}
    public String removeValue5{get;set;}
    public String removeValue6{get;set;}
    public String removeValue7{get;set;}
    public String removeValue8{get;set;}
    public String removeValue9{get;set;}
    public String removeValue10{get;set;}
    public Boolean showAdvanceFormula{get; set;}
    public String advanceFilterLogic{get; set;}
    private integer counter=0;  //keeps track of the offset
    private integer list_size=100; //sets the page size or number of rows
    public integer ContactCallListsSize{get;set;}
    Cockpit_AdminHelperClass AdminHelperClass=  new Cockpit_AdminHelperClass();
    Boolean hasError=false;
    public Cockpit_RemoveContactsctrl(){
        hasError=false;
        prevBool = true;
        nextBool = false;
        removeFieldname1 = '--None--';
        removeFieldname2 = '--None--';
        removeFieldname3 = '--None--';
        removeFieldname4 = '--None--';
        removeFieldname5 = '--None--';
        removeFieldname6 = '--None--';
        removeFieldname7 = '--None--';
        removeFieldname8 = '--None--';
        removeFieldname9 = '--None--';
        removeFieldname10 = '--None--';
        ContactCallListsSize =0;
        showAdvanceFormula=false;
        advanceFilterLogic='';
   } 
   public void getContactCallListsSize() {
       string countFilters='';
       string CCLCountQuery='select count() from Contact_Call_List__c ';       
       countFilters = getfilterconditions();
       
       if(hasError)
        return;
       
       if(countFilters != '' && countFilters != null)
       CCLCountQuery = CCLCountQuery + countFilters;
       system.debug('CCLCountQuery'+ CCLCountQuery);
       ContactCallListsSize = Database.countQuery(CCLCountQuery);

   }
   public boolean getDisablePrevious() { 
      //this will disable the previous and beginning buttons
      system.debug('getDisablePrevious counter'+ counter);
      if(counter > 0 && ContactCallListsSize != 0) 
         return false;
      else 
         return true;
   }

   public boolean getDisableNext() { //this will disable the next and end buttons
       if(counter + list_size < ContactCallListsSize && ContactCallListsSize != 0)
          return false;
       else
          return true;
   }
   public integer getTotalPages() {
       integer totalPages = 0;
       system.debug('getTotalPages ContactCallListsSize'+ContactCallListsSize);
       if(math.mod(ContactCallListsSize, list_size) > 0) {
          return ContactCallListsSize/list_size + 1;
       } 
       else{
          totalPages = ContactCallListsSize/list_size;
          if(totalPages == 0)
             totalPages = 1;
          return totalPages;
       }
   }
   public Integer getPageNumber() {
       system.debug('page counter value'+counter+'Page List size'+list_size);
       return counter/list_size + 1;
   }

   public void go(){
       getContactCallLists();
   }
   public void previous() { //user clicked previous button
       counter -= list_size;
       if(test.isrunningtest()){
          counter = 2;
       }
       system.debug('previous Method'+counter);
       getPageNumber();
       getContactCallLists();
   }

   public void next() { //user clicked next button
       counter += list_size;
       system.debug('Next Method'+counter);
       getPageNumber();
       getContactCallLists();
   }
    // Getting Operators in the operator drop down box in VF page --- Start ---
   public list<SelectOption> getRemoveOperatorList1() {
       list<SelectOption> removeOperatorList1 = new List<SelectOption>();
       removeOperatorList1=createOperatorList(removeFieldName1);
       return removeOperatorList1;
   }
     
   public List<SelectOption> getRemoveOperatorList2() {
       List<SelectOption> removeOperatorList2 = new List<SelectOption>();
       removeOperatorList2=createOperatorList(removeFieldName2);
       return removeOperatorList2;  
   }
     
   public List<SelectOption> getRemoveOperatorList3() {
       List<SelectOption> removeOperatorList3 = new List<SelectOption>();
       removeOperatorList3=createOperatorList(removeFieldName3);
       return removeOperatorList3;
   }

   public List<SelectOption> getRemoveOperatorList4() {
       List<SelectOption> removeOperatorList4 = new List<SelectOption>();
       removeOperatorList4=createOperatorList(removeFieldName4);
       return removeOperatorList4;
   }

   public List<SelectOption> getRemoveOperatorList5() {
       List<SelectOption> removeOperatorList5 = new List<SelectOption>();
       removeOperatorList5=createOperatorList(removeFieldName5);
       return removeOperatorList5;
   }  
   public List<SelectOption> getRemoveOperatorList6() {
       List<SelectOption> removeOperatorList6 = new List<SelectOption>();
       removeOperatorList6=createOperatorList(removeFieldName6);
       return removeOperatorList6;
   }
     
   public List<SelectOption> getRemoveOperatorList7() {
       List<SelectOption> removeOperatorList7 = new List<SelectOption>();
       removeOperatorList7=createOperatorList(removeFieldName7);
       return removeOperatorList7;  
   }
     
   public List<SelectOption> getRemoveOperatorList8() {
       List<SelectOption> removeOperatorList8 = new List<SelectOption>();
       removeOperatorList8=createOperatorList(removeFieldName8);
       return removeOperatorList8;
   }

   public List<SelectOption> getRemoveOperatorList9() {
       List<SelectOption> removeOperatorList9 = new List<SelectOption>();
       removeOperatorList9=createOperatorList(removeFieldName9);
       return removeOperatorList9;
   }

   public List<SelectOption> getRemoveOperatorList10() {
       List<SelectOption> removeOperatorList10 = new List<SelectOption>();
       removeOperatorList10=createOperatorList(removeFieldName10);
       return removeOperatorList10;
   }   
    
   public void getContactCallLists(){ 
       system.debug('removeContactCallList'+ removeContactCallList);
       if(showAdvanceFormula && (advanceFilterLogic==null || advanceFilterLogic=='')){
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Formula is Empty.'));
          return;     
       } 
       if(validate('Remove Contacts',removeContactCallList,'',removeFieldName1,removeFieldName2,removeFieldName3,removeFieldName4,removeFieldName5,removeFieldName6,removeFieldName7,removeFieldName8,removeFieldName9,removeFieldName10,removeOperator1,removeOperator2,removeOperator3,removeOperator4,removeOperator5,removeOperator6,removeOperator7,removeOperator8,removeOperator9,removeOperator10)){ 
          getContactCallListsSize();
          system.debug('!!Entering Go method!!'+removeContactCallList);   
          string filters = '';        
          string CCLQuery='select id, name,CallList_Name__c,Owner__c,Contact__c,Call_Complete__c,Automated_Call_List__r.IsDeleted__c,contact__r.phone,contact__r.Territory__c,contact__r.name from Contact_Call_List__c ';       
          filters = getfilterconditions();
          if(hasError)
            return;
          
          if(filters != '' && filters != null)
             CCLQuery = CCLQuery + filters;
          CCLQuery = CCLQuery + ' order by contact__r.name limit :list_size offset :counter';
          system.debug('CCLQuery'+ CCLQuery);
          conlist=Database.query(CCLQuery);
          system.debug('Check the records in the list...'+ conlist.size());
       }
   }
   public string getfilterconditions(){
       String filterConditions ='';
       Map<String, String> fieldsMap= new Map<String, String>();
       list<string> fieldsList = new list<string>();

       if(removeFieldName1!=null && removeFieldName1!='--None--'){
          fieldsList.add(String.escapeSingleQuotes(removeFieldName1)+'&&!!'+String.escapeSingleQuotes(removeOperator1)+'&&!!'+((removeValue1!='')?String.escapeSingleQuotes(removeValue1):null));                
          if(showAdvanceFormula)
              fieldsMap.put('0', String.escapeSingleQuotes(removeFieldName1)+'&&!!'+String.escapeSingleQuotes(removeOperator1)+'&&!!'+((removeValue1!='')?String.escapeSingleQuotes(removeValue1):null));
       }
       if(removeFieldName2!=null && removeFieldName2!='--None--'){
          fieldsList.add(String.escapeSingleQuotes(removeFieldName2)+'&&!!'+String.escapeSingleQuotes(removeOperator2)+'&&!!'+((removeValue2!='')?String.escapeSingleQuotes(removeValue2):null));
          if(showAdvanceFormula)
              fieldsMap.put('1', String.escapeSingleQuotes(removeFieldName2)+'&&!!'+String.escapeSingleQuotes(removeOperator2)+'&&!!'+((removeValue2!='')?String.escapeSingleQuotes(removeValue2):null)); 
       }
       if(removeFieldName3!=null && removeFieldName3!='--None--'){
          fieldsList.add(String.escapeSingleQuotes(removeFieldName3)+'&&!!'+String.escapeSingleQuotes(removeOperator3)+'&&!!'+((removeValue3!='')?String.escapeSingleQuotes(removeValue3):null));
          if(showAdvanceFormula)
              fieldsMap.put('2', String.escapeSingleQuotes(removeFieldName3)+'&&!!'+String.escapeSingleQuotes(removeOperator3)+'&&!!'+((removeValue3!='')?String.escapeSingleQuotes(removeValue3):null)); 
       }
       if(removeFieldName4!=null && removeFieldName4!='--None--'){
          fieldsList.add(String.escapeSingleQuotes(removeFieldName4)+'&&!!'+String.escapeSingleQuotes(removeOperator4)+'&&!!'+((removeValue4!='')?String.escapeSingleQuotes(removeValue4):null));
          if(showAdvanceFormula)
              fieldsMap.put('3', String.escapeSingleQuotes(removeFieldName4)+'&&!!'+String.escapeSingleQuotes(removeOperator4)+'&&!!'+((removeValue4!='')?String.escapeSingleQuotes(removeValue4):null));
       }
       if(removeFieldName5!=null && removeFieldName5!='--None--'){
          fieldsList.add(String.escapeSingleQuotes(removeFieldName5)+'&&!!'+String.escapeSingleQuotes(removeOperator5)+'&&!!'+((removeValue5!='')?String.escapeSingleQuotes(removeValue5):null));
          if(showAdvanceFormula)
              fieldsMap.put('4', String.escapeSingleQuotes(removeFieldName5)+'&&!!'+String.escapeSingleQuotes(removeOperator5)+'&&!!'+((removeValue5!='')?String.escapeSingleQuotes(removeValue5):null)); 
       }
       if(removeFieldName6!=null && removeFieldName6!='--None--'){
          fieldsList.add(String.escapeSingleQuotes(removeFieldName6)+'&&!!'+String.escapeSingleQuotes(removeOperator6)+'&&!!'+((removeValue6!='')?String.escapeSingleQuotes(removeValue6):null));
          if(showAdvanceFormula)
              fieldsMap.put('5', String.escapeSingleQuotes(removeFieldName6)+'&&!!'+String.escapeSingleQuotes(removeOperator6)+'&&!!'+((removeValue6!='')?String.escapeSingleQuotes(removeValue6):null));
       }
       if(removeFieldName7!=null && removeFieldName7!='--None--'){
          fieldsList.add(String.escapeSingleQuotes(removeFieldName7)+'&&!!'+String.escapeSingleQuotes(removeOperator7)+'&&!!'+((removeValue7!='')?String.escapeSingleQuotes(removeValue7):null));
          if(showAdvanceFormula)
              fieldsMap.put('6', String.escapeSingleQuotes(removeFieldName7)+'&&!!'+String.escapeSingleQuotes(removeOperator7)+'&&!!'+((removeValue7!='')?String.escapeSingleQuotes(removeValue7):null));
       }
       if(removeFieldName8!=null && removeFieldName8!='--None--'){
          fieldsList.add(String.escapeSingleQuotes(removeFieldName8)+'&&!!'+String.escapeSingleQuotes(removeOperator8)+'&&!!'+((removeValue8!='')?String.escapeSingleQuotes(removeValue8):null));
          if(showAdvanceFormula)
              fieldsMap.put('7', String.escapeSingleQuotes(removeFieldName8)+'&&!!'+String.escapeSingleQuotes(removeOperator8)+'&&!!'+((removeValue8!='')?String.escapeSingleQuotes(removeValue8):null));
       }
       if(removeFieldName9!=null && removeFieldName9!='--None--'){
          fieldsList.add(String.escapeSingleQuotes(removeFieldName9)+'&&!!'+String.escapeSingleQuotes(removeOperator9)+'&&!!'+((removeValue9!='')?String.escapeSingleQuotes(removeValue9):null));
          if(showAdvanceFormula)
              fieldsMap.put('8', String.escapeSingleQuotes(removeFieldName9)+'&&!!'+String.escapeSingleQuotes(removeOperator9)+'&&!!'+((removeValue9!='')?String.escapeSingleQuotes(removeValue9):null));
       }
       if(removeFieldName10!=null && removeFieldName10!='--None--'){
          fieldsList.add(String.escapeSingleQuotes(removeFieldName10)+'&&!!'+String.escapeSingleQuotes(removeOperator10)+'&&!!'+((removeValue10!='')?String.escapeSingleQuotes(removeValue10):null));
          if(showAdvanceFormula)
              fieldsMap.put('9', String.escapeSingleQuotes(removeFieldName10)+'&&!!'+String.escapeSingleQuotes(removeOperator10)+'&&!!'+((removeValue10!='')?String.escapeSingleQuotes(removeValue10):null));
       }

       String advFilterLogic='';
       if(showAdvanceFormula){
          ValidateAndOrExpression v= new ValidateAndOrExpression(advanceFilterLogic, fieldsMap.keySet(), true);
          if(v.hasError){
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Check Advance Filter Logic.\n'+v.message));
              hasError=true;
              return null;
          }
          advFilterLogic= ValidateAndOrExpression.convertExpressionKeys(advanceFilterLogic);
       }


       filterConditions = !showAdvanceFormula ? AdminHelperClass.generateDynamicQueryFilters('Remove Contacts',fieldsList,removeFilterLogic,'All Contacts')
                                              : AdminHelperClass.generateAdvanceFormulaFilterCondition('Remove Contacts', 'All Contacts', fieldsMap, advFilterLogic);
       system.debug('filterConditions'+filterConditions);
       if(filterConditions != '' && filterConditions != null){
          filterConditions += ' and Automated_Call_List__r.Name=:removeContactCallList and Call_Complete__c=false and Automated_Call_List__r.IsDeleted__c=false and Automated_Call_List__r.Archived__c=false and IsDeleted__c=false';
       }
       else{
          filterConditions = ' where Automated_Call_List__r.Name=:removeContactCallList and Call_Complete__c=false and Automated_Call_List__r.IsDeleted__c=false and Automated_Call_List__r.Archived__c=false and IsDeleted__c=false' ;        
       }   
       return filterConditions;

   }
}