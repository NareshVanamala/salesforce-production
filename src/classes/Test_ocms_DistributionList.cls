@isTest
public class Test_ocms_DistributionList 
{
    public static testmethod void Test_ocms_DistributionList () 
    { 
        profile pf=[select id,name from profile where name='Cole Capital Community'];
        account a=new account();
        a.name='test';
        insert a;
        
        Contact c1= new Contact();
        c1.firstName='Ninad';
        c1.lastName='Tambe';
        c1.accountid=a.id;
        c1.email='test123@noemail.com';
        insert c1;
        
        User u = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = pf.id, country='United States',IsActive =true,
                ContactId = c1.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert u;
        
        System.assertEquals(u.profileid,pf.id);

         
        system.runas(u)
        {
        Distributions__c di=new Distributions__c();
        di.category__c='Distributions';
        di.type_of_share__c='class w shares';
        di.daily_nav__c=40.00;
        di.date_paid__c=system.today();
        di.Net_Asset_Value__c=30.00;
        di.Annualized_Yield__c=10.00;
        di.Daily_Distribution__c=10.00;
        insert di;
     
        ocms_DistributionList rc14=new ocms_DistributionList();
        try
        {
            rc14.getDistributionList('class w shares','2014');
            rc14.writeControls();
            rc14.getHtml();
            
        }
          catch(Exception e)  {        }    
         
        
        ocms_DistributionList rc141=new ocms_DistributionList();
        try
        {
            rc141.getDistributionList('class w shares','Since Inception');
            rc141.writeControls();
            rc141.getHtml();
            
        }
          catch(Exception e)  {        }    
         
        
        }
         }
         
   }