public with sharing class DcActionPlanTaskTriggerUtilities {
public DcActionPlanTaskTriggerUtilities(){
        
       
    }
/*@future
    public static void deleteAPTasks( List<String> aPTasks ) {
        
        if( !aPTasks.isEmpty() ){
            
            delete [select aPT.id from APTaskTemplate__c aPT where aPT.id in : aPTasks];
        }
    }
    */
    /**
    * Delete Action Plan Tasks related Tasks
    * @parms apttIds
    */
    @future
    public static void deleteTasks( set<ID> apttIds ) {
        
        if( !apttIds.isEmpty() ){
            
         if (Task.sObjectType.getDescribe().isDeletable()) 
                {
                        delete [ select Id from Task where id in :apttIds ];
                }
            
        }
    }    

}