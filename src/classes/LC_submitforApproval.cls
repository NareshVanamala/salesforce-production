public with sharing class LC_submitforApproval {

  public LC_LeaseOpprtunity__c LCOP {get;set;}
   public String ids {get;set;}
  public ID parentId {get;set;}
   public List<LC_ClauseType__c> cl {get;set;}
  public Integer fileSize {get; set; }
  public List<Attachment> att{get;set;}
   Public Centralized_Helper_Object__c helperObject{get;set;}
 

  public LC_submitforApproval(ApexPages.StandardController controller)
  {
        ids = ApexPages.currentPage().getParameters().get('ids');
        system.debug('get me the current deal id'+ids);
        helperObject = new Centralized_Helper_Object__c();
        helperObject =[select id,LeaseCentral_Approval_Instructions__c,name from Centralized_Helper_Object__c where name='Lease Central-I'];
         
        if(ids!=null)
        {
         LCOP=[select Approval_status__c,LC_Already_Submitted__c, id,Milestone__c,Status__c, First_Approver__c,Second_Approver__c,Third_Approver__c,Fourth_Approver__c,Fifth_Approver__c,Sixth_Approver__c,Prepared_by__c,Deal_Maker__c,By__c,Current_Date__c from LC_LeaseOpprtunity__c where id=:ids];
         parentId = ids ;
         cl=[select Id,Name,Clause_Description__c,Clauses__c,ClauseType_Name__c,Lease_Opportunity__c from LC_ClauseType__c where Lease_Opportunity__c=:ids];
         
          att=[ SELECT Id,Name,ParentID,body FROM Attachment WHERE ParentID=:ids ];
        }
        
       
        
  }
  
    public PageReference SubmitForApproval()
   {
           for(LC_ClauseType__c clt : cl)
             
             {
                if(clt.Clause_Description__c== Null)
                {
                
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Warning,'Clause Descriptions cannot be Null: Please make sure that all the Clause Descriptions are entered before submitting LETS for Approval.');
                ApexPages.addMessage(myMsg);
                 return null;
                }
             
             }
          
           LCOP.Milestone__c='Approval Pending';
           LCOP.Status__c='In Active';
           LCOP.LC_Already_Submitted__c=LCOP.LC_Already_Submitted__c+1;
           LCOP.Approval_status__c='Submitted for Approval';
           if (Schema.sObjectType.LC_LeaseOpprtunity__c.isUpdateable()) 
           update LCOP;
           System.debug('chek the lease opportuniy'+LCOP );
           
           string mystringvarible='Called from Lets form';
           Decimal myintegervarible=LCOP.LC_Already_Submitted__c;
           LC_LeaseCentralHelper.SaveLETSFormCopy(parentId,myintegervarible,mystringvarible);
            //Attachment Code Starts
            /*PageReference pdf = Page.LC_LETSFormPdfPage;
            pdf.getParameters().put('id',parentId);
            Attachment attach = new Attachment();
            Blob body;
            
             try 
            {
            if (Test.IsRunningTest())
            {
                 body =Blob.valueOf('UNIT.TEST');
            }
              else
              {
                body = pdf.getContentAsPdf();

          
              }
            
            } 
            catch (VisualforceException e) 
            {
           
            }
            
             date d= system.today();
            attach.Body = body;
            attach.Name = 'LETS Form.pdf';
            attach.IsPrivate = false;
            attach.ParentId = parentId;
            insert attach;*/
            //Attachment Code ends
        
            System.debug('Welcome to the submit for approval method');
            List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();
            if(LCOP!=null)
            {
                
                 Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                 req.setComments('Submitted for Approval');  
                 req.setObjectId(LCOP.id);   
                 approvalReqList.add(req);
            }
            List<Approval.ProcessResult> resultList1 = Approval.process(approvalReqList); 
        
      
         PageReference acctPage = new PageReference('/'+LCOP.id);
        acctPage.setRedirect(true);
        return acctPage;
   

    }
    
      public PageReference Cancel()
    {
       PageReference acctPage = new PageReference('/'+LCOP.id);
        acctPage.setRedirect(true);
        return acctPage;
    }
    

}