@isTest
private class LogRIACallsTestCls{
static testMethod void  LogRIAtest() {
Account a =new Account();
a.Name =  'Test';
insert a;

Contact c = new Contact();
c.LastName = 'RIA';
c.AccountId = a.Id;
insert c;

Campaign cam = new Campaign();
cam.Name = 'Connectoer';
insert cam;

Test.startTest();

System.assertEquals('Test', a.Name );

System.assertEquals('RIA', c.LastName );
System.assertEquals('Connectoer', cam.Name);

Activity_Connector_Log_A_Call__c button = new Activity_Connector_Log_A_Call__c();

button.Contact__c = c.Id;
button.Account__c = a.Id;
button.Campaign__c = cam.Id;

insert button;
Test.stopTest();
}
}