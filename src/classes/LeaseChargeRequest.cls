public with sharing class LeaseChargeRequest{
private final MRI_PROPERTY__c MRI; 
public String LCRid{set;get;}
String cid;
public String newlease{get;set;} 
public String LCR{get;set;} 
 
        public LeaseChargeRequest(ApexPages.StandardController stdController) {
        this.MRI = (MRI_PROPERTY__c )stdController.getRecord();
        cid=ApexPages.currentPage().getParameters().get('cid');
        system.debug('cid'+cid);
        system.debug('this.MRI'+this.MRI);
        
    }
   
    
    public List<selectOption> getLeases() {
        List<selectOption> options = new List<selectOption>(); 
        list<Lease__c>Activeleaslist= new list<Lease__c>();
       options.add(new selectOption('', 'Select Lease Name')); 
       
       set<string> Activesuiteids = new set<string>();
        Activeleaslist=[select id, name ,Lease_ID__c,Lease_Status__c,MRI_PROPERTY__c,SuitVacancyIndicator__c ,Tenant_Name__c,SuiteId__c,Base_Rent_at_expiration__c from Lease__c where MRI_PROPERTY__c =:cid and Lease_Status__c = 'Active' and SuitVacancyIndicator__c = FALSE order by createddate desc];
        if(Activeleaslist!= null && Activeleaslist.size()>0){
            for(Lease__C leaseobj : Activeleaslist){
                Activesuiteids.add(leaseobj.SuiteId__c);
            }
        }
       if(Activeleaslist!= null && Activeleaslist.size()>0)
        {     
        list<string> suiteidnames = new list<string>();
        suiteidnames.addall(Activesuiteids);
        for(integer i=0;i<Activesuiteids.size();i++){
        for(Lease__c  l1:[select id,name,Lease_Status__c,Lease_ID__c,Lease_Status_Time_Stamp__c,LeaseId_TenanatName__c,SuiteId__c from Lease__c where SuiteId__c in :Activesuiteids and MRI_PROPERTY__c=:cid and Lease_Status__c = 'Active' order by createddate desc])
                {   if(l1.SuiteId__c == suiteidnames[i]){
                       options.add(new selectOption(L1.id, L1.LeaseId_TenanatName__c));  
                       break;
                   }
               }     
        }
        }

       // for (Lease__c L1 : [SELECT Id, Name,LeaseId_TenanatName__c FROM Lease__c where MRI_PROPERTY__c =:cid and Lease_Status__c = 'Active']) {     

      //  }
        return options; 
    }
    public pagereference save()
    {  
    
    system.debug('newlease'+newlease);
    if(newlease==null)
    {
       system.debug('newlease'+newlease);
       //String URL1=System.Label.Lease_Charge_Request_URL;
       PageReference pr1 = ApexPages.currentPage();
       pr1.setRedirect(true);
       return pr1;
    }
    else 
    {
         Double randomNumber = Math.random();
         Lease__c temp =[select id,name from Lease__c where id=:newlease];
         Lease_Charges_Request__c eachLeaseChargeReq= new Lease_Charges_Request__c();
         Lease_Charges_Request__c NewLCR = new Lease_Charges_Request__c();
         
         eachLeaseChargeReq.Lease__c=temp.id;
         eachLeaseChargeReq.Unique_ID__c=string.valueof(randomNumber);
         
         if (Schema.sObjectType.Lease_Charges_Request__c.isCreateable())

         insert eachLeaseChargeReq;
       
        PageReference pr = new PageReference('/' + eachLeaseChargeReq.id);
        system.debug('newlease'+newlease);
        pr.setRedirect(true);
        return pr;   
        }
    }
}