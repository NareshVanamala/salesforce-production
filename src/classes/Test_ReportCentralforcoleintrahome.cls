@isTest (SeeAllData = true)
public class Test_ReportCentralforcoleintrahome 
{
  
   public static testmethod void TestforReportCentralforcoleintrahome() 
   {     
 
   }
   
   public static testmethod void geteventsmethodstest()
   {  
   ReportCentralforcoleintrahome rc1 = new ReportCentralforcoleintrahome();
      String dtFormat = 'EEE, d MMM yyyy HH:mm:ss z';
      list<Calender_Event__c> ARCPCorporateEvent = new  list<Calender_Event__c>();
      list<Calender_Event__c> HolidayEvent = new  list<Calender_Event__c>();
      list<Calender_Event__c> BirthdayEvents= new  list<Calender_Event__c>();
      list<ReportCentralforcoleintrahome.calEvent> events = new list<ReportCentralforcoleintrahome.calEvent>();
      Date dummydate = System.today();
      DateTime dummydatetime = System.now();
      Map<String,Schema.RecordTypeInfo> recordTypesadjustment = Calender_Event__c.sObjectType.getDescribe().getRecordTypeInfosByName();
      //system.debug('abc'+recordTypesadjustment );
      Id RecTypeIdArcp = recordTypesadjustment.get('VEREIT & Cole Corporate Events').getRecordTypeId();
      //id idRT = [select Id from RecordType where SobjectType = 'Calender_Event__c' ].Id; 
      
      Calender_Event__c arcpe1 = new Calender_Event__c(RecordTypeId = RecTypeIdArcp ,Start_Date_Time__c=dummydatetime   ,End_Time__c= dummydatetime , Event_Description__c='testclass',name='arcpevent1',Event_Date__c=dummydate);
      ARCPCorporateEvent.Add(arcpe1);
      Calender_Event__c arcpe2 = new Calender_Event__c(RecordTypeId = RecTypeIdArcp ,Event_Description__c='testclass',name='arcpevent2',Event_Date__c=dummydate);
      ARCPCorporateEvent.Add(arcpe2);
      insert ARCPCorporateEvent;
      system.assert(ARCPCorporateEvent!=null);
      list<Calender_Event__c> ARCPList=[select id,Start_Date_Time__c,End_Time__c,Is_All_day_Event__c,Event_Description__c,Event_Date__c,name from Calender_Event__c where Event_Description__c= 'testclass'];
        
        
        
    for(Calender_Event__c evnt:ARCPList)
    {
        DateTime startDT = evnt.Start_Date_Time__c;
        DateTime endDT = evnt.End_Time__c;
        ReportCentralforcoleintrahome.calEvent myEvent = new ReportCentralforcoleintrahome.calEvent();
        myEvent.title = evnt.name;
        myEvent.allDay = evnt.Is_All_day_Event__c;
        if(startDT!= NULL)
            {
               myEvent.startString = startDT.format(dtFormat);
            }
          
            if(endDT != NULL)
            {
               myEvent.endString = endDT.format(dtFormat);
            }
        myEvent.url = evnt.Event_Description__c;
        myEvent.className = 'event-personal';
        myEvent.eventDescription = evnt.Event_Description__c;       
        events.add(myEvent);
        system.debug('My  events'+events);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
     }  
        Id RecTypeIdholi = recordTypesadjustment.get('Holiday Event').getRecordTypeId();
      Calender_Event__c holi1 = new Calender_Event__c(RecordTypeId = RecTypeIdholi ,Start_Date_Time__c=dummydatetime   ,End_Time__c= dummydatetime , Event_Description__c='testclasshalidayevent',name='halidayevent1',Event_Date__c=dummydate);
      HolidayEvent.Add(holi1 );
      Calender_Event__c holi2 = new Calender_Event__c(RecordTypeId = RecTypeIdholi ,Event_Description__c='testclasshalidayevent',name='halidayevent2',Event_Date__c=dummydate);
      HolidayEvent.Add(holi2 );
      insert HolidayEvent;
      system.assert(HolidayEvent!=null);
      list<Calender_Event__c> holidayEventLst =[select id,Start_Date_Time__c,End_Time__c,Is_All_day_Event__c,Event_Description__c,Event_Date__c,name from Calender_Event__c where Event_Description__c= 'testclasshalidayevent'];
        
    for(Calender_Event__c evnt:holidayEventLst)
    {
        DateTime startDT = evnt.Start_Date_Time__c;
        DateTime endDT = evnt.End_Time__c;
        ReportCentralforcoleintrahome.calEvent myEvent = new ReportCentralforcoleintrahome.calEvent();
        myEvent.title = evnt.name;
        myEvent.allDay = evnt.Is_All_day_Event__c;
          if(startDT!= NULL)
            {
               myEvent.startString = startDT.format(dtFormat);
            }
          
            if(endDT != NULL)
            {
               myEvent.endString = endDT.format(dtFormat);
            }
        myEvent.url = evnt.Event_Description__c;
        myEvent.className = 'event-personal';
        myEvent.eventDescription = evnt.Event_Description__c;       
        events.add(myEvent);
        system.debug('My  events'+events);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
     }  
     Id RecTypeIdBrthday = recordTypesadjustment.get('Birthday & Anniversary Event').getRecordTypeId();
     
      Calender_Event__c birthdayevent1= new Calender_Event__c(RecordTypeId = RecTypeIdBrthday ,Start_Date_Time__c=dummydatetime   ,End_Time__c= dummydatetime , Event_Description__c='testclassbirthdayevent',name='Birthdayevent1',Event_Date__c=dummydate);
      Birthdayevents.Add(birthdayevent1);
      Calender_Event__c birthdayevent2 = new Calender_Event__c(RecordTypeId = RecTypeIdBrthday ,Event_Description__c='testclassbirthdayevent',name='Birthdayevent2',Event_Date__c=dummydate);
      Birthdayevents.Add(birthdayevent2);
      insert Birthdayevents;
      system.assert(Birthdayevents!=null);
      list<Calender_Event__c> BdayList = [select id,Start_Date_Time__c,End_Time__c,Is_All_day_Event__c,Event_Description__c,Event_Date__c,name from Calender_Event__c where Event_Description__c= 'Birthdayevents'];
        
    for(Calender_Event__c evnt:BdayList)
    {
        DateTime startDT = evnt.Start_Date_Time__c;
        DateTime endDT = evnt.End_Time__c;
        ReportCentralforcoleintrahome.calEvent myEvent = new ReportCentralforcoleintrahome.calEvent();
        myEvent.title = evnt.name;
        myEvent.allDay = evnt.Is_All_day_Event__c;
          if(startDT!= NULL)
            {
               myEvent.startString = startDT.format(dtFormat);
            }
          
            if(endDT != NULL)
            {
               myEvent.endString = endDT.format(dtFormat);
            }
        myEvent.url = evnt.Event_Description__c;
        myEvent.className = 'event-personal';
        myEvent.eventDescription = evnt.Event_Description__c;       
        events.add(myEvent);
        system.debug('My  events'+events);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
     }  
     
     
       ReportCentralforcoleintrahome.PopulateCorporateEvent();
       //ReportCentralforcoleintrahome.populateBirthdays();
       ReportCentralforcoleintrahome.populateHolidayEvent();
       ReportCentralforcoleintrahome.birthdayData();
       string todayeventresult  = ReportCentralforcoleintrahome.GetCoraporateEventforToday();
     //  rc1.populateReportCentralData();
       string testreulst =ReportCentralforcoleintrahome.getCalendarEvent(); 
      
   
   }
   
  


}