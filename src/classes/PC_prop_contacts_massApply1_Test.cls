@isTest(seeAllData=true)
public class PC_prop_contacts_massApply1_Test {
    static testMethod void testmethod1(){
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
        Account acc = new Account (
        Name = 'newAcc1'
        );  
        insert acc;
        Contact con = new Contact (
        AccountId = acc.id,
        LastName = 'portalTestUser'
        );
        insert con;
        Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
         
        User newUser = new User(
        profileId = p.id,
        username = 'newUser0615201511@arcpreit.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = con.id
        );
        insert newUser;
        
        MRI_Property__c mrpObj = new MRI_Property__c();
        mrpObj.Name = 'testmrp';
        mrpObj.Property_Manager__c = newUser.id;
        mrpObj.Fund1__c = 'ARCP';
        mrpObj.Date_Acquired__c = Date.today();
        mrpObj.Common_Name__c = 'cname';
        mrpObj.Property_Id__c = '1234';
        insert mrpObj;
        
        Lease__c lObj1 = new Lease__c();
        lObj1.MRI_Property__c = mrpObj.Id;
        lObj1.Tenant_Name__c = 'testTenant1';
        lObj1.Lease_Status__c = 'Active';
        insert lObj1;
        
        Lease__c lObj2 = new Lease__c();
        lObj2.MRI_Property__c = mrpObj.Id;
        lObj2.Tenant_Name__c = 'testTenant1';
        lObj2.Lease_Status__c = 'Active';
        insert lObj2;
        
        Lease__c lObj3 = new Lease__c();
        lObj3.MRI_Property__c = mrpObj.Id;
        lObj3.Tenant_Name__c = 'testTenant1';
        lObj3.Lease_Status__c = 'Active';
        insert lObj3;
        
        Property_Contacts__c pcObj1 = new Property_Contacts__c();
        pcObj1.Name = 'test1';
        //pcObj1.Lease__c = lObj1.Id;
        pcObj1.MRI_Property__c = mrpObj.Id;
        insert pcObj1;
        
        Property_Contacts__c pcObj2 = new Property_Contacts__c();
        pcObj2.Name = 'test2';
        //pcObj2.Lease__c = lObj2.Id;
        pcObj2.MRI_Property__c = mrpObj.Id;
        insert pcObj2;
        
        Property_Contact_Leases__c pclObj1 = new Property_Contact_Leases__c();
        pclObj1.Lease__c = lObj1.Id;
        pclObj1.Property_Contacts__c = pcObj1.Id;
        pclObj1.Contact_Type__c = 'Billing Notice;Developer;Facilities Manager;Financials;Franchisor;Gross Sales;Guarantor;Landlord;Legal Admin;Seller;Servicer;';
        insert pclObj1;
        
        Property_Contact_Leases__c pclObj2 = new Property_Contact_Leases__c();
        pclObj2.Lease__c = lObj2.Id;
        pclObj2.Property_Contacts__c = pcObj2.Id;
        pclObj2.Contact_Type__c = 'Billing Notice;Developer;Facilities Manager;Franchisor;Guarantor;Landlord;Legal Admin;Seller;Servicer;';
        insert pclObj2;
        
        ApexPages.currentPage().getParameters().put('lid',pclObj1.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(mrpObj);
        PC_prop_contacts_massApply1 classObj = new PC_prop_contacts_massApply1(sc);
        List<PC_prop_contacts_massApply1.clease> cleaselist = new List<PC_prop_contacts_massApply1.clease>();
        cleaselist.add(new PC_prop_contacts_massApply1.clease(lObj1));
        cleaselist.add(new PC_prop_contacts_massApply1.clease(lObj2));
        classObj.leaseid = lObj1.id;
        classObj.commonname = 'cname';
        classObj.propertid = '1234';
        classObj.SaveRecord();
        classObj.SearchRecord(); 
        //classObj.next();
        //classObj.getnxt();
        //classObj.previous();
        //classObj.FirstPage();
        //classObj.done();
        //classObj.getprev();
        //classObj.LastPage();
        //classObj.previous();
    }
     static testMethod void testmethod2 (){
         Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
        Account acc = new Account (
        Name = 'newAcc1'
        );  
        insert acc;
        Contact con = new Contact (
        AccountId = acc.id,
        LastName = 'portalTestUser'
        );
        insert con;
        Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
         
        User newUser = new User(
        profileId = p.id,
        username = 'newUser061520151221@arcpreit.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = con.id
        );
        insert newUser;
        
        MRI_Property__c mrpObj = new MRI_Property__c();
        mrpObj.Name = 'testmrp';
        mrpObj.Property_Manager__c = newUser.id;
        mrpObj.Fund1__c = 'ARCP';
        mrpObj.Date_Acquired__c = Date.today();
        mrpObj.Common_Name__c = 'cname';
        mrpObj.Property_Id__c = '1234';
        insert mrpObj;
        
        Lease__c lObj1 = new Lease__c();
        lObj1.MRI_Property__c = mrpObj.Id;
        lObj1.Tenant_Name__c = 'testTenant1';
        lObj1.Lease_Status__c = 'Active';
        insert lObj1;
        
        Lease__c lObj2 = new Lease__c();
        lObj2.MRI_Property__c = mrpObj.Id;
        lObj2.Tenant_Name__c = 'testTenant1';
        lObj2.Lease_Status__c = 'Active';
        insert lObj2;
        
        Lease__c lObj3 = new Lease__c();
        lObj3.MRI_Property__c = mrpObj.Id;
        lObj3.Tenant_Name__c = 'testTenant1';
        lObj3.Lease_Status__c = 'Active';
        insert lObj3;
        
        Property_Contacts__c pcObj1 = new Property_Contacts__c();
        pcObj1.Name = 'test1';
       // pcObj1.Lease__c = lObj1.Id;
        pcObj1.MRI_Property__c = mrpObj.Id;
        insert pcObj1;
        
        Property_Contacts__c pcObj2 = new Property_Contacts__c();
        pcObj2.Name = 'test2';
       //pcObj2.Lease__c = lObj2.Id;
        pcObj2.MRI_Property__c = mrpObj.Id;
        insert pcObj2;
        
        Property_Contact_Leases__c pclObj1 = new Property_Contact_Leases__c();
        pclObj1.Lease__c = lObj1.Id;
        pclObj1.Property_Contacts__c = pcObj1.Id;
        pclObj1.Contact_Type__c = 'Billing Notice;Developer;Facilities Manager;Financials;Franchisor;Gross Sales;Guarantor;Landlord;Legal Admin;Seller;Servicer;';
        insert pclObj1;
        
        Property_Contact_Leases__c pclObj2 = new Property_Contact_Leases__c();
        pclObj2.Lease__c = lObj2.Id;
        pclObj2.Property_Contacts__c = pcObj2.Id;
        pclObj2.Contact_Type__c = 'Billing Notice;Developer;Facilities Manager;Franchisor;Guarantor;Landlord;Legal Admin;Seller;Servicer;';
        insert pclObj2;
        
        ApexPages.currentPage().getParameters().put('lid',pclObj2.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(mrpObj);
        PC_prop_contacts_massApply1 classObj = new PC_prop_contacts_massApply1(sc);
        List<PC_prop_contacts_massApply1.clease> cleaselist = new List<PC_prop_contacts_massApply1.clease>();
        cleaselist.add(new PC_prop_contacts_massApply1.clease(lObj1));
        cleaselist.add(new PC_prop_contacts_massApply1.clease(lObj2));
        classObj.leaseid = lObj3.id;
        classObj.commonname = 'cname';
        classObj.propertid = '1234';
        classObj.SaveRecord();
        classObj.SearchRecord(); 
        //classObj.next();
        //classObj.getnxt();
        //classObj.previous();
        //classObj.FirstPage();
        //classObj.done();
        //classObj.getprev();
       // classObj.LastPage();
    }
    static testMethod void testmethod3(){
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
        Account acc = new Account (
        Name = 'newAcc1'
        );  
        insert acc;
        Contact con = new Contact (
        AccountId = acc.id,
        LastName = 'portalTestUser'
        );
        insert con;
        Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
         
        User newUser = new User(
        profileId = p.id,
        username = 'newUser061520151331@arcpreit.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = con.id
        );
        insert newUser;
        
        MRI_Property__c mrpObj = new MRI_Property__c();
        mrpObj.Name = 'testmrp';
        mrpObj.Property_Manager__c = newUser.id;
        mrpObj.Fund1__c = 'ARCP';
        mrpObj.Date_Acquired__c = Date.today();
        mrpObj.Common_Name__c = 'cname';
        mrpObj.Property_Id__c = '1234';
        insert mrpObj;
        
        Lease__c lObj1 = new Lease__c();
        lObj1.MRI_Property__c = mrpObj.Id;
        lObj1.Tenant_Name__c = 'testTenant2';
        lObj1.Lease_Status__c = 'Active';
        insert lObj1;
        
        Lease__c lObj2 = new Lease__c();
        lObj2.MRI_Property__c = mrpObj.Id;
        lObj2.Tenant_Name__c = 'testTenant2';
        lObj2.Lease_Status__c = 'Active';
        insert lObj2;
              
        Property_Contacts__c pcObj1 = new Property_Contacts__c();
        pcObj1.Name = 'test1';
        //pcObj1.Lease__c = lObj1.Id;
        pcObj1.MRI_Property__c = mrpObj.Id;
        insert pcObj1;
        
        Property_Contact_Leases__c pclObj1 = new Property_Contact_Leases__c();
        pclObj1.Lease__c = lObj1.Id;
        pclObj1.Property_Contacts__c = pcObj1.Id;
        pclObj1.Contact_Type__c = 'Billing Notice;Developer;Facilities Manager;Financials;Franchisor;Gross Sales;Guarantor;Landlord;Legal Admin;Seller;Servicer;';
        insert pclObj1;
        
        ApexPages.currentPage().getParameters().put('lid',pclObj1.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(mrpObj);
        PC_prop_contacts_massApply1 classObj = new PC_prop_contacts_massApply1(sc);
        List<PC_prop_contacts_massApply1.clease> cleaselist = new List<PC_prop_contacts_massApply1.clease>();
        cleaselist.add(new PC_prop_contacts_massApply1.clease(lObj1));
        classObj.leaseid = lObj2.id;
        classObj.commonname = '';
        classObj.propertid = '';
        classObj.SaveRecord();
        classObj.SearchRecord();
    }
    static testMethod void testmethod4(){
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
        Account acc = new Account (
        Name = 'newAcc1'
        );  
        insert acc;
        Contact con = new Contact (
        AccountId = acc.id,
        LastName = 'portalTestUser'
        );
        insert con;
        Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
         
        User newUser = new User(
        profileId = p.id,
        username = 'newUser061520151331@arcpreit.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = con.id
        );
        insert newUser;
        
        MRI_Property__c mrpObj = new MRI_Property__c();
        mrpObj.Name = 'testmrp';
        mrpObj.Property_Manager__c = newUser.id;
        mrpObj.Fund1__c = 'ARCP';
        mrpObj.Date_Acquired__c = Date.today();
        mrpObj.Common_Name__c = 'cname';
        mrpObj.Property_Id__c = '1234';
        insert mrpObj;
        
        Lease__c lObj1 = new Lease__c();
        lObj1.MRI_Property__c = mrpObj.Id;
        lObj1.Tenant_Name__c = 'testTenant2';
        lObj1.Lease_Status__c = 'Active';
        insert lObj1;
        
        Lease__c lObj2 = new Lease__c();
        lObj2.MRI_Property__c = mrpObj.Id;
        lObj2.Tenant_Name__c = 'testTenant2';
        lObj2.Lease_Status__c = 'Active';
        insert lObj2;
              
        Property_Contacts__c pcObj1 = new Property_Contacts__c();
        pcObj1.Name = 'test1';
        //pcObj1.Lease__c = lObj1.Id;
        pcObj1.MRI_Property__c = mrpObj.Id;
        insert pcObj1;
        
        Property_Contact_Leases__c pclObj1 = new Property_Contact_Leases__c();
        pclObj1.Lease__c = lObj1.Id;
        pclObj1.Property_Contacts__c = pcObj1.Id;
        pclObj1.Contact_Type__c = 'Billing Notice;Developer;Facilities Manager;Financials;Franchisor;Gross Sales;Guarantor;Landlord;Legal Admin;Seller;Servicer;';
        insert pclObj1;

        Property_Contact_Leases__c pclObj2 = new Property_Contact_Leases__c();
        pclObj2.Lease__c = lObj2.Id;
        pclObj2.Property_Contacts__c = pcObj1.Id;
        pclObj2.Contact_Type__c = 'Billing Notice;Developer;Facilities Manager;Financials;Franchisor;Gross Sales;Guarantor;Landlord;Legal Admin;Seller;Servicer;';
        insert pclObj2;
        
        ApexPages.currentPage().getParameters().put('lid',pclObj1.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(mrpObj);
        PC_prop_contacts_massApply1 classObj = new PC_prop_contacts_massApply1(sc);
        List<PC_prop_contacts_massApply1.clease> cleaselist = new List<PC_prop_contacts_massApply1.clease>();
        cleaselist.add(new PC_prop_contacts_massApply1.clease(lObj1));
        classObj.leaseid = lObj2.id;
        classObj.commonname = '';
        classObj.propertid = '';
        classObj.SaveRecord();
        classObj.SearchRecord();
    }
    static testMethod void testmethod5(){
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
        Account acc = new Account (
        Name = 'newAcc1'
        );  
        insert acc;
        Contact con = new Contact (
        AccountId = acc.id,
        LastName = 'portalTestUser'
        );
        insert con;
        Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
         
        User newUser = new User(
        profileId = p.id,
        username = 'newUser061520151331@arcpreit.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = con.id
        );
        insert newUser;
        
        MRI_Property__c mrpObj = new MRI_Property__c();
        mrpObj.Name = 'testmrp';
        mrpObj.Property_Manager__c = newUser.id;
        mrpObj.Fund1__c = 'ARCP';
        mrpObj.Date_Acquired__c = Date.today();
        mrpObj.Common_Name__c = 'cname';
        mrpObj.Property_Id__c = '1234';
        insert mrpObj;
        
        Lease__c lObj1 = new Lease__c();
        lObj1.MRI_Property__c = mrpObj.Id;
        lObj1.Tenant_Name__c = 'testTenant2';
        lObj1.Lease_Status__c = 'Active';
        insert lObj1;
        
        Lease__c lObj2 = new Lease__c();
        lObj2.MRI_Property__c = mrpObj.Id;
        lObj2.Tenant_Name__c = 'testTenant2';
        lObj2.Lease_Status__c = 'Active';
        insert lObj2;
              
        Property_Contacts__c pcObj1 = new Property_Contacts__c();
        pcObj1.Name = 'test1';
        //pcObj1.Lease__c = lObj1.Id;
        pcObj1.MRI_Property__c = mrpObj.Id;
        insert pcObj1;
        
        Property_Contact_Leases__c pclObj1 = new Property_Contact_Leases__c();
        pclObj1.Lease__c = lObj1.Id;
        pclObj1.Property_Contacts__c = pcObj1.Id;
        pclObj1.Contact_Type__c = 'Billing Notice;Developer;Facilities Manager;Franchisor;Gross Sales;Guarantor;Landlord;Legal Admin;Seller;Servicer;';
        insert pclObj1;

        Property_Contact_Leases__c pclObj2 = new Property_Contact_Leases__c();
        pclObj2.Lease__c = lObj2.Id;
        pclObj2.Property_Contacts__c = pcObj1.Id;
        pclObj2.Contact_Type__c = 'Billing Notice;Developer;Facilities Manager;Financials;Franchisor;Gross Sales;Guarantor;Landlord;Legal Admin;Seller;Servicer;';
        insert pclObj2;
        
        ApexPages.currentPage().getParameters().put('lid',pclObj1.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(mrpObj);
        PC_prop_contacts_massApply1 classObj = new PC_prop_contacts_massApply1(sc);
        List<PC_prop_contacts_massApply1.clease> cleaselist = new List<PC_prop_contacts_massApply1.clease>();
        cleaselist.add(new PC_prop_contacts_massApply1.clease(lObj1));
        classObj.leaseid = lObj2.id;
        classObj.commonname = '';
        classObj.propertid = '';
        classObj.SaveRecord();
        classObj.SearchRecord();
    }   
    static testMethod void testmethod6(){
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
        Account acc = new Account (
        Name = 'newAcc1'
        );  
        insert acc;
        Contact con = new Contact (
        AccountId = acc.id,
        LastName = 'portalTestUser'
        );
        insert con;
        Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
         
        User newUser = new User(
        profileId = p.id,
        username = 'newUser061520151331@arcpreit.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = con.id
        );
        insert newUser;
        
        MRI_Property__c mrpObj = new MRI_Property__c();
        mrpObj.Name = 'testmrp';
        mrpObj.Property_Manager__c = newUser.id;
        mrpObj.Fund1__c = 'ARCP';
        mrpObj.Date_Acquired__c = Date.today();
        mrpObj.Common_Name__c = 'cname';
        mrpObj.Property_Id__c = '1234';
        insert mrpObj;
        
        Lease__c lObj1 = new Lease__c();
        lObj1.MRI_Property__c = mrpObj.Id;
        lObj1.Tenant_Name__c = 'testTenant2';
        lObj1.Lease_Status__c = 'Active';
        insert lObj1;
        
        Lease__c lObj2 = new Lease__c();
        lObj2.MRI_Property__c = mrpObj.Id;
        lObj2.Tenant_Name__c = 'testTenant2';
        lObj2.Lease_Status__c = 'Active';
        insert lObj2;
              
        Property_Contacts__c pcObj1 = new Property_Contacts__c();
        pcObj1.Name = 'test1';
        //pcObj1.Lease__c = lObj1.Id;
        pcObj1.MRI_Property__c = mrpObj.Id;
        insert pcObj1;
        
        Property_Contact_Leases__c pclObj1 = new Property_Contact_Leases__c();
        pclObj1.Lease__c = lObj1.Id;
        pclObj1.Property_Contacts__c = pcObj1.Id;
        pclObj1.Contact_Type__c = 'Billing Notice;Developer;Facilities Manager;Financials;Franchisor;Guarantor;Landlord;Legal Admin;Seller;Servicer;';
        insert pclObj1;

        Property_Contact_Leases__c pclObj2 = new Property_Contact_Leases__c();
        pclObj2.Lease__c = lObj2.Id;
        pclObj2.Property_Contacts__c = pcObj1.Id;
        pclObj2.Contact_Type__c = 'Billing Notice;Developer;Facilities Manager;Financials;Franchisor;Gross Sales;Guarantor;Landlord;Legal Admin;Seller;Servicer;';
        insert pclObj2;
        
        ApexPages.currentPage().getParameters().put('lid',pclObj1.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(mrpObj);
        PC_prop_contacts_massApply1 classObj = new PC_prop_contacts_massApply1(sc);
        PC_prop_contacts_massApply1 classObj1 = new PC_prop_contacts_massApply1(sc);
        List<PC_prop_contacts_massApply1.clease> cleaselist = new List<PC_prop_contacts_massApply1.clease>();
        classObj1.getPropManagers();
        classObj1.getProptypes();
        classObj1.getPropstates();
        classObj1.customSearch();
        classObj1.getPropstates();
        classObj1.getPropstates();
        classObj1.SaveRecord();
        classObj1.SearchRecord();
        cleaselist.add(new PC_prop_contacts_massApply1.clease(lObj1));
        classObj.leaseid = lObj2.id;
        classObj.commonname = '';
        classObj.propertid = '';
        classObj.SaveRecord();
        classObj.SearchRecord();
    }   
}