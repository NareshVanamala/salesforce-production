@isTest
public class Test_ContactLifeCycle
{
    
    static testmethod void myTest_Cycle()
    {
        Test.StartTest();
        
        
         Contact c = new Contact();             
         c.FirstName = 'Test';             
         c.LastName = 'Contact';                         
         c.Email = 'contactemail@example.com';   
         c.Contact_Life_Cycle__c = 'Producers';
         c.ColorRBG__c ='Red';
         insert c; 
         
         System.assert(c != null);
         
         ApexPages.currentPage().getParameters().put('id',c.id);

  
         ApexPages.StandardController controller = new ApexPages.StandardController(c); 
         ContactLifeCyclesnehal con = new ContactLifeCyclesnehal(controller);
         ContactLifeCyclesnehal.Conupdate(c.id,'Green');
         
         c.ColorRBG__c = 'Green';
         update c;
         
    }
}