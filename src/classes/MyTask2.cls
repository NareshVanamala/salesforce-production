public with sharing class MyTask2
{ 
    
 /*  list<Task> lsttask = new list<Task>();
    list<contact> lstAllContact = new list<contact>();
    public list<contact> lstContact {set;get;}
    public List<SelectOption> CallListValues {set;get;}
    public String CallList {set;get;}
    public Map<Id,Task> mapOfContactToTask{get;set;}
    public Map<integer,Contact> rowmap{get;set;}
    public integer first{set;get;}
    public integer rows{set;get;}
    public Contact con {set;get;}
    public boolean showPrev{set;get;}
    public boolean showNext{set;get;}
    public Integer rownum { get;set;}{rownum=-10;}
    integer limitFrom=0;
    integer limitTo=0;
    Transient  long currenttime=0;
    Transient  decimal duration=0;
    Transient  integer D1;
    Transient integer D1Minits;
    Transient  integer Mintssec;
    Transient  Integer HoursSec;
    Transient integer D2;
    Transient  integer D2Minits;
    Transient  integer Mintssec1;    
    public Task tskObj {set;get;}
    public Task newtskObj {get;set;}
    public String cid{get;set;}
    private  String  nxtcId=null;
    public String nxt{get;set;}
    public List<Task> trellist{get;set;}
    public Integer showmorecount{get;set;}
    public boolean bnCount{get;set;}
    public String CallQueueValue {set;get;}
    set<id> ConIds = new set<id>();
    ApexPages.StandardSetController cont1;
    Integer PageNo =1;
    
    public MyTask2(){
        lstContact = new list<contact>();
        set<String> call = new Set<String>();    
        List<String> lscall = new List<String>();
        newtskObj = new task();
        showPrev = false;
        showNext = false;
        rows = 10;
        first = 0;
         try{
             
             //get the call list names assigned to the logged in user//
         
            List<Task> tcall = [select OwnerId,ID,Call_list__c from Task where(OwnerId =:userinfo.getUserid() and Status='Not Started'and Call_list__c!=null and ActivityDate=null ) limit 10000];
            //Add call lists name into picklist call list//
           //First added all the vslues in the set to avoidd duplicates and then into the list and then into the picklist//
            for(Task t : tcall)
            call.add(t.Call_list__c); 
            call.add('--None--');
            CallListValues = new  List<SelectOption>();
            lscall.AddAll(call);
            lscall.sort();
            for(String s : lscall){                
                CallListValues.add(new SelectOption(s,s));
            }
            if(CallListValues.size()==0)
               CallListValues.add(new SelectOption('--None--','--None--'));
        }catch(Exception e){}   
    }
    public PageReference calllistgenerator(){
        PageReference pageref = new PageReference('/apex/CallListGeneratorVf');
        return pageref;
    }
    public void AdviserCallList(){         
        String qry='';
        integer count=1;        
        mapOfContactToTask = new Map<Id,Task>();
        rowmap = new Map<integer,Contact>();
       // get the tasks from the selcted call list//
        lsttask=[select Id, OwnerId,Status,Type,Whoid,Who.Name,What.Name,ActivityDate,Priority,Subject,Call_list__c from Task 
                where OwnerId =:userinfo.getUserid() and Call_list__c=:CallList and Status='Not Started' limit 1500];
        //create the map of contact and task//
        for(Task tObj : lsttask){
            mapOfContactToTask.put(tObj.Whoid,tObj);
        }
        //get the contact ids for getting the Contact lists//
        ConIds = mapOfContactToTask.keyset();
        qry = 'select id,name,Account.name,Company_Name__c,Territory__c,phone,email,Advisor_Lifecycle__c,OwnerId from Contact where id in : ConIds ORDER BY CreatedDate ASC limit 1500';
        //get the standard Controller to have contact values//
        cont1 = new ApexPages.StandardSetController(Database.getQueryLocator(qry));
        //set the page size of the standard Controller//
        cont1.setPageSize(10);
        cont1.setPageNumber(PageNo);
        //initilaize the rowmap to set the sequence of the contacts//
        for(sObject sobj :cont1.getRecords()){         
           Contact con = (Contact)sobj;
           rowmap.put(count++,con);        
        }
        showNext = cont1.gethasNext();      
        showPrev = cont1.gethasPrevious();
    }
    
    public void NextAdvDetails()
    {
      try
       {
        if(tskobj.ownerid!=null ){
              //calculate the duration of the call//
              //Calculate();
              //save the contact details//
            ContactSave();
            //create the new task//
            CreateNewTask();            
            TaskSave();    
            try{
                // get the selected contact id from the Rowmap//
                system.debug('Check the selected Contact'+ cid);
                Integer i=0;                
                for(Contact con : rowmap.values()){
                    if(con.Id==cid){
                        i++;
                        break ;
                    }
                    i++;
                }  
                //delete the Completed task from the call queue //              
                AdviserCallList();
                system.debug('check the cid after calling AdviserCallList method'+cid);
                  //adjust the sequence get the next advisor//
                if(rowmap.get(i)!=null)
                    cid = rowmap.get(i).Id;
               system.debug('check the cid after calling AdviserCallList and adjusting the sequence'+cid);     
                    //get all the details of the selected advisor// 
                condetail();        
            }catch(Exception E)  { system.debug('Exception---->'+E.getmessage());}
        }else{
           ApexPages.Message  msg= new ApexPages.Message(ApexPages.Severity.ERROR, 'Assigned to cannot be left blank!', 'Assigned to cannot be left blank!');
           ApexPages.addmessage(msg); 
        }
        }
        Catch(Exception e)
        {}        
    }   
    public void CreateNewTask(){
        try{
             //get the task after selecting the advisor//
               if(mapOfContactToTask.get(cid) != NULL)
               tskObj = mapOfContactToTask.get(cid);
               system.debug('TASK RASKSKKKKKKKKKKKKKKKKK'+tskobj.Call_list__c);
           //copy the Call list name into activity call list and create a new task for the advisor//
            if(newtskobj.subject!=null && newtskobj.ownerid!=null ){
                 newtskobj.Activity_Call_list__c = tskobj.Call_list__c;
                 system.debug('NINAD NINAD NINAD NINAD NINAD REPORT FIELD REPORT FIELD REPORT FIELD'+tskobj.Call_list__c);
                 system.debug('NINAD NINAD NINAD NINAD NINAD REPORT FIELD REPORT FIELD REPORT FIELD'+newtskobj.Activity_Call_list__c);
                 upsert newtskObj;
            }
        }
        catch(Exception e){system.debug('-----exception----'+e.getmessage());}    
    }
    
    public void ContactSave() {
        try{
        //save the contact if modified//
            update con;
            System.debug('*****************************\n'+con);
        }catch(Exception e){System.debug('EEEEEEEXXXX'+e);}
    }
    
    public void TaskSave(){        
        try{
        //update the status as completed and save the task//
            tskObj.Status = 'Completed';           
            update tskObj;
        }Catch(Exception e){}           
    }
    /*public void getshowNext(){
        if(cont1!=null){
            if(cont1.gethasNext())
                showNext = true;
            else
                showNext = false;
            system.debug('------showNext------'+showNext);  
        }
    }
    public void getshowPrev(){
        if(cont1!=null){
            if(cont1.gethasNext())
                showPrev = true;
            else
                showPrev = false;
        }
    }
   
    public void next(){     
          if(cont1.gethasNext()){
            integer count=1;
            rowmap= new Map<Integer,Contact>();
        //get the next page of the controller//
            cont1.Next();
            PageNo++;
            //arrange the sequence of the contacts in rowmap//
            for(sObject sobj :cont1.getRecords()){
                Contact con = (Contact)sobj;
                rowmap.put(count++,con);
            }
            showNext = cont1.gethasNext();      
            showPrev = cont1.gethasPrevious();  
        }
    }
    public void previous(){
        if(cont1.gethasPrevious()){
            integer count=1;
            rowmap= new Map<Integer,Contact>();
            cont1.Previous();   
            PageNo--;
            //adjust the sequence of the Contacts in rowmap//
            for(sObject sobj :cont1.getRecords()){
                Contact con = (Contact)sobj;
                rowmap.put(count++,con);
            }
            showNext = cont1.gethasNext();      
            showPrev = cont1.gethasPrevious();  
        }
    }
    public void condetail(){
        System.debug('D!!!!!!!!!!!!!!!!!!:-'+D1);
           tskObj = new Task();
         newtskobj=new task();
        if(mapOfContactToTask.get(cid) != NULL)
           tskObj = mapOfContactToTask.get(cid);
           //get the start time  of the call for calculating the duration//
           datetime myDate = datetime.now();
         D1=myDate.second();
         HoursSec = myDate.hour();
         D1Minits = myDate.minute();   
         Mintssec = (HoursSec*60*60)+(D1Minits*60) + D1;
         System.debug('D!!!!!!!!!!!!!!!!!!:-'+D1);
         tskobj.Time_Duration__c = Mintssec;    
         system.debug('GET THE START TIME OF THE CALL'+tskobj.Time_Duration__c);        
        id OwnerId;
        try{
            nxt=nxtcId;   
            //for the last record//     
            If(cid != null && nxtcId==null ){ 
              //  con=[select OwnerId,id,name,Middle_Name__c,Company_Name__c,Account.name,Account.X1031_Selling_Agreement__c,Phone,MailingCountry,MailingCity,MailingPostalCode,MailingState,MailingStreet,Verify_Mailing_Address__c,OtherCity,OtherCountry,OtherPostalCode,OtherState,OtherStreet,Email,HasOptedOutOfEmail,Wholesaler__c,Nearby_CCPT_III_Properties__c,Nearby_CCPTII_Properties__c,Nearby_CCPTII_Properties_II__c,License__c,Suffix__c,Contact_Nickname__c,Title,Note__c,MobilePhone,OtherPhone,HomePhone,fax,ExactTarget_Status__c,Website__c,Do_NOT_Contact__c,AssistantName,AssistantPhone,Assistant_Email__c,Rep_CRD__c,Firm_CRD__c,Company_ID__c,Discovery_Data__c,Total_Cole_Production_Funds_in_SF__c,Total_Cole_Tickets_Only_Funds_in_SF__c,CCIT__c,CCPT_IV__c,CCPT_III__c,CCPT_III_Tickets__c,CCPT_II__c,CCPT_II_Tickets__c,CCPT__c,CCPT_Tickets__c,Relationship__c,Rating__c,Contact_Type__c,Territory__c,Regional_Territory_Manager__c,Internal_Wholesaler__c,Virtual_Relationship__c,Wholesaler_Management__c,Advisor_Lifecycle__c,Lifecycle_Override__c,Days_Since_Last_Transaction__c,First_Producer_Date__c,Last_Producer_Date__c,Territory_Zone__c,Region_Zone__c,Gross_Annual_Production__c,AUM_Actual__c,Years_as_a_Rep__c,Years_at_Current_Firm__c,Date_of_Birth__c,Business_Profile__c,Product_Profile__c,Client_Profile__c,Competitors__c,BD_Top_Producer__c,Top_REIT_Producer__c,Real_Estate_Producer__c,Duly_Registered__c,Due_Diligence_Meeting_Attended__c,Symposium_March_2010_Attended__c,Symposium_2011_Attended__c,Symposium_Relationship__c,Annuities_1__c,Bonds__c,Derivatives__c,Exchange_Traded_Funds__c,Hedge_Funds__c,Insurance_Products__c,International_Investments_ADRs__c,Mutual_Funds_1__c,Options__c,Sells_Retirement_Pension_Products__c,Separately_Managed_Accounts__c,Stocks__c,Non_Producer_Discovery_Data__c,Growth_Opportunity_Fund__c,REIT_Investments_Amount__c,X1031_Kit_Received__c,X1031_National_Meetings__c,X1031_Rating__c,X1031_Relationship__c,X1031_Sales_Director__c,REIT_Investments_Count__c,X1031_Zone__c,Marketing_OverridesFees__c from Contact where id=: cid limit 1500 ];
              con=[select OwnerId,id,name,Middle_Name__c,Company_Name__c,Account.name,Account.X1031_Selling_Agreement__c,Phone,MailingCountry,MailingCity,MailingPostalCode,MailingState,MailingStreet,Verify_Mailing_Address__c,OtherCity,OtherCountry,OtherPostalCode,OtherState,OtherStreet,Email,HasOptedOutOfEmail,Wholesaler__c,Nearby_CCPT_III_Properties__c,Nearby_CCPTII_Properties__c,Nearby_CCPTII_Properties_II__c,License__c,Suffix__c,Contact_Nickname__c,Title,Note__c,MobilePhone,OtherPhone,HomePhone,fax,ExactTarget_Status__c,Website__c,Do_NOT_Contact__c,AssistantName,AssistantPhone,Assistant_Email__c,Rep_CRD__c,Firm_CRD__c,Company_ID__c,Discovery_Data__c,Total_Cole_Production_Funds_in_SF__c,Total_Cole_Tickets_Only_Funds_in_SF__c,CCIT__c,CCPT_IV__c,CCPT_III__c,CCPT_III_Tickets__c,Relationship__c,Rating__c,Contact_Type__c,Territory__c,Regional_Territory_Manager__c,Internal_Wholesaler__c,Virtual_Relationship__c,Wholesaler_Management__c,Advisor_Lifecycle__c,Lifecycle_Override__c,Days_Since_Last_Transaction__c,First_Producer_Date__c,Last_Producer_Date__c,Territory_Zone__c,Region_Zone__c,Gross_Annual_Production__c,AUM_Actual__c,Years_as_a_Rep__c,Years_at_Current_Firm__c,Date_of_Birth__c,Business_Profile__c,Product_Profile__c,Client_Profile__c,Competitors__c,BD_Top_Producer__c,Top_REIT_Producer__c,Real_Estate_Producer__c,Duly_Registered__c,Due_Diligence_Meeting_Attended__c,Symposium_March_2010_Attended__c,Symposium_2011_Attended__c,Symposium_Relationship__c,Annuities_1__c,Bonds__c,Derivatives__c,Exchange_Traded_Funds__c,Hedge_Funds__c,Insurance_Products__c,International_Investments_ADRs__c,Mutual_Funds_1__c,Options__c,Sells_Retirement_Pension_Products__c,Separately_Managed_Accounts__c,Stocks__c,Non_Producer_Discovery_Data__c,Growth_Opportunity_Fund__c,REIT_Investments_Amount__c,X1031_Kit_Received__c,X1031_National_Meetings__c,X1031_Rating__c,X1031_Relationship__c,X1031_Sales_Director__c,REIT_Investments_Count__c,X1031_Zone__c from Contact where id=: cid limit 1500 ];
                trellist=[select Id,OwnerId,Status,Short_Comment__c,CheckTask__c,LastModifiedDate,Type,Whoid,Who.Name,What.Name,ActivityDate,Priority,Subject from Task where Whoid =: con.id and Status='Completed' Order By ActivityDate desc];
                //tasklistsize = trellist.size();
                System.debug('#####trellist' + trellist);
                System.debug('#####trellist' + trellist.size());
                showmorecount=10;
                bnCount = trellist.size() <= 10;                
                System.debug('----------'+nxtcId);
            }else{
                System.debug('-------NNNNNNNN---'+nxtcId);              
              //  con=[select OwnerId,id,name,Middle_Name__c,Company_Name__c,Account.name,Account.X1031_Selling_Agreement__c,Phone,MailingCountry,MailingCity,MailingPostalCode,MailingState,MailingStreet,Verify_Mailing_Address__c,OtherCity,OtherCountry,OtherPostalCode,OtherState,OtherStreet,Email,HasOptedOutOfEmail,Wholesaler__c,Nearby_CCPT_III_Properties__c,Nearby_CCPTII_Properties__c,Nearby_CCPTII_Properties_II__c,License__c,Suffix__c,Contact_Nickname__c,Title,Note__c,MobilePhone,OtherPhone,HomePhone,fax,ExactTarget_Status__c,Website__c,Do_NOT_Contact__c,AssistantName,AssistantPhone,Assistant_Email__c,Rep_CRD__c,Firm_CRD__c,Company_ID__c,Discovery_Data__c,Total_Cole_Production_Funds_in_SF__c,Total_Cole_Tickets_Only_Funds_in_SF__c,CCIT__c,CCPT_IV__c,CCPT_III__c,CCPT_III_Tickets__c,CCPT_II__c,CCPT_II_Tickets__c,CCPT__c,CCPT_Tickets__c,Relationship__c,Rating__c,Contact_Type__c,Territory__c,Regional_Territory_Manager__c,Internal_Wholesaler__c,Virtual_Relationship__c,Wholesaler_Management__c,Advisor_Lifecycle__c,Lifecycle_Override__c,Days_Since_Last_Transaction__c,First_Producer_Date__c,Last_Producer_Date__c,Territory_Zone__c,Region_Zone__c,Gross_Annual_Production__c,AUM_Actual__c,Years_as_a_Rep__c,Years_at_Current_Firm__c,Date_of_Birth__c,Business_Profile__c,Product_Profile__c,Client_Profile__c,Competitors__c,BD_Top_Producer__c,Top_REIT_Producer__c,Real_Estate_Producer__c,Duly_Registered__c,Due_Diligence_Meeting_Attended__c,Symposium_March_2010_Attended__c,Symposium_2011_Attended__c,Symposium_Relationship__c,Annuities_1__c,Bonds__c,Derivatives__c,Exchange_Traded_Funds__c,Hedge_Funds__c,Insurance_Products__c,International_Investments_ADRs__c,Mutual_Funds_1__c,Options__c,Sells_Retirement_Pension_Products__c,Separately_Managed_Accounts__c,Stocks__c,Non_Producer_Discovery_Data__c,Growth_Opportunity_Fund__c,REIT_Investments_Amount__c,X1031_Kit_Received__c,X1031_National_Meetings__c,X1031_Rating__c,X1031_Relationship__c,X1031_Sales_Director__c,REIT_Investments_Count__c,X1031_Zone__c,Marketing_OverridesFees__c from Contact where id=: nxtcId limit 1500];
                con=[select OwnerId,id,name,Middle_Name__c,Company_Name__c,Account.name,Account.X1031_Selling_Agreement__c,Phone,MailingCountry,MailingCity,MailingPostalCode,MailingState,MailingStreet,Verify_Mailing_Address__c,OtherCity,OtherCountry,OtherPostalCode,OtherState,OtherStreet,Email,HasOptedOutOfEmail,Wholesaler__c,Nearby_CCPT_III_Properties__c,Nearby_CCPTII_Properties__c,Nearby_CCPTII_Properties_II__c,License__c,Suffix__c,Contact_Nickname__c,Title,Note__c,MobilePhone,OtherPhone,HomePhone,fax,ExactTarget_Status__c,Website__c,Do_NOT_Contact__c,AssistantName,AssistantPhone,Assistant_Email__c,Rep_CRD__c,Firm_CRD__c,Company_ID__c,Discovery_Data__c,Total_Cole_Production_Funds_in_SF__c,Total_Cole_Tickets_Only_Funds_in_SF__c,CCIT__c,CCPT_IV__c,CCPT_III__c,CCPT_III_Tickets__c,Relationship__c,Rating__c,Contact_Type__c,Territory__c,Regional_Territory_Manager__c,Internal_Wholesaler__c,Virtual_Relationship__c,Wholesaler_Management__c,Advisor_Lifecycle__c,Lifecycle_Override__c,Days_Since_Last_Transaction__c,First_Producer_Date__c,Last_Producer_Date__c,Territory_Zone__c,Region_Zone__c,Gross_Annual_Production__c,AUM_Actual__c,Years_as_a_Rep__c,Years_at_Current_Firm__c,Date_of_Birth__c,Business_Profile__c,Product_Profile__c,Client_Profile__c,Competitors__c,BD_Top_Producer__c,Top_REIT_Producer__c,Real_Estate_Producer__c,Duly_Registered__c,Due_Diligence_Meeting_Attended__c,Symposium_March_2010_Attended__c,Symposium_2011_Attended__c,Symposium_Relationship__c,Annuities_1__c,Bonds__c,Derivatives__c,Exchange_Traded_Funds__c,Hedge_Funds__c,Insurance_Products__c,International_Investments_ADRs__c,Mutual_Funds_1__c,Options__c,Sells_Retirement_Pension_Products__c,Separately_Managed_Accounts__c,Stocks__c,Non_Producer_Discovery_Data__c,Growth_Opportunity_Fund__c,REIT_Investments_Amount__c,X1031_Kit_Received__c,X1031_National_Meetings__c,X1031_Rating__c,X1031_Relationship__c,X1031_Sales_Director__c,REIT_Investments_Count__c,X1031_Zone__c from Contact where id=: nxtcId limit 1500];
                trellist=[select Id,OwnerId,Status,Short_Comment__c,CheckTask__c,LastModifiedDate,Type,Whoid,Who.Name,What.Name,ActivityDate,Priority,Subject from Task where Whoid =: con.id and Status='Completed' Order By ActivityDate desc];
                //tasklistsize = trellist.size();
                showmorecount=10;
                bnCount = trellist.size() <= 10;            
            }
            if(trellist.size()>0){
                for(task objtask :trellist){
                    objtask.CheckTask__c = true;
                }
            }
        //get the default values in Call recap and new task section after selcting the advisor from the Call queue//
            tskObj.Status='Completed';
            tskObj.WhoId=con.id;
            tskObj.Priority='Normal';
            tskobj.type='Outbound';
            tskobj.CallType='Outbound';
            //tskObj.OwnerId=null;
            tskObj.ActivityDate=System.today();
            tskObj.Subject='Prospecting Call';
            tskObj.Whatid=null;
            tskObj.Description=null;
            newtskobj.Status='Not Started';
            newtskobj.ownerid=userinfo.getUserid();
            newtskobj.WhoId=con.id;
            newtskobj.ActivityDate=System.today();
            newtskobj.subject=null;
            newtskobj.CallType='Outbound';
            newtskobj.type= 'Outbound';
            nxtcId=null;
        }
        catch(exception e){}        
    }
    public PageReference Calculate()
    {
        try  
        {  
        
           //Get the current time in seconds, minutes and hours//
           datetime myDate1 = datetime.now();
           D2=myDate1.second();
           D2Minits = myDate1.minute();   
           HoursSec = myDate1.hour();
          //Convert the current time in seconds// 
           Mintssec1 = (HoursSec*60*60)+(D2Minits*60) + D2;
           System.debug(' Mintssec1  Mintssec1  Mintssec1  Mintssec1  Mintssec1  Mintssec1'+ Mintssec1);
           //we already recorded time duration in the Condetail method when we select the advisor or start a call//
           system.debug('Get tehe time duration time duration......'+tskobj.Time_Duration__c);
           //get the duration of the call which we are recording in time duration field//
           if(mapOfContactToTask.get(cid)!= NULL)
              tskObj = mapOfContactToTask.get(cid);
          system.debug('Get tehe time duration time duration..After grtting cid....'+tskobj.Time_Duration__c);
          //get the call duration in seconds//
           tskobj.CallDurationInSeconds= Mintssec1-Integer.ValueOf(tskobj.Time_Duration__c);    
           tskobj.Time_Duration__c = Mintssec1;  
           system.debug('DURATION DURATION DURATION DURATION DURATION DURATION'+tskobj.Time_Duration__c );  
           system.debug('time Duration time Duration time Duration time Duration' + tskobj.CallDurationInSeconds); 
   
   
        }catch(exception e){}
        return null;
    }
    
        
    public void ShowMore()
    {
         try
         {
           showmorecount = showmorecount + 10;             
           if(showmorecount >= trellist.size())
             bnCount= true;
           
         }
           
          catch(exception e){}                       
    }
    
public string getDetailLink(){
string link;

//the id corresponds to the selector in the jQuery
link = '<a id="viewDetails" style="color:red; font-size: 10px;" href="#">Show More Details</a>';

return link; 
}*/
}