global with sharing class coleCaptialCFGAPIService implements cms.ServiceInterface {
        public static String JSONResponse {get; set;}
        private String action {get; set;}
        private Map<String, String> parameters {get; set;}
        private static coleCaptialCFGAPICtrl wpl = new coleCaptialCFGAPICtrl();
        private System.JSONGenerator response;
     global coleCaptialCFGAPIService() {
        
    }
    global System.Type getType(){
        return coleCaptialCFGAPIService.class;
    }
    global String executeRequest(Map<String, String> p) {
            
        this.action = p.get('action');
        this.parameters = p; 
        this.LoadResponse();    
        return JSONResponse;
    }
    global void loadResponse(){
        if (this.action == 'getcoleCaptialCFGAPIList'){
            getcoleCaptialCFGAPIList(parameters.get('pageName'));
        } else if (this.action == 'getsaveCartList'){
            getsaveCartList(parameters.get('cartDetails'));
        }else if(this.action=='getCartList'){
            getCartList();
        }else if(this.action=='removeItemFromCart'){
            removeItemFromCart(parameters.get('cartObjId'));
        }else if(this.action=='removeItemFromBookmarks'){
            removeItemFromBookmarks(parameters.get('bookmarkObjId'));
        }else if(this.action=='removeLiteratureList'){
            removeLiteratureList(parameters.get('listId'));
        }else if(this.action=='UpdateItemInCart'){
            UpdateItemInCart(parameters.get('updateCartValue'));
        }else if(this.action=='getBookMarkList'){
            addBookMarkList(parameters.get('bookMarkDetails'));
        }else if(this.action=='createLiteratureList'){
            createLiteratureList(parameters.get('literatureListName'));
        }else if(this.action=='renameLiteratureList'){
            renameLiteratureList(parameters.get('literatureListName'));
           // renameLiteratureList(parameters.get('literatureListName')); 
        }else if(this.action=='getBookmarkData'){
            getBookmarkData();
        }else if(this.action=='getBookMarks'){
            getBookMarks(parameters.get('listName'));
        }else if(this.action=='getContactData'){
            getContactData();
        }else if(this.action=='getSubmitOrder'){
            getSubmitCartOrder(parameters.get('jsonBody'));
        }else if(this.action=='getsendEmail'){
            getSendEmailList(parameters.get('emailListDetails'));
        }else if(this.action=='updateAllItemsInCart'){
            updateAllItemsInCart(parameters.get('updateAllCartValues'));
        }else if(this.action=='getBookmarkName'){
            getBookmarkName(parameters.get('withskuName'));
        }
        else if(this.action=='getBookmarkListsWithOutSKU'){
            getBookmarkListsWithOutSKU(parameters.get('withOutskuName'));
        }else if(this.action=='addToMultipleBookMarks'){
            addToMultipleBookMarks(parameters.get('bookMarkJson'));
        }
    }
      private static void removeItemFromCart(string cartObjId){
        integer cartCount = wpl.removeItemFromCart(cartObjId);
        JSONResponse = string.valueOf(cartCount);
        system.debug('removeItemFromCart'+JSONResponse );
    }
    private static void removeItemFromBookmarks(string bookmarkObjId){
        integer cartCount = wpl.removeItemFromBookmarks(bookmarkObjId);
        JSONResponse = string.valueOf(cartCount);
        system.debug('removeItemFromBookmarks'+JSONResponse );
    }
    private static void removeLiteratureList(string listId){
        integer cartCount = wpl.removeLiteratureList(listId);
        JSONResponse = string.valueOf(cartCount);
        system.debug('removeLiteratureList'+JSONResponse );
    }
     private static void UpdateItemInCart(string updateCartValue){
      system.debug('**********updateCartValue*************'+updateCartValue);
        String[] result = updateCartValue.split(',\\s*');
        integer updateItemCount  = wpl.UpdateItemInCart(result[0],result[1]);
         JSONResponse = string.valueOf(updateItemCount);
        system.debug('UpdateItemInCart'+JSONResponse );
    }
    private static void updateAllItemsInCart(string updateAllCartValues){
        integer updateAllItemCount  = wpl.updateAllItemsInCart(updateAllCartValues);
        JSONResponse = string.valueOf(updateAllItemCount);
        system.debug('updateAllCartValue'+JSONResponse );
    }
     private static void getCartList(){
        JSONResponse = JSON.serialize(wpl.getCartList());
        system.debug('getCartList'+JSONResponse );
    }
    private static void getBookmarkData(){
        JSONResponse = JSON.serialize(wpl.getBookmarkList());
        system.debug('getBookmarkList'+JSONResponse );
    }
     private static void getBookmarkName(string skuName){
        JSONResponse = JSON.serialize(wpl.getBookmarkListsWithSKU(skuName));
        system.debug('getBookmarkName'+JSONResponse );
    }
    private static void getBookmarkListsWithOutSKU(string skuName){
        JSONResponse = JSON.serialize(wpl.getBookmarkListsWithOutSKU(skuName));
        system.debug('getBookmarkListsWithOutSKU'+JSONResponse );
    }
     // @future(callout=true)
     private static void getsaveCartList(string cartDetails){
        string savedcartDetails = cartDetails;
        system.debug('**********savedcartDetails *************'+savedcartDetails);
        if(savedcartDetails!=null){
            integer maxorder = 0;
            integer quantity = 0;
            String[] result = savedcartDetails.split('\\*\\&');
            system.debug('string1233'+result[1]);
            system.debug('result[9]'+result[9]);
            if(result[1] != 'Dummy'){
            maxorder = integer.valueOf(result[3]);
            quantity = integer.valueOf(result[4]);
            }
            system.debug('result[10]'+result[10]);
            Boolean isCount = boolean.valueOf(result[10]); 
            integer cartCount; 
            cartCount = wpl.saveCart(result[0],result[1],result[2],maxorder,quantity,result[5],result[6],result[7],result[8],result[9],isCount);
            JSONResponse = string.valueOf(cartCount);
            system.debug('getsaveCartList'+JSONResponse );
        }

    }
    private static void addBookMarkList(string bookMarkDetails){
        string savedbookMarkDetails = bookMarkDetails;
        system.debug('**********savedbookMarkDetails *************'+savedbookMarkDetails);
        if(savedbookMarkDetails!=null){
            integer maxorder = 0;
            String[] result = savedbookMarkDetails.split('\\*\\&');  
            system.debug('result[10]'+result[10]);       
            maxorder = integer.valueOf(result[4]);
            Boolean isCount = boolean.valueOf(result[10]);
            integer bookmarkCount; 
            system.debug('result[7]'+result[7]);
            system.debug('result[9]'+result[9]);
            system.debug('result[8]'+result[8]);
            system.debug('result[10]'+result[10]);
            bookmarkCount = wpl.addToBookmark(result[0],result[1],result[2],result[3],maxorder,result[5],result[6],result[7],result[8],result[9],isCount);
            JSONResponse = string.valueOf(bookmarkCount);
            system.debug('getBookMarkList'+JSONResponse );
        }

    }
     private static void createLiteratureList(string litListname){
        system.debug('**********litListname *************'+litListname);
        
         JSONResponse = wpl.createList(litListname);
         //JSONResponse = string.valueOf(bookmarkCount);
         system.debug('getBookMarkList'+JSONResponse );
        }
     private static void renameLiteratureList(string litListname){
        system.debug('**********litListname *************'+litListname);
        String[] result = litListname.split('\\*\\&'); 
        JSONResponse = wpl.renameLiteratureList(result[0],result[1]);
         //JSONResponse = string.valueOf(bookmarkCount);
         system.debug('getBookMarkList'+JSONResponse );
        }

    
    private static void getBookMarks(string listName){
        //string bookMarkDetails = bookMarkDetails;
        String[] result = listName.split('\\*\\&'); 
        
        JSONResponse = JSON.serialize(wpl.getBookmarks(result[0],result[1]));
        system.debug('getBookmarks'+JSONResponse );
    }
    private static void getSubmitCartOrder(string jsonOrder){
        JSONResponse = JSON.serialize(wpl.submitOrder(jsonOrder));
      system.debug('submitOrder'+JSONResponse );
    }
    private static void getContactData(){
        JSONResponse = JSON.serialize(wpl.getContactData());
      system.debug('getContactData'+JSONResponse );
    }
    private static void getcoleCaptialCFGAPIList(string pageName){
      JSONResponse = wpl.coleCaptialCFGAPI(pageName);
      system.debug('getcoleCaptialCFGAPIList'+JSONResponse );
    }
     private static void addToMultipleBookMarks(string bookMarkJson){
     system.debug('entered addToMultipleBookMarks'+bookMarkJson );
      integer bookMarkCount; 
      String[] result = bookMarkJson.split('\\*\\&\\*'); 
      system.debug('entered addToMultipleBookMarks123 result[0]'+result[0]);
      bookMarkCount = wpl.addToMultipleBookmark(result[0],result[1]);
      system.debug('entered addToMultipleBookMarks result[0]'+result[0]);
      JSONResponse = string.valueOf(bookMarkCount);
      system.debug('addToMultipleBookMarks'+JSONResponse );
    }
     private static void getSendEmailList(string EmailListDetails){
      system.debug('**********EmailListDetails *************'+EmailListDetails);
     String[] result = EmailListDetails.split('\\*\\&');   
      JSONResponse = wpl.sendEmail(result[0],result[1],result[2]);
      system.debug('getSendEmailList'+JSONResponse );
    }
}