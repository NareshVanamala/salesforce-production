@isTest
private class Test_DefaultlookUp
{
   public static testMethod void TestforDafaultlookup () 
   {     
    date d= date.newInstance(Date.today().Year()+1,1,1);

    set<RecordType>recordtypeSet= new set<RecordType >();
    list<RecordType>recordtyprlist1= new list<RecordType >();
    list<Event_Automation_Request__c>Insertist= new list<Event_Automation_Request__c>();
    list<Event_Automation_Request__c>updatelist234= new list<Event_Automation_Request__c>();
    
    list<RecordType >recordtyprlist= new list<RecordType >();
    list<Event_Automation_Request__c>Insertistlist23= new list<Event_Automation_Request__c>();
    
    Id NewhireRequest=[Select id,name,DeveloperName from RecordType where DeveloperName='BrokerDealerPlatformRecordType' and SobjectType = 'Event_Automation_Request__c'].id;  
           
    Event_Automation_Request__c ear1= new Event_Automation_Request__c();
    ear1.recordtypeid= NewhireRequest;
    ear1.Start_Date__c=System.Today();
     insert ear1;
     ear1.Start_Date__c=d;
       update ear1;
    string eventype=  ear1.Event_Type__c;
    
  system.assertequals(ear1.recordtypeid,NewhireRequest);

    
    Id NewhireRequest1=[Select id,name,DeveloperName from RecordType where DeveloperName='Cole_Hosted_Rep_Training_Non_CE_RecordType' and SobjectType = 'Event_Automation_Request__c'].id;         
    Event_Automation_Request__c ear11= new Event_Automation_Request__c();
    ear11.recordtypeid= NewhireRequest;
    ear11.Start_Date__c=System.Today();
    insert ear11;
   ear11.Start_Date__c=d;
     update ear11;
    // insert Insertistlist23;
    
    Event_Automation_Request__c ear21= new Event_Automation_Request__c();
    ear21.recordtypeid= NewhireRequest1;
    ear21.Start_Date__c=System.Today();
    insert ear21;
     ear21.Start_Date__c=d;
     update ear21;
    
   Event_Automation_Request__c ear112= new Event_Automation_Request__c();
    ear112.recordtypeid= NewhireRequest1;
    ear112.Start_Date__c=System.Today();
    insert ear112;
   ear112.Start_Date__c=d;
     update ear112;
       
      system.assertequals(ear112.recordtypeid,NewhireRequest1);

    
   recordtyprlist=[Select id,name,DeveloperName from RecordType where SobjectType ='Event_Automation_Request__c' and (DeveloperName='Cole_Hosted_Rep_Training_Non_CE_RecordType' or DeveloperName='Cole_Hosted_Rep_Training_CE_RecordType' or DeveloperName='Employee_Management_Event_Record_Type' or DeveloperName='Employee_Event_Record_Type')];
   recordtypeSet.addAll(recordtyprlist);
   recordtyprlist1.addAll(recordtypeSet);
   
      for(RecordType ret:recordtyprlist1)
      {
        Event_Automation_Request__c ear= new Event_Automation_Request__c();
        ear.recordtypeid= ret.id;
        ear.Start_Date__c=System.Today();
        Insertist.add(ear);

     }
                  
     insert Insertist; 
     date d2= date.newInstance(Date.today().Year()+1,1,1);
     for(Event_Automation_Request__c ear:Insertist)
      ear.Start_Date__c=d2;
              
    }

}