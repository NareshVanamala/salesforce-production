@isTest
Public class LC_ReminderSubmitemail_Test
{
   static testmethod void testCreatelease()
  {
     test.starttest();
       MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.name='Test_MRI_Property';
         mc.Property_ID__c='123ABC789123456789';
         insert mc; 
         system.debug('This is my MRI Property'+mc);
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU';
         mylease.Suite_Sqft__c=1234;
         mylease.Lease_ID__c='A046677';
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         system.debug('This is my Lease'+mylease);


        
         User u=[select id,name,Territory__c,isactive from User where isactive=true limit 1];
         //u.firstName='Testing';
         //u.lastname='Approval';
         u.email='skalamkar@colecapital.com';
       // update u;
             
            
         list<LC_LeaseOpprtunity__c> lc1list= new list<LC_LeaseOpprtunity__c>();
         LC_LeaseOpprtunity__c lc1= new LC_LeaseOpprtunity__c();
         lc1.name='TestOpportunity';
         lc1.Lease_Opportunity_Type__c='Lease - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.LC_OtherOpportunity_Description__c='Thisis my description';
         lc1.Base_Rent__c=12345;   
         lc1.Milestone__c='Approval Pending';
         lc1.Current_Approver__c=u.name;
         lc1.Current_Approver_1__c=u.id;
         lc1.Current_Approver_2__c=u.id;
         lc1.Current_Approver_3__c=u.id;
         lc1.Current_Approver_4__c=u.id;
         lc1.Current_Approver_5__c=u.id;
         lc1.Current_Approver_6__c=u.id;
 
         lc1list.add(lc1);
         insert lc1list;
         
         System.assertEquals(myLease.MRI_PROPERTY__c , mc.id);
         System.assertEquals(u.Id,lc1.Current_Approver_1__c);
         System.assertEquals( lc1.name,'TestOpportunity');
               
          LC_ReminderSubmitemail np = new LC_ReminderSubmitemail ();
          Database.BatchableContext bc;
          np.start(bc);
           np.execute(bc,lc1list);
         np.finish(bc);
         
 
      test.stoptest();
 
 }
}