global class UpdateWebproperties implements Database.Batchable<SObject>,Database.Stateful
{
    global String Query;  
    global Database.QueryLocator start(Database.BatchableContext BC)  
    {  
        Query= 'select id from Web_Properties__c ';     
        system.debug('Query' + Query);
        return Database.getQueryLocator(Query);  
    }
    global void execute(Database.BatchableContext BC,List<Web_Properties__c> scope)  
    {  
         list<Web_Properties__c>webpropertylist= new list<Web_Properties__c>();
          for(Web_Properties__c Acl : scope)
         {
               webpropertylist.add(Acl);
         }
         
         update webpropertylist;
    }
    global void finish(Database.BatchableContext BC)  
    { 
     }
 }