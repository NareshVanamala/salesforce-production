public class clearShoppingCartAndBookmarkItems{
//system.debug('ContactIds'+ContactIds);
@InvocableMethod
public static void deleteShoppingCartAndBookmarkItems(List<Id> ContactIds){
  system.debug('ContactIds'+ContactIds);
  List<Forms_Literature_List__c> formLitLists= [select id from Forms_Literature_List__c where Contact__c in:ContactIds];
  system.debug('formLitLists'+formLitLists);
  if(formLitLists.size()>0){
  delete formLitLists;
  }
  List<Forms_Literature_Shopping_Cart__c> shoppingCartItems= [select id from Forms_Literature_Shopping_Cart__c where Contact__c in:ContactIds];
  if(shoppingCartItems.size()>0){
  delete shoppingCartItems;
  }
}
}