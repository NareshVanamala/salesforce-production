@isTest
Public class RC_UpdateAppraisaldate_Test
{
  static testmethod void testUpdatePropertyFinal1Class()
  {
        
        Portfolio__c pc= new Portfolio__c();
        pc.name='Testing1';
        Insert pc;
        
        RC_Risk_Portfolio__c RPC = new RC_Risk_Portfolio__c();
        RPC.name='Testing1';
        RPC.Related_Portfolio__c=pc.id;
        Insert RPC;
                
        Deal__c d1 = new Deal__c();
        d1.Name ='Test13'; 
        d1.Portfolio_Deal__c=pc.id;  
        d1.Build_to_Suit_Type__c ='Current';
        d1.Estimated_COE_Date__c = date.today();
        d1.Targeted_COE_Date__c  = date.today();
        d1.HVAC_Warranty_Status__c ='Received';
        d1.Ownership_Interest__c = 'Fee Simple';
        d1.Roof_Warranty_Status__c = 'Received';
        d1.Zip_Code__c = '98033';
        d1.Create_Workspace__c =true; 
        d1.Deal_Status__c='Signed LOI';
        d1.Division__c='ST Retail';
        d1.LOI_Signed_Date__c=system.today();
        d1.MRI_Property_Type_picklist__c='Freestanding Retail';
        d1.Number_of_Floors__c='3';
        d1.GAAP_NOI__c=13.8;
        d1.Ownership_Type__c='TIC';
        d1.Primary_Use__c='Call Center';
        d1.Property_Location__c='Suburban [OID Only]';
        d1.Property_Sub_Type__c='Big Box Retail';
       
        insert d1 ;
          
        Template__c temprec = new Template__c();
        temprec.Name = 'Template';
        temprec.deal__c=d1.id;
        insert temprec;      
        
        
        list< Task_Plan__c>tplist=new list< Task_Plan__c>();
        Task_Plan__c tp = new Task_Plan__c ();
        tp.Name='appraisal';
        tp.priority__c = 'Medium';
        tp.Index__c = 1;
        tp.Assigned_To__c = Userinfo.getUserId();
        tp.Template__c =temprec.Id ; 
        tp.Date_Received__c=date.today();
        tp.Date_Needed__c=date.today();
        tplist.add(tp);
        
        Task_Plan__c tp1 = new Task_Plan__c ();
        tp1.Name='Property Condition Report';
        tp1.priority__c = 'Medium';
        tp1.Index__c = 1;
        tp1.Assigned_To__c = Userinfo.getUserId();
        tp1.Template__c =temprec.Id ; 
        tp1.Date_Received__c=date.today();
        tp1.Date_Needed__c=date.today();
        tplist.add(tp1);
  
        Task_Plan__c tp2 = new Task_Plan__c ();
        tp2.Name='Roof Report';
        tp2.priority__c = 'Medium';
        tp2.Index__c = 1;
        tp2.Assigned_To__c = Userinfo.getUserId();
        tp2.Template__c =temprec.Id ; 
        tp2.Date_Received__c=date.today();
        tp2.Date_Needed__c=date.today();
        tplist.add(tp2);
  
        Task_Plan__c tp3 = new Task_Plan__c ();
        tp3.Name='Copy of CC&Rs saved and distributed';
        tp3.priority__c = 'Medium';
        tp3.Index__c = 1;
        tp3.Assigned_To__c = Userinfo.getUserId();
        tp3.Template__c =temprec.Id ; 
        tp3.Date_Received__c=date.today();
        tp3.Date_Needed__c=date.today();
        tplist.add(tp3);
        
        Task_Plan__c tp4 = new Task_Plan__c ();
        tp4.Name='Environmental Report';
        tp4.priority__c = 'Medium';
        tp4.Index__c = 1;
        tp4.Assigned_To__c = Userinfo.getUserId();
        tp4.Template__c =temprec.Id ; 
        tp4.Date_Received__c=date.today();
        tp4.Date_Needed__c=date.today();
        tplist.add(tp4);
         insert tplist;
  
       
  
  
  
  }
  
  
}