global with sharing class ocms_propertiesService implements cms.ServiceInterface {

    public String JSONResponse {get; set;}
    private String action {get; set;}
    private Map<String, String> parameters {get; set;}
    private ocms_arcp_properties  wpl = new ocms_arcp_properties();

    global ocms_propertiesService() {
        
    }

    global System.Type getType(){
        return ocms_propertiesService.class;
    }

    global String executeRequest(Map<String, String> p) {
        this.action = p.get('action');
        this.parameters = p; 
        this.LoadResponse();    
        return this.JSONResponse;
    }

    global void loadResponse(){
        if (this.action == 'getIndustryList'){
            this.getIndustryList();
        } else if (this.action == 'getTenantList'){
            this.getTenantList();
        } else if (this.action == 'getStateList'){
            this.getStateList();
        } else if (this.action == 'getCityList'){
            this.getCityList(parameters.get('state'));
        } else if (this.action == 'getResultTotal'){

            String state = parameters.get('state');
            String city = parameters.get('city');
            String tenantIndustry = parameters.get('tenantIndustry');
            String tenant = parameters.get('tenant');
            String[] filters = new List<String>();

            if(parameters.get('filterRetail') != null && parameters.get('filterRetail') != ''){
                filters.add(parameters.get('filterRetail'));
            }
            if(parameters.get('filterMultiTenant') != null && parameters.get('filterMultiTenant') != ''){
                filters.add(parameters.get('filterMultiTenant'));
            }
            if(parameters.get('filterOffice') != null && parameters.get('filterOffice') != ''){
                filters.add(parameters.get('filterOffice'));
            }
            if(parameters.get('filterIndustrial') != null && parameters.get('filterIndustrial') != ''){
                filters.add(parameters.get('filterIndustrial'));
            }
            if(parameters.get('filterOther') != null && parameters.get('filterOther') != ''){
                filters.add(parameters.get('filterOther'));
            }

            this.getResultTotal(state, city, tenantIndustry, tenant, filters);

        } else if (this.action == 'getPropertyList'){

            String state = parameters.get('state');
            String city = parameters.get('city');
            String tenantIndustry = parameters.get('tenantIndustry');
            String tenant = parameters.get('tenant');
            String[] filters = new List<String>();
            String sortBy = parameters.get('sortBy');
            String rLimit = parameters.get('limit');
            String offset = parameters.get('offset');

            if(parameters.get('filterRetail') != null && parameters.get('filterRetail') != ''){
                filters.add(parameters.get('filterRetail'));
            }
            if(parameters.get('filterMultiTenant') != null && parameters.get('filterMultiTenant') != ''){
                filters.add(parameters.get('filterMultiTenant'));
            }
            if(parameters.get('filterOffice') != null && parameters.get('filterOffice') != ''){
                filters.add(parameters.get('filterOffice'));
            }
            if(parameters.get('filterIndustrial') != null && parameters.get('filterIndustrial') != ''){
                filters.add(parameters.get('filterIndustrial'));
            }
            if(parameters.get('filterOther') != null && parameters.get('filterOther') != ''){
                filters.add(parameters.get('filterOther'));
            }

            this.getPropertyList(state, city, tenantIndustry, tenant, filters, sortBy, rLimit, offset);
            
        }
    }

    private void getResultTotal(String state, String city, String tenantIndustry, String tenant, String[] filters){
        this.JSONResponse = JSON.serialize(wpl.getResultTotal(state, city, tenantIndustry, tenant, filters));
    }

    private void getPropertyList(String state, String city, String tenantIndustry, String tenant, String[] filters, String sortBy, String rLimit, String offset){
        this.JSONResponse = JSON.serialize(wpl.getPropertyList(state, city, tenantIndustry, tenant, filters, sortBy, rLimit, offset));
    }

    private void getIndustryList(){
        this.JSONResponse = JSON.serialize(wpl.getIndustryList());
    }

    private void getTenantList(){
        this.JSONResponse = JSON.serialize(wpl.getTenantList());
    }

    private void getStateList(){
        this.JSONResponse = wpl.getStateList();
    }

    private void getCityList(String state){
        this.JSONResponse = JSON.serialize(wpl.getCityList(state));
    }

}