public with sharing class MassApplyActionPlan
{
      private Double taskIndex   { get; set; }
      public Boolean  addedTask  { get; set; }
      public Boolean applyActionPlan{get;set;}
      public String smail{get;set;}
      public String templateSelected{get;set;}
      
      public String ids {get;set;}
      public String id {get;set;}
      public List<String> strs = new List<String>();
      public List<String> selectedDealIds = new List<String>();
     
      public List<SelectOption> getRelatedObjectPicklist()
      {
          List<SelectOption> options = new List<SelectOption>();
          options.add(new SelectOption('Deal__c','Deal'));
          options.add(new SelectOption('Loan__c','Loan'));
          return options;
     }
     public Template__c actionPlan{get;set;}
     public Boolean massAssign{get;set;}
     public String objectName{get;set;}
     public Url url{get;set;}
     public Map<Integer,Task_Plan__c> mapTaskOrder{get;set;}
   //public Map<id,Task_Plan__c> mapTaskOrder{get;set;}
     public integer count;
     public List<Task_Plan__c> actionPlanTasks{get;set;}
     public List<Task_Plan__c> actionPlansubTasks{get;set;}
     public List<Integer> RollCnt{get;set;}
     List<Template__c> templatelist= new List<Template__c>();
    
    public Map<Integer,List<Task_Plan__c>> mapTask{get;set;}
    private ApexPages.StandardController stdController;
    public String duplicateTasksString{get;set;}
   
    public MassApplyActionPlan(ApexPages.StandardController controller)
    { 
        actionPlan= new Template__c();
        if(apexpages.currentpage().getparameters().get('id')==null){
            getarrangetask();    
        }
        
        applyActionPlan=false;
        duplicateTasksString='';
       
        //pre-populate 'Tempate -' on load
        this.actionPlan= (Template__c)controller.getRecord(); 
        
        actionPlan.Name = 'Action Plan -';        
        stdcontroller=controller; 
        mapTaskOrder = new map<Integer,Task_Plan__c>();
        count=0;
        //smail=apexpages.currentpage().getparameters().get('objectId');
        //actionPlan.Deal__c =smail;
        //system.debug('get me the deal id'+smail);
        
        ids = ApexPages.currentPage().getParameters().get('ids');
        selectedDealIds = ids.split(',');
        id = ApexPages.currentPage().getParameters().get('id');
    }
    
     public List<String> strDealIds = new List<String>();
     //--Display the selected deals under Related To
     public List<Deal__c> getDeals()
     {
        List<Deal__c> dls =  [Select Id ,Portfolio_Deal__c, Name from Deal__c where Id =:selectedDealIds ] ;
        system.debug('get the list of the deals..'+dls );
          for(Deal__c d : dls)
          {
            strDealIds.add(d.Id);
          }
          return dls ;
     }
     
    public void checkDuplicateTasks(){}
    
    public List<SelectOption> getdateFields()
    {
        /*List<SelectOption> piclistValue=new List<SelectOption>();
        String OldType;
        if(OldType!=actionPlan.RelatedObjectName__c && actionPlan.RelatedObjectName__c!=null){
        }
        if(actionPlan.RelatedObjectName__c==null){actionPlan.RelatedObjectName__c='Deal__c';}
         try
          {
            //piclistValue.add(new selectoption('None', 'None'));
            if(actionPlan.RelatedObjectName__c == 'Deal__c')
            {
                for(Schema.SObjectField F : Schema.SObjectType.Deal__c.fields.getMap().values())
                {
                    if(String.valueof(f.getDescribe().getType())=='Date'){
                    piclistValue.add(new selectoption(f.getDescribe().getName(), f.getDescribe().getLabel()));
                }
             }
           }
            else if(actionPlan.RelatedObjectName__c == 'Loan__c'){
            for(Schema.SObjectField F : Schema.SObjectType.Loan__c.fields.getMap().values()) {
                if(String.valueof(f.getDescribe().getType())=='Date'){
                    piclistValue.add(new selectoption(f.getDescribe().getName(), f.getDescribe().getLabel()));
                }
            }
        }
        }
        catch(exception e){}
        OldType=actionPlan.RelatedObjectName__c;
        return piclistValue; */ 
        return null;      
    }
    
    public pagereference CalldateFields(){        
        getdateFields();
        return null;
    }
         
     Task_Plan__c TP;     
     public void getarrangetask(){
      system.debug('I am in arrangetask method');
        if(apexpages.currentpage().getparameters().get('id')==null){
            actionPlanTasks = new List<Task_Plan__c>();
            mapTask = new Map<Integer,List<Task_Plan__c>>();
            mapTask.put(0, new List<Task_Plan__c>());
            mapTask.get(0).add(new Task_Plan__c());
            
            mapTask.put(1, new List<Task_Plan__c>());
            mapTask.get(1).add(new Task_Plan__c());
            
            
            actionPlanTasks = new List<Task_Plan__c>();
            actionPlanTasks.add(new Task_Plan__c(Index__c=0));
            actionPlanTasks.add(new Task_Plan__c(Index__c=1)); 
            
            RollCnt = new List<Integer>();
            RollCnt.add(1);
        }
    }
     public pagereference addTask(){  
        system.debug('chek the count value');  
        TP = new Task_Plan__c();
        TP.Priority__c='Medium';
        TP.Index__c=actionPlanTasks.size()+1;
        System.debug('**Index' + TP.Index__c);
        TP.Name = 'Task'+(TP.Index__c);
        mapTask.put(actionPlanTasks.size(), new List<Task_Plan__c>());
        mapTask.get(actionPlanTasks.size()).add(new Task_Plan__c());
        //mapTaskorder.get(actionPlanTasks.size()).add(new Task_Plan__c())
        //TP.Name=String.Valueof(actionPlanTasks.size());
        system.debug('%%%'+Rollcnt.size());
        RollCnt.add(RollCnt.size()+1);
        actionPlanTasks.add(TP);
       
        mapTaskorder.put(count, TP);
        
        count++;
        system.debug('Check the map-----'+mapTaskorder); 
        system.debug('Check the map-----'+mapTask);   
        return null;
       }
    
    public pagereference addSubTask()
    {
        Map<Id, Integer> IdIntMap = new Map<Id, Integer>();
        RollCnt = new List<Integer>();
        system.debug('check the subtasks list..'+actionPlansubTasks);
        system.debug('check the tasks also..'+actionPlanTasks); 
        /* if(actionPlanTasks.size()>0)
        {
            for(Task_Plan__c sobj :actionPlanTasks)
            { 
                system.debug('@@@@@@@'+count + sobj);
                mapTaskorder.put(count,sobj);
                count=count+1;
                IdIntMap.put(sobj.id, integer.valueof(sobj.Index__c)); 
                RollCnt.add(integer.valueOf(sobj.Index__c)); 
            }
        }*/
        Integer inst;
        TP=new Task_Plan__c();
        if(actionPlansubTasks==null){actionPlansubTasks=new List<Task_Plan__c>();}
        actionPlansubTasks.add(TP);
        Integer ParentIndex=Integer.Valueof(apexpages.currentpage().getparameters().get('parentindex'));
        system.debug('Give me ParentIndex..'+ParentIndex);
//if(ParentIndex >=1)
// {
// ParentIndex=ParentIndex-1;
//}
/* else if(ParentIndex ==1){
ParentIndex=ParentIndex;
}*/
system.debug('Now check the subtasks list..'+actionPlansubTasks);
system.debug('get me the maptask..'+mapTask);
if(!mapTask.ContainsKey(ParentIndex-1)){
system.debug('Hey i am in the if loop');
mapTask.put(ParentIndex-1, new List<Task_Plan__c>());
mapTask.get(ParentIndex-1).add(new Task_Plan__c(Index__c=0));
}
else{
system.debug('Hey i am in the else loop');
Integer countrec = mapTask.get(ParentIndex-1).size();
system.debug('Check the countrec..'+countrec);
mapTask.get(ParentIndex-1).add(new Task_Plan__c(Index__c=countrec+1,Name='SubTask'+countrec,Priority__c='Medium'));
}
// mapTask=new map<Integer,list<Task_Plan__c>>();
/* for(Task_Plan__c T:actionPlansubTasks)
{ 
inst=Integer.valueof(IdIntMap.get(T.parent__c));
if(mapTask.Containskey(inst)){
mapTask.get(inst).add(T);
}
else{
mapTask.put(inst, new List<Task_Plan__c>());
mapTask.get(inst).add(T);
}

}*/
system.debug('Now check the map for subtask index..'+mapTask);
return null;

     }
     
     public PageReference Save()
    {        
       system.debug('get me the action plan'+actionPlan.deal__c);
       list<deal__c>updateddeals= new list<deal__c>();
       map<id,Template__c>dealActionplanmap= new map<id,Template__c>();
       list<Template__c>actionplanlist= new list<Template__c>();
       set<id>actionplanid= new set<id>();
       List<Deal__c> dls =  [Select Id ,Portfolio_Deal__c, Name from Deal__c where Id =:selectedDealIds ] ;
       system.debug('get the list of the deals..'+dls );
       Template__c Tempct= new Template__c();
       Tempct=[select id from Template__c where name =:templateSelected Limit 1];
        list<Template__c >insrtlist= new list<Template__c >();
       // actionPlan.Template__c =Tempct.id;
       id userid= Userinfo.getUserid();

        for(Deal__c currentdeal :dls)
        {
          Template__c actionPlan1= new Template__c ();
          actionPlan1.name=actionPlan.name;
          actionPlan1.Send_Email__c=actionPlan.Send_Email__c;
          if(actionPlan.Mass_Assign_To__c==null)
          actionPlan1.Mass_Assign_To__c=userid;
          else if(actionPlan.Mass_Assign_To__c!=null)
          actionPlan1.Mass_Assign_To__c=actionPlan.Mass_Assign_To__c;
          actionPlan1.Deal__c=currentdeal.id;
          actionPlan1.Template__c =Tempct.id;
          insrtlist.add(actionPlan1);
        }
        insert insrtlist;
        
        for(Template__c ct:insrtlist)
          dealActionplanmap.put(ct.Deal__c,ct);
         system.debug('check my special Map..'+dealActionplanmap);
      
        //upsert actionPlan;
        system.debug('Check the action plan after creation now..'+actionPlan.deal__c);
        system.debug('check the task plans..'+actionPlanTasks);
        system.debug('get all the subtasks..'+actionPlansubTasks);
        LIST<Task_Plan__c> MainTaskPlans = new LIST<Task_Plan__c>();
        map<Integer,id>actplantaskidindexmap= new map<Integer,id>();
        map<id,Integer>maintaskplanintegertoid= new map<id,Integer>();
       for(Deal__c currentdeal :dls)
      {
         for(Task_Plan__c T:actionPlanTasks)
        {
            Task_Plan__c ct= new Task_Plan__c();
            ct.create_task__c=true;
           // if(T.Assigned_To__c!=null)
           
            //ct.Assigned_To__c=T.Assigned_To__c;
            //else
           // ct.Assigned_To__c=userid;
           If(ct.Template__r.Mass_Assign_To__c!=null)
            {
               if(actionplan.Mass_Assign_To__c!=null)
               {
                ct.Assigned_TO__c = actionplan.Mass_Assign_To__c;
                }
                else
                {
                ct.Assigned_TO__c =ct.Template__r.Mass_Assign_To__c;
                }
                
            }
             If(ct.Template__r.Mass_Assign_To__c==null)
             {
             if(actionplan.Mass_Assign_To__c!=null)
             
             {
              ct.Assigned_TO__c = actionplan.Mass_Assign_To__c;
             }
             else
             {
             ct.Assigned_To__c=Userinfo.getUserId();
             }
             }  
            ct.Template__c =dealActionplanmap.get(currentdeal.id).id;
            ct.name=T.name;
            ct.Date_Needed__c=T.Date_Needed__c;
            ct.Date_Ordered__c=T.Date_Ordered__c;
            If(T.Date_Received__c!=null)
            {
                ct.Date_Received__c=T.Date_Received__c;
                ct.Status__c='Completed';
                
            }    
            ct.Index__c =T.Index__c ;
            ct.Field_to_update_Date_Received__c = T.Field_to_update_Date_Received__c;
            ct.Field_to_update_for_date_needed__c= T.Field_to_update_for_date_needed__c ;
            ct.Notify_Emails__c =T.Notify_Emails__c ;
            ct.Priority__c=T.Priority__c;
            ct.Comments__c=T.Comments__c;
            ct.Date_Completed__c=T.Date_Completed__c;
           //this code is for updating the deal field 
            If(T.Date_Received__c!=null && T.Field_to_update_Date_Received__c!=null) 
            { 
              if(T.Field_to_update_Date_Received__c=='Site_Visit_Date__c')
               currentdeal.Site_Visit_Date__c=T.Date_Received__c; 
                If(T.Field_to_update_Date_Received__c=='Accenture_Due_Date__c')
                  currentdeal.Accenture_Due_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Go-Hard Date')
             currentdeal.Go_Hard_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Rep_Burn_Off__c')
             currentdeal.Rep_Burn_Off__c=T.Date_Received__c;
            //if(T.Field_to_update_Date_Received__c=='Site_Visit_Date__c')
            //currentdeal.Dead_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Passed_Date__c')
             currentdeal.Passed_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Estimated_COE_Date__c')
             currentdeal.Estimated_COE_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Hard_Date__c')
             currentdeal.Hard_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Estimated_SP_Date__c')
             currentdeal.Estimated_SP_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Purchase_Date__c')
             currentdeal.Purchase_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Estimated_Hard_Date__c')
             currentdeal.Estimated_Hard_Date__c=T.Date_Received__c;  
            if(T.Field_to_update_Date_Received__c=='Contract_Date__c')
             currentdeal.Contract_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Closing_Date__c')
             currentdeal.Closing_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='LOI_Signed_Date__c')
             currentdeal.LOI_Signed_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='LOI_Sent_Date__c')
             currentdeal.LOI_Sent_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Lease_Abstract_Date__c')
             currentdeal.Lease_Abstract_Date__c=T.Date_Received__c; 
             // if(T.Field_to_update_Date_Received__c=='Lease_Expiration__c')
             //currentdeal.Lease_Expiration__c=T.Date_Received__c;
              if(T.Field_to_update_Date_Received__c=='Open_Escrow_Date__c')
             currentdeal.Open_Escrow_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Purchase_Date__c')
             currentdeal.Purchase_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Tenant_ROFR_Waiver__c')
             currentdeal.Tenant_ROFR_Waiver__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Date_Audit_Completed__c')
             currentdeal.Date_Audit_Completed__c=T.Date_Received__c; 
             if(T.Field_to_update_Date_Received__c=='Date_Identified__c')
             currentdeal.Date_Identified__c=T.Date_Received__c; 
             if(T.Field_to_update_Date_Received__c=='Investment_Committee_Approval__c')
             currentdeal.Investment_Committee_Approval__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Part_1_Start_Date__c')
             currentdeal.Part_1_Start_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Estoppel__c')
             currentdeal.Estoppel__c=T.Date_Received__c;  
              if(T.Field_to_update_Date_Received__c=='nd_Estimated_SP_Date__c')
             currentdeal.nd_Estimated_SP_Date__c=T.Date_Received__c; 
              if(T.Field_to_update_Date_Received__c=='nd_SP_End_Date__c')
             currentdeal.nd_SP_End_Date__c=T.Date_Received__c; 
             } 

            If(T.Date_Needed__c!=null && T.Field_to_update_for_date_needed__c!=null) 
            { 
              if(T.Field_to_update_for_date_needed__c=='Estimated_COE_Date__c')
               currentdeal.Estimated_COE_Date__c=T.Date_Needed__c;   
              if(T.Field_to_update_for_date_needed__c=='Dead_Date__c')
             currentdeal.Dead_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Appraisal_Date__c')
             currentdeal.Appraisal_Date__c=T.Date_Needed__c;
            If(T.Field_to_update_for_date_needed__c=='Accenture_Due_Date__c')
             currentdeal.Accenture_Due_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Go-Hard Date')
             currentdeal.Go_Hard_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Rep_Burn_Off__c')
             currentdeal.Rep_Burn_Off__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Site_Visit_Date__c')
            currentdeal.Dead_Date__c=T.Date_Needed__c;
           // if(T.Field_to_update_for_date_needed__c=='Query_Sorted_Date__c')
            // currentdeal.Query_Sorted_Date__c=T.Rep_Burn_Off__c;
            if(T.Field_to_update_for_date_needed__c=='Passed_Date__c')
             currentdeal.Passed_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Hard_Date__c')
             currentdeal.Hard_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Estimated_SP_Date__c')
             currentdeal.Estimated_SP_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Purchase_Date__c')
             currentdeal.Purchase_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Estimated_Hard_Date__c')
             currentdeal.Estimated_Hard_Date__c=T.Date_Needed__c;  
            if(T.Field_to_update_for_date_needed__c=='Contract_Date__c')
             currentdeal.Contract_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Closing_Date__c')
             currentdeal.Closing_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='LOI_Signed_Date__c')
             currentdeal.LOI_Signed_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='LOI_Sent_Date__c')
             currentdeal.LOI_Sent_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Lease_Abstract_Date__c')
             currentdeal.Lease_Abstract_Date__c=T.Date_Needed__c;  
             // if(T.Field_to_update_for_date_needed__c=='Lease_Expiration__c')
             //currentdeal.Lease_Expiration__c=T.Date_Needed__c;
              if(T.Field_to_update_for_date_needed__c=='Open_Escrow_Date__c')
             currentdeal.Open_Escrow_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Purchase_Date__c')
             currentdeal.Purchase_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Tenant_ROFR_Waiver__c')
             currentdeal.Tenant_ROFR_Waiver__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Date_Audit_Completed__c')
             currentdeal.Date_Audit_Completed__c=T.Date_Needed__c; 
             if(T.Field_to_update_for_date_needed__c=='Date_Identified__c')
             currentdeal.Date_Identified__c=T.Date_Needed__c; 
             if(T.Field_to_update_for_date_needed__c=='Investment_Committee_Approval__c')
             currentdeal.Investment_Committee_Approval__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Part_1_Start_Date__c')
             currentdeal.Part_1_Start_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Estoppel__c')
             currentdeal.Estoppel__c=T.Date_Needed__c; 
             if(T.Field_to_update_for_date_needed__c=='nd_Estimated_SP_Date__c')
             currentdeal.nd_Estimated_SP_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='nd_SP_End_Date__c')
             currentdeal.nd_SP_End_Date__c=T.Date_Needed__c;
         


             } 




            MainTaskPlans.add(ct);
         }
          updateddeals.add(currentdeal);
        
      }  
        insert MainTaskPlans;
        update updateddeals;
        System.debug('****' + MainTaskPlans);
        System.debug('Check the Map<<<<'+actplantaskidindexmap);
        list<Task_Plan__c>ctlist= new list<Task_Plan__c>();
        list<Task_Plan__c>inertsubctlist= new list<Task_Plan__c>();
        for(Task_Plan__c ctask :MainTaskPlans)
        {
          system.debug('Get me the index..'+ctask.Index__c);
          ctlist=mapTask.get(Integer.valueOf(ctask.Index__c-1)); 
          system.debug('show me the list>>>>>>'+ctlist);
          if(ctlist.size()>0)
          {
           for(Task_Plan__c csubtask :ctlist)
           {
             if(csubtask.name!=null)
             {
             Task_Plan__c subtask1= new Task_Plan__c();
             subtask1.Parent__c=ctask.id;
             subtask1.name=csubtask.name;
             subtask1.Template__c = ctask.Template__c;
             subtask1.Assigned_To__c=ctask.Assigned_To__c;
             subtask1.Index__c = csubtask .Index__c ;
             subtask1.create_task__c=true; 
             inertsubctlist.add(subtask1);
            }
           }
         }
        }
        
       insert inertsubctlist;
         //maintaskplanintegertoid.put(ct.id,Integer.valueOf(ct.Index__c));
        /*for(Task_Plan__c ct:actionPlansubTasks)
        {
            decimal index = ct.Parent__r.Index__c;
            integer Index1=integer.valueOf(index);
            id myid= actplantaskidindexmap.get(index1);
            Task_Plan__c ct1= new Task_Plan__c();
            //ct1.Parent__c= maintaskplanintegertoid.get(myid);        
           
         
        }*/
       /* map<Integer,Id>MapofIndextoId= new map<Integer,Id>();
        LIST<Task_Plan__c> MapTaskPlans = new LIST<Task_Plan__c>();
        for(Task_Plan__c tct:MainTaskPlans)
         MapofIndextoId.put(Integer.valueOf(tct.Index__c),tct.id);
        for(Task_Plan__c T:MainTaskPlans){
            if(mapTask.ContainsKey(Integer.valueof(T.Index__c-1))){
                for(Task_Plan__c STP: mapTask.get(Integer.valueof(T.Index__c-1))){
                                 
                        Task_Plan__c ct1= new Task_Plan__c();
                        ct1.Template__c = actionPlan.id;
                        ct1.name = STP.name;
                        ct1.Index__c=STP.index__c;
                        if(STP.Index__c!=null)
                         ct1.Parent__c = MapofIndextoId.get(integer.valueOf(STP.Index__c-1));
                        if(STP.Index__c!=null){
                       // MapTaskPlans.add(STP);
                    //}
                    MapTaskPlans.add(ct1);
                    }
                }
            }*/
        //}
        
        //system.debug('check the maptask'+mapTask);
       //for(Task_Plan__c ct:)
       
        //upsert MapTaskPlans;
        //insert MapTaskPlans;
        //system.debug('check the subtasks..'+MapTaskPlans);
        if(dls.size()>0)
        {
        PageReference pref = new PageReference('/'+dls[0].Portfolio_Deal__c);
        return pref;
        }
        return null;
     }
     
     Public void RemoveTask()
    {
        Integer Indexcnt = Integer.valueof(apexpages.currentpage().getparameters().get('removeRec'));
        System.debug('^^^index' + Indexcnt);
       //system.debug('Check the task..'+actionPlanTasks.get(Indexcnt));
       if(Indexcnt>=1)
       {
          Indexcnt=Indexcnt-1;
       }
       List<Task_Plan__c> TPL=new List<Task_Plan__c>();
        actionPlanTasks.remove(Indexcnt);
        system.debug('check the task after removing the index..'+actionPlanTasks);
        //Integer rccnt= Integer.valueof(apexpages.currentpage().getparameters().get('RCCount'));
        //System.debug('^^^rc' + rccnt);
        /*if(rccnt!=null){
         for(integer rc=0; rc<=RollCnt.size()-1; rc++){
         if(RollCnt[rc]==rccnt){RollCnt.remove(rc);}
          }
          }*/
       count=0;
         mapTaskorder= new map<integer,Task_Plan__c>();
        for(Task_Plan__c Tc:actionPlanTasks)
         mapTaskorder.put(count++,Tc);
        system.debug('check the actionplantasks'+actionPlanTasks); 
         //system.debug('check the map'+maptask) ;
         //maptask.remove(Indexcnt);
          system.debug('check the map'+maptask) ;
     }
    
    
    Public void removesubTask()
    {
     
      system.debug('Can i check the task map.'+maptask);
      system.debug('Welcome to Removesubtask method');
      Integer ParenTask=integer.valueOf(apexpages.currentpage().getparameters().get('SubtaskParent'));
      system.debug('Tell me the parent of subtask'+ParenTask);
      Integer SubtTaskindex=integer.valueOf(apexpages.currentpage().getparameters().get('SubtaskIndex')); 
      system.debug('Tell me the Index of subtask'+SubtTaskindex);
      system.debug('Can i check the task map.'+maptask);
      list<Task_Plan__c>subtasklist=new list<Task_Plan__c>();
      subtasklist=maptask.get(ParenTask);
      system.debug('check the list..'+subtasklist);
      subtasklist.remove(SubtTaskindex-1);
      system.debug('Now check the Map'+maptask);
    }
    public List<Task_Plan__c> actionPlanTasksList{get;set;}
    public List<Task_Plan__c> mapTaskList{get;set;}
    
    public pagereference nutralact(){
    
        actionPlan = [Select id, name, RelatedObjectName__c , Send_Email__c , Mass_Assign_To__c, Deal__c, Loan__c from Template__c where id=:apexpages.currentpage().getparameters().get('id')];
        actionPlanTasksList=[Select Index__c, Name, id, Field_to_update_Date_Received__c , Date_Needed__c, Field_to_update_for_date_needed__c, Notify_Emails__c, Priority__c, Comments__c from Task_Plan__c where Template__c=:apexpages.currentpage().getparameters().get('id') and Parent__c =null];
        mapTaskList=[Select Index__c, Name, id, Field_to_update_Date_Received__c , Date_Needed__c, Field_to_update_for_date_needed__c, Notify_Emails__c, Priority__c, Comments__c, Date_Ordered__c, Parent__c  from Task_Plan__c where Template__c=:apexpages.currentpage().getparameters().get('id') and Parent__c !=null];
        return null;
    }
     /*public void nutralexists(){
        actionPlanTasks = new List<Task_Plan__c>();
        RollCnt = new List<Integer>();                
        integer reccnt=0;
        Map<Id, Integer> IdIntMap = new Map<Id, Integer>();
        
        for(Task_Plan__c T:[Select Index__c, Name, id, Field_to_update_Date_Received__c , Date_Needed__c, Field_to_update_for_date_needed__c, Notify_Emails__c, Priority__c, Comments__c from Task_Plan__c where Template__c=:apexpages.currentpage().getparameters().get('id') and Parent__c =null]){
            T.Index__c=reccnt;
            IdIntMap.put(T.id, reccnt);
            actionPlanTasks.add(T);
            RollCnt.add(reccnt);
            reccnt++;
        }
        
        mapTask = new Map<Integer,List<Task_Plan__c>>();
        Integer inst;
        for(Task_Plan__c T:[Select Index__c, Name, id, parent__r.Index__c, Field_to_update_Date_Received__c , Date_Needed__c, Field_to_update_for_date_needed__c, Notify_Emails__c, Priority__c, Comments__c, Date_Ordered__c, Parent__c  from Task_Plan__c where Template__c=:apexpages.currentpage().getparameters().get('id') and Parent__c !=null]){
            inst=Integer.valueof(IdIntMap.get(T.parent__c));
            if(mapTask.Containskey(inst)){
                mapTask.get(inst).add(T);
            }
            else{
                mapTask.put(inst, new List<Task_Plan__c>());
                mapTask.get(inst).add(T);
            }
        }
         //mapTask = new Map<Integer,List<Task_Plan__c>>();
        
    }*/
     //----Apply Action Plan-------//
     public List<SelectOption> gettemplatesOptions()
    {
        List<SelectOption> options = new List<SelectOption>();
        String autoname;        
            templatelist = [select id, name from Template__c where Is_Template__c = True and RelatedObjectName__c='Deal__c'];
            options.add(new SelectOption('--None--','--None--'));
        For(integer i=0;i<templatelist.size();i++)
        {
            options.add(new SelectOption(templatelist[i].Name,templatelist[i].name));
        }
        return options;
    } // -- End---
    public void changeTemplate(){
    
    mapTaskOrder=new map<Integer,Task_Plan__c>();
   //public Map<id,Task_Plan__c> mapTaskOrder{get;set;}
     actionPlanTasks = new list<Task_Plan__c>();
     actionPlansubTasks= new list<Task_Plan__c>();
     List<Template__c> templatelist= new List<Template__c>();
     count=0;
     Try
     {
        Template__c ct= new Template__c();
        Map<Id, Integer> IdIntMap = new Map<Id, Integer>();
        RollCnt = new List<Integer>();
        mapTask = new Map<Integer,List<Task_Plan__c>>();
        Integer inst;
        set<Integer>IndexOfParent= new set<Integer>();
        system.debug('get me the template selected..'+templateSelected);
        ct=[select id from Template__c where name =:templateSelected Limit 1];
        system.debug('get me the template id..'+ct.id);
        actionPlanTasks=[select Template__c,id ,Assigned_To__c,name,Date_Completed__c,Date_Needed__c,Date_Ordered__c,Date_Received__c,Index__c ,Field_to_update_Date_Received__c ,Field_to_update_for_date_needed__c ,Notify_Emails__c ,Priority__c,Comments__c,Template__r.Deal__c ,Parent__c from Task_Plan__c where Template__c =:ct.id and Index__c!=null and Parent__c =null order by Index__c Asc];
        actionPlansubTasks=[select Template__c,id ,Assigned_To__c,name,Date_Completed__c,Date_Needed__c,Date_Ordered__c,Date_Received__c,Index__c ,Field_to_update_Date_Received__c ,Field_to_update_for_date_needed__c ,Notify_Emails__c ,Priority__c,Comments__c,Template__r.Deal__c ,Parent__c from Task_Plan__c where Template__c =:ct.id and Parent__c !=null ];
        system.debug('get me the list ..'+actionPlanTasks.size());
        system.debug('get me the list ..'+actionPlansubTasks.size());
        system.debug('>>>>>>>'+ActionplanTasks.size());
        if(actionPlanTasks.size()>0)
         {
             for(Task_Plan__c  sobj :actionPlanTasks)
            { 
                system.debug('@@@@@@@'+count + sobj);
                mapTaskorder.put(count,sobj);
                count=count+1;
                //IndexOfParent.add(sobj.Index__c-1);
                IdIntMap.put(sobj.id, integer.valueof(sobj.Index__c-1)); 
                system.debug('check the iD index map.'+IdIntMap);
                RollCnt.add(integer.valueOf(sobj.Index__c-1));
               //added below line on 28th november
               //mapTask.put(Integer.valueOf(sobj.Index__c-1), new List<Task_Plan__c>());
               //mapTask.get(Integer.valueOf(sobj.Index__c-1)).add(new Task_Plan__c(Index__c=1));
            }
       }
      if(actionPlansubTasks.size()>0)
     {
      // mapTask= new Map<Integer,List<Task_Plan__c>>();
            for(Task_Plan__c taskplan :actionPlanTasks)
           {
              if(taskplan.Index__c!=null)
              {
                   if(!mapTask.ContainsKey(Integer.valueOf(taskplan.Index__c)))
                  {
                     system.debug('Hey i am in the if loop'+taskplan );
                     mapTask.put(Integer.valueOf(taskplan.Index__c-1), new List<Task_Plan__c>());
                     mapTask.get(Integer.valueOf(taskplan.Index__c-1)).add(new Task_Plan__c(Index__c=0));
                  }
              }
           }
           system.debug('Checkthemap..<<>>>>>'+mapTask);
           for(Task_Plan__c  T:actionPlansubTasks)
            {         
                inst=Integer.valueof(IdIntMap.get(T.parent__c));
                system.debug('Check the index..'+inst);
                if(mapTask.Containskey(inst))
                {
                    integer Subindex=(mapTask.get(inst)).size();
                    T.Index__c=Subindex;
                    mapTask.get(inst).add(T);
                }
                else
                {
                    mapTask.put(inst, new List<Task_Plan__c>());
                     T.Index__c=1;
                    mapTask.get(inst).add(T);
                }
             }
      }
     if(actionPlansubTasks.size()==0)
     {
        mapTask= new Map<Integer,List<Task_Plan__c>>();
        for(Task_Plan__c taskplan :actionPlanTasks)
       {
          if(taskplan.Index__c!=null)
          {
               if(!mapTask.ContainsKey(Integer.valueOf(taskplan.Index__c-1)))
              {
                 system.debug('Hey i am in the if loop'+taskplan );
                 mapTask.put(Integer.valueOf(taskplan.Index__c-1), new List<Task_Plan__c>());
                 mapTask.get(Integer.valueOf(taskplan.Index__c-1)).add(new Task_Plan__c(Index__c=0));
              }
           }
        }
     }
     system.debug('check the map..'+mapTask);
      system.debug('check the map..'+mapTaskorder);
    }
    
    catch(Exception e)
    {
    system.debug('Exception e'+e);
    
    } 
     
}
}