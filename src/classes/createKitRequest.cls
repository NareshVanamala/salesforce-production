global class createKitRequest {
    @AuraEnabled
    public static String createKitRequest_Ligtning(String contactId){
        String result= createKitRequest(contactId);

        if(result!='noAgreement'){
            return FGSPushToWebservice.sendDoc(contactId, result);
        }
        return result;
    }
    webService static string createKitRequest(String contactId){
        // Establish User
        User u = [SELECT ID, Name FROM User WHERE Id = :userInfo.getUserId()];
        // Establish Contact
        List<Contact> contact = [SELECT ID, Name, MailingStreet, MailingCity, MailingState, MailingPostalCode, AccountId FROM Contact WHERE Id = :contactId AND Account.X1031_Selling_Agreement__c INCLUDES('CCPTIII - Add On','CCIT','Income NAV','CCPT IV') LIMIT 1];
        system.debug('---------Contact:'+contact);
        if (contact.size() > 0){
            for(Contact c : contact){
                // Creating the Kit Request on our side.
                Kit_Request__c k = new Kit_Request__c();
                k.Order_Date__c = datetime.now();
                k.Contact__c = contactId;
                k.Requestor__c = u.id;
                k.Address_Sent_To__c = c.MailingStreet + '; ' + c.MailingCity + ', ' + c.MailingState + ' ' + c.MailingPostalCode;
                k.FGS_Status__c = 'New - Awaiting Confirmation';
                k.Broker_Dealer__c = c.AccountId;
                insert k;
                return k.id;
            }
            return 'noAgreement';
        } else {
            return 'noAgreement';
        }
    }
}