@isTest(seeAlldata=true)
private class Test_EmployeeTerminationform{
    static testmethod void EmployeeTerminationformtest(){
     user u =[select id,name from user where id=:userinfo.getuserid()];
    Employee__c emp = new Employee__c();
    emp.name = 'Test';
    emp.Start_Date__c= system.today();
    emp.Supervisor__c = 'Rajesh';
    emp.First_Name__c ='N';
    emp.Title__c ='Title';
    emp.Last_Name__c ='Test';
    emp.Move_User_To__c ='move user';
    emp.Office_Location__c ='Hyderabad';
   
    emp.Employee_Status__c='On Boarding inProcess';
    emp.Terminated__c = false;
    emp.Department__c='01-External Sales';
    insert emp;
    
     Asset__c a = new Asset__c();
    a.Added_On__c= system.today();
    a.Asset_Status__c='Added';
    a.Added_On__c = system.today();
    a.Access_Name__c = 'VDI';
    a.Access_Type__c = 'Building';
    a.Email_Subject_Asset__c='email';
    a.Removed_On__c = system.today();
    a.Termination_Status__c ='Active';
    a.Terminating__c = false;
    a.Updating__c = true;
    a.Application_Owner__c = u.id;
    a.Application_Implementor__c = u.id;
    a.Employee__c = emp.id;
   insert a;
   
     system.assertequals(a.Employee__c,emp.id);

     a.Asset_Status__c = 'Removal In Process';
     a.Terminating__c = true;
     a.Departmant_Exco__c =true;
     update a;



     Asset__c a1 = new Asset__c();
    a1.Added_On__c= system.today();
    a1.Asset_Status__c='Active';
    a1.Added_On__c = system.today();
    a1.Access_Name__c = 'VDI';
    a1.Access_Type__c = 'Building';
    a1.Email_Subject_Asset__c='email';
    a1.Removed_On__c = system.today();
    a1.Termination_Status__c ='Active';
    a1.Terminating__c = false;
    a1.Updating__c = true;
    a1.Application_Owner__c = u.id;
    a1.Application_Implementor__c = u.id;
    a1.Employee__c = emp.id;
    insert a1;
    a1.Asset_Status__c = 'In Process';
    a1.Terminating__c = true;
    a1.Departmant_Exco__c =true;
    update a1;
     
    system.assertequals(a1.Employee__c,emp.id);

     
     ApexPages.currentPage().getParameters().put('id', emp.id);
    ApexPages.StandardController sc;              
    EmployeeTerminationform controller = new EmployeeTerminationform(sc);       
  
    controller.submit();
    
    
    }



}