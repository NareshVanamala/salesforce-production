public with sharing class InvestorAttachmentController {
                
    public Contact contact{get;set;}   
    public Investor_Attachment__c CA{get;set;}    
    public string filename{get;set;}
    public Blob fileBody{get;set;}

    public InvestorAttachmentController(ApexPages.StandardController controller) 
    {      
        this.contact=(Contact)Controller.getrecord();
        CA= new Investor_Attachment__c();        
    }
      
    public pagereference processUpload()
    {
        system.debug('fileName----'+filename);
        if(fileName == null || fileName == '')
        {
            ApexPages.Message errorMsg1 = new ApexPages.Message(ApexPages.Severity.INFO, 'Please Select a file');
            ApexPages.addMessage(errorMsg1);
            return null;    
        }
        Investor_Attachment__c cAtt = new Investor_Attachment__c();
        cAtt.Contact__c = contact.Id;
        cAtt.Type__c = CA.Type__c;
        cAtt.Sub_Type__c = CA.Sub_Type__c;
        cAtt.Name = filename;
        if (Schema.sObjectType.Investor_Attachment__c.isCreateable())

        insert cAtt;
               
        if((fileBody !=null && fileName !=null))
        {   
        Attachment myAttachment = new Attachment();
          myAttachment.Body = fileBody;
            myAttachment.Name = fileName;
            myAttachment.ParentId = cAtt.Id;
                  
            try
            {
                if (Schema.sObjectType.Attachment.isCreateable())

                insert myAttachment;  
                
                cAtt.Attachment_Id__c =   myAttachment.Id;
                if (Schema.sObjectType.Investor_Attachment__c.isUpdateable()) 

                update cAtt;
            }
            catch(Exception Ex)
            {
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.INFO, Ex.getMessage());
                ApexPages.addMessage(errorMsg);
                return null;
            }

        }
        system.debug('contact.Id-----'+contact.Id);
        Pagereference Upsucess  = new Pagereference('/'+contact.Id);
        Upsucess.setredirect(true);
        return Upsucess;            
    }
      
    public pagereference back()
    {
     PageReference pref = new ApexPages.StandardController(contact).view();
     pref.setRedirect(true);
     return pref;
    }
    
    public pagereference Cancel()
    {  
        PageReference pr = new PageReference('/' + String.valueOf(contact.Id).substring(0,3));
        pr.setRedirect(true);
        return pr;   
    }

}