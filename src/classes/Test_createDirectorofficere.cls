@isTest
public class Test_createDirectorofficere
{
    static testMethod void ClassForCreateDirectorofficere ()
    {
        Entity__c Ec= new Entity__c();
        Ec.name='Checkthetrigger';
        insert Ec;
        Entity_Contact__c EC23= new Entity_Contact__c();
        EC23.name='NinadTambe';
        insert EC23;
      
        Entity_Contact__c EC24= new Entity_Contact__c();
        EC24.name='NinadT';
        insert EC24;
       

        Director__c dir= new Director__c();
        //dir.name='NinadTambe';
        dir.Date_of_Election__c=date.newInstance(Date.today().Year()- 2,1,1);   
        dir.Date_of_Resignation__c=system.today();
        dir.Entity__c=Ec.id;
        dir.Director_Name_Lookup__c=EC23.id;
        insert dir;
        
       system.assertequals(dir.Director_Name_Lookup__c,EC23.id);

        dir.Date_of_Resignation__c=system.today();
        update dir;
        delete dir;
        
        Officer__c off= new Officer__c();
        //dir.name='NinadTambe';
        off.Date_of_Election__c=date.newInstance(Date.today().Year()- 2,1,1);   
        off.Date_of_Resignation__c=system.today();
        off.Entity__c=Ec.id;
        off.Officer_Name_Lookup__c=EC24.id;
        off.Title__c='CEO';
        insert off;
        
        system.assertequals(off.Officer_Name_Lookup__c,EC24.id);

        dir.Date_of_Resignation__c=system.today();
        update off;
       
         delete off; 


    }
    
    }