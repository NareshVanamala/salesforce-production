global with sharing class ocms_DailyNAVService implements cms.ServiceInterface {

    public String JSONResponse {get; set;}
    private String action {get; set;}
    private Map<String, String> parameters {get; set;}
    private ocms_DailyNAVList wpl = new ocms_DailyNAVList();

    public ocms_DailyNAVService() {
        
    }

    public System.Type getType(){
        return ocms_DailyNAVService.class;
    }

    public String executeRequest(Map<String, String> p) {
        this.action = p.get('action');
        this.parameters = p; 
        this.LoadResponse();    
        return this.JSONResponse;
    }

    public void loadResponse() {        
        if (this.action == 'getDailyNAVList') {
            String sharetype = parameters.get('sharetype');
            String period = parameters.get('period');
            System.debug(sharetype);
            System.debug(period);
            this.getDailyNAVList(sharetype, period);
        }
    }

    private void getDailyNAVList(String sharetype, String period){
        this.JSONResponse = JSON.serialize(wpl.getDailyNAVList(sharetype, period));
    }

}