global without sharing class ocms_ARMyProfile extends cms.ContentTemplateController implements cms.ServiceInterface {
  
    /**
    * @description global variables
    **/
    public String contentLayoutName { get; set; }

    /**
    * @description default constructor
    **/
  global ocms_ARMyProfile() {
    
  }

    /**
    * @description the generate content constructor 
    **/ 
  global ocms_ARMyProfile(cms.GenerateContent gc) {
       super(gc);
    }

    // constructor for content editor
    global ocms_ARMyProfile(cms.CreateContentController cc) {
        super(cc);
    }

    /**
     * @description required by cms.ServiceInterface
     * @return System.Type
     */
    public System.Type getType() {
        return ocms_ARMyProfile.class;
    }

    /**
     * @description service endpoint required by cms.ServiceInterface
     * @param params - parameters describe the request and any additional options
     *          params.action - string name identifying the specific action being requested
     * @return String – JSON response
     */
    global String executeRequest(Map<String, String> params) {
        String response = '{"success": false}';
        String action = params.get('action');

        if (action == 'saveProfile') {
            response = saveProfile(params.get('street'),
                                          params.get('city'),
                                          params.get('state'),
                                          params.get('postalCode'),
                                          params.get('country'),
                                          params.get('phone'),
                                          params.get('email')
                                         );
        } 
        
        return response;

    }

    /**
    * @description content generator for the American Realty 
    *        self registration content
    **/ 
    global override String getHTML() {
        String street = '';
        if (currentUser.Street != null) {
            street = currentUser.Street;
        }
        String city = '';
        if (currentUser.City != null) {
            city = currentUser.City;
        }
        String state = '';
        if (currentUser.State != null) {
            state = currentUser.State;
        }
        String postalCode = '';
        if (currentUser.PostalCode != null) {
            postalCode = currentUser.PostalCode;
        }
        String country = '';
        if (currentUser.Country != null) {
            country = currentUser.Country;
        }
        String phone = '';
        if (currentUser.Phone != null) {
            phone = currentUser.Phone;
        }
        String email = '';
        if (currentUser.Email != null) {
            email = currentUser.Email;
        }
        String name = '';
        if (currentUser.Name != null) {
            name = currentUser.Name;
        }
        String company = '';
        if (currentUser.CompanyName != null) {
            company = currentUser.CompanyName;
        }
        
      String glassOverlay =  '<div class ="glassOverlay" style="position:fixed;background-color:rgba(0,0,0,0.75);width:100%;height:100%;top:0;left:0;">'+
                                    '<div class ="editForm" style="width:300px;height:500px;background-color:white;margin:auto;position:absolute;top:0;left:0;bottom:0;right:0;">'+
                                        '<div class="msgCenter"></div>'+
                                        '<div class="editFormItem"><label>Street</label><input id="street" type="text" value="' + street + '"/></div>' +
                                        '<div class="editFormItem"><label>City</label><input id="city" type="text" value="' + city + '"/></div>' +
                                        '<div class="editFormItem"><label>State</label><input id="state" type="text" value="' + state+ '"/></div>' +
                                        '<div class="editFormItem"><label>PostalCode</label><input id="postalCode" type="text" value="' + postalCode + '"/></div>' +
                                        '<div class="editFormItem"><label>Country</label><input id="country" type="text" value="' + country + '"/></div>' +
                                        '<div class="editFormItem"><label>Phone</label><input id="phone" type="text" value="' + phone + '"/></div>' +
                                        '<div class="editFormItem"><label>Email</label><input id="email" type="text" value="' + email + '" readonly="readonly"/></div>' +
                                        '<div class="actionButtons"><a href="javascript:saveChanges();">SAVE</a><a href="javascript:closeOverlay();">CANCEL</a></div>' +
                                    '</div>' +
                                '</div>' ;
        String html =   '<div class="myProfile">' +
                            '<div class="profileName">'+ name +'</div>' +
                            '<div class="profileCompany">'+ company +'</div>';
                            if (street != '' && city != '' && state != '' && postalCode != '' ) {
                                html += '<div class="profileAddress">'+ street + '</br>' +
                                city + ', ' + state + ' ' + postalCode + '</div>';
                            }
                            html += '<div class="profilePhone">'+ phone +'</div>' +
                            '<div class="profileEmail">'+ email +'</div>' +
                            '<div class="profileEdit"><a href="javascript:editProfile();">EDIT PROFILE</a></div>' +
                        '</div>' +
                        '<script>' +
                            'function editProfile () {' +
                                '$(\'.myProfile\').append(\''+ glassOverlay +'\');' +
                            '}' +
                            'function saveChanges() {' +
                                '$(\'.msgCenter\').children(\'.msg\').remove();' +
                                '$(\'.editForm\').height(500);' +
                                'var street = $(\'input[id=street]\').val();' +
                                'var city = $(\'input[id=city]\').val();' +
                                'var state = $(\'input[id=state]\').val();' +
                                'var postalCode = $(\'input[id=postalCode]\').val();' +
                                'var country = $(\'input[id=country]\').val();' +
                                'var phone = $(\'input[id=phone]\').val();' +
                                'var email = $(\'input[id=email]\').val();' +
                                
                                'var serviceParams = {action: \'saveProfile\', ' +
                                'street: street, '+
                                'city: city, '+
                                'state: state, '+
                                'postalCode: postalCode, '+
                                'country: country, '+
                                'phone: phone, '+
                                'email: email };' +

                                '$.orchestracmsRestProxy.doAjaxServiceRequest(' +
                                    '\'ocms_ARMyProfile\',' +
                                    'serviceParams, ' +
                                    'function (testStatus, json, xhr) {' +
                                        'if (json.success) {' +
                                            '$(\'.editForm\').height(525);' +
                                            'var successMessage =  \'<div class="msg" style="color: green;">\' + json.message + \'</div>\';' +
                                            '$(\'.msgCenter\').append(successMessage);' +
                                            'setTimeout(function(){' +
                                                'closeOverlay()' +
                                            '},1500);' +
                                            'location.reload();' +
                                        '} else {' +
                                            '$(\'.editForm\').height(515);' +
                                            'var errorMessage =  \'<div class="msg" style="color: red;">\' + json.message + \'</div>\';' +
                                            '$(\'.msgCenter\').append(errorMessage);' +
                                        '}' +
                                    '}' +
                               ');' +
                            '}' +
                            'function closeOverlay () {' +
                                '$(\'.glassOverlay\').remove();' +
                            '}' +
                        '</script>';
      return html;
    }

    private String saveProfile (String street, String city, String state, String postalCode, String country, String phone, String email) {
        String response = '';
        
        String formattedPhone;
        String formattedpostalCode;
        Pattern emailPattern;
        Matcher emailMatcher;
        Pattern postalCodePattern;
        Matcher postalCodeMatcher;
        Pattern statePattern;
        Matcher stateMatcher;
        Pattern cityPattern;
        Matcher cityMatcher;
        Pattern countryPattern;
        Matcher countryMatcher;
        
        if (!String.isEmpty(postalCode)) {
            postalCodePattern = Pattern.compile('[0-9]*');
            postalCodeMatcher = postalCodePattern.matcher(postalCode);
        }
        
        if (!String.isEmpty(state)) {
            statePattern = Pattern.compile('[A-Za-z]*');
            stateMatcher = statePattern.matcher(state);
        }
        
        if (!String.isEmpty(city)) {
            cityPattern = Pattern.compile('[A-Za-z]*');
            cityMatcher = cityPattern.matcher(city);
        }
        
        if (!String.isEmpty(country)) {
            countryPattern = Pattern.compile('[A-Za-z]*');
            countryMatcher = countryPattern.matcher(country);
        }
        
        if (!String.isEmpty(email)) {
            emailPattern = Pattern.compile('^[_A-Za-z0-9-\']+(\\.[_A-Za-z0-9-\']+)*@[A-Za-z0-9][_A-Za-z0-9-\']*(\\.[_A-Za-z0-9-\']+)*(\\.[A-Za-z]{2,})$');
            emailMatcher = emailPattern.matcher(email);
        }

        if (!String.isEmpty(phone)) {
            formattedPhone = phone.replaceAll('[^0-9]', '');
        }
        
        
        // validate that values are not empty
        if (String.isEmpty(street)) {
            response = '{"success": false, "message":"Street is required for address."}';
        } else if (String.isEmpty(city) || !cityMatcher.matches()) {
            response = '{"success": false, "message":"A valid City is required for address."}';
        } else if (String.isEmpty(state) || !stateMatcher.matches()) {
            response = '{"success": false, "message":"A valid State is required for address."}';
        } else if (String.isEmpty(postalCode) || !postalCodeMatcher.matches()) {
            response = '{"success": false, "message":"A valid postalCode is required for address."}';
        } else if (String.isEmpty(country) || !countryMatcher.matches()) {
            response = '{"success": false, "message":"A valid Country is required for address."}';
        } else if (String.isEmpty(email) || !emailMatcher.matches()) {
            response = '{"success": false, "message":"A valid email is required."}';
        } else if (String.isEmpty(phone) || formattedPhone.length()!=10) {
            response = '{"success": false, "message":"A valid phone number is required."}';
        } else {

            User updateUser = [SELECT Id, Name,contactid FROM User WHERE Id = :UserInfo.getUserId()];
            if (updateUser != null) {
            
                updateUser.Street = street;
                updateUser.City = city;
                updateUser.State = state;
                updateUser.PostalCode = postalCode;
                updateUser.Country = country;
                updateUser.Email = email;
                updateUser.Phone = formattedPhone;
                
                
            }
            if (updateUser != null) {
            
            contact c=[select id,name,email,mailingcity,mailingstate,mailingcountry,mailingstreet,phone,MailingPostalCode from contact where id=:updateuser.contactid];
            
                c.mailingStreet = street;
                c.mailingCity = city;
                c.mailingState = state;
                c.mailingPostalCode = postalCode;
                c.mailingCountry = country;
                c.Email = email;
                c.Phone = formattedPhone;
                
                update c;
            } 

            try { 
                update updateUser;
                
                response = '{"success":true, "message":"You have successfully updated your profile!"}';
            } 
            catch (Exception e) {
                response = '{"success":false, "message":"Error updating profile, Please contact site Administrator."}';
            }   
        }

        return response;
    }

    /**
    * @description the current user 
    **/
    public User currentUser {
        get {        
            String userId = UserInfo.getUserId();
            currentUser = new User();
            User queriedUser = [SELECT Id, FirstName, LastName, Name, CompanyName, Street, City, State, PostalCode, Country, Phone, Email FROM User WHERE Id = :userId LIMIT 1];
            if (queriedUser != null) {
                currentUser = queriedUser;
            }
            return currentUser;
        } set;
    }

}