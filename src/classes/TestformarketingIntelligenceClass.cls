@isTest
private class TestformarketingIntelligenceClass 
 {
      public static testMethod void TestformarketingIntelligenceClass () 
      {
          Account a= new Account();
          a.name='Testaccount1';
          //RecordType rtypes = [Select Name, Id From RecordType where isActive=true and sobjecttype='Account'and name='National_Accounts'];
          //a.recordtypeid=rtypes.id;
          insert a;  
          
          Marketing_Intelligence__c ct= new Marketing_Intelligence__c();
          ct.Account__c=a.id;
          ct.name='Testingthe Program';
          ct.Description__c='Letmego';
          insert ct;
          
          Marketing_Intelligence__c ct1= new Marketing_Intelligence__c();
          ct1.Account__c=a.id;
          ct1.name='Testingthe Program2';
          ct1.Description__c='Letmego2';
          insert ct1;
          
           String nameFile = 'hello';
                   
           Attachment attachment= new Attachment();
           attachment.OwnerId = UserInfo.getUserId();
           attachment.ParentId = ct.Id;// the record the file is attached to
           attachment.Name = nameFile;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
           attachment.body=bodyBlob;
           insert attachment;
           
              
         ApexPages.currentPage().getParameters().put('id',a.id);
         ApexPages.StandardController controller = new ApexPages.StandardController(a); 
         marketingIntelligenceClass x = new marketingIntelligenceClass(controller); 
         
          x.nameFile='Vasu_Snehal';
         Blob blb=Blob.valueOf('Vasu_SnehalVasu_SnehalVasu_Snehal');         
         x.contentFile=blb;
         x.addrow.Name='B_D_Systems Name';
         List<Attachment> attachments=[select id, name from Attachment where parent.id=:ct.id];
         System.assertEquals(1, attachments.size());
         
          
         x.newProgram();
         x.savenewProg();
         x.editrecord();
         //x.viewRecord();
         x.SaveRecord();
         x.removecon();
         x.cancelnewProg();          

       }
         
          
           
 }