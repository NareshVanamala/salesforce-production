public with sharing class CM_CustomVendor
{

    public string selectedvendor{get;set;}
    public Vendor__c vnd {get;set;}
    public Project__c cp{get;set;}
    public CM_CustomVendor(ApexPages.StandardController controller)
    {
     cp =[select id,Vendor__c  from Project__c where id = :ApexPages.currentPage().getParameters().get('id')];   
     system.debug('This is my project'+cp);
    }
    
   public void SaveVendor()
   {
     system.debug('Please check the value'+selectedvendor);
     vnd=[select id, name from Vendor__c where name=:selectedvendor];
     system.debug('check my Vendor'+vnd); 
      cp.Vendor__c =vnd.id;
      if (Schema.sObjectType.Vendor__c.isUpdateable()) 

      update cp;  
    
     
     
   }
    


}