public with sharing class CP_ConProdcls {
public CP_HelperAccConProdcls cls {get;set;}
public string str {get;set;}
public Boolean selected {get;set;}
public id curid;
    public CP_ConProdcls(ApexPages.StandardController controller) {
        cls=new CP_HelperAccConProdcls();
        curid= ApexPages.CurrentPage().getParameters().get('id'); 
        str='';
    }
    public void search() {
        if(str!='') {
             cls.Searchpro('%' + str + '%'); 
         }
    }
    public PageReference addcont() {
        
            cls.addCon(curid, selected);
        
        if(!cls.isFailed && cls.selectprod.size() > 0) {
            PageReference pageRef = new PageReference('/'+curid);
            pageRef.setRedirect(true);
            return pageRef;
        }
        
        return null;
    }
    public PageReference Clear() {
         PageReference pageRef = new PageReference('/apex/CP_ConProd?id='+curid);
            pageRef.setRedirect(true);
            return pageRef;
    }

}