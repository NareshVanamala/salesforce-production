public with sharing class ClassForCustomeRelatedlist {

    Public List<Contact> memberList{get;set;}
    public Contact deletelist1;
    public Contact updatedlist1;
    public integer removepos{get;set;}
    public string conid{get;set;}
    public Id recId{get;set;} 
    //public Account a;
    public Id recTypeIdNational;
    public ClassForCustomeRelatedlist (ApexPages.StandardController controller)
    {
        addrow = new Contact();
        //a =(Account)controller.getrecord();
        recId= controller.getrecord().id;
        recTypeIdNational=[Select id,name,DeveloperName from RecordType where DeveloperName='National_Accounts' and SobjectType = 'contact'].id;
        memberList =[select name,Title,Phone,Job_Title__c,Contact_Role__c,Cole_Contact_2__c,Reported_To__c,Job_Description__c,Email,Fax,mailingcity,mailingstate, id,accountid,Cole_Contact__c,Last_Producer_Date__c,Total_Cole_Tickets_Only_Funds_in_SF__c,CCPT__c,CCPT_II__c,CCPT_III__c,CCPT_IV__c,CCIT__c,Income_NAV__c,AUM_Actual__c from Contact where accountid=:recId and recordtypeid=:recTypeIdNational order by LastModifiedDate DESC];
     // memberList =[select name,Title,Phone,Job_Title__c,Contact_Role__c,Cole_Contact_2__c,Reported_To__c,Job_Description__c,Email,Fax,mailingcity,mailingstate, id,accountid,Cole_Contact__c,Last_Producer_Date__c,Total_Cole_Tickets_Only_Funds_in_SF__c,CCPT_III__c,CCPT_IV__c,CCIT__c,Income_NAV__c,AUM_Actual__c from Contact where accountid=:recId and recordtypeid=:recTypeIdNational order by LastModifiedDate DESC];  
        deletelist1= new Contact();  
        updatedlist1=new Contact();        
    }
   /* public Pagereference removecon(){
        if(memberList.size()>0)
        {
        
            string paramname= Apexpages.currentPage().getParameters().get('node');
            system.debug('Check the paramname'+paramname);
            for(Integer i=0;i<memberList.size();i++)
            {
                if(memberList[i].id==conid)
                {
                    removepos=i;
                    deletelist1.id=memberList[i].id;
                  
                }
            }
            memberList.remove(removepos);
            system.debug('check the deletelist..'+deletelist1);
             delete  deletelist1;          
        }
       return ApexPages.currentPage();
   }*/
   
   // Edit Record method ....
   //Pagereference p;
   public Id recordId {set;get;}
   public Boolean edit {set;get;}   
  
   /*public Pagereference editRecord(){      
        PageReference conEditPage = new Pagereference('/'+conid+'/e');
        conEditPage.setRedirect(true);
        return conEditPage;
       
   }*/
   
  /* public Pagereference viewRecord(){
       recordId = conid;   
      myatt=[select id, name, parentid from Attachment where parentid=:recordId ];
        system.debug('check the attachement..'+myatt);
       p = new Pagereference('/'+myatt.id);
       p.setRedirect(true);
       return p;
   }*/
   
  /* public Pagereference SaveRecord(){
      if(memberList.size()>0)
        {
             for(Integer i=0;i<memberList.size();i++)
            {
                if(memberList[i].id==conid)
                {
                   updatedlist1.id= memberList[i].id;
                   updatedlist1.Name= memberList[i].Name;
                   updatedlist1.Assets__c= memberList[i].Assets__c;
                   updatedlist1.Category__c=memberList[i].Category__c;
                   updatedlist1.Cole_Product__c= memberList[i].Cole_Product__c;
                }
            }
                 update updatedlist1;          
        }
       edit = false;
       return null
       
   }*/
    //adding new record
    public Boolean addnew{set;get;}
    public Contact addrow {set;get;}

   /* public Pagereference newProgram(){
        //https://cs4.salesforce.com/003/e?retURL=%2F0013000000BlAy2&accid=0013000000BlAy2&RecordType=012500000005S6L&ent=Contact
        PageReference conNewPage = new Pagereference('/003/e?retURL=%2F'+recId+'&accid='+recId+'&RecordType='+recTypeIdNational+'&ent=Contact');        
        conNewPage.setRedirect(true);
        return conNewPage;
    }*/
   /* public Pagereference savenewProg(){
        addnew=false;
        try{
            addrow.Account__c = a.id;
            if(addrow.name!=null)
            insert addrow;
           else
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Enter Name of the Program'));

            system.debug('Added --->'+addrow.Id);
           // memberList = [select name, id,(Select Id,Name from Attachments) Attmts,Account__c,Assets__c,Category__c,Cole_Product__c from Program__c where Account__c=:a.id order by LastModifiedDate DESC]; 
                        
            attachment.OwnerId = UserInfo.getUserId();
            attachment.ParentId = addrow.Id;// the record the file is attached to
            attachment.Name = nameFile;
            attachment.Body = contentFile;   
            //attachment.IsPrivate = true;
            
            system.debug('\n myAttachment.name--->'+attachment.name);
            system.debug('check the attachement..'+ attachment);
            try {
                if((attachment.Name!=null) &&(attachment.Body!=null))
                insert attachment;
                memberList = [select name, id,(Select Id,Name from Attachments) Attmts,Account__c,Assets__c,Category__c,Cole_Product__c from Program__c where Account__c=:a.id order by LastModifiedDate DESC];
            } catch (DMLException e) {
                system.debug('check the attachement..'+ attachment);
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
            }
       
        }catch(Exception e){
            system.debug('Exception --->'+e);
        } 
          
        return ApexPages.currentPage(); 
           
    }
    public Pagereference cancelnewProg(){
        addnew=false;
        return null;    
    } */
   

    
 }