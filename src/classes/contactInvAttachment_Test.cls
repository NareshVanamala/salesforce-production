@isTest(SeeAllData = true)
public class contactInvAttachment_Test{
    static testmethod void contactinv(){
        contact conobj=new contact();
        conobj =[select id from contact limit 1];
        Contact_InvestorList__c coninvobj =new Contact_InvestorList__c();
        coninvobj.name ='Test Contact Investor';
        coninvobj.Fund__c ='CCIT II';
        coninvobj.Advisor_Name__c =conobj.id;
        insert coninvobj;
        Attachment attach=new Attachment();    
            attach.Name='pdocattachment.pdf';
            Blob bodyBlob=Blob.valueOf('sample pdf file');
            attach.body=bodyBlob;
            attach.parentId=coninvobj.id;
            insert attach;
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:coninvobj.id];
            System.assertEquals(1, attachments.size());

        
    }

}