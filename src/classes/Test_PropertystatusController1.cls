@isTest(seealldata=true)
 private class Test_PropertystatusController1
 {
   static Testmethod void TestPropertystatusController1()
     {
       user u =[select id,name from user where id= :Userinfo.getuserid()];
       user u1 =[select id,name from user where id= :Userinfo.getuserid()];
        
        List<String> selectedparalegalids = new List<String>();
        selectedparalegalids.add(u.id);
        
         contact c = new contact();
         c.firstname = 'Test';
         c.lastname = 'Test';
      insert c;
      
      String ids = u.id+','+u1.id;
       
      list<Deal__c>paralegaldeals= new list<Deal__c>(); 
       Deal__c d = new Deal__c();
        d.Name ='Test12'; 
        d.Deal_Status__c = 'Reviewing';
        d.Contract_Price__c=100;
        d.Attorney__c = c.id;
        d.Fund__c = 'TBD';
        d.Paralegal__c =u.id;   
        paralegaldeals.add(d);    
       
        
        Deal__c d1 = new Deal__c();
        d1.Name ='Test13';  
        d1.Deal_Status__c = 'Reviewing';
        d1.Contract_Price__c=100;
        d1.Attorney__c = c.id;
        d1.Fund__c = 'TBD';
        d1.Paralegal__c = u1.id;
             
       
         paralegaldeals.add(d1);   
        insert paralegaldeals;
      
        list<task> tsklist = new list<task>();
        Task t = new Task();
        t.whatid = d.id;
        t.status ='Not Started';
        t.subject = 'Left Voice Mail';
        t.Date_Ordered__c = system.today();
        t.ActivityDate = system.today();
        t.Description = 'xxx';
        t.Date_Received__c  = system.today();
        tsklist.add(t);
       insert tsklist;
       
       System.assertEquals(t.whatid,d.id);
      
        ApexPages.currentPage().getParameters().put('paralegalIds',ids);
        PropertystatusControllerforParalegal pr = new PropertystatusControllerforParalegal();
        pr.getParalegals();
        pr.dealListMap.put('a',paralegaldeals);
        pr.TaskListMap.put(t.id,tsklist);
        //pr.mywrapper;
        
        
      //  MyWrapper m;
        
    PropertystatusControllerforParalegal.MyWrapper obj = new PropertystatusControllerforParalegal.MyWrapper();
        //p.paralegaldeals = dlist;
     system.debug('-------->'+paralegaldeals);
     
     }

 }