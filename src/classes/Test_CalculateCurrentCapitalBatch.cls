@isTest
public class Test_CalculateCurrentCapitalBatch
{
    
    static testmethod void myTestMethod1()
    {
    
     Test.StartTest();
      
     Account a = new Account();
     a.name = 'Test Account';
     insert a;
     
     
     Contact c = new Contact();
     c.firstname = 'xxx';
     c.lastname = 'yyy';
     c.accountid = a.id;
     insert c;
     
     system.assertequals(c.accountid,a.id);
     
      List <REIT_Investment__c> lds = new List<REIT_Investment__c>();
        for(integer i = 0; i<200; i++){
        REIT_Investment__c l = new REIT_Investment__c(Current_Capital__c = 10000,Rep_Contact__c = c.id);
        lds.add(l);
        }
        insert lds;
     
     REIT_Investment__c reit = new REIT_Investment__c();
     reit.Deposit_Date__c = System.Today();
     reit.Current_Capital__c = 10000;
     reit.Rep_Contact__c = c.id;
     insert reit;
     
      system.assertequals(reit.Rep_Contact__c,c.id);

     REIT_Investment__c reit1 = new REIT_Investment__c();
     reit1.Deposit_Date__c = System.Today();
     reit1.Current_Capital__c = 10000;
     reit1.Rep_Contact__c = c.id;
     insert reit1;
     
    system.assertequals(reit1.Rep_Contact__c,c.id);

     
     c.Year_To_Date__c = reit.Current_Capital__c + reit1.Current_Capital__c;
     update c;
     
     CalculateCurrentCapitalBatch batch1 = new CalculateCurrentCapitalBatch();
     Id batchId1 = Database.executeBatch(batch1);
     //batch1.start();
     //batch1.execute(null,lds);
    }
}