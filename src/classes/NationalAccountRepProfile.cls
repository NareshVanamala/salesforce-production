public with sharing class NationalAccountRepProfile {
  
  // Constructor
 public NationalAccountRepProfile(ApexPages.StandardController controller) {
 
  this.Acc= (Account)controller.getSubject();
  system.debug('Mohe lage pyare...'+Acc);
      this.nat = [ SELECT 
      d.Platform_A_SOW__c, d.Platform_A_Total_Assets__c, 
      d.Platform_B_SOW__c, d.Platform_B_Total_Assets__c, d.Platform_C_SOW__c, 
      d.Platform_C_Total_Assets__c, d.Id, d.Platform_D_SOW__c, d.Platform_D_total_Assets__c, d.TotalReps__c, d.Reps_With_7__c, d.Total_Selling_Reps__c, d.Total_Sale__c, d.Sales_Per_Rep__c
       
            FROM 
      National_Account_Helper__c d 
      WHERE 
      d.Account__c =:Acc.id ];
      system.debug('Lets check the value of it....'+nat);
 }
 
 
 // Action Method called from page button
 public pagereference saveChanges() { 
  update this.nat;
 PageReference pageRef1 = new PageReference('/apex/RepProfileOutput');
 return pageRef1;
 
 }
 public pagereference Edit1() { 
  PageReference pageRef = new PageReference('/apex/RepProfile');
 return pageRef;
 }
 // public Getter to provide table headers 
 //public string[] getheaders() { return new string [] 
 // {'LastName','FirstName','Phone' } ; }
 
 // public Getter to list project deliverables
 public National_Account_Helper__c getrecs() { 
  return this.nat; 
 } 
 
 // class variables
 Account Acc = new Account();
 //Account Acc=[select id, name from Account where id = :ApexPages.currentPage().getParameters().get('id')];
// Account Acc=[select id, name from Account where id ='001W0000007fISW'];

 National_Account_Helper__c nat = new National_Account_Helper__c();
 
  
}