public with sharing class ValidateAndOrExpression {
	public Boolean hasError{get; set;}
	public String errorMsg{get; set;}
	public String message{get; set;}
	private Map<String, String> validChars= new Map<String, String>{'1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'9', '10'=>'10', 'and'=>'and', 'or'=>'or'};

	public ValidateAndOrExpression(String exp, Set<String> filterKeysSet, Boolean hasAndOr) {
		hasError=false;
		errorMsg='';
		message='';
		String expNew='';

		try{
			if(exp==null || exp=='')
				throw new VereitException('Empty expression.');
			if(hasAndOr){
				expNew= exp.toLowerCase().replaceAll('and','+').replaceAll('or','-');
			}else {
				expNew=exp;
			}
			
			
			checkExpressionAndFilter(expNew, filterKeysSet);

			Integer val=evaluate(expNew, filterKeysSet.size());

			isAndOrBracketSurrounded(expNew);
		}catch(Exception e){
			hasError= true;
			errorMsg='Error: '+e.getMessage()+',\nLine: '+e.getLineNumber()+',\nStack: '+e.getStackTraceString();
			message= e.getMessage();
			system.debug(logginglevel.info, '-------- ************** --------------\n'+errorMsg);
		}
		
	}

	private void isAndOrBracketSurrounded(String exp){
		String[] tokens = exp.split('');

		Stack ops = new Stack();

		for (Integer i = 0; i < tokens.size(); i++){
			if (tokens[i] == ' ')
				continue;

			if (tokens[i] == '(' || tokens[i] == ')'){
				ops.push(tokens[i]);
			}else if(tokens[i]=='+' || tokens[i]=='-'){
				if(ops.peek() ==null || String.valueOf(ops.peek()) == '(' || String.valueOf(ops.peek()) == ')'){
					ops.push(tokens[i]);
				}else if( String.valueOf(ops.peek()) != '(' && String.valueOf(ops.peek()) != ')' && ops.peek()!=null 
							&& ops.peek()!='' && tokens[i] != String.valueOf(ops.peek())){
					throw new VereitException('Your filter must use parentheses around successive AND and OR expressions.');
				}
			}
		}//for
	}

	private static void checkExpressionAndFilter(String exp, Set<String> filterKeysSet){
		Map<String, String> valuesMap= new Map<String, String>();

		String exp1=convertExpression(exp);

		String[] tokens = exp1.deleteWhitespace().split('');
		
		for (Integer i = 0; i < tokens.size(); i++){
			if (tokens[i] >= '0' && tokens[i] <= '9'){
				String sbuf='';
				// There may be more than one digits in number
				while (i < tokens.size() && tokens[i] >= '0' && tokens[i] <= '9')
					sbuf +=tokens[i++];

					i--;
				valuesMap.put(sbuf, sbuf);
			}//if
		}// for

		Map<String, String> filterKeysMap= new Map<String, String>();

		for(String key: filterKeysSet){
			filterKeysMap.put(key, key);			
		}

		for(String key: filterKeysMap.keySet()){
			if(valuesMap.get(key)==null){
				throw new VereitException('Invalid formula, filter key is not being used in the formula.');
			}
		}


		for(String key: valuesMap.keySet()){
			if(filterKeysMap.get(key)==null){
				throw new VereitException('Invalid formula, formula does not contain the mentioned filter key.');
			}
		}

	}

	public static String convertExpression(String exp){
		return exp.replaceAll('10', '##').replaceAll('1','0').replaceAll('2','1').replaceAll('3','2').replaceAll('4','3')
				  .replaceAll('5','4').replaceAll('6','5').replaceAll('7','6').replaceAll('8','7').replaceAll('9','8').replaceAll('##','9');
	}
	public static String convertExpressionKeys(String exp){
		return exp.replaceAll('10', '##&&##').replaceAll('1','##0##').replaceAll('2','##1##').replaceAll('3','##2##').replaceAll('4','##3##')
				  .replaceAll('5','##4##').replaceAll('6','##5##').replaceAll('7','##6##').replaceAll('8','##7##').replaceAll('9','##8##').replaceAll('##&&##','##9##');
	}
	
	public static Integer evaluate(String expression, Integer totalFields){
		String[] tokens = expression.split('');

		// Stack for numbers: 'values'
		Stack values = new Stack();

		// Stack for Operators: 'ops'
		Stack ops = new Stack();

		for (Integer i = 0; i < tokens.size(); i++){
			// Current token is a whitespace, skip it
			if (tokens[i] == ' ')
				continue;

			// Current token is a number, push it to stack for numbers
			if (tokens[i] >= '0' && tokens[i] <= '9'){
				String sbuf='';
				// There may be more than one digits in number
				while (i < tokens.size() && tokens[i] >= '0' && tokens[i] <= '9')
					sbuf +=tokens[i++];

					i--;
				values.push(Integer.valueOf(sbuf));
			}else if (tokens[i] == '('){
				// Current token is an opening brace, push it to 'ops'
				ops.push(tokens[i]);
			}else if (tokens[i] == ')'){
				// Closing brace encountered, solve entire brace
				while (ops.peek() != '(')
					values.push(applyOp(String.valueOf(ops.pop()), Integer.valueOf(values.pop()), Integer.valueOf(values.pop())));
				ops.pop();
			}else if (tokens[i] == '+' || tokens[i] == '-'){// Current token is an operator.
				// While top of 'ops' has same or greater precedence to current
				// token, which is an operator. Apply operator on top of 'ops'
				// to top two elements in values stack
				while (!ops.isempty() && hasPrecedence(tokens[i], String.valueOf(ops.peek())))
					values.push(applyOp(String.valueOf(ops.pop()), Integer.valueOf(values.pop()), Integer.valueOf(values.pop())));

				// Push current token to 'ops'.
				ops.push(tokens[i]);
			}else{
				throw new VereitException('Invalid character: '+tokens[i]);
			}
		}

		// Entire expression has been parsed at this point, apply remaining
		// ops to remaining values
		while (!ops.isempty())
			values.push(applyOp(String.valueOf(ops.pop()), Integer.valueOf(values.pop()), Integer.valueOf(values.pop())));

		// Top of 'values' contains result, return it
		return Integer.valueOf(values.pop());
	}

	// Returns true if 'op2' has higher or same precedence as 'op1',
	// otherwise returns false.
	public static boolean hasPrecedence(String op1, String op2){
		if (op2 == '(' || op2 == ')')
			return false;
		if ((op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-'))
			return false;
		else
			return true;
	}

	// A utility method to apply an operator 'op' on operands 'a' 
	// and 'b'. Return the result.
	public static Integer applyOp(String op, Integer b, Integer a){
		//system.debug('---- op: '+op+', a: '+a+', b: '+b);
		if(op=='+'){
			return Math.abs(a + b);
		}else if(op=='-'){
			return Math.abs(a - b);
		}else if(op=='*'){
			return a * b;
		}else if(op=='/'){
			if (b == 0)
				throw new VereitException('Cannot divide by 0');
			return a / b;
		}
		return 0;
	}

}