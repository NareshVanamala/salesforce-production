public with sharing class ActionPlanMassUpdateForAlldeals_class1
{
    private final ApexPages.StandardSetController cntr;
    private final PageReference fromPage;
    public String ids {get;set;}
    public String id {get;set;}
    public List<String> strs = new List<String>();
    public List<String> selectedDealIds = new List<String>();
    Public Deal__c thedeal{get;set;}
    public Template__c temps {get;set;}
    public list<Deal__c> selecteddealslist{get;set;}
    public id tmpid{get;set;}
    public ActionPlanMassUpdateForAlldeals_class1(ApexPages.StandardController controller) 
    {
         ids = ApexPages.currentPage().getParameters().get('ids');
         system.debug('get me the current deal id'+ids);
          if(ids!=null)
         {
            selectedDealIds = ids.split(',');
          }
         id = ApexPages.currentPage().getParameters().get('id');
         
          system.debug('Get me the id'+selectedDealIds);
        selecteddealslist= [Select Id , Name,Portfolio_deal__c from Deal__c where Id =:selectedDealIds ] ;
         system.debug('Get me the id'+selecteddealslist);
    }
    
    public List<String> strDealIds = new List<String>();
  /*  public List<Deal__c> getDeals()
    {
        List<Deal__c> dls =  [Select Id , Name,Portfolio_deal__c  from Deal__c where Id =:selectedDealIds ] ;
        selecteddealslist= [Select Id , Name,Portfolio_deal__c from Deal__c where Id =:selectedDealIds ] ;
        system.debug('Find the selected deallist'+selecteddealslist);
        for(Deal__c d : dls)
        {
             strDealIds.add(d.Id);
        }
            return dls ;
    }*/
    public List<Template__c> getTemplete()
    {
       List<Template__c> temp = [Select Id , Template__c from Template__c Where Deal__c IN :selectedDealIds and Is_Template__c = False] ;
     // List<Template__c> temp = [Select Id , Template__c from Template__c Where Deal__c IN :selectedDeal and Is_Template__c = False] ;
        return temp ;
       // return [Select Id,Name from Template__c Where ID IN :ret] ;
    }
    public String tempValues{get; set;}
    public List<SelectOption> gettempletName()
    {
              List<SelectOption> options = new List<SelectOption>();
              options.add(new SelectOption('--NONE--','--NONE--'));
 
              List<Template__c> templist = new List<Template__c>();
              Set<String> languages = new set<String>();
              templist = [Select Id,Template__r.Name , Name FROM Template__c Where Deal__c IN :selectedDealIds  ];
              system.debug('templisttemplist ========== '+templist);
              for (Integer j=0;j<templist.size();j++)
              {
                if(!languages.contains(templist[j].Template__c)){
                    languages.add(templist[j].Template__c);
                    if(templist[j].Template__r.Name!=null)
                  options.add(new SelectOption(templist[j].Template__r.Name ,templist[j].Template__r.Name ));
                }  
              }
            return options;
     }

    public List<Task_Plan__c> taskplans {get;set;}
    public PageReference  search()
    {
    system.debug('check the tempvalues'+tempValues);
    system.debug('=======testeds deal========'+selectedDealIds.size()+'_________'+selectedDealIds);    
     temps = [Select Id , Template__r.Name , Template__r.id , Name from Template__c Where Name =:tempValues limit 1] ;
        
        System.debug('temps ===>>'+temps);
        
/*        List<String> strTemIds = new List<String>();
            for(Template__c tem : temps){
                strTemIds.add(tem.Id);
            }
            */
            taskplans =[Select Id ,Date_Completed__c,Priority__c,Date_Ordered__c,Date_Needed__c,Status__c,Comments__c,Date_Received__c, Name,(Select Index__c, Name,  Field_to_update_Date_Received__c ,
                Status__c,Date_Needed__c,Date_Received__c,Date_Ordered__c,Parent__c,
                Field_to_update_for_date_needed__c, Notify_Emails__c, Priority__c, 
                Comments__c from Task_Plans__r),Index__c,Field_to_update_Date_Received__c,Field_to_update_for_date_needed__c from Task_Plan__c where Template__c =:temps.Id and Parent__c=null order by Index__c];
            system.debug('check my first taksplans..'+taskplans);
            valuesAdded = new List<OppWrapper>();
            Integer countVal =1 ;   
        
            for(Task_Plan__c task : taskplans){
                OppWrapper warper = new OppWrapper(task , countVal);
                countVal++;
                valuesAdded.add(warper);
            }
            return null; 

    }
    public   List<OppWrapper> valuesAdded {get;set;}
    public Pagereference  saveUpdateValues()
    {
        System.debug('Check my action plan..'+temps);
        System.debug('Check the task plans..'+taskplans);
        list<Deal__c>deallist23= new list<Deal__c>();  
        list<Task_Plan__c>ctlist1= new list<Task_Plan__c>();
        list<Task_Plan__c>remainingtasklist= new list<Task_Plan__c>();
        list<Task_Plan__c>updateremaininglist= new list<Task_Plan__c>();         
        list<Template__c> remainingTemplates= new list<Template__c>();  
        list<Template__c> updateremainingTemplates= new list<Template__c>(); 
        set<id>remainingtempids= new set<id>();
        set<id>dealids= new set<id>();    
        system.debug('give me my selected deallist..'+selecteddealslist);
        for(Deal__c deal:selecteddealslist)
         dealids.add(deal.id);
    // get the template of the action plan//
        //string tempid=temps.Template__c ;
        //Template__c Tt=[select id, name from Template__c where id=:tempid];
        remainingTemplates=[select id,name from Template__c where Template__c =:temps.id and Deal__c IN :selectedDealIds ];

        system.debug('get me the how many are remaining..'+remainingTemplates);
        for(Template__c remtemp:remainingTemplates) 
        {
            remainingtempids.add(remtemp.id);
            //remtemp.name=temps.name;
            updateremainingTemplates.add(remtemp);            
        }
        remainingtasklist=[select id,name,Comments__c,Date_Needed__c,Date_Ordered__c,Date_Received__c,Priority__c,Status__c,Index__c,Field_to_update_Date_Received__c,Field_to_update_for_date_needed__c,Template__c from Task_Plan__c where Template__c in:remainingtempids];
        ctlist1=[select id,Index__c,name,Comments__c,Date_Needed__c,Date_Ordered__c,Date_Received__c,Priority__c,Status__c,Field_to_update_Date_Received__c,Field_to_update_for_date_needed__c,Template__c from Task_Plan__c where Template__c=:temps.id];        
        //system.debug('Check my template..'+Tt);  
        system.debug('Check the template list'+ctlist1); 
        map<String,Task_Plan__c>indextotaskmap=new map<String,Task_Plan__c>();
        map<String,Task_Plan__c>indextotaskmap1=new map<String,Task_Plan__c>();
        for(Task_Plan__c tyr:taskplans)
        //  indextotaskmap.put(tyr.Name.touppercase(),tyr);
        indextotaskmap.put(tyr.Name,tyr);
        
        
        for(Task_Plan__c tyr1:ctlist1)
          //indextotaskmap1.put(tyr1.Name.touppercase(),tyr1); 
           
           indextotaskmap1.put(tyr1.Name,tyr1); 
           
           
       //update all the tasks of all the actionplans//
        set<id>updatedlistcheck= new set<id>(); 
         
            for(Task_Plan__c tasktm1:remainingtasklist)
            {
              Task_Plan__c tasktm = new Task_Plan__c();
              tasktm = tasktm1;
              
              System.debug('tasktm  ============>>>'+tasktm );
              
             // tasktm.Name = tasktm1.Name.touppercase();
              
              System.debug('tasktm.Name  ============>>>'+tasktm.Name );
              
              System.debug('indextotaskmap.get(tasktm.Name)=====>>>>>>>>>>>>>>>>>>'+indextotaskmap.get(tasktm.Name));
              
               try{
                if(indextotaskmap.get(tasktm.Name).Date_Ordered__c!=indextotaskmap1.get(tasktm.Name).Date_Ordered__c)
                {
                  tasktm.Date_Ordered__c=indextotaskmap.get(tasktm.Name).Date_Ordered__c; 
                  //tasktm.Name = tasktm1.Name ;      
                 updatedlistcheck.add(tasktm.id);

               }
               if(indextotaskmap.get(tasktm.Name).Date_Needed__c!=indextotaskmap1.get(tasktm.Name).Date_Needed__c)
                {
                  tasktm.Date_Needed__c=indextotaskmap.get(tasktm.Name).Date_Needed__c; 
                  //tasktm.Name = tasktm1.Name ;
                 updatedlistcheck.add(tasktm.id);
                 }
               
               if(indextotaskmap.get(tasktm.Name).Date_Received__c!=indextotaskmap1.get(tasktm.Name).Date_Received__c)
                {
                    tasktm.Date_Received__c=indextotaskmap.get(tasktm.Name).Date_Received__c;   
                    //tasktm.Name = tasktm1.Name ;    
                   updatedlistcheck.add(tasktm.id);
                 }
               if(indextotaskmap.get(tasktm.Name).Status__c!=indextotaskmap1.get(tasktm.Name).Status__c)
                { 
                  tasktm.Status__c=indextotaskmap.get(tasktm.Name).Status__c; 
                  //tasktm.Name = tasktm1.Name ;
                  updatedlistcheck.add(tasktm.id);

               }
               if(indextotaskmap.get(tasktm.Name).Priority__c!=indextotaskmap1.get(tasktm.Name).Priority__c)
                {
                  tasktm.Priority__c=indextotaskmap.get(tasktm.Name).Priority__c; 
                 // tasktm.Name = tasktm1.Name ;
                  updatedlistcheck.add(tasktm.id);

             }
             if(indextotaskmap.get(tasktm.Name).Comments__c!=indextotaskmap1.get(tasktm.Name).Comments__c)
              {
                 tasktm.Comments__c=indextotaskmap.get(tasktm.Name).Comments__c; 
               //  tasktm.Name = tasktm1.Name ;
                  updatedlistcheck.add(tasktm.id);
                }
             
              
               if(tasktm.Date_Received__c!=null)
               {
                tasktm.Status__c='Completed';
              //  tasktm.Name = tasktm1.Name ;
                updatedlistcheck.add(tasktm.id);
                }
            
            // tasktm.Name = tasktm.Name;
       
         System.debug(' tasktm before adding to list'+tasktm);
            
              updateremaininglist.add(tasktm);
              
              System.debug(' updateremaininglist ========>>'+updateremaininglist);
              
              }
              catch(Exception e) 
   {
   system.debug('e');
   }
            }
        system.debug('check the map..'+indextotaskmap);
        system.debug('Check the list..'+updatedlistcheck.size());
      //  list<Task_Plan__c>checkmylist=[select id  from Task_Plan__c  where id in:updatedlistcheck];
       
       
        update updateremainingTemplates;
        update updateremaininglist;
        //update checkmylist;
        for(Deal__c Currentdeal:selecteddealslist)
       {      
           for(Task_Plan__c T:taskplans) 
            {
                If(T.Date_Received__c!=null && T.Field_to_update_Date_Received__c!=null)
                {
                if(T.Field_to_update_Date_Received__c=='Site_Visit_Date__c')
                currentdeal.Site_Visit_Date__c=T.Date_Received__c;
                if(T.Field_to_update_Date_Received__c=='Passed_Date__c')
                currentdeal.Passed_Date__c=T.Date_Received__c;
               if(T.Field_to_update_Date_Received__c=='Estimated_COE_Date__c')
             currentdeal.Estimated_COE_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Hard_Date__c')
             currentdeal.Hard_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Estimated_SP_Date__c')
             currentdeal.Estimated_SP_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Purchase_Date__c')
             currentdeal.Purchase_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Estimated_Hard_Date__c')
             currentdeal.Estimated_Hard_Date__c=T.Date_Received__c;  
            if(T.Field_to_update_Date_Received__c=='Contract_Date__c')
             currentdeal.Contract_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Closing_Date__c')
             currentdeal.Closing_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='LOI_Signed_Date__c')
             currentdeal.LOI_Signed_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='LOI_Sent_Date__c')
             currentdeal.LOI_Sent_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Lease_Abstract_Date__c')
             currentdeal.Lease_Abstract_Date__c=T.Date_Received__c; 
             // if(T.Field_to_update_Date_Received__c=='Lease_Expiration__c')
             //currentdeal.Lease_Expiration__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Open_Escrow_Date__c')
             currentdeal.Open_Escrow_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Dead_Date__c')
             currentdeal.Dead_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Tenant_ROFR_Waiver__c')
             currentdeal.Tenant_ROFR_Waiver__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Date_Audit_Completed__c')
             currentdeal.Date_Audit_Completed__c=T.Date_Received__c; 
             if(T.Field_to_update_Date_Received__c=='Date_Identified__c')
             currentdeal.Date_Identified__c=T.Date_Received__c; 
             if(T.Field_to_update_Date_Received__c=='Investment_Committee_Approval__c')
             currentdeal.Investment_Committee_Approval__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Part_1_Start_Date__c')
             currentdeal.Part_1_Start_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Estoppel__c')
             currentdeal.Estoppel__c=T.Date_Received__c;  
             if(T.Field_to_update_Date_Received__c=='nd_SP_End_Date__c')
             currentdeal.nd_SP_End_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='nd_Estimated_SP_Date__c')
             currentdeal.nd_Estimated_SP_Date__c=T.Date_Received__c;
            }
            If(T.Date_Needed__c!=null && T.Field_to_update_for_date_needed__c!=null)
            {                 
             if(T.Field_to_update_for_date_needed__c=='Dead_Date__c')
             currentdeal.Dead_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Appraisal_Date__c')
             currentdeal.Appraisal_Date__c=T.Date_Needed__c;
            If(T.Field_to_update_for_date_needed__c=='Accenture_Due_Date__c')
             currentdeal.Accenture_Due_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Go-Hard Date')
             currentdeal.Go_Hard_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Rep_Burn_Off__c')
             currentdeal.Rep_Burn_Off__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Site_Visit_Date__c')
            currentdeal.Site_Visit_Date__c=T.Date_Needed__c;
           // if(T.Field_to_update_for_date_needed__c=='Query_Sorted_Date__c')
            // currentdeal.Query_Sorted_Date__c=T.Rep_Burn_Off__c;
            if(T.Field_to_update_for_date_needed__c=='Passed_Date__c')
             currentdeal.Passed_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Estimated_COE_Date__c')
             currentdeal.Estimated_COE_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Hard_Date__c')
             currentdeal.Hard_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Estimated_SP_Date__c')
             currentdeal.Estimated_SP_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Purchase_Date__c')
             currentdeal.Purchase_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Estimated_Hard_Date__c')
             currentdeal.Estimated_Hard_Date__c=T.Date_Needed__c;  
            if(T.Field_to_update_for_date_needed__c=='Contract_Date__c')
             currentdeal.Contract_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Closing_Date__c')
             currentdeal.Closing_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='LOI_Signed_Date__c')
             currentdeal.LOI_Signed_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='LOI_Sent_Date__c')
             currentdeal.LOI_Sent_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Lease_Abstract_Date__c')
             currentdeal.Lease_Abstract_Date__c=T.Date_Needed__c;  
             // if(T.Field_to_update_for_date_needed__c=='Lease_Expiration__c')
             //currentdeal.Lease_Expiration__c=T.Date_Needed__c;
              if(T.Field_to_update_for_date_needed__c=='Open_Escrow_Date__c')
             currentdeal.Open_Escrow_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Purchase_Date__c')
             currentdeal.Purchase_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Tenant_ROFR_Waiver__c')
             currentdeal.Tenant_ROFR_Waiver__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Date_Audit_Completed__c')
             currentdeal.Date_Audit_Completed__c=T.Date_Needed__c; 
             if(T.Field_to_update_for_date_needed__c=='Date_Identified__c')
             currentdeal.Date_Identified__c=T.Date_Needed__c; 
             if(T.Field_to_update_for_date_needed__c=='Investment_Committee_Approval__c')
             currentdeal.Investment_Committee_Approval__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Part_1_Start_Date__c')
             currentdeal.Part_1_Start_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Estoppel__c')
             currentdeal.Estoppel__c=T.Date_Needed__c; 
             if(T.Field_to_update_for_date_needed__c=='nd_Estimated_SP_Date__c')
             currentdeal.nd_Estimated_SP_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='nd_SP_End_Date__c')
             currentdeal.nd_SP_End_Date__c=T.Date_Needed__c;
             } 
            } 
           deallist23.add(currentdeal);      
       }    
            
              
        
        update deallist23;
        
         //string mydealid=temps 
         MassUpdateActionPlanforAlldeals_class batch = new MassUpdateActionPlanforAlldeals_class(selecteddealslist[0].id,temps.id);
        Id batchId = Database.executeBatch(batch,1);   
                
     
      return new Pagereference('/'+deallist23[0].Portfolio_Deal__c);

          
   }  



public Pagereference cancel(){
       String samp = System.Label.Current_Org_Url;
   return new Pagereference(''+samp+'/a3O/o');
  //return new Pagereference('https://cs13.salesforce.com/a3U/o');

}
    
     public class OppWrapper{
         
        public Task_Plan__c obj{get;set;}
        public Integer countVal{get;set;}
        public OppWrapper(Task_Plan__c obj,Integer s){
            this.obj = obj;
            countVal = s ;
        }
         
    }


}