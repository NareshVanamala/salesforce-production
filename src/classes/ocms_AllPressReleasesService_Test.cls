@isTest
private class ocms_AllPressReleasesService_Test{
   static testMethod void testDoGet() {
    ocms_AllPressReleasesService wps = new ocms_AllPressReleasesService();
        
        wps.gettype();

        Map<String,String> sendMap = new Map<String,String>();
        
        sendMap.put('action','getYearList');
        sendMap.put('year','2015');
        sendMap.put('pressRelease','Earning Release');
        system.assert(sendMap!= Null);

        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getPressReleaseList');
        sendMap.put('year','2015');
        sendMap.put('pressRelease','Earning Release');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getPropertyList');
        sendMap.put('pressRelease','Earning Release');
        sendMap.put('year','2015');
        wps.executeRequest(sendMap);
       
        sendMap.clear();
        sendMap.put('action','getResultTotal');
        sendMap.put('year','2015');
        sendMap.put('pressRelease','Earning Release');
        wps.executeRequest(sendMap);
  
        wps.loadResponse();
    }
    
    
}