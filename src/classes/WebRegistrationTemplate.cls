public with sharing class WebRegistrationTemplate
{
    private Contact con;
    private EmailTemplate et;
    private BrandTemplate bt;
    private User user;
    public boolean disabled{get;set;}
    public boolean rendered{get;set;}
    public string message{get;set;}
    public string style{get;set;}
    public string url{get;set;}
       
    public WebRegistrationTemplate(ApexPages.StandardController controller)
    {
        disabled=false;
        rendered=true;
        message='';
        style='color:green';
        this.con=(Contact)controller.getRecord();
        this.con=[select Id,FirstName,LastName,Website_Registered_User__c,Email from Contact Where Id=:con.Id];
        this.et=new EmailTemplate();
        this.et=[Select Id,Subject,Body,HtmlValue,BrandTemplateId from EmailTemplate where name='Web Site Registration Email'];
        this.user=new User();
        this.user=[select id, FirstName, LastName from User where id = :Userinfo.getUserId()];
        this.bt=new BrandTemplate();
        this.bt=[select Value from BrandTemplate where Id=:et.BrandTemplateId];



               
        if(this.con.Website_Registered_User__c)
        {
            disabled=true;
            rendered=false;
        }
        else
        {        
            if(this.con.Email == null)
            {
                message='Email Address not present';
                disabled=true;
                rendered=false;
                style='color:red';
            
            }
        }
        
        EmailForm();
   }
    
    public void SendEmail()
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(con.Id);
        mail.setTemplateId(this.et.id);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        message='Registration Email sent.';
        //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Registration Email has been sent.'));
    }

   public void SendEmail2()
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(con.Id);
        //mail.setTemplateId(this.et.id);
        mail.setsubject(et.subject);
        string body='<center><img src="https://c.cs4.content.force.com/servlet/servlet.ImageServer?id=015P0000000DAGh&oid=00DP00000004dv5" /></center>';
        body=body + et.HtmlValue; 
        body=body.replace('<![CDATA[','');
        body=body.replace(']]>','');
        body=body.replace('{!Contact.Id}',con.Id);
        body=body.replace('{!Contact.FirstName}',con.FirstName);
        body=body.replace('{!Contact.LastName}',con.LastName);
        body=body.replace('{!User.FirstName}',user.FirstName);
        body=body.replace('{!User.LastName}',user.LastName);
        mail.setHtmlBody(body);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        message='Registration Email sent.';
        //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Registration Email has been sent.'));
    }

    
    public void EmailForm()
    {
      url='/email/author/emailauthor.jsp?';
      url += 'p2_lkid=' + con.Id;
      //url += '&p3_lkid=' + con.Id;
      url += '&template_id=' + this.et.Id;  
      url += '&new_template=1';
      url += '&retURL=%2F' + con.Id;
      //url += '&nosave=0';   // not sure what this parameter does, or if it's really needed
      //url += "&save=1";   // send it now - don't just bring us to the "send email" page
       // window.open(url)

    
    }
}