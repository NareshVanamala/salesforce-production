@isTest
Public class LC_LOILandingClass_Test
{
  static testmethod void testCreatelease()
  {
         test.starttest();
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease1';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
         system.debug('let me know'+MYMAp);
         
    //********************with record typr New**********************    
     
     RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LOI__c' and DeveloperName = 'New_LOI' limit 1]; 
      string recid=NewRecType.id;
    //created New LOI with look up to lease
        
         LC_LOI__c lc1= new LC_LOI__c();
         lc1.name='TestNEWLOI';
         lc1.LOI_Type__c='Lease - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.Other_LOI_Description__c='Thisis my description';       
         lc1.recordtypeid=recid;
         insert lc1;
         
           
         System.assertEquals(myLease.MRI_PROPERTY__c , mc.id);
         System.assertEquals(myLease1.Name,'Test-lease1');
         System.assertEquals( lc1.name,'TestNEWLOI');
              
         ApexPages.StandardController stdcon = new ApexPages.StandardController(lc1);
         LC_LOILandingClass LCcontroller = new LC_LOILandingClass (stdcon);
         LCcontroller.LOI=lc1;
         LCcontroller.myprop=lc1.MRI_Property__c;
         LCcontroller.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontroller.IdtoLeasevsuitmap=MYMAp;
         LCcontroller.AdviserCallList();
         LCcontroller.LOI_Id=lc1.id;  
         LCcontroller.hideSectionOnChange();
         LCcontroller.save();
         LCcontroller.Cancel();
 
   
      
        LC_LOILandingClass  LCcontrollertest = new LC_LOILandingClass ();
    

    test.stoptest();
 
    
  } 
  
  static testmethod void testCreatelease1()
  {
         test.starttest();
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
         system.debug('let me know'+MYMAp);
         
  
  
  
  
    //********************with record type Amendent**********************      
         
         RecordType NewRecType1 = [Select Id From RecordType  Where SobjectType = 'LC_LOI__c' and DeveloperName = 'Amendment_LOI' limit 1]; 
       string recid1=NewRecType1.id;
         
       //created Amendent LOI with lookup to lease
         LC_LOI__c lc2= new LC_LOI__c();
         lc2.name='TestAmmLOI';
         lc2.LOI_Type__c='Lease - Amendment';
         lc2.MRI_Property__c=mc.id;
         lc2.Lease__c=mylease.id;
         lc2.Current_Date__c=system.today();
         lc2.Other_LOI_Description__c='This is my description';
         lc2.recordtypeid=recid1;
         insert lc2;
          
         ApexPages.StandardController stdcon = new ApexPages.StandardController(lc2);
         LC_LOILandingClass LCcontroller = new LC_LOILandingClass (stdcon);
         LCcontroller.LOI=lc2;
         LCcontroller.myprop=lc2.MRI_Property__c;
         LCcontroller.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontroller.IdtoLeasevsuitmap=MYMAp;
         LCcontroller.AdviserCallList();
         LCcontroller.LOI_Id=lc2.id;  
         LCcontroller.save();
         LCcontroller.Cancel() ;
          
             
    
     //********************with record type Renewal**********************   
     
     RecordType NewRecType2 = [Select Id From RecordType  Where SobjectType = 'LC_LOI__c' and DeveloperName = 'Renewal_LOI' limit 1]; 
     string recid2=NewRecType2.id;
     //created Amendent LOI with lookup to lease
         LC_LOI__c lc3= new LC_LOI__c();
         lc3.name='TestRenLOI';
         lc3.LOI_Type__c='Lease - Renewal';
         lc3.MRI_Property__c=mc.id;
         lc3.Lease__c=mylease.id;
         lc3.Current_Date__c=system.today();
         lc3.Other_LOI_Description__c='Thisis my description';
         lc3.recordtypeid=recid2;
         insert lc3;
              
         System.assertEquals(myLease.MRI_PROPERTY__c , mc.id);
         System.assertEquals(myLease1.Name,'Test-lease');
         System.assertEquals( lc3.name,'TestRenLOI');
         
         ApexPages.StandardController stdcon2 = new ApexPages.StandardController(lc3);
         LC_LOILandingClass LCcontroller2 = new  LC_LOILandingClass (stdcon2 );
         LCcontroller2.LOI_Id=lc3.id;
         LCcontroller2.LOI=lc3;
         LCcontroller2.myprop=lc3.MRI_Property__c;
         LCcontroller2.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontroller2.IdtoLeasevsuitmap=MYMAp;
         LCcontroller2.AdviserCallList();
         LCcontroller2.save();
         LCcontroller2.Cancel();
  
     //********************with record type Other**********************   
     
     //RecordType NewRecTypeOther = [Select Id From RecordType  Where SobjectType = 'LC_LOI__c' and DeveloperName = 'Other']; 
     //string recidother=NewRecTypeOther.id;
     //created Amendent leae with lookup to lease
         LC_LOI__c lcOther= new LC_LOI__c();
         lcOther.name='TestLOIOther';
         lcOther.LOI_Type__c='CAM Only Agreement';
         lcOther.MRI_Property__c=mc.id;
         lcOther.Lease__c=mylease.id;
         lcOther.Current_Date__c=system.today();
         lcOther.Other_LOI_Description__c='Thisis my description';
         //lcOther.recordtypeid=recidother;
         insert lcOther;
              
         ApexPages.StandardController stdconother = new ApexPages.StandardController(lcOther);
         LC_LOILandingClass LCcontrollerOther = new LC_LOILandingClass (stdconother );
         LCcontrollerOther.LOI_id=lcOther.id;
         LCcontrollerOther.LOI=lcOther;
         LCcontrollerOther.myprop=lcOther.MRI_Property__c;
         LCcontrollerOther.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontrollerOther.IdtoLeasevsuitmap=MYMAp;
         LCcontrollerOther.AdviserCallList();
         LCcontrollerOther.save();
         LCcontrollerOther.Cancel();
  
   LC_LOILandingClass  LCcontrollertest = new LC_LOILandingClass ();
    

    test.stoptest();
   }
   
   static testmethod void testCreatelease2()
  {
         test.starttest();
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease1';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
         system.debug('let me know'+MYMAp);
         
    //********************with record typr Lease Expansion - Renewal**********************    
     
     RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LOI__c' and DeveloperName = 'LOI_Expansion' limit 1]; 
      string recid=NewRecType.id;
    //created New LOI with look up to lease
        
         LC_LOI__c lc1= new LC_LOI__c();
         lc1.name='Test_LOI_Expansion';
         lc1.LOI_Type__c='Lease Expansion - Renewal';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.Other_LOI_Description__c='Thisis my description';       
         lc1.recordtypeid=recid;
         insert lc1;
         
           
         System.assertEquals(myLease.MRI_PROPERTY__c , mc.id);
         System.assertEquals(myLease1.Name,'Test-lease1');
         System.assertEquals( lc1.name,'Test_LOI_Expansion');
              
         ApexPages.StandardController stdcon = new ApexPages.StandardController(lc1);
         LC_LOILandingClass LCcontroller = new LC_LOILandingClass (stdcon);
         LCcontroller.LOI=lc1;
         LCcontroller.myprop=lc1.MRI_Property__c;
         LCcontroller.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontroller.IdtoLeasevsuitmap=MYMAp;
         LCcontroller.AdviserCallList();
         LCcontroller.LOI_Id=lc1.id;  
         LCcontroller.save();
         LCcontroller.Cancel();
 
   
      
        LC_LOILandingClass  LCcontrollertest = new LC_LOILandingClass ();
    

    test.stoptest();
 
    
  }


  static testmethod void testCreatelease3()
  {
         test.starttest();
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease1';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
         system.debug('let me know'+MYMAp);
         
    //********************with record typr LOI_Outparcel_Pad_Sale**********************    
     
     RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LOI__c' and DeveloperName = 'LOI_Outparcel_Pad_Sale' limit 1]; 
      string recid=NewRecType.id;
    //created New LOI with look up to lease
        
         LC_LOI__c lc1= new LC_LOI__c();
         lc1.name='Test_LOI_Expansion';
         lc1.LOI_Type__c='Outparcel/Pad Sale or New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.Other_LOI_Description__c='Thisis my description';       
         lc1.recordtypeid=recid;
         insert lc1;
         
           
         System.assertEquals(myLease.MRI_PROPERTY__c , mc.id);
         System.assertEquals(myLease1.Name,'Test-lease1');
         System.assertEquals( lc1.name,'Test_LOI_Expansion');
              
         ApexPages.StandardController stdcon = new ApexPages.StandardController(lc1);
         LC_LOILandingClass LCcontroller = new LC_LOILandingClass (stdcon);
         LCcontroller.LOI=lc1;
         LCcontroller.myprop=lc1.MRI_Property__c;
         LCcontroller.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontroller.IdtoLeasevsuitmap=MYMAp;
         LCcontroller.AdviserCallList();
         LCcontroller.LOI_Id=lc1.id;  
         LCcontroller.save();
         LCcontroller.Cancel();
 
   
      
        LC_LOILandingClass  LCcontrollertest = new LC_LOILandingClass ();
    

    test.stoptest();
 
    
  } 
  
   static testmethod void testCreatelease4()
  {
         test.starttest();
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease1';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
         system.debug('let me know'+MYMAp);
         
    //********************with record type Other**********************    
     
     RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LOI__c' and DeveloperName = 'Other_LOI' limit 1]; 
      string recid=NewRecType.id;
    //created New LOI with look up to lease
        
         LC_LOI__c lc1= new LC_LOI__c();
         lc1.name='Test_LOI_Expansion';
         lc1.LOI_Type__c='CAM Only Agreement - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.Other_LOI_Description__c='Thisis my description';       
         lc1.recordtypeid=recid;
         insert lc1;
         
           
         System.assertEquals(myLease.MRI_PROPERTY__c , mc.id);
         System.assertEquals(myLease1.Name,'Test-lease1');
         System.assertEquals( lc1.name,'Test_LOI_Expansion');
              
         ApexPages.StandardController stdcon = new ApexPages.StandardController(lc1);
         LC_LOILandingClass LCcontroller = new LC_LOILandingClass (stdcon);
         LCcontroller.LOI=lc1;
         LCcontroller.myprop=lc1.MRI_Property__c;
         LCcontroller.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontroller.IdtoLeasevsuitmap=MYMAp;
         LCcontroller.AdviserCallList();
         LCcontroller.LOI_Id=lc1.id;  
         LCcontroller.save();
         LCcontroller.Cancel();
 
   
      
        LC_LOILandingClass  LCcontrollertest = new LC_LOILandingClass ();
    

    test.stoptest();
 
    
  } 
  static testmethod void testCreatelease5()
  {
         test.starttest();
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease1';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
         system.debug('let me know'+MYMAp);
         
    //********************with record type Other**********************    
     
     RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LOI__c' and DeveloperName = 'Other_LOI' limit 1]; 
      string recid=NewRecType.id;
    //created New LOI with look up to lease
        
         LC_LOI__c lc1= new LC_LOI__c();
         lc1.name='Test_LOI_Expansion';
         lc1.LOI_Type__c='License/Vendor Agreement';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.Other_LOI_Description__c='Thisis my description';       
         lc1.recordtypeid=recid;
         insert lc1;
         
           
         System.assertEquals(myLease.MRI_PROPERTY__c , mc.id);
         System.assertEquals(myLease1.Name,'Test-lease1');
         System.assertEquals( lc1.name,'Test_LOI_Expansion');
              
         ApexPages.StandardController stdcon = new ApexPages.StandardController(lc1);
         LC_LOILandingClass LCcontroller = new LC_LOILandingClass (stdcon);
         LCcontroller.LOI=lc1;
         LCcontroller.myprop=lc1.MRI_Property__c;
         LCcontroller.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontroller.IdtoLeasevsuitmap=MYMAp;
         LCcontroller.AdviserCallList();
         LCcontroller.LOI_Id=lc1.id;  
         LCcontroller.save();
         LCcontroller.Cancel();
 
   
      
        LC_LOILandingClass  LCcontrollertest = new LC_LOILandingClass ();
    

    test.stoptest();
 
    
  }

  static testmethod void testCreatelease6()
  {
         test.starttest();
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease1';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
         system.debug('let me know'+MYMAp);
         
    //********************with record typr Lease Expansion - Renewal**********************    
     
     RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LOI__c' and DeveloperName = 'Other_LOI' limit 1]; 
      string recid=NewRecType.id;
    //created New LOI with look up to lease
        
         LC_LOI__c lc1= new LC_LOI__c();
         lc1.name='Test_CAM_Only_Agreement_New';
         lc1.LOI_Type__c='CAM Only Agreement - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.Other_LOI_Description__c='Thisis my description';       
         lc1.recordtypeid=recid;
         insert lc1;
         
           
         System.assertEquals(myLease.MRI_PROPERTY__c , mc.id);
         System.assertEquals(myLease1.Name,'Test-lease1');
         System.assertEquals( lc1.name,'Test_CAM_Only_Agreement_New');
              
         ApexPages.StandardController stdcon = new ApexPages.StandardController(lc1);
         LC_LOILandingClass LCcontroller = new LC_LOILandingClass (stdcon);
         LCcontroller.LOI=lc1;
         LCcontroller.myprop=lc1.MRI_Property__c;
         LCcontroller.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontroller.IdtoLeasevsuitmap=MYMAp;
         LCcontroller.AdviserCallList();
         LCcontroller.LOI_Id=lc1.id; 
         LCcontroller.leaseOrSuit='None'; 
         LCcontroller.save();
         LCcontroller.Cancel();
 
   
      
        LC_LOILandingClass  LCcontrollertest = new LC_LOILandingClass ();
    

    test.stoptest();
 
    
  }

  static testmethod void testCreatelease7()
  {
         test.starttest();
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease1';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
         system.debug('let me know'+MYMAp);
         
    //********************with record typr LOI_Outparcel_Pad_Sale**********************    
     
     RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LOI__c' and DeveloperName = 'LOI_Outparcel_Pad_Sale' limit 1]; 
      string recid=NewRecType.id;
    //created New LOI with look up to lease
        
         LC_LOI__c lc1= new LC_LOI__c();
         lc1.name='Test_LOI_Expansion';
         lc1.LOI_Type__c='Outparcel/Pad Sale or New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.Other_LOI_Description__c='Thisis my description';       
         lc1.recordtypeid=recid;
         insert lc1;
         
           
         System.assertEquals(myLease.MRI_PROPERTY__c , mc.id);
         System.assertEquals(myLease1.Name,'Test-lease1');
         System.assertEquals( lc1.name,'Test_LOI_Expansion');
              
         ApexPages.StandardController stdcon = new ApexPages.StandardController(lc1);
         LC_LOILandingClass LCcontroller = new LC_LOILandingClass (stdcon);
         LCcontroller.LOI=lc1;
         LCcontroller.myprop=lc1.MRI_Property__c;
         LCcontroller.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontroller.IdtoLeasevsuitmap=MYMAp;
         LCcontroller.AdviserCallList();
         LCcontroller.LOI_Id=lc1.id;  
         LCcontroller.leaseOrSuit='None'; 
         LCcontroller.save();
         LCcontroller.Cancel();
 
   
      
        LC_LOILandingClass  LCcontrollertest = new LC_LOILandingClass ();
    

    test.stoptest();
 
    
  } 
  
  static testmethod void testCreatelease8()
  {
         test.starttest();
         map<string, string>MYMAp= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.Property_ID__c='A1234';
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU1';
         mylease.Lease_ID__c='A046677';
         mylease.Lease_Status__c='Active';
         mylease.SuitVacancyIndicator__c=FALSE;
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         String myvalue=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         string firstvalue='L'+'-'+mylease.id;
         MYMAp.put(myvalue,firstvalue);
      
         Lease__c myLease1= new Lease__c();
         myLease1.name='Test-lease1';
         myLease1.Tenant_Name__c='Test-Tenant';
         myLease1.SuiteId__c='AOTYU';
         mylease1.Lease_ID__c='A046677222';
         mylease1.Lease_Status__c='InActive';
         mylease1.SuitVacancyIndicator__c=TRUE;
         myLease1.MRI_PROPERTY__c=mc.id;
         insert mylease1;
         
         String myvalue2=mylease1.Lease_ID__c+':'+mylease1.name +':'+mylease1.Lease_Status__c;
         string firstvalue2='L'+'-'+mylease1.id;
         MYMAp.put(myvalue2,firstvalue2);
         system.debug('let me know'+MYMAp);
         
    //********************with record typr New**********************    
     
     RecordType NewRecType = [Select Id From RecordType  Where SobjectType = 'LC_LOI__c' and DeveloperName = 'New_LOI' limit 1]; 
      string recid=NewRecType.id;
    //created New LOI with look up to lease
        
         LC_LOI__c lc1= new LC_LOI__c();
         lc1.name='TestNEWLOI';
         lc1.LOI_Type__c='Lease - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.Other_LOI_Description__c='Thisis my description';       
         lc1.recordtypeid=recid;
         insert lc1;
         
           
         System.assertEquals(myLease.MRI_PROPERTY__c , mc.id);
         System.assertEquals(myLease1.Name,'Test-lease1');
         System.assertEquals( lc1.name,'TestNEWLOI');
              
         ApexPages.StandardController stdcon = new ApexPages.StandardController(lc1);
         LC_LOILandingClass LCcontroller = new LC_LOILandingClass (stdcon);
         LCcontroller.LOI=lc1;
         LCcontroller.myprop=lc1.MRI_Property__c;
         LCcontroller.calllist=mylease.Lease_ID__c+':'+mylease.name +':'+mylease.Lease_Status__c;
         LCcontroller.IdtoLeasevsuitmap=MYMAp;
         LCcontroller.AdviserCallList();
         LCcontroller.LOI_Id=lc1.id;  
         LCcontroller.hideSectionOnChange();
         LCcontroller.leaseOrSuit='None';
         LCcontroller.save();
         LCcontroller.Cancel();
 
   
      
        LC_LOILandingClass  LCcontrollertest = new LC_LOILandingClass ();
    

    test.stoptest();
 
    
  } 
    
}