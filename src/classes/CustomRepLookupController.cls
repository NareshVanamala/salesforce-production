global with sharing class CustomRepLookupController 
{
    private ApexPages.StandardController controller;
    public Contact splcon {get;set;}
    public List<Contact> results{get;set;} 
    public string searchString{get;set;}
    public string reitString;
    public string newsplcon{get;set;}
    public REIT_Investment__c reit{get;set;}
    public boolean refreshPage{get;set;}
    public String searchedRecordId { get; set; }
    public string accntId{get;set;}
    public String selectedAccout{get;set;}

    public void setInputvalue()
    {
        System.debug(selectedAccout);
    }
    public list<account> getAccountList()
    {
        return [select id,name from account limit 25];
    }   
    public CustomRepLookupController(ApexPages.StandardController controller) 
    {
        reit = new REIT_Investment__c(); 
       // reit = (REIT_Investment__c)controller.getRecord();
        system.debug('reit --------'+reit);
        splcon = new Contact ();
        reitString = System.currentPageReference().getParameters().get('lksearch');
        system.debug('reitString---------'+reitString);
        if(reitString!= '' && reitString!= null)
        reit = [Select Id, Name from REIT_Investment__c where Name =: reitString];
        runSearch();  
        refreshPage=false;
        
    }
    
    public PageReference search() 
    {
        runSearch(); 
        return null;
    }
    private void runSearch() 
    {
        results = performSearch();                   
    }
    private List<Contact> performSearch() 
    {
        String queryName = '%' + searchString + '%';
         System.debug('------queryName-------------- '+queryName);
        /*List<Contact> qcon = [select id, name, Contact_Type__c,Rep_CRD__c,Phone,Relationship__c,License__c from Contact WHERE (Name like :queryName) limit 10];
        return qcon;*/
        string soql = 'select id, name, Contact_Type__c,Rep_CRD__c,Phone,Relationship__c,License__c,Account.Name  from Contact';
        if(searchString != '' && searchString != null)  
        soql = soql +  ' where name LIKE : queryName ';
        soql = soql + ' limit 100';
        System.debug(soql);
        return database.query(soql);       
    }

    public PageReference saveSplitContact() 
    {      
       if(splcon!=Null)
        {         
        try{
               
                Account objAcc=new Account();             
                if(selectedAccout!=null)
                {   
                    objAcc=[select id,name from account where name=:selectedAccout limit 1]; 
                }                                                                                                    
                if(objAcc.Id!=Null)
                system.debug('-------objAcc.Id------------'+objAcc.Id);
                splcon.AccountId=objAcc.id;
                splcon.split_contact__c= True; 
                if (Schema.sObjectType.Contact.isCreateable())
                insert splcon;
                system.debug('-------splcon------------'+splcon);
                REIT_Investment__c objReit = new REIT_Investment__c();
                objReit.id = reit.Id;
                objReit.reitsplitcontact__c = splcon.Id;
                system.debug('-------objReit.reitsplitcontact__c------------'+objReit.reitsplitcontact__c);
                if (Schema.sObjectType.REIT_Investment__c.isUpdateable()) 
                update objReit;
             }
         catch(Exception e)
            {
            }      
       return null;
       }
        //refreshPage=True;       
       return null;
      
         
    }
   
    public PageReference CancelSplitContact() 
    {
        
       splcon = new Contact();  
        return Null;
    }
    public string getFormTag() 
    {
        return System.currentPageReference().getParameters().get('frm');
    }
  
    public string getTextBox() 
    {
        return System.currentPageReference().getParameters().get('txt');
    }
    
    public String searchedRecordId1 { get; set; }
    public static list<Account> lstsearchedRecord {get;set;}  
  

}