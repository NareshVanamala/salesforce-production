@isTest
public class TestDataAccessClass1
{
 /*public static testmethod void myTestMethod1()
 {
   CallList__c cl = new CallList__c();
   cl.name = 'TestList';
   cl.Field_API__c = 'Territory__c';
   cl.Operator__c = 'equals';
   cl.Value__c ='HeartLand';
   cl.Field_API1__c = 'Phone';
   cl.Operator1__c = 'equals';
   cl.Value1__c ='(847) 336-1717';
   cl.Field_API2__c = 'Firstname';
   cl.Operator2__c = 'Starts with';
   cl.Value2__c ='t';
   cl.Field_API3__c = 'LastName';
   cl.Operator3__c = 'equals';
   cl.Value3__c='record';
   cl.Field_API4__c = 'DoNotCall';
   cl.Operator4__c = 'equals';
   cl.Value4__c ='true';
   cl.Description__c ='Hello Test record';
   cl.CampaignName__c ='70150000000JOH9';
   insert cl;
   System.assert(cl!= null);
   
   CallList__c c3 = new CallList__c();
   c3.CampaignName__c ='70150000000JOH9';
   insert c3;
   System.assert(c3!= null);
   
   contact c= new Contact();
   c.FirstName='test';
   c.lastName='record';
   c.Territory__c='HeartLand';
   c.Phone='8473361717';
   c.DoNotCall=true;
   c.ownerid=Userinfo.getuserid();
   insert c;
   System.assert(c!= null);
   
   contact c1= new Contact();
   c1.firstname='test1';
   c1.lastname='record';
   c1.Territory__c='HeartLand';
   c1.Phone='4802375787'; 
   c1.ownerid=Userinfo.getuserid();
   insert c1;
   System.assert(c1!= null);
   
   contact c2= new Contact();
   c2.firstname='test2';
   c2.lastname='record';
   c2.Territory__c='HeartLand';
   c2.Phone='70484311';
   c2.ownerid=Userinfo.getuserid(); 
   insert c2;
   System.assert(c2!= null);
   
   List<Contact> scope = new List<Contact>(); 
   scope.add(c);
   scope.add(c1);
   scope.add(c2); 
            
   System.assert(cl != null);
   System.debug('***' + cl.Field_API2__c + cl.Operator4__c + cl.Value1__c);
   Automated_Call_List__c al1 = new Automated_Call_List__c();
    //al1.name =  cl.name;
    insert al1;
    al1.TempLock__c = '';
    update al1;
    
    Manual_or_Automatic__c m = new Manual_or_Automatic__c();
     m.Manual_Assignment__c = True;
     insert m;
     
     Manual_or_Automatic__c ma = [Select Id, Name, Manual_Assignment__c from Manual_or_Automatic__c limit 1];
    
    contact_call_list__c clist = new contact_call_list__c();
     clist.Call_Complete__c = false;
     clist.CallListOrder__c = al1.id;
     clist.Contact__c = c.id;
     clist.campId__c = cl.CampaignName__c;
     insert clist;
     
  // start the batchprocess
  // List<Calllist__c> cls = [Select Id,CampaignName__c from calllist__c where ID = :cl.Id];
   Set<ID> activatebatch = new Set<ID>();
   activatebatch.add(cl.Id);
   Test.StartTest();
   try
   {
     calllistinserts1 ins = new calllistinserts1(activatebatch);
     Id batchId = Database.executeBatch(ins);
     Database.BatchableContext BC;
     ins.execute(BC,scope);
    }
   Catch(Exception e){
          }
   Test.StopTest();
   // batch process end
}    
 static testmethod void myTestMethod2()
 {
    contact c= new Contact();
   c.FirstName='test';
   c.lastName='record';
   c.Territory__c='HeartLand';
   c.Phone='8473361717';
   c.DoNotCall=true;
   c.ownerid=Userinfo.getuserid();
   insert c;
   
   
   contact csnehal= new Contact();
   csnehal.FirstName='testcsnehal';
   csnehal.lastName='recordcsnehal';
   csnehal.Territory__c='HeartLand';
   csnehal.Phone='8473361717';
   csnehal.DoNotCall=true;
   csnehal.ownerid=Userinfo.getuserid();
   insert csnehal;
     
     System.assert(c!= null);
     
     CallList__c c2 = new CallList__c();
     c2.name = 'TestList';
     c2.Field_API__c = 'Territory__c';
     c2.Operator__c = 'equals';
     c2.Value__c ='HeartLand';
     c2.Field_API1__c = 'Phone';
     c2.Operator1__c = 'equals';
     c2.Value1__c ='(847) 336-1717';
   
     c2.Description__c ='Hello Test record';
     c2.Contact_Type__c ='My Contacts';
    // cl.CampaignName__c ='70150000000JOH9';
     insert c2;
     
     System.assert(c2 != null);
     
     CallList__c c4 = new CallList__c();
     c4.CampaignName__c ='70150000000JOH9';
     insert c4;
     
     Automated_Call_List__c al = new Automated_Call_List__c();
     al.name =  c4.name;
     insert al;
     
     System.assert(al!= null);
     
      Manual_or_Automatic__c m = new Manual_or_Automatic__c();
     m.Manual_Assignment__c = True;
     insert m;
     
     Manual_or_Automatic__c ma = [Select Id, Name, Manual_Assignment__c from Manual_or_Automatic__c limit 1];
     
     
     List<contact_call_list__c> clistlist = new List<contact_call_list__c >();
    
     contact_call_list__c clist = new contact_call_list__c();
     clist.Call_Complete__c = false;
     //clist.CallListOrder__c = al.id;
     clist.Contact__c = csnehal.id;
     clistlist.add(clist);
               
        insert clistlist;  
     
      System.debug('***' + c2.Field_API2__c + c2.Operator4__c + c2.Value1__c);
     
     // start the batchprocess
         List<Calllist__c> cls = [Select Id,CampaignName__c from calllist__c where ID = :c2.Id];
         Set<ID> activatebatch = new Set<ID>();
          activatebatch.add(c2.Id);
               
              Test.StartTest();
              
               calllistinserts1 ins = new calllistinserts1(activatebatch);             
               Id batchId = Database.executeBatch(ins);
                                           
             Test.StopTest();
           
                 // batch process end
     
         
    }*/
 }