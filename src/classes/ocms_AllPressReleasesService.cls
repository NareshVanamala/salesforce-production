global with sharing class ocms_AllPressReleasesService implements cms.ServiceInterface {

    public String JSONResponse {get; set;}
    private String action {get; set;}
    private Map<String, String> parameters {get; set;}
    private ocms_AllPressReleases  wpl = new ocms_AllPressReleases();

    global ocms_AllPressReleasesService() {
        
    }

    global System.Type getType(){
        return ocms_AllPressReleasesService.class;
    }

    global String executeRequest(Map<String, String> p) {
        this.action = p.get('action');
        this.parameters = p; 
        this.LoadResponse(); 
        system.debug('this.parameters'+this.parameters);   
        return this.JSONResponse;
    }

    global void loadResponse(){
        if (this.action == 'getYearList'){
            this.getYearList();
           // system.debug('this.getYearList()'+this.getYearList() ); 
        } else if (this.action == 'getPressReleaseList'){
            this.getPressReleaseList();
        } else if (this.action == 'getResultTotal'){
            String year = parameters.get('year');
            String pressRelease = parameters.get('pressRelease');
             
            this.getResultTotal(year, pressRelease);

        } else if (this.action == 'getPropertyList'){

            String year = parameters.get('year');
            String pressRelease = parameters.get('pressRelease');
            system.debug('year'+year );    
            system.debug('pressRelease '+pressRelease );   
            this.getPropertyList(year, pressRelease);
            
        }
    }

    private void getResultTotal(String year, String pressRelease){
        this.JSONResponse = JSON.serialize(wpl.getResultTotal(year, pressRelease));
    }

    private void getPropertyList(String year, String pressRelease){
        this.JSONResponse = JSON.serialize(wpl.getPropertyList(year, pressRelease));
    }

    private void getYearList(){
        this.JSONResponse = JSON.serialize(wpl.getYearList());
        system.debug('this.JSONResponse'+this.JSONResponse );  
    }

    private void getPressReleaseList(){
        this.JSONResponse = JSON.serialize(wpl.getPressReleaseList());
    }
}