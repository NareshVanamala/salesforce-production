Public with sharing Class CMProjectsEmailTemplateCtrl {
   public String CMProjId{get;set;}
   private  list<string> bidset {get;set;}
   public  List<Bid__c> bidList{get;set;}
   public  List<Bid__c> AttbidList{get;set;}
    public CMProjectsEmailTemplateCtrl (){
       
   }
   public list<Bid__c> getbidsList(){
                bidset =  new list<string>();
                List<Bid__c>  bidList= new List<Bid__c>();
           for(Bid__c bidObj : [Select id,name From Bid__c where CM_Project__c =: CMProjId]){
                bidset.add(bidObj.id);
                bidList.add(bidObj);
               
           }
    
       return bidList;
   }
   public map<string,List<Attachment>> getAllattList()
   
   {          
     map<string,List<Attachment>> mapToAttachment = new map<string,List<Attachment>>();
               for(Attachment attObj :[Select id,ParentId,Name,Body,BodyLength From Attachment where ParentId in : bidset]){
               
                   if(mapToAttachment.get(attObj.ParentId)!=null ){
                    mapToAttachment.get(attObj.ParentId).add(attObj);
                   }
                   else {
                       mapToAttachment.put(attObj.ParentId,new LIst<Attachment>{attObj});
                   }
                  }
                  for(id bidid : bidset){
                      if(mapToAttachment.get(bidid)==null ){
                    mapToAttachment.put(bidid,new LIst<Attachment>{});
                   } 
                  }

         return  mapToAttachment;        
      }
      
 
}