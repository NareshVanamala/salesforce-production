public with sharing virtual class PC_PropContactsHelperClass{
    public list<Lease__c>InsertedPropContLeasesList{get;set;}
    public list<Lease__c>SkippedPropContLeasesList{get;set;}
    public list<MRI_PROPERTY__c>InsertedPropContBuildingsList{get;set;}
    public list<MRI_PROPERTY__c>SkippedPropContBuildingsList{get;set;}
    public map<string,string> mapLeaseIdToContacttype {get;set;} 
    public map<string,string> mapLeaseIdToSkippedContacttype {get;set;} 
    public map<string,string> mapBuildingIdToContacttype {get;set;} 
    public map<string,string> mapBuildingIdToSkippedContacttype {get;set;} 
    public PC_PropContactsHelperClass(){
           
    }
    public string insertPropConLeases(List<Lease__c> selectedleaselist,String propConID, String selPropConType, boolean MassApplyFunctionality, String PCLId){
        String duplicateContactTypeMsg='';
        String editingContactType='';
        set<ID> lsidset=new set<ID>();   
        Map<Id,list<string>> existingPropertyconatctids = new Map<Id,list<string>>();
        list<Property_Contact_Leases__c>propConLeasesInsertList=new list<Property_Contact_Leases__c>();
        list<Property_Contact_Leases__c>FinalUpdateMatchedPropContLeasesList=new list<Property_Contact_Leases__c>();
        list<Property_Contact_Leases__c>updateMatchedPropContLeasesList=new list<Property_Contact_Leases__c>();
        InsertedPropContLeasesList=new list<Lease__c>();
        SkippedPropContLeasesList=new list<Lease__c>();
        mapLeaseIdToContacttype= new map<string,string>();
        mapLeaseIdToSkippedContacttype= new map<string,string>();
        for(Lease__c lls :selectedleaselist)
            lsidset.add(lls.id);
        system.debug('lsidset'+lsidset);
        list<Property_Contact_Leases__c>Junctionobjctlist = new list<Property_Contact_Leases__c>();
        Junctionobjctlist=[select Local_Tenant_Concept__c,id,Lease__r.Tenant_Name__c,Lease__c,Property_Contacts__c,Contact_Type__c,Lease__r.MRI_Lease_ID__c,Lease__r.MRI_PROPERTY__r.Common_Name__c,Lease__r.MRI_PROPERTY__r.Property_ID__c ,Lease__r.MRI_PROPERTY__r.Tenant_Name__c  from Property_Contact_Leases__c where Lease__c in:lsidset and Contact_Type__c!=null];   
        system.debug('check the junction object list'+Junctionobjctlist);
        Map<Id,list<string>> existingPropertycontactTypes= new Map<Id,list<string>>();
        Map<Id,Property_Contact_Leases__c> matchedpropContactLeases= new Map<Id, Property_Contact_Leases__c>();
         
        for(Property_Contact_Leases__c ri:Junctionobjctlist){
            if(PCLId == ri.id)
               editingContactType = ri.Contact_Type__c;
            boolean mysresult;
            string selectedpcid='';
            selectedpcid = propConID;
            system.debug('this is propertycontact'+selectedpcid);
            string propContactid='';
            propContactid =ri.Property_Contacts__c;
            system.debug('this is lease property contact'+propContactid);
            mysresult=compareid(selectedpcid,propContactid);
            system.debug('this is the result'+mysresult);
            if(mysresult==true){
               matchedpropContactLeases.put(ri.Lease__c,ri);
               system.debug('matchedpropContactLeases...'+matchedpropContactLeases);
            }
            system.debug('existingPropertyconatctids'+existingPropertyconatctids);

            if(existingPropertyconatctids.containsKey(ri.Lease__c)){
               existingPropertyconatctids.get(ri.Lease__c).add(ri.Property_Contacts__c);
               existingPropertycontactTypes.get(ri.Lease__c).add(ri.Contact_Type__c);
               system.debug('if existingPropertyconatctids'+existingPropertyconatctids);
               system.debug('if existingPropertycontactTypes'+existingPropertycontactTypes);
            }
            else{
               existingPropertyconatctids.put(ri.Lease__c, new List<string>{ri.Property_Contacts__c});
               existingPropertycontactTypes.put(ri.Lease__c, new List<string>{ri.Contact_Type__c});
               system.debug('else existingPropertyconatctids'+existingPropertyconatctids);
               system.debug('else existingPropertycontactTypes'+existingPropertycontactTypes);
            }
            
        }    
        //system.debug('Am I getting the correct the junction object'+pc);
           
        list<string>selectedpropContacttypelist=new list<string>(); 
        // get all the contacttypes of the selected record into one list
        selectedpropContacttypelist.addAll(selPropConType.split(';'));
        system.debug('selected propcontacts....'+selectedpropContacttypelist);  
        string addContactType;
        string skippedContactType;      
        system.debug('Check the contact Type'+selectedpropContacttypelist);
        list<String>existingContactTypelist=new list<String>(); 
        set<String>existingContactTypeset= new set<String>(); 
        list<String>PropconLeasesContactTypesList= new list<String>(); 
        String PropconLeasesContactTypes='';        
        //check if, Selected property contact lease's contact type is present or not for the seleted leases.
        if(selectedpropContacttypelist.size()>0){
           system.debug('selectedleaselist.size'+selectedleaselist.size());
           for(Integer i=0 ; i< selectedleaselist.size(); i++) {
               addContactType='';
               existingContactTypelist.clear();
               existingContactTypeset.clear();
               if(existingPropertycontactTypes.get(selectedleaselist[i].id)!=null){
                  existingContactTypeset.addAll(existingPropertycontactTypes.get(selectedleaselist[i].id));
                  existingContactTypelist.addAll(existingContactTypeset);                  
                  system.debug('If existingContactTypelist'+existingContactTypelist); 
                  system.debug('selectedleaselist[i].id'+selectedleaselist[i].id);                    
               }
               system.debug('existingContactTypelist'+existingContactTypelist);
               PropconLeasesContactTypes='';           
               PropconLeasesContactTypes = getuniqueContacttypes(selectedpropContacttypelist,existingContactTypelist);
               system.debug('PropconLeasesContactTypes'+PropconLeasesContactTypes);                
               PropconLeasesContactTypesList=PropconLeasesContactTypes.split('&&&&');
               addContactType= PropconLeasesContactTypesList[0];
               skippedContactType= PropconLeasesContactTypesList[1];
               
               if(skippedContactType != null && skippedContactType != '' && skippedContactType != 'skippedContactType'){
                  list<string> duplicateContactTypesList = skippedContactType.split(';');
                  list<string> editingContactTypeList = editingContactType.split(';');
                  list<string> duplicateContactTypeMsgList = new list<string>();
                  Set<String> editingContactTypeSet = new Set<String>(editingContactTypeList);
                  for(Integer k=0 ; k< duplicateContactTypesList.size(); k++){
                      if(!editingContactTypeSet.contains(duplicateContactTypesList[k])){
                         duplicateContactTypeMsg = duplicateContactTypeMsg+duplicateContactTypesList[k]+','; 
                         duplicateContactTypeMsgList.add(duplicateContactTypesList[k]);
                         system.debug('else duplicateContactTypeMsg'+duplicateContactTypeMsg);
                      }                   
                  }
                  duplicateContactTypeMsg = duplicateContactTypeMsg.removeEnd(',');
                  if(duplicateContactTypeMsgList.size()==0 && addContactType == 'addContactType')
                     addContactType = skippedContactType;
                  if(duplicateContactTypeMsgList.size()>1)
                     duplicateContactTypeMsg = duplicateContactTypeMsg + ' contact types are being duplicated';
                  else if(duplicateContactTypeMsgList.size() == 1)
                     duplicateContactTypeMsg = duplicateContactTypeMsg + ' contact type is being duplicated';
               }               
               system.debug('return addContactType'+addContactType);
               system.debug('return skippedContactType'+skippedContactType);                
                            
               if(addContactType!='' && addContactType!= null && addContactType!='addContactType'){
                  system.debug('if addContactType'+addContactType); 
                  mapLeaseIdToContacttype.put(selectedleaselist[i].id,addContactType);
                  if(matchedpropContactLeases.get(selectedleaselist[i].id)!=null){
                     system.debug('if selectedleaselist[i]'+selectedleaselist[i]);
                     Property_Contact_Leases__c updateMatchedPropContLeases = matchedpropContactLeases.get(selectedleaselist[i].id);
                     if((addContactType == skippedContactType) || (editingContactType != null && editingContactType != '' && skippedContactType == 'skippedContactType'))
                        updateMatchedPropContLeases.Contact_Type__c = addContactType; 
                     else
                        updateMatchedPropContLeases.Contact_Type__c += ';'+addContactType;                   
                     FinalUpdateMatchedPropContLeasesList.add(updateMatchedPropContLeases);
                     InsertedPropContLeasesList.add(selectedleaselist[i]);                       
                     system.debug('FinalUpdateMatchedPropContLeasesList'+FinalUpdateMatchedPropContLeasesList);
                  }
                  else if(matchedpropContactLeases.get(selectedleaselist[i].id)==null){
                     system.debug('else if selectedleaselist[i]'+selectedleaselist[i]);
                     Property_Contact_Leases__c pcl=new Property_Contact_Leases__c();
                     pcl.Property_Contacts__c=propConID;
                     pcl.Contact_Type__c=addContactType;
                     pcl.Lease__c=selectedleaselist[i].id;
                     propConLeasesInsertList.add(pcl);
                     InsertedPropContLeasesList.add(selectedleaselist[i]);   
                     system.debug('else if propConLeasesInsertList'+propConLeasesInsertList); 
                  }
               }
               else{
                  skippedPropContLeasesList.add(selectedleaselist[i]);
                  system.debug('skippedPropContLeasesList'+skippedPropContLeasesList);
                  mapLeaseIdToSkippedContacttype.put(selectedleaselist[i].id,skippedContactType);
               }
               if(skippedContactType!='' && skippedContactType!= null && skippedContactType!= 'skippedContactType' && addContactType!='' && addContactType!= null && addContactType!= 'addContactType'){
                  skippedPropContLeasesList.add(selectedleaselist[i]);
                  mapLeaseIdToSkippedContacttype.put(selectedleaselist[i].id,skippedContactType);
               }
           }
           if((MassApplyFunctionality == false && (duplicateContactTypeMsg == null || duplicateContactTypeMsg == '')) || MassApplyFunctionality == true){
              if(FinalUpdateMatchedPropContLeasesList.size()>0){
                 if(Schema.sObjectType.Property_Contact_Leases__c.isUpdateable()) 
                    update FinalUpdateMatchedPropContLeasesList;
                 system.debug('FinalUpdateMatchedPropContLeasesList'+FinalUpdateMatchedPropContLeasesList);
              }
              system.debug('propConLeasesInsertList'+propConLeasesInsertList);    
              if(propConLeasesInsertList.size()>0){
                 if(Schema.sObjectType.Property_Contact_Leases__c.isCreateable())
                    insert propConLeasesInsertList;
              } 
           }
        }
        return duplicateContactTypeMsg;     
    } 
    public String insertPropConBuildings(List<MRI_PROPERTY__c> selectedMRIList,String propConID, String selPropConType, boolean MassApplyFunctionality, String PCBId){
        String duplicateContactTypeMsg='';
        String editingContactType='';
        set<ID>MRIIdSet=new set<ID>();   
        Map<Id,list<string>> existingPropertyconatctids = new Map<Id,list<string>>();
        list<Property_Contact_Buildings__c>propConBuildingsInsertList=new list<Property_Contact_Buildings__c>();
        list<Property_Contact_Buildings__c>FinalUpdateMatchedPropContBuildingsList=new list<Property_Contact_Buildings__c>();
        InsertedPropContBuildingsList= new list<MRI_PROPERTY__c>();
        SkippedPropContBuildingsList= new list<MRI_PROPERTY__c>();
        mapBuildingIdToContacttype = new map<string,string>();
        mapBuildingIdToSkippedContacttype= new map<string,string>();
        for(MRI_PROPERTY__c mri :selectedMRIList)
            MRIIdSet.add(mri.id);
        system.debug('MRIIdSet'+MRIIdSet);
        list<Property_Contact_Buildings__c>PropContBuildingsList = new list<Property_Contact_Buildings__c>();
        PropContBuildingsList=[select id,Local_Tenant_Concept__c,Property_Contacts__c,Contact_Type__c,Center_Name__c  from Property_Contact_Buildings__c where Center_Name__c in:MRIIdSet and Contact_Type__c!=null];   
        system.debug('check the junction object list'+PropContBuildingsList);
        Map<Id,list<string>> existingPropertycontactTypes= new Map<Id,list<string>>();
        Map<Id,Property_Contact_Buildings__c> matchedpropContactBuildings= new Map<Id, Property_Contact_Buildings__c>();
         
        for(Property_Contact_Buildings__c ri:PropContBuildingsList){
            if(PCBId == ri.id)
               editingContactType = ri.Contact_Type__c;
            boolean mysresult;
            string selectedpcid='';
            selectedpcid = propConID;
            system.debug('this is propertycontact'+selectedpcid);
            string propContactid='';
            propContactid =ri.Property_Contacts__c;
            system.debug('this is lease property contact'+propContactid);
            mysresult=compareid(selectedpcid,propContactid);
            system.debug('this is the result'+mysresult);
            if(mysresult==true){
               matchedpropContactBuildings.put(ri.Center_Name__c,ri);
               system.debug('matchedpropContactBuildings...'+matchedpropContactBuildings);
            }
            system.debug('existingPropertyconatctids'+existingPropertyconatctids);

            if(existingPropertyconatctids.containsKey(ri.Center_Name__c)){
               existingPropertyconatctids.get(ri.Center_Name__c).add(ri.Property_Contacts__c);
               existingPropertycontactTypes.get(ri.Center_Name__c).add(ri.Contact_Type__c);   
               system.debug('if existingPropertyconatctids'+existingPropertyconatctids);
               system.debug('if existingPropertycontactTypes'+existingPropertycontactTypes);
            }
            else{
               existingPropertyconatctids.put(ri.Center_Name__c, new List<string>{ri.Property_Contacts__c});
               existingPropertycontactTypes.put(ri.Center_Name__c, new List<string>{ri.Contact_Type__c});
               system.debug('else existingPropertyconatctids'+existingPropertyconatctids);
               system.debug('else existingPropertycontactTypes'+existingPropertycontactTypes);
            }
            
        }    
        //system.debug('Am I getting the correct the junction object'+pc);
           
        list<string>selectedpropContacttypelist=new list<string>(); 
        // get all the contacttypes of the selected record into one list
        selectedpropContacttypelist.addAll(selPropConType.split(';'));
        system.debug('selected propcontacts....'+selectedpropContacttypelist);  
        string addContactType;   
        string skippedContactType;
        system.debug('Check the contact Type'+selectedpropContacttypelist);
        list<String>existingContactTypelist=new list<String>(); 
        set<String>existingContactTypeset= new set<String>(); 
        list<String>PropConBuildingsContactTypesList= new list<String>(); 
        String PropConBuildingsContactTypes='';     
        //check if, Selected property contact lease's contact type is present or not for the seleted leases.
        if(selectedpropContacttypelist.size()>0){
           system.debug('selectedMRIList.size'+selectedMRIList.size());
           for(Integer i=0 ; i< selectedMRIList.size(); i++){
               addContactType='';
               existingContactTypelist.clear();
               existingContactTypeset.clear();
               if(existingPropertycontactTypes.get(selectedMRIList[i].id)!=null){
                  existingContactTypeset.addAll(existingPropertycontactTypes.get(selectedMRIList[i].id));
                  existingContactTypelist.addAll(existingContactTypeset);                                 
               }
               system.debug('existingContactTypelist'+existingContactTypelist);           
               PropConBuildingsContactTypes = getuniqueContacttypes(selectedpropContacttypelist,existingContactTypelist);
               system.debug('PropconBuildingsContactTypes'+PropConBuildingsContactTypes+'test');               
               PropConBuildingsContactTypesList=PropConBuildingsContactTypes.split('&&&&');
               addContactType= PropConBuildingsContactTypesList[0];
               skippedContactType= PropconBuildingsContactTypesList[1];
               system.debug('skippedContactType444'+skippedContactType);  
               if(skippedContactType != null && skippedContactType != '' && skippedContactType != 'skippedContactType'){
                  list<string> duplicateContactTypesList = skippedContactType.split(';');
                  list<string> editingContactTypeList = editingContactType.split(';');
                  list<string> duplicateContactTypeMsgList = new list<string>();
                  Set<String> editingContactTypeSet = new Set<String>(editingContactTypeList);
                  for(Integer k=0 ; k< duplicateContactTypesList.size(); k++){
                      if(!editingContactTypeSet.contains(duplicateContactTypesList[k])){
                         duplicateContactTypeMsg = duplicateContactTypeMsg+duplicateContactTypesList[k]+','; 
                         duplicateContactTypeMsgList.add(duplicateContactTypesList[k]);
                         system.debug('else duplicateContactTypeMsg'+duplicateContactTypeMsg);
                      }                 
                  }               
                  duplicateContactTypeMsg = duplicateContactTypeMsg.removeEnd(',');
                  if(duplicateContactTypeMsgList.size()==0 && addContactType == 'addContactType')
                     addContactType = skippedContactType;
                  if(duplicateContactTypeMsgList.size()>1)
                     duplicateContactTypeMsg = duplicateContactTypeMsg + ' contact types are being duplicated';
                  else if(duplicateContactTypeMsgList.size() == 1)
                     duplicateContactTypeMsg = duplicateContactTypeMsg + ' contact type is being duplicated';
               }   
               system.debug('return addContactType'+addContactType);
               system.debug('return skippedContactType'+skippedContactType);                                   
               if(addContactType!='' && addContactType!= null && addContactType!= 'addContactType'){
                  system.debug('if addContactType'+addContactType);  
                  mapBuildingIdToContacttype.put(selectedMRIList[i].id,addContactType);
                  if(matchedpropContactBuildings.get(selectedMRIList[i].id)!=null){
                     system.debug('if selectedMRIList[i]'+selectedMRIList[i]);
                     Property_Contact_Buildings__c updateMatchedPropContBuildings = matchedpropContactBuildings.get(selectedMRIList[i].id);
                     if((addContactType == skippedContactType) || (editingContactType != null && editingContactType != '' && skippedContactType == 'skippedContactType'))
                        updateMatchedPropContBuildings.Contact_Type__c = addContactType;    
                     else
                        updateMatchedPropContBuildings.Contact_Type__c += ';'+addContactType; 
                     FinalUpdateMatchedPropContBuildingsList.add(updateMatchedPropContBuildings);
                     InsertedPropContBuildingsList.add(selectedMRIList[i]);                       
                     system.debug('FinalUpdateMatchedPropContBuildingsList'+FinalUpdateMatchedPropContBuildingsList);
                  }
                  else if(matchedpropContactBuildings.get(selectedMRIList[i].id)==null){
                     system.debug('else if selectedMRIList[i]'+selectedMRIList[i]);
                     Property_Contact_Buildings__c pcb=new Property_Contact_Buildings__c();
                     pcb.Property_Contacts__c=propConID;
                     pcb.Contact_Type__c=addContactType;
                     pcb.Center_Name__c=selectedMRIList[i].id;
                     propConBuildingsInsertList.add(pcb);
                     InsertedPropContBuildingsList.add(selectedMRIList[i]);   
                     system.debug('else if propConBuildingsInsertList'+propConBuildingsInsertList); 
                  }
               }
               else{
                  SkippedPropContBuildingsList.add(selectedMRIList[i]);
                  mapBuildingIdToSkippedContacttype.put(selectedMRIList[i].id,skippedContactType);
                  system.debug('SkippedPropContBuildingsList'+SkippedPropContBuildingsList);
               }
               system.debug('InsertedPropContBuildingsList'+InsertedPropContBuildingsList);
               if(skippedContactType!='' && skippedContactType!= null && skippedContactType!= 'skippedContactType' && addContactType!='' && addContactType!= null && addContactType!= 'addContactType'){
                  SkippedPropContBuildingsList.add(selectedMRIList[i]);
                  mapBuildingIdToSkippedContacttype.put(selectedMRIList[i].id,skippedContactType);
               }
           } 
           if((MassApplyFunctionality == false && (duplicateContactTypeMsg == null || duplicateContactTypeMsg == '')) || MassApplyFunctionality == true){
              if(FinalUpdateMatchedPropContBuildingsList.size()>0){
                 if(Schema.sObjectType.Property_Contact_Buildings__c.isUpdateable()) 
                    update FinalUpdateMatchedPropContBuildingsList;
                 system.debug('FinalUpdateMatchedPropContBuildingsList'+FinalUpdateMatchedPropContBuildingsList);
              } 
              if(propConBuildingsInsertList.size()>0){
                 if(Schema.sObjectType.Property_Contact_Buildings__c.isCreateable())
                    insert propConBuildingsInsertList;
              } 
           }
        }
        return duplicateContactTypeMsg;     
    } 
            
    public string getuniqueContacttypes(list<string>selpropContypelist,list<string>existingPropConTypelist) {
        set<string>splitExistingPropConTypesSet = new set<string>();  
        system.debug('this is my selpropContypelist'+selpropContypelist);
        system.debug('this is my existingPropConTypelist'+existingPropConTypelist);
        for(Integer k=0;k<existingPropConTypelist.size();k++){
            splitExistingPropConTypesSet.addAll(existingPropConTypelist[k].split(';'));
        }
        existingPropConTypelist.clear();
        existingPropConTypelist.addAll(splitExistingPropConTypesSet);
        system.debug('final existingPropConTypelist'+existingPropConTypelist);
        system.debug('final existingPropConTypelist.size()'+existingPropConTypelist.size());
        list<string>AddToConTypesList = new list<String>();
        list<string>skippedConTypesList = new list<String>();
        set<string>AddToConTypesSet=new set<String>();
        set<string>skipppedToConTypesSet=new set<String>();
        integer count=0; 
        string addContactType='';
        string skippedContactType='';
        for(Integer j=0;j<selpropContypelist.size();j++){
            count=0;
            for(Integer i=0;i<existingPropConTypelist.size();i++){
                system.debug('existingPropConTypelist[i]'+existingPropConTypelist[i]);
                system.debug('selpropContypelist[j]'+selpropContypelist[j]);
                if(selpropContypelist[j]==existingPropConTypelist[i])
                   count=count+1;
            }
            if(count==0) 
               AddToConTypesList.add(selpropContypelist[j]);
            else
               skippedConTypesList.add(selpropContypelist[j]);                    
                 
        }
        AddToConTypesSet.addAll(AddToConTypesList);
        system.debug('AddToConTypesSet'+AddToConTypesSet);
        AddToConTypesList.clear();
        AddToConTypesList.addAll(AddToConTypesSet);
        skipppedToConTypesSet.addAll(skippedConTypesList);
        skippedConTypesList.clear();
        skippedConTypesList.addAll(skipppedToConTypesSet);
        system.debug('AddToConTypesList'+AddToConTypesList);
        system.debug('skippedConTypesList'+skippedConTypesList);
          
        for(String s:AddToConTypesList){
            system.debug('this is my string'+s);
            addContactType = addContactType+s+';';
        } 
        for(String s1:skippedConTypesList){
            system.debug('this is my string'+s1);
            skippedContactType = skippedContactType+s1+';';
        }       
        addContactType = addContactType.removeEnd(';');
        if(addContactType == '' || addContactType == null)
           addContactType = 'addContactType';
        skippedContactType = skippedContactType.removeEnd(';');
        if(skippedContactType == '' || skippedContactType == null)
           skippedContactType = 'skippedContactType';
        system.debug('addContactType'+addContactType);
        system.debug('skippedContactType'+skippedContactType);
        return addContactType+'&&&&'+skippedContactType;
    }  
    public boolean compareid(string checkme, string yourid){
        boolean mycheck;
        if(checkme.length()>15)
           checkme=checkme.substring(0,15);
        system.debug('here is the checkme'+checkme);
        if(yourid.length()>15)
           yourid= yourid.substring(0,15);
        system.debug('here is the yourid'+yourid);
        if(checkme.equals(yourid))
           mycheck=true;
        else
           mycheck=false;
        system.debug('here is the tue'+mycheck);
        return mycheck;
  }  
 }