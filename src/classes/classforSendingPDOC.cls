global class classforSendingPDOC implements Database.Batchable<SObject>,Database.Stateful
{
       global String Query;
       global list<Contact_InvestorList__c >processinglist;
       global string contactid;
       global String investmentlistid;
       
              
   global classforSendingPDOC ( )  
   {  
          processinglist=new list<Contact_InvestorList__c>();
         Query='select id,Send_Pdoc_complete__c,Advisor_Name__c,name from Contact_InvestorList__c where Send_Pdoc_complete__c=false and  Pdoc_Ready__c=true order by Advisor_Name__c'; 
        //return database.getQueryLocator(query);   
        system.debug('What is the problem'+Query);
      
     }
     
      global Database.QueryLocator start(Database.BatchableContext BC)  
    {  
      system.debug('This is start method');
      system.debug('Myquery is......'+Query);
      return Database.getQueryLocator(Query);  
    }  
      global void execute(Database.BatchableContext BC,List<Contact_InvestorList__c> Scope)  
      { 
          
           system.debug('scope...'+Scope);
          //first delet  the attachment list of the email template
           EmailTemplate et=[SELECT DeveloperName,Id FROM EmailTemplate WHERE Name = 'PDoc'];
           system.debug('check me'+et);
           list<Attachment>Deleteattacemplist=new list<Attachment>();
           if(et!=null)
           Deleteattacemplist=[select Body,BodyLength,ContentType,Description,Id,Name,OwnerId,ParentId FROM Attachment where ParentId=:et.id]; 
          
          //system.debug('First check'+Deleteattacemplist);
            if(Deleteattacemplist.size()>0)
              delete Deleteattacemplist;
           
          //system.debug('First check'+Deleteattacemplist.size());
          //finished deleting the atatchmentlist from emial template  
        
         //Now get the first four attachment list of the contact and get the attachment of those 4 list records.
          
           set<id>contactidset= new set<id>();
           //first get the id of the contact of the Investor list
           for(Contact_InvestorList__c cty:Scope)
           {
            contactidset.add(cty.Advisor_Name__c);
            }
         //got the id of the investor list
          //Now lets get the first four list records of the contact.
          //list<Contact_InvestorList__c >processinglist=new list<Contact_InvestorList__c>();
          processinglist=[select id,Send_Pdoc_complete__c,Advisor_Name__c from Contact_InvestorList__c where Send_Pdoc_complete__c=false and  Pdoc_Ready__c=true and Advisor_Name__c in:contactidset  order by Advisor_Name__c limit 4 ];
          system.debug('lets check the size of the list'+processinglist.size());
          list<Contact>clist=[select id,Trigger_PDoc_Email__c,PDOc_Ready__c,Sending_PDoc_Complete__c from Contact where id in:contactidset];
          if(processinglist !=NULL && processinglist.size()>0)
          {
          //now get the ids of the Investor list of the attachments of invsetor list 
           set<id>investorsetid=new set<id>();
            for(Contact_InvestorList__c inv:processinglist)
              investorsetid.add(inv.id);
                //got the ids.
          //now get all the attachments of the same
          list<Attachment>Invsetorlistattchments=new list<Attachment>();
          Invsetorlistattchments=[select Body,BodyLength,ContentType,Description,Id,Name,OwnerId,ParentId FROM Attachment where ParentId in:investorsetid];
          system.debug('check the list size'+Invsetorlistattchments);
         //got the attachments. Now loop through these attachments and create new attachments for template.
          
           list<Attachment>insertlistAttchment= new list<Attachment>();
           for(Attachment acty:Invsetorlistattchments)
           {
              
               attachment a = new attachment();
               a.name=acty.name;
               a.body=acty.body;
               a.parentid=et.id;
               insertlistAttchment.add(a);  
           }
          
          insert insertlistAttchment;
          //now get the contact and make the Trigger Pdoc email flag true.
          list<Contact>Updatelist=new list<Contact>();
        
          for(Contact c:clist)
          {
            c.Trigger_PDoc_Email__c =true;
                Updatelist.add(c);
          
          }
          update  Updatelist;
          contactid=scope[0].Advisor_Name__c; 
          //investmentlistid=scope[0].id;
          Contact C=clist[0];
         
        //****************update the invsetment list************
         list<Contact_InvestorList__c > CISlist=new list<Contact_InvestorList__c >();
         for(Contact_InvestorList__c tyui:processinglist)
         {
             tyui.Send_Pdoc_complete__c=true;  
             CISlist.add(tyui);
         
         }
         update CISlist;
         system.debug('<<<<<<<>>>>>>>>'+CISlist);
        // *********************now check whether any INvestmentlist still pending for the contact.
       // Task T=new Task();
      /*  list<Contact_InvestorList__c >mylist=[select id,Send_Pdoc_complete__c,Advisor_Name__c from Contact_InvestorList__c where Send_Pdoc_complete__c=false and Pdoc_Ready__c=true and Advisor_Name__c ='0035000000KIKhP'];
        if(mylist.size()==0)
        {
         //T=[select id, status,subject,whoid from task where subject='Pdoc Requested' and whoid=:contactid and status='In Progress' limit 1];
         C.PDOc_Ready__c=false;
         C.Sending_PDoc_Complete__c=true;
         //c.Trigger_PDoc_Email__c =false;
         update C;
                   
         
        }
        */
         
     }
     
     else {  
         if(clist.size()>0)
            { 
            Contact C=clist[0]; 
            datetime myDateTime = datetime.now();  
            //date myDate = date.today();  
             C.PDOc_Ready__c=false;
             C.Sending_PDoc_Complete__c=true;
             c.Pdoc_Email_delivery__c=false;
             c.Pdoc_Email_Delivery_Date__c=myDateTime;
             //c.Trigger_PDoc_Email__c =false;
             update C;
             
             }
     
     }  
    }  
    global void finish(Database.BatchableContext BC)  
    { 
        //AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
        //system.debug('This is my scope'+processinglist[0]);
        //get the invsetment list and update them with pdoc complete true.
       //system.debug('This is my'+contactid );
       //system.debug(investmentlistid);
       //system.debug(processinglist);
       //Now update the processing list as  Send_Pdoc_complete__c=true
       /*list<Contact_InvestorList__c >Completeupdatedlist=new list<Contact_InvestorList__c >();
        Contact C=[select id,Trigger_PDoc_Email__c from Contact where id=:contactid ];
         system.debug('This is my processinglist');
          for(Contact_InvestorList__c yut:processinglist)
          {
              yut.Send_Pdoc_complete__c=true;  
              Completeupdatedlist.add(yut); 
          }
           update Completeupdatedlist;
       
        list<Contact_InvestorList__c >checkedtheTruelist=[select id,Send_Pdoc_complete__c,Advisor_Name__c from Contact_InvestorList__c where Send_Pdoc_complete__c=false and Pdoc_Ready__c=true and Advisor_Name__c =:contactid];   
        
        if(checkedtheTruelist.size()>0) 
        {
          C.Trigger_PDoc_Email__c =false;
          update C; 
    
         }*/
    }
    
  }