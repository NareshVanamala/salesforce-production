@isTest(SeeAllData=false)
private class TenantDetailctrl_Test {
    static testMethod void TenantDetailctrlTest(){
        
         
        MRI_PROPERTY__c  mo = new MRI_PROPERTY__c () ;
        mo.Address__c ='2450 Atlanta Hwy Ste 703' ;
        mo.City__c='Atlanta';
        mo.Zip_code__c='30040';
        mo.State__c='GA';
        mo.Country__c= 'USA';  
        mo.Name ='Name' ;
        mo.AvgLeaseTerm__c  = 12 ;
        mo.Long_Description__c  = 'test' ; 
        mo.Web_Ready__c='Approved';
        mo.status__c='Owned';
        mo.Program_Id__c='ARCP';
        mo.Property_ID__c='PL1234789'; 
       // mo.Related_Resources__c='Testing';
        insert mo ; 
        Property_Contacts__c propCon = new Property_Contacts__c();
        propCon.Name = 'Sample PropCon';
        propCon.Email_Address__c ='SamplePropCon@company.com';
        propCon.MRI_Property__c = mo.id;
        propCon.OwnerId =UserInfo.getuserid();
        insert propCon;
         Lease__c lease= new Lease__c();
        lease.Name='PL3A441'; 
        lease.MRI_PROPERTY__c=mo.id;
        lease.Lease_ID__c='PL3A441_1234_A';
        lease.MRI_PM__c='00550000002tACW';
        lease.Lease_Status__c='Active';
        lease.SuitVacancyIndicator__c= FALSE;
        lease.Sales_Report_Required__c =true;
        lease.Industry__c='Multi Tenant';
        lease.LeaseType__c='Rent';
        lease.Moodys__c='test';
        lease.NYSE__c='test';
        lease.S_P__c='test';
        lease.SuiteId__c='A123';
        lease.Tenant_ID__c='A1234';
        lease.Tenant_Name__c='Macys';
        lease.web_Ready__c='Approved';
        insert lease;  
       
             System.assertEquals( lease.MRI_PROPERTY__c,mo.id);

        Property_Contact_Leases__c propleases = new Property_Contact_Leases__c();
        propleases.Lease__c=lease.id;
        propleases.Property_Contacts__c=propCon.id;
        propleases.Contact_Type__c='Financials;Gross Sales';
        insert propleases;
        Integer CurrentYear = Date.Today().Year();
        Integer PreviousYear = CurrentYear-1;
        Integer twoYearsBack = PreviousYear-1;
        Integer threeYearsBack = twoYearsBack-1;
        Integer fourYearsBack = threeYearsBack-1;
       Lease_Sales_Data__c grossSalesObj = new Lease_Sales_Data__c();
       grossSalesObj.Lease__c = lease.id;
       grossSalesObj.Name = 'Test Gross';
        grossSalesObj.SalesType__c = 'GROSS';
        
        grossSalesObj.Period__c  = Date.Today()-4;
        insert grossSalesObj;
        
        Financial_Data__c financialObj =new Financial_Data__c();
        financialObj.Lease__c = lease.id;
        financialObj.Period__c  = Date.Today()-4;
        insert financialObj;
        String propconid = apexpages.currentpage().getparameters().put('id' , propCon.id);
        TenantDetailctrl updateTenClass = new TenantDetailctrl();
        updateTenClass.getFunds();
        updateTenClass.getAvgLeaseTerm();
        updateTenClass.getPropManagers();
        updateTenClass.doSearch();
        updateTenClass.PropCronSearch();
        updateTenClass.tosave();
        updateTenClass.generateExcel();
        updateTenClass.CurrentPage();
        updateTenClass.exportToExcel();
        //updateTenClass.next();
        updateTenClass.previous();
        updateTenClass.propcontactsfilter(true,false,0,0,'ARCP',null,propconid,null,null,null,null);
    }
}