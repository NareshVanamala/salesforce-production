global virtual with sharing class ocms_WebCast_DetailPage extends cms.ContentTemplateController
{
    @TestVisible private List<SObject>ProductList;
    @TestVisible private List<SObject>ProductList1;
    private String html;
    public id urlid;
    public id urlid1;
   /* Service methods */
    public List<SObject> getProductList()
    {
        urlid= System.currentPageReference().getParameters().get('id');
        //urlid='a5MP000000000QT';
       // WebCast__c webacst[select id from WebCast__c where id=]
        List<SObject> ProductList;
        String query= 'select Title__c,id,Campaigns__c,Contacts__c,Description__c,meeting_ID__c,Start_Date_Time__c,Status__c from WebCast__c where id=:urlid';
        ProductList= Database.query(query);
        return ProductList;
       // return sanitizeList(ProductList);
    }
     
    public List<SObject> getProductList1()
    {
        urlid1= System.currentPageReference().getParameters().get('str');
        //urlid='a5MP000000000QT';
       // WebCast__c webacst[select id from WebCast__c where id=]
        List<SObject> ProductList;
        String query= 'select Title__c,id,Campaigns__c,Contacts__c,Description__c,meeting_ID__c,Start_Date_Time__c,Status__c from WebCast__c where id=:urlid1';
        ProductList= Database.query(query);
        return ProductList;
       // return sanitizeList(ProductList);
    }
    
   /* private List<SObject> sanitizeList(List<SObject> inList, String field)
    {
        Integer ctr = 0;
        while(ctr < inList.size())
        {
            if(inList[ctr].get(field) == null)
            {
                inList.remove(ctr);
            } 
            else 
            {
                ctr++;
            }
         }
         return inList;
    }*/
    
      /* Create Content */
   @TestVisible private String writeControls()
    {
        String html = '';
        return html;
    }
    
    @TestVisible private String writeListView()
     {
         String html = '';
         for(SObject pl : ProductList)
         {
            html+='<article class="topMar7 wrapper teer3">'+
            '<h1>Webcast</h1>'+
            '<div class="newsBlock">';
            if(pl.get('Start_Date_Time__c')!=null)
            {
            html+='<p><strong>Date/time: </strong>'+ pl.get('Start_Date_Time__c')+'<br />';
            }
            if(pl.get('Title__c')!=null)
            {
            html+='<strong>title: </strong>'+ pl.get('Title__c')+'<br />';
            }
            if(pl.get('Description__c')!=null)
            {
            html+='<strong>Detailed Description: </strong>'+ pl.get('Description__c')+'<br />';
            }
            if(pl.get('Status__c')!=null)
            {
            html+='<strong>Status: </strong>'+ pl.get('Status__c')+'<br />';
            }
            if(pl.get('meeting_ID__c')!=null)
            {
            html+='<strong>Meeting ID: </strong>'+pl.get('meeting_ID__c')+'<br />';
            }
            html+='</div>'+
            '</article>';
         } 
            return html;
     }
     
    @TestVisible private String writeListView1()
     {
         String html = '';
         for(SObject pl : ProductList1)
         {
            html+='<article class="topMar7 wrapper teer3">'+
            '<h1>Events</h1>'+
            '<div class="newsBlock">';
            if(pl.get('Start_Date_Time__c')!=null)
            {
            html+='<p><strong>Date/time: </strong>'+ pl.get('Start_Date_Time__c')+'<br />';
            }
            if(pl.get('Title__c')!=null)
            {
            html+='<strong>title: </strong>'+ pl.get('Title__c')+'<br />';
            }
            if(pl.get('Description__c')!=null)
            {
            html+='<strong>Detailed Description: </strong>'+ pl.get('Description__c')+'<br />';
            }
            if(pl.get('Status__c')!=null)
            {
            html+='<strong>Status: </strong>'+ pl.get('Status__c')+'<br />';
            }
            if(pl.get('meeting_ID__c')!=null)
            {
            html+='<strong>Meeting ID: </strong>'+pl.get('meeting_ID__c')+'<br />';
            }
            html+='</div>'+
            '</article>';
         } 
            return html;
     }
        
    
        
        /*private String writeJavascript()
        {
              String html = '';
            html += '<script>' +
            ' getProductList();' +
            ' function getProductList(){' +
            ' var data = {};' +
            ' data["action"] = "getProductList";' +
            ' $.orchestracmsRestProxy.doAjaxServiceRequest(\'ocms_WebProductService\', data, function(bSuccess, response){' +
            ' console.log(response);' +
            '</script>';
             return html;
        
        }*/
        
        global override virtual String getHTML()
        {
            String html = '';
            
            html += writeControls();
            ProductList= getProductList();
            if(ProductList.size()>0)
            {
            html += writeListView();
            }
            ProductList1= getProductList1();
            if(ProductList1.size()>0)
            {
            html += writeListView1();
            }
            
            return html;
        }

}