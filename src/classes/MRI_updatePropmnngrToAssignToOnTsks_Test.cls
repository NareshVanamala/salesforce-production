@isTest
public class MRI_updatePropmnngrToAssignToOnTsks_Test{
  static testMethod void MRI_updateMethod(){
        MRI_Property__c mriProp = new MRI_Property__c();
        mriProp.Name = 'Sample PropCon';
        mriProp.AvgLeaseTerm__c =72.00;
        mriProp.Property_Id__c ='12345';
        mriProp.OwnerId =UserInfo.getuserid();
        mriProp.Property_Manager__c=UserInfo.getuserid();
        insert mriProp;
        list<string> MRIId = new list<string>();
        MRIId.add(mriProp.id);
        Task tsk = new Task();
        tsk.whatid = mriProp.id;
        tsk.subject='test';
        tsk.status = 'New';
        tsk.IsRecurrence = False;
        //tsk.Admin_Task__c = False;
        tsk.RecordTypeId='01238000000E9nb';
        insert tsk;
        
        MRI_updatePropmanagerToAssignedToOnTasks.MRI_updatePropmanagerToAssignedToOnTasks(MRIId);
   }

}