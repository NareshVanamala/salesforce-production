@isTest
private class createKitRequestTest {
    static testMethod void createKitRequestTest() {
        Account a = new Account(name = 'Test Account', X1031_Selling_Agreement__c = 'CCPTIII - Add On');
        insert a;
        Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', Accountid = a.id);
        insert c;
               System.assertNotEquals( a.name, c.firstname);

        Account a1= new Account(name = 'Test Account', X1031_Selling_Agreement__c = 'CCPTIII');
        insert a1;
        
        test.startTest();
            String result = createKitRequest.createKitRequest(c.id);
        test.stopTest();
    }
    static testMethod void createKitRequest_LigtningTest(){
        Account a = new Account(name = 'Test Account', X1031_Selling_Agreement__c = 'CCPTIII - Add On');
        insert a;
        Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', Accountid = a.id);
        insert c;
               System.assertNotEquals( a.name, c.firstname);

        Account a1= new Account(name = 'Test Account', X1031_Selling_Agreement__c = 'CCPTIII');
        insert a1;
        
        test.startTest();
            String result = createKitRequest.createKitRequest_Ligtning(c.id);
        test.stopTest();
    }
}