@isTest
public class Test_PropertystatusController
{
    static testMethod void PropertystatusControllerTest()
    {
        Test.StartTest();
        
        List<Deal__c> deals = new List<Deal__c>();
        Deal__c d = new Deal__c();
        d.name = 'TestDeal'; 
        d.Build_to_Suit_Type__c ='Current';
        d.Estimated_COE_Date__c = date.today();
        d.HVAC_Warranty_Status__c ='Received';
        d.Ownership_Interest__c = 'Fee Simple';
        d.Roof_Warranty_Status__c = 'Received';
        d.Address__c = 'Northeast';
        d.City__C = 'Kansas';
        d.State__c = 'AZ';
        d.Zip_Code__c = '98033';  
        d.Create_Workspace__c =true; 
        insert d;
        
        Task t = new Task();
        t.status = 'New';
        t.subject ='Left Voice Email';
        t.Date_Ordered__c = System.Today();
        t.ActivityDate = System.Today();
        t.Description = 'comments';
        t.Date_Received__c = System.Today();
        t.whatid = d.id;
        insert t;
        
        
        System.assertEquals(t.whatid, d.id);

        ApexPages.currentPage().getParameters().put('id',d.id);
        
        PropertystatusController p = new PropertystatusController();
        p.getTasks();
        
        Test.StopTest();
      }
}