@isTest(SeeAllData = true)
private class ContactInvestorAttachmentCtrl_Test {

  static testMethod void testDoGet() {
    contact conobj = new  contact();
    conobj = [select id from contact limit 1];
    system.assert(conobj != Null );
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    req.requestURI = '/services/apexrest/ContactinvestorsPDF'+conobj.id;  
    req.httpMethod = 'GET';
    RestContext.request = req;
    RestContext.response = res;
    ApexPages.StandardController StandardsObjectController = new ApexPages.StandardController([SELECT Id FROM contact limit 1]);
    system.assert(StandardsObjectController != Null );

    ContactInvestorAttachmentCtrl coninvattctrl  = new ContactInvestorAttachmentCtrl(StandardsObjectController);
    ContactInvestorAttachmentCtrl.createconinvPDFREST();
  }

}