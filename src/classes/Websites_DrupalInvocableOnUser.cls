public with sharing class Websites_DrupalInvocableOnUser implements Database.AllowsCallouts{
    public static  boolean isFromTestClass=false;

    @InvocableMethod(label='invokeUser' description='')
    public static void invokeUser (List<Id> userIds) {
        if(Test.isRunningTest() && (Websites_Drupal_Endpoint_URLs__c.getAll().Values()==null || Websites_Drupal_Endpoint_URLs__c.getAll().Values().isEmpty())){
            Websites_HttpClass.insertDrupalEnpointURLsForTest();
        }
        if(Test.isRunningTest() && !isFromTestClass){
            Test.setMock(HttpCalloutMock.class, new Websites_HttpClass.Mock('{"status": "SUCCESS"}', 200));
        }
                
       system.debug(logginglevel.info, '---------*****----- \nUser created/modified: '+userIds);
       String ColeCapitalUsersEndPointURL = Websites_HttpClass.DrupalEndPointURLs('Cole Capital Users');
        try{
            Websites_HttpClass.makeCall(ColeCapitalUsersEndPointURL, 
                                    Websites_HttpClass.COLECAPITAL_DRUPAL_TOKEN, 
                                    Websites_HttpClass.METHOD_POST,
                                    Websites_HttpClass.TIMEOUT_MAX, 
                                    Websites_HttpClass.ACCEPT, 
                                    JSON.serialize(userIds));
        }catch(CalloutException e){
            system.debug(logginglevel.info, '-----------CalloutException: '+e.getStackTraceString());
            system.debug(logginglevel.info, '-----------CalloutException: '+e.getMessage());
        }
    }

    public static void invokeUserTest (List<Id> userIds) {
        isFromTestClass= true;
        invokeUser(userIds);
    }
}