global class  businessProfileContact1 
{
  // implements Database.Batchable<SObject>,Database.Stateful
   /* global String Query;
    global String BusinessProfileContactlist;
    global Database.QueryLocator start(Database.BatchableContext BC)  
    {
        Query = 'select ID,Contact__c,name,LastModifiedDate,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from BusinessProfileContactTracking__c  ';
        BusinessProfileContactlist='Business Profile id,Business Type,ContactId\n';
        return Database.getQueryLocator(Query);  
    }
    global void execute(Database.BatchableContext BC,List<BusinessProfileContactTracking__c> scope)  
    {   
     list<Contact>AUMlist= new list<Contact>();
     list<id>AUMidlist= new list<id>();
     list<Contact>UpdatedAUMlist= new list<Contact>(); 
    
     List<id>clientsinREidlist=new list<id>();
     List<Contact>clientsinREACClist=new list<Contact>();
     List<Contact>updatedClientsinReAcc=new list<Contact>();  
    
     List<id>AllocatedtoREidlist=new list<id>();
     List<Contact>AllocatedtoREACCist=new list<Contact>();
     List<Contact>updatedallocatetore=new list<Contact>();
        
     List<id>Rexposureidlist=new list<id>();
     List<Contact>RexposureACClist =new list<Contact>();
     List<Contact>updatedReExposure=new list<Contact>();
    
     List<id>BDTopproduceridlist=new list<id>();
     List<Contact>BDTopproducerACCist=new list<Contact>();
     List<Contact>BDupdateTopproducer=new list<Contact>();
   
     list<id>alternateInvestementid=new list<id>();
     List<Contact>alternateInvestementACCist=new list<Contact>();
     List<Contact>UpdatedAlternateInvestementACC=new list<Contact>();   
    
     list<id>Totalsaleslastyearid=new list<id>();
     list<Contact>TotalsaleslastyearAcc= new list<Contact>(); 
     list<Contact>updatedTotalsaleslastyearAcc= new list<Contact>(); 
    
     list<id>FeeBasednoncommisionid=new list<id>();
     list<Contact>FeeBasednoncommisionAcc= new list<Contact>(); 
     list<Contact>updatedFeeBasednoncommisionAcc= new list<Contact>(); 
    
     list<id>businesstageIdlist=new list<id>();
     list<Contact>businesstageAcclist=new list<Contact>();
     list<Contact>updatdBusinessStage=new List<Contact>();
    
     list<id>howcanwehelpIdlist=new list<id>();
     list<Contact>howcanwehelpAcclist=new list <Contact>();
     list<Contact>updatdhowcanwehelp=new List<Contact>();
    
     list<id>howwouldyoudescribeurclientbase=new list<id>();
     list<Contact>howwouldyoudescribeurclientbaseconlist=new list<Contact>();
     list<Contact>Updatehowyoudescribeurclientbase=new list<Contact>();
    
     list<id>IncomeProductsUsedid=new list<id>();
     list<Contact>IncomeProductsUsedACC=new list<Contact>();
     list<Contact>UpdateIncomeProductsUsed=new list<Contact>();
    
     list<id>whatreitid=new list<id>();
     list<Contact>whatreitACC=new list<Contact>();
     list<Contact>UpdatewhatREITs=new list<Contact>();
    
     list<id>wouldyouinvestid=new list<id>();
     list<Contact>wouldyouinvestACC=new list<Contact>();
     list<Contact>UpdatedwouldyouinvestAcc=new list<Contact>();
    
     list<id>whynotid=new list<id>();
     list<Contact>whynotACC=new list<Contact>();
     list<Contact>UpdatedwhynotAcc=new list<Contact>();
    
     list<id>Volatilityproductid=new list<id>();
     list<Contact>VolatilityproducACC=new list<Contact>();
     list<Contact>UpdateVolatility=new list<Contact>();
    
        for(BusinessProfileContactTracking__c objCon : scope) 
        {                        
            if(objCon.Business_Type__c=='Total AUM' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {
                AUMidlist.add(objCon.Contact__c);  
                  
            }
            if(objCon.Business_Type__c=='% of Clients with RE in Portfolio' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {   
                clientsinREidlist.add(objCon.Contact__c);
                
             
            }
            if(objCon.Business_Type__c=='RE Exposure Through' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {   
                Rexposureidlist.add(objCon.Contact__c);
                
             
            }
             if(objCon.Business_Type__c=='Allocated to RE' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {   
                AllocatedtoREidlist.add(objCon.Contact__c);
             }
                                         
            if(objCon.Business_Type__c=='BD Top Producer' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {   
                BDTopproduceridlist.add(objCon.Contact__c);
             } 
            if(objCon.Business_Type__c=='Alternative Investments(Firm Provided)' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {   
                alternateInvestementid.add(objCon.Contact__c);
             } 
           if(objCon.Business_Type__c=='Total Sales Last Year' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {   
                Totalsaleslastyearid.add(objCon.Contact__c);
                   
            } 
           if(objCon.Business_Type__c=='Fee Based (Non Commission)' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {   
                FeeBasednoncommisionid.add(objCon.Contact__c);
            } 
           if(objCon.Business_Type__c=='Business Stage' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {   
                businesstageIdlist.add(objCon.Contact__c);
            } 
             if(objCon.Business_Type__c=='How Can We Help?' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {   
                howcanwehelpIdlist.add(objCon.Contact__c);
            } 
            if(objCon.Business_Type__c=='How would you describe your client base?' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {   
                howwouldyoudescribeurclientbase.add(objCon.Contact__c);
            } 
            if(objCon.Business_Type__c=='Income Products Used' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {   
                IncomeProductsUsedid.add(objCon.Contact__c);
             }  
           if(objCon.Business_Type__c=='What REITs?' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {   
                whatreitid.add(objCon.Contact__c);
                         
            }  
          if(objCon.Business_Type__c=='Would You Invest?' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {   
                wouldyouinvestid.add(objCon.Contact__c);
            }   
          if(objCon.Business_Type__c=='WHy Not?' && objCon.LastModifiedDate== system.today().adddays(-1))
            {   
                whynotid.add(objCon.Contact__c);
             }
        if(objCon.Business_Type__c=='Changes in Volitality' && objCon.LastModifiedDate<= system.today().adddays(-1))
            {   
                Volatilityproductid.add(objCon.Contact__c);
                    
            }
            
        }    
    
        AUMlist=[select id, TotalAUMCheck__c,BusSize_Total_AUM__c  from Contact where id in: AUMidlist];
        for(Contact ct: AUMlist)
        {
            ct.TotalAUMCheck__c=true;
            UpdatedAUMlist.add(ct);
    
        }
       clientsinREACClist=[select id, of_Clients_with_RE_in_Portfoliocheck__c,TOP5_of_Clients_with_RE_in_Portfolio__c  from Contact where id in: clientsinREidlist];
        for(Contact ct: clientsinREACClist)
        {
            ct.of_Clients_with_RE_in_Portfoliocheck__c=true;
            updatedClientsinReAcc.add(ct);
    
        }
        RexposureACClist=[select id,RE_Exposure_Through_check__c,Bus_Size_RE_Exposure_Through__c,Other_RE_Exposure__c  from Contact where id in: Rexposureidlist];
        for(Contact ct: RexposureACClist)
        {
            ct.RE_Exposure_Through_check__c=true;
            updatedReExposure.add(ct);
    
        }
       AllocatedtoREACCist=[select id, Allocated_to_RE_check__c,Bus_Size_Allocated_to_RE__c from Contact where id in: AllocatedtoREidlist];
        for(Contact acc4: AllocatedtoREACCist) 
      {
           acc4.Allocated_to_RE_check__c  =true;
            updatedallocatetore.add(acc4);
      }
      BDTopproducerACCist=[select id,BD_Top_Producer__c,BD_Top_Producer_check__c from Contact where id in: BDTopproduceridlist];
         for(Contact ct1: BDTopproducerACCist)
         {
             ct1.BD_Top_Producer_check__c  =true;
             BDupdateTopproducer.add(ct1);
         }
      alternateInvestementACCist=[select id,Alternative_Investments_Firm_Provided__c ,Alternative_Investments_Check__c from Contact where id in: alternateInvestementid];
         for(Contact ct1: alternateInvestementACCist)
         {
            ct1.Alternative_Investments_Check__c=true; 
            UpdatedAlternateInvestementACC.add(ct1);
           } 
     TotalsaleslastyearAcc=[select id,Total_Sales_Last_Year__c ,Total_Sales_Last_Year_Check__c from Contact where id in: Totalsaleslastyearid];
          for(Contact c1: TotalsaleslastyearAcc)
           {
              c1.Total_Sales_Last_Year_Check__c=true;  
              updatedTotalsaleslastyearAcc.add(c1);   
            }      
       FeeBasednoncommisionAcc=[select id,Fee_Based_Non_Commission_check__c,Fee_Based_Non_Commission__c  from Contact where id in:FeeBasednoncommisionid]; 
              for(Contact conts: FeeBasednoncommisionAcc)
            {
                 conts.Fee_Based_Non_Commission_check__c  = TRUE;
                 updatedFeeBasednoncommisionAcc.add(conts);
               }            
      
        businesstageAcclist =[select id, Business_Stage__c,Other_Business_Stage__c ,How_Can_We_Help__c ,Business_Stage_check__c,How_Can_We_Help_check__c from Contact where id in :businesstageIdlist];
       for(Contact counts : businesstageAcclist )
            {
             counts.Business_Stage_check__c = TRUE;
              updatdBusinessStage.add(counts);
             }
      howcanwehelpAcclist =[select id,Business_Stage__c,Other_Business_Stage__c ,How_Can_We_Help__c ,Business_Stage_check__c,How_Can_We_Help_check__c from Contact where id in : howcanwehelpIdlist];
          for(Contact accounts : howcanwehelpAcclist )
            {
             accounts.How_Can_We_Help_check__c= TRUE;
             updatdhowcanwehelp.add(accounts);
             }
      howwouldyoudescribeurclientbaseconlist=[select id,How_would_you_describe_your_client_base__c,Howwouldusescribeyourclientcheck__c from Contact where id in: howwouldyoudescribeurclientbase];     
           for(Contact contsrec : howwouldyoudescribeurclientbaseconlist)
            {
             contsrec.Howwouldusescribeyourclientcheck__c= TRUE;
              Updatehowyoudescribeurclientbase.add(contsrec);
             }       
       IncomeProductsUsedACC=[select id,Income_Products_UsedCheck__c from Contact where id in: IncomeProductsUsedid];    
       for(Contact act: IncomeProductsUsedACC)
        {
             act.Income_Products_UsedCheck__c= TRUE;
             UpdateIncomeProductsUsed.add(act); 
         } 
        whatreitACC=[select id,What_REITs_check__c,What_REITs__c from contact where id in: whatreitid];   
            for(Contact act: whatreitACC)
            {
                act.What_REITs_check__c = TRUE;
                UpdatewhatREITs.add(act); 
            }  
         wouldyouinvestACC =[select id,Would_you_invest_check__c,Would_you_invest__c from Contact where id in: wouldyouinvestid];
            for(Contact accounts : wouldyouinvestACC )
            {
                accounts.Would_you_invest_check__c = TRUE;
                UpdatedwouldyouinvestAcc.add(accounts);  
             }  
           whynotACC =[select id,Why_Not_check__c,Inv_Why_Not__c from Contact where id in: whynotid];
              for(Contact accounts : whynotACC )
            {
                accounts.Why_Not_check__c= TRUE;
                UpdatedwhynotAcc.add(accounts);  
            } 
            VolatilityproducACC =[select id,What_Changes_in_Volitilaty__c,Changes_in_Volitality_check__c from Contact where id in: Volatilityproductid];
            for(Contact accounts : VolatilityproducACC )
            {
            accounts.Changes_in_Volitality_check__c= TRUE;
            UpdateVolatility.add(accounts);  
             }
   
         
             
        try
        {
           helper.run=false;
           if(UpdatedAUMlist.size()>0)
           update UpdatedAUMlist;
           if(updatedClientsinReAcc.size()>0)
           update updatedClientsinReAcc;
           if(updatedReExposure.size()>0)
           update updatedReExposure;
           if(updatedallocatetore.size()>0)
           update updatedallocatetore;
           if(BDupdateTopproducer.size()>0) 
           update BDupdateTopproducer; 
           if(UpdatedAlternateInvestementACC.size()>0)
           update UpdatedAlternateInvestementACC;
           if(updatedTotalsaleslastyearAcc.size()>0)
           update updatedTotalsaleslastyearAcc;
           if(updatedFeeBasednoncommisionAcc.size()>0)
           update updatedFeeBasednoncommisionAcc;
           if(updatdBusinessStage.size()>0)
           update updatdBusinessStage;
           if(updatdhowcanwehelp.size()>0)
           update updatdhowcanwehelp;
           if(Updatehowyoudescribeurclientbase.size()>0)
            update Updatehowyoudescribeurclientbase;
          if(UpdateIncomeProductsUsed.size()>0)
            update UpdateIncomeProductsUsed;  
          if(UpdatewhatREITs.size()>0)
            update UpdatewhatREITs;  
          if(UpdatedwouldyouinvestAcc.size()>0)
            update UpdatedwouldyouinvestAcc; 
           if(UpdatedwhynotAcc.size()>0)
            update UpdatedwhynotAcc;  
             if(UpdateVolatility.size()>0)
         update UpdateVolatility;    

         }
         
         catch(system.dmlexception e)
         {
              System.debug('Not updated the records ' + e);
         }    
         
    }    
    global void finish(Database.BatchableContext BC)  
    { 
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
       Messaging.EmailFileAttachment csvAttc1 = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(BusinessProfileContactlist);
        string csvname= 'Business Profile Tracking Contactlist.csv';
        csvAttc1.setFileName(csvname);
        csvAttc1.setBody(csvBlob); 
        
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'preddy@colecapital.com'};
        String[] bccAddresses = new String[] {'ntambe@colecapital.com','skalamkar@colecapital.com'};
        String subject ='BusinessProfileTrackingContact list';
        email.setSubject(subject);
        email.setBccSender(true);
        email.setToAddresses( toAddresses );
        email.setBccAddresses(bccAddresses);
        email.setPlainTextBody('Business Profile aging process has been completed');
        //email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc1});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});         
       
    }*/            
        
          
        





}