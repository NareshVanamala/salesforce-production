public with sharing class CheckPendingMembers
{ 
public Map<Id, List<CampaignMember >> accountCampaigns;
public String subject { get; set; }
public String description { get; set; }
public list<Account> lsAccounts {set;get;} 
public list<CampaignMember > lsttask1 {set;get;}
public Event_Automation_Request__c evetobj {set;get;}
Event_Automation_Request__c CalllistEvent= new Event_Automation_Request__c();
public List<SelectOption> CallListValues {set;get;}
public list<Account> inviteeaccount;
public set<id>accountidset;
set<String> accMailSet;
public String CallList {set;get;}
set<id> ConIds = new set<id>();
public Boolean showpanel{get;set;}
public Boolean showpanel1{get;set;}
public Boolean showNext{get;set;}
Set<String> Tempset;
Set<String> Tempset1;
public list<MyWrapper> wcampList{get;set;}
public String sMail{get;set;}
public String fMail{get;set;}
public String bMail{get;set;}
public Boolean showCheckall{get;set;}

Map<Id, String> GlobalExp_map;

public CheckPendingMembers(){
showpanel=true;
showpanel1=false;
showNext=false;
showCheckall=false;
set<String> call = new Set<String>(); 
List<String> lscall = new List<String>();
accountidset=new set<id>();
accMailSet=new set<String>();
Tempset=new Set<String>();
Tempset1=new Set<String>();
try{ 
//get the call list names assigned to the logged in user//
List<Event_Automation_Request__c> tcall = [select name,OwnerId,ID,Event_Type__c,Event_Name__c,Start_Date__c,End_Date__c,Campaign__r.Name from Event_Automation_Request__c where Campaign__c!=null and Start_Date__c>today ];
system.debug('get the lsit...'+tcall.size());
// Add call lists name into picklist call list//
//First added all the vslues in the set to avoidd duplicates and then into the list and then into the picklist//

for(Event_Automation_Request__c t : tcall){
if(t.Event_Name__c!=null)
call.add(t.Event_Name__c);
}
system.debug('Check the call..'+call);
call.add('--None--');
CallListValues = new List<SelectOption>();
lscall.AddAll(call);
lscall.sort();
for(String s : lscall){ 
CallListValues.add(new SelectOption(s,s));
}
if(CallListValues.size()==0){ 
CallListValues.add(new SelectOption('--None--','--None--'));
}
}catch(Exception e){} 
}

public void AdviserCallList() { 

Tempset = new Set<String>();
Tempset1 = new Set<String>();

accountCampaigns = new Map<Id,List<CampaignMember >>();
evetobj= new Event_Automation_Request__c ();
GlobalExp_map = new Map<Id, String>();
wcampList = new List<MyWrapper>();
// get the Contacts event attendee from the selcted call list//
date d= system.today();
if(calllist!='--None--')
{
CalllistEvent=[select Event_Name__c,id, name,Campaign__r.id ,Start_Date__c,End_Date__c ,Event_Description__c,Event_Location__c from Event_Automation_Request__c where Event_Name__c=:calllist limit 1];
system.debug('CalllistEvent is : ' + CalllistEvent);
system.debug('Filters are : CalllistEvent.Campaign__r.id = ' + CalllistEvent.Campaign__r.Id);
//lsttask1=[select id, contactid,CampaignId ,Contact.FirstName,Contact.name,contact.accountid,Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c from CampaignMember where CampaignId =:CalllistEvent.Campaign__r.id and status='Pending' and contact.Account.Event_Approver__c!=null ];

/*
subject='Invitation for an Event: '+CalllistEvent.Event_Name__c;
description ='Hi,</br>You have been Invited for '+CalllistEvent.Event_Name__c+ ' Event';

DateTime sd=CalllistEvent.Start_Date__c;
String startDate=sd.format('MMMMM dd, yyyy');
DateTime ed=CalllistEvent.End_Date__c;
String endDate=ed.format('MMMMM dd, yyyy');
description=description+' </b></br></br>'+
' Start Date:<b> '+startDate+' </b></br>'+
' End Date:<b> '+endDate+' </b></br></br>'+
'Please login to Check the Invitees from your Account:<b> '+
'<a href="https://ocms.stage.cs4.force.com/eReview">Click here</a>' +' </b></br></br>'+
'Thanks</br>';
*/

//<a href="http://colereit.force.com/ereview">

sObject et = [select id, subject, body from EmailTemplate where Name=: 'Pending Approval Campaign Members'];
//fMail = (String) e.get('from_address__c');
//bMail = (String) e.get('BccAddresses__c');
  subject = (string) et.get('subject');
  description = (string) et.get('body');

DateTime sd=CalllistEvent.Start_Date__c;
String startDate=sd.format('MMMMM dd, yyyy');
DateTime ed=CalllistEvent.End_Date__c;
String endDate=ed.format('MMMMM dd, yyyy');
String evlocation=CalllistEvent.Event_Location__c;
//subject='Request For Approval: Cole Capital Success Forum 2016';
//for Production
/*description='Good Morning,<br></br>'+ 
'We are pleased to invite  a select group of representatives from your firm to attend our upcoming event. Using our automated, online system you may review a complete meeting agenda, the associated FINRA letter and submit your approval of each representative’s attendance. Please follow these simple instructions:</br></br>'+ 
'1.<span style="padding-left:10px;">Click the following link and enter your firm’s Compliance request email address to login.</span>'+ 
'<a href="http://colereit.force.com/ereview" target="_top"> Click here to review </a></br>'+ 
'2.<span style="padding-left:10px;">Select <u> Cole Capital Success Forum 2016 </u> from the dropdown list of events.</span></br>'+ 
'3.<span style="padding-left:10px;">Click the agenda and FINRA letter icons in the upper left corner to review each document.</span></br>'+ 
'4.<span style="padding-left:10px;">To approve or decline attendance of all listed representatives, click the respective boxes above the list</span><span style="padding-left:20px;"> of representative names.</span></br>'+ 
'5.<span style="padding-left:10px;">To individually approve each representative, please click the “Approved” OR “Declined” box beside </span><span style="padding-left:20px;">each name. If needed, you may add comments beside the name of a particular representative.</span></br>'+
'6.<span style="padding-left:10px;">Once you indicate your selections, click “Continue”.</span></br>'+ 
'7.<span style="padding-left:10px;">To complete the process, check the box indicating your review of the information and then click the </span><span style="padding-left:20px;"> “Submit Approval” button at the bottom of the screen.</span></br></br>'+ 
'For assistance or if you have trouble viewing the information, please contact Erika Henry  at 602-778-6193 or <a href="mailto:ehenry@colecapital.com" target="_top"> ehenry@colecapital.com</a><br/><br/>'+ 
'Thank you for your time and attention. We appreciate your partnership.';*/

/*description='Greetings,<br></br>'+
'Cole would like to seek approval from the attached list of representatives from your firm to possibly receive an invitation for one of the '+CalllistEvent.Event_Name__c+' scheduled below.</br>'+
'<ul><li><b>February 6-7, 2014<span style="padding-left:30px;">Phoenix, AZ</span></b></li></ul>'+
'<ul><li><b>May 15-16, 2014<span style="padding-left:30px;">New York, NY</span></b></li></ul>'+
'<ul><li><b>September 18-19, 2014<span style="padding-left:30px;">New York, NY</span></b></li></ul>'+
'<ul><li><b>October 16-17, 2014<span style="padding-left:30px;">Phoenix, AZ</span></b></li></ul>'+
'<ul><li><b>November 13-14, 2014<span style="padding-left:30px;">Phoenix, AZ</span></b></li></ul>'+
'Please review the attached information letter, list of representatives and the meeting agenda.<br/><br/>'+
'Please respond by choosing either "Approve" or "Decline" from the drop down menu. Select "Continue" to receive a confirmation of your selection.<br></br>'+
'<a href="http://colereit.force.com/ereview" target="_top">Click here to review</a> Your log in is your email address. For assistance or if you have trouble viewing the information please contact Natalie Carroll at 602-778-6486 or <a href="mailto:Natalie.carroll@colereit.com" target="_top">Natalie.carroll@colereit.com</a>.<br/><br/><br/>'+ 
'<b>Please Note:</b><br/>'+
'Due to limited space, not all of the approved advisors on the list will receive an invitation. As we do not want to disappoint any of the approved advisors, please do not notify them that they will be receiving an invitation. Once your advisors have received an invitation and have attended the meeting, we will send you a list for your files.<br/><br/>'+
'Thank you for your assistance on this, we appreciate your partnership.';
*/ 



//[select id, contactid,CampaignId ,Contact.FirstName,Contact.name,contact.accountid,Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c from CampaignMember where CampaignId =:CalllistEvent.Campaign__r.id and status='Pending' and contact.Account.Event_Approver__c!=null];

for(CampaignMember ct:[select id, contactid,CampaignId ,Contact.FirstName,Contact.name,contact.accountid,Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c from CampaignMember where CampaignId =:CalllistEvent.Campaign__r.id and status='Pending' and status!='Not Approved' and (contact.Account.Event_Approver__c!=null or contact.Account.Event_Approver1__c!=null or contact.Account.Event_Approver2__c!=null)])
{
Tempset.add(ct.contact.accountid); 
}

if(Tempset.size()>0)
{
//lsAccounts=[SELECT id,Account.name,Event_Approver__r.email,Event_Approver__r.name,(SELECT id,name,MailingCity,MailingState FROM contacts where id in:Tempset1) FROM Account where id in:Tempset]; 
lsAccounts=[SELECT id,Event_Approver__r.EmailBouncedDate,Account.name,Event_Approver__r.email,Event_Approver1__r.email,Event_Approver2__r.email,Event_Approver__r.name,Event_Approver1__r.name,Event_Approver2__r.name FROM Account where id in:Tempset order by Account.name ]; 

MyWrapper w;
for(Account acc:lsAccounts){
w = new MyWrapper();
w.wAccountName = acc.name ;
w.wAccountName1 = acc.name ;
w.wAccountName2 = acc.name ;
w.wChecked = false;
w.wId = acc.id;
w.wMail = acc.Event_Approver__r.email;
w.wMail1 = acc.Event_Approver1__r.email;
w.wMail2 = acc.Event_Approver2__r.email;
w.wEApprover = acc.Event_Approver__r.name;
w.wEApprover1 = acc.Event_Approver1__r.name;
w.wEApprover2 = acc.Event_Approver2__r.name;


//w.wConList=accountCampaigns.get(acc.id);
wcampList.add(w);

/*List<CampaignMember> CM=new List<CampaignMember>();
CM = accountCampaigns.get(acc.id);
List<CampaignMember> CM2=new List<CampaignMember>();
if(CM.Size()>1000){
Integer IndtVar=1;
List<CampaignMember> CM_Dup = new List<CampaignMember>();
String step;
for(integer i=0; i<=CM.size()-1;i++){
if(IndtVar<1000){
CM_Dup.add(CM[i]);
IndtVar++;
step='hasnext';
}
else{
IndtVar=1;
w.wConList=CM_Dup;
wcampList.add(w);
CM_Dup.clear();
step='no next';
} 
}
if(step=='hasnext'){
w.wConList=CM_Dup;
wcampList.add(w);
}
}
else{
w.wConList=accountCampaigns.get(acc.id);
wcampList.add(w);
}*/
}
} 
evetobj.Start_Date__c=CalllistEvent.Start_Date__c;
evetobj.End_Date__c=CalllistEvent.End_Date__c;
evetobj.Event_Description__c=CalllistEvent.Event_Description__c; 
Tempset.clear();
Tempset1.clear();
//lsttask1.clear();
//accountCampaigns.clear();
showNext=true;
}
else
{
showNext=false;
}
}

public void selectAllAccounts(){
System.debug('Welcome to the Account selction method..');
System.debug('Please check the wrapper list..'+wcampList);
if(wcampList.size()>0)
{
for(MyWrapper obj: wcampList)
{
if(showCheckall==true)
obj.wChecked=true;
if(showCheckall==false)
obj.wChecked=false;
} 
}
//showCheckall=false;
} 

public void doAction() {
if(wcampList.size()>0)
{
for(MyWrapper obj: wcampList)
{
if(obj.wChecked==true){
accountidset.add(obj.wId);
accMailSet.add(obj.wMail);
accMailSet.add(obj.wMail1);
accMailSet.add(obj.wMail2);

}
}
}
sMail = '';
for(String s:accMailSet)
{
if(s!=null)
{
if(sMail=='')
sMail=s;
else
sMail=sMail+','+s;
}
} 
if(accountidset.size()>0)
{ 
showpanel=false;
showpanel1=true; 

/* for stage */
//fMail='ninad.tambe@colereit.com'; 
//bMail='ninad.tambe@colereit.com';
/* for prodution 
fMail='ehenry@colecapital.com'; 
bMail='ehenry@colecapital.com'; */
//sObject e = [select id, from_address__c, to_address__c,BccAddresses__c from email_addresses__c limit 1];
 Email_addresses__c ec=Email_addresses__c.getvalues('BD Approval');
 string toaddress=ec.To_Address__c;
 string toaddress1=ec.To_Address_1__c;

fMail =  ec.from_address__c;
bMail =  ec.BccAddresses__c;


}
else
{ 
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select account'));
}
}

public PageReference doAction2(){
showpanel=true;
showpanel1=false;
showNext=false;
PageReference reference=new PageReference('/apex/CheckMyMembers');
reference.setRedirect(true);
return reference; 
}

public PageReference sendEmail()
{
list<Messaging.SingleEmailMessage> mails= new list<Messaging.SingleEmailMessage>();
list<BD_Approval_Email_History__c> emailhistorylist= new list<BD_Approval_Email_History__c>();
List<Messaging.SendEmailResult> results= new list<messaging.SendEmailResult>();

try{
inviteeaccount=[select Event_Approver__r.EmailBouncedDate , id,name,Event_Approver__r.email,Event_Approver__r.name,Event_Approver__r.account.name from Account where id in:accountidset]; 

system.debug('Dekho rutha na karo...'+inviteeaccount.size());
System.debug('Check the Accounts..'+inviteeaccount);

//Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
// List<String> toAddresses = new List<String>();
//List<String> bccAddresses = new List<String>();
if(inviteeaccount.size()>0)
{
for(Account obj:inviteeaccount)
{ 
if((obj.Event_Approver__r.Email != null)&&(obj.Event_Approver__r.emailbounceddate==null ))
{
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
mail.settargetobjectid(obj.Event_Approver__c);
mail.setSaveAsActivity(false);
//*************************added by Snehal to track the email sent history Started**********************// 

BD_Approval_Email_History__c emailhistory= new BD_Approval_Email_History__c();
emailhistory.Event_Approver__c=obj.Event_Approver__c;
emailhistory.Approver_Email__c= obj.Event_Approver__r.Email;
emailhistory.Account_Name__c=obj.Event_Approver__r.account.name;
emailhistory.Approval_Request_Sent_On__c= system.now();
emailhistory.Event_Name__c=calllist; 
emailhistorylist.add(emailhistory);

//*************************added by Snehal to track the email sent history finished**********************// 
//bccAddresses.add(obj.Event_Approver__r.email);
// mail.setToAddresses(toAddresses); 
// mail.setToAddresses(toAddresses);
/* for stage */
// mail.setOrgWideEmailAddressId('0D2P0000000Cb0g'); 
/* for prodution */
mail.setOrgWideEmailAddressId('0D2500000004CuY');
mail.setReplyTo(fMail);

String[] bccAddresses = new String[]{bMail};
if(bMail!='')
mail.setBccAddresses(bccAddresses);
//mail.setToAddresses(toAddresses);
//mail.setBccAddresses(bccAddresses); 
mail.setSubject(subject); 
mail.setHtmlBody(description);
mails.add(mail);
//Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
} 
} 

Messaging.sendEmail(mails); 

//if (!results.get(0).isSuccess()) {
//System.StatusCode statusCode = results.get(0).getErrors()[0].getStatusCode();
//String errorMessage = results.get(0).getErrors()[0].getMessage();


if (Schema.sObjectType.BD_Approval_Email_History__c.isCreateable()) 
insert emailhistorylist;

PageReference reference=new PageReference('/apex/CheckMyMembers');
reference.setRedirect(true);
return reference;
}
}catch(Exception e){
system.debug('>>>>>>>>>>>>>>>>exception>>>>>>>>>>>>>>>>>>email chain is breaked');
system.debug('>>>>>>>>>>>>>>>>e>>>>>>>>>>>>>>>>>> '+e);

system.debug('Check the results...'+results );
ApexPages.addMessages(e); 
}
return null; 
}

public class MyWrapper { 
public String wName{get;set;}
public String wAccountName{get;set;}
public String wAccountName1{get;set;}
public String wAccountName2{get;set;}
public String wCity{get;set;}
public String wState{get;set;}
public Boolean wChecked{get;set;}
public Id wId{get;set;}
public String wMail{get;set;}
public String wMail1{get;set;}
public String wMail2{get;set;}
public String wEApprover{get;set;}
public String wEApprover1{get;set;}
public String wEApprover2{get;set;}
public List<CampaignMember> wConList{get;set;} 
public MyWrapper(){
wConList = new List<CampaignMember>();
}
}
}