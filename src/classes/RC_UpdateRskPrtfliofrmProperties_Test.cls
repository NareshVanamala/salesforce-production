@isTest
Public class RC_UpdateRskPrtfliofrmProperties_Test
{
  static testmethod void testUpdateRskPrtfliofrmProperties()
  {
 
        RC_Risk_Portfolio__c testportfolio= new RC_Risk_Portfolio__c();    
        testportfolio.name='testPortfolio';
         insert testportfolio;
        
        list<RC_Property__c>rclist=new list<RC_Property__c>();
        
        RC_Property__c RCPT=new RC_Property__c();
        RCPT.name='Amazon DC 2014 USAA~A11123';
        RCPT.Risk_Portfolio__c=testportfolio.id;
        RCPT.Deal_Type__c='Existing';
        RCPT.Status__c='Signed LOI';
        RCPT.Contract_Price__c=8944130.00;
        RCPT.Year_1_NOI__c=8944130.00;
        RCPT.Cap_Rate__c=8944130.00;
        RCPT.Total_Square_Footage__c=8944130.00;
        RCPT.Price_PSF_Total_Price__c=8944130.00;
        rclist.add(RCPT);
        
        
        RC_Property__c RCPT1=new RC_Property__c();
        RCPT1.name='Amazon DC 2014 USAA~A11123';
        RCPT1.Risk_Portfolio__c=testportfolio.id;
        RCPT1.Deal_Type__c='Existing';
        RCPT1.Status__c='Signed LOI';
        RCPT1.Contract_Price__c=8944130.00;
        RCPT1.Year_1_NOI__c=8944130.00;
        RCPT1.Cap_Rate__c=8944130.00;
        RCPT1.Total_Square_Footage__c=8944130.00;
        RCPT1.Price_PSF_Total_Price__c=8944130.00;
         rclist.add(RCPT1);
         
         insert rclist;
        
         RC_UpdateRskPrtfliofrmProperties batch1 = new RC_UpdateRskPrtfliofrmProperties();
         Id batchId1 = Database.executeBatch(batch1);
         


  }
    
}