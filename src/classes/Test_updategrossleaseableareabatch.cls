@isTest
public class Test_updategrossleaseableareabatch{
    static testMethod void testone() {
    try{
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        
        list<web_properties__c> wblist=new list<web_properties__c>();
    for(integer i=0; i<5;i++)
    {    
    web_properties__c wb=new web_properties__c();
    wb.name='test';
    wb.Gross_Leaseable_Area__c=1000;
    wb.property_id__c ='test';
    wb.web_ready__c='approved';
    wb.Address__c='2620 W. Chandler Blvd';
    wb.city__c='Chandler';
    wb.State__c='AZ';
    wb.country__c='USA';
    wb.Latitude_del__c=36.5864546;
    wb.Longitude_del__c=-108.6078704;
    wb.Location__Latitude__s=36.5864546;
    wb.Location__Longitude__s=-108.6078704;
    
   system.assert(wb!=Null);

    wblist.add(wb);
    }
    insert wblist;
    update wblist;
    
        updatepropertygrossleaseableareabatch AU = new updatepropertygrossleaseableareabatch(wblist);
        QL = AU.start(bc);
        
        Database.QueryLocatorIterator QIT =  QL.iterator();
        while (QIT.hasNext())
        {
            web_properties__c Acc = (web_properties__c)QIT.next();            
            System.debug(Acc);
            wblist.add(Acc);
        }        
        
        AU.execute(BC, wblist);
        AU.finish(BC);       
        }
        catch(Exception e)
        {
        } 
    }
}