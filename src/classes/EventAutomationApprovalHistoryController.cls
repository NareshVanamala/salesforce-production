public with sharing class EventAutomationApprovalHistoryController {
public EventAutomationApprovalHistoryController (ApexPages.StandardController controller)

{
Event_Automation_Request__c e =[select id from Event_Automation_Request__c  where id =: ApexPages.currentPage().getParameters().get('id')];
EAId1=e.id;

}

public EventAutomationApprovalHistoryController(){}
    public String EAId1{get;set;}
    public List<ProcessInstanceHistory> getApprovalSteps() {
      if (EAId1!= null) {
        Event_Automation_Request__c quote = [Select Id, (Select TargetObjectId, SystemModstamp, StepStatus, RemindersSent, ProcessInstanceId, OriginalActorId, IsPending, IsDeleted, Id, CreatedDate, CreatedById, Comments, ActorId From ProcessSteps order by SystemModstamp desc) from Event_Automation_Request__c where Id = :EAId1];
        return quote.ProcessSteps;
      }
      return new List<ProcessInstanceHistory> ();
    }  
    
}