@isTest
public with sharing class ocms_ARMyProfile_test {
    
    static cms__Content__c txc {get; set;}

    public ocms_ARMyProfile_test() {
        
    }

    /**
    * @description setup the page
    **/
    static private cms.GenerateContent setUpController() {
        Map <String, String> contextProperties = new Map<String, String>{'page_mode' => 'prev'};    
        cms__Sites__c mySite = cms.TestExtensionFixtures.InitiateTest('Public', contextProperties);
        
        cms__Content_Type__c ct = new cms__Content_Type__c(cms__Name__c = 'ARSelfRegistration', cms__Site_Name__c = 'Public', cms__Label__c = 'ARSelfRegistration');
        insert ct;

        System.assertEquals(ct.cms__Name__c ,'ARSelfRegistration');

             
        txc = new cms__Content__c(
                cms__Content_Type__c         = ct.Id,
                cms__Name__c                 = 'ARSelfRegistrationCon',
                cms__Description__c          = 'Testing',
                cms__Preview__c              = true,
                cms__Published__c            = false,
                cms__Published_Start_Date__c = System.now(),
                cms__Site_Name__c            = 'Public',
                cms__Revision_Number__c      = 0,
                cms__Revision_Origin__c      = null,
                cms__Version_Number__c       = 1,
                cms__Version_Origin__c       = null,
                cms__Version_Original__c     = true,
                cms__Version_Parent__c       = null,
                cms__Depth__c                = 0
        );
        insert txc;
        
        cms__Content_Layout__c cl = new cms__Content_Layout__c(cms__Name__c = 'ARSelfRegistrationCL');
        insert cl;
        
        cms__Page__c page = new cms__Page__c(cms__Name__c = 'ARSelfRegistrationPage', cms__Site_Name__c = 'Public');
        insert page;
        
        cms__Content_Layout_Instance__c cli = new cms__Content_Layout_Instance__c(cms__Content__c = txc.Id, cms__Content_Layout__c = cl.Id);
        insert cli;

        cms__Page_Content_Layout_Instance__c pcli = new cms__Page_Content_Layout_Instance__c(cms__Content_Layout_Instance__c=cli.Id,cms__Page__c=page.Id);
        insert pcli;

        cms.API anAPI = new cms.API(null, 'prev');
        anAPI.site_name = 'Public';
        
        System.currentPageReference().getParameters().put('ecms', anAPI.getSerialize());
        System.currentPageReference().getParameters().put('content_id', txc.Id);
        System.currentPageReference().getParameters().put('cli_id', cli.Id);
        System.currentPageReference().getParameters().put('pcli_id', pcli.Id);

        cms.GenerateContent gc = new cms.GenerateContent();

        gc.content = txc;
        gc.cli = cli;
        gc.pcli = pcli;

        return gc;
    }

    /**
    * @description Main test function
    *
    **/
    static testMethod void testARMyProfile() {

        //Setip page controller
        cms.GenerateContent gc = setUpController();

        //Create new instance of the SELF REG page
        ocms_ARMyProfile myProfilePage = new ocms_ARMyProfile();

        System.Type type = myProfilePage.getType();

        //Test the getHTML function
        String testGetUrl = myProfilePage.getHTML();

        Map<String, String> serviceParams = new Map<String, String>();
        serviceParams.put('action', 'saveProfile');
        serviceParams.put('street', 'Test St');
        serviceParams.put('city', 'Testville');
        serviceParams.put('state', 'West Testus');
        serviceParams.put('postalCode', '12345');
        serviceParams.put('country', 'Testation');
        serviceParams.put('email', 'testguy1@testyourcode.com');
        serviceParams.put('phone', '1234567890');
        
        System.assert(serviceParams != Null);

        String resp1 = myProfilePage.executeRequest(serviceParams);

        serviceParams = new Map<String, String>();
        serviceParams.put('action', 'saveProfile');
        serviceParams.put('street', '');
        serviceParams.put('city', 'Testville');
        serviceParams.put('state', 'West Testus');
        serviceParams.put('postalCode', '12345');
        serviceParams.put('country', 'Testation');
        serviceParams.put('email', 'testguy1@testyourcode.com');
        serviceParams.put('phone', '1234567890');

        String resp2 = myProfilePage.executeRequest(serviceParams);

        serviceParams = new Map<String, String>();
        serviceParams.put('action', 'saveProfile');
        serviceParams.put('street', 'Test St');
        serviceParams.put('city', '');
        serviceParams.put('state', 'West Testus');
        serviceParams.put('postalCode', '12345');
        serviceParams.put('country', 'Testation');
        serviceParams.put('email', 'testguy1@testyourcode.com');
        serviceParams.put('phone', '1234567890');

        String resp3 = myProfilePage.executeRequest(serviceParams);

        serviceParams = new Map<String, String>();
        serviceParams.put('action', 'saveProfile');
        serviceParams.put('street', 'Test St');
        serviceParams.put('city', 'Testville');
        serviceParams.put('state', '');
        serviceParams.put('postalCode', '12345');
        serviceParams.put('country', 'Testation');
        serviceParams.put('email', 'testguy1@testyourcode.com');
        serviceParams.put('phone', '1234567890');

        String resp4 = myProfilePage.executeRequest(serviceParams);

        serviceParams = new Map<String, String>();
        serviceParams.put('action', 'saveProfile');
        serviceParams.put('street', 'Test St');
        serviceParams.put('city', 'Testville');
        serviceParams.put('state', 'West Testus');
        serviceParams.put('postalCode', '');
        serviceParams.put('country', 'Testation');
        serviceParams.put('email', 'testguy1@testyourcode.com');
        serviceParams.put('phone', '1234567890');

        String resp5 = myProfilePage.executeRequest(serviceParams);

        serviceParams = new Map<String, String>();
        serviceParams.put('action', 'saveProfile');
        serviceParams.put('street', 'Test St');
        serviceParams.put('city', 'Testville');
        serviceParams.put('state', 'West Testus');
        serviceParams.put('postalCode', '12345');
        serviceParams.put('country', '');
        serviceParams.put('email', 'testguy1@testyourcode.com');
        serviceParams.put('phone', '1234567890');

        String resp6 = myProfilePage.executeRequest(serviceParams);

        serviceParams = new Map<String, String>();
        serviceParams.put('action', 'saveProfile');
        serviceParams.put('street', 'Test St');
        serviceParams.put('city', 'Testville');
        serviceParams.put('state', 'West Testus');
        serviceParams.put('postalCode', '12345');
        serviceParams.put('country', 'Testation');
        serviceParams.put('email', 'testguy1urcodecom');
        serviceParams.put('phone', '1234567890');

        String resp7 = myProfilePage.executeRequest(serviceParams);

        serviceParams = new Map<String, String>();
        serviceParams.put('action', 'saveProfile');
        serviceParams.put('street', 'Test St');
        serviceParams.put('city', 'Testville');
        serviceParams.put('state', 'West Testus');
        serviceParams.put('postalCode', '12345');
        serviceParams.put('country', 'Testation');
        serviceParams.put('email', 'testguy1@testyourcode.com');
        serviceParams.put('phone', '123');

        String resp8 = myProfilePage.executeRequest(serviceParams);

        System.debug(resp1);
        System.debug(resp2);
        System.debug(resp3);
        System.debug(resp4);
        System.debug(resp5);
        System.debug(resp6);
        System.debug(resp7);
        System.debug(resp8);
    }

}