public with sharing class Cockpit_GetAlltheCalllists{ 
    
    public List< Contact_Call_List__c >getAllCalllist{set;get;}
    public Cockpit_GetAlltheCalllists(){
        Contact c =[select id from Contact where id = :ApexPages.currentPage().getParameters().get('id')];    
        getAllCalllist= [select id,name,Owner__c,Call_Complete__c,CallList_Name__c, Contact__c,Owner__r.name,Contact__r.name,Description__c,createdDate from Contact_Call_List__c  where(Contact__c  = :c.Id and Call_Complete__c=false and IsDeleted__c=false and Automated_Call_List__r.Archived__c=false)];
        system.debug('Waht is my query'+ getAllCalllist);
    }

    
}