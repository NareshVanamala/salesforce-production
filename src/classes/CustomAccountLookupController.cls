public with sharing class CustomAccountLookupController {
 
  public Account account {get;set;} // new account to create
  public String phoneNumberPassToTextBox {get;set;}
  public String phoneNamePassToTextBox {get;set;}
  public list<string> selectedrightvalues;
  public List<user> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
  set<string> originalvalues = new set<string>();
  set<string> originalvalues1 = new set<string>();
  set<string> originalvalues2 = new set<string>();
  set<string> originalvalues3 = new set<string>();
  Public List<string> leftselected{get;set;}
  Public List<string> rightselected{get;set;}
  Set<string> leftvalues = new Set<string>();
  Set<string> rightvalues = new Set<string>();
  public string picklist{get;set;}
  public List<user> results1{get;set;} // search results
  public string searchString1{get;set;} // search keyword
    
  public CustomAccountLookupController() {
    leftselected = new List<String>();
    rightselected = new List<String>();
    phoneNumberPassToTextBox ='';
    phoneNamePassToTextBox = '';
    account = new Account();
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
    runSearch();
    searchString1 = System.currentPageReference().getParameters().get('lksrch');
    //runSearch1();
    leftvalues.clear();
    
       
  }
  
   public pagereference selectedvalue(){
    if(picklist=='users')
    {
     try{
      leftvalues.clear();
      list<user> users = [select name from user limit 100];
      for(user u:users)
        {
        originalvalues.add(u.name);
        }
        leftvalues.addAll(originalValues);
        if(users.size()>=100){
        apexPages.addMessage(new ApexPages.message(ApexPages.severity.info,
                                'Your search returned more than 100 rows. Only the first 100 are displayed. Please refine search criteria'));
        }
        }
     catch(Exception ex){
            apexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'No Users Found!'));
            }
        
   }
  
    
    if(picklist=='Roles')
    {
      try{
      leftvalues.clear();
      if(searchString1!=''&&searchString1!=null)
          {
          searchString1='';
          }     
      list<UserRole> UserRoles = [select name from UserRole limit 100];
      for(UserRole u:UserRoles)
        {
        originalvalues1.add(u.name);
       
        }
          leftvalues.addAll(originalValues1);
       }
     catch(Exception ex){
            apexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'No Roles Found!'));
            }
     }
    if(picklist=='Public Groups')
    {
     try{
      leftvalues.clear();
      list<Group> Groups = [select name from Group limit 100];
      if(groups.size()!=null){
      for(Group u:Groups)
        {
        originalvalues2.add(u.name);
        }
         leftvalues.addAll(originalValues2);
       }
       }
       catch(Exception ex){
            apexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'No Groups Found!'));
            }
      } 
    if(picklist=='None')
    {
      leftvalues.clear();
    }
    return null;
    }    
     
    public PageReference selectclick(){
        rightselected.clear();
        for(String s : leftselected){
            leftvalues.remove(s);
            rightvalues.add(s);
        }
        return null;
    }
     
    public PageReference unselectclick(){
        leftselected.clear();
        for(String s : rightselected){
            rightvalues.remove(s);
            leftvalues.add(s);
        }
        return null;
    }
 
   public List<SelectOption> getSelectedValues(){
        List<SelectOption> options1 = new List<SelectOption>();
        List<string> tempList = new List<String>();
        selectedrightvalues = new list<string>(rightvalues);    
        tempList.addAll(rightvalues);
        tempList.sort();
        for(String s : tempList)
            options1.add(new SelectOption(s,s));
        return options1;
    }
    public pagereference insertrecord(){
     for(integer i=0;i<selectedrightvalues.size();i++)
     {
     if(phoneNumberPassToTextBox=='')
     phoneNumberPassToTextBox=selectedrightvalues[i];
     else
     phoneNumberPassToTextBox=phoneNumberPassToTextBox+','+selectedrightvalues[i];
     }
   
     return null;
     }
    
          
    public list<selectoption> getuserpicklist(){
        List<selectOption> options2 = new list<selectOption>();
        set<string> ab = new set<string>{'Users','Roles'};
        options2.add(new Selectoption('None','None'));
        for(string s1:ab)
        options2.add(new Selectoption(s1,s1));
        return options2;    
        }
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
 
  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    results = performSearch(searchString);               
  } 
 
  // run the search and return the records found. 
  public List<user> performSearch(string searchString) {
 
    String soql = 'select id, name from user';
    if(searchString != '' && searchString != null)
      soql = soql +  ' where name LIKE \'%' + String.escapeSingleQuotes(searchString)+'%\'';
    soql = soql + ' limit 25';
    System.debug(soql);
    return database.query(soql); 
 
  }
  public PageReference search1() {
    runSearch1();
    return null;
  }
 
  // prepare the query and issue the search command
  public void runSearch1() {
    // TODO prepare query string for complex serarches & prevent injections
        if(searchstring1!=''&&searchstring1!=null)
        {
          results1 = performSearch1(searchString1);
        }
        leftvalues.clear();
        originalvalues3.clear();
        for(user u30:results1)
        {
            originalvalues3.add(u30.name);
        }  
        leftvalues.addAll(originalvalues3);             
  } 
 
  // run the search and return the records found. 
  public List<user> performSearch1(string searchString1) {
    if(searchString1!=''&&searchString1!=null)
    {
    leftvalues.clear();
    String soql = 'select id, name from user';
    if(searchString1 != '' && searchString1 != null)
      soql = soql +  ' where name LIKE \'%' + String.escapeSingleQuotes(searchString1) +'%\'';
    soql = soql + ' limit 10';
    System.debug(soql);
    return database.query(soql); 
    }
    return null;
    
  }     
  public List<SelectOption> getunSelectedValues(){
        List<SelectOption> options = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(leftvalues);
        tempList.sort();
        for(string s : tempList)
          options.add(new SelectOption(s,s));    
        return options;   
    } 
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
 
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
 
}