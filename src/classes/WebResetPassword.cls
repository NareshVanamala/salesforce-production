public with sharing class WebResetPassword
{
    private Contact con;
     public boolean disabled{get;set;}    
     public boolean rendered{get;set;}    
     public string message{get;set;}
           
    public WebResetPassword(ApexPages.StandardController controller)
    {
       this.con=(Contact)controller.getRecord();
       this.con=[select Id,Website_Registered_User__c,Email from Contact Where Id=:con.Id];
      
        if(!this.con.Website_Registered_User__c)        
        {            
            disabled=true;            
              
        }        
             
    }
    
    public void SendResetPassword()
    {
              // HTTP Post Method
                Http h = new Http();
                HttpRequest req = new HttpRequest();
                req.setEndpoint('https://www.colecapital.com/financial-professionals?reset=' + this.con.Id);
                req.setMethod('GET');
                req.setTimeout(60000);
                HttpResponse res = h.send(req);
                System.debug(res.getStatus());
                System.debug(res.getBody());
                message='Email has been sent to Reset Password.';

    }

      
       
 }