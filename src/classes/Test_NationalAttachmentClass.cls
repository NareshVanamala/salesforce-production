@isTest
private class Test_NationalAttachmentClass
{
   public static testMethod void TestforNationalAttachmentClass() 
   {        
        Account acc = new Account();
        acc.Name = 'Test Prospect Account';
        Insert acc;
        
        //Set up user
        User us = [SELECT Id FROM User limit 1];          
        
        National_Attachment__c na = new National_Attachment__c();
        na.Account__c=acc.id;
        na.Body__c='test body';
        na.Description__c='test description';
        na.Title__c='test title';
        na.Type__c='test type';
        insert na;        
        
        Attachment attachment= new Attachment(); 
        attachment.OwnerId = UserInfo.getUserId();  
        attachment.ParentId = na.Id;// the record the file is attached to   
        String nameFile = 'hello';                
        attachment.Name = nameFile;
        Blob contentFile = Blob.valueOf('Unit Test Attachment Body');
        attachment.body=contentFile;
        insert attachment;
        
        system.assertequals(attachment.ParentId , na.Id);

        
        ApexPages.currentPage().getParameters().put('id',na.id);
        ApexPages.currentPage().getParameters().put('nameFile','test nameFile');
        
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        NationalAttachmentClass nac = new NationalAttachmentClass(controller);
        
        nac.nameFile='Unit Test Attachment';
        nac.contentFile=Blob.valueOf('Unit Test Attachment Body');
        
        nac.removecon();
        nac.editRecord();
        nac.SaveRecord();
        nac.newProgram();
        nac.newProgram1();
        nac.savenewProg();
        nac.savenewProg1();
        nac.cancelnewProg();        
        
   }
}