@isTest(seeAlldata=true)
private class Test_NewhireNdcontroller{
    static testmethod void NewhireNdcontrollertest(){
     user u =[select id,name from user where id=:userinfo.getuserid()];
    Employee__c emp = new Employee__c();
    emp.name = 'Test';
    emp.Start_Date__c= system.today();
    emp.Supervisor__c = 'Rajesh';
    emp.First_Name__c ='N';
    emp.Title__c ='Title';
    emp.Last_Name__c ='Test';
    emp.Move_User_To__c ='move user';
    emp.Office_Location__c ='Hyderabad';
    emp.Employee_Role__c  ='Developer';
    emp.Employee_Status__c='On Boarding inProcess';
    emp.Terminated__c = false;
    emp.Department__c='01-External Sales';
    insert emp;
    
     Asset_Inventory__c  ai = new Asset_Inventory__c ();
    ai.Asset_Type__c = 'Building';
    ai.Processed__c = true;
    ai.Application_Owner__c = u.id;
    ai.Application_Implementor__c = u.id;
    ai.Processed__c = true;
    ai.Name='ADP'; 
   insert ai;

     Asset__c a = new Asset__c();
    a.Added_On__c= system.today();
    a.Asset_Status__c='Added';
    a.Added_On__c = system.today();
    a.Access_Name__c = 'VDI';
    a.Access_Type__c = 'Building';
    a.Email_Subject_Asset__c='email';
    a.Removed_On__c = system.today();
    a.Termination_Status__c ='Active';
    a.Terminating__c = false;
    a.Updating__c = true;
    a.Asset_Inventory__c = ai.id;
    a.Application_Owner__c = u.id;
    a.Application_Implementor__c = u.id;
    a.Employee__c = emp.id;
   insert a;
   
     a.Asset_Status__c = 'Removal In Process';
     a.Terminating__c = true;
     a.Departmant_Exco__c =true;
     update a;
     
    system.assertequals(a.Employee__c,emp.id);

    //ApexPages.currentPage().getParameters().put('quoteId1',emp.id);
    //NewhireNdcontroller nd = new NewhireNdcontroller();
    //nd.buildingTypelist();
    
    
    }
}