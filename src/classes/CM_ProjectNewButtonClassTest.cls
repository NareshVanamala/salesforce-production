@isTest
private class CM_ProjectNewButtonClassTest
{
    static testmethod void test1() 
    {
       Project__c ct=new Project__c();
       String Url=System.Label.Current_Org_Url+'/a3s/e?nooverride=1&retURL=%2Fa3s%2Fo&RecordType=012500000005cCP&ent=01I500000003doK&Name=Will Auto fill';
       PageReference acctPage = new PageReference(Url);
       Test.setCurrentPage(acctPage); 

       ApexPages.StandardController controller = new ApexPages.StandardController(ct);
       CM_ProjectNewButtonClass csr=new CM_ProjectNewButtonClass (controller ); 
       //csr.cmbs =cam;  
        csr.Gobacktostandardpage();

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standard@testorg.com');
        Insert u;
        
        Vendor__c Vc= new Vendor__c();
        Vc.name='TestSnehal';
        Vc.City__c='Hyderbad';
        insert Vc;
        
        MRI_PROPERTY__c mpt= new MRI_PROPERTY__c();
        mpt.name='TestRec';
        mpt.Property_Type__c= 'Freestanding Retail';
        insert mpt;
        MRI_PROPERTY__c mp = [SELECT ID, Team__c FROM MRI_PROPERTY__c  Where ID =:mpt.ID];
          System.assertNotEquals( Vc.name,mpt.name);
        
        Project__c pct= new Project__c();
        pct.name='TestProject';
        pct.Building__c=mpt.id;
        pct.Status__c ='Complete';
        Pct.Vendor__c = Vc.Id;
        Pct.Actual_Cost__c = 123456;
        pct.Work_Type__c = 'Tenant Improvement';
        pct.Tenant_Name__c ='Test Tenant';
        pct.Maintenance_Manager__c= u.id;
        insert pct;
        
        Project__c pct1= new Project__c();
        pct1.name='TestProject1';
        pct1.Building__c=mpt.id;
        pct1.Status__c ='Complete';
        pct1.Vendor__c = Vc.Id;
        pct1.Actual_Cost__c = 123456;
        pct1.Work_Type__c = 'Tenant Improvement';
        pct1.Tenant_Name__c ='Test Tenant';
        pct1.Maintenance_Manager__c= u.id;
        insert pct1; 
        
        System.assertNotEquals(pct.name,pct1.name);               
        Project__c pct2= new Project__c();
        pct2.name='TestProject1';
        pct2.Building__c=mpt.id;
        pct2.Status__c ='Complete';
        pct2.Vendor__c = Vc.Id;
        pct2.Actual_Cost__c = 123456;
        pct2.Work_Type__c = 'BTS';
        pct2.Tenant_Name__c ='Test Tenant';
        pct2.Maintenance_Manager__c= u.id;
        insert pct2;
        System.assertEquals(pct2.name,'TestProject1');

        
      /* MRI_PROPERTY__c mpy= new MRI_PROPERTY__c();
       mpy.name='Testlist';
       insert mpy;

       Project__c ct1=new Project__c();
       ct1.Building__c=mpy.id;*/
        ApexPages.StandardController controller1 = new ApexPages.StandardController(pct1);
        CM_ProjectNewButtonClass csr1=new CM_ProjectNewButtonClass (controller1); 
        //csr1.recordtypeid='012R00000009CRZ';
        csr1.Gobacktostandardpage();

        RecordType rt = [select id,Name from RecordType where SobjectType='Project__c' and Name='Maintenance Request' Limit 1];
         
       Project__c ct11=new Project__c();
       ct11.Building__c=mpt.id;
       ct11.recordtypeid=rt.id;
        ApexPages.StandardController controller121 = new ApexPages.StandardController(ct11);
          CM_ProjectNewButtonClass csr121=new CM_ProjectNewButtonClass (controller121 ); 
          csr121.Gobacktostandardpage();

    }
    static testmethod void test2() 
    {
       Project__c ct=new Project__c();
       String Url1=System.Label.Current_Org_Url+'/a3s/e?nooverride=1&retURL=%2Fa3s%2Fo&RecordType=012500000005cCP&ent=01I500000003doK&Name=Will Auto fill';
       PageReference acctPage = new PageReference(Url1);
       Test.setCurrentPage(acctPage); 

       ApexPages.StandardController controller = new ApexPages.StandardController(ct);
       CM_ProjectNewButtonClass csr=new CM_ProjectNewButtonClass (controller ); 
       //csr.cmbs =cam;  
        csr.Gobacktostandardpage();
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standard@testorg.com');
        Insert u;
        
        Vendor__c Vc= new Vendor__c();
        Vc.name='TestSnehal';
        Vc.City__c='Hyderbad';
        insert Vc;
        
        MRI_PROPERTY__c mpt= new MRI_PROPERTY__c();
        mpt.name='TestRec';
        mpt.Property_Type__c= 'Freestanding Retail';
        insert mpt;
        MRI_PROPERTY__c mp = [SELECT ID, Team__c FROM MRI_PROPERTY__c  Where ID =:mpt.ID];
        
        System.assertNotEquals( Vc.name,mpt.name);
        
        Project__c pct= new Project__c();
        pct.name='TestProject';
        pct.Building__c=mpt.id;
        pct.Status__c ='Complete';
        Pct.Vendor__c = Vc.Id;
        Pct.Actual_Cost__c = 123456;
        pct.Work_Type__c = 'Tenant Improvement';
        pct.Tenant_Name__c ='Test Tenant';
        pct.Maintenance_Manager__c= u.id;
        insert pct;
        
        Project__c pct1= new Project__c();
        pct1.name='TestProject1';
        pct1.Building__c=mpt.id;
        pct1.Status__c ='Complete';
        pct1.Vendor__c = Vc.Id;
        pct1.Actual_Cost__c = 123456;
        pct1.Work_Type__c = 'Tenant Improvement';
        pct1.Tenant_Name__c ='Test Tenant';
        pct1.Maintenance_Manager__c= u.id;
        insert pct1; 
        
         System.assertNotEquals(pct.name,pct1.name);

        
                       
      /* MRI_PROPERTY__c mpy= new MRI_PROPERTY__c();
       mpy.name='Testlist';
       insert mpy;

       Project__c ct1=new Project__c();
       ct1.Building__c=mpy.id;*/
        ApexPages.StandardController controller1 = new ApexPages.StandardController(pct1);
        CM_ProjectNewButtonClass csr1=new CM_ProjectNewButtonClass (controller1); 
        //csr1.recordtypeid='012R00000009CRZ';
        csr1.Gobacktostandardpage();

        RecordType rt = [select id,Name from RecordType where SobjectType='Project__c' and Name='Project' Limit 1];
         
       Project__c ct11=new Project__c();
     
        ct11.name='TestProject1';
        ct11.Building__c=mpt.id;
        ct11.Status__c ='Complete';
        ct11.Vendor__c = Vc.Id;
        ct11.Actual_Cost__c = 123456;
        ct11.Work_Type__c = 'BTS';
        ct11.Tenant_Name__c ='Test Tenant';
        ct11.Maintenance_Manager__c= u.id;
        
       ct11.Building__c=mpt.id;
       ct11.recordtypeid=rt.id;
        ApexPages.StandardController controller121 = new ApexPages.StandardController(ct11);
          CM_ProjectNewButtonClass csr121=new CM_ProjectNewButtonClass (controller121 ); 
          csr121.Gobacktostandardpage();

    }
    
    
    
}