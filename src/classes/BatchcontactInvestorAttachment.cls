global class BatchcontactInvestorAttachment implements Database.Batchable<sObject>,Database.AllowsCallouts{
global string sessionId;
global string advisorId;
global BatchcontactInvestorAttachment(){
  this.sessionId  = sessionId;
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //string contactid='0033000000GYhMe';
         //string contactid = scope[0].Advisor_Name__c;
        String query = 'select id from contact where Contactinvestorscount__c >0 LIMIT 9999';
        return Database.getQueryLocator(query);
    }
      global void execute(Database.BatchableContext BC, List<contact> scope) {
         advisorId = scope[0].id;
         ContactInvestorAttachmentCtrl.CreateContactInvAttachment(advisorId,this.sessionId);
    }
        
    global void finish(Database.BatchableContext BC) {
        
    }
}