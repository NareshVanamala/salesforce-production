@isTest
public class Test_ocms_DailyNAVList  
{
    public static testmethod void Test_ocms_DailyNAVList() 
    { 
        profile pf=[select id,name from profile where name='Cole Capital Community'];
        account a=new account();
        a.name='test';
        insert a;
        
        Contact c1= new Contact();
        c1.firstName='Ninad';
        c1.lastName='Tambe';
        c1.accountid=a.id;
        c1.email='test123@noemail.com';
        insert c1;
        
        User u = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = pf.id, country='United States',IsActive =true,
                ContactId = c1.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert u;
        
        System.assertEquals(u.ProfileId,pf.Id);

         
        system.runas(u)
        {
        Distributions__c di=new Distributions__c();
        di.type_of_share__c='class w shares';
        di.daily_nav__c=40.00;
        di.Date_As_Of__c=system.today();
        di.date_paid__c=system.today();
        di.Net_Asset_Value__c=30.00;
        di.Annualized_Yield__c=10.00;
        di.Daily_Distribution__c=10.00;
        di.category__c='DailyNAV';
        insert di;
     
        ocms_DailyNAVList rc14=new ocms_DailyNAVList();
        try
        {
            rc14.getDailyNAVList('class w shares','1 Month');
            rc14.writeControls();
            rc14.getHtml();
            
        }
          catch(Exception e)  {        }    
         
        
        ocms_DailyNAVList rc141=new ocms_DailyNAVList();
        try
        {
            rc141.getDailyNAVList('class w shares','3 Months');
            rc141.writeControls();
            rc141.getHtml();
            
        }
          catch(Exception e)  {        }    
         
        
        ocms_DailyNAVList rc142=new ocms_DailyNAVList();
        try
        {
            rc142.getDailyNAVList('class w shares','6 Months');
            rc142.writeControls();
            rc142.getHtml();
            
        }
          catch(Exception e)  {        }    
         
        
        ocms_DailyNAVList rc143=new ocms_DailyNAVList();
        try
        {
            rc143.getDailyNAVList('class w shares','1 Year');
            rc143.writeControls();
            rc143.getHtml();
            
        }
          catch(Exception e)  {        }    
         
        
        ocms_DailyNAVList rc144=new ocms_DailyNAVList();
        try
        {
            rc144.getDailyNAVList('class w shares','Year to Date');
            rc144.writeControls();
            rc144.getHtml();
            
        }
          catch(Exception e)  {        }    
         
        
        
        ocms_DailyNAVList rc1441=new ocms_DailyNAVList();
        try
        {
            rc1441.getDailyNAVList('class w shares','Since Inception');
            rc1441.writeControls();
            rc1441.getHtml();
            
        }
          catch(Exception e)  {        }     
        
        }
         }
         
   }