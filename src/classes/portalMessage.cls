global with sharing class portalMessage extends cms.ContentTemplateController implements cms.ServiceInterface {

    global portalMessage() {

    }

    global portalMessage (cms.CreateContentController cc) {
        super(cc);
    }
    
    global portalMessage(cms.GenerateContent gc) {
        super(gc);
        setDetailMessage();
    }

    public override String getHTML() {
        String html = '';
        return html;
    }

    global System.Type getType() { return portalMessage.class;}

    global String executeRequest (Map <String, String> p) {
        String response = '{"success": false}';
        if (p.get('action') == 'deleteCurrentMessage') {
            String messageId = p.get('msgId');
            response = '{"success": true, "deleted": ' + json.serialize(deleteCurrentMessage(messageId)) + '}';
        }
        else if (p.get('action') == 'deleteMessages') {
            String formattedJson = '{"msgIds":' + p.get('msgIds') + '}';
            messageIds messageIdList = (messageIds)JSON.deserialize(formattedJson, messageIds.class);
            System.debug(messageIdList);
            response = '{"success": true, "deleted": ' + json.serialize(deleteMessages(messageIdList)) + '}';
        }
        return response;
    }

    /**
    * @description a class to deserialize the list of json message id's to 
    **/
    public class messageIds {
        public List<String> msgIds;
    }

    //Variables 
    Map<String, String> pageParameters;
    private String userId;
    private String contentLayoutName {get; set;}
    private String msgId {get; set;}
    public Portal_Message__c detailMsg{get;set;}
    private final String MESSAGE_LIST_VIEW = 'PortalMessageList';
    private final String MESSAGE_DETAIL_VIEW = 'PortalMessageDetail';

    @TestVisible private String getContentLayoutNameByContentLayoutId(String contentLayoutId) {
        cms__Content_Layout__c contentLayout = Database.query('SELECT Id, cms__Name__c, cms__Label__c FROM cms__Content_Layout__c WHERE Id = :contentLayoutId');
        return (String) contentLayout.get('cms__Name__c');
    }

    
    private void setDetailMessage() {
        pageParameters = ApexPages.currentPage().getParameters();
        msgId = pageParameters.get('msgId');
        if (msgId != null) {
            Portal_Message__c portalMessage =  Database.query('SELECT Id, Message_Subject__c, Message_Body__c, New_Message__c, Message_Date__c FROM Portal_Message__c WHERE Id = :msgId LIMIT 1');
            detailMsg = portalMessage;
        }
    }

    /**
    * @description get all messages for user with an id that equals userId
    **/
    public List<Portal_Message__c> pm {
        get {
            if (pm == null) {
                userId = UserInfo.getUserId();
                pm = new List<Portal_Message__c>();
                for (Portal_Message__c msg : Database.query('SELECT Id, Message_Subject__c, Message_Body__c, New_Message__c, Message_Date__c FROM Portal_Message__c WHERE Message_User__c = :userId ORDER BY Message_Date__c DESC')) {
                    pm.add(msg);
                }
            }

            return pm;

            } set;
    }

    /**
    * @decription get the URL vars
    **/
    public String detailPageString {
        get {
            return getProperty('detailPage');
        }
        set;
    }

    public cms.Link detailPageCMSLink {
        get { 
            if (this.detailPageCMSLink == null) {
                if (this.detailPageString != null) {
                    this.detailPageCMSLink = new cms.Link(this.detailPageString);
                }
            }
            return detailPageCMSLink;
        }
        set;
    }

    public String dPage {
        get{
            String dp = this.detailPageCMSLink.targetPage;
            dp = dp.replace('#tab', '%23tab');
            return dp;
        }
        set;
    }

    public String messagePageString {
        get {
            return getProperty('messagePage');
        }
        set;
    }

    public cms.Link messagePageCMSLink {
        get { 
            if (this.messagePageCMSLink == null) {
                if (this.messagePageString != null) {
                    this.messagePageCMSLink = new cms.Link(this.messagePageString);
                }
            }
            return messagePageCMSLink;
        }
        set;
    }

    public String mPage {
        get{
            String mp = this.messagePageCMSLink.targetPage;
            mp = mp.replace('#tab', '%23tab');
            //List <String> splitter = mp.split('&msgId');
            //String finalString = splitter[0].replace('&msgId', '');
            //return finalString;
            return mp;
        } set;
    }
        

    
    /**
    * @description This function gets called from the action function in the
    *               detail view of the message portal. This function will get 
    *               the currently loaded message and update the New Message 
    *               field to false  
    **/
    public void updateNewMsgStatus() {
        Portal_Message__c dMsg = new Portal_Message__c();
        pageParameters = ApexPages.currentPage().getParameters();
        msgId = pageParameters.get('msgId');
        if (msgId != null) {
            Portal_Message__c portalMessage =  Database.query('SELECT Id, Message_Subject__c, Message_Body__c, New_Message__c, Message_Date__c FROM Portal_Message__c WHERE Id = :msgId LIMIT 1');
            dMsg = portalMessage;
        }   
        if (dMsg.New_Message__c) {
            dMsg.New_Message__c = false;
            if (Schema.sObjectType.Portal_Message__c.isUpdateable()) 

            update dMsg;
        }
    }

    /**
    * @description This function will delete the the record currently loaded
    *               within the detail panel. 
    **/
    public Boolean deleteCurrentMessage(String mId) {
        Portal_Message__c portalMessage = new Portal_Message__c();
        portalMessage =  [SELECT Id, Message_Subject__c, Message_Body__c, New_Message__c, Message_Date__c FROM Portal_Message__c WHERE Id = :mId LIMIT 1];
         if (Portal_Message__c.sObjectType.getDescribe().isDeletable()) 
            {
                    delete portalMessage;
            }
        return true;
    }

    public Boolean deleteMessages(messageIds mIds) {
        List<Portal_Message__c> pMsgList = [SELECT Id, Message_Subject__c, Message_Body__c, New_Message__c, Message_Date__c FROM Portal_Message__c WHERE Id = :mIds.msgIds];
        System.debug(pMsgList);
        for (Portal_Message__c pMsg : pMsgList) 
        {
            if (Portal_Message__c.sObjectType.getDescribe().isDeletable()) 
            delete pMsg;
        }
        return true;
    }

}