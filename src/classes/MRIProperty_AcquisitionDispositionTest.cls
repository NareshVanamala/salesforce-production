@isTest
private class MRIProperty_AcquisitionDispositionTest
{
    @isTest
    static void testInvoke1(){
        List<RecordType> rt = [select id,Name from RecordType where SobjectType='Deal__c' and (Name='Acquisition' or Name='Disposition') Limit 2];

        Map<String, RecordType> recId= new Map<String, RecordType>();

        for(RecordType r: rt){
            recId.put(r.Name, r);
        }

        test.starttest();
            
            List<Deal__c> deals= new List<Deal__c>();

            Deal__c d1 = new Deal__c();
            d1.name='test deal';
            d1.Cole_Initials__c='test cole';
            d1.Property_Type__c='Land';
            d1.Tenancy__c='Single-Tenant';
            d1.Primary_Use__c='Retail';
            d1.Contract_Price__c=20252;
            d1.Report_Deal_Name__c='testing';
            d1.Deal_Status__c ='Reviewing';
            d1.RecordType= recId.get('Acquisition');
            d1.Owned_Id__c='A1234';
            deals.add(d1);

            /*Deal__c d2 = new Deal__c();
            d2.name='test deal2';
            d2.Cole_Initials__c='test cole';
            d2.Property_Type__c='Land';
            d2.Tenancy__c='Single-Tenant';
            d2.Primary_Use__c='Retail';
            d2.Contract_Price__c=20252;
            d2.Report_Deal_Name__c='testing';
            d2.Deal_Status__c ='Reviewing';
            d2.RecordType= recId.get('Disposition');
            d2.Owned_Id__c='A1234';
            deals.add(d2);

            Deal__c d3 = new Deal__c();
            d3.name='test deal3';
            d3.Cole_Initials__c='test cole';
            d3.Property_Type__c='Land';
            d3.Tenancy__c='Single-Tenant';
            d3.Primary_Use__c='Retail';
            d3.Contract_Price__c=20252;
            d3.Report_Deal_Name__c='testing';
            d3.Deal_Status__c ='Reviewing';
            d3.RecordType= recId.get('Disposition');
            d3.Owned_Id__c='A12345';
            deals.add(d3);

            Deal__c d4 = new Deal__c();
            d4.name='test deal4';
            d4.Cole_Initials__c='test cole';
            d4.Property_Type__c='Land';
            d4.Tenancy__c='Single-Tenant';
            d4.Primary_Use__c='Retail';
            d4.Contract_Price__c=20252;
            d4.Report_Deal_Name__c='testing';
            d4.Deal_Status__c ='Reviewing';
            d4.RecordType= recId.get('Acquisition');
            d4.Owned_Id__c='A12345';
            deals.add(d4);*/

           // insert deals;

            MRI_PROPERTY__c m= new MRI_PROPERTY__c();
            m.Property_ID__c='A1234';
            m.name='Test_MRI_Property';
            insert m;

            MRI_PROPERTY__c m1= new MRI_PROPERTY__c();
            m1.Property_ID__c='A123456';
            m1.name='Test_MRI_Property1';
            insert m1;

            //MRIProperty_AcquisitionDispositionInvoke.invokeMRIProperty(new List<String>{m.id});

            m.Property_ID__c='A12345';
            update m;

            //MRIProperty_AcquisitionDispositionInvoke.invokeMRIProperty(new List<String>{m.id});

            m.Property_ID__c='A1234';
            update m;
            //MRIProperty_AcquisitionDispositionInvoke.invokeMRIProperty(new List<String>{m.id});

            //d1.Owned_Id__c='A12345';
            //update d1;
            //MRIProperty_AcquisitionDispositionInvoke.invokeMRIProperty(new List<String>{m.id});

           // d1.Owned_Id__c='A123456';
            //update d1;
            
            //MRIProperty_AcquisitionDispositionInvoke.invokeMRIProperty(new List<String>{m.id});
        test.stoptest();     
    }
    
    /*@isTest
    /*static void testInvoke2(){
        List<RecordType> rt = [select id,Name from RecordType where SobjectType='Deal__c' and (Name='Acquisition' or Name='Disposition') Limit 2];

        Map<String, RecordType> recId= new Map<String, RecordType>();

        for(RecordType r: rt){
            recId.put(r.Name, r);
        }

        test.starttest();
            
            List<Deal__c> deals= new List<Deal__c>();

            Deal__c d1 = new Deal__c();
            d1.name='test deal';
            d1.Cole_Initials__c='test cole';
            d1.Property_Type__c='Land';
            d1.Tenancy__c='Single-Tenant';
            d1.Primary_Use__c='Retail';
            d1.Contract_Price__c=20252;
            d1.Report_Deal_Name__c='testing';
            d1.Deal_Status__c ='Reviewing';
            d1.RecordType= recId.get('Acquisition');
            d1.Owned_Id__c='A1234';
            deals.add(d1);

            Deal__c d2 = new Deal__c();
            d2.name='test deal2';
            d2.Cole_Initials__c='test cole';
            d2.Property_Type__c='Land';
            d2.Tenancy__c='Single-Tenant';
            d2.Primary_Use__c='Retail';
            d2.Contract_Price__c=20252;
            d2.Report_Deal_Name__c='testing';
            d2.Deal_Status__c ='Reviewing';
            d2.RecordType= recId.get('Disposition');
            d2.Owned_Id__c='A1234';
            deals.add(d2);

            Deal__c d3 = new Deal__c();
            d3.name='test deal3';
            d3.Cole_Initials__c='test cole';
            d3.Property_Type__c='Land';
            d3.Tenancy__c='Single-Tenant';
            d3.Primary_Use__c='Retail';
            d3.Contract_Price__c=20252;
            d3.Report_Deal_Name__c='testing';
            d3.Deal_Status__c ='Reviewing';
            d3.RecordType= recId.get('Disposition');
            d3.Owned_Id__c='A12345';
            deals.add(d3);

            
            Deal__c d4 = new Deal__c();
            d4.name='test deal4';
            d4.Cole_Initials__c='test cole';
            d4.Property_Type__c='Land';
            d4.Tenancy__c='Single-Tenant';
            d4.Primary_Use__c='Retail';
            d4.Contract_Price__c=20252;
            d4.Report_Deal_Name__c='testing';
            d4.Deal_Status__c ='Reviewing';
            d4.RecordType= recId.get('Acquisition');
            d4.Owned_Id__c='A12345';
            deals.add(d4);
            
            
            Deal__c d5 = new Deal__c();
            d5.name='test deal5';
            d5.Cole_Initials__c='test colesdf';
            d5.Property_Type__c='Land';
            d5.Tenancy__c='Single-Tenant';
            d5.Primary_Use__c='Retail';
            d5.Deal_Status__c ='Reviewing';
            d5.RecordType= recId.get('Disposition');
            d5.Owned_Id__c='A123456';
            deals.add(d5);
            
            
            insert deals;

            MRI_PROPERTY__c m= new MRI_PROPERTY__c();
            m.Property_ID__c='A1234';
            m.Related_Deal__c=d1.id;
            m.Disposition_in_DC__c= d2.id;
            m.name='Test_MRI_Property';
            insert m;

            m.Property_ID__c='A12345';
            update m;

            m.Property_ID__c='A1234';
            update m;
            //MRIProperty_AcquisitionDispositionInvoke.invokeMRIProperty(new List<String>{m.id});

            MRI_PROPERTY__c m1= new MRI_PROPERTY__c();
            m1.Property_ID__c='A12345';
            m1.name='Test_MRI_Property1';
            insert m1;
            
            
            MRI_PROPERTY__c m2= new MRI_PROPERTY__c();
            m2.Property_ID__c='A123456';
            m2.name='Test_MRI_Property2';
            insert m2;
            
            delete d2;
            
            m.Property_ID__c='A12345';
            update m;
            
            m.Property_ID__c='A1234';
            update m;
            
            delete d1;
            
            m.Property_ID__c='A123456';
            update m;
            
            delete m;
            m2.Property_ID__c='A1234';
            update m2;
            
            m2.Property_ID__c='A123456';
            update m2;

            delete d5;
            Deal__c d6 = new Deal__c();
            d6.name='test deal5';
            d6.Cole_Initials__c='test colesdf';
            d6.Property_Type__c='Land';
            d6.Tenancy__c='Single-Tenant';
            d6.Primary_Use__c='Retail';
            d6.Deal_Status__c ='Reviewing';
            d6.RecordType= recId.get('Disposition');
            d6.Owned_Id__c='A123456';
            insert d6;
            
            d6.Owned_Id__c='A1234';
            update d6;  
            
        test.stoptest();     
    }
    
    
    @isTest
    static void testInvoke3(){
        List<RecordType> rt = [select id,Name from RecordType where SobjectType='Deal__c' and (Name='Acquisition' or Name='Disposition') Limit 2];

        Map<String, RecordType> recId= new Map<String, RecordType>();

        for(RecordType r: rt){
            recId.put(r.Name, r);
        }

        test.starttest();
            
            List<Deal__c> deals= new List<Deal__c>();

            Deal__c d1 = new Deal__c();
            d1.name='test deal1';
            d1.Cole_Initials__c='test cole';
            d1.Property_Type__c='Land';
            d1.Tenancy__c='Single-Tenant';
            d1.Primary_Use__c='Retail';
            d1.Contract_Price__c=20252;
            d1.Report_Deal_Name__c='testing';
            d1.Deal_Status__c ='Reviewing';
            d1.RecordType= recId.get('Acquisition');
            d1.Owned_Id__c='A1234';
            deals.add(d1);

            Deal__c d2 = new Deal__c();
            d2.name='test deal2';
            d2.Cole_Initials__c='test cole';
            d2.Property_Type__c='Land';
            d2.Tenancy__c='Single-Tenant';
            d2.Primary_Use__c='Retail';
            d2.Contract_Price__c=20252;
            d2.Report_Deal_Name__c='testing';
            d2.Deal_Status__c ='Reviewing';
            d2.RecordType= recId.get('Disposition');
            d2.Owned_Id__c='A1234';
            deals.add(d2);
            insert deals;
            
            List<MRI_PROPERTY__c > mlist= new List<MRI_PROPERTY__c >();
            MRI_PROPERTY__c m= new MRI_PROPERTY__c();
            m.Property_ID__c='A1234';
            m.Related_Deal__c=d1.id;
            m.Disposition_in_DC__c= d2.id;
            m.name='Test_MRI_Property';
            mlist.add(m);
            
            MRI_PROPERTY__c m1= new MRI_PROPERTY__c();
            m1.Property_ID__c='A12345';
            m1.name='Test_MRI_Property111';
            mlist.add(m1);
            
            insert mlist;
            
            d2.Owned_Id__c='A12345';
            update d2;
            d1.Owned_Id__c='A1235';
            update d1;
            
            delete d2;
            deals= new List<Deal__c>();
            
            Deal__c d3 = new Deal__c();
            d3.name='test deal3';
            d3.Cole_Initials__c='test cole';
            d3.Property_Type__c='Land';
            d3.Tenancy__c='Single-Tenant';
            d3.Primary_Use__c='Retail';
            d3.Contract_Price__c=20252;
            d3.Report_Deal_Name__c='testing';
            d3.Deal_Status__c ='Reviewing';
            d3.RecordType= recId.get('Acquisition');
            d3.Owned_Id__c='A1234';
            deals.add(d3);

            Deal__c d4 = new Deal__c();
            d4.name='test deal4';
            d4.Cole_Initials__c='test cole';
            d4.Property_Type__c='Land';
            d4.Tenancy__c='Single-Tenant';
            d4.Primary_Use__c='Retail';
            d4.Contract_Price__c=20252;
            d4.Report_Deal_Name__c='testing';
            d4.Deal_Status__c ='Reviewing';
            d4.RecordType= recId.get('Disposition');
            d4.Owned_Id__c='A1234';
            deals.add(d4);
            
            Deal__c d5 = new Deal__c();
            d5.name='test deal5';
            d5.Cole_Initials__c='test cole';
            d5.Property_Type__c='Land';
            d5.Tenancy__c='Single-Tenant';
            d5.Primary_Use__c='Retail';
            d5.Contract_Price__c=20252;
            d5.Report_Deal_Name__c='testing';
            d5.Deal_Status__c ='Reviewing';
            d5.RecordType= recId.get('Acquisition');
            d5.Owned_Id__c='A1234';
            deals.add(d5);

            Deal__c d6 = new Deal__c();
            d6.name='test deal6';
            d6.Cole_Initials__c='test cole';
            d6.Property_Type__c='Land';
            d6.Tenancy__c='Single-Tenant';
            d6.Primary_Use__c='Retail';
            d6.Contract_Price__c=20252;
            d6.Report_Deal_Name__c='testing';
            d6.Deal_Status__c ='Reviewing';
            d6.RecordType= recId.get('Disposition');
            d6.Owned_Id__c='A1234';
            deals.add(d6);
            insert deals;
            
            deals= new List<Deal__C>();
            d4.Owned_Id__c='A12345';
            d5.Owned_Id__c='A12345';
            deals.add(d4);
            deals.add(d5);
            update deals;
            
            system.debug(logginglevel.info, '-------->>'+m);
            test.stoptest();     
    }*/
}