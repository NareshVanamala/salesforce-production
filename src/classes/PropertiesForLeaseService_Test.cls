@isTest (SeeAllData=true)
private class PropertiesForLeaseService_Test{
    
    @isTest static void test_method_one() {
        PropertiesForLeaseService wps = new PropertiesForLeaseService();
        
        wps.gettype();

        Map<String,String> sendMap = new Map<String,String>();
        sendMap.clear();
        sendMap.put('action','getStateList');
        //sendMap.put('filterGroceryAnchored','true');
        //sendMap.put('filterPowerCenters','true');
        //sendMap.put('filterSQFT1','true');
       // sendMap.put('filterSQFT2','true');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getCityList');
        sendMap.put('state','TX');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getResultTotal');
        sendMap.put('state','TX');
        sendMap.put('city','Houston');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getPropertyList');
        sendMap.put('state','TX');
        sendMap.put('city','Houston');
        system.assert(sendMap!= Null );
        wps.executeRequest(sendMap);

        wps.loadResponse();
    }
    
  
}