public with sharing class TemplateTaskPlan_New{

    public List<Task_Plan__c> ChildTask{get;set;}
    map<integer, integer> parindexmap = new map<integer, integer>();
    
  private Double taskIndex   { get; set; }
  public Boolean  addedTask  { get; set; }

    public Boolean applyActionPlan{get;set;}
    public String templateSelected{get;set;}
   
   
    public List<SelectOption> getRelatedObjectPicklist() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Deal__c','Deal'));
        options.add(new SelectOption('Loan__c','Loan'));
        return options;
    }

   
    public Template__c actionPlan{get;set;}
    Public Task_plan__c actionPlanTask{get;set;}
    public Boolean massAssign{get;set;}
    public String objectName{get;set;}
    public Url url{get;set;}
    public Map<Integer,Task_Plan__c> mapTaskOrder{get;set;}
    
   public List<Task_Plan__c> actionPlanTasks{get;set;}
   
    public List<Integer> RollCnt{get;set;}

    public Map<Decimal, List<Task_Plan__c>> mapTask{get;set;}
    private ApexPages.StandardController stdController;
    public String duplicateTasksString{get;set;}
        


    public TemplateTaskPlan_New(ApexPages.StandardController controller) { 
        ChildTask = new List<Task_Plan__c>();
        if(apexpages.currentpage().getparameters().get('id')==null){        
            arrangetask();    
        }
        else{
            nutralexists();
        }
        applyActionPlan=false;
        duplicateTasksString='';
        
        //pre-populate 'Tempate -' on load
        this.actionPlan= (Template__c)controller.getRecord(); 
        if(apexpages.currentpage().getparameters().get('id')==null){
            actionPlan.Name = 'Template -';  
        }
        else{
            actionPlan= [Select id, name, RelatedObjectName__c , Send_Email__c ,Template__c, Mass_Assign_To__c, Deal__c, Loan__c from Template__c where id=:apexpages.currentpage().getparameters().get('id')];
        }
        stdcontroller=controller; 
    }
     
    public void checkDuplicateTasks(){
        save();
    }
    
    public List<SelectOption> getdateFields(){
        List<SelectOption> piclistValue=new List<SelectOption>();
        String OldType;
        if(OldType!=actionPlan.RelatedObjectName__c && actionPlan.RelatedObjectName__c!=null){
            //getarrangetask();
        }
        if(actionPlan.RelatedObjectName__c==null){
        actionPlan.RelatedObjectName__c='Deal__c';
        }
        
        try{
        
        if(actionPlan.RelatedObjectName__c == 'Deal__c'){
        piclistValue.add(new selectoption('--NONE--','--NONE--'));
            for(Schema.SObjectField F : Schema.SObjectType.Deal__c.fields.getMap().values()) {
                if(String.valueof(f.getDescribe().getType())=='Date'){
                    piclistValue.add(new selectoption(f.getDescribe().getName(), f.getDescribe().getLabel()));
                }
            }
        }
        else if(actionPlan.RelatedObjectName__c == 'Loan__c'){
        
            piclistValue.add(new selectoption('--NONE--','--NONE--'));
                for(Schema.SObjectField F : Schema.SObjectType.Loan__c.fields.getMap().values()) {
                    if(String.valueof(f.getDescribe().getType())=='Date'){
                        piclistValue.add(new selectoption(f.getDescribe().getName(), f.getDescribe().getLabel()));
                        
                    }
                }
              
            }
        }
        catch(exception e){}
        OldType=actionPlan.RelatedObjectName__c;
        return piclistValue;        
    }
    
    public pagereference CalldateFields(){  
        system.debug('================'+actionPlan.RelatedObjectName__c);      
        getdateFields();
        return null;
    }
   
    public List<Task_Plan__c> actionPlanTasksList{get;set;}
    public List<Task_Plan__c> mapTaskList{get;set;}
    
    public pagereference nutralact(){
        if(apexpages.currentpage().getparameters().get('id') != null){
            actionPlan = [Select id, name, RelatedObjectName__c , Send_Email__c ,Template__c, Mass_Assign_To__c, Deal__c, Loan__c from Template__c where id=:apexpages.currentpage().getparameters().get('id')];
            actionPlanTasksList=[Select Index__c, Date_Completed__c, Name, id, Field_to_update_Date_Received__c ,Status__c,Date_Needed__c,Date_Received__c,Date_Ordered__c,Field_to_update_for_date_needed__c, Notify_Emails__c, Priority__c, Comments__c from Task_Plan__c where Template__c=:apexpages.currentpage().getparameters().get('id') and Parent__c =null order by Index__c ASC];
            mapTaskList=[Select Index__c, Name, id, parent__r.Index__c, Parent_index__c, Field_to_update_Date_Received__c , Date_Needed__c,Date_Received__c,Field_to_update_for_date_needed__c, Notify_Emails__c, Priority__c, Comments__c, Date_Ordered__c,Status__c,Parent__c  from Task_Plan__c where Template__c=:apexpages.currentpage().getparameters().get('id') and Parent__c !=null order by Index__c ASC];
        }
        return null;
    }
    public void nutralexists(){
        actionPlanTasks = [Select Index__c, Name, id, Field_to_update_Date_Received__c , Date_Needed__c,Date_Received__c,Date_Ordered__c,Field_to_update_for_date_needed__c, Notify_Emails__c, Priority__c, Date_Completed__c , Comments__c,Status__c from Task_Plan__c where Template__c=:apexpages.currentpage().getparameters().get('id') and Parent__c =null order by Index__c ASC];
        ChildTask = [Select Index__c, Name, id, parent__r.Index__c, Parent_index__c, Field_to_update_Date_Received__c , Date_Needed__c,Date_Received__c,Field_to_update_for_date_needed__c, Notify_Emails__c, Priority__c, Comments__c, Date_Ordered__c,Status__c,Parent__c  from Task_Plan__c where Template__c=:apexpages.currentpage().getparameters().get('id') and Parent__c !=null order by Index__c ASC];
    }
     
    Task_Plan__c TP;
    public pagereference addTask(){ 
       
        TP = new Task_Plan__c();      
        TP.Index__c=actionPlanTasks.size()+1;
        TP.Name = 'Task'+(TP.Index__c);
        TP.Priority__c = 'Medium';
        actionPlanTasks.add(TP);       
        return null;
    }
    
    public void arrangetask(){
        if(apexpages.currentpage().getparameters().get('id')==null){
            for(Template__c T:[Select id, name, Completed_Tasks__c, Deal__c, Escrow_Template__c, External_Id__c, Is_Template__c, Loan__c, Mail_Sent__c, Mass_Assign_To__c, RelatedObjectName__c, Send_Email__c, Status__c,  Template__c, Total_Tasks__c from Template__c where id=:apexpages.currentpage().getparameters().get('id')]){
                actionPlan=T;
            }
            RollCnt = new List<Integer>();
            actionPlanTasks = new List<Task_Plan__c>();
            actionPlanTasks.add(new Task_Plan__c(Name='Task1',Index__c=1));
            RollCnt.add(1);
           
        }
    }
    
    Map<integer, integer> parchldmap = new Map<integer, integer>();
   
    public pagereference addSubTask(){
            
        Integer ParentIndex=Integer.Valueof(apexpages.currentpage().getparameters().get('parentindex'));
        
        if(parchldmap.containskey(ParentIndex)){
            integer curval = parchldmap.get(ParentIndex);
            parchldmap.put(ParentIndex, curval+1);
        }
        else{
            parchldmap.put(ParentIndex, 1);
        }
         
        Task_Plan__c ST = new Task_Plan__c(Name='Sub Task'+parchldmap.get(ParentIndex), Priority__c ='Medium', Parent_index__c=ParentIndex, index__c = parchldmap.get(ParentIndex));
       // countrec++;
        ChildTask.add(ST);
        return null;
    }
     
    public PageReference Save(){    
       system.debug('show me my action plan'+actionplan.RelatedObjectName__c); 
        actionplan.Is_Template__c = True;
        if (Schema.sObjectType.Template__c.isCreateable())

        upsert actionPlan;
        
        for(Task_Plan__c mtp:actionPlanTasks){
        
            if(mtp.id == null){
                mtp.Assigned_To__c=Userinfo.getUserId();
                mtp.Template__c = actionPlan.id;
            }
        }
        if(actionPlanTasks.size()>0){
        
        if (Schema.sObjectType.Task_Plan__c.isCreateable())
        upsert actionPlanTasks;
        }

        Map<Decimal, id> mtpmap = new Map<Decimal, id>();
        for(Task_Plan__c mtp:actionPlanTasks){
            mtpmap.put(mtp.index__c, mtp.id);
        }
        List<Task_Plan__c> upsertchild = new List<Task_Plan__c>();
        for(Task_Plan__c ctp:ChildTask){
            if(ctp.id == null){
                ctp.Assigned_To__c=Userinfo.getUserId();
                ctp.Template__c = actionPlan.id;
                ctp.Parent__c = mtpmap.get(ctp.Parent_index__c);
                upsertchild.add(ctp);
                system.debug('--------------------------QRY-------'+ctp.Date_Ordered__c);
            }
        }
        if(upsertchild.size()>0){
        
        if (Schema.sObjectType.Task_Plan__c.isCreateable())

        upsert upsertchild;
        }
        pagereference pref = new pagereference('/'+actionPlan.id);
        //pagereference pref = new pagereference('/a3U/o');
        return pref;
    }
    Public Pagereference Edit1()
    {
     Pagereference pr = new Pagereference('/apex/TemplateTaskPlan_New?id='+apexpages.currentpage().getparameters().get('id')); 
         pr.setRedirect(true);
        // return pr;
         Pagereference pr1 = new Pagereference('/apex/ApplyActionPlanForDeal_Edit_Page?id='+apexpages.currentpage().getparameters().get('id')); 
         pr1.setRedirect(true);
        // return pr1;
        If(actionplan.Deal__c!=null || actionplan.Loan__c!=null)
        {
        return pr1; 
        } 
       else
       {
       return pr;
       }
      
    }
    
    public void removesubTask(){
        List<Task_Plan__c> DeleteTasks = new List<Task_Plan__c>();
       
        integer subtask = integer.valueof(apexpages.currentpage().getparameters().get('subtask'));
         system.debug('@@@@@'+subtask);
        integer partask = integer.valueof(apexpages.currentpage().getparameters().get('partask')); 
           
        if(subtask != null && partask != null){
            List<Task_Plan__c> compCTL = new List<Task_Plan__c>();
            compCTL = ChildTask;
           
            for(integer ins=0; ins<=compCTL.size()-1; ins++){
                if(compCTL[ins].index__c == subtask && compCTL[ins].parent_index__c == partask){
                    if(compCTL[ins].id != null){
                        DeleteTasks.add(compCTL[ins]);
                        system.debug('#####'+compCTL[ins].Name);
                    
                    }
                    ChildTask.remove(ins);
                    
                }
            }
            System.debug('!!!!!'+DeleteTasks);
            if(DeleteTasks.size()>0){
            if (Task_Plan__c.sObjectType.getDescribe().isDeletable()) 
                {
                        delete DeleteTasks;
                }
            }
        }
    }
    
    Public void RemoveTask(){
        List<Task_Plan__c> DeleteTasks = new List<Task_Plan__c>();
        integer Indexcnt = integer.valueof(apexpages.currentpage().getparameters().get('removeRec'));      
        if(Indexcnt != null){
            List<Task_Plan__c> compCTL = new List<Task_Plan__c>();
            compCTL = actionPlanTasks;
            for(integer ins=0; ins<=compCTL.size()-1; ins++){
                if(compCTL[ins].index__c == Indexcnt){
                    if(compCTL[ins].id != null){
                        DeleteTasks.add(compCTL[ins]);
                    }
                    actionPlanTasks.remove(ins);
                }
                if((compCTL.size()-1) <= ins){
                    //if(Indexcnt < compCTL[ins].index__c){
                    system.debug('------------1-----'+Indexcnt);
                    system.debug('------------2-----'+ins);
                    system.debug('------------4-----'+actionPlanTasks.size());
                    
                    
                    if((Indexcnt-1) < actionPlanTasks.size()){
                        if(Indexcnt == actionPlanTasks[Indexcnt-1].index__c){
                            actionPlanTasks[Indexcnt-1].index__c +=-1;   
                        }
                        else{
                            actionPlanTasks[ins].index__c +=-1;   
                        }
                    }
                }
            }
            
            compCTL = new List<Task_Plan__c>();
            compCTL = ChildTask;
            for(integer ins=0; ins<=compCTL.size()-1; ins++){
                if(compCTL[ins].parent_index__c == Indexcnt){
                    if(compCTL[ins].id != null){
                        DeleteTasks.add(compCTL[ins]);
                    }
                    ChildTask.remove(ins);
                }
            }
        }  
        if(DeleteTasks.size() > 0 ){
          if (Task_Plan__c.sObjectType.getDescribe().isDeletable()) 
                {
                        delete DeleteTasks;
                }
        }      
    }
}