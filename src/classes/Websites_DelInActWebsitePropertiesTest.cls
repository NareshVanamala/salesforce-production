@isTest(SeeAllData=true)
public class Websites_DelInActWebsitePropertiesTest{
  
   static testmethod void DeleteInActiveWebsitePropertiesTest(){
    Test.startTest();
    MRI_Property__c MRI = new MRI_Property__c ();
    MRI.Name ='Test';
    MRI.AvgLeaseTerm__c = 72.00;
    MRI.Property_Id__c = 'Test12345';
    MRI.OwnerId = UserInfo.getuserid();
    MRI.Property_Manager__c = UserInfo.getuserid();
    insert MRI;
    
    System.assert(MRI != null);
    
    list<id> MRIRecordIds = new List<id>();
    MRIRecordIds.add(MRI.Id);
    
    Website_Properties__c website = new Website_Properties__c();
    website.Name = 'Test';
    website.MRI_Property__c = MRI.id;
    website.Building_Id__c = 'Test12345';
    insert website;
    
    System.assert(website != null);
    
    Websites_DeleteInActiveWebsiteProperties.deleteInActiveWebsiteProperties(MRIRecordIds);
    
    Test.stopTest();
  }
}