@isTest
private class Test_departmentSplitclass
{
   public static testMethod void TestfordepartmentSplitclass() 
   {     
        Account acc = new Account();
        acc.Name = 'Test Prospect Account';
        Insert acc;
        
        //Set up user
        User us = [SELECT Id FROM User limit 1];          
           
        Event_Automation_Request__c ear = new Event_Automation_Request__c();
        ear.Name='BDE13006';
        ear.Event_Name__c='testeventname';
        ear.Start_Date__c=System.Today();
        ear.End_Date__c=System.Today();
        ear.Broker_Dealer_Name__c=acc.id;
        ear.Event_Location__c='testlocation';
        ear.Key_Account_Manager_Name_Requestor__c=us.id;
        ear.Event_Venue__c='testvenue';
        insert ear;
        system.assertequals(ear.Broker_Dealer_Name__c,acc.id);

        
        //id, Region_Split__c,name,Split__c from Event_Request_Split__c
        Event_Request_Split__c ers = new Event_Request_Split__c();
        ers.Department_Split__c='Accounting';
        ers.Department_Split_Percentage__c=100;
        ers.name='BDE12000';
        ers.Event_Automation_Request__c=ear.id;
        insert ers;
        
        system.assertequals(ers.Event_Automation_Request__c,ear.id);
  
        
        ApexPages.currentPage().getParameters().put('id',ear.id); 
        
        ApexPages.StandardController controller = new ApexPages.StandardController(ear);
        departmentSplitclass sc = new departmentSplitclass(controller);              
        
        sc.updatedlist1=ers;
        sc.removecon();
        sc.editRecord();       
        sc.SaveRecord();
        sc.newProgram();
        sc.savenewProg();
        sc.cancelnewProg();       
    }
}