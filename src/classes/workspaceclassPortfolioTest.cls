@ISTEST
private class workspaceclassPortfolioTest{

    static testMethod void workspaceTest1() {
    Test.startTest();
    Portfolio__C D = new Portfolio__c();
    D.Name = 'Test';
    D.workspace_Date__c= system.now();
    
   insert D;
   
  system.assertequals(D.Name,'Test');

   ApexPages.StandardController sc = new ApexPages.StandardController(D);
   ApexPages.currentPage().getParameters().put('id',D.Id);
   workspaceclassPortfolio controller = new workspaceclassPortfolio(sc) ;
   controller.createworkspace();
     
Test.stopTest();
}
}