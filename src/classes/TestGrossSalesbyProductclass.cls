@isTest
 private class TestGrossSalesbyProductclass 
 {
    public static testMethod void  testGrossSalesbyProduct()  
    {
        Id recdBrokerDealer=[Select id,name,DeveloperName from RecordType where DeveloperName='BrokerDealerAccount' and SobjectType = 'Account'].id; 
        Map<id,National_Account_Helper__c> newmapaccount= new Map<id,National_Account_Helper__c>();
        Account a = new Account();
        a.name = 'testaccount';
        a.recordtypeid=recdBrokerDealer;
        insert a;
        List<National_Account_Helper__c> nahListExisting = new List<National_Account_Helper__c>();
        List<REIT_Investment__c> reitnewlist = new List<REIT_Investment__c>();

        List<National_Account_Helper__c> updatednahListExisting = new List<National_Account_Helper__c>();
        nahListExisting = [select Account__c,AprilCCIT__c,AprilCCPT__c,AprilCCPTII__c,AprilCCPTIII__c,AprilCCPTIV__c,AprilIncomeNav__c,AugustCCIT__c,AugustCCPT__c,AugustCCPTII__c,AugustCCPTIII__c,AugustCCPTIV__c,AugustIncomeNav__c,CCITlastyear__c,CCPTIIILastYear__c,CCPTIILastYear__c,CCPTIVlastyear__c,CCPT_LastYear__c,CurrentYearMinus2CCIT__c,CurrentYearMinus2CCPT__c,CurrentYearMinus2CCPTII__c,CurrentYearMinus2CCPTIII__c,CurrentYearMinus2CCPTIV__c,CurrentYearMinus2IncomeNav__c,CurrentyearMinus3CCIT__c,CurrentYearMinus3CCPT__c,CurrentYearMinus3CCPTII__c,CurrentYearMinus3CCPTIII__c,CurrentYearMinus3CCPTIV__c,CurrentYearMinus3IncomeNav__c,FebCCIT__c,FebCCPT__c,FebCCPTII__c,FebCCPTIII__c,FebCCPTIV__c,FebIncomeNav__c,INCOMENAVLastyear__c,JanCCIT__c,JanCCPT__c,JanCCPTII__c,JanCCPTIII__c,JanCCPTIV__c,JanIncomeNav__c,JulyCCIT__c,JulyCCPT__c,JulyCCPTII__c,JulyCCPTIII__c,JulyCCPTIV__c,JulyIncomeNav__c,JuneCCIT__c,JuneCCPT__c,JuneCCPTII__c,JuneCCPTIII__c,JuneCCPTIV__c,JuneIncomeNav__c,MarCCIT__c,MarCCPT__c,MarCCPTII__c,MarCCPTIII__c,MarCCPTIV__c,MarIncomeNav__c,MayCCIT__c,MayCCPT__c,MayCCPTII__c,MayCCPTIII__c,MayCCPTIV__c,MayIncomeNav__c,NovCCIT__c,NovCCPT__c,NovCCPTII__c,NovCCPTIII__c,NovIncomeNav__c,OctCCIT__c,OctCCPT__c,OctCCPTII__c,OctCCPTIII__c,OctCCPTIV__c,OctIncomeNav__c,SeptCCIT__c,SeptCCPT__c,SeptCCPTII__c,SeptCCPTIII__c,SeptCCPTIV__c,SeptIncomeNav__c,DecCCIT__c,DecCCPT__c,DecCCPTII__c,DecCCPTIII__c,DecCCPTIV__c,DecIncomeNav__c,NovCCPTIV__c FROM National_Account_Helper__c WHERE Account__c=:a.id]; 
        
        for(National_Account_Helper__c  cnt:nahListExisting)
        {
            cnt.CCITlastyear__c=0;
            cnt.CCPTIIILastYear__c=0;
            cnt.CCPTIILastYear__c=0;
            cnt.CCPTIVlastyear__c=0;
            cnt.CCPT_LastYear__c=0;
            cnt.AugustCCIT__c =0;
            cnt.CurrentYearMinus2CCPT__c=0;
            cnt.CurrentYearMinus2CCIT__c=0;
            cnt.CurrentYearMinus2CCPTIII__c=0;
            cnt.CurrentYearMinus2CCPTIV__c=0;
            cnt.CurrentYearMinus2IncomeNav__c=0;
            cnt.CurrentyearMinus3CCIT__c=0;
            cnt.CurrentYearMinus3CCPT__c=0;
            cnt.CurrentYearMinus3CCPTII__c=0;
            cnt.CurrentYearMinus3CCPTIII__c=0;
            cnt.CurrentYearMinus3CCPTIV__c=0;
            cnt.CurrentYearMinus3IncomeNav__c=0;
            updatednahListExisting.add(cnt); 
         }
         update updatednahListExisting;
         
         
       
        Contact c1= new Contact();
        c1.accountid=a.id;
        c1.firstname='DonotTrustAnyone';
        c1.lastname='ThatIsTrue';
        insert c1;
        
        System.assertEquals(c1.accountid,a.id);
        
        REIT_Investment__c ct= new REIT_Investment__c();
        ct.Rep_Contact__c = c1.id;
        date d = date.newInstance(Date.today().year()-1,6,25);
        ct.Deposit_Date__c= d;
        ct.Fund__c='3770';
        ct.Current_Capital__c=15000;
        //insert ct;
        reitnewlist.add(ct);
        //updatednahListExisting[0].CCITlastyear__c= ct.Current_Capital__c;
        
        REIT_Investment__c ct1= new REIT_Investment__c();
        ct1.Rep_Contact__c = c1.id;
        ct1.Deposit_Date__c= d;
        ct1.Fund__c='3770';
        ct1.Current_Capital__c=15000;
        reitnewlist.add(ct1);
        
        REIT_Investment__c ccptlastyear= new REIT_Investment__c();
        ccptlastyear.Rep_Contact__c = c1.id;
        ccptlastyear.Deposit_Date__c= d;
        ccptlastyear.Fund__c='3700';
        ccptlastyear.Original_Capital__c=15000;
         reitnewlist.add(ccptlastyear);  
        
        REIT_Investment__c ccpt3= new REIT_Investment__c();
        ccpt3.Rep_Contact__c = c1.id;
        ccpt3.Deposit_Date__c= d;
        ccpt3.Fund__c='3767';
        ccpt3.Current_Capital__c=15000;
        reitnewlist.add(ccpt3); 
       
        /*REIT_Investment__c ct2= new REIT_Investment__c();
        ct2.Rep_Contact__c = c1.id;
        ct2.Deposit_Date__c= d;
        ct2.Fund__c='3771';
        ct2.Current_Capital__c=15000;
        reitnewlist.add(ct2);*/  
         
       
       REIT_Investment__c ct3= new REIT_Investment__c();
        ct3.Rep_Contact__c = c1.id;
        ct3.Deposit_Date__c= d;
        ct3.Fund__c='3772';
        ct3.Current_Capital__c=15000;
         reitnewlist.add(ct3);    
       
        REIT_Investment__c ct4= new REIT_Investment__c();
        ct4.Rep_Contact__c = c1.id;
        ct4.Deposit_Date__c= d;
        ct4.Fund__c='3701';
        ct4.Current_Capital__c=15000;
        reitnewlist.add(ct4);    
     
     //************************current year minus 2***************   
        REIT_Investment__c ct5= new REIT_Investment__c();
        date d1 = date.newInstance(Date.today().year()-2,6,25);
        ct5.Rep_Contact__c = c1.id;
        ct5.Deposit_Date__c= d1;
        ct5.Fund__c='3700';
        ct5.Original_Capital__c=15000;
         reitnewlist.add(ct5);   
         
        System.assertEquals(ct5.Rep_Contact__c,c1.id);

        
        REIT_Investment__c ccptIIcurrentyearminus2= new REIT_Investment__c();
        ccptIIcurrentyearminus2.Rep_Contact__c = c1.id;
        ccptIIcurrentyearminus2.Deposit_Date__c= d1;
        ccptIIcurrentyearminus2.Fund__c='3701';
        ccptIIcurrentyearminus2.Current_Capital__c=15000;
         reitnewlist.add(ccptIIcurrentyearminus2);   
       
        REIT_Investment__c ccptIIIcurrentyearminus2= new REIT_Investment__c();
        ccptIIIcurrentyearminus2.Rep_Contact__c = c1.id;
        ccptIIIcurrentyearminus2.Deposit_Date__c= d1;
        ccptIIIcurrentyearminus2.Fund__c='3767';
        ccptIIIcurrentyearminus2.Current_Capital__c=15000;
      reitnewlist.add(ccptIIIcurrentyearminus2); 
       
        REIT_Investment__c ccITcurrentyearminus2= new REIT_Investment__c();
        ccITcurrentyearminus2.Rep_Contact__c = c1.id;
        ccITcurrentyearminus2.Deposit_Date__c= d1;
        ccITcurrentyearminus2.Fund__c='3770';
        ccITcurrentyearminus2.Current_Capital__c=15000;
        reitnewlist.add(ccITcurrentyearminus2);
        
        REIT_Investment__c ccPTIVcurrentyearminus2= new REIT_Investment__c();
        ccPTIVcurrentyearminus2.Rep_Contact__c = c1.id;
        ccPTIVcurrentyearminus2.Deposit_Date__c= d1;
        ccPTIVcurrentyearminus2.Fund__c='3772';
        ccPTIVcurrentyearminus2.Current_Capital__c=15000;
        reitnewlist.add(ccPTIVcurrentyearminus2);
        
       
       /* REIT_Investment__c IncomeNavcurrentyearminus2= new REIT_Investment__c();
        IncomeNavcurrentyearminus2.Rep_Contact__c = c1.id;
        IncomeNavcurrentyearminus2.Deposit_Date__c= d1;
        IncomeNavcurrentyearminus2.Fund__c='3771';
        IncomeNavcurrentyearminus2.Current_Capital__c=15000;
        reitnewlist.add(IncomeNavcurrentyearminus2);*/
   //****************currentminus3***************
        REIT_Investment__c ccptcurrentminus3= new REIT_Investment__c();
        date d2 = date.newInstance(Date.today().year()-3,6,25);
        ccptcurrentminus3.Rep_Contact__c = c1.id;
        ccptcurrentminus3.Deposit_Date__c= d1;
        ccptcurrentminus3.Fund__c='3700';
        ccptcurrentminus3.Original_Capital__c=15000;
        reitnewlist.add(ccptcurrentminus3); 
        
        REIT_Investment__c ccptIIcurrentyearminus3= new REIT_Investment__c();
        ccptIIcurrentyearminus3.Rep_Contact__c = c1.id;
        ccptIIcurrentyearminus3.Deposit_Date__c= d2;
        ccptIIcurrentyearminus3.Fund__c='3701';
        ccptIIcurrentyearminus3.Current_Capital__c=15000;
        reitnewlist.add(ccptIIcurrentyearminus3); 
       
        REIT_Investment__c ccptIIIcurrentyearminus3= new REIT_Investment__c();
        ccptIIIcurrentyearminus3.Rep_Contact__c = c1.id;
        ccptIIIcurrentyearminus3.Deposit_Date__c= d2;
        ccptIIIcurrentyearminus3.Fund__c='3767';
        ccptIIIcurrentyearminus3.Current_Capital__c=15000;
        reitnewlist.add(ccptIIIcurrentyearminus3); 
       
        REIT_Investment__c ccITcurrentyearminus3= new REIT_Investment__c();
        ccITcurrentyearminus3.Rep_Contact__c = c1.id;
        ccITcurrentyearminus3.Deposit_Date__c= d2;
        ccITcurrentyearminus3.Fund__c='3770';
        ccITcurrentyearminus3.Current_Capital__c=15000;
        reitnewlist.add(ccITcurrentyearminus3);  
        
        REIT_Investment__c ccPTIVcurrentyearminus3= new REIT_Investment__c();
        ccPTIVcurrentyearminus3.Rep_Contact__c = c1.id;
        ccPTIVcurrentyearminus3.Deposit_Date__c= d2;
        ccPTIVcurrentyearminus3.Fund__c='3772';
        ccPTIVcurrentyearminus3.Current_Capital__c=15000;
         reitnewlist.add(ccPTIVcurrentyearminus3); 
       
       /* REIT_Investment__c IncomeNavcurrentyearminus3= new REIT_Investment__c();
        IncomeNavcurrentyearminus3.Rep_Contact__c = c1.id;
        IncomeNavcurrentyearminus3.Deposit_Date__c= d2;
        IncomeNavcurrentyearminus3.Fund__c='3771';
        IncomeNavcurrentyearminus3.Current_Capital__c=15000;
        reitnewlist.add(IncomeNavcurrentyearminus3); */
//********************jan************
        REIT_Investment__c ccptJan= new REIT_Investment__c();
        date djan = date.newInstance(Date.today().year(),1,23);
        ccptJan.Rep_Contact__c = c1.id;
        ccptJan.Deposit_Date__c= djan ;
        ccptJan.Fund__c='3700';
        ccptJan.Original_Capital__c=15000;
        reitnewlist.add(ccptJan);   
        
        REIT_Investment__c ccptIIJan= new REIT_Investment__c();
        ccptIIJan.Rep_Contact__c = c1.id;
        ccptIIJan.Deposit_Date__c= djan;
        ccptIIJan.Fund__c='3701';
        ccptIIJan.Current_Capital__c=15000;
         reitnewlist.add(ccptIIJan);   
       
        REIT_Investment__c ccptIIIJan= new REIT_Investment__c();
        ccptIIIJan.Rep_Contact__c = c1.id;
        ccptIIIJan.Deposit_Date__c= djan;
        ccptIIIJan.Fund__c='3767';
        ccptIIIJan.Current_Capital__c=15000;
        
        System.assertEquals(ccptIIIJan.Rep_Contact__c,c1.id);

        reitnewlist.add(ccptIIIJan);  
       
        REIT_Investment__c ccITJan= new REIT_Investment__c();
        ccITJan.Rep_Contact__c = c1.id;
        ccITJan.Deposit_Date__c= djan;
        ccITJan.Fund__c='3770';
        ccITJan.Current_Capital__c=15000;
         reitnewlist.add(ccITJan);  
        
        REIT_Investment__c ccPTIVJan= new REIT_Investment__c();
        ccPTIVJan.Rep_Contact__c = c1.id;
        ccPTIVJan.Deposit_Date__c= djan;
        ccPTIVJan.Fund__c='3772';
        ccPTIVJan.Current_Capital__c=15000;
        reitnewlist.add(ccPTIVJan); 
       
        /*REIT_Investment__c IncomeNavJan= new REIT_Investment__c();
        IncomeNavJan.Rep_Contact__c = c1.id;
        IncomeNavJan.Deposit_Date__c= djan;
        IncomeNavJan.Fund__c='3771';
        IncomeNavJan.Current_Capital__c=15000;
        reitnewlist.add(IncomeNavJan); */
//********************feb month****************

        REIT_Investment__c ccptFeb= new REIT_Investment__c();
        date dFeb = date.newInstance(Date.today().year(),2,23);
        ccptFeb.Rep_Contact__c = c1.id;
        ccptFeb.Deposit_Date__c= dFeb ;
        ccptFeb.Fund__c='3700';
        ccptFeb.Original_Capital__c=15000;
        //insert ccptFeb;  
        reitnewlist.add(ccptFeb);
        
        REIT_Investment__c ccptIIFeb= new REIT_Investment__c();
        ccptIIFeb.Rep_Contact__c = c1.id;
        ccptIIFeb.Deposit_Date__c= dFeb ;
        ccptIIFeb.Fund__c='3701';
        ccptIIFeb.Current_Capital__c=15000;
        //insert ccptIIFeb;  
       reitnewlist.add(ccptIIFeb);
      // insert reitnewlist;
        REIT_Investment__c ccptIIIFeb= new REIT_Investment__c();
        ccptIIIFeb.Rep_Contact__c = c1.id;
        ccptIIIFeb.Deposit_Date__c= dFeb ;
        ccptIIIFeb.Fund__c='3767';
        ccptIIIFeb.Current_Capital__c=15000;
        //insert ccptIIIFeb; 
        reitnewlist.add(ccptIIIFeb);
        REIT_Investment__c ccITFeb= new REIT_Investment__c();
        ccITFeb.Rep_Contact__c = c1.id;
        ccITFeb.Deposit_Date__c= dFeb ;
        ccITFeb.Fund__c='3770';
        ccITFeb.Current_Capital__c=15000;
        //insert ccITFeb; 
        reitnewlist.add(ccITFeb);
        REIT_Investment__c ccPTIVFeb= new REIT_Investment__c();
        ccPTIVFeb.Rep_Contact__c = c1.id;
        ccPTIVFeb.Deposit_Date__c= dFeb ;
        ccPTIVFeb.Fund__c='3772';
        ccPTIVFeb.Current_Capital__c=15000;
        //insert ccPTIVFeb;
       reitnewlist.add(ccPTIVFeb);
       
        /*REIT_Investment__c IncomeNavFeb= new REIT_Investment__c();
        IncomeNavFeb.Rep_Contact__c = c1.id;
        IncomeNavFeb.Deposit_Date__c= dFeb ;
        IncomeNavFeb.Fund__c='3771';
        IncomeNavFeb.Current_Capital__c=15000;*/
       // insert IncomeNavFeb;
       //reitnewlist.add(IncomeNavFeb);
       //insert reitnewlist;
//***********************march month
        REIT_Investment__c ccptMarch= new REIT_Investment__c();
        date dMarch = date.newInstance(Date.today().year(),3,23);
        ccptMarch.Rep_Contact__c = c1.id;
        ccptMarch.Deposit_Date__c= dMarch ;
        ccptMarch.Fund__c='3700';
        ccptMarch.Original_Capital__c=15000;
        //insert ccptMarch;  
        reitnewlist.add(ccptMarch);
        REIT_Investment__c ccptIIMarch= new REIT_Investment__c();
        ccptIIMarch.Rep_Contact__c = c1.id;
        ccptIIMarch.Deposit_Date__c= dMarch ;
        ccptIIMarch.Fund__c='3701';
        ccptIIMarch.Current_Capital__c=15000;
        //insert ccptIIMarch;  
       reitnewlist.add(ccptIIMarch);
        REIT_Investment__c ccptIIIMarch= new REIT_Investment__c();
        ccptIIIMarch.Rep_Contact__c = c1.id;
        ccptIIIMarch.Deposit_Date__c= dMarch ;
        ccptIIIMarch.Fund__c='3767';
        ccptIIIMarch.Current_Capital__c=15000;
        //insert ccptIIIMarch; 
       reitnewlist.add(ccptIIIMarch);
      
        REIT_Investment__c ccITMarch= new REIT_Investment__c();
        ccITMarch.Rep_Contact__c = c1.id;
        ccITMarch.Deposit_Date__c= dMarch ;
        ccITMarch.Fund__c='3770';
        ccITMarch.Current_Capital__c=15000;
        //insert ccITMarch; 
        reitnewlist.add(ccITMarch);

        REIT_Investment__c ccPTIVMarch= new REIT_Investment__c();
        ccPTIVMarch.Rep_Contact__c = c1.id;
        ccPTIVMarch.Deposit_Date__c= dMarch ;
        ccPTIVMarch.Fund__c='3772';
        ccPTIVMarch.Current_Capital__c=15000;
        //insert ccPTIVMarch;
       reitnewlist.add(ccPTIVMarch);
       
        /*REIT_Investment__c IncomeNavMarch= new REIT_Investment__c();
        IncomeNavMarch.Rep_Contact__c = c1.id;
        IncomeNavMarch.Deposit_Date__c= dMarch ;
        IncomeNavMarch.Fund__c='3771';
        IncomeNavMarch.Current_Capital__c=15000;*/
        //insert IncomeNavMarch;
        //reitnewlist.add(IncomeNavMarch);
        //insert  reitnewlist;       
//****************april******************
        REIT_Investment__c ccptApril= new REIT_Investment__c();
        date dApril = date.newInstance(Date.today().year(),4,23);
        ccptApril.Rep_Contact__c = c1.id;
        ccptApril.Deposit_Date__c= dApril ;
        ccptApril.Fund__c='3700';
        ccptApril.Original_Capital__c=15000;
        //insert ccptApril;  
        reitnewlist.add(ccptApril);
        
        REIT_Investment__c ccptIIApril= new REIT_Investment__c();
        ccptIIApril.Rep_Contact__c = c1.id;
        ccptIIApril.Deposit_Date__c= dApril ;
        ccptIIApril.Fund__c='3701';
        ccptIIApril.Current_Capital__c=15000;
        //insert ccptIIApril;  
       reitnewlist.add(ccptIIApril);
        
        REIT_Investment__c ccptIIIApril= new REIT_Investment__c();
        ccptIIIApril.Rep_Contact__c = c1.id;
        ccptIIIApril.Deposit_Date__c= dApril ;
        ccptIIIApril.Fund__c='3767';
        ccptIIIApril.Current_Capital__c=15000;
        //insert ccptIIIApril; 
       reitnewlist.add(ccptIIIApril);
       
        REIT_Investment__c ccITApril= new REIT_Investment__c();
        ccITApril.Rep_Contact__c = c1.id;
        ccITApril.Deposit_Date__c= dApril ;
        ccITApril.Fund__c='3770';
        ccITApril.Current_Capital__c=15000;
        //insert ccITApril; 
        reitnewlist.add(ccITApril);
        REIT_Investment__c ccPTIVApril= new REIT_Investment__c();
        ccPTIVApril.Rep_Contact__c = c1.id;
        ccPTIVApril.Deposit_Date__c= dApril ;
        ccPTIVApril.Fund__c='3772';
        ccPTIVApril.Current_Capital__c=15000;
       // insert ccPTIVApril;
       reitnewlist.add(ccPTIVApril);
        /*REIT_Investment__c IncomeNavApril= new REIT_Investment__c();
        IncomeNavApril.Rep_Contact__c = c1.id;
        IncomeNavApril.Deposit_Date__c= dApril ;
        IncomeNavApril.Fund__c='3771';
        IncomeNavApril.Current_Capital__c=15000;*/
        //insert IncomeNavApril;
        //reitnewlist.add(IncomeNavApril);
//*************may month*************
        REIT_Investment__c ccptMay= new REIT_Investment__c();
        date dMay = date.newInstance(Date.today().year(),5,23);
        ccptMay.Rep_Contact__c = c1.id;
        ccptMay.Deposit_Date__c= dApril ;
        ccptMay.Fund__c='3700';
        ccptMay.Original_Capital__c=15000;
        //insert ccptMay;  
        reitnewlist.add(ccptMay);
        REIT_Investment__c ccptIIMay= new REIT_Investment__c();
        ccptIIMay.Rep_Contact__c = c1.id;
        ccptIIMay.Deposit_Date__c= dMay ;
        ccptIIMay.Fund__c='3701';
        ccptIIMay.Current_Capital__c=15000;
        //insert ccptIIMay;  
       reitnewlist.add(ccptIIMay);
        REIT_Investment__c ccptIIIMay= new REIT_Investment__c();
        ccptIIIMay.Rep_Contact__c = c1.id;
        ccptIIIMay.Deposit_Date__c= dMay ;
        ccptIIIMay.Fund__c='3767';
        ccptIIIMay.Current_Capital__c=15000;
        //insert ccptIIIMay; 
       reitnewlist.add(ccptIIIMay);
        REIT_Investment__c ccITMay= new REIT_Investment__c();
        ccITMay.Rep_Contact__c = c1.id;
        ccITMay.Deposit_Date__c= dMay ;
        ccITMay.Fund__c='3770';
        ccITMay.Current_Capital__c=15000;
        //insert ccITMay; 
         reitnewlist.add(ccITMay);
        
        REIT_Investment__c ccPTIVMay= new REIT_Investment__c();
        ccPTIVMay.Rep_Contact__c = c1.id;
        ccPTIVMay.Deposit_Date__c= dMay ;
        ccPTIVMay.Fund__c='3772';
        ccPTIVMay.Current_Capital__c=15000;
        //insert ccPTIVMay;
        reitnewlist.add(ccPTIVMay);
      
      /* REIT_Investment__c IncomeNavMAy= new REIT_Investment__c();
        IncomeNavMAy.Rep_Contact__c = c1.id;
        IncomeNavMAy.Deposit_Date__c= dMay ;
        IncomeNavMAy.Fund__c='3771';
        IncomeNavMAy.Current_Capital__c=15000;*/
        //insert IncomeNavMAy;
        //reitnewlist.add(IncomeNavMAy);
 //******************June***************   
         REIT_Investment__c ccptJune= new REIT_Investment__c();
        date dJune = date.newInstance(Date.today().year(),6,23);
        ccptJune.Rep_Contact__c = c1.id;
        ccptJune.Deposit_Date__c= dJune ;
        ccptJune.Fund__c='3700';
        ccptJune.Original_Capital__c=15000;
        //insert ccptMay;  
        reitnewlist.add(ccptJune);
       
        REIT_Investment__c ccptIIjune= new REIT_Investment__c();
        ccptIIjune.Rep_Contact__c = c1.id;
        ccptIIjune.Deposit_Date__c= dJune ;
        ccptIIjune.Fund__c='3701';
        ccptIIjune.Current_Capital__c=15000;
        //insert ccptIIMay;  
       reitnewlist.add(ccptIIjune);
       
        REIT_Investment__c ccptIIIJune= new REIT_Investment__c();
        ccptIIIJune.Rep_Contact__c = c1.id;
        ccptIIIJune.Deposit_Date__c= dJune ;
        ccptIIIJune.Fund__c='3767';
        ccptIIIJune.Current_Capital__c=15000;
        //insert ccptIIIMay; 
       reitnewlist.add(ccptIIIJune);
        
        REIT_Investment__c ccITJune= new REIT_Investment__c();
        ccITJune.Rep_Contact__c = c1.id;
        ccITJune.Deposit_Date__c= dJune ;
        ccITJune.Fund__c='3770';
        ccITJune.Current_Capital__c=15000;
        //insert ccITMay; 
         reitnewlist.add(ccITJune);
        
        REIT_Investment__c ccPTIVJune= new REIT_Investment__c();
        ccPTIVJune.Rep_Contact__c = c1.id;
        ccPTIVJune.Deposit_Date__c= dJune ;
        ccPTIVJune.Fund__c='3772';
        ccPTIVJune.Current_Capital__c=15000;
        //insert ccPTIVMay;
        reitnewlist.add(ccPTIVJune);
      
      /* REIT_Investment__c IncomeNavJune= new REIT_Investment__c();
        IncomeNavJune.Rep_Contact__c = c1.id;
        IncomeNavJune.Deposit_Date__c= dJune ;
        IncomeNavJune.Fund__c='3771';
        IncomeNavJune.Current_Capital__c=15000;
        //insert IncomeNavMAy;
        reitnewlist.add(IncomeNavJune);*/
    //******************july******************
         REIT_Investment__c ccptJuly= new REIT_Investment__c();
        date dJuly = date.newInstance(Date.today().year(),7,23);
        ccptJuly.Rep_Contact__c = c1.id;
        ccptJuly.Deposit_Date__c= dJuly ;
        ccptJuly.Fund__c='3700';
        ccptJuly.Original_Capital__c=15000;
        //insert ccptMay;  
        reitnewlist.add(ccptJuly);
       
        REIT_Investment__c ccptIIjuly= new REIT_Investment__c();
        ccptIIjuly.Rep_Contact__c = c1.id;
        ccptIIjuly.Deposit_Date__c= dJuly ;
        ccptIIjuly.Fund__c='3701';
        ccptIIjuly.Current_Capital__c=15000;
        //insert ccptIIMay;  
       reitnewlist.add(ccptIIjuly);
       
        REIT_Investment__c ccptIIIJuly= new REIT_Investment__c();
        ccptIIIJuly.Rep_Contact__c = c1.id;
        ccptIIIJuly.Deposit_Date__c= dJuly ;
        ccptIIIJuly.Fund__c='3767';
        ccptIIIJuly.Current_Capital__c=15000;
        //insert ccptIIIMay; 
       reitnewlist.add(ccptIIIJuly);
        
        REIT_Investment__c ccITJuly= new REIT_Investment__c();
        ccITJuly.Rep_Contact__c = c1.id;
        ccITJuly.Deposit_Date__c= dJuly ;
        ccITJuly.Fund__c='3770';
        ccITJuly.Current_Capital__c=15000;
        //insert ccITMay; 
         reitnewlist.add(ccITJuly);
        
        REIT_Investment__c ccPTIVJuly= new REIT_Investment__c();
        ccPTIVJuly.Rep_Contact__c = c1.id;
        ccPTIVJuly.Deposit_Date__c= dJuly ;
        ccPTIVJuly.Fund__c='3772';
        ccPTIVJuly.Current_Capital__c=15000;
        //insert ccPTIVMay;
        reitnewlist.add(ccPTIVJuly);
      
       /*REIT_Investment__c IncomeNavJuly= new REIT_Investment__c();
        IncomeNavJuly.Rep_Contact__c = c1.id;
        IncomeNavJuly.Deposit_Date__c= dJuly ;
        IncomeNavJuly.Fund__c='3771';
        IncomeNavJuly.Current_Capital__c=15000;
        //insert IncomeNavMAy;
        reitnewlist.add(IncomeNavJuly);*/
  //*******************August************************  
        REIT_Investment__c ccptAugust= new REIT_Investment__c();
        date dAugust = date.newInstance(Date.today().year(),8,23);
        ccptAugust.Rep_Contact__c = c1.id;
        ccptAugust.Deposit_Date__c= dAugust ;
        ccptAugust.Fund__c='3700';
        ccptAugust.Original_Capital__c=15000;
        //insert ccptMay;  
        reitnewlist.add(ccptAugust);
       
        REIT_Investment__c ccptIIAugust= new REIT_Investment__c();
        ccptIIAugust.Rep_Contact__c = c1.id;
        ccptIIAugust.Deposit_Date__c= dAugust ;
        ccptIIAugust.Fund__c='3701';
        ccptIIAugust.Current_Capital__c=15000;
        //insert ccptIIMay;  
       reitnewlist.add(ccptIIAugust);
       
        REIT_Investment__c ccptIIIAugust= new REIT_Investment__c();
        ccptIIIAugust.Rep_Contact__c = c1.id;
        ccptIIIAugust.Deposit_Date__c= dAugust ;
        ccptIIIAugust.Fund__c='3767';
        ccptIIIAugust.Current_Capital__c=15000;
        //insert ccptIIIMay; 
       reitnewlist.add(ccptIIIAugust);
        
        REIT_Investment__c ccITAugust= new REIT_Investment__c();
        ccITAugust.Rep_Contact__c = c1.id;
        ccITAugust.Deposit_Date__c= dAugust ;
        ccITAugust.Fund__c='3770';
        ccITAugust.Current_Capital__c=15000;
        //insert ccITMay; 
         reitnewlist.add(ccITAugust);
        
        REIT_Investment__c ccPTIVAugust= new REIT_Investment__c();
        ccPTIVAugust.Rep_Contact__c = c1.id;
        ccPTIVAugust.Deposit_Date__c= dAugust ;
        ccPTIVAugust.Fund__c='3772';
        ccPTIVAugust.Current_Capital__c=15000;
        //insert ccPTIVMay;
        reitnewlist.add(ccPTIVAugust);
      
       /*REIT_Investment__c IncomeNavAugust= new REIT_Investment__c();
        IncomeNavAugust.Rep_Contact__c = c1.id;
        IncomeNavAugust.Deposit_Date__c= dAugust ;
        IncomeNavAugust.Fund__c='3771';
        IncomeNavAugust.Current_Capital__c=15000;
        //insert IncomeNavMAy;
        reitnewlist.add(IncomeNavAugust);*/
   //**************sept****************** 
        REIT_Investment__c ccptSept= new REIT_Investment__c();
        date dSept = date.newInstance(Date.today().year(),9,23);
        ccptSept.Rep_Contact__c = c1.id;
        ccptSept.Deposit_Date__c= dSept ;
        ccptSept.Fund__c='3700';
        ccptSept.Original_Capital__c=15000;
        //insert ccptMay;  
        reitnewlist.add(ccptSept);
       
        REIT_Investment__c ccptIISept= new REIT_Investment__c();
        ccptIISept.Rep_Contact__c = c1.id;
        ccptIISept.Deposit_Date__c= dSept ;
        ccptIISept.Fund__c='3701';
        ccptIISept.Current_Capital__c=15000;
        //insert ccptIIMay;  
       reitnewlist.add(ccptIISept);
       
        REIT_Investment__c ccptIIISept= new REIT_Investment__c();
        ccptIIISept.Rep_Contact__c = c1.id;
        ccptIIISept.Deposit_Date__c= dSept ;
        ccptIIISept.Fund__c='3767';
        ccptIIISept.Current_Capital__c=15000;
        //insert ccptIIIMay; 
       reitnewlist.add(ccptIIISept);
        
        REIT_Investment__c ccITSept= new REIT_Investment__c();
        ccITSept.Rep_Contact__c = c1.id;
        ccITSept.Deposit_Date__c= dSept ;
        ccITSept.Fund__c='3770';
        ccITSept.Current_Capital__c=15000;
        //insert ccITMay; 
         reitnewlist.add(ccITSept);
        
        REIT_Investment__c ccPTIVSept= new REIT_Investment__c();
        ccPTIVSept.Rep_Contact__c = c1.id;
        ccPTIVSept.Deposit_Date__c= dSept ;
        ccPTIVSept.Fund__c='3772';
        ccPTIVSept.Current_Capital__c=15000;
        //insert ccPTIVMay;
        reitnewlist.add(ccPTIVSept);
      
       /*REIT_Investment__c IncomeNavSept= new REIT_Investment__c();
        IncomeNavSept.Rep_Contact__c = c1.id;
        IncomeNavSept.Deposit_Date__c= dSept ;
        IncomeNavSept.Fund__c='3771';
        IncomeNavSept.Current_Capital__c=15000;
        //insert IncomeNavMAy;
        reitnewlist.add(IncomeNavSept);*/
    //****************************Oct month************
       REIT_Investment__c ccptOct= new REIT_Investment__c();
        date dOct = date.newInstance(Date.today().year(),10,23);
        ccptOct.Rep_Contact__c = c1.id;
        ccptOct.Deposit_Date__c= dOct;
        ccptOct.Fund__c='3700';
        ccptOct.Original_Capital__c=15000;
        //insert ccptMay;  
        reitnewlist.add(ccptOct);
       
        REIT_Investment__c ccptIIOct= new REIT_Investment__c();
        ccptIIOct.Rep_Contact__c = c1.id;
        ccptIIOct.Deposit_Date__c= dOct;
        ccptIIOct.Fund__c='3701';
        ccptIIOct.Current_Capital__c=15000;
        //insert ccptIIMay;  
       reitnewlist.add(ccptIIOct);
       
        REIT_Investment__c ccptIIIOct= new REIT_Investment__c();
        ccptIIIOct.Rep_Contact__c = c1.id;
        ccptIIIOct.Deposit_Date__c= dOct;
        ccptIIIOct.Fund__c='3767';
        ccptIIIOct.Current_Capital__c=15000;
        //insert ccptIIIMay; 
       reitnewlist.add(ccptIIIOct);
        
        REIT_Investment__c ccITOct= new REIT_Investment__c();
        ccITOct.Rep_Contact__c = c1.id;
        ccITOct.Deposit_Date__c= dOct;
        ccITOct.Fund__c='3770';
        ccITOct.Current_Capital__c=15000;
        //insert ccITMay; 
         reitnewlist.add(ccITOct);
        
        REIT_Investment__c ccPTIVOct= new REIT_Investment__c();
        ccPTIVOct.Rep_Contact__c = c1.id;
        ccPTIVOct.Deposit_Date__c= dOct;
        ccPTIVOct.Fund__c='3772';
        ccPTIVOct.Current_Capital__c=15000;
        //insert ccPTIVMay;
        reitnewlist.add(ccPTIVOct);
      
        /*REIT_Investment__c IncomeNavOct= new REIT_Investment__c();
        IncomeNavOct.Rep_Contact__c = c1.id;
        IncomeNavOct.Deposit_Date__c= dOct;
        IncomeNavOct.Fund__c='3771';
        IncomeNavOct.Current_Capital__c=15000;
        //insert IncomeNavMAy;
        reitnewlist.add(IncomeNavOct);*/
    
    //******************november******************
       REIT_Investment__c ccptNov= new REIT_Investment__c();
        date dNov = date.newInstance(Date.today().year(),11,23);
        ccptNov.Rep_Contact__c = c1.id;
        ccptNov.Deposit_Date__c= dNov ;
        ccptNov.Fund__c='3700';
        ccptNov.Original_Capital__c=15000;
        //insert ccptMay;  
        reitnewlist.add(ccptNov);
       
        REIT_Investment__c ccptIINov= new REIT_Investment__c();
        ccptIINov.Rep_Contact__c = c1.id;
        ccptIINov.Deposit_Date__c= dNov ;
        ccptIINov.Fund__c='3701';
        ccptIINov.Current_Capital__c=15000;
        //insert ccptIIMay;  
       reitnewlist.add(ccptIINov);
       
        REIT_Investment__c ccptIIINOv= new REIT_Investment__c();
        ccptIIINOv.Rep_Contact__c = c1.id;
        ccptIIINOv.Deposit_Date__c= dNov;
        ccptIIINOv.Fund__c='3767';
        ccptIIINOv.Current_Capital__c=15000;
        //insert ccptIIIMay; 
       reitnewlist.add(ccptIIINOv);
        
        REIT_Investment__c ccITNov= new REIT_Investment__c();
        ccITNov.Rep_Contact__c = c1.id;
        ccITNov.Deposit_Date__c= dNov;
        ccITNov.Fund__c='3770';
        ccITNov.Current_Capital__c=15000;
        //insert ccITMay; 
         reitnewlist.add(ccITNov);
        
        REIT_Investment__c ccPTIVNov= new REIT_Investment__c();
        ccPTIVNov.Rep_Contact__c = c1.id;
        ccPTIVNov.Deposit_Date__c= dNov;
        ccPTIVNov.Fund__c='3772';
        ccPTIVNov.Current_Capital__c=15000;
        //insert ccPTIVMay;
        reitnewlist.add(ccPTIVNov);
      
        /*REIT_Investment__c IncomeNavNOv= new REIT_Investment__c();
        IncomeNavNOv.Rep_Contact__c = c1.id;
        IncomeNavNOv.Deposit_Date__c= dNov;
        IncomeNavNOv.Fund__c='3771';
        IncomeNavNOv.Current_Capital__c=15000;
        //insert IncomeNavMAy;
        reitnewlist.add(IncomeNavNOv);*/
    //************************december**************
       REIT_Investment__c ccptDec= new REIT_Investment__c();
        date dDec = date.newInstance(Date.today().year(),12,23);
        ccptDec.Rep_Contact__c = c1.id;
        ccptDec.Deposit_Date__c= dDec ;
        ccptDec.Fund__c='3700';
        ccptDec.Original_Capital__c=15000;
        //insert ccptMay;  
        reitnewlist.add(ccptDec);
       
        REIT_Investment__c ccptIIDec= new REIT_Investment__c();
        ccptIIDec.Rep_Contact__c = c1.id;
        ccptIIDec.Deposit_Date__c= dDec ;
        ccptIIDec.Fund__c='3701';
        ccptIIDec.Current_Capital__c=15000;
        //insert ccptIIMay;  
       reitnewlist.add(ccptIIDec);
       
        REIT_Investment__c ccptIIIDec= new REIT_Investment__c();
        ccptIIIDec.Rep_Contact__c = c1.id;
        ccptIIIDec.Deposit_Date__c= dDec;
        ccptIIIDec.Fund__c='3767';
        ccptIIIDec.Current_Capital__c=15000;
        //insert ccptIIIMay; 
       reitnewlist.add(ccptIIIDec);
        
        REIT_Investment__c ccITDec= new REIT_Investment__c();
        ccITDec.Rep_Contact__c = c1.id;
        ccITDec.Deposit_Date__c= dDec;
        ccITDec.Fund__c='3770';
        ccITDec.Current_Capital__c=15000;
        //insert ccITMay; 
         reitnewlist.add(ccITDec);
        
        REIT_Investment__c ccPTIVDec= new REIT_Investment__c();
        ccPTIVDec.Rep_Contact__c = c1.id;
        ccPTIVDec.Deposit_Date__c= dDec;
        ccPTIVDec.Fund__c='3772';
        ccPTIVDec.Current_Capital__c=15000;
        //insert ccPTIVMay;
        reitnewlist.add(ccPTIVDec);
      
        /*REIT_Investment__c IncomeNavDec= new REIT_Investment__c();
        IncomeNavDec.Rep_Contact__c = c1.id;
        IncomeNavDec.Deposit_Date__c= dDec;
        IncomeNavDec.Fund__c='3771';
        IncomeNavDec.Current_Capital__c=15000;
        //insert IncomeNavMAy;
        reitnewlist.add(IncomeNavDec);*/
      insert reitnewlist;
//ApexPages.currentPage().getParameters().put('id',a.id);
//ApexPages.StandardController controller = new ApexPages.StandardController(a); 
     Test.StartTest();
         grosssalesbyProduct batch1  = new grosssalesbyProduct();
         Id batchId1 = Database.executeBatch(batch1,200);
         
   }

}