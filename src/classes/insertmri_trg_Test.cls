@isTest(SeeAllData = true)
private class insertmri_trg_Test{

static testMethod void testDoGet() {
        MRI_PROPERTY__c  mo = new MRI_PROPERTY__c () ;
        mo.Address__c ='2450 Atlanta Hwy Ste 703' ;
        mo.City__c='Atlanta';
        mo.Zip_code__c='30040';
        mo.State__c='GA';
        mo.Country__c= 'USA';  
        mo.Name ='Name' ;
        mo.AvgLeaseTerm__c  = 12 ;
        mo.Long_Description__c  = 'test' ; 
        mo.Web_Ready__c='Approved';
        mo.status__c='Owned';
        mo.Program_Id__c='ARCP';
        mo.Property_ID__c='PL1234789'; 
        insert mo ; 
        system.debug('mo.id'+mo.id);
        
  
        MRI_PROPERTY__c  mo1 = new MRI_PROPERTY__c () ;
        mo1.Address__c ='test' ;
        mo1.Name ='Name' ;
        mo1.AvgLeaseTerm__c  = 12 ;
        mo1.Long_Description__c  = 'test' ; 
        mo1.Web_Ready__c='Pending Approval';
        mo1.status__c='Owned';
        mo1.Program_Id__c='ARCP';
        mo1.Property_ID__c='PL3A445123'; 
        mo1.Address__c ='2450 Atlanta Hwy Ste 703' ;
        mo1.City__c='Atlanta';
        mo1.Zip_code__c='30040';
        mo1.State__c='GA';
        mo1.Country__c= 'USA';
        insert mo1 ; 
        system.debug('mo1.id'+mo1.id);
        
        System.assertEquals(mo.Name,mo1.Name);

        mo1.Web_Ready__c ='Pending Deleted';
        update mo1;
        mo1.Web_Ready__c ='Deleted';
        update mo1;   
        system.debug('mo1.id'+mo1.Web_Ready__c);
            
        List<MRI_PROPERTY__c> mri = new List<MRI_PROPERTY__c>();       
        MRI_PROPERTY__c  mo2 = new MRI_PROPERTY__c () ;
        mo2.Address__c ='test' ;
        mo2.Name ='Name' ;
        mo2.AvgLeaseTerm__c  = 12 ;
        mo2.Long_Description__c  = 'test' ; 
        mo2.Web_Ready__c='Pending Deleted';
        mo2.status__c='Owned';
        mo2.Property_ID__c='PL3A555123'; 
        mo2.Address__c ='2450 Atlanta Hwy Ste 703' ;
        mo2.City__c='Atlanta';
        mo2.Zip_code__c='30040';
        mo2.State__c='GA';
        mo2.Program_Id__c='ARCP';
        mo2.Country__c= 'USA';
        insert mo2 ; 
          
        mo2.Web_Ready__c ='Pending Approval';
        update mo2;
        mo2.Web_Ready__c='Approved';  
        update mo2;        
            
        Web_Properties__c web = new Web_Properties__c();
        web.Address__c =mo.Address__c;
        web.City__c=mo.City__c;
        web.Country__c=mo.Country__c;
        web.State__c=mo.State__c;
        web.Zip_Code__c=mo.Zip_code__c;
        web.Name =mo.Name;
        web.AvgLeaseTerm__c  = mo.AvgLeaseTerm__c ;
        web.Long_Description__c  = mo.Long_Description__c ; 
        web.Web_Ready__c=mo.Web_Ready__c;
        web.status__c=mo.status__c;
        web.Property_ID__c=mo.Property_ID__c;
        web.Override_Latitude__c=40.92639550000000;
        web.Override_Longitude__c=-87.74086530000000;
        web.Geocode__c='a1E500000014r3X';
        upsert web Property_ID__c; 
        system.debug('web.Web_Ready__c'+web.Web_Ready__c);
        
        
        Lease__c lease= new Lease__c();
        lease.Name='PL3A441'; 
        lease.MRI_PROPERTY__c=mo.id;
        lease.Lease_ID__c='PL3A441_1234_A';
        lease.MRI_PM__c='00550000002tACW';
        lease.Lease_Status__c='Active';
        lease.SuitVacancyIndicator__c= FALSE;
        lease.Industry__c='Multi Tenant';
        lease.LeaseType__c='Rent';
        lease.Moodys__c='test';
        lease.NYSE__c='test';
        lease.S_P__c='test';
        lease.SuiteId__c='A123';
        lease.Tenant_ID__c='A1234';
        lease.Tenant_Name__c='Macys';
        lease.web_Ready__c='Approved';
        insert lease;  
        Leases__c weblease= new Leases__c();
        weblease.MRI_PROPERTIES__c  = web.id; 
        weblease.name=lease.Name;
        weblease.Lease_ID__c=lease.Lease_ID__c; 
        weblease.SuiteId__c=lease.SuiteId__c;
        weblease.Tenant_ID__c= lease.Tenant_ID__c;
        upsert weblease Lease_ID__c;
       
}  


}