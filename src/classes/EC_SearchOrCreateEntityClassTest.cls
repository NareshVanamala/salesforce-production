@isTest
Public class EC_SearchOrCreateEntityClassTest
{
    static testmethod void testunit1()
    {
        test.starttest();
                  
        Portfolio__c port = new Portfolio__c();
        port.Name ='Test Port';
        insert port;
        
        Deal__C deal= new Deal__c();
        deal.Name ='New Deal';
        deal.Property_Type__c ='Industrial';
        deal.Tenancy__c ='Single-Tenant';
        deal.Primary_Use__c ='Industrial';
        deal.Owned_Id__c ='123456';
        deal.Fund__c ='TBD';
        deal.Legal_Department__c ='Pheonix';
        deal.Report_Deal_Name__c='New Deal';
        deal.Address__c='Test Address';
        deal.city__c='Test City';
        deal.State__c='teststate';
        deal.Zip_Code__c='98033';
        deal.Portfolio_Deal__c =port.Id;
        deal.Build_to_Suit_Type__c ='Current';
        deal.Estimated_COE_Date__c = date.today();
        deal.HVAC_Warranty_Status__c ='Received';
        deal.Ownership_Interest__c = 'Fee Simple';
        deal.Roof_Warranty_Status__c = 'Received';
        deal.Create_Workspace__c =true; 
        insert deal;
        
        System.assertNotEquals(port.Name,deal.Name);

        deal=[select id,name from Deal__c where id =:deal.id];  
        
        Entity__c ent = new Entity__c();
        ent.Name = 'Test Entity';
        ent.Entity_Type__c = 'Corp';
        insert ent;
        
        deal.Property_Owner_SPEL__c= ent.Id;
        update deal;
        deal=[select id,name from Deal__c where id =:deal.id];       

        ApexPages.currentPage().getParameters().put('id',deal.id);
        
        EC_SearchOrCreateEntityClass sEnt= new EC_SearchOrCreateEntityClass ();
        sEnt.searchString='Test Entity';
        sEnt.Search();
        sEnt.Save();
        sEnt.CancelEntity();
        sEnt.dealUpd();
        sEnt.getFormTag();
        sEnt.getTextBox();
        
        test.stoptest();
    } 
     static testmethod void testunit2()
    {
        test.starttest();
        
        Portfolio__c port = new Portfolio__c();
        port.Name ='Test Port';
        insert port;
        
        Deal__C deal= new Deal__c();
        deal.Name ='New Deal';
        deal.Property_Type__c ='Industrial';
        deal.Tenancy__c ='Single-Tenant';
        deal.Primary_Use__c ='Industrial';
        deal.Owned_Id__c ='123456';
        deal.Fund__c ='TBD';
        deal.Legal_Department__c ='Pheonix';
        deal.Report_Deal_Name__c='New Deal';
        deal.Address__c='Test Address';
        deal.city__c='Test City';
        deal.State__c='teststate';
        deal.Zip_Code__c='98033';
        deal.Portfolio_Deal__c =port.Id;
        deal.Build_to_Suit_Type__c ='Current';
        deal.Estimated_COE_Date__c = date.today();
        deal.HVAC_Warranty_Status__c ='Received';
        deal.Ownership_Interest__c = 'Fee Simple';
        deal.Roof_Warranty_Status__c = 'Received';
        deal.Create_Workspace__c =true; 
        insert deal;
        System.assertNotEquals(port.Name,deal.Name);

        
        deal=[select id,name from Deal__c where id =:deal.id];  
        
        Deal__C deal1= new Deal__c();
        deal1.Name ='New Deal';
        deal1.Property_Type__c ='Industrial';
        deal1.Tenancy__c ='Single-Tenant';
        deal1.Primary_Use__c ='Industrial';
        deal1.Owned_Id__c ='123456';
        deal1.Fund__c ='TBD';
        deal1.Legal_Department__c ='Charlotte';
        deal1.Report_Deal_Name__c='New Deal';
        deal1.Address__c='Test Address';
        deal1.city__c='Test City';
        deal1.State__c='teststate';
        deal1.Zip_Code__c='98033';
        deal1.Portfolio_Deal__c =port.Id;
        deal1.Build_to_Suit_Type__c ='Current';
        deal1.Estimated_COE_Date__c = date.today();
        deal1.HVAC_Warranty_Status__c ='Received';
        deal1.Ownership_Interest__c = 'Fee Simple';
        deal1.Roof_Warranty_Status__c = 'Received';
        deal1.Create_Workspace__c =true; 
        insert deal1;     
        
        Entity__c ent = new Entity__c();
        ent.Name = 'Test Entity1';
        ent.Entity_Type__c = 'Corp';
        insert ent;
        
        deal.Property_Owner_SPEL__c = ent.Id;
       deal1.Property_Owner_SPEL__c= ent.Id;
        update deal;
        update deal1;
        deal=[select id,name from Deal__c where id =:deal.id];       
        
        ApexPages.currentPage().getParameters().put('id',deal.id);
        
        EC_SearchOrCreateEntityClass sEnt= new EC_SearchOrCreateEntityClass();
        sEnt.searchString='Test Entity';
        sEnt.Search();
        sEnt.Save();
        sEnt.CancelEntity();
        sEnt.dealUpd();
        sEnt.getFormTag();
        sEnt.getTextBox();
        
        test.stoptest();
    } 
    
 
    
}