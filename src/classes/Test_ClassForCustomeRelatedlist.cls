@isTest
public class Test_ClassForCustomeRelatedlist {
static testMethod void ClassForCustomeRelatedlist ()
{
    Account a = new Account();
    a.name = 'testaccount';
    a.recordtypeid='012500000004uvF';
    insert a;
    
    Id recId = a.id;     
    Id recTypeIdNational=[Select id,name,DeveloperName from RecordType where DeveloperName='National_Accounts' and SobjectType = 'contact'].id;
    List<Contact> memberList =[select name,Phone,Email,Fax,mailingcity,mailingstate, id,accountid,Last_Producer_Date__c,Total_Cole_Tickets_Only_Funds_in_SF__c,CCPT_III__c,CCPT_IV__c,CCIT__c,Income_NAV__c,AUM_Actual__c from Contact where accountid=:recId and recordtypeid=:recTypeIdNational order by LastModifiedDate DESC];  
    
    Contact c = new Contact();
    c.accountid = a.id;
    c.FirstName = 'Satheesh';
    c.LastName ='Polaris';
    c.MailingStreet='1078 3rd Ave';
    c.Mailingcity='New York';
    c.MailingPostalCode='10065';
    c.MailingCountry='USA';
    c.recordtypeid=recTypeIdNational;
    //c.Total_Cole_Tickets_Only_Funds_in_SF__c = 1;
    //c.Total_Cole_Production_Funds_in_SF__c = 1000;
    //c.Last_Producer_Date__c = System.Today();
    insert c;
    
   system.assertequals(c.recordtypeid,recTypeIdNational);

    Contact c1 = new Contact();
    c1.accountid = a.id;
    c1.FirstName = 'Satheesh1';
    c1.LastName ='Polaris1';
    c1.MailingStreet='1078 3rd Ave1';
    c1.Mailingcity='New York1';
    c1.MailingPostalCode='100651';
    c1.MailingCountry='USA11';
    c1.recordtypeid=recTypeIdNational;
    //c.Total_Cole_Tickets_Only_Funds_in_SF__c = 1;
    //c.Total_Cole_Production_Funds_in_SF__c = 1000;
    //c.Last_Producer_Date__c = System.Today();
    insert c1;
      
    Contact c2 = new Contact();
    c2.accountid = a.id;
    c2.FirstName = 'Satheesh11';
    c2.LastName ='Polaris11';
    c2.MailingStreet='1078 3rd Ave11';
    c2.Mailingcity='New York11';
    c2.MailingPostalCode='1006511';
    c2.MailingCountry='USA111';
    c2.recordtypeid=recTypeIdNational;
    //c.Total_Cole_Tickets_Only_Funds_in_SF__c = 1;
    //c.Total_Cole_Production_Funds_in_SF__c = 1000;
    //c.Last_Producer_Date__c = System.Today();
    insert c2;
    
    system.assertnotequals(c1.FirstName,c2.FirstName);


 //PageReference pref = new PageReference();

ApexPages.currentPage().getParameters().put('id',a.id);

 //Test.setCurrentPage(pref);
 
 ApexPages.StandardController sc = new ApexPages.StandardController(a);

//ApexPages.StandardController controller ; 
ClassForCustomeRelatedlist x = new ClassForCustomeRelatedlist(sc);
//x.editRecord();
//x.newProgram();
//x.removecon();

}

}