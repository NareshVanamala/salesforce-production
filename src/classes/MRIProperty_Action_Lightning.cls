public with sharing class MRIProperty_Action_Lightning {
    @AuraEnabled
    public static String consolidatedProperties(){
        Approval_Records_Preparation c = new Approval_Records_Preparation(); 
        ID batchId=Database.executeBatch(c, 200);
        return String.valueof(batchId);
    }
    @AuraEnabled
    public static String pollBatch(String batchId){
        AsyncApexJob job= [select status from AsyncApexJob where id=:batchId][0];        
        return job.status;
    }
    @AuraEnabled
    public static String getAccountRestriction(String accountId){
        Account account= [select IsRestricted__c from Account where id=:accountId][0];
        return account.IsRestricted__c;
    }
        
    @AuraEnabled
    public static Project__c getProject(String id){
        return [select Count_Bids_With_RecVendor_As_True__c,Approval_Status__c, name, Contract_Agreement_Type__c from Project__c where id=:id][0];
    }    
    
    @AuraEnabled
    public static Lease_Charges_Request__c getLeaseChargesRequestRecords(String id){
        return [select Approval_Status__c from Lease_Charges_Request__c where id=:id][0];
    }
    
    // for submitting getLeaseChargesRequestRecords
    @AuraEnabled
    public static void massSubmit_Lightning(String leasechargeId){
        LeaseChargeDetailsMassSubmitForApproval.MassSubmit(leasechargeId);
    }
    
    @AuraEnabled
    public static LC_LeaseOpprtunity__c getLeaseOpportunityRecords(String id){
        return [select Milestone__c from LC_LeaseOpprtunity__c where id=:id][0];
    }
}