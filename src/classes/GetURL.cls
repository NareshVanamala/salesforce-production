global class GetURL {

    public String url {get; set;}
    public String tabid {get; set;}
    public String sname {get; set;}
	
	public String getUrl(String page_name) {
		url = System.currentPageReference().getUrl();
        tabid = System.currentPageReference().getParameters().get('tabid');
        sname = System.currentPageReference().getParameters().get('sname');
        if(page_name != null) {
            if(Site.getName() != null && Site.getName() != '') {
                if(Site.getPrefix() != null ) {
                    return Site.getPrefix()+'/cms__Main?name='+page_name+'&id=';
                } else {
                    return '/cms__Main?name='+page_name+'&id=';   
                }
            } else {
                if(url != null) {
                    if(url.indexOf('Preview') >= 0) {
                        return '/apex/cms__Preview?name='+page_name+'&sname='+EncodingUtil.urlEncode(sname, 'UTF-8')+'&tabid='+EncodingUtil.urlEncode(tabid, 'UTF-8')+'&id='; 
                    } else if(url.indexOf('Debug') >= 0){
                        return '/apex/cms__Debug?name='+page_name+'&sname='+EncodingUtil.urlEncode(sname, 'UTF-8')+'&tabid='+EncodingUtil.urlEncode(tabid, 'UTF-8')+'&id=';
                    } else if(sname != null && tabid != null) {
                        return '/apex/cms__Main?name='+page_name+'&sname='+EncodingUtil.urlEncode(sname, 'UTF-8')+'&tabid='+EncodingUtil.urlEncode(tabid, 'UTF-8')+'&id=';
                    } else if(sname != null){
                        return '/apex/cms__Main?name='+page_name+'&sname='+EncodingUtil.urlEncode(sname, 'UTF-8')+'&id=';
                    } else {
                    	return '/apex/'+page_name+'?id=';
                    }
                                           
                }   
            }       
        }
        
        return '/';
    }
    
    public String escapeDoubleQuotes(String s){

        String filtered = s;

        if(s != null) {
            filtered = s.replaceAll('\n', '');
            filtered = filtered.replaceAll('\r', '');
            filtered = filtered.replaceAll('"','\\\\"');
            filtered = filtered.trim();

        }
        return filtered;
    }
}