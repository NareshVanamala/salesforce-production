public with sharing class Compliancesuitabilityclass{

    Public List<Compliance_Suitability__c> memberList{get;set;}
    public Compliance_Suitability__c deletelist1;
    public Compliance_Suitability__c updatedlist1;
    Public List<Compliance_Suitability__c> updatemelist;
    Public List<Compliance_Suitability__c> deleteemelist;
    private integer removepos{get;set;}
    public string conid{get;set;}
    private Id recId{get;set;} 
    
    public string fileName{get;set;}     
    
    public Attachment attachment{get;set;}
    public Attachment myatt{get;set;}
    public string nameFile{get;set;}
    public transient Blob contentFile{get;set;}
    public Boolean showInputFile{get;set;}     
    private Account a;
    
    public Compliancesuitabilityclass(ApexPages.StandardController controller) {
        addrow = new Compliance_Suitability__c();
        //myAttachment = new Attachment();

        a =(Account)controller.getrecord();
        showInputFile = false;
        //a =[select id from Account where id=:ApexPages.currentPage().getParameters().get('id')];
        //a=[select id from Account where id= '001P000000adMsB'];
        memberList =[select name, id,(Select Id,Name from Attachments) Attmts,Account__c,Description__c from Compliance_Suitability__c where Account__c=:a.id order by LastModifiedDate DESC];  
        deletelist1= new Compliance_Suitability__c();  
        updatedlist1= new Compliance_Suitability__c();
        updatemelist=new list<Compliance_Suitability__c>();
        deleteemelist=new list<Compliance_Suitability__c>();
        recId= controller.getRecord().id;
        myatt=new attachment();
    }
    public Pagereference removecon(){
        if(memberList.size()>0)
        {
        
            string paramname= Apexpages.currentPage().getParameters().get('node');
            system.debug('Check the paramname'+paramname);
            for(Integer i=0;i<memberList.size();i++)
            {
                if(memberList[i].id==conid)
                {
                    removepos=i;
                    //deletelist1.id=memberList[i].id;
                    deleteemelist.add(memberList[i]);
                }
            }
            if(removepos!=null)
            memberList.remove(removepos);
            //system.debug('check the deletelist..'+deletelist1);             
            if(deleteemelist.size()>0)
           {
              if (Compliance_Suitability__c.sObjectType.getDescribe().isDeletable()) 
                {
                         delete deleteemelist;
                }
            deleteemelist.clear();
            }
        }
       return null;
   }
   
   // Edit Record method ....
   Pagereference p;
   public Id recordId {set;get;}
   public Boolean edit {set;get;}   
   public Pagereference editRecord(){
       recordId = conid;
       edit = true;
       //p = new Pagereference('/apex/AdvancedProgrampage?id='+ApexPages.currentPage().getParameters().get('id'));
       return null;
   }
   
   public Pagereference viewRecord(){
       recordId = conid;   
      myatt=[select id, name, parentid from Attachment where parentid=:recordId ];
        system.debug('check the attachement..'+myatt);
       p = new Pagereference('/'+myatt.id);
      // p.setRedirect(true);
       return p;
   }
   
   public Pagereference SaveRecord(){
      if(memberList.size()>0)
        {
             for(Integer i=0;i<memberList.size();i++)
            {
                if(memberList[i].id==conid)
                {
                  /* updatedlist1.id= memberList[i].id;
                   updatedlist1.name= memberList[i].name;
                   updatedlist1.Description__c= memberList[i].Description__c;*/
                   updatemelist.add(memberList[i]); 
                }
            }
                   if(updatemelist.size()>0)
                  {
                 update updatemelist;
                 updatemelist.clear();
                 }         
        }
       edit = false;
       return null;
       
   }
    //adding new record
    public Boolean addnew{set;get;}
    public Compliance_Suitability__c addrow {set;get;}

    public Pagereference newProgram(){
        attachment =  new Attachment();
        addrow= new Compliance_Suitability__c();
        addnew=true;        
        showInputFile = true;
        return null;    
    }
    public Pagereference savenewProg(){             
        system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>nameFile>>>>>>>>>>>>>>>>>>>>>>>>>>>'+nameFile);
        system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>contentFile>>>>>>>>>>>>>>>>>>>>>>>>>>>'+contentFile);        
        try{            
            addnew=true;
            addrow.Account__c = a.id;           
            if((nameFile==null) && (contentFile==null))
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please add attachment'));
            else if(contentFile.size()>5242880)
            {
            system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> >5MB..');
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Attachment size is not greater than 5MB'));            
            }
            else
            {   
                addnew=false;
                
                if (Schema.sObjectType.Compliance_Suitability__c.isCreateable())
                insert addrow;
                
                attachment.OwnerId = UserInfo.getUserId();
                attachment.ParentId = addrow.Id;// the record the file is attached to
                attachment.Name = nameFile;
                attachment.Body = contentFile;   
                //attachment.IsPrivate = true;
            
                system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>contentFile.Size>>>>>>>>>>>>>>>>>>>>>>>>>>> if '+contentFile.size());
                system.debug('\n myAttachment.name--->'+attachment.name);
                system.debug('check the attachement..'+ attachment);
                
                try
                 {
                    if((attachment.Name!=null) &&(attachment.Body!=null))                               
                    insert attachment;
                    attachment.Body=null;
                    attachment.clear();
                     nameFile=null;
                     contentFile=null;                   
                     memberList =[select name, id,(Select Id,Name from Attachments) Attmts,Account__c,Description__c from Compliance_Suitability__c where Account__c=:a.id order by LastModifiedDate DESC];  
                 }
                 catch (DMLException e)
                 {
                    system.debug('check the attachement..'+ attachment);
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
                  }
                return ApexPages.currentPage();
            }           
        }
        catch(Exception e){
            system.debug('Exception --->'+e);
        } 
          
       // pagereference   p = new Pagereference('/'+ApexPages.currentPage().getParameters().get('id')) ;
       contentFile=null;
       nameFile=null;
       system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> End');
       return null;           
    }
    public Pagereference cancelnewProg(){
        addnew=false;
        return null;    
    }  
   

    
 }