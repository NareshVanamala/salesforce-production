@isTest
Public class RC_LocationSearchClass_Test
{
  static testmethod void testLocationSearchClass()
  {
  
     RC_Property__c PRT=new RC_Property__c();
     PRT.name='Amazon DC 2014 USAA~A11123';
     PRT.City__c='Hyderabad';
     insert PRT;
     
     RC_Location__c LC=new RC_Location__c();
     LC.name='Amazon DC 2014 USAA~A11123';
     LC.Entity_Tenant_Code__c='12345';
     //LC.City__c='Hyderabad';
     LC.Property__c=PRT.id;
     //insert LC;
     
      System.assertEquals(LC.Name, PRT.name);

     
     list<RC_Location__c>RCLocation=new list<RC_Location__c>();
     RCLocation.add(lc);
     insert RCLocation;
     
     
     
     RC_LocationSearchClass  RCLsearchClass=new RC_LocationSearchClass ();
     RCLsearchClass.searchString='Hyderabad';
     RCLsearchClass.searchby ='Hyderabad';
     string mystring='Hyderabad';
     
     RCLsearchClass.OffsetSize =0;
     RCLsearchClass.LimitSize=50;
     RCLsearchClass.Search();
     RCLocation=RCLsearchClass.performSearch(mystring);
    
    RCLsearchClass.getSearchString();
    RCLsearchClass.setSearchString(mystring); 
    RCLsearchClass.FirstPage();
    RCLsearchClass.OffsetSize =50;
    RCLsearchClass.previous();
    RCLsearchClass.next();
    RCLsearchClass.LastPage();
    RCLsearchClass.getprev();
    RCLsearchClass.getnxt();
    RCLsearchClass.getValue();
    RCLsearchClass.getSearch();
  
  }
  
  
}