global class SobjectFieldSetHelperClass{
   //this method return the list of fields and acceptd the fieldsetname and objectName as parameter
   public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName){ 
       Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
       Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
       Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
       Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
       return fieldSetObj.getFields(); 
            
   }  
   // this method return the label of the fields in one string and this method uses the readfieldset method to check the fields
   //This method also accpetd the fieldssetName and objectname as paramter;
        
   public static string getLabelsFromFieldset(String fieldsetName, string Objname){
       List<Schema.FieldSetMember> fieldSetMemberList = SobjectFieldSetHelperClass.readFieldSet(fieldsetName,Objname);
       string fieldlabels;
       for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList){
           if(fieldlabels !=null)
              fieldlabels=fieldlabels+fieldSetMemberObj.getLabel()+';';
           if(fieldlabels ==null)
              fieldlabels= fieldSetMemberObj.getLabel()+';';
       }
       fieldlabels=fieldlabels.removeend(';');
       return fieldlabels;
   }
   //this method returns api names and type of the fields from the field set and this method takes the fieldsetName and objectname as parameter 
        
   public static string getAPINamesandTypeFromFieldset(String fielsetName, string Objname){
       List<Schema.FieldSetMember> fieldSetMemberList = SobjectFieldSetHelperClass.readFieldSet(fielsetName,Objname);
       string fieldApiNamesandTypes;
       for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList){
           if(fieldApiNamesandTypes !=null)
              fieldApiNamesandTypes=fieldApiNamesandTypes+fieldSetMemberObj.getFieldPath()+'='+fieldSetMemberObj.getType()+';';
           if(fieldApiNamesandTypes==null)
              fieldApiNamesandTypes= fieldSetMemberObj.getFieldPath()+'='+fieldSetMemberObj.getType()+';';
                        
           system.debug('API Name ====>' + fieldSetMemberObj.getFieldPath()); //api name
           system.debug('Label ====>' + fieldSetMemberObj.getLabel());
           system.debug('Required ====>' + fieldSetMemberObj.getRequired());
           system.debug('DbRequired ====>' + fieldSetMemberObj.getDbRequired());
           system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
       }
       system.debug('These are field names and their types'+fieldApiNamesandTypes);
       return  fieldApiNamesandTypes;
    
   }
        
   //this method returns api names of the fields from the field set and this method takes the fieldsetName and objectname as parameter 
        
   public static string getAPINamesFromFieldset(String fielsetName, string Objname){
       List<Schema.FieldSetMember> fieldSetMemberList = SobjectFieldSetHelperClass.readFieldSet(fielsetName,Objname);
       string fieldApiNames;
       for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList){
           if(fieldApiNames !=null)
              fieldApiNames=fieldApiNames+fieldSetMemberObj.getFieldPath()+',';
           if(fieldApiNames ==null)
              fieldApiNames= fieldSetMemberObj.getFieldPath()+',';
       }
       return  fieldApiNames;
    
   }
   public static string getLabelsandPicklistValuesFromFieldset(String fieldsetName, string Objname){
       map<string, string>typetofieldMap=new map<string,string>();
       typetofieldMap.put('MULTIPICKLIST','None');
       typetofieldMap.put('STRING','Null');
       typetofieldMap.put('PICKLIST','None');
       typetofieldMap.put('BOOLEAN','False');
       List<Schema.FieldSetMember> fieldSetMemberList = SobjectFieldSetHelperClass.readFieldSet(fieldsetName,Objname);
       string fieldlabels;
       for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList){
           if((fieldlabels !=null)&&((fieldSetMemberObj.getType()==Schema.DisplayType.PICKLIST)||(fieldSetMemberObj.getType()==Schema.DisplayType.MULTIPICKLIST))){
              system.debug('I am in second block '+fieldSetMemberObj );
              string picklistVlues=SobjectFieldSetHelperClass.getpicklistValuesfromtheschema(Objname,fieldSetMemberObj.getFieldPath());
              fieldlabels= fieldlabels+'FieldName: '+fieldSetMemberObj.getLabel()+', '+'Type:'+fieldSetMemberObj.getType()+', '+'Value: '+'{'+picklistVlues+'}'+',';
           }
           else if((fieldlabels !=null)&&((fieldSetMemberObj.getType()!=Schema.DisplayType.PICKLIST)||(fieldSetMemberObj.getType()!=Schema.DisplayType.MultiPicklist))){
              system.debug('I am in first block '+fieldSetMemberObj );
              fieldlabels=fieldlabels +'FieldName: '+fieldSetMemberObj.getLabel()+', '+'Type:'+fieldSetMemberObj.getType()+', '+'Value: '+ typetofieldMap.get(string.valueOf(fieldSetMemberObj.getType()))+',';
           }
           else if((fieldlabels ==null)&&((fieldSetMemberObj.getType()==Schema.DisplayType.PICKLIST)||(fieldSetMemberObj.getType()==Schema.DisplayType.MULTIPICKLIST))){
              string picklistVlues=SobjectFieldSetHelperClass.getpicklistValuesfromtheschema(Objname,fieldSetMemberObj.getFieldPath());
              fieldlabels= 'FieldName: '+fieldSetMemberObj.getLabel()+', '+'Type:'+fieldSetMemberObj.getType()+', '+'Value: '+'{'+picklistVlues+'}'+',';
           }
           else if((fieldlabels ==null)&&((fieldSetMemberObj.getType()!=Schema.DisplayType.PICKLIST)||(fieldSetMemberObj.getType()!=Schema.DisplayType.MultiPicklist))){
              fieldlabels='FieldName: '+fieldSetMemberObj.getLabel()+', '+'Type:'+fieldSetMemberObj.getType()+', '+'Value: '+ typetofieldMap.get(string.valueOf(fieldSetMemberObj.getType()))+',';
           } 
       }
       system.debug('These are fields'+fieldlabels);
       return fieldlabels;
   }
 
   //this method returns the picklistvalues from the object as one string. This method takes objectnames and the list of the picklistfields as parameter
   public static string getpicklistValuesfromtheschema(string objname,string fieldname){
       system.debug('objname123'+objname);
       system.debug('outer fieldname'+fieldname);
       if(objname == 'Contact' && !fieldname.contains('Account.') && Schema.describeSObjects(new String[]{objname})[0].fields.getMap().get(fieldname).getDescribe().getController()==null){
       //if(objname == 'Contact'){
          system.debug('Fieldname123'+fieldname);
          string ContactPickListValues;
          set<string> pickListValuesSet = new set<string>();
          list<string> pickListValuesList = new list<string>();  
          list<string> ConTypeValues = new list<string>(); 
          for(Cockpit_Contact_Type_Filters__c recTypeValues : Cockpit_Contact_Type_Filters__c.getAll().values()){
              ConTypeValues.add(recTypeValues.Record_Type_Name__c);
          } 
          for(integer i=0;i<ConTypeValues.Size();i++){            
              List<string> options = PicklistDescriberBasedOnReordType.describe('Contact', ConTypeValues[i], fieldName);
              pickListValuesSet.addAll(options);
              pickListValuesList.addAll(pickListValuesSet);  
          }
          for(integer i=0;i<pickListValuesList.Size();i++){
              if(ContactPickListValues !=null)
                 ContactPickListValues= ContactPickListValues+pickListValuesList[i]+';';
              else
                 ContactPickListValues = pickListValuesList[i]+';';
          }  
          system.debug('String ContactPickListValues=' +ContactPickListValues);
          if(ContactPickListValues != null && ContactPickListValues != '')
             ContactPickListValues= ContactPickListValues.removeend(';');
          system.debug('@@@@@@@@@@@@picklistvalue@@@@@####String ContactPickListValues ='+ContactPickListValues);
          return ContactPickListValues;
       }
       else{
          if(fieldname.contains('Account.')){
             objname='Account'; 
             fieldName=fieldName.replace('Account.','');
          }
          Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
          Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objname);
          Map<String, Schema.SobjectField> fieldMap= SObjectTypeObj.getDescribe().fields.getMap();
          string pckval;
          system.debug('fieldMap'+fieldMap);
          System.debug('##Field API Name='+fieldName);
          System.debug('##Object API Name='+objname);
          List<Schema.Picklistentry>fld =fieldmap.get(fieldname).getDescribe().getpicklistValues();
          System.debug('####AAA='+fld);
          for(integer i=0;i<fld.Size();i++){
              if(pckval !=null)
                 pckval= pckval+fld[i].getvalue()+';';
              else
                 pckval = fld[i].getvalue()+';';
          }    
          pckval= pckval.removeend(';');
          system.debug('@@@@@@@@@@@@picklistvalue@@@@@####String pickvalue ='+pckval);
          return pckval;
      }
   }
   public static string getLabelstypeValuefromFieldsetinJason(String fieldsetName, string Objname){
       string fieldlabels;
       list<FilterWrapper>filterWrapperlist=new list<FilterWrapper>();
       map<string, string>typetofieldMap=new map<string,string>();
       typetofieldMap.put('MULTIPICKLIST','None');
       typetofieldMap.put('STRING','Null');
       typetofieldMap.put('PICKLIST','None');
       typetofieldMap.put('BOOLEAN','False');    
       List<Schema.FieldSetMember> fieldSetMemberList = SobjectFieldSetHelperClass.readFieldSet(fieldsetName,Objname);    
       for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList){
           if((fieldlabels !=null)&&((fieldSetMemberObj.getType()==Schema.DisplayType.PICKLIST)||(fieldSetMemberObj.getType()==Schema.DisplayType.MULTIPICKLIST))){
               system.debug('I am in second block '+fieldSetMemberObj );
               string picklistVlues=SobjectFieldSetHelperClass.getpicklistValuesfromtheschema(Objname,fieldSetMemberObj.getFieldPath());
               FilterWrapper myft=new FilterWrapper (fieldSetMemberObj.getLabel(),fieldSetMemberObj.getFieldPath(),string.valueof(fieldSetMemberObj.getType()),picklistVlues,'');
               filterWrapperlist.add(myft);
           }
           else if((fieldlabels !=null)&&((fieldSetMemberObj.getType()!=Schema.DisplayType.PICKLIST)||(fieldSetMemberObj.getType()!=Schema.DisplayType.MultiPicklist))){
              system.debug('I am in first block '+fieldSetMemberObj );
              FilterWrapper myft=new FilterWrapper (fieldSetMemberObj.getLabel(),fieldSetMemberObj.getFieldPath(),string.valueOf(fieldSetMemberObj.getType()),typetofieldMap.get(string.valueOf(fieldSetMemberObj.getType())), '');
              filterWrapperlist.add(myft);
           }
           else if((fieldlabels ==null)&&((fieldSetMemberObj.getType()==Schema.DisplayType.PICKLIST)||(fieldSetMemberObj.getType()==Schema.DisplayType.MULTIPICKLIST))){
              string picklistVlues=SobjectFieldSetHelperClass.getpicklistValuesfromtheschema(Objname,fieldSetMemberObj.getFieldPath());
              FilterWrapper myft=new FilterWrapper (fieldSetMemberObj.getLabel(),fieldSetMemberObj.getFieldPath(),string.valueOf(fieldSetMemberObj.getType()),picklistVlues, '');    
              filterWrapperlist.add(myft);  
           }
           else if ((fieldlabels ==null)&&((fieldSetMemberObj.getType()!=Schema.DisplayType.PICKLIST)||(fieldSetMemberObj.getType()!=Schema.DisplayType.MultiPicklist))){
              filterWrapper myft=new FilterWrapper (fieldSetMemberObj.getLabel(),fieldSetMemberObj.getFieldPath(),string.valueOf(fieldSetMemberObj.getType()),typetofieldMap.get(string.valueOf(fieldSetMemberObj.getType())), '');
              filterWrapperlist.add(myft);  
           } 
       }
       system.debug('These are fields'+filterWrapperlist);
       string CCLJSON = JSON.serializepretty(filterWrapperlist);
       CCLJSON = CCLJSON.removeend(',');
       system.debug('this is myquery'+CCLJSON );
       return CCLJSON ;
   }
   //below method is only for cockpit as ui needs the output in particuler format
   public static string getLabelstypeValuefromFieldsetinJasonforcockpit(String fieldsetName, string Objname){
       string fieldlabels;
       list<FilterWrapper>filterWrapperlist=new list<FilterWrapper>();
       map<string, string>typetofieldMap=new map<string,string>();
       typetofieldMap.put('MULTIPICKLIST','None');
       typetofieldMap.put('STRING','Null');
       typetofieldMap.put('PICKLIST','None');
       typetofieldMap.put('BOOLEAN','False');    
       List<Schema.FieldSetMember> fieldSetMemberList = SobjectFieldSetHelperClass.readFieldSet(fieldsetName,Objname);
       for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList){
           if((fieldlabels !=null)&&((fieldSetMemberObj.getType()==Schema.DisplayType.PICKLIST)||(fieldSetMemberObj.getType()==Schema.DisplayType.MULTIPICKLIST))){
              system.debug('I am in second block '+fieldSetMemberObj );
              string picklistVlues=SobjectFieldSetHelperClass.getpicklistValuesfromtheschema(Objname,fieldSetMemberObj.getFieldPath());
              if(Objname=='Contact'){
                 FilterWrapper myft=new FilterWrapper (fieldSetMemberObj.getLabel(),'Contact__r.'+fieldSetMemberObj.getFieldPath(),string.valueof(fieldSetMemberObj.getType()),picklistVlues, '');
                 filterWrapperlist.add(myft);
              }  
              if(Objname=='Account'){
                 FilterWrapper myft=new FilterWrapper (fieldSetMemberObj.getLabel(),'Contact__r.Account.'+fieldSetMemberObj.getFieldPath(),string.valueof(fieldSetMemberObj.getType()),picklistVlues, '');
                 filterWrapperlist.add(myft);
              }          
           }
           else if((fieldlabels !=null)&&((fieldSetMemberObj.getType()!=Schema.DisplayType.PICKLIST)||(fieldSetMemberObj.getType()!=Schema.DisplayType.MultiPicklist))){
              system.debug('I am in first block '+fieldSetMemberObj );
              if(Objname=='Contact'){
                 FilterWrapper myft=new FilterWrapper(fieldSetMemberObj.getLabel(),'Contact__r.'+fieldSetMemberObj.getFieldPath(),string.valueOf(fieldSetMemberObj.getType()),typetofieldMap.get(string.valueOf(fieldSetMemberObj.getType())), 
                                                              (string.valueOf(fieldSetMemberObj.getType()).equalsIgnoreCase('reference') && !Schema.sObjectType.Contact.fields.getMap().get(fieldSetMemberObj.getFieldPath()).getDescribe().isNamePointing()
                                                                    ? Schema.sObjectType.Contact.fields.getMap().get(fieldSetMemberObj.getFieldPath()).getDescribe().getReferenceTo()[0].getdescribe().getName() : 'Multiple'));
                 filterWrapperlist.add(myft);
                         
              }
              if(Objname=='Account'){
                 FilterWrapper myft=new FilterWrapper (fieldSetMemberObj.getLabel(),'Contact__r.Account.'+fieldSetMemberObj.getFieldPath(),string.valueOf(fieldSetMemberObj.getType()),typetofieldMap.get(string.valueOf(fieldSetMemberObj.getType())), 
                                                              (string.valueOf(fieldSetMemberObj.getType()).equalsIgnoreCase('reference') && !Schema.sObjectType.Account.fields.getMap().get(fieldSetMemberObj.getFieldPath()).getDescribe().isNamePointing()
                                                                    ? Schema.sObjectType.Contact.fields.getMap().get(fieldSetMemberObj.getFieldPath()).getDescribe().getReferenceTo()[0].getdescribe().getName() : 'Multiple'));
                 filterWrapperlist.add(myft);
                         
              }
           }
           else if((fieldlabels ==null)&&((fieldSetMemberObj.getType()==Schema.DisplayType.PICKLIST)||(fieldSetMemberObj.getType()==Schema.DisplayType.MULTIPICKLIST))){
              string picklistVlues=SobjectFieldSetHelperClass.getpicklistValuesfromtheschema(Objname,fieldSetMemberObj.getFieldPath());
              if(Objname=='Contact'){
                 FilterWrapper myft=new FilterWrapper (fieldSetMemberObj.getLabel(),'Contact__r.'+fieldSetMemberObj.getFieldPath(),string.valueOf(fieldSetMemberObj.getType()),picklistVlues,'');
                 filterWrapperlist.add(myft);  
              } 
              if(Objname=='Account'){
                 FilterWrapper myft=new FilterWrapper (fieldSetMemberObj.getLabel(),'Contact__r.Account.'+fieldSetMemberObj.getFieldPath(),string.valueOf(fieldSetMemberObj.getType()),picklistVlues, '');
                 filterWrapperlist.add(myft);  
              } 
                         
           }
           else if((fieldlabels ==null)&&((fieldSetMemberObj.getType()!=Schema.DisplayType.PICKLIST)||(fieldSetMemberObj.getType()!=Schema.DisplayType.MultiPicklist))){
              if(Objname=='Contact'){
                 filterWrapper myft=new FilterWrapper (fieldSetMemberObj.getLabel(),'Contact__r.'+fieldSetMemberObj.getFieldPath(),string.valueOf(fieldSetMemberObj.getType()),typetofieldMap.get(string.valueOf(fieldSetMemberObj.getType())), 
                                                                (string.valueOf(fieldSetMemberObj.getType()).equalsIgnoreCase('reference') && !Schema.sObjectType.Contact.fields.getMap().get(fieldSetMemberObj.getFieldPath()).getDescribe().isNamePointing()
                                                                    ? Schema.sObjectType.Contact.fields.getMap().get(fieldSetMemberObj.getFieldPath()).getDescribe().getReferenceTo()[0].getdescribe().getName() : 'Multiple'));
                 filterWrapperlist.add(myft);
              }    
              if(Objname=='Account'){
                 filterWrapper myft=new FilterWrapper (fieldSetMemberObj.getLabel(),'Contact__r.Account.'+fieldSetMemberObj.getFieldPath(),string.valueOf(fieldSetMemberObj.getType()),typetofieldMap.get(string.valueOf(fieldSetMemberObj.getType())), 
                                                                (string.valueOf(fieldSetMemberObj.getType()).equalsIgnoreCase('reference') && !Schema.sObjectType.Account.fields.getMap().get(fieldSetMemberObj.getFieldPath()).getDescribe().isNamePointing()
                                                                    ? Schema.sObjectType.Contact.fields.getMap().get(fieldSetMemberObj.getFieldPath()).getDescribe().getReferenceTo()[0].getdescribe().getName() : 'Multiple'));
                 filterWrapperlist.add(myft);
              }  
           } 
       }
       system.debug('These are fields'+filterWrapperlist);
       string CCLJSON = JSON.serializepretty(filterWrapperlist);
       CCLJSON = CCLJSON.removeend(',');
       system.debug('this is myquery'+CCLJSON );
       return CCLJSON ;
   }
   
   public class FilterWrapper{
       public String fieldName {get;set;}
       public String type{get;set;}
       public String value {get;set;}
       public string fieldapiName{get;set;}
       public String parentObject{get; set;}
       public FilterWrapper(String name,string fieldapiname,String type,String value, String parentObject){
           this.fieldName =name;
           this.fieldapiName=fieldapiname;
           this.type=type;
           this.value =value;
           this.parentObject=parentObject;
       }
   }
   //below method takes the string in the Json format. the string should be in the format of label and value
   //e.g{ "First Name" :"Snehal","Territory" : "Not assigned". 
   //This method return the json string with apiname and value .
   
   public static string getapinameandvalueFromlabel(string mystring){
       set<String>labelsfromstring=new set<string>();
       Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(mystring);
       labelsfromstring=m.keyset();
       system.debug('These are my keys'+labelsfromstring);   
       Map<String, Schema.SObjectField> Mp = Schema.SObjectType.Contact.fields.getMap();
       Map<string, string>lebeltoNameMap=new map<string, string>();
       map<string,string>apinametoValueMap=new map<String, string>();
       for(String mylabel: labelsfromstring){
           for(String key : Mp.keyset()){
               if(mylabel== Mp.get(key).getDescribe().getLabel()){                           
                  lebeltoNameMap.put(mylabel,key);
               }
           }
       }  
       system.debug('This is the string'+lebeltoNameMap);
       for(String mylabel: labelsfromstring){
           apinametoValueMap.put(lebeltoNameMap.get(mylabel),string.Valueof(m.get(mylabel)) );   
       }
       list<map<string,String>>jsonstringlist=new list<map<String,String>>();
       jsonstringlist.add(apinametoValueMap);
       string  MyreturnString=JSON.serializePretty(jsonstringlist);
       return MyreturnString;  
   }
   public static string getLabelapiobjectfromFieldsetinJason(String fieldsetName, string Objname){
       list<wrappercolumn>mywrapperlist=new list<wrappercolumn>();
       List<Schema.FieldSetMember> fieldSetMemberList = SobjectFieldSetHelperClass.readFieldSet(fieldsetName,Objname); 
       for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList){
           system.debug('is this my label'+fieldSetMemberObj);
           wrappercolumn wc=new wrappercolumn(string.valueof(fieldSetMemberObj.getLabel()),string.valueof(fieldSetMemberObj.getFieldPath()),Objname);
           mywrapperlist.add(wc);
       }  
       string  MyreturnString=JSON.serializePretty(mywrapperlist);
       return MyreturnString;

   }
   public class wrappercolumn{
       public String fieldName {get;set;}
       public String fieldapiName{get;set;}
       public String objectName {get;set;}
       public wrappercolumn(String name,String apiName,String objectname){
           this.fieldName =name;
           this.fieldapiName=apiName;
           this.objectName =objectname;
      }
   }   
 
// **************adding new method for related list
   public static string getLabelApiNameAndTypeOfobjectfromFieldsetinJason(String fieldsetName, string Objname){
       list<wrapperRelatedList>mywrapperlist=new list<wrapperRelatedList>();
       List<Schema.FieldSetMember> fieldSetMemberList = SobjectFieldSetHelperClass.readFieldSet(fieldsetName,Objname); 
       for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList){
           system.debug('is this my label'+fieldSetMemberObj);
           wrapperRelatedList wc=new wrapperRelatedList(string.valueof(fieldSetMemberObj.getLabel()),string.valueof(fieldSetMemberObj.getFieldPath()),string.valueof(fieldSetMemberObj.getType()),Objname);
           mywrapperlist.add(wc);

       }  
       string  MyreturnString=JSON.serializePretty(mywrapperlist);
       return MyreturnString;

   }
   public class wrapperRelatedList{
       public String fieldName {get;set;}
       public String fieldapiName{get;set;}
       public String objectName {get;set;}
       public String FieldType {get;set;}
       public wrapperRelatedList(String name,String apiName,String fieldtype,String objectname){
           this.fieldName =name;
           this.fieldapiName=apiName;
           this.fieldtype=FieldType; 
           this.objectName =objectname;
       }
   }   

   public class wrapperAdvisorDetail{
       public String SectionName {get;set;}
       public list<sobject>Slist {get;set;}
       public list<string>otherCalllistNames{get;set;}
       public map<string,list<sobject>> objectMap {get;set;}
       public wrapperAdvisorDetail(String name,list<sobject>apiName){
           this.SectionName =name;
           this.Slist =apiName;
       }
       public wrapperAdvisorDetail(String name,map<string,list<sobject>>apiNameMap){
           this.SectionName =name;
           this.objectMap =apiNameMap;
       }
       public wrapperAdvisorDetail(String name,list<string>othercalllistnames){
           this.SectionName =name;
           this.otherCalllistNames=othercalllistnames;
       }
   }   
   
}