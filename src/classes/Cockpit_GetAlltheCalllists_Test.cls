@isTest
public class Cockpit_GetAlltheCalllists_Test{
    static testMethod void myTest_Method1(){
        Account a = new Account();
        a.name = 'testaccount';
        insert a;   
     
        Contact c = new Contact();
        c.accountid = a.id;
        c.FirstName = 'Satheesh';
        c.LastName ='Polaris';
        c.MailingStreet='1078 3rd Ave';
        c.Mailingcity='New York';
        c.MailingPostalCode='10065';
        c.MailingCountry='USA';
        insert c;
        system.assert(c!=null);
        
        Automated_Call_List__c CockpitAutomatedCalllist= new Automated_Call_List__c();
        CockpitAutomatedCalllist.name='CockpitAutomatedcallListName';
        CockpitAutomatedCalllist.Type_Of_Call_List__c='Campaign Call List';
        CockpitAutomatedCalllist.IsDeleted__c=false;
        CockpitAutomatedCalllist.Archived__c=false;
        insert CockpitAutomatedCalllist;
        system.assert(CockpitAutomatedCalllist!=null);
        
        Contact_Call_List__c cockpitCCL=new Contact_Call_List__c();
        cockpitCCL.Automated_Call_List__c=CockpitAutomatedCalllist.id;
        cockpitCCL.contact__c=c.id;
        cockpitCCL.IsDeleted__c=false;
        cockpitCCL.Call_Complete__c=false;
        insert cockpitCCL;
        system.assert(cockpitCCL!=null);

        system.assertequals(cockpitCCL.Contact__c,c.id);

        ApexPages.currentPage().getParameters().put('id',c.id);
        //ApexPages.StandardController controller ; 
        Cockpit_GetAlltheCalllists x = new Cockpit_GetAlltheCalllists();
        
        
    }

}