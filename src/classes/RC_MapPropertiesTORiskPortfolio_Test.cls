@isTest
Public class RC_MapPropertiesTORiskPortfolio_Test
{
  static testmethod void testMapPropertiesTORiskPortfolioClass()
  {
  
    Portfolio__c pc= new Portfolio__c();
    pc.name='Testing1';
    Insert pc; 
     
    RC_Risk_Portfolio__c RPC = new RC_Risk_Portfolio__c();
    RPC.name='Testing1';
    RPC.Related_Portfolio__c=pc.id;
    Insert RPC;
    
    list<Deal__c>deallist=new list<Deal__c>();
    Deal__c d = new Deal__c();
    d.Name ='Test12';  
    d.Portfolio_Deal__c=pc.id;  
    d.Build_to_Suit_Type__c ='Current';
    d.Estimated_COE_Date__c = date.today();
    d.Targeted_COE_Date__c  = date.today();
    d.HVAC_Warranty_Status__c ='Received';
    d.Ownership_Interest__c = 'Fee Simple';
    d.Roof_Warranty_Status__c = 'Received';
    d.Zip_Code__c = '98033';
    d.Create_Workspace__c =true; 
    d.Deal_Status__c='Signed LOI';
    d.GAAP_NOI__c = 13.8;
    d.Division__c='ST Retail';
    d.LOI_Signed_Date__c=system.today();
    d.MRI_Property_Type_picklist__c='Freestanding Retail';
    d.Number_of_Floors__c='3';
    d.Ownership_Type__c='TIC';
    d.Primary_Use__c='Call Center';
    d.Property_Location__c='Suburban [OID Only]';
    d.Property_Sub_Type__c='Big Box Retail';
    
    deallist.add(d) ;
  
     Deal__c d2 = new Deal__c();
    d2.Name ='Test121';  
    d2.Portfolio_Deal__c=pc.id;  
    d2.Build_to_Suit_Type__c ='Current';
    d2.Estimated_COE_Date__c = date.today();
    d2.Targeted_COE_Date__c  = date.today();
    d2.HVAC_Warranty_Status__c ='Received';
    d2.Ownership_Interest__c = 'Fee Simple';
    d2.Roof_Warranty_Status__c = 'Received';
    d2.Zip_Code__c = '98033';
    d2.GAAP_NOI__c = 13.8;
    d2.Create_Workspace__c =true; 
    d2.Deal_Status__c='Signed LOI';
    d2.Division__c='ST Retail';
    d2.LOI_Signed_Date__c=system.today();
    d2.MRI_Property_Type_picklist__c='Freestanding Retail';
    d2.Number_of_Floors__c='3';
    d2.Ownership_Type__c='TIC';
    d2.Primary_Use__c='Call Center';
    d2.Property_Location__c='Suburban [OID Only]';
    d2.Property_Sub_Type__c='Big Box Retail';
    d2.GAAP_NOI__c=1526627.00;
    deallist.add(d2) ;
    
    insert deallist;
    
    pc.Status__c='Signed LOI';
    update pc;
  
  
  
  }
  
}