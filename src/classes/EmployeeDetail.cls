public with sharing class EmployeeDetail
 {
 
    public string empid{get;set;} 
    public Employee__c emp{get;set;} 
    public List<Asset__c>samemaillist1{set;get;}


    
    public EmployeeDetail(ApexPages.StandardController controller)
    {
      emp=new Employee__c();
      
          empid=ApexPages.currentPage().getParameters().get('id');
          emp=[select Termination_Status__c,Termination_date__c, Additional_MailBox4__c,Additional_MailBox3__c,Additional_MailBox1__c,Additional_MailBox2__c,Active_Directory_Group__c,Active_Directory_Group_2__c,Active_Directory_Group_3__c,Active_Directory_Group_4__c,Active_Directory_Group_5__c,Active_Directory_Group_6__c,MGR__r.name,SQL__c,Role__c ,id,name,HR_Manager_Status__c,Employee_Status__c,Full_Name__c,Supervisor__c,Terminated__c,Asset__c,Employee_Role__c,First_Name__c,Last_Name__c,MGR__c,Employee_Office_Location__c,Start_Date__c,Department__c,Move_User_To__c,Seating_location__c,Title__c from Employee__c where id=:empid];            
          samemaillist1= [select id,Asset_Status__c,Added_On__c,Access_Name__c,Access_Type__c,Removed_On__c,Name,Updating__c from  Asset__c where Employee__c=:emp.ID and ((Updating__c=true)or((Asset_Status__c='Removal In Process')and(Updating__c=false))) order by Access_Name__c];

        }

    public EmployeeDetail()
    {
      
      empid=ApexPages.currentPage().getParameters().get('id');
      emp=[select id,name from Employee__c where id=:empid]; 
      samemaillist1= [select id,Asset_Status__c,Added_On__c,Access_Name__c,Access_Type__c,Removed_On__c,Name,Updating__c from  Asset__c where Employee__c=:emp.ID and ((Updating__c=true)or((Asset_Status__c='Removal In Process')and(Updating__c=false))) order by Access_Name__c];
           

    }


  
}