@isTest(SeeAlldata = true)
 public class Test_ActionPlanMassUpdateAlldeals_class1
{
    static testMethod void myUnitTest1234()
    {
        
        Portfolio__c pc= new Portfolio__c();
        pc.name='Testing1';
        Insert pc;  
               
        Deal__c d = new Deal__c();
        d.Name ='Test12'; 
        d.Portfolio_Deal__c=pc.id;  
        d.Build_to_Suit_Type__c ='Current';
        d.Estimated_COE_Date__c = date.today();
        d.HVAC_Warranty_Status__c ='Received';
        d.Ownership_Interest__c = 'Fee Simple';
        d.Roof_Warranty_Status__c = 'Received';
        d.Zip_Code__c = '98033';
        d.Create_Workspace__c =true; 
        insert d ;
    
        Deal__c d1 = new Deal__c();
        d1.Name ='Test13';  
        d1.Portfolio_Deal__c=pc.id;  
        d1.Build_to_Suit_Type__c ='Current';
        d1.Estimated_COE_Date__c = date.today();
        d1.HVAC_Warranty_Status__c ='Received';
        d1.Ownership_Interest__c = 'Fee Simple';
        d1.Roof_Warranty_Status__c = 'Received';
        d1.Zip_Code__c = '98033';
        d1.Create_Workspace__c =true;   
        d1.Portfolio_Deal__c=pc.id;      
        insert d1 ;

       System.assertNotEquals(d.Name,d1.Name );

         
        Template__c temp = new Template__c();
        temp.Name = 'Template';
        temp.Is_Template__c=true;
        insert temp ;   
       
        Template__c Actiontemp = new Template__c();
        Actiontemp.Name = 'ActionPlanfordeal';
        Actiontemp.Is_Template__c=false;
        Actiontemp.Deal__c=d1.id;
        Actiontemp.Template__c=temp.id; 
        insert Actiontemp ;  
    
        Template__c Actiontemp1 = new Template__c();
        Actiontemp1.Name = 'ActionPlanfordeal';
        Actiontemp1.Is_Template__c=false;
        Actiontemp1.Deal__c=d.id;
        Actiontemp1.Template__c=temp.id; 
        insert Actiontemp1 ; 
    
    
        Task_Plan__c tp3 = new Task_Plan__c ();
        tp3.Name='test';
        tp3.priority__c = 'Medium';
        tp3.Index__c = 1;
        tp3.Assigned_To__c = Userinfo.getUserId();
        tp3.Template__c =temp.Id ; 
        tp3.Comments__c='Check me';
        tp3.Date_Ordered__c=date.today();
        tp3.Date_Needed__c=date.today();
        tp3.Date_Received__c=date.today();
        tp3.Field_to_update_Date_Received__c='Site_Visit_Date__c';
       // tp.Field_to_update_Date_Received__c=string.valueof(date.today());
        insert tp3 ; 
        
       
        Task_Plan__c tp1 = new Task_Plan__c ();
         tp1.Name='test';
        tp1.priority__c = 'Medium';
        tp1.Index__c = 1;
        tp1.Assigned_To__c = Userinfo.getUserId();
        tp1.Template__c =Actiontemp.Id ; 
        tp1.Comments__c='Check me';
        tp1.Date_Ordered__c=date.today();
        tp1.Date_Needed__c=date.today();
        tp1.Date_Received__c=date.today();
        //tp1.Field_to_update_Date_Received__c=string.valueof(date.today());
        tp1.Field_to_update_Date_Received__c='Site_Visit_Date__c'; 
       
        insert tp1 ; 
    
       Task_Plan__c tp2 = new Task_Plan__c ();
       tp2.Name='test';
        tp2.priority__c = 'Medium';
        tp2.Index__c = 1;
        tp2.Assigned_To__c = Userinfo.getUserId();
        tp2.Template__c =Actiontemp1.Id ; 
        tp2.Comments__c='Check me';
        tp2.Date_Ordered__c=date.today();
        tp2.Date_Needed__c=date.today();
        tp2.Date_Received__c=date.today();
        //tp2.Field_to_update_Date_Received__c=string.valueof(date.today());
        tp2.Field_to_update_Date_Received__c='Site_Visit_Date__c';
        insert tp2 ; 
    
      tp2.priority__c ='High';
      update tp2;
    
    
         Task_Plan__c tp = new Task_Plan__c ();
    
        List<Task_Plan__c> TPL=new List<Task_Plan__c>();
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Dead_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Site_Visit_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Appraisal_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Passed_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Accenture_Due_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_COE_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Go-Hard Date', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Hard_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Rep_Burn_Off__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_SP_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Site_Visit_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Purchase_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Query_Sorted_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_Hard_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Passed_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Contract_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_COE_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Closing_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Hard_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='LOI_Signed_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
        tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_SP_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='LOI_Sent_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Purchase_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Lease_Abstract_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_Hard_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Lease_Expiration__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Contract_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Open_Escrow_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Closing_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Dead_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='LOI_Signed_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Tenant_ROFR_Waiver__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='LOI_Sent_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Date_Audit_Completed__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1 .Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Lease_Abstract_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Date_Identified__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Lease_Expiration__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Investment_Committee_Approval__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Open_Escrow_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Part_1_Start_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Purchase_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Tenant_ROFR_Waiver__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Date_Audit_Completed__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Date_Identified__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Investment_Committee_Approval__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Part_1_Start_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
         tp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estoppel__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp1.Id);
        TPL.add(tp);
       insert TPL;
        
        Task_Plan__c tpp = new Task_Plan__c ();
        List<Task_Plan__c> TPL1=new List<Task_Plan__c>();
        tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Dead_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Site_Visit_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
        tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Appraisal_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Passed_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
        tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Accenture_Due_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_COE_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
        tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Go-Hard Date', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Hard_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
        tpp  = new Task_Plan__c (Field_to_update_for_date_needed__c='Rep_Burn_Off__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_SP_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
        tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Site_Visit_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Purchase_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
        tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Query_Sorted_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estimated_Hard_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
        tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Passed_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Contract_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
        tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_COE_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Closing_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
        tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Hard_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='LOI_Signed_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
        tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_SP_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='LOI_Sent_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Purchase_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Lease_Abstract_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estimated_Hard_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Lease_Expiration__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Contract_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Open_Escrow_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Closing_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Dead_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='LOI_Signed_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Tenant_ROFR_Waiver__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='LOI_Sent_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Date_Audit_Completed__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Lease_Abstract_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Date_Identified__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Lease_Expiration__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Investment_Committee_Approval__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Open_Escrow_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Part_1_Start_Date__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Purchase_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Tenant_ROFR_Waiver__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Date_Audit_Completed__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Date_Identified__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Investment_Committee_Approval__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Part_1_Start_Date__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         tpp = new Task_Plan__c (Field_to_update_for_date_needed__c='Estoppel__c', Date_Needed__c=Date.Today(), Field_to_update_Date_Received__c='Estoppel__c', Date_Received__c=date.today(), Name='test', priority__c = 'Medium', Index__c = 1, Assigned_To__c = Userinfo.getUserId(), Template__c =Actiontemp.Id);
        TPL1.add(tpp);
         
        insert TPL1;
  
        string Query;
        Query='select id, name from Deal__c where id=:d.id';
        ApexPages.StandardController sc = new ApexPages.standardController(d);
    
    
        MassUpdateActionPlanforAlldeals_class  batch = new MassUpdateActionPlanforAlldeals_class (d.id,temp.id);
        Id batchId = Database.executeBatch(batch,1); 
    
    }
    
    
 }