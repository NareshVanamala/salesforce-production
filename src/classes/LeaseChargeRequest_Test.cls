@isTest(SeeAllData = true)
private class LeaseChargeRequest_Test {
 static testMethod void testDoGet() {
    MRI_PROPERTY__c mriPropobj =new MRI_PROPERTY__c();
    mriPropobj = [Select id from MRI_PROPERTY__c limit 1];
    Lease__c leaseobj =new Lease__c();
    leaseobj.name = 'Test Lease';
    leaseobj.Lease_ID__c= 'test12345';
    leaseobj.MRI_PROPERTY__c = mriPropObj.id;
    leaseobj.Lease_Status__c='Active';
    leaseobj.SuitVacancyIndicator__c =FALSE;
    leaseobj.MRI_PM__c = userinfo.getuserid();
    insert leaseobj;
    
    System.assertEquals(leaseobj.MRI_PROPERTY__c, mriPropObj.id);
    
    ApexPages.CurrentPage().getparameters().put('cid', mriPropobj.id);
    ApexPages.StandardController StandardsObjectController =  new ApexPages.StandardController(mriPropobj);
    LeaseChargeRequest leaseChargeReq  = new LeaseChargeRequest(StandardsObjectController);
    leaseChargeReq.newlease = leaseobj.id;
    leaseChargeReq.getLeases();
    leaseChargeReq.save();
    }
}