@isTest
                                
private class LC_ScheduleReminderEmail_Test 
{
   /* private static testmethod void test1() 
    {
        Test.StartTest();
        LC_ScheduleReminderEmail sh1 = new LC_ScheduleReminderEmail();
        String sch = '0 0 23 * * ?';
        system.schedule('Test check', sch, sh1); 
        Test.stopTest();
    }
    */
    
    static testMethod void myUnitTest() {

            Test.startTest();
                    String CRON_EXP = '0 0 0 1 1 ? 2050';  
                    String jobId = System.schedule('testScheduledApex', CRON_EXP, new LC_ScheduleReminderEmail() );

                    CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];

                    System.assertEquals(CRON_EXP, ct.CronExpression); 
                    System.assertEquals(0, ct.TimesTriggered);
                    System.assertEquals('2050-01-01 00:00:00', String.valueOf(ct.NextFireTime));

            Test.stopTest();
    }
}