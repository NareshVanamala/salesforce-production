public with sharing class RC_New_ClaimButtonClass
{
    public String recortypeid{get;set;}
    public String pid{get;set;}
    public RC_Claim__c mp{get;set;}
    public RC_Claim__c ProjectObj{get;set;}
    public RC_Location__c ppc{get;set;}
    
  Public RC_New_ClaimButtonClass(Apexpages.StandardController controller)
  {
     recortypeid=ApexPages.CurrentPage().getParameters().get('RecordType');
     mp= (RC_Claim__c)controller.getRecord();
     pid=mp.id;
   }
  
   public PageReference Gobacktostandardpage() 
   {  
     if((mp.Location__c==null))
     {
       //String Url=System.Label.Current_Org_Url+'/a5D/e?nooverride=1&retURL=%2Fa5D%2Fo&RecordType='+mp.RecordTypeId+'&ent=01IR00000005onK';
    String Url= System.Label.Current_Org_Url+'/a4X/e?nooverride=1&retURL=%2Fa4X%2Fo&RecordType='+mp.RecordTypeId+'&ent=01I500000003hbg';
       system.debug('Url'+Url); 
      PageReference acctPage = new PageReference(Url);
       acctPage.setRedirect(true); 
       return acctPage; 
     }  
     else if((mp.Location__c!=null))
     {
        
         system.debug('Check the Location'+mp.Location__c);
         string bid1;
         string bid=mp.Location__c;
         system.debug('Check the Location'+bid);
         ppc=[select id, Name,Account__c,Account__r.name from RC_Location__c where id=:bid];
         if(ppc!=null)
         bid1=ppc.Name;
         string accountid=ppc.Account__c;
         String accountName=ppc.Account__r.name;
         accountName=accountName.replaceAll('&','%26');
         String s2 = bid1.replaceAll('&', '%26');
         //String Url1=System.Label.Current_Org_Url+'/a5D/e?nooverride=1&CF00NR0000001JRV9_lkold='+bid+'&CF00NR0000001JRV9='+s2+'&CF00NR0000001JRST_lkold='+accountid+'&CF00NR0000001JRST='+accountName+'&retURL=%2F'+ppc.id+'&RecordType='+mp.RecordTypeId+'&ent=01IR00000005onJ';
       String Url1=System.Label.Current_Org_Url+'/a4X/e?nooverride=1&CF00N50000003LzlV_lkid='+bid+'&CF00N50000003LzlV='+s2+'&CF00N50000003Lzin_lkid='+accountid+'&CF00N50000003Lzin='+accountName+'&retURL=%2F'+ppc.id+'&RecordType='+mp.RecordTypeId+'&ent=01I500000003hbg';

        
         PageReference acctPage = new PageReference(Url1);
         acctPage.setRedirect(true); 
         return acctPage; 
     }
     return null;
  }
  
}