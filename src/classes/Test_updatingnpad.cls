@isTest
public class Test_updatingnpad
 {
    static testmethod void Test_updatingnpad()
    {
       Id rrrecordtype = [select Id, Name,DeveloperName from RecordType where DeveloperName ='NonInvestorRepContact' and SobjectType = 'Contact'].id;
        Account a = new Account();
       a.name = 'Test';
       insert a;
       
      Contact c = new Contact();
       c.lastname = 'Cockpit';
       c.Accountid = a.id;
       c.Relationship__c ='Accounting';
       c.Contact_Type__c = 'Cole';
       c.Wholesaler_Management__c = 'Producer A';
       c.Territory__c ='Chicago';
       c.RIA_Territory__c ='Southeast Region';
       c.RIA_Consultant_Picklist__c ='Brian Mackin';
       c.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c.Wholesaler__c='Andrew Garten';
       c.Regional_Territory_Manager__c ='Andrew Garten';
       c.Internal_Wholesaler__c ='Aaron Williams';
       c.Territory_Zone__c = 'NV-50';
       c.recordtypeid = rrrecordtype;
       c.Priority__c='1';
       c.Next_Planned_External_Appointment__c = null; 
       c.Next_External_Appointment__c = null; 
       insert c;
       
       c.Next_Planned_External_Appointment__c = system.now() + 30; 
        update c;
        
       system.assertequals(c.Accountid,a.id);

       Contact c1 = new Contact();
       c1.lastname = 'Cockpit';
       c1.Accountid = a.id;
       c1.Relationship__c ='Accounting';
       c1.Contact_Type__c = 'Cole';
       c1.Wholesaler_Management__c = 'Producer A';
       c1.Territory__c ='Chicago';
       c1.RIA_Territory__c ='Southeast Region';
       c1.RIA_Consultant_Picklist__c ='Brian Mackin';
       c1.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c1.Wholesaler__c='Andrew Garten';
       c1.Regional_Territory_Manager__c ='Andrew Garten';
       c1.Internal_Wholesaler__c ='Aaron Williams';
       c1.Territory_Zone__c = 'NV-50';
       c1.recordtypeid = rrrecordtype;
       c1.Priority__c='1';
       c1.Next_Planned_External_Appointment__c = system.now() + 1; 
       c1.Next_External_Appointment__c = null; 
       insert c1;
       c1.Priority__c='2';
       update c1;
        system.assertequals(c1.Accountid,a.id);

       
       Contact c2 = new Contact();
       c2.lastname = 'Cockpit';
       c2.Accountid = a.id;
       c2.Relationship__c ='Accounting';
       c2.Contact_Type__c = 'Cole';
       c2.Wholesaler_Management__c = 'Producer A';
       c2.Territory__c ='Chicago';
       c2.RIA_Territory__c ='Southeast Region';
       c2.RIA_Consultant_Picklist__c ='Brian Mackin';
       c2.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c2.Wholesaler__c='Andrew Garten';
       c2.Regional_Territory_Manager__c ='Andrew Garten';
       c2.Internal_Wholesaler__c ='Aaron Williams';
       c2.Territory_Zone__c = 'NV-50';
       c2.recordtypeid = rrrecordtype;
       c2.Priority__c='1';
       c2.Next_Planned_External_Appointment__c = system.now() + 1; 
       c2.Next_External_Appointment__c = null; 
       insert c2;
       c2.Priority__c='3';
       update c2;
       system.assertequals(c2.Accountid,a.id);

       
       REIT_Investment__c R = new REIT_Investment__c();
       R.Rep_Contact__c = c.id;
       R.Deposit_Date__c = System.today().adddays(90);
       insert R;
        
       
       
     }
       }