@isTest(SeeAllData=true)

Public class AC_EMPaproval_Test 
{

     static testMethod void testApprovalSuccess()
    {
    
        list<Employee__c >emplist=new list<Employee__c >();
        
         AC_Contractor_Extension__c ec=AC_Contractor_Extension__c.getvalues('AC_EmpType');
        string Emptypelist=ec.Employee_Type__c;
       
        /*User U= new User();
        U.firstname='Snehal';
        U.lastname='Kalamkar';
        U.email='skalamkarr@colecapital.com';
        insert U;*/
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', FirstName='Snehal',lastName='Kalamkar', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='skalamkar@testorg.com');
        
        insert u;
        
        system.debug('This is the name of the user'+u);
        
        Employee__c ect= new Employee__c();
        ect.First_Name__c='Snehal';
        ect.Last_Name__c='Kalamkar';
        ect.Employee_Status__c='On Board';
        ect.name='Snehal kalamkar';
        ect.HR_Manager_Status__c='Approved';
        insert ect;
        system.debug('This is supervisor'+ect);
        
        
        
        Employee__c ect1= new Employee__c();
        ect1.First_Name__c='Neha';
        ect1.Last_Name__c='Kalamkar';
        ect1.Employee_Status__c='On Board';
        ect1.HR_Manager_Status__c='Approved';
        ect1.Employee_Type__c='External Contractor';
        ect1.MGR__c=ect.id;
         
        //insert ect1;  
        emplist.add(ect1);
        //insert emplist;
        
        
        
        Employee__c ect11= new Employee__c();
        ect11.First_Name__c='Neha1';
        ect11.Last_Name__c='Kalamkar1';
        ect11.Employee_Status__c='On Board';
        ect11.HR_Manager_Status__c='Approved';
        ect11.Employee_Type__c='External Contractor';
        ect11.MGR__c=ect.id;
        
       // ect11.Reminder_Email_Date__c=system.today();
       ect11.Start_Date__c=system.today()-80;
       ect11.Supervisor_Lookup__c=u.id;
        //insert ect1;  
        emplist.add(ect11);
        
        
        Employee__c ect12= new Employee__c();
        ect12.First_Name__c='Neha11';
        ect12.Last_Name__c='Kalamkar11';
        ect12.Employee_Status__c='On Board';
        ect12.HR_Manager_Status__c='Approved';
        ect12.Employee_Type__c='External Contractor';
        ect12.MGR__c=ect.id;
        ect12.Contractor_renewal_Status__c='Submit for Approval';
        ect12.Supervisor_Lookup__c=u.id;
       // ect11.Reminder_Email_Date__c=system.today();
       ect12.Start_Date__c=system.today()-85;
        
          emplist.add(ect12);
          
        insert emplist;
        
        System.assertNotEquals(ect11, ect12);
        
        system.debug('This is my emplist'+emplist);
        list<Employee__c>mytestemplist=[SELECT MGR__r.Last_Name__c,MGR__r.First_Name__c,MGR__r.Employee_Status__c,Employee_Status__c,HR_Manager_Status__c,AC_Send_reminder_email__c,Id,Send_Termination_Email__c,MGR__c,MGR__r.name,Name,Testing_SystemToday__c,Supervisor__c,Supervisor_Lookup__r.name,Employee_Approval__c,Reminder_Email_Date__c,Termination_Email_Date__c,Employee_Type__c,Contractor_renewal_Status__c,Start_Date__c,First_Name__c,Last_Name__c from Employee__c where MGR__c=:ect.id];
      
        system.debug('Thiss is the list'+mytestemplist) ;          
          AC_EMPaproval np = new AC_EMPaproval ();
          np.scope=mytestemplist;
         
         // system.debug('chcek the scope'+np.scope[0].First_Name__c);
          Database.BatchableContext bc;
         np.start(bc);
          np.execute(bc,mytestemplist);
         np.finish(bc);
        
   }



}