global with sharing class ContactLifeCyclesnehal {

    public Contact con{get;set;}
    public String colorRadio {set;get;}
    //blic string cid; 
    //blic string colorRGB{set;get;}    
    
    
    public ContactLifeCyclesnehal (ApexPages.StandardController controller) {    
        System.debug('Loading ContactLifeCyclesnehal  Class'); 
        con =[select id, name, Contact_Life_Cycle__c, ColorRBG__c,Advisor_Pipeline__c from Contact where id = :ApexPages.currentPage().getParameters().get('id')];
        colorRadio=con.ColorRBG__c;
        System.debug('con.ColorRBG__c===>'+con.ColorRBG__c);
        if(con.ColorRBG__c=='Red-Yellow-Green-NQ')
         con.Advisor_Pipeline__c='Red';
        else if(con.ColorRBG__c=='Green-Red-Yellow-NQ')
        con.Advisor_Pipeline__c='Green';
         else if(con.ColorRBG__c=='Yellow-Red-Green-NQ')
        con.Advisor_Pipeline__c='Yellow';
         else if(con.ColorRBG__c=='NQ-Red-Yellow-Green')
          con.Advisor_Pipeline__c='NQ';
     //This is to update the taks advisor pipeline field.....//
      /*  list<task>tlist=new list<Task>(); 
          tlist=[select id,Advisor_Pipeline__c,whoid from task where whoid=:con.id ];
          system.debug('get the listof tasks....'+tlist.size());
          if(tlist.size()>0)
            {
              for(Task t:tlist) 
              {
               if(con.ColorRBG__c=='Red-Yellow-Green-NQ')
               t.Advisor_Pipeline__c='Red';
              else if(con.ColorRBG__c=='Green-Red-Yellow-NQ')
               t.Advisor_Pipeline__c='Green';
               else if(con.ColorRBG__c=='Yellow-Red-Green-NQ')
               t.Advisor_Pipeline__c='Yellow';
               else if(con.ColorRBG__c=='NQ-Red-Yellow-Green')
               t.Advisor_Pipeline__c='NQ';
               update t;
               
             }
          }*/
     }
     
/*public String saveChanges()
 { 
   Contact c  =[select id, name, Contact_Life_Cycle__c, ColorRBG__c from Contact where id = :ApexPages.currentPage().getParameters().get('id')];
   colorRGB='NQ-Red-Green-Yellow'; 
   if(c!=null){
   c.ColorRBG__c = colorRGB;
     update c;
        }        
        System.debug('*****c.ColorRBG__c'+c.ColorRBG__c);
        return c.ColorRBG__c;

 }*/
  
   
    @RemoteAction
    global static String Conupdate(String cId,String colorRGB){
        System.debug('&&&&&&&&&&&CID&&&&&&&'+cId);
        System.debug('&&&&&&&&&&&colorRGB&&&'+colorRGB);    
        Contact c  =[select id, name, Contact_Life_Cycle__c,Advisor_Pipeline__c, ColorRBG__c from Contact where id = :cId];
        if(c!=null){
            c.ColorRBG__c = colorRGB;
            if(colorRGB=='Red-Yellow-Green-NQ')
             c.Advisor_Pipeline__c='Red';
            else if(colorRGB=='Green-Red-Yellow-NQ')
             c.Advisor_Pipeline__c='Green';
            else if(colorRGB=='Yellow-Red-Green-NQ')
             c.Advisor_Pipeline__c='Yellow';
             else if(colorRGB=='NQ-Red-Yellow-Green')
             c.Advisor_Pipeline__c='NQ';
            update c;
                    
        } 
        //This is to update the taks advisor pipeline field.....//
        /* list<task>tlist1=new list<Task>(); 
          tlist1=[select id,Advisor_Pipeline__c,whoid from task where whoid=:cId ];
          system.debug('get the listof tasks....'+tlist1.size());
          if(tlist1.size()>0)
            {
              for(Task t:tlist1) 
              {
               if(colorRGB=='Red-Yellow-Green-NQ')
               t.Advisor_Pipeline__c='Red';
               if(colorRGB=='Green-Red-Yellow-NQ')
               t.Advisor_Pipeline__c='Green';
               if(colorRGB=='Yellow-Red-Green-NQ')
               t.Advisor_Pipeline__c='Yellow';
               if(colorRGB=='NQ-Red-Yellow-Green')
               t.Advisor_Pipeline__c='NQ';
               update t;
             }
          }
               */
        System.debug('*****c.ColorRBG__c'+c.ColorRBG__c);
        return c.ColorRBG__c;
    }    

}