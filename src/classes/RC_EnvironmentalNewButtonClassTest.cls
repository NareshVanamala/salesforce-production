@isTest
private class RC_EnvironmentalNewButtonClassTest
{
    static testmethod void test1() 
    {
       RC_Environmental__c ct=new RC_Environmental__c();
       String Url=System.Label.Current_Org_Url+'/a5E/e?nooverride=1&retURL=%2Fa5E%2Fo';
       PageReference acctPage = new PageReference(Url);
       Test.setCurrentPage(acctPage); 
       
       ApexPages.StandardController controller = new ApexPages.StandardController(ct);
       RC_Environmental_New_ButtonClass csr=new RC_Environmental_New_ButtonClass(controller);
      
       csr.Gobacktostandardpage();
       
            
         RC_Property__c PRT=new RC_Property__c();
         PRT.name='Amazon DC 2014 USAA~A11123';
         PRT.City__c='Hyderabad';
         insert PRT;
         
         Account a= new Account();
         a.name='TestAccount';
         insert a;

          System.assertNotEquals(PRT.name,a.Name);
          
         RC_Location__c LC=new RC_Location__c();
         LC.name='Amazon DC 2014 USAA~A11123';
         LC.Entity_Tenant_Code__c='12345';
         //LC.City__c='Hyderabad';
         LC.Property__c=PRT.id;
         LC.Account__c=a.id;
         insert LC;
         System.assertEquals(LC.Account__c,a.id);

         string bid=LC.id;
         string bid1=LC.Name;
         string accountid=LC.Account__c;
         String accountName=LC.Account__r.name;
         //accountName=accountName.replaceAll('&','%26');
         String s2 = bid1.replaceAll('&', '%26');
        String Url1=System.Label.Current_Org_Url+'/a5E/e?nooverride=1&CF00NR0000001JRXX_lkold='+bid+'&CF00NR0000001JRXX='+s2+'&CF00NR0000001JRX8_lkold='+accountid+'&CF00NR0000001JRX8='+accountName+'&retURL=%2F'+LC.id+'';
        RC_Environmental__c ct1=new RC_Environmental__c();
       
        ApexPages.StandardController controller1 = new ApexPages.StandardController(ct1);
       RC_Environmental_New_ButtonClass csr1=new RC_Environmental_New_ButtonClass(controller1);
       csr1.ppc=LC;
       csr1.mp=ct1;
        PageReference acctPage1 = new PageReference(Url1);
       Test.setCurrentPage(acctPage1); 
       csr1.mp.Location__c=LC.id;
       csr1.Gobacktostandardpage();
       
     }
     
  }