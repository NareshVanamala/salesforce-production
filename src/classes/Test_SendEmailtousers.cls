@isTest(seeAlldata=true)
private class Test_SendEmailtousers{
        static testMethod void TestsendEmailtoUsers() {

        test.startTest();
        Deal__c d1 =new Deal__c();
        d1.name = 'deal';
        d1.Property_Type__c = 'Retail';
        d1.Tenancy__c= 'Multi-Tenant';
        d1.Primary_Use__c = 'Anchor';
        insert d1;
        
        Template__c temp =new Template__c();
        temp.name = 'test';
        temp.Deal__c = d1.id;
        insert temp;
                
        Messaging.reservesingleEmailCapacity(1);
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

        Task_plan__c tp= new Task_plan__c();
        Tp.Name = 'Test';
        Tp.priority__c = 'Medium';
        Tp.Notify_Emails__c = 'test@test.com';
        Tp.Template__c = temp.id;
        Tp.Date_received__c = System.today();
        Tp.Assigned_To__c = '00550000001j5tyAAA';
        Tp.Status__c = 'Not Started';
        insert Tp;
        system.assertequals( Tp.Template__c,temp.id);

        //Tp.Status__c = 'Completed';
                
        //update tp;

        If( Tp.Date_received__c  !=null && Tp.Notify_Emails__c!=null)
        {

                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

                email.setToAddresses(new String[] {Tp.Notify_Emails__c});
                email.setReplyTo('Pratyush.reddy@colereit.com');
                email.setSenderDisplayName('Action Plan Tasks');
                email.setSubject('Action Plan Task');
                email.setPlainTextBody('The task');
                emails.add(email);
        }
        test.stopTest();
    }
}