@isTest
Public class LC_RemainingOptions_Test
{
   static testmethod void testCreatelease()
  {
     test.starttest();
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.name='Test_MRI_Property';
         insert mc; 
         system.debug('This is my MRI Property'+mc);
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU';
         mylease.Suite_Sqft__c=1234;
         mylease.Lease_ID__c='A046677';
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         system.debug('This is my Lease'+mylease);

         LC_LeaseOpprtunity__c lc1= new LC_LeaseOpprtunity__c();
         lc1.name='TestOpportunity';
         lc1.Lease_Opportunity_Type__c='Lease - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.LC_OtherOpportunity_Description__c='Thisis my description';
         lc1.Remaining_Options_Intact__c=true;
         lc1.Base_Rent__c=12345;
         insert lc1;
         system.debug('This is my lease Opportunity'+lc1);
         
         
          System.assertEquals(myLease.MRI_PROPERTY__c , mc.id);
          System.assertEquals( lc1.name,'TestOpportunity');
 
          ApexPages.currentPage().getParameters().put('ids',lc1.id);
          ApexPages.StandardController stdcon1 = new ApexPages.StandardController(lc1);
          LC_RemainingOptions LCcontroller1 = new LC_RemainingOptions (stdcon1);
          LCcontroller1.LeaseOpportunity=lc1; 
          LCcontroller1.ids=lc1.id;  
          LCcontroller1.AdviserCallList();

      test.stoptest();


    }
    
    
 }