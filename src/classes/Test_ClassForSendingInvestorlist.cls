@isTest
 private class Test_ClassForSendingInvestorlist
 {
    
       
    static testmethod void test() 
    {
       
        string Type='';
        string subtype='';
        String Templateid='';
        
        Investment_List_Email_details__c pdcemail=Investment_List_Email_details__c.getvalues('Email Attachment Details');
        //system.debug('This is it'+pdcemail);
       
        if(pdcemail!=null)
        {
          Type=pdcemail.Attachment_Type__c;
          subtype= pdcemail.Attachment_SubType__c;
          Templateid=pdcemail.Email_Template_Id__c;
          
         }
         
         else
         {
          Type='Investor List';
          subtype='PDF File';
          Templateid='00XR0000000ERK3';
         }
        
        
        list<Contact>clist=new list<Contact>();
        Account a = new Account();
        a.name = 'Test';
        Insert a; 
        
        Contact c1 = new Contact();
        c1.lastname = 'Cockpit';
        c1.firstname='test';
        c1.Accountid = a.id;
        c1.Relationship__c ='Accounting';
        c1.Contact_Type__c = 'Cole';
        c1.Wholesaler_Management__c = 'Producer A';
        c1.Territory__c ='Chicago';
        c1.RIA_Territory__c ='Southeast Region';
        c1.RIA_Consultant_Picklist__c ='Brian Mackin';
        c1.Internal_Consultant_Picklist__c ='Ben Satterfield';
        c1.Wholesaler__c='Andrew Garten';
        c1.Regional_Territory_Manager__c ='Andrew Garten';
        c1.Internal_Wholesaler__c ='Keith Severson';
        c1.Territory_Zone__c = 'NV-50';
        c1.Next_Planned_External_Appointment__c = system.now();
       // c1.recordtypeid = rrrecordtype;
        c1.mailingstate='OH';
        c1.email='sandeepa.mariyala@polarisft.com';
        //insert c1;  
        clist.add(c1);
        
        Contact c = new Contact();
        c.lastname = 'Cockpit1';
        c.firstname='test1';
        c.Accountid = a.id;
        c.Relationship__c ='Accounting';
        c.Contact_Type__c = 'Cole';
        c.Wholesaler_Management__c = 'Producer A';
        c.Territory__c ='Chicago';
        c.RIA_Territory__c ='Southeast Region';
        c.RIA_Consultant_Picklist__c ='Brian Mackin';
        c.Internal_Consultant_Picklist__c ='Ben Satterfield';
        c.Wholesaler__c='Andrew Garten';
        c.Regional_Territory_Manager__c ='Andrew Garten';
        c.Internal_Wholesaler__c ='Keith Severson';
        c.Territory_Zone__c = 'NV-50';
        c.Next_Planned_External_Appointment__c = system.now();
       // c1.recordtypeid = rrrecordtype;
        c.mailingstate='OH';
        c.email='sandeep.mariyala@polarisft.com';
        clist.add(c); 
        insert clist;
        
        Investor_Attachment__c act= new Investor_Attachment__c();
        act.Sub_Type__c='PDF File';
        act.Type__c ='Investor List';
        act.Contact__c=c1.id;
        insert act;
        
        Campaign cam = new Campaign();
        cam.name='Dinearound-National Conference-Cole Capital'; 
        cam.status='Pending'; 
        cam.status='Approved'; 
        cam.status='Rejected'; 
        cam.isActive=true;
        cam.parentid=null;
        insert cam;    
       
        CampaignMember member1= new CampaignMember();
        member1.CampaignId = cam.id;
        member1.ContactId = c1.id; 
        member1.Status='Approved';
        insert member1;
        
        
        CampaignMember member11= new CampaignMember();
        member11.CampaignId = cam.id;
        member11.ContactId = c.id; 
        member11.Status='Approved';
        insert member11;
        
        
       
        Test.startTest();
        ClassForSendingInvestorlist np = new ClassForSendingInvestorlist(cam.id);
        Database.BatchableContext bc;
    
        np.start(bc);
        np.execute(bc,clist);
        np.finish(bc);
        Test.stopTest();     
   }
   
}