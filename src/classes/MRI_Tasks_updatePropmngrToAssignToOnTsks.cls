global class MRI_Tasks_updatePropmngrToAssignToOnTsks{

   @InvocableMethod
   public static void MRI_Tasks_updatePropmngrToAssignToOnTsks(list<string>TaskIds){
   list<User>OpenTaskslist=new list<User>();
   list<task>updateTaskList = new list<task>();
   list<MRI_PROPERTY__c>MRIPropList = new list<MRI_PROPERTY__c>();
   set<String>MRIPropSet=new set<String>(); 
   list<Task>taskList = new list<Task>();
   Map<id,id>mapPropMangerToAssignedTo=new Map<id,id>();
   Map<id,string>mapBuildingID=new Map<id,string>();
     taskList = [Select Id,whatid from task where id in:TaskIds]; 
     system.debug('taskList'+taskList);
      for(task tsk: taskList){
            MRIPropSet.add(tsk.whatid);
         }
     MRIPropList = [select id,Property_Manager__c,Property_ID__c from MRI_PROPERTY__c where id in:MRIPropSet];
     if(MRIPropList.size()>0){
        for(MRI_PROPERTY__c MRI:MRIPropList){
           mapPropMangerToAssignedTo.put(MRI.id,MRI.Property_Manager__c);
           mapBuildingID.put(MRI.id,MRI.Property_ID__c);
           system.debug('MRIPropList'+MRIPropList);
        } 
        for(task tsk:taskList){
            if((tsk.whatid!=null)&&(mapPropMangerToAssignedTo.get(tsk.whatid)!=null))
              tsk.ownerId=mapPropMangerToAssignedTo.get(tsk.whatid); 
            if((tsk.whatid!=null)&&(mapBuildingID.get(tsk.whatid)!=null))
              tsk.Building_ID__c=mapBuildingID.get(tsk.whatid);           
   
            updateTaskList.add(tsk); 
            system.debug('MRIPropList'+MRIPropList); 
                                 
        }   
     }     
         if(updateTaskList.size()>0)
           update updateTaskList;
    
}
}