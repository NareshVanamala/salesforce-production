@isTest
public class Cockpit_updateLastActivityDate_Test 
{
    static testmethod void myTest_Method1()
    {
        Test.StartTest();
          Contact cockpitcontact1= new Contact();
         cockpitcontact1.firstname='cockpitcontact1FirstName';
         cockpitcontact1.lastname='cockpitcontact1lastName';
         cockpitcontact1.Territory__c='HeartLand';
         cockpitcontact1.Phone='4802375787'; 
         insert cockpitcontact1;
         
         System.assert(cockpitcontact1!= null);
         
         system.debug(cockpitcontact1);
             
         Contact cockpitcontact11= new Contact();
         cockpitcontact11.firstname='cockpitcontact11FirstName';
         cockpitcontact11.lastname='cockpitcontact11lastName';
         cockpitcontact11.Territory__c='Chesapeake';
         cockpitcontact11.Phone='4802375797'; 
         insert cockpitcontact11;   
         
         System.assert(cockpitcontact11!= null);
         system.debug(cockpitcontact11);  
         
         Automated_Call_List__c CockpitAutomatedCalllist= new Automated_Call_List__c();
         CockpitAutomatedCalllist.name='CockpitAutomatedcallListName';
         CockpitAutomatedCalllist.Type_Of_Call_List__c='Campaign Call List';
         insert CockpitAutomatedCalllist;
       
         System.assert(CockpitAutomatedCalllist!= null);
       
         Contact_Call_List__c cockpitCCL=new Contact_Call_List__c();
         cockpitCCL.Automated_Call_List__c=CockpitAutomatedCalllist.id;
         cockpitCCL.contact__c=cockpitcontact1.id;
         insert cockpitCCL;
         system.debug(cockpitCCL);
         
         System.assert(cockpitCCL!= null);
         
         Profile p = [SELECT Id FROM Profile WHERE Name='Internal Sales']; 
         User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testingmyapplication', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='mytestuser@testorg.com');

          System.runAs(u) {
            // The following code runs as user 'u' 
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId());
             
          task t =new task();
          t.status='Completed';
          t.activitydate=system.today();
          t.whoid= cockpitcontact1.id;
          t.ownerid=u.id;  
          insert t;  
          system.debug('This is the task id'+t); 
          
          System.assert(t!= null);
          
          task tt =new task();
          tt.status='New';
          tt.activitydate=system.today()-1;
          tt.whoid= cockpitcontact1.id;
          tt.ownerid=u.id;  
          insert tt;  
          system.debug('This is the new task id'+tt); 
          
          System.assert(tt!= null);
        
       
    
    
    
    
    }
  
  
  }
  
}