global class CreateREITProxyVotes implements Database.Batchable<SObject>{
 
global final String Query;  
global list<REIT_Proxy_Votes__c>Insertlist;

global Database.QueryLocator start(Database.BatchableContext BC)  
{  
  string query='select id, name, Account__c,Current_Capital__c,Current_Units__c,Fund__c, Rep_Contact__c,Investor_Name__c,Original_Capital__c from REIT_Investment__c';
  system.debug('This is start method');
  system.debug('Myquery is......'+Query);
   return Database.getQueryLocator(query);  
}  


global void execute(Database.BatchableContext BC,List<REIT_Investment__c> scope)  
{  
    Insertlist = new List<REIT_Proxy_Votes__c>();
    System.debug('Size*****' + scope.size());
    for(REIT_Investment__c ccl: scope)
    {
         REIT_Proxy_Votes__c RV= new REIT_Proxy_Votes__c();
         RV.Investor__c=ccl.Investor_Name__c;
         RV.Fund__c=ccl.Fund__c;
         RV.Contact__c=ccl.Rep_Contact__c;
         RV.Amount__c=ccl.Original_Capital__c;
         RV.Shares__c=ccl.Current_Units__c;
         RV.DST_Account_Number__c=ccl.Account__c;
         RV.REIT_Investemnt_id__c=ccl.id;
       Insertlist.add(RV);
    }
    
   if (Schema.sObjectType.REIT_Proxy_Votes__c.isCreateable()) 
   insert Insertlist;
    
}

global void finish(Database.BatchableContext BC)  
{ 


   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
   String[] toAddresses =new String[] {'skalamkar@colecapital.com'};
   String[] BccAddresses = new String[] {'skalamkar@colecapital.com'};
   mail.setToAddresses(toAddresses);
   mail.setBccSender(true);
   mail.setBccAddresses(bccAddresses);
   mail.setSubject('Created REIT Proxy Records');
   mail.setPlainTextBody
   ('Batch class finished' );
   Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
   
}
}