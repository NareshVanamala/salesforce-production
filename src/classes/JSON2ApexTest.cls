@isTest
 public class JSON2ApexTest {
 
      static testmethod void testJson() {
        String json = '{\"d\":{\"results\":[{\"metadata\":{\"id\":\"http://data.snl.com/IRWebAPI/V1/PressReleases/PressReleases(13281131)\",\"uri\":\"http://data.snl.com/IRWebAPI/V1/PressReleases/PressReleases(13281131)\",\"type\":\"SNL.SNLWebX.IRWebAPI.Common.Models.PressReleases.PressReleaseEntity\"},\"KeyDoc\":13281131,\"KeyFile\":11711596,\"FilingType\":\"Press Release\",\"Title\":\"American Realty Capital Properties Executive Management Team to Ring the Bell on NASDAQ\",\"ShortDescription\":null,\"FullText\":null,\"ReleaseDateTime\":\"2011-09-08T17:44:00\",\"ReleaseDateTimeFormatted\":\"9/8/2011\",\"ReleaseDay\":8,\"ReleaseMonth\":9,\"ReleaseYear\":2011,\"ReleaseTime\":\"17:44:00\",\"ReleaseType\":null,\"Format\":\"XML\",\"RelatedDocs\":{\"metadata\":{\"type\":\"Collection(SNL.SNLWebX.IRWebAPI.Common.Models.Common.RelatedDoc)\"},\"results\":[]}},{\"metadata\":{\"id\":\"http://data.snl.com/IRWebAPI/V1/PressReleases/PressReleases(13296707)\",\"uri\":\"http://data.snl.com/IRWebAPI/V1/PressReleases/PressReleases(13296707)\",\"type\":\"SNL.SNLWebX.IRWebAPI.Common.Models.PressReleases.PressReleaseEntity\"},\"KeyDoc\":13296707,\"KeyFile\":11727727,\"FilingType\":\"Press Release\",\"Title\":\"American Realty Capital Repays Investors $51.6 Million by Harvesting Three Note Programs and an Equity Fund\",\"ShortDescription\":null,\"FullText\":null,\"ReleaseDateTime\":\"2011-09-13T12:07:00\",\"ReleaseDateTimeFormatted\":\"9/13/2011\",\"ReleaseDay\":13,\"ReleaseMonth\":9,\"ReleaseYear\":2011,\"ReleaseTime\":\"12:07:00\",\"ReleaseType\":null,\"Format\":\"XML\",\"RelatedDocs\":{\"metadata\":{\"type\":\"Collection(SNL.SNLWebX.IRWebAPI.Common.Models.Common.RelatedDoc)\"},\"results\":[]}},{\"metadata\":{\"id\":\"http://data.snl.com/IRWebAPI/V1/PressReleases/PressReleases(13536161)\",\"uri\":\"http://data.snl.com/IRWebAPI/V1/PressReleases/PressReleases(13536161)\",\"type\":\"SNL.SNLWebX.IRWebAPI.Common.Models.PressReleases.PressReleaseEntity\"},\"KeyDoc\":13536161,\"KeyFile\":11972632,\"FilingType\":\"Press Release\",\"Title\":\"American Realty Capital Properties, Inc. Announces Pricing of Follow-On Offering of Common Stock\",\"ShortDescription\":null,\"FullText\":null,\"ReleaseDateTime\":\"2011-10-28T09:21:00\",\"ReleaseDateTimeFormatted\":\"10/28/2011\",\"ReleaseDay\":28,\"ReleaseMonth\":10,\"ReleaseYear\":2011,\"ReleaseTime\":\"09:21:00\",\"ReleaseType\":null,\"Format\":\"XML\",\"RelatedDocs\":{\"metadata\":{\"type\":\"Collection(SNL.SNLWebX.IRWebAPI.Common.Models.Common.RelatedDoc)\"},\"results\":[]}}]}}';
        JSON2Apex r = JSON2Apex.parse(json);
        System.assert(r != null);
        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        JSON2Apex.d objd = new JSON2Apex.d(System.JSON.createParser(json));
        System.assert(objd != null);
        System.assert(objd.results == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        JSON2Apex.metadata_Z objmetadata_Z = new JSON2Apex.metadata_Z(System.JSON.createParser(json));
        System.assert(objmetadata_Z != null);
        System.assert(objmetadata_Z.type == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        JSON2Apex objRoot = new JSON2Apex(System.JSON.createParser(json));
        System.assert(objRoot != null);
        System.assert(objRoot.d == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        JSON2Apex.Results_Z objResults_Z = new JSON2Apex.Results_Z(System.JSON.createParser(json));
        System.assert(objResults_Z != NULL);
        System.assert(objResults_Z.metadata == null);
        System.assert(objResults_Z.KeyDoc == null);
        System.assert(objResults_Z.KeyFile == null);
        System.assert(objResults_Z.FilingType == null);
        System.assert(objResults_Z.Title == null);
        System.assert(objResults_Z.ShortDescription == null);
        System.assert(objResults_Z.FullText == null);
        System.assert(objResults_Z.ReleaseDateTime == null);
        System.assert(objResults_Z.ReleaseDateTimeFormatted == null);
        System.assert(objResults_Z.ReleaseDay == null);
        System.assert(objResults_Z.ReleaseMonth == null);
        System.assert(objResults_Z.ReleaseYear == null);
        System.assert(objResults_Z.ReleaseTime == null);
       // System.assert(objResults_Z.ReleaseType == null);
        System.assert(objResults_Z.Format == null);
        System.assert(objResults_Z.RelatedDocs == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        JSON2Apex.RelatedDocs objRelatedDocs = new JSON2Apex.RelatedDocs(System.JSON.createParser(json));
        System.assert(objRelatedDocs != null);
        System.assert(objRelatedDocs.metadata == null);
        System.assert(objRelatedDocs.results == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        JSON2Apex.Results objResults = new JSON2Apex.Results(System.JSON.createParser(json));
        System.assert(objResults != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        JSON2Apex.metadata objmetadata = new JSON2Apex.metadata(System.JSON.createParser(json));
        System.assert(objmetadata != null);
        System.assert(objmetadata.id == null);
        System.assert(objmetadata.uri == null);
        System.assert(objmetadata.type == null);
        }
    }