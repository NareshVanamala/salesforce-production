@isTest
public class Test_Block_dupe
{  
     static testmethod void myTest_Method1() 
     {
        Account a= new Account();
a.name='TestBrokerDealerAccount';
a.recordtypeid='012500000004uvF';
a.type='Broker Dealer' ;
insert a; 

Recordtype rr1= [select id, name from Recordtype where name='RIA' and sobjecttype='Account']; 
Account a1= new Account();
a1.name='TestRIAAccount';
a1.recordtypeid=rr1.id; 
insert a1;

//Recordtype RRcnt=[select id, name from Recordtype where name='NonInvestorRepContact' and sobjecttype='Contact']; 
Contact c= new Contact();
c.firstname='TestBrokerDealerContact'; 
c.lastname='testing';
c.accountid=a.id;
c.Rep_CRD__c='988120';
c.recordtypeid='012300000004rLn';
c.Contact_Type__c='S & E Contact';
c.Relationship__c='Attorney';
insert c; 

System.assertEquals(c.accountid,a.id);


//Recordtype Riacnt=[select id, name from Recordtype where name='Registered_Investment_Advisor_RIA_Contact' and sobjecttype='Contact']; 
Contact c1= new Contact();
c1.firstname='TestRiaContact'; 
c1.lastname='testing11';
c1.accountid=a1.id;
c1.Rep_CRD__c='988120';
c1.recordtypeid='012500000005Qqz';
insert c1; 

System.assertEquals(c1.accountid,a1.id);

Contact_Affiliation__c ca1= new Contact_Affiliation__c();
ca1.Contact_Name__c=c.id;
ca1.Account__c=a1.id;
string check= ca1.Contact_Name__c;
check=check+ca1.Account__c;
insert ca1;

Contact_Affiliation__c ca11= new Contact_Affiliation__c();
ca11.Contact_Name__c=c.id;
ca11.Account__c=a1.id;
string check1= ca11.Contact_Name__c;
check1=check1+ca11.Account__c;
//if(check
Boolean result=check1.equals(check);
if(result==false)
insert ca11;


Contact_Affiliation__c ca2= new Contact_Affiliation__c();
ca2.Contact_Name__c=c.id;
ca2.Account__c=a1.id;
string check2= ca2.Contact_Name__c;
check2=check2+ca2.Account__c;
try{
insert ca2;
}

catch (System.DmlException e) {
    
      
    }
    
    System.assertEquals(ca2.account__c,a1.id);


}
}