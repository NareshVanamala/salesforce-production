global virtual without sharing class ocms_arcp_properties extends cms.ContentTemplateController{

    private List<SObject> propertyList;
    private String html;
    public string webready='Approved';
  
    public set<string> programid=new set<string>();
    public Map<String, String> stateMap = new Map<String, String>(); 

    global ocms_arcp_properties() {
        
        programid.add('ARCP');
        programid.add('CCPT3');
        programid.add('CGPRED');
        programid.add('CVCRE1');
        programid.add('STFRED');
        programid.add('STFRE2');
        programid.add('CGPRE2');
      //  programid.add('CCPT4');
        stateMap.put('AL','Alabama');
        stateMap.put('AK','Alaska');
        stateMap.put('AZ','Arizona');
        stateMap.put('AR','Arkansas');
        stateMap.put('CA','California');
        stateMap.put('CO','Colorado');
        stateMap.put('CT','Connecticut');
        stateMap.put('DE','Delaware');
        stateMap.put('DC','District of Columbia');
        stateMap.put('FL','Florida');
        stateMap.put('GA','Georgia');
        stateMap.put('HI','Hawaii');
        stateMap.put('ID','Idaho');
        stateMap.put('IL','Illinois');
        stateMap.put('IN','Indiana');
        stateMap.put('IA','Iowa');
        stateMap.put('KS','Kansas');
        stateMap.put('KY','Kentucky');
        stateMap.put('LA','Louisiana');
        stateMap.put('ME','Maine');
        stateMap.put('MD','Maryland');
        stateMap.put('MA','Massachusetts');
        stateMap.put('MI','Michigan');
        stateMap.put('MN','Minnesota');
        stateMap.put('MS','Mississippi');
        stateMap.put('MO','Missouri');
        stateMap.put('MT','Montana');
        stateMap.put('NE','Nebraska');
        stateMap.put('NV','Nevada');
        stateMap.put('NH','New Hampshire');
        stateMap.put('NJ','New Jersey');
        stateMap.put('NM','New Mexico');
        stateMap.put('NY','New York');
        stateMap.put('NC','North Carolina');
        stateMap.put('ND','North Dakota');
        stateMap.put('OH','Ohio');
        stateMap.put('OK','Oklahoma');
        stateMap.put('OR','Oregon');
        stateMap.put('PA','Pennsylvania');
        stateMap.put('RI','Rhode Island');
        stateMap.put('SC','South Carolina');
        stateMap.put('SD','South Dakota');
        stateMap.put('TN','Tennessee');
        stateMap.put('TX','Texas');
        stateMap.put('UT','Utah');
        stateMap.put('VT','Vermont');
        stateMap.put('VA','Virginia');
        stateMap.put('WA','Washington');
        stateMap.put('WV','West Virginia');
        stateMap.put('WI','Wisconsin');
        stateMap.put('WY','Wyoming');
    } 
   
     public Integer getResultTotal(String state, String city, String tenantIndustry, String tenant, String[] filters){
        
        String query;
        
        query = 'SELECT COUNT() FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Program_Id__c in: programid  and Status__c=\'Owned\'';
        
        
        if(state != null && state != '' && state != 'all'){
            state = state.escapeEcmaScript();
            query += ' AND State__c = \'' + state + '\'';
        }

        if(city != null && city != '' && state != 'all' && city != 'all'){
            city = city.escapeEcmaScript();
            query += ' AND City__c = \'' + city + '\'';
        }

        if(tenantIndustry != null && tenantIndustry != '' && tenantIndustry != 'all'){
            tenantIndustry = tenantIndustry.escapeEcmaScript();
            query += ' AND Sector__c = \'' + tenantIndustry + '\'';
        }

        if(tenant != null && tenant != '' && tenant != 'all'){
            tenant = tenant.escapeEcmaScript();
            query += ' AND Name= \'' + tenant + '\'';
        }

        if(filters != null && filters.size() > 0){
            query += ' AND ( ';
            Boolean first = true;
            for(String f: filters){
                if(first){
                    first=false;
                }else{
                    query += ' OR ';
                }

                query += 'Property_Type_Group__c = \'' + f + '\'';

            }
            query += ' )';
        }

        return Database.countQuery(query);

    } 
    
       public List<SObject> getPropertyList(String state, String city, String tenantIndustry, String tenant, String[] filters, String sortBy, String rLimit, String offset){

        List<SObject> propertyList;
        String query;
        
        query = 'SELECT Common_name__C,Name,Web_Name__c, SqFt__c, City__c, State__c,Property_Type__c,Property_Type_Group__c FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Program_Id__c in: programid and Status__c = \'Owned\'';
        
        
        if(state != null && state != '' && state != 'all'){
            state = state.escapeEcmaScript();
            query += ' AND State__c = \'' + state + '\'';
        }

        if(city != null && city != '' && state != 'all' && city != 'all'){
            city = city.escapeEcmaScript();
            query += ' AND City__c = \'' + city + '\'';
        }

        if(tenantIndustry != null && tenantIndustry != '' && tenantIndustry != 'all'){
            tenantIndustry = tenantIndustry.escapeEcmaScript();
            query += ' AND Sector__c = \'' + tenantIndustry + '\'';
        }

        if(tenant != null && tenant != '' && tenant != 'all'){
            tenant = tenant.escapeEcmaScript();
            query += ' AND Web_Name__c= \'' + tenant + '\'';
        }

        if(filters != null && filters.size() > 0){
            query += ' AND ( ';
            Boolean first = true;
            for(String f: filters){
                if(first){
                    first=false;
                }else{
                    query += ' OR ';
                }

                query += 'Property_Type_Group__c = \'' + f + '\'';

            }
            query += ' )';
        }

        if(sortBy != null && sortBy != ''){
            if( sortBy == 'SqFt__c' ){
                query += ' ORDER BY ' + sortBy + ' ASC ';
            }else if( sortBy != 'SqFt__c' ){
                query += ' ORDER BY ' + sortBy + ' ASC ';
            } 
        }

     /*   if(rLimit != null && rLimit != ''){
            query += ' LIMIT ' + rLimit;
        }

        if(offset != null && offset != ''){
            query += ' OFFSET ' + offset;
       }*/
        propertyList = Database.query(query);
       
        return propertyList;
    }
    
  
    public String getStateList(){

        List<SObject> stateList;
        List<String> outList = new List<String>();
        String response = '';
        Boolean first = true;

        String query = 'SELECT State__c FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Program_Id__c in: programid and Status__c = \'Owned\' GROUP BY State__c ORDER BY State__c ASC';
        stateList = Database.query(query);
        stateList = sanitizeList(stateList, 'State__c');

        response+='[';

        for(SOBject s : stateList){
            if(first){
                first=false;
            }else{
                response+=', ';
            }
            response+='{"short":"' + String.valueOf(s.get('State__c')) + '", ';
            if(stateMap.get(String.valueOf(s.get('State__c'))) != null)
                response+='"long":"' + stateMap.get(String.valueOf(s.get('State__c'))) + '"}';
            else
                response+='"long":"' + String.valueOf(s.get('State__c')) + '"}';
        }

        response+=']';

        return response;

    }
    
    public List<SObject> getCityList(String state){

        List<SObject> cityList;

        String query = 'SELECT City__c FROM Web_Properties__c WHERE State__c = \'' + state + '\' and Web_Ready__c=\'' + webready + '\' and Program_Id__c in: programid and Status__c = \'Owned\' GROUP BY City__c ORDER BY City__c ASC';
        cityList = Database.query(query);

        return sanitizeList(cityList, 'City__c');

    }
   
    public List<SObject> getIndustryList(){

        List<SObject> industryList;

        String query = 'SELECT Sector__c FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Program_Id__c in: programid and Status__c = \'Owned\' GROUP BY Sector__c ORDER BY Sector__c ASC';
        industryList = Database.query(query);

        return sanitizeList(industryList, 'Sector__c');

    }
    
    public List<SObject> getTenantList(){

       List<SObject> tenantList;
       string query;
       query = 'SELECT Name FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Program_Id__c in: programid and Status__c = \'Owned\' and (NOT Property_Type_Group__c LIKE \'%Anchored Shopping Centers%\') GROUP BY Name';
             
       tenantList = Database.query(query);
       return sanitizeList(tenantList, 'Name');

    }
 
    private List<SObject> sanitizeList(List<SObject> inList, String field){

        Integer ctr = 0;

        while(ctr < inList.size()){
            if(inList[ctr].get(field) == null){
                inList.remove(ctr);
            } else {
                ctr++;
            }
        }
        return inList;

    }
 
    private String writeControls(){
        String html = '';
  
        html += '<div class="ocms_WebPropertyList">' +
                '   <div class="ocms_WP_Controls">' +
                '   <div class="ocms_WP_narrowResultsArea">' +
                '       <div class="ocms_WP_narrowResults">' +
                '           <a href="javascript:void(0)">Narrow Results</a>' +
                '       </div>' +
                '       <div class="ocms_WP_searchForm">' +
                '           <div class="ocms_WP_leftColumn">' +
                '               <div class="ocms_WP_narrowRadios">' +
                '                   <input class="byLocationRadio" type="radio" name="narrowBy" value="byLocation" > By Location</input>' +
                '                   <input class="byIndustryRadio" type="radio" name="narrowBy" value="byIndustry"> By Industry</input>' +
                '                   <input class="byTenantRadio" type="radio" name="narrowBy" value="byTenant"> By Tenant</input>' +
                '               </div>' +
                '               <div class="byLocationForm">' +
                '                   <div class="ocms_WP_label"><strong>State</strong></div>' +
                '                   <select class="stateDropdown">' +
                '                       <option value="all">All States</option>' +
                '                   </select>' +
                '                   <div class="ocms_WP_label"><strong>City</strong>' +
                '                   <select class="cityDropdown">' +
                '                       <option value="all">All Cities</option>' +
                '                   </select>' +
                '                   </div>' +
                '               </div>' +
                '               <div class="byIndustryForm">' +
                '                   <div class="ocms_WP_label"><strong>Tenant Industry</strong></div>' +
                '                   <select class="industryDropdown">' +
                '                       <option value="all">All Industries</option>' +
                '                   </select>' +
                '               </div>' +
                '               <div class="byTenantForm">' +
                '                   <div class="ocms_WP_label"><strong>Tenant Name</strong></div>' +
                '                   <select class="tenantDropdown">' +
                '                       <option value="all">All Tenants</option>' +
                '                   </select>' +
                '               </div>' +
                '           </div>' +
                '           <div class="filterBy">' +
                '               <div class="ocms_WP_label"><strong>Filter By Property Type</strong></div>' +
                '               <div class="sector">' +
                '                   <div class="leftColumn">' +
                '                       <div class="filterContainer">' +
                '                           <input type="checkbox" class="filterRetail" name="propertyCheckbox" value="Freestanding Retail"> Single-tenant Retail</input>' +
                '                       </div>' +
                '                       <div class="filterContainer">' +
                '                           <input type="checkbox" class="filterMultiTenant" name="propertyCheckbox" value="Anchored Shopping Centers"> Anchored Shopping Centers</input>' +
                '                       </div>' +
                '                   </div>' +
                '                   <div class="rightColumn">' +
                '                       <div class="filterContainer">' +
                '                           <input type="checkbox" class="filterOffice" name="propertyCheckbox" value="Office"> Office</input>' +
                '                       </div>' +
                '                       <div class="filterContainer">' +
                '                           <input type="checkbox" class="filterIndustrial" name="propertyCheckbox" value="Industrial"> Industrial</input>' +
                '                       </div>' +
                '                       <div class="filterContainer">' +
                '                           <input type="checkbox" class="filterOther" name="propertyCheckbox" value="Other"> Other</input>' +
                '                       </div>' +
                '                   </div>' +
                '               </div>' +
                '           </div>' +
                '       </div>' +
                '   </div>' +
                '   <div class="resultBar">' +
                '       <div class="resultBarCount">' +
                '           <div class="propertyCountContainer"><span class="propertyCount"></span> <span class="propertyText"></span></div>' +
                '           <div class="displayingResults">Displaying <span class="lower"></span> - <span class="upper"></span> of <span class="propertyCount"></span></div>' +
                '       </div>' +
                '       <div class="display">' +
                '           <div class="ocms_WP_label"><strong>Display</strong></div>' +
                '           <div class="uniformDropdown">' +
                '               <select class="displayDropdown">' +
                '                   <option value="25">25 Results</option>' +
                '                   <option value="50">50 Results</option>' +
                '                   <option value="100">100 Results</option>' +
                '                   <option value="200">200 Results</option>' +
                '               </select>' +
                '           </div>' +
                '       </div>' +
                '       <div class="sort">' +
                '           <div class="ocms_WP_label"><strong>Sort By</strong></div>' +
                '           <div class="uniformDropdown">' +
                '               <select class="sortByDropdown">' +
                '                   <option value="Name">Tenants</option>' +
                '                   <option value="City__c">City</option>' +
                '                   <option value="State__c">State</option>' +
                '                   <option value="SqFt__c">SQ FT</option>' +
                '               </select>' +
                '           </div>' +
                '       </div>' +
                '<div class="padTopBot">'+
                system.label.ocms_arcp_properties+
                '</div>'+
                '       <div class="resultBarPaginationTop">' +
                '           <div class="leftArrow"></div>' +
                '           <div class="paginateText">Page <span class="pageLower"></span> of <span class="pageUpper"></span></div>' +
                '           <div class="rightArrow"></div>' +
                '       </div>' +
                '   </div>' +
                '   </div>' +
                '   <div class="propertyListView"></div>' +
                '   <div class="resultBarPaginationBottom">' +
                '       <div class="leftArrow"></div>' +
                '       <div class="paginateText">Page <span class="pageLower"></span> of <span class="pageUpper"></span></div>' +
                '       <div class="rightArrow"></div>' +
                '   </div>' +
                
                '</div>';

        return html;
    }
  
    global override virtual String getHTML(){
        String html = '';

        html += writeControls();
       
        html += '<script src="resource/1446694502000/ocms_properties" type="text/javascript"></script>';

        return html;
    }
  }