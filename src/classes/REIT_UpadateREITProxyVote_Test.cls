@isTest
Public class REIT_UpadateREITProxyVote_Test
{
  static testmethod void testProxyVotes()
  {
      REIT_Proxy_Votes__c cpv= new REIT_Proxy_Votes__c();
     
       Account a = new Account();
       a.name = 'testaccount';
       insert a;
    
       Contact c1= new Contact();
       c1.accountid=a.id;
       c1.firstname='xxx';
       c1.lastname='yyy';
       insert c1;
  
      REIT_Proxy_Votes__c prv= new REIT_Proxy_Votes__c();
      prv.Investor__c='NinadTambe';
      prv.Shares__c=576879.89;
      prv.Fund__c='3374';
      prv.Amount__c=899900.99;
      prv.DST_Account_Number__c='7799400016';
      prv.Contact__c=c1.id;
      insert prv;
      

       
      REIT_Proxy_Votes__c prv2= new REIT_Proxy_Votes__c();
      prv2.Investor__c='NinadTambe';
      prv2.Shares__c=19000.89;
      prv2.Fund__c='3376';
      prv2.Amount__c=899900.99;
      prv2.DST_Account_Number__c='7799400016';
      prv2.Contact__c=c1.id;
      insert prv2;
       
       System.assertEquals(prv.Investor__c,prv2.Investor__c);

      Proxy_Data_Staging__c PDSt= new Proxy_Data_Staging__c();
      PDSt.Account_Number__c='7799400016';
      PDSt.Fund__c='3376';
      insert PDSt;
  
  
  }
  
  
}