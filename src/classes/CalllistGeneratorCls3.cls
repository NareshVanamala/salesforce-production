public with sharing class CalllistGeneratorCls3
{
    /*CallList__c callList ;
    Automated_Call_List__c LockedCL;
    public string[] viewnamelist{get;set;}

    public integer totalcontactsCreated{set;get;}
    public String prid{get;set;}
    public String visibility{get;set;}
    public integer totalcontactsAssigned{set;get;}
    public List<CallList__c> results{get;set;}
    public List<Automated_Call_List__c> autoresults{get;set;}
    public string searchString{get;set;} // search keyword
    GerateDynamicQuery generateQuery =  new GerateDynamicQuery();
    List<CallList__c> clist= new List<CallList__c>();
    List<Automated_Call_List__c> autolist= new List<Automated_Call_List__c>();

    List<User> ulist = new List<User>();
    public Boolean flag{get;set;}
    public String vname{get;set;}
    public Boolean success{get;set;}
    public Boolean bnNext{get;set;}
    public Boolean bnPrevious{get;set;}
    public Boolean bnFirst{get;set;}
    public Boolean bnLast{get;set;}
    String Viewname='';
    String Fieldname='--None--';
    String Fieldname1='--None--';
    String Fieldname2='--None--';
    String Fieldname3='--None--';
    String Fieldname4='--None--';
    String Operator='';
    String Operator1='';
    String Operator2='';
    String Operator3='';
    String Operator4='';
    String value='';
    public Integer cmCount;
    public Integer first{get;set;}
    public Integer rows{get;set;}
    String ContactOption='';
    String[] Username= new String[]{};
    String username1='';
    public Map<String,String> mapfieldnames{get; set;}
    public List<Contact> conlist{get;set;}
    public Transient List<Contact> previewconlist{get;set;}
    public ApexPages.StandardSetController ssc;
    PageReference pageRef12;
    public String automtedCallListid;

    list<Contact_Call_List__c> lsttask = new list<Contact_Call_List__c>();
    public list<contact> lstContact{set;get;}
    public List<SelectOption> options1{set;get;} 
    public List<SelectOption> UserListViews{set;get;}
    public String CallList1 {set;get;}
    public Map<Id,Contact_Call_List__c> mapOfContactToTask{get;set;}
    public Map<integer,Contact_Call_List__c> rowmap{get;set;}
    public boolean showPrev{set;get;}
    public boolean showNext{set;get;}
    public Integer rownum {get;set;}{rownum=-10;}
    public Boolean bnSelect{get;set;}   
    public Contact_Call_List__c tskObj {set;get;}
    public list<Contact_Call_List__c>contactcalllist{get;set;}
    id userid;
    Integer PageNo = 1;
    
    public void init()
    {
       flag=false;
       success=false;
       cmCount = 0;
       Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
       Schema.SObjectType sobjType = gd.get('contact');
       Schema.DescribeSObjectResult r = sobjType.getDescribe();
       Map<String,Schema.SObjectField> M = r.fields.getMap();
       System.debug('+++m'+M);
       for(String fieldName1 : M.keyset())
        {
            Schema.SObjectField field = M.get(fieldName1);                                                    
            Schema.DescribeFieldResult fieldDesc = field.getDescribe();                    
            System.debug('+++++label'+fieldDesc.getLabel());
            System.debug('+++++label'+fieldDesc.getname());
            mapfieldnames.put(fieldDesc.getname(),String.ValueOf(fieldDesc.gettype()));                                        
        }
    } 
    public CallList__c getCallList()
    {        
        if(callList == null)
        callList = new CallList__c();
        return callList;
    }
    public List<SelectOption> getVisbilityValues() {
        List<SelectOption> VisibilityOptions = new List<SelectOption>(); 
        VisibilityOptions.add(new SelectOption('Visible only to me','Visible only to me')); 
        VisibilityOptions.add(new SelectOption('Visible to all users','Visible to all users')); 
        VisibilityOptions.add(new SelectOption('Visible to certain groups of users','Visible to certain groups of users')); 
        return VisibilityOptions; 
    }
    public PageReference save1()
    { 
               
        system.debug('Welcome to the save method......');
        callList.Field_API__c= FieldName=='--None--'?null:FieldName;
        callList.Field_API1__c= FieldName1=='--None--'?null:FieldName1;
        callList.Field_API2__c= FieldName2=='--None--'?null:FieldName2;
        callList.Field_API3__c= FieldName3=='--None--'?null:FieldName3;
        callList.Field_API4__c= FieldName4=='--None--'?null:FieldName4;
        callList.Operator__c = Operator;
        callList.Operator1__c = Operator1;
        callList.Operator2__c = Operator2;
        callList.Operator3__c = Operator3;
        callList.Operator4__c = Operator4;
        callList.Contact_Type__c=ContactOption;  
        
        try
        {
            if(validate())
            {
               List<CallList__c> cls1 = [Select Id,Name from CallList__c where Name =: callList.name];
               System.debug('Duplicate Name Check' + cls1.size() + '*******' +callList.name+'******');
               if(cls1.size() >= 1){               
                 System.debug('*****Entering if');
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'CallList with the same Name already exists'));
                 return null;
               }
               else{
               system.debug('Testing Visisbility'+visibility);
               insert callList; 
               LockedCL = new Automated_Call_List__c ();
               LockedCL.Name =  callList.name;
               LockedCL.TempLock__c = 'General Lock';
               insert LockedCL;
               
               flag=true;   
               
               List<Calllist__c> cls = [Select Id,CampaignName__c from calllist__c where ID = :Calllist.Id];
               
               
               Set<ID> activatebatch = new Set<ID>();
               
               for(Calllist__c abc : cls)
               {
               activatebatch.add(abc.Id);
               }     
                 If(cls[0].CampaignName__c == null) { 
                
                       calllistinserts1 batch25 = new calllistinserts1(activatebatch);
                       Id batchId = Database.executeBatch(batch25);
                   
                 }
                 If(cls[0].CampaignName__c != null){
                  
                     calllistinsertsforcm batch = new calllistinsertsforcm(activatebatch);
                     Id batchId1 = Database.executeBatch(batch);
                   }
                  
                 
            }
            }                
            else
            {
               return null;
            }
        }
        catch(Exception E)
        {    
           system.debug('Exception---->'+E.getmessage());
           
        } 
        if(visibility == 'Visible only to me'){
         pageRef12 = new PageReference('/apex/calllistpage');
         
        }else if(visibility == 'Visible to all users'){
            Automated_Call_List__share automatedShare =  new Automated_Call_List__share();
            ID groupId = [select id from Group where Type = 'Organization'].id;
            automatedShare.ParentId = LockedCL.id;
            automatedShare.UserOrGroupId = groupId;
            automatedShare.AccessLevel = 'Edit';
             automatedShare.RowCause = Schema.Automated_Call_List__share.RowCause.Manual;
             Database.SaveResult sr = Database.insert(automatedShare,false);
            if(sr.isSuccess()){
                 pageRef12 = new PageReference('/apex/calllistpage');
                 system.debug('*******sr************'+sr);
            }else{
                 Database.Error err = sr.getErrors()[0];
                 system.debug('*******err************'+err);
                if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  && 
                  err.getMessage().contains('AccessLevel')){
                       pageRef12 = new PageReference('/apex/calllistpage');
                  }else{
                      return null;
                  }

            }
        }
        else{
        system.debug('Automated Call List Id'+LockedCL.id);
        pageRef12 = new PageReference('/p/share/CustomObjectSharingEdit?parentId='+LockedCL.id);
        
        }
       pageRef12.setRedirect(true);
        return pageRef12; 
    } 
    public Boolean validate()
    {   
        if(callList.Name == NULL || callList.Name == '')
        {
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter Call List Name'));
           return false;
        }
        if(callList.CampaignName__c== NULL && callList.Field_API__c == null && callList.Field_API1__c == null && 
        callList.Field_API2__c == null && callList.Field_API3__c == null && 
        callList.Field_API4__c == null && callList.Operator__c == null && 
        callList.Operator1__c == null  && callList.Operator2__c == null && 
        callList.Operator3__c == null && callList.Operator4__c == null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter atleast one filter value'));
            return false;
        }
        if((callList.Field_API__c != null && callList.Operator__c == null) || (callList.Field_API__c == null && callList.Operator__c != null) ||          
        (callList.Field_API1__c != null && callList.Operator1__c == null) || (callList.Field_API1__c == null && callList.Operator1__c != null) ||
        (callList.Field_API2__c != null && callList.Operator2__c == null) || (callList.Field_API2__c == null && callList.Operator2__c != null) ||
        (callList.Field_API3__c != null && callList.Operator3__c == null) || (callList.Field_API3__c == null && callList.Operator3__c != null) ||
        (callList.Field_API4__c != null && callList.Operator4__c == null) || (callList.Field_API4__c == null && callList.Operator4__c != null))
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select the operator'));
            return false;
        }
         
         return true;
    } 
    public CalllistGeneratorCls3() 
    {
     Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
     viewnamelist = new string[]{};

        first=0;
        rows=10;
        bnNext = false;
        bnPrevious = false;
        bnFirst = false;
        bnLast = false;
        bnselect = false;
        calllist = new CallList__c();
        mapfieldnames = new Map<String,String>();
        mapfieldnames.put('--None--','--None--');
        mapfieldnames.put('Account.Name','STRING');
        
        // get the current search string
        searchString = System.currentPageReference().getParameters().get('lksrch');
        runSearch();
        lstContact = new list<contact>();
        set<String> call = new Set<String>();    
        List<String> lscall = new List<String>();
        showPrev = false;
        showNext = false;
        visibility = 'Visible only to me';
        ContactOption = 'All Contacts';
       
    } // --End of Constructor --

    //Search Call List from call List drop down box. --Start---
    public List<SelectOption> getListViews()
    {
        List<SelectOption> options = new List<SelectOption>();
        String autoname;
        if(searchString == null || searchString == '')
        {              
            autolist = [Select id,name from Automated_Call_List__c where Archived__c = false and IsDeleted__c=false and Original_Call_list__c=null Order By Name];           
            System.debug('$$$$$size' + autolist.size());
        }
        else
        {
            autolist = [select id, name from Automated_Call_List__c where Archived__c = false and IsDeleted__c=false and Original_Call_list__c=null and name like:'%'+searchString+'%'];
        }
        For(integer i=0;i<autolist.size();i++)
        {
            options.add(new SelectOption(autolist[i].Name,autolist[i].name));
        }
        return options;
    } 
    
    public List<SelectOption> getListViews1()
    {
        List<SelectOption> options = new List<SelectOption>();
                      
            autolist = [Select id,name from Automated_Call_List__c where Archived__c = false and IsDeleted__c=false Order By Name];           
            System.debug('$$$$$size' + autolist.size());
            For(integer i=0;i<autolist.size();i++)
            {
                options.add(new SelectOption(autolist[i].Name,autolist[i].name));
            }
            return options;
    } 
    

    
   
    public String getViewname() 
    {
      return Viewname;
    }
    public void setViewname(String Viewname) 
    {
      this.Viewname = Viewname;
    }

    
    public String getContactOption() 
    {
      return ContactOption;
    }
    public void setContactOption(String ContactOption) 
    {
      this.ContactOption = ContactOption;
    }

   
    public String getSearchString()
    {
        return searchString;
    }  
    public void setSearchString(String SearchString) 
    {
        this.SearchString= SearchString;
    }

    //Search related methods --- Start 
    public void search() 
    {
        runSearch();
    }
      
    private void runSearch() 
    {
       autoresults = performSearch(searchString);               
    }

    private List<Automated_Call_List__c> performSearch(string searchString) 
    {
    
        String soql = 'select id, name from Automated_Call_List__c  where Archived__c=false And IsDeleted__c=false';
        if(searchString != '' && searchString != null)
        soql = soql + ' AND'+' name LIKE \'%' + searchString +'%\'';
        return database.query(soql); 
        
          
         
    }
    
    public string getFormTag() 
    {
         return System.currentPageReference().getParameters().get('frm');
    }  
    
    public string getTextBox() 
    {
         return System.currentPageReference().getParameters().get('txt');
    } 
    public Pagereference RetrieveUser()
    {
            
            System.debug('Entering Retrieve Method--->');
           Automated_Call_List__c lock = [Select Id, Name, TempLock__c from Automated_Call_List__c where Name= :ViewName and Archived__c = false and IsDeleted__c=false];
       System.debug('****' + lock + lock.TempLock__c);
        
        If (lock.TempLock__c != null) {
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Some operation is being on this call list So please wait for sometime'));
          return null;  
            }
          try
          {
             
            set<String> call = new Set<String>();    
            List<String> lscall = new List<String>();
            List<Contact_Call_List__c> clist1 = [Select id ,CallListOrder__c , Owner__r.Name,CallList_Name__c,Call_Complete__c from Contact_Call_List__c where CallList_Name__c =:Viewname and Owner__r.Name != null];//wholesaler.Id];
            System.debug('&&&&&&clist' + clist1);
            for(Contact_Call_List__c t : clist1)
            call.add(t.Owner__r.Name); 
            System.debug('!!!!!!!!' + call);
         
            UserListViews = new  List<SelectOption>();
            lscall.AddAll(call);
            lscall.sort();
            for(String s : lscall){                
                UserListViews.add(new SelectOption(s,s));
            }
            if(UserListViews.size()==0)
               UserListViews.add(new SelectOption('--None--','--None--'));               
               return null;
        }catch(Exception e){ System.debug('Exception-----'+e);return null;} 
        
    }
   
    public Pagereference deleteCallList()
    {     
         System.debug('Hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii' + Viewnamelist); 
         flag = true;
         set<Id> aId = new set<id>();
         set<Id> aId1 = new set<id>();
         list<Automated_Call_List__c> autolist = [Select id, name ,TempLock__c from Automated_Call_List__c where name in:Viewnamelist and Archived__c = false and IsDeleted__c=false];
         for(Automated_Call_List__c autolist1:autolist)
         {
         If(autolist1.TempLock__c != null)
         {
         aId.add(autolist1.id);
         }
         else
         {
         aId1.add(autolist1.id);
         }
         }
         if(aId.size()>0)
         {
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Some operation is being on this call list So please wait for sometime'));
         }
         if(aId.size()==0)
         {
         system.debug('Hiiiiiiiiiiiiiiiiiiiiiiiiiiiii');
         calllistdelete3 batch = new calllistdelete3(aId1);
         Id batchId = Database.executeBatch(batch);  
         
         pageRef12 = new PageReference('/apex/calllistpage');
         pageRef12.setRedirect(true);
         return pageRef12;
         }
       
        return null;
               
    }
    public List<SelectOption> getFields()
    {
       List<SelectOption> fldoptions = new List<SelectOption>();
       Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
       Schema.SObjectType sobjType = gd.get('Contact');
       Schema.DescribeSObjectResult r = sobjType.getDescribe();
       Map<String,Schema.SObjectField> M = r.fields.getMap();
       System.debug('+++m'+M);
       fldoptions.add(new SelectOption('--None--','--None--'));
       fldoptions.add(new SelectOption('Account.Name','Account Name'));
       for(String fieldName1 : M.keyset())
       {
         Schema.SObjectField field = M.get(fieldName1);                                                    
         Schema.DescribeFieldResult fieldDesc = field.getDescribe();                    
         System.debug('+++++label'+fieldDesc.getLabel());
         System.debug('+++++label'+fieldDesc.getname());
         mapfieldnames.put(fieldDesc.getname(),String.ValueOf(fieldDesc.gettype()));
         fldoptions.add(new SelectOption(fieldDesc.getname(),fieldDesc.getlabel()));                                        
       }
       SelectOptionSorter.doSort(fldoptions, SelectOptionSorter.FieldToSort.Label);
       return fldoptions;
    }

    public String getFieldname()
    {
       return Fieldname;
    }        
    public void setFieldname(String Fieldname)
    {
       this.Fieldname= Fieldname;
    }
    
    public String getFieldname1() 
    {
      return Fieldname1;
    }
    public void setFieldname1(String Fieldname1) 
    {
      this.Fieldname1 = Fieldname1;
    }

    public String getFieldname2() 
    {
      return Fieldname2;
    }
    public void setFieldname2(String Fieldname2) 
    {
      this.Fieldname2 = Fieldname2;
    }

    public String getFieldname3() 
    {
      return Fieldname3;
    }
    public void setFieldname3(String Fieldname3) 
    {
      this.Fieldname3 = Fieldname3;
    }

    public String getFieldname4() 
    {
      return Fieldname4;
    }
    public void setFieldname4(String Fieldname4) {
      this.Fieldname4 = Fieldname4;
    }

    public List<SelectOption> getOperatorList() 
    {
         List<SelectOption> OperatorList = new List<SelectOption>();
         OperatorList=createOperatorList(FieldName);
         return OperatorList;
    }
     
    public List<SelectOption> getOperatorList1() 
    {
         List<SelectOption> OperatorList1 = new List<SelectOption>();
         OperatorList1=createOperatorList(FieldName1);
         return OperatorList1;  
    }
     
    public List<SelectOption> getOperatorList2() 
    {
         List<SelectOption> OperatorList2 = new List<SelectOption>();
         OperatorList2=createOperatorList(FieldName2);
         return OperatorList2;
    }

    public List<SelectOption> getOperatorList3() 
    {
         List<SelectOption> OperatorList3 = new List<SelectOption>();
         OperatorList3=createOperatorList(FieldName3);
         return OperatorList3;
    }

    public List<SelectOption> getOperatorList4() 
    {
         List<SelectOption> OperatorList4 = new List<SelectOption>();
         OperatorList4=createOperatorList(FieldName4);
         return OperatorList4;
    }
     
    public List<SelectOption> createOperatorList(String FieldAPI)
    {
        List<SelectOption> Oproptions = new List<SelectOption>();
        Oproptions.add(new SelectOption('', '--None--'));
        if((String.valueof(mapfieldnames.get(FieldAPI))) == 'BOOLEAN' )
        {
          Oproptions.add(new SelectOption('equals', 'equals'));
        }
        else{
        Oproptions.add(new SelectOption('not equal to', 'not equal to'));
        Oproptions.add(new SelectOption('equals', 'equals'));
        if((String.valueof(mapfieldnames.get(FieldAPI))) == 'STRING' || (String.valueof(mapfieldnames.get(FieldAPI))) == 'PICKLIST' || (String.valueof(mapfieldnames.get(FieldAPI))) == 'TEXTAREA' || (String.valueof(mapfieldnames.get(FieldAPI))) == 'PHONE' ||(String.valueof(mapfieldnames.get(FieldAPI))) == 'Email' )
        {  
           Oproptions.add(new SelectOption('starts with', 'starts with'));
           Oproptions.add(new SelectOption('contains', 'contains'));
           Oproptions.add(new SelectOption('does not contain', 'does not contain'));
        }
        else if((String.valueof(mapfieldnames.get(FieldAPI))) == 'INTEGER' || (String.valueof(mapfieldnames.get(FieldAPI)))== 'CURRENCY' || (String.valueof(mapfieldnames.get(FieldAPI)))== 'DOUBLE' || (String.valueof(mapfieldnames.get(FieldAPI))) == 'PERCENT'|| (String.valueof(mapfieldnames.get(FieldAPI))) == 'DATE' ||(String.valueof(mapfieldnames.get(FieldAPI)))== 'DATETIME')    
        {
          Oproptions.add(new SelectOption('less than', 'less than'));
          Oproptions.add(new SelectOption('greater than', 'greater than'));
          Oproptions.add(new SelectOption('less or equal', 'less or equal'));
          Oproptions.add(new SelectOption('greater or equal', 'greater or equal'));
        }     
        else if((String.valueof(mapfieldnames.get(FieldAPI))) == 'MULTIPICKLIST' )
        {
          Oproptions.add(new SelectOption('includes', 'includes'));
          Oproptions.add(new SelectOption('excludes', 'excludes'));
        }
        }          
        return Oproptions;
    }
     
    public String getOperator() 
    {
      return Operator;
    }
    public void setOperator(String Operator) 
    {
      this.Operator = Operator;
    }
     
    public String getOperator1() 
    {
      return Operator1;
    }
    public void setOperator1(String Operator1) 
    {
      this.Operator1 = Operator1;
    }

    public String getOperator2() 
    {
      return Operator2;
    }
    public void setOperator2(String Operator2) 
    {
      this.Operator2 = Operator2;
    }

    public String getOperator3() 
    {
      return Operator3;
    }
    public void setOperator3(String Operator3) 
    {
      this.Operator3 = Operator3;
    }

    public String getOperator4() 
    {
      return Operator4;
    }
    public void setOperator4(String Operator4) 
    {
      this.Operator4 = Operator4;
    } 

    public String getValue() 
    {
      return value;
    }
    public void setValue(String value) 
    {
      this.value = value;
    }
     
    public List<SelectOption> getUserList()
    {
       List<SelectOption> options1 = new List<SelectOption>();
       List<Profile> plist = new List<Profile>();
       List<Id> pid= new List<Id>();
       plist = [select id,name from Profile where name='Internal Sales'];
       For(integer i=0;i<plist.size();i++)
       pid.add(plist[i].id);
       ulist= [select id, name from User where ProfileId in : pid and IsActive = true order By Name];
       For(integer i=0;i<ulist.size();i++)
        {
           options1.add(new SelectOption(ulist[i].name,ulist[i].name));
        }
        return options1;
    }    

    public String[] getUsername() 
    {
      return Username;
    }
    public void setUsername(String[] Username) 
    {
      this.Username = Username;
    }

    public String getUsername1() 
    {
      return Username1;
    }   
    public void setUsername1(String Username1) 
    {
      this.Username1 = Username1;
    }
    
    public List<SelectOption> getOwner() 
    {
        List<SelectOption> radoptions = new List<SelectOption>();
        radoptions.add(new SelectOption('All Contacts', 'All Contacts'));
        radoptions.add(new SelectOption('My Contacts', 'My Contacts'));
        return radoptions;
    }
      
   
    public  void previewredirect()
    {
       Id aId;
       Automated_Call_List__c autolist = [Select id, name ,TempLock__c from Automated_Call_List__c where name =: Viewname and Archived__c = false and IsDeleted__c=false];
       aId = autolist.id;
      
                                     
           ID activatebatchset;
           activatebatchset = autolist.id;
          If (autolist.TempLock__c != null) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Some operation is being on this call list So please wait for sometime'));
          }
          else{
            Generatebatchpreview batch29 = new Generatebatchpreview(activatebatchset);
            Id batchId111 = Database.executeBatch(batch29);
          }
             
             
                 
       
    }

    public Boolean Valid()
    {
       if(username.size()>1)
       {
         return true;
       }
       return false;
    }

    public void Next()
    {
        conList = new List<Contact>();
        ssc.next();
        for(sObject sObj: ssc.getRecords())
        {
           conlist.add((Contact)sObj);
        }
        disableLinks();
    }

    public void Previous()
    {
        conList = new List<Contact>();
        ssc.previous();
        for(sObject sObj: ssc.getRecords()){
            conlist.add((Contact)sObj);
        }
        disableLinks();
    }

    public void getFirstRows()
    {
        conList = new List<Contact>();
        ssc.first();
        for(sObject sObj: ssc.getRecords()){
            conlist.add((Contact)sObj);
        }
        disableLinks();
    }

    public void getLastRows()
    {
        conList = new List<Contact>();
        ssc.last();
        for(sObject sObj: ssc.getRecords()){
          conlist.add((Contact)sObj);
        }
        disableLinks();
    }

    public void disableLinks()
    {
        bnNext= ssc.getHasNext();
        bnPrevious = ssc.getHasPrevious();
        if(ssc.getHasPrevious())
            bnFirst = true;
        else
            bnFirst = false;
        if(ssc.getHasNext())
            bnLast = true;
        else
            bnLast = false;
    } 
    
     public PageReference GeneralLock(){
    
     
       Automated_Call_List__c lock = [Select Id, Name, TempLock__c from Automated_Call_List__c where Name= :ViewName and Archived__c = false and IsDeleted__c=false];
       System.debug('****' + lock + lock.TempLock__c);
        
        If (lock.TempLock__c != null) {
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Some operation is being on this call list So please wait for sometime'));
          return null;
        }
        return null;
    }
    
   
    
    
    public PageReference cancel()
    {
          PageReference cancelref = new PageReference('/apex/calllistpage');
          cancelref.setRedirect(true);
          return cancelref;
    }
    
    
    
    public List<User> ul{get;set;}
    public String User {set;get;}
 
    public List<SelectOption> getUserValues()      
    
    {    
         List<SelectOption> intsales = new List<SelectOption>();       
         intsales.add(new SelectOption('--None--','--None--'));           
         List<Profile> plist = new List<Profile>();
         List<Id> pid= new List<Id>();
         plist = [select id,name from Profile where name='Internal Sales'];
         For(integer i=0;i<plist.size();i++)
          pid.add(plist[i].id);
          ul = [ Select Id, Name from User where IsActive = True And ProfileId in : pid Order By Name];
                  
         for( integer i=0; i< ul.size();i++)
            {
                intsales.add(new SelectOption(ul[i].Name,ul[i].name));
            }         
         return intsales;     
     }  

     
      
   Public List<Automated_Call_List__c> getAssigned()
      {
          
          Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
          
            list<Automated_Call_List__c> at = new list<Automated_Call_List__c>();
            List<Id> acl = new List<Id>();           
            List <User> wholesaler = [ Select Id, Name from User where Name = :User]; 
            List<Id> uid = new List<Id>();  
              For(User u : wholesaler)
              {
                 uid.add(u.Id);
              }
                List<Automated_Call_List__c> sortthelist = [Select ID, Name,Original_Call_list__c ,TotalRemaining__c, PHandler__c, (Select Id,CallList_Name__c, CallListOrder__c,Owner__c from Contact_Call_List_Orders1__r Where Owner__c in : uid and Call_Complete__c = False and CSV_Generated__c=false Limit 1 ) from Automated_call_list__c where Archived__c = False and IsDeleted__c=false];
               
               for(Automated_Call_List__c sortingthelist : sortthelist )
               {
               List<Contact_Call_List__c> dragableccl = sortingthelist.Contact_Call_List_Orders1__r;
               If(dragableccl.size()>0)
               {
                 acl.add(sortingthelist.Id);
               }
               
               }
               List<Automated_Call_List__c> plis = [Select Name,Original_Call_list__c ,TotalRemaining__c, PHandler__c    from Automated_Call_List__c where Id in :acl And Archived__c = False and IsDeleted__c=false and Global_Priority__c=null order by PHandler__c nulls Last];
               return plis; 
               
       
       } 
       
      Public List<Automated_Call_List__c> getAssigned1()
      {
          
          Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
          
            list<Automated_Call_List__c> at = new list<Automated_Call_List__c>();
            List<Id> acl = new List<Id>();           
            List <User> wholesaler = [ Select Id, Name from User where Name = :User]; 
            List<Id> uid = new List<Id>();  
              For(User u : wholesaler)
              {
                 uid.add(u.Id);
              }
                List<Automated_Call_List__c> sortthelist = [Select ID, Name,Original_Call_list__c ,TotalRemaining__c, PHandler__c, (Select Id,CallList_Name__c, CallListOrder__c,Owner__c  from Contact_Call_List_Orders1__r Where  Call_Complete__c = False and Owner__c in : uid and CSV_Generated__c=false Limit 1 ) from Automated_call_list__c where Archived__c = False and IsDeleted__c=false and Global_Priority__c!=null order by Global_Priority__c ASC ];
                if(sortthelist.size()>0)
               {
                   for(Automated_Call_List__c sortingthelist : sortthelist )
                   {
                       List<Contact_Call_List__c> dragableccl = sortingthelist.Contact_Call_List_Orders1__r;
                       

                      Integer Totalcons = [select COUNT() from Contact_Call_List__c where CallListOrder__c =:sortingthelist.Id and Owner__c in : uid And ReasontoSkip__c = null And Call_Complete__c = False and IsDeleted__c=false and CSV_Generated__c=false]; 
 
                       sortingthelist.TotalRemaining__c = Totalcons;
                       at.add(sortingthelist );
                   }
               }   
              
          return at ; 
               
        
       } 
       
       public pagereference UserSave()     
       {
         List<Id> apl = new List<Id>();
         List<Automated_Call_List__c> updatename = new List<Automated_Call_List__c>();
         List <User> wholesaler1 = [ Select Id, Name from User where Name = :User];
         List<Id> uid1 = new List<Id>();
         Id usids1;
         Set<id>aclid=new Set<id>();
         For(User u1 : wholesaler1)
            {
                 uid1.add(u1.Id);
            }
         List<Contact_Call_List__c> ccla = [Select Id,CallList_Name__c, CallListOrder__c,Owner__c from Contact_Call_List__c Where Owner__c in : uid1 and Call_Complete__c = False and CSV_Generated__c=false];
         For(Contact_Call_List__c ccl11 : ccla)
             {
                apl.add(ccl11.CallListOrder__c);
             }
         List<Automated_Call_List__c> plisa = [Select id,PrioritySetter__c,Name from Automated_Call_List__c where Id in :apl And Archived__c = False and IsDeleted__c=false];
          
           
         for(integer i3=0;i3<plisa.size();i3++)
          {
            aclid.add(plisa[i3].id);            
          } 
         list<Contact_Call_List__c>snehalspeciallist= new list<Contact_Call_List__c>();  
         list<Contact_Call_List__c>Updatesnehalspeciallist= new list<Contact_Call_List__c>();
         User useridss = [ Select Id from User where Name = :user limit 1];
         usids1 = useridss.Id;
         for(Automated_Call_List__c ns:plisa)
             {
               snehalspeciallist=[select id,Owner__c,Priority__c, CallListOrder__c,UpdatedPriority__c from Contact_Call_List__c where CallListOrder__c =:ns.id and Owner__c =:usids1 limit 1];   
               if(snehalspeciallist.size()>0)
               {
                   snehalspeciallist[0].Priority__c=ns.PrioritySetter__c;
                   snehalspeciallist[0].UpdatedPriority__c = true; 
                   Updatesnehalspeciallist.add(snehalspeciallist[0]);
                }   
                   ns.TransitionUserId__c = useridss.Id;
                    updatename.add(ns);
             }
             system.debug('check the size'+Updatesnehalspeciallist);
             if(Updatesnehalspeciallist.size()>0)
             update Updatesnehalspeciallist;
             helper.run = FALSE;
            System.debug('****Userids' + usids1);
            SetPriorityonCCL1 batch = new SetPriorityonCCL1(usids1,aclid);
            Id batchId = Database.executeBatch(batch);
            PageReference Backtogenerator = new PageReference('/apex/calllistpage');
            Backtogenerator.setRedirect(true);
            return Backtogenerator;
        
       } 
    public pagereference BackfromPrioritySetter()
    {
          PageReference Backtogenerator1 = new PageReference('/apex/calllistpage');
          Backtogenerator1.setRedirect(true);
          return Backtogenerator1;
    }*/
      
      

}