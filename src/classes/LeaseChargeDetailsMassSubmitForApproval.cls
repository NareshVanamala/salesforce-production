global with sharing class LeaseChargeDetailsMassSubmitForApproval
{
    List<Lease__c> Lease = new List<Lease__c>();       
    Public Lease_Charges_Request__c leaseChargeRequestObj {get;set;}
    public LeaseChargeDetailsMassSubmitForApproval(ApexPages.StandardController controller) {
        leaseChargeRequestObj = new Lease_Charges_Request__c ();
        leaseChargeRequestObj = (Lease_Charges_Request__c)controller.getRecord();
        
    } 
   
    webservice static void MassSubmit(Id leasechargeId)
    {
        
        List<Lease_Charges_Request__c> LCR1= new List<Lease_Charges_Request__c>();
        if(leasechargeId!=null || Test.isRunningTest())
        {   
            List<Lease_Charges_Request__c> NewLCR = new List<Lease_Charges_Request__c>();
            List<Lease_Charges_Request__c> LCR = new List<Lease_Charges_Request__c>();
            LCR = [select id,Lease__r.MRI_PROPERTY__r.Property_ID__c ,MRI_Property_Team__c,Unique_ID__c,Approval_Status__c, MRI_Property__c,Asset_Manager__c,Director_Vice_President__c,Property_Manager__c,SVP__c,Source_Code__c,Lease__r.MRI_PROPERTY__r.Common_Name__c,Lease__r.Lease_ID__c,Pdf_Name__c from Lease_Charges_Request__c where Id =:leasechargeId];
            System.debug('LCR'+LCR);
           List<ProcessInstanceWorkitem> piwi1 = [SELECT Id, ProcessInstanceId, ProcessInstance.TargetObjectId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =:LCR[0].id]; 
            
              If(LCR[0].Unique_ID__c!=NULL){
              
                   NewLCR=[select id,Unique_ID__c,Approval_Status__c,MRI_Property_Team__c from Lease_Charges_Request__c where Unique_ID__c =:LCR[0].Unique_ID__c];
                   System.debug('NewLCR'+NewLCR);
                      for (Integer i = 0; i < NewLCR.size(); i++) {
                            List<ProcessInstanceWorkitem> piwi = [SELECT Id, ProcessInstanceId, ProcessInstance.TargetObjectId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =:NewLCR[i].id]; 
                         If ((NewLCR[i].MRI_Property_Team__c == 'ST Retail' || NewLCR[i].MRI_Property_Team__c == 'Office/Industrial' || NewLCR[i].MRI_Property_Team__c == 'Restaurant' || NewLCR[i].MRI_Property_Team__c == 'MT Retail') && piwi.size()==0)  {
                           
                                Approval.ProcessSubmitRequest[] lRequests = New Approval.ProcessSubmitRequest[]{};
                                Approval.ProcessSubmitRequest req   = new   Approval.ProcessSubmitRequest();            
                                req.setComments('Submitted for Approval');            
                                req.setObjectId(NewLCR[i].id);          
                                lRequests.add(req);                               
                                Approval.ProcessResult[] results =Approval.process(lRequests); 
                                
                        
                       }
                   }
               }
             else
             {
                  If ((LCR[0].MRI_Property_Team__c == 'ST Retail' || LCR[0].MRI_Property_Team__c == 'Office/Industrial'|| LCR[0].MRI_Property_Team__c == 'Restaurant'|| LCR[0].MRI_Property_Team__c == 'MT Retail') && piwi1.size()==0)   {
                            System.debug('test');
                            Approval.ProcessSubmitRequest[] lRequests1 = New Approval.ProcessSubmitRequest[]{};
                            Approval.ProcessSubmitRequest req1   = new   Approval.ProcessSubmitRequest();            
                            req1.setComments('Submitted for Approval');            
                            req1.setObjectId(LCR[0].id);         
                            lRequests1.add(req1);
                            System.debug('req1'+req1);                  
                            Approval.ProcessResult[] results1 =Approval.process(lRequests1); 
            
                 }  
                  
              
              }
        }
     }
 }