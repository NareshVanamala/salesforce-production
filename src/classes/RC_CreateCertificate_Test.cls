@isTest
Public class RC_CreateCertificate_Test
{
  static testmethod void testCreateCertificate()
  {
  
     
     RC_Property__c PRT=new RC_Property__c();
     PRT.name='Amazon DC 2014 USAA~A11123';
     insert PRT;
     
     RC_Location__c LC=new RC_Location__c();
     LC.name='Amazon DC 2014 USAA~A11123';
     LC.Entity_Tenant_Code__c='12345';
     LC.Property__c=PRT.id;
     insert LC;
     
     System.assertEquals(PRT.name,LC.name);

     RC_Certificate_of_Insurance__c certific=new RC_Certificate_of_Insurance__c();
     certific.Location__c=LC.id;
      insert certific;
     
     string key=PRT.id;
     ApexPages.currentPage().getParameters().put('ids', key);
     
     RC_CreateCertificate rcClass= new RC_CreateCertificate();
     rcClass.Certcate=certific;
     
     rcClass.SAVE();
     rcClass.Cancel();
  
  
  
  
  }
  
}