global class schedularforbusinessprofile 
{
 /* implements System.Schedulable public schedularforbusinessprofile()
 global void execute(SchedulableContext sc)
 {
   List<Account>Expirysixmonths=new list<Account>();
   List<Account>Expiryoneyesr=new List<Account>();
   date beforSixmonths=date.newInstance(Date.today().Year(),date.today().month()-6,date.today().day());
   date beforeoneYear= date.newInstance(date.today().year()-1,date.today().month(),date.today().day());
   
   Expirysixmonths=[Select Id, Name, LastModifiedDate from Account where LastModifiedDate = :beforSixmonths And RecordTypeId = '012500000005RQ4'];
   Expiryoneyesr=[Select Id, Name, LastModifiedDate from Account where LastModifiedDate = :beforeoneYear And RecordTypeId = '012500000005RQ4'];
 
  For Business profile question section
 
 List<Business_Profile_Tracking__c> tBusinessProfilequestion = [select ID,Account__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from Business_Profile_Tracking__c where (Name='Custody Relationship Updated'OR Name='Custody Relationship Created' OR Name='Helping Clients with Income in this Env Updated'or Name='Helping Clients with Income in this Env Created' Or Name='Business Fee Based Created' or Name='Business Fee Based Updated'or name='RIA_Experience__c Updated' or name='RIA_Experience__c Created') AND  Account__c in : Expirysixmonths];
 list<id>custodyrelationshipid=new list<id>();
 List<account>custodyrelationshipACC=new list<Account>();
 
 list<id>Helpingclientsid=new list<id>();
 List<account>helpclientsACC=new list<Account>();

  list<id>businessfeebasedid=new list<id>();
  List<account>businessfeebaseACC=new list<Account>(); 
 
 list<id>RIAExperienceid=new list<id>();
 List<account>RIAExperiencecACC=new list<Account>();
 
 list<Account>UpdatedCustodylist=new list<Account>();
 list<Account>UpdatedChelpingClientlist=new list<Account>();
 list<Account>Updatedbusinessfeelist=new list<Account>();
 list<Account>UpdatedRiaexperiencelist=new list<Account>();
 
 for(Business_Profile_Tracking__c bt: tBusinessProfilequestion )
 {
     if(bt.name=='Custody Relationship Updated'||bt.name=='Custody Relationship Created')
     custodyrelationshipid.add(bt.Account__c);
      
     if(bt.name=='Helping Clients with Income in this Env Created'||bt.name=='Helping Clients with Income in this Env Created')
     Helpingclientsid.add(bt.Account__c);
     
     if(bt.name=='Business Fee Based Updated'||bt.name=='Business Fee Based Created')
     businessfeebasedid.add(bt.Account__c);
     
     if(bt.name=='RIA_Experience__c Updated'||bt.name=='RIA_Experience__c Created')
     RIAExperienceid.add(bt.Account__c);
  }
 
 custodyrelationshipACC=[select id,Custody_Relationship_value_changedcheck__c,Custody_Relationship__c  from Account where id in: custodyrelationshipid];
    let's check the list
 
  system.debug('Check custody relationship has not been updated for how many accounts'+ custodyrelationshipACC.size());
  system.debug('Check custody relationship has not been updated for which accounts'+ custodyrelationshipACC);

for(Account acc: custodyrelationshipACC)
{
     acc.Custody_Relationship_value_changedcheck__c=true;
     acc.Custody_Relationship__c =null;
     acc.Your_custodial_relationship_with_other__c=null;
     UpdatedCustodylist.add(acc);
}
helper.run = FALSE;
update UpdatedCustodylist;
system.debug('Check whether still updated or not'+ UpdatedCustodylist);
system.debug('how many updated'+ UpdatedCustodylist.size());

helpclientsACC=[select id,Helping_Clients_with_Income_in_check__c,Helping_Clients__c from Account where id in: Helpingclientsid];
let's check the list
 
  system.debug('Check  updated for how many accounts'+ helpclientsACC.size());
  system.debug('Check  updated for which accounts'+ helpclientsACC);

for(Account acc1:helpclientsACC)
{
     acc1.Helping_Clients_with_Income_in_check__c =true;
     acc1.Helping_Clients__c=null;
     acc1.Helping_Clients_wi_Inv_Others__c=null;
     UpdatedChelpingClientlist.add(acc1);
}
helper.run = FALSE;
update UpdatedChelpingClientlist;
system.debug('Check whether still updated or not'+ UpdatedChelpingClientlist);
system.debug('how many updated'+ UpdatedChelpingClientlist.size());

businessfeebaseACC=[select id,Business_Fee_Based_valuecheck__c,Percentage_fee_base__c  from Account where id in: businessfeebasedid];
let's check the list
 
  system.debug('Check businessfeebaseACC has not been updated for how many accounts'+ businessfeebaseACC.size());
  system.debug('Check businessfeebaseACC has not been updated for which accounts'+ businessfeebaseACC);
for(Account acc2: businessfeebaseACC)
{
     acc2.Business_Fee_Based_valuecheck__c  =true;
     acc2.Percentage_fee_base__c =null;
     Updatedbusinessfeelist.add(acc2);
}
helper.run = FALSE;
update Updatedbusinessfeelist;
system.debug('Check the updated list'+ Updatedbusinessfeelist);

RIAExperiencecACC=[select id,RIAExperiencec_value_changedcheck__c,RIA_Experience__c  from Account where id in: RIAExperienceid];
system.debug('Check the list'+ RIAExperiencecACC.size());
system.debug('Check the accounts'+ RIAExperiencecACC);

for(Account acc3: RIAExperiencecACC)
{
     acc3.RIAExperiencec_value_changedcheck__c  =true;
     acc3.RIA_Experience__c=null;
     acc3.How_Long_have_you_been_an_RIA_Other_Valu__c=null;
     UpdatedRiaexperiencelist.add(acc3);
}
helper.run = FALSE;
update UpdatedRiaexperiencelist;
system.debug('Check the updated the list'+ UpdatedRiaexperiencelist);



   for size of business section
List<id>Aumidlist=new list<id>();
List<Account>AumACCist=new list<Account>();
list<Account>updatedAUM=new list<Account>();

List<id>clientsinREidlist=new list<id>();
List<Account>clientsinREACClist=new list<Account>();
List<Account>updatedClientsinReAcc=new list<Account>();

List<id>otherexposureidlist=new list<id>();
List<Account>otherexposureACClist =new list<Account>();
List<Account>updatedotherExposure=new list<Account>();

List<id>AllocatedtoREidlist=new list<id>();
List<Account>AllocatedtoREACCist=new list<Account>();
List<Account>updatedallocatetore=new list<Account>();

List<id>REExposureThroughidlist=new list<id>();
List<Account>REExposureThrougACCist=new list<Account>();
List<Account>updateReExposure=new list<Account>();


List<Business_Profile_Tracking__c> tsizeOfBusiness = [select ID,Account__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from Business_Profile_Tracking__c where (Name='Total AUM Updated'or name='Total AUM Created' OR Name='Clients with RE in Portfolio Created'or name='% of Clients with RE in Portfolio Updated' Or  name='Allocated to RE' or name='Allocated to RE Updated'or name='RE Exposure Through Updated'or name='RE Exposure Through Created') AND  Account__c in : Expiryoneyesr];
 for(Business_Profile_Tracking__c ct2 : tsizeOfBusiness)
     {
          if(ct2.name=='Total AUM Updated' || ct2.name=='Total AUM Created')
             Aumidlist.add(ct2.Account__c);
          if(ct2.name=='Clients with RE in Portfolio Created'||ct2.name=='% of Clients with RE in Portfolio Updated') 
               clientsinREidlist.add(ct2.Account__c);
           if(ct2.name=='Other RE Exposure Updated'|| ct2.name=='Other RE Exposure Created')     
                otherexposureidlist.add(ct2.Account__c);
          if(ct2.name=='Allocated to RE'|| ct2.name=='Allocated to RE Updated')     
                AllocatedtoREidlist.add(ct2.Account__c);
           if(ct2.name=='RE Exposure Through Updated'|| ct2.name=='RE Exposure Through Created')
                REExposureThroughidlist.add(ct2.Account__c);
        }
      AumACCist=[select id, BusSize_Total_AUM_Check__c,BusSize_Total_AUM__c  from Account where id in: Aumidlist];
      system.debug('AUM has changed in how many accounts ...'+AumACCist);
      for(Account acc1: AumACCist) 
      {
           acc1.BusSize_Total_AUM_Check__c=true;
           acc1.BusSize_Total_AUM__c='';
          updatedAUM.add(acc1);
       } 
       system.debug('/Updated Aum or notcheck the account'+updatedAUM);
       helper.run=false;
        update updatedAUM;
       helper.run=true;
       clientsinREACClist=[select id, of_Clients_with_RE_in_Portfolio_Check__c,TOP5_of_Clients_with_RE_in_Portfolio__c  from Account where id in: clientsinREidlist]; 
     system.debug('/%clients changed accounts/'+clientsinREACClist);

        for(Account acc2: clientsinREACClist) 
      {
           acc2.of_Clients_with_RE_in_Portfolio_Check__c =true;
          acc2.TOP5_of_Clients_with_RE_in_Portfolio__c =null;
          updatedClientsinReAcc.add(acc2);
       }
       system.debug('%clients updated accounts'+updatedClientsinReAcc);
       helper.run=false;
       update updatedClientsinReAcc;
       helper.run=true;
       
       otherexposureACClist=[select id,Othe_RE_Exposure_check__c,Othe_RE_Exposure__c from Account where id in: otherexposureidlist];
        for(Account acc3:otherexposureACClist) 
      {
           acc3.Othe_RE_Exposure_check__c  =true;
           acc3.Othe_RE_Exposure__c=null;
          updatedotherExposure.add(acc3);
        }
        helper.run=false;
        update updatedotherExposure;
         helper.run=true;
        
       AllocatedtoREACCist=[select id, Allocated_to_RE_check__c,Bus_SizeAllocated_to_RE__c from Account where id in: AllocatedtoREidlist];
         system.debug('alloacted to changed accounts'+AllocatedtoREACCist);
        for(Account acc4: AllocatedtoREACCist) 
      {
           acc4.Allocated_to_RE_check__c  =true;
           acc4.Bus_SizeAllocated_to_RE__c=null;
           updatedallocatetore.add(acc4);
        }
        system.debug('updated allocatedto accounts'+updatedallocatetore);
         helper.run=false;
        update updatedallocatetore;
     helper.run=true;
      REExposureThrougACCist=[select id,RE_Exposure_Through_check__c, Othe_RE_Exposure__c ,RE_Exposure_Through__c from Account where id in:REExposureThroughidlist];  
        system.debug('check the REexposure through changed accounts'+REExposureThrougACCist);
        for(Account acc5: REExposureThrougACCist) 
      {
           acc5.RE_Exposure_Through_check__c  =true;
           acc5.RE_Exposure_Through__c=null;
          acc5.Othe_RE_Exposure__c=null;
          updateReExposure.add(acc5);
        }
        system.debug('Updated accounts/11111'+updateReExposure);
        helper.run=false;
        update updateReExposure;
           helper.run=true;
           for Total New Sales Last Year:
          List<Business_Profile_Tracking__c> TtotalNewLastyear= [select ID,Account__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from Business_Profile_Tracking__c where (Name='Total Sales Last Year Updated'or name='Total Sales Last Year Created' OR Name='Fee Based (Non Commission) Created' or name='Fee Based (Non Commission) Updated') AND  Account__c in : Expiryoneyesr];
         
          list<id>totalsaleslstyearid=new list<id>();
          list<Account>totalsaleslstyearAcc= new list<Account>(); 
          list<Account>updatedtotalsaleslstyearAcc= new list<Account>(); 

         
         list<id>FeeBasednoncommisionid=new list<id>();
          list<Account>FeeBasednoncommisionAcc= new list<Account>(); 
           list<Account>updatedFeeBasednoncommisionAcc= new list<Account>(); 

           for(Business_Profile_Tracking__c ct1: TtotalNewLastyear)
            {
               if(ct1.name=='Total Sales Last Year Updated'||ct1.name=='Total Sales Last Year Created')
               totalsaleslstyearid.add(ct1.Account__c);
               if(ct1.name=='Fee Based (Non Commission) Created'||ct1.name=='Fee Based (Non Commission) Updated')
                FeeBasednoncommisionid.add(ct1.Account__c);
            }
           totalsaleslstyearAcc=[select id,Total_Sales_Last_Year_Check__c,Fee_Based_Non_Commission_check__c,Total_Sales_Last_Year_RIA__c from Account where id in:totalsaleslstyearid]; 
          
          system.debug('Ek ghar banauunga tere ghar ke samane'+totalsaleslstyearAcc);
           for(Account accounts : totalsaleslstyearAcc)
            {
                 accounts.Total_Sales_Last_Year_Check__c = TRUE;
                 accounts.Total_Sales_Last_Year_RIA__c=null;
                 updatedtotalsaleslstyearAcc.add(accounts);
               }  
               system.debug('duniya basaunga tere ghar ke samne'+updatedtotalsaleslstyearAcc);
                helper.run=false;
                update updatedtotalsaleslstyearAcc;
                helper.run=true;
                
            FeeBasednoncommisionAcc=[select id,Total_Sales_Last_Year_Check__c,Fee_Based_Non_Commission_check__c,Fee_Based_Non_Commission__c  from Account where id in:FeeBasednoncommisionid]; 
             system.debug('Chote se kasoor pe aisee ho khafa'+FeeBasednoncommisionAcc);
              for(Account accounts : FeeBasednoncommisionAcc)
            {
                 accounts.Fee_Based_Non_Commission_check__c  = TRUE;
                 accounts.Fee_Based_Non_Commission__c =null;
                 updatedFeeBasednoncommisionAcc.add(accounts);
               }  
               
               system.debug('Jeeven ke raste lambe hai sanaam'+updatedFeeBasednoncommisionAcc);
             helper.run=false;
            update updatedFeeBasednoncommisionAcc;
            helper.run=true;
        for stage of business section
       List<Business_Profile_Tracking__c> TStageOfBusiness= [select ID,Account__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from Business_Profile_Tracking__c where (Name='Business Stage Updated' or Name='How Can We Help Updated'or name='How would you describe your client base Updated' or name='How would you describe your client base Created'or name='Business Stage Created' or  name='How Can We Help Created') AND  Account__c in : Expirysixmonths];
       list<id>businesstageIdlist=new list<id>();
       list<account>businesstageAcclist=new list<Account>();
       list<Account>updatdBusinessStage=new List<Account>();
       
       list<id>otherbusinesstageIdlist=new list<id>();
       list<account>otherbusinesstageAcclist=new list <Account>();
       list<Account>updatdotherBusinessStage=new List<Account>();
       
       
       list<id>howcanwehelpIdlist=new list<id>();
       list<account>howcanwehelpAcclist=new list <Account>();
       list<Account>updatdhowcanwehelp=new List<Account>();
       
       
       list<id>HowwouldYouDescribeYourClientBaseidlist= new list<id>();
       list<Account>HowwouldYouDescribeYourClientAcclist=new list<Account>();
       list<Account>UpdatedHowwouldYouDescribeYourClientAccliat=new list<Account>();
       
      for(Business_Profile_Tracking__c x :TStageOfBusiness)
       {
              if(x.name=='Business Stage Updated' ||x.name=='Business Stage Created')
              businesstageIdlist.add(x.Account__c);
              
              if(x.name=='Other Business Stage Updated' ||x.name=='Other Business Stage Created')
              otherbusinesstageIdlist.add(x.Account__c);
              
              if(x.name=='How Can We Help Updated' ||x.name=='How Can We Help Created')
              howcanwehelpIdlist.add(x.Account__c);
              
              
              if(x.name=='How would you describe your client base Created' ||x.name=='How would you describe your client base Updated')
              HowwouldYouDescribeYourClientBaseidlist.add(x.Account__c);
      
          }
       businesstageAcclist =[select id,Business_Stage__c,Other_Business_Stage__c ,How_Can_We_Help__c ,Business_Stage_check__c,How_Can_We_Help_check__c from Account where id in :businesstageIdlist];
       system.debug('dekho reh sako ge na..tum bhi chain se'+businesstageAcclist );
       for(Account accounts : businesstageAcclist )
            {
             accounts.Business_Stage_check__c = TRUE;
            accounts.Business_Stage__c=null;
            accounts.Other_Business_Stage__c=null; 
             updatdBusinessStage.add(accounts);
             }
             system.debug('hum na rahe to yaad karo ge samajhe'+updatdBusinessStage);
             helper.run=false;
             if(updatdBusinessStage.size()>0)
            update updatdBusinessStage;
            helper.run=true;
            
       otherbusinesstageAcclist=[select id,Business_Stage__c,Other_Business_Stage__c ,How_Can_We_Help__c, Business_Stage_check__c,Other_Business_Stage_check__c,How_Can_We_Help_check__c from Account where id in : otherbusinesstageIdlist];
  
             for(Account accounts : otherbusinesstageAcclist)
            {
             accounts.Other_Business_Stage_check__c = TRUE;
             accounts.Other_Business_Stage__c=null;
             updatdotherBusinessStage.add(accounts);
             }
             helper.run=false;
            update updatdotherBusinessStage; 
            helper.run=true;
  
         howcanwehelpAcclist =[select id,Business_Stage__c,Other_Business_Stage__c ,How_Can_We_Help__c ,Business_Stage_check__c,How_Can_We_Help_check__c from Account where id in : howcanwehelpIdlist];
system.debug('you updated me in accounts'+howcanwehelpAcclist );
          for(Account accounts : howcanwehelpAcclist )
            {
             accounts.How_Can_We_Help_check__c= TRUE;
             accounts.How_Can_We_Help__c  =null;
             updatdhowcanwehelp.add(accounts);
             }
             system.debug('Updates updated updated'+updatdhowcanwehelp);
              helper.run=false;
              if(updatdhowcanwehelp.size()>0)
            update updatdhowcanwehelp;
            helper.run=true; 
           
      HowwouldYouDescribeYourClientAcclist=[select id,How_would_you_describe_your_client_base__c,HowWdUDescribURClientcheck__c from Account where id in: HowwouldYouDescribeYourClientBaseidlist];     
       for(Account accounts : HowwouldYouDescribeYourClientAcclist)
            {
             accounts.HowWdUDescribURClientcheck__c  = TRUE;
             accounts.How_Can_We_Help__c  =null;
             UpdatedHowwouldYouDescribeYourClientAccliat.add(accounts);
             }
             system.debug('Updates updated updated'+updatdhowcanwehelp);
              helper.run=false;
              if(UpdatedHowwouldYouDescribeYourClientAccliat.size()>0)
            update UpdatedHowwouldYouDescribeYourClientAccliat;
            helper.run=true; 
       
       
       
       
       
       
       
         Top 3 Product Categories used Last Year
         List<Business_Profile_Tracking__c> TTop2productcat= [select ID,Account__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from Business_Profile_Tracking__c where (Name='ETF Updated'or name='REIT Updated'or name='SMA Updated'or name='Hedge Fund Updated'or name='Actively Managed Funds Updated' or name='Independent Stocks and Bonds Updated' or name='Limited Partnership Updated'or name='Other Alternate Investments Updated'OR Name='ETF Created' or Name='REIT Created'or name='SMA Created'or name='Hedge Fund Created' or name='Actively Managed Funds Created'or name='Other Alternate Investments Created'or name='Independent Stocks and Bonds Created' or name='Limited Partnership  Created') AND  Account__c in : Expiryoneyesr];
         
          List<id>etfidlist=new List<id>();
          List<Account>etfAcclist=new List<Account>();
           List<Account>updatedetfAcclist=new List<Account>();
           
           List<id>OtherAlternateInvestmentsidlist=new List<id>();
          List<Account>OtherAlternateInvestmentsAcclist=new List<Account>();
           List<Account>updatedOtherAlternateInveAcclist=new List<Account>();
           
           
           List<id>reitfidlist=new List<id>();
          List<Account>reitfAcclist=new List<Account>();
           List<Account>updatedeReitfAcclist=new List<Account>();
           
           List<id>smaidlist=new List<id>();
          List<Account>smaAcclist=new List<Account>();
           List<Account>updatedesmaAcclist=new List<Account>();
           
           List<id>HedgeFundidlist=new List<id>();
          List<Account>HedgeFundAcclist=new List<Account>();
           List<Account>updatedHedgeFundAcclist=new List<Account>();
           
           List<id>ManagedFundsidlist=new List<id>();
          List<Account>ManagedFundsAcclist=new List<Account>();
           List<Account>updatedManagedFundsAcclist=new List<Account>();
           
           List<id>IndependentStocksandBondsidlist=new List<id>();
          List<Account>IndependentStocksandBondsAcclist=new List<Account>();
           List<Account>updatedeIndependentStocksandBondsAcclist=new List<Account>();
           
           List<id>LimitedPartnershipidlist=new List<id>();
          List<Account>LimitedPartnershipAcclist=new List<Account>();
           List<Account>updatedLimitedPartnershipAcclist=new List<Account>();
           
           
            for(Business_Profile_Tracking__c x : TTop2productcat)
            {
              if(x.name=='ETF Created' ||x.name=='ETF Updated')
              etfidlist.add(x.Account__c);
              
              if(x.name=='REIT Created' ||x.name=='REIT Updated')
              reitfidlist.add(x.Account__c);
              
              if(x.name=='SMA Created' ||x.name=='SMA Updated')
              smaidlist.add(x.Account__c);
                      
              if(x.name=='Hedge Fund Created' ||x.name=='Hedge Fund Updated')
              HedgeFundidlist.add(x.Account__c);
              
              if(x.name=='Actively Managed Funds Created' ||x.name=='Actively Managed Funds Updated')
              ManagedFundsidlist.add(x.Account__c);
              
              if(x.name=='Independent Stocks and Bonds Created' ||x.name=='Independent Stocks and Bonds Updated')
              IndependentStocksandBondsidlist.add(x.Account__c);
               
               if(x.name=='Limited Partnership  Created' ||x.name=='Limited Partnership Updated')
              LimitedPartnershipidlist.add(x.Account__c);
              
              if(x.name=='Other Alternate Investments Created' ||x.name=='Other Alternate Investments Updated')
              OtherAlternateInvestmentsidlist.add(x.Account__c);
                      
            }
         etfAcclist=[select id,ETF_check__c,TOP5_ETF__c from Account where id in:etfidlist];
          system.debug('ETF CHANGED ACCOUNTS ACCOUNTA ACCOUNTS'+etfAcclist);         
         for(Account acct: etfAcclist)
         {
         acct.ETF_check__c=true;
         acct.TOP5_ETF__c=null;   
         updatedetfAcclist.add(acct);
         }
         system.debug('ETF UPDATED ETF UPDATED ETF UPDATED'+updatedetfAcclist);
         helper.run=false;
         if(updatedetfAcclist.size()>0)
         update updatedetfAcclist;
        helper.run=true;
        reitfAcclist=[select id,REIT_check__c,TOP5_REIT__c from Account where id in:reitfidlist];
        system.debug('MERE mehboob kayamat hogi.REIT ACCOUNTS...aaj ruswa teri galiyon me'+reitfAcclist);
        for(Account acct:reitfAcclist)
         {
          acct.REIT_check__c=true;
         acct.TOP5_REIT__c=null;   
         updatedeReitfAcclist.add(acct);
         }
         system.debug('TERI GALI ME AATA SANAM /UPDATED REITmeri nazare to gila karti hai'+updatedeReitfAcclist);
         helper.run=false;
         if(updatedeReitfAcclist.size()>0)
         update updatedeReitfAcclist;
         helper.run=true;
        smaAcclist=[select id,SMA_check__c,TOP5_SMA__c  from Account where id in:smaidlist]; 
       system.debug('khatm bas aaj ye p!!!!SMA SMA SMA ACCOUNTS/'+smaAcclist);
       
        for(Account acct: smaAcclist)
         {
          acct.SMA_check__c=true;
          acct.TOP5_SMA__c=null;   
         updatedesmaAcclist.add(acct);
         }
         system.debug('AAJ RUSAWA TERI GALIYON ME ############UPDATED SMA UPDATED SMA'+updatedesmaAcclist);
         helper.run=false;
         if(updatedesmaAcclist.size()>0)
         update updatedesmaAcclist;
         helper.run=true;
        HedgeFundAcclist=[select id,Hedge_Fund_check__c,Acct_Hedge_Fund__c  from Account where id in: HedgeFundidlist]; 
       system.debug('NA TUM HAME JANO>HEDGE FUND>>NA HUM TUMHE JANE'+HedgeFundAcclist);
        for(Account acct: HedgeFundAcclist)
         {
          acct.Hedge_Fund_check__c=true;
         acct.Acct_Hedge_Fund__c=null;   
         updatedHedgeFundAcclist.add(acct);
         }
         system.debug('NAZAR BA  gayi hai>> updated hedge fund'+updatedHedgeFundAcclist);
         helper.run=false;
         if(updatedHedgeFundAcclist.size()>0)
         update updatedHedgeFundAcclist;
         helper.run=true;
        
        ManagedFundsAcclist=[select id,Actively_Managed_Funds_check__c,Actively_Managed_Funds__c  from Account where id in : ManagedFundsidlist];
       
        system.debug('magar lagta hai mera humdum mil gaya>>ManagedFundsAcclist<<'+ManagedFundsAcclist);
         for(Account acct: ManagedFundsAcclist)
         {
          acct.Actively_Managed_Funds_check__c =true;
          acct.Actively_Managed_Funds__c  =null;   
         updatedManagedFundsAcclist.add(acct);
         }
         system.debug('yeh mausam yeh rat chup hai/updated managedfunds'+updatedManagedFundsAcclist);
         helper.run=false;
         if(updatedManagedFundsAcclist.size()>0)
         update updatedManagedFundsAcclist;
         helper.run=true;
       IndependentStocksandBondsAcclist=[select id,Independent_Srocks_and_Bonds_check__c,TOP5_Independent_Srocks_and_Bonds__c from Account where id in : IndependentStocksandBondsidlist];
      system.debug('yeh mausam yeh rat chup hai/Independant stocks'+IndependentStocksandBondsAcclist);

        for(Account acct: IndependentStocksandBondsAcclist)
         {
          acct.Independent_Srocks_and_Bonds_check__c =true;
         acct.TOP5_Independent_Srocks_and_Bonds__c=null;   
         updatedeIndependentStocksandBondsAcclist.add(acct);
         }
     system.debug('yeh mausam yeh rat chup hai/Independant stocks'+updatedeIndependentStocksandBondsAcclist);

         helper.run=false;
         if(updatedeIndependentStocksandBondsAcclist.size()>0)
        update updatedeIndependentStocksandBondsAcclist;
        helper.run=true;
        
       LimitedPartnershipAcclist=[select id, Limited_Partnerships_check__c,Acct_Limited_Partnerships__c  from Account where id in: LimitedPartnershipidlist];
      system.debug('yeh mausam yeh rat chup hai/limited partner ship'+LimitedPartnershipAcclist);

       for(Account acct: LimitedPartnershipAcclist)
         {
              acct.Limited_Partnerships_check__c =true;
              acct.Acct_Limited_Partnerships__c =null;   
             updatedLimitedPartnershipAcclist.add(acct);
         }
      system.debug('yeh mausam yeh rat chup hai/limited partner ship'+updatedLimitedPartnershipAcclist);

          helper.run=false;
          if(updatedLimitedPartnershipAcclist.size()>0)
        update updatedLimitedPartnershipAcclist;
        helper.run=true;
        
       OtherAlternateInvestmentsAcclist=[select id,Other_Alternative_Investments_check__c,Acct_Other_Alternative_Investments__c from Account where id in: OtherAlternateInvestmentsidlist];
        
        for(Account acct: OtherAlternateInvestmentsAcclist)
         {
              acct.Other_Alternative_Investments_check__c =true;
             acct.Acct_Other_Alternative_Investments__c  =null;   
             updatedOtherAlternateInveAcclist.add(acct);
         }
          helper.run=false;
          if(updatedOtherAlternateInveAcclist.size()>0)
        update updatedOtherAlternateInveAcclist;
           helper.run=true;   
         Have you used a non-listed REIT before? section
         list<id>Nonlistedreitid=new list<id>();
         list<Account>NonlistedreitACC=new list<Account>();
         list<Account>UpdateNonlistedREITs=new list<Account>();
         
         list<id>howlongid=new list<id>();
         list<Account>howLongACC=new list<Account>();
         list<Account>UpdatedHowlongACC=new list<Account>();
         
        list<id>whatreitid=new list<id>();
         list<Account>whatreitACC=new list<Account>();
         list<Account>UpdatewhatREITs=new list<Account>();
         
         List<Business_Profile_Tracking__c> Thaveyouusednonlisted= [select ID,Account__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from Business_Profile_Tracking__c where (Name='What REITs Updated' or name='What REITs Created') AND  Account__c in : Expiryoneyesr];
            for(Business_Profile_Tracking__c x: Thaveyouusednonlisted)
            {
              if(x.name=='Used REIT Before Created' ||x.name=='Used REIT Before Updated')
              Nonlistedreitid.add(x.Account__c);
              
              if(x.name=='How Long Created' ||x.name=='How Long Updated')
              howlongid.add(x.Account__c);
              
              if(x.name=='What REITs Created' ||x.name=='What REITs Updated')
              whatreitid.add(x.Account__c);
                         
            }
        NonlistedreitACC=[select id,Used_REIT_Before_value_changed_check__c,Used_REIT_Before__c from Account where id in: Nonlistedreitid];    
      system.debug('yeh mausam yeh rat chup hai/non listed reit accounts'+NonlistedreitACC);
          
          
            for(Account act: NonlistedreitACC)
            {
             act.Used_REIT_Before_value_changed_check__c = TRUE;
            act.Used_REIT_Before__c=null;
             UpdateNonlistedREITs.add(act); 
                       
            }
        system.debug('Tumsekhaa nahi di/non listed reit accounts'+UpdateNonlistedREITs);
           helper.run=false;
            if(UpdateNonlistedREITs.size()>0)
            update UpdateNonlistedREITs;
            helper.run=true;
            
         howLongACC=[select id,Used_REIT_Before_value_changed_check__c,Used_REIT_Before__c from Account where id in: howlongid];   
         system.debug('Jeevan ke safar me rahi howLongACC accounts'+howLongACC);
  
            for(Account act: howLongACC)
            {
            act.How_Long_check__c = TRUE;
            act.How_Long__c =null;
            UpdatedHowlongACC.add(act); 
          }
          system.debug('Jeevan ke safar me rahi / updated/howLongACC accounts'+UpdatedHowlongACC);

            helper.run=false;
            if(UpdatedHowlongACC.size()>0)
            update UpdatedHowlongACC;
            helper.run=true;
         whatreitACC=[select id,What_REITs_check__c,What_REITs__c from Account where id in: whatreitid];   
      system.debug('Jeevan ke safar me rahi /  whatreitACC'+ whatreitACC);

            for(Account act: whatreitACC)
            {
            act.What_REITs_check__c = TRUE;
            act.What_REITs__c  =null;
            UpdatewhatREITs.add(act); 
            }
   system.debug('Jeevan ke safar me rahi /  whatreitACC'+ UpdatewhatREITs);

             helper.run=false;
            if(UpdatewhatREITs.size()>0)
            update UpdatewhatREITs; 
            helper.run=true;
   
         Do you think you would like to invest in our products? section
      
      list<id>wouldyouinvestid=new list<id>();
      list<Account>wouldyouinvestACC=new list<Account>();
      list<Account>UpdatedwouldyouinvestAcc=new list<Account>();
      
      list<id>whynotid=new list<id>();
      list<Account>whynotACC=new list<Account>();
      list<Account>UpdatedwhynotAcc=new list<Account>();
      
      
      List<Business_Profile_Tracking__c>TInteresttoinvest= [select ID,Account__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from Business_Profile_Tracking__c where (Name='Would You Invest Updated' OR Name='WHy Not Updated'or name='WHy Not Created'or name='Would You Invest Created') AND  Account__c in : Expirysixmonths];
            for(Business_Profile_Tracking__c x: TInteresttoinvest)
            {
                if(x.name=='Would You Invest Created' || x.name=='Would You Invest Updated')
                wouldyouinvestid.add(x.Account__c);
                
                if(x.name=='WHy Not Created' || x.name=='WHy Not Updated')
                whynotid.add(x.Account__c);
                
            }
         wouldyouinvestACC =[select id,Would_you_invest_check__c,Would_you_invest__c from Account where id in: wouldyouinvestid];
         system.debug('Jeevan ke safar me rahi /  wouldyouinvestACC '+ wouldyouinvestACC );
   
           
            for(Account accounts : wouldyouinvestACC )
            {
            accounts.Would_you_invest_check__c = TRUE;
            accounts.Would_you_invest__c =null;
            UpdatedwouldyouinvestAcc.add(accounts);  
             }
             system.debug('Jeevan ke safar me rahi /  UpdatedwouldyouinvestAcc'+ UpdatedwouldyouinvestAcc);
             helper.run=false;
             if(UpdatedwouldyouinvestAcc.size()>0)
         update UpdatedwouldyouinvestAcc;
         helper.run=true;
      whynotACC =[select id,Why_Not_check__c,Inv_Why_Not__c from Account where id in: whynotid];
     system.debug('khoya khoya chand khoya chand / whynotACC '+ whynotACC );
     
      for(Account accounts : wouldyouinvestACC )
            {
            accounts.Why_Not_check__c= TRUE;
            accounts.Inv_Why_Not__c =null;
            accounts.Why_Not_Other__c=null; 
            UpdatedwhynotAcc.add(accounts);  
            
            }
       
    system.debug('hum mit chale jinake liye/UpdatedwhynotAccbin kuch kahe woh chup chup rahe / '+ UpdatedwhynotAcc);

            helper.run=false;
            if(UpdatedwhynotAcc.size()>0)
            update UpdatedwhynotAcc;
            helper.run=true;
  What changes have you made in the volatility of the Products section
 
            list<id>Volatilityproductid=new list<id>();
            list<Account>VolatilityproducACC=new list<Account>();
            list<Account>UpdateVolatility=new list<Account>();
         List<Business_Profile_Tracking__c>TvolatilityProduct= [select ID,Account__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from Business_Profile_Tracking__c where (Name='Changes in Volitality Updated' or name='Changes in Volitality Created') AND  Account__c in : Expirysixmonths];
            for(Business_Profile_Tracking__c x: TvolatilityProduct)
            {
            Volatilityproductid.add(x.Account__c);
            }

         VolatilityproducACC =[select id,Changes_in_Volitality_check__c from Account where id in: Volatilityproductid];
              system.debug('hum mit chale jinake liye/VolatilityproducACC bin kuch kahe woh chup chup rahe / '+ VolatilityproducACC );

            for(Account accounts : VolatilityproducACC )
            {
            accounts.Changes_in_Volitality_check__c= TRUE;
          accounts.What_Changes_in_Volitality__c=null;
            UpdateVolatility.add(accounts);  
             }
          system.debug('hum mit chale jinake liye/UpdateVolatilitybin kuch kahe woh chup chup rahe / '+ UpdateVolatility);
   
          helper.run=false;
         if(UpdateVolatility.size()>0)
         update UpdateVolatility;
         helper.run=true;
 Income Products Used (added by kunal)
    list<id>income=new list<id>();
            list<Account>incomeacc=new list<Account>();
            list<Account>Updateincome=new list<Account>();

List<Business_Profile_Tracking__c>Tincome= [select ID,Account__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from Business_Profile_Tracking__c where (Name='Income Products Used Updated'or name='Income Products Used Created') AND  Account__c in : Expirysixmonths];
            for(Business_Profile_Tracking__c xyz: Tincome)
            {
            income.add(xyz.Account__c);
            }
incomeacc =[select id,Income_Products_UsedCheck__c from Account where id in: income];

for(Account accountsss : incomeacc)
{
accountsss.Income_Products_UsedCheck__c = TRUE;
updateincome.add(accountsss);
}
helper.run = FALSE; 
update updateincome;
     }*/      
  }