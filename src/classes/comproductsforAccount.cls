public with sharing class comproductsforAccount {
    public string color;
    public comproductsforAccount() {
    productval1 = new list<string>();
    Competitor1 = new list<string>();
    if(ApexPages.CurrentPage().getParameters().get('id')!=null)
        conrec = ApexPages.CurrentPage().getParameters().get('id');
    else
        conrec =myValue;
        system.debug('---------myValue-------------'+myValue);
        
   // system.debug('I am in first constructor..'+conrec );
        //cmpr=[select id,Competitors__c,Product__c,Products1__c,Selected_Producs__c from Competitor_Product__c where Account_Name__c =:conrec limit 1];
        lst = [select id,name,Competitors__c,Product__c,Selected_Producs__c from Competitor_Product__c where Account_Name__c =:conrec  ];
        // lst1= new list<Competitor_Product__c>();
        //lst = [select id,name,Competitors__c,Product__c from Competitor_Product__c];
        editValue ='';
          system.debug('I am in first constructor..'+conrec );
        if(lst==null)
        {
            addvalues = true;
            
        }else
        {
            selectedvalues = true;
            addvalues = true;
            Editpan = false;
        }
    
    
      }
   
     public void call(){
     
      system.debug('---------callmethod-------------'+myValue);
        
       if(ApexPages.CurrentPage().getParameters().get('id')!=null)
        conrec = ApexPages.CurrentPage().getParameters().get('id');
    else
        conrec =myValue;
        system.debug('---------myValue-------------'+myValue);
        
   // system.debug('I am in first constructor..'+conrec );
        //cmpr=[select id,Competitors__c,Product__c,Products1__c,Selected_Producs__c from Competitor_Product__c where Account_Name__c =:conrec limit 1];
        lst = [select id,name,Competitors__c,Product__c,Selected_Producs__c from Competitor_Product__c where Account_Name__c =:conrec ];
        // lst1= new list<Competitor_Product__c>();
        //lst = [select id,name,Competitors__c,Product__c from Competitor_Product__c];
        editValue ='';
          system.debug('I am in first constructor..'+conrec );
        if(lst==null)
        {
            addvalues = true;
            
        }else
        {
            selectedvalues = true;
            addvalues = true;
            Editpan = false;
        }

     }
   
    public Competitor_Product__c cmpt{set;get;} 
    public id j{get; set;}
    public String[] Productval1{set;get;} 
    public String[] Competitor1 {set;get;} 
    public boolean toselect{set;get;}
    public list<SelectOption> prod1{set;get;}
    public boolean selectedvalues{set;get;}
    public String[] Productval{set;get;} 
    public boolean addvalues{set;get;}
    public boolean Editpan{set;get;}
    public Competitor_Product__c cmpr{set;get;} 
    public id edt{set;get;}
    public id id1{set;get;}
    Public PageReference pageRef{set;get;}
    public List<Competitor_Product__c> lst{get; set;}
    public List<Competitor_Product__c> lst1{get; set;}
    public string conrec{ get; set; }
    public List<SelectOption> prod{set;get;}
    public String[] Competitor {set;get;}
    public List<string> tempList{set;get;}
     public List<string> complist{set;get;}
    public Map<String, Competitor_code__c> customSettMap {set;get;}
    public String editValue {set;get;}
    public String quoteId1;
    public String myValue;
    public List<SelectOption> comp1 
    {
        get{
            List<SelectOption> comp1 = new List<SelectOption>();
            complist = new List<string>();
           string[] ct=(cmpt.Competitors__c).split(';');
            for(string c:ct){
            complist.add(c);
            //comp1.add(new SelectOption(ct,ct));
            }
            for(string st:complist){
            comp1.add(new SelectOption(st ,st));
            }
            return comp1 ;
        }set;
    }
    
    public comproductsforAccount(ApexPages.StandardController controller)
     {
        
        productval1 = new list<string>();
        Competitor1 = new list<string>();
        conrec = ApexPages.CurrentPage().getParameters().get('id');
        system.debug('I am in second constructor..'+conrec );
        //cmpr=[select id,Competitors__c,Product__c,Products1__c,Selected_Producs__c from Competitor_Product__c where Account_Name__c =:conrec limit 1];
        lst = [select id,name,Competitors__c,Product__c,Selected_Producs__c from Competitor_Product__c where Account_Name__c =:conrec ];
        // lst1= new list<Competitor_Product__c>();
        //lst = [select id,name,Competitors__c,Product__c from Competitor_Product__c];
        editValue ='';
        if(lst==null)
        {
            addvalues = true;
            
        }else
        {
            selectedvalues = true;
            addvalues = true;
            Editpan = false;
        }
        
    }
    
    public void savechanges()
    {   
        system.debug('-------Svemethod-------'+conrec);
       // system.debug('Can you please tell me the values of..'+Competitor[0]);
        //String Comp=Competitor[0];
        system.debug(' Also Can you please tell me the values of..'+Productval);
        lst1= [select id,name,Competitors__c,Product__c,Selected_Producs__c from Competitor_Product__c where Account_Name__c =:conrec and Competitors__c=:Competitor[0]];    
        system.debug('Give me the List of all comps'+lst1);
        if(lst1.size()>0)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Competitor already exists for this Account'));
        
        
        }
      else
      {
          string a = '' ;
          for(string q:Competitor)
          {
          
          a = q+';'+a;
         //   a = q+'\n'+a;
          }
            string b ='';
            for(string p:Productval)
            {
                b = p+';'+b;
            }
        Competitor_Product__c ct= new Competitor_Product__c();
       // ct.Competitors__c=Competitor[0];
       // system.debug('get me the values of competitors'+ct.Competitors__c);
        ct.Competitors__c = a;
        ct.Selected_Producs__c=b;
        ct.Account_Name__c=conrec;
        if (Schema.sObjectType.Competitor_Product__c.isCreateable())
        insert ct;
        system.debug('This is my products'+ct);
       } 
        lst = [select id,name,Competitors__c,Product__c,Selected_Producs__c from Competitor_Product__c where Account_Name__c =:conrec ];        
        
        
        addvalues = true;
        toselect = false;
        selectedvalues = true;
        Editpan = false;
     
    }
    
    public void Cancelupdate()
    {  
         selectedvalues = true;
            addvalues = true;
            Editpan = false;
    }
    
    public void addtotable()
    {
       
        addvalues = false;
        toselect = true;
        selectedvalues = true;
        Editpan = false;
    }
    
    public void editrow()
    {
        
        id id1 = ApexPages.currentPage().getParameters().get('edt');
        editValue =id1;
        system.debug('--------editrow-------------->'+id1);
        if(id1 != null )
        {
        
        system.debug('------editrow if Block--------->'+id1);
        addvalues = false;
        selectedvalues = false;
        Editpan = true;
        
        cmpt=[select id,Account_Name__c,Product__c,Products1__c,Selected_Producs__c,Competitors__c  from Competitor_Product__c where id=:id1];
            Prod1  = new list<SelectOption>();
            customSettMap = new Map<String, Competitor_code__c>();
            customSettMap = Competitor_code__c.getall();
            Map<String, List<String>> competitorMap = new Map<String, List<String>>();
            for(Competitor_code__c cmc : customSettMap.values())
            {
                if(competitorMap.containsKey(cmc.Competitor_Name__c))
                {
                    competitorMap.get(cmc.Competitor_Name__c).add(cmc.Product__c );
                }
                else
                {
                    competitorMap.put(cmc.Competitor_Name__c, new List<String>());       
                    competitorMap.get(cmc.Competitor_Name__c).add(cmc.Product__c );
                }
            }
            
            system.debug('------------competitorMap--------------->'+competitorMap);
            tempList = new List<String>();
                String[] str = (cmpt.Competitors__c).split(';');
                string ac=cmpt.Selected_Producs__c;
                if(str!=null)
                {
                for(String s : str)
                {
                    if(competitorMap.containsKey(s))
                    {
                        tempList.addAll(competitorMap.get(s));
                    }
                }
              } 
              if(tempList.size()>0) 
              {
                for(String st : tempList)
                {
                    if(ac.contains(st))
                    {
                    color='#BDBDBD';
                    }
                    else
                    {
                    color='white';
                    }
                    SelectOption products = new SelectOption(st,'<option value="' + st + '" style="font-weight:bold;background-color:' + color + ';">' + st + '</option>'); 
                    products.setEscapeItem(false);
                    Prod1.add(products);
                }
                
              }  
         }
          
    }

    public void deleterow()
    {    
        id id1 = ApexPages.currentPage().getParameters().get('dlt');
        cmpr= [select id,name,Competitors__c,Product__c from Competitor_Product__c where id=:id1];
          if (Competitor_Product__c.sObjectType.getDescribe().isDeletable()) 
                {
                        delete  cmpr;
                }
        lst = [select id,name,Competitors__c,Product__c,Selected_Producs__c from Competitor_Product__c where Account_Name__c =:conrec];
        selectedvalues = true;
        addvalues = true;
        Editpan = false; 
    }
    
    public void UpdateRow()
    {   
      system.debug('check the Productval1'+Productval1);
        /*try{ 
        controller.save();}
        catch(exception e){}*/
        System.debug('Check the value here..'+editValue);
        id i = ApexPages.currentPage().getParameters().get('edt');
        system.debug('-------------i-in update------------>'+editValue);
        if (editValue!= null)
        {
            Competitor_Product__c c = [select id,Name,Competitors__c ,Account_Name__c,Selected_Producs__c from Competitor_Product__c where id=:editValue];
          
          string a ='' ;
          for(string q:Competitor1)
          {
          system.debug('string q value'+q);
         a = q+';'+a;
       //  a = q+'\n'+a;
          }
           
           string b ='';
           string abc=c.Selected_Producs__c ;
           if(abc!=null)
           {
            for(string p:Productval1)
            {
                b = p+';'+b;
            }
           }
           if(abc==null)
           {
            for(string p:Productval1)
            {
                b = p+';'+b;
            }
           } 
            system.debug('>>>>>>>>>'+a);
            //c.Competitors__c =a;
            c.Selected_Producs__c=c.Selected_Producs__c+b;
            If(c.Competitors__c!='')
            {
            System.debug('check the competitors'+c.Competitors__c);
            if (Schema.sObjectType.Competitor_Product__c.isUpdateable()) 
            update c;
            }
            else
            {
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please select the Competitors');
           apexpages.addmessage(myMsg);
            }
            system.debug('check the c'+c );
        }    
        string j='';
        lst = [select id,name,Competitors__c,Product__c,Selected_Producs__c from Competitor_Product__c where Account_Name__c =:conrec];
        selectedvalues = true;
        addvalues = true;
        Editpan = false;
          
   }
    public void deleteproduct()
    {   
        system.debug('check the Productval1'+Productval1);
        System.debug('Check the value here..'+editValue);
        id i = ApexPages.currentPage().getParameters().get('edt');
        system.debug('-------------i-in update------------>'+editValue);
        if (editValue!= null)
        {
          
          Competitor_Product__c c = [select id,Name,Competitors__c ,Account_Name__c,Selected_Producs__c from Competitor_Product__c where id=:editValue];
          
           string b ='';
           string abc=c.Selected_Producs__c ;
           If(abc!=null)
           {
            for(string p:Productval1)
            {
                string p1=p+';';
                c.Selected_Producs__c=c.Selected_Producs__c.remove(p1);
            }
           }
           If(c.Competitors__c!='')
            {
            System.debug('check the competitors'+c.Competitors__c);
            if (Schema.sObjectType.Competitor_Product__c.isUpdateable())           
            update c;
            }
            else
            {
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please select the Competitors');
           apexpages.addmessage(myMsg);
            }
            system.debug('check the c'+c );
        }    
        string j='';
        lst = [select id,name,Competitors__c,Product__c,Selected_Producs__c from Competitor_Product__c where Account_Name__c =:conrec];
        selectedvalues = true;
        addvalues = true;
        Editpan = false;
          
   }

    public void Cancelchoice()
    {    
        addvalues = true;
        toselect = false;
        selectedvalues = true;
        Editpan = false; 
    }
    
    public List<SelectOption> comp 
    {
        get{
            List<SelectOption> comp = new List<SelectOption>();
            Schema.DescribeFieldResult optionFieldDescription = Competitor_Product__c.Competitors__c.getDescribe();
            for(Schema.PicklistEntry pleOptions : optionFieldDescription.getPicklistValues()){
                comp.add(new SelectOption(pleOptions.getvalue(),pleOptions.getLabel()));
            }
            return comp ;
        }set;
    }
    public pagereference updateProduct(){
    system.debug('Welcome to the UpdataeProduct method');
         if(Competitor!=null){
             prod = new List<SelectOption>();
             List<Competitor_code__c> t = [select id,Competitor_Name__c, Product__c from Competitor_code__c where Competitor_Name__c IN:(Competitor) ];
             t.sort();
             system.debug('----------------'+t);
             system.debug('----------------'+Competitor);
             for(Competitor_code__c pro : t){
                 Prod.add(new SelectOption(pro.Product__c ,pro.Product__c ));
                 
             } 
         }return null;
         }
    
     public String getquoteId1() {
        system.debug('----------quoteId1------------ '+quoteId1);
        return quoteId1;
    }
    
     public void setquoteId1 (String s) {
         myValue=s;
         call();
         system.debug('---------s-------------'+s);
     } 
    
}