@isTest
private class Test_ERApprovalClass
{
    public static testMethod void ERApprovalClass()
      {
        Account acc = new Account();
        acc.Name = 'Test Prospect Account';
        Insert acc;
        
        Contact c = new Contact();
        c.firstname='RSVP';
        c.lastname='test';
        c.accountid=acc.id;
        insert c;
        
        system.assertequals(c.accountid,acc.id);

        Contact ct = new Contact();
        ct.firstname='Attend';
        ct.lastname='testing';
        c.accountid=acc.id;
        ct.email='skalamkar@colecapital.com';
        insert ct;
        acc.Event_Approver__c=ct.id;
        User us = [SELECT Id FROM User limit 1];  
        
        Campaign cam = new Campaign();
        cam.name='Dinearound-National Conference-Cole Capital';
        cam.status='Pending';
        cam.status='Approved';
        cam.status='Rejected';
        cam.isActive=true;
        cam.parentid=null;
        insert cam;
        
        CampaignMemberStatus cms1 = new CampaignMemberStatus(CampaignId=cam.Id, Label='Pending', HasResponded=true,sortorder=3);            
        insert cms1;
      
        Event_Automation_Request__c ear = new Event_Automation_Request__c();
        ear.Name='BDE13006';
        ear.Event_Name__c='testeventname';
        ear.Start_Date__c=System.Today() + 1;
        ear.End_Date__c=System.Today() + 1;
        ear.Broker_Dealer_Name__c=acc.id;
        ear.Event_Location__c='testlocation';
        ear.Key_Account_Manager_Name_Requestor__c=us.id;
        ear.Event_Venue__c='testvenue';
        ear.Campaign__c=cam.id;
        insert ear;
        
        CampaignMember member= new CampaignMember();
        member.CampaignId = cam.id;
        member.ContactId = c.id;
        member.Status='Pending';
        insert member;
        
        CampaignMember member1= new CampaignMember();
        member1.CampaignId = cam.id;
        member1.ContactId = ct.id;
        member1.Status='Pending';
        insert member1;
        
       system.assertequals(member1.ContactId,ct.id);
          
           
      ERApprovalClass cttt= new ERApprovalClass ();  
      cttt.sMail='skalamkar@colecapital.com'; 
      cttt.calllist='testeventname';
      ERApprovalClass.MyWrapper ct1=new  ERApprovalClass.MyWrapper();
      cttt.AdviserCallList();
      cttt.doAction();
      cttt.doAction1();
      cttt.doAction2();
      cttt.doAction3();
      //cttt.getItems();
      cttt.campPendList=[select id, RecordTypeId, CampaignId ,Contact.FirstName,Contact.name, Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c,Broker_Dealer_Name__c ,contact.account.name from CampaignMember where CampaignId =:ear.Campaign__c and status='Pending' and Contact.Account.Event_Approver__r.email=:cttt.sMail];
      cttt.campApprList=[select id, RecordTypeId, CampaignId ,Contact.FirstName,Contact.name, Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c,Broker_Dealer_Name__c,contact.account.name from CampaignMember where CampaignId =:ear.Campaign__c and status='Approved' and Contact.Account.Event_Approver__r.email=:cttt.sMail];
      cttt.campDeclList=[select id, RecordTypeId, CampaignId ,Contact.FirstName,Contact.name, Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c,Broker_Dealer_Name__c,contact.account.name from CampaignMember where CampaignId =:ear.Campaign__c and status='Declined' and Contact.Account.Event_Approver__r.email=:cttt.sMail];
      cttt.approverAccount=acc.Name; 
      cttt.approvername=ct.name; 
      
      //cttt.CallListValues.add(new SelectOption('--None--','--None--'));
      // cttt.CallListValues.add(new SelectOption('testeventname','testeventname'));
      //cttt.earList=[select id,]
    
          ct1.wName='Testmywrapper';
          ct1.wAccountName='Test Prospect Account';
          ct1.Wcity='Florida';
          ct1.wState='CA';
          ct1.wApproved=true;
          ct1.wComment='These contacts have been approved';
          ct1.wId=acc.id;
                      
     cttt.wcampList.add(ct1);
     
   
    }
  
}