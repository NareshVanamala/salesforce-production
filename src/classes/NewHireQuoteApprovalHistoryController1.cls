public with sharing class NewHireQuoteApprovalHistoryController1
 {
    public String quoteId1;
    public String str{get;set;}
    public set<id>Subcaseid;
    public list<ProcessInstanceStep >plist;
    public list<ProcessInstance>plist1;
    Public list<Employee__c>quote;
    Public list<Asset__c>quoteasset;
    public set<id>ProcessCaseid;
    public map<id,string>StepToComment;
    public map<id,String> subjectCaseMap;
    public map<id, list<ProcessInstanceHistory>> processinstancestepmap;
    public list<MyWrapper> wcampList;
    public String myValue;
  
   /* public QuoteApprovalHistoryController()
   {
       system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> '+quoteId1);
         Subcaseid=new set<id>();
         quote=new list<Case>();
         ProcessCaseid= new set<id>();
         subjectCaseMap= new map<id,String>();
         processinstancestepmap= new map<id, list<ProcessInstanceStep>>();
         subjectCaseMap= new map<id,String>();
         plist= new list<ProcessInstanceStep>();
         plist1= new list<ProcessInstance>();
         wcampList= new list<MyWrapper>();
    }*/
    
    public List<MyWrapper> getwcampList () {
        
         system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> '+quoteId1);
         Subcaseid=new set<id>();
         quote=new list<Employee__c>();
         ProcessCaseid= new set<id>();
         subjectCaseMap= new map<id,String>();
         processinstancestepmap= new map<id, list<ProcessInstanceHistory>>();
         subjectCaseMap= new map<id,String>();
         plist= new list<ProcessInstanceStep>();
         StepToComment=new map<id,String>();
         plist1= new list<ProcessInstance>();
         wcampList= new list<MyWrapper>();
        //quoteId1='500P0000003RFli';
         wcampList= new list<MyWrapper>();
         quoteasset=[select id,Access_Name__c,ActiveDirectorygroupformula__c,Access_Type__c from Asset__c where Employee__c=:myValue and Access_Name__c!=null]; 
         system.debug('>>>>>>>>>>>>>> '+myValue);
         system.debug('get the list size of cases'+quote.size());  
           
             for(Asset__c c:quoteasset)
             {
                
                 
                 Subcaseid.add(c.id);
                 
                 if((c.Access_Type__c=='Active Directory')&&(c.Access_Name__c!='Email Account'))
                  subjectCaseMap.put(c.id,c.ActiveDirectorygroupformula__c);
                 else 
                  subjectCaseMap.put(c.id,c.Access_Type__c + c.Access_Name__c );
             }
           system.debug('Check the map'+subjectCaseMap);   
           
         if(Subcaseid.size()>0)
        {
            plist1=[SELECT CreatedById,CreatedDate,Id,IsDeleted,LastModifiedById,LastModifiedDate,Status,SystemModstamp,TargetObjectId,TargetObject.Name,(SELECT SystemModstamp, StepStatus, ProcessInstanceId, OriginalActorId,OriginalActor.name, Id, CreatedDate, CreatedById, Comments, ActorId,Actor.name FROM StepsAndWorkitems order by StepStatus) FROM ProcessInstance where TargetObjectId in:Subcaseid ];
        }
         system.debug('Check the list of processInstance..'+plist1);  
      /*  if(plist1.size()>0)
        {
            For(ProcessInstance Pc1:plist1){
              ProcessCaseid.add(Pc1.id);
              system.debug('>>>>>>>>>>>pc1>>>>>>>>>>>>> '+pc1);
              }
              //subjectCaseMap.put(Pc1.id,TargetObject.subject);
             plist=[Select SystemModstamp, StepStatus, ProcessInstanceId, OriginalActorId, Id, CreatedDate, CreatedById, Comments, ActorId From ProcessInstanceStep where  ProcessInstanceId in:ProcessCaseid and (Stepstatus='Approved' or Stepstatus='Rejected' or Stepstatus='Pending' or StepStatus='Started') order by StepStatus,ActorId ];
             
        }
      */
       
        for(processinstance plist2:plist1)
        {
        for(ProcessInstanceHistory ri:plist2.StepsAndWorkitems)
        {   
           if(ri.stepstatus=='Started')
           {
          StepToComment.put(ri.ProcessInstanceId,ri.Comments);
           }
           
           if(processinstancestepmap.containsKey(ri.ProcessInstanceId))
                 processinstancestepmap.get(ri.ProcessInstanceId).add(ri);
           else
               processinstancestepmap.put(ri.ProcessInstanceId, new List<ProcessInstanceHistory>{ri});
             
        }
        }
        system.debug('Check the map...'+processinstancestepmap);  
         MyWrapper w;
       for(ProcessInstance acc:plist1)
        {
            w = new MyWrapper();
            w.wAccseesRequestName=subjectCaseMap.get(acc.TargetObjectId) ;
            w.wcaption=StepToComment.get(acc.id);
            w.wStatus= acc.status;
          //  w.wApprover= OriginalActorname.get(acc.id);
          //  w.wActualApprover=ActorName.get(acc.id); 
          //  w.wDate= SystemModstampvalue.get(acc.id);
         //   w.wOverallStatus=acc.status;
         //   w.wcomments=commentsvalue.get(acc.id);
            w.wConList=processinstancestepmap.get(acc.id);
            system.debug('check the conlist'+w.wConList);
            wcampList.add(w);
        }
         
        system.debug('check the wrapperlist..'+wcampList);
        return wcampList;
    
    
    }
      
    public String getquoteId1() {
        system.debug('>>>>>>>>>>>>>> '+quoteId1);
        return quoteId1;
    }
    
     public void setquoteId1 (String s) {
         myValue=s;
     } 
  
     
  public class MyWrapper
  { 

    public String wAccseesRequestName{get;set;}
    public String wStatus{get;set;}
    public String wApprover{get;set;}
    public String wActualApprover{get;set;}
    public Datetime wDate{get;set;}
    public String wcomments{get;set;}
    public string wcaption{get;set;}
    
    
    public List<ProcessInstanceHistory> wConList{get;set;} 
    public MyWrapper()
    {
        this.wConList = new List<ProcessInstanceHistory>();
     }
   }
     
      
 }