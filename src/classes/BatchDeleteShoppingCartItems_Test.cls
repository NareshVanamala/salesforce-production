@isTest (seeAllData = true)
   private class BatchDeleteShoppingCartItems_Test{
   static testmethod void testBatchDeleteShoppingCartItems() {
   list<contact> conList = new list<contact> ();
     conList = [select id,Accountid,mailingstate,RecordType.Name,Name from contact limit 1];
     String contactId = conList[0].id;     
     Forms_Literature_Shopping_Cart__c  cartObj = new Forms_Literature_Shopping_Cart__c();
     cartObj.Title__c = 'title';                   
     cartObj.Description__c = 'Description';
     cartObj.Contact__c = contactId;
     cartObj.Download_File_URL__c = 'Download File';                   
     cartObj.Max_Order__c = 20;
     cartObj.Published_Date__c = '06/01/2016';                     
     cartObj.Expiration_Date__c = '06/01/2016';
     cartObj.FINRA_PDF__c = 'Finra PDF';
     //cartObj.Forms_Literature_List__c = literatureList.id;
     cartObj.External_Id__c = contactId;   
     insert cartObj;
     System.assertEquals(cartObj.Description__c,'Description');
   test.starttest();
   BatchDeleteShoppingCartItems  cls = new BatchDeleteShoppingCartItems ();
   database.executebatch(cls);
    test.stopTest();
   }
   
}