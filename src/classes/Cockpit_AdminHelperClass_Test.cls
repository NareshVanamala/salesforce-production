@isTest
public class Cockpit_AdminHelperClass_Test{
    
   static testmethod void myTestMethod(){

     //       
    // System.assert(cl != null);
    
     Contact c = new Contact();
     c.Firstname='Test';
     c.Lastname='record';
     insert c;
     System.assert(c != null);
     
     Contact c1 = new Contact();
     c1.Firstname='Test';
     c1.Lastname='record';
     insert c1;
     System.assert(c1 != null);
     
         
     List<Contact> scope1 = new List<Contact>();
     scope1.add(c); 
     
     Campaign cn = new Campaign();
     cn.name='Blast Email';
     insert cn;
     System.assert(cn != null);
 
     CampaignMember cr = new CampaignMember();
     cr.campaignid = cn.id;
     cr.contactid = c.id;
     insert cr;
     System.assert(cr != null);
  
     
     Automated_Call_List__c ACL = new Automated_Call_List__c();
     ACL.name ='TestingCallList'+c.id;
     ACL.Campaign__c =cn.id;
     ACL.Dynamic_Query_Filters__c='Select Campaign Status&&&&Alternative_Investments_Firm_Provided__c,equals,null-->X1031_Relationship__c,equals,null-->Account.HNW_Accounts_2M__c,equals,null-->Account.Analyst_and_Research_Role__c,equals,False-->Account.Name,equals,null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c,starts with,null-->Account.Active__c,starts with,null-->Account.of_Book_in_Retirement_Plans_Non_IRA__c,contains,null-->Account.BusinessProfileTrackPercent__c,equals,null-->Account.CapleaseNumberofLocations__c,equals,null';
     //ACL.Last_Activity_Date__c=Date.today()-30;
     ACL.Archived__c=false;
     insert ACL;
     System.assert(ACL != null);
     
     ACL.TempLock__c = 'General Lock';
     update ACL;
    
     ACL.TempLock__c = '';
     ACL.Internal_Flag__c  =true;
     update ACL;
   
     contact_call_list__c clist = new contact_call_list__c();
     clist.Call_Complete__c = false;
     clist.Automated_Call_List__c = ACL.id;
     clist.CallList_Name__c = ACL.Name;
     clist.Description__c = 'Testing';
     clist.ContactCalllist_UniqueConbination__c=ACL.Name+c.Id;
     clist.Contact__c = c.id;
    // clist.Archived__c=false;
     //clist.campaign__c = cn.id;
     insert clist;
     System.assert(clist != null);
     

     list<string> fieldsList = new list<string>();
     Map<String, String> filterMap = new Map<String, String>();
     filterMap.put('1','First_Name__c&&!!equals&&!!James');
     filterMap.put('2','First_Name__c&&!!equals&&!!Naresh');
     filterMap.put('3','First_Name__c&&!!equals&&!!Dave');
     fieldsList.add('First_Name__c&&!!equals&&!!James');
     fieldsList.add('First_Name__c&&!!equals&&!!John');
     
     Test.StartTest();
     Cockpit_AdminHelperClass adminHelperClass = new Cockpit_AdminHelperClass();
     adminHelperClass.getmapfieldnames();
     adminHelperClass.generateAdvanceFormulaFilterCondition('Remove Contacts','My Contacts',filterMap,'(1 AND @) OR 3');
     adminHelperClass.generateDynamicQueryFilters('Remove Contacts',fieldsList,'AND','My Contacts');
     adminHelperClass.generateDynamicQueryFilters('Remove Contacts',fieldsList,'OR','All Contacts');
     adminHelperClass.updateVisibility(ACL.Name,'Visible only to me');
     adminHelperClass.updateVisibility(ACL.Name,'Visible to all users');
     adminHelperClass.updateVisibility(ACL.Name,'Visible to certain groups of users');
     adminHelperClass.getFields();
     adminHelperClass.createOperatorList('Name');
     adminHelperClass.createOperatorList('Investor_Number__c');
     adminHelperClass.createOperatorList('Alt_Investments_Percentage__c');
     adminHelperClass.createOperatorList('X1031_National_Meetings__c');
     adminHelperClass.createOperatorList('Alternative_Investments_Firm_Provided__c');
     adminHelperClass.createOperatorList('of_Clients_with_RE_in_Portfoliocheck__c');
     adminHelperClass.getVisbilityValues();
     adminHelperClass.cancel();
     adminHelperClass.getOwner();
     
     adminHelperClass.conditionQuery('Name','not equal to','Testing','Edit functionality');
     adminHelperClass.conditionQuery('Name','not equal to','','Edit functionality');
     adminHelperClass.conditionQuery('Name','not equal to',null,'Edit functionality');
     adminHelperClass.conditionQuery('Name','not equal to','Testing,Testing123','Edit functionality');
     adminHelperClass.conditionQuery('Name','equals','Testing','Edit functionality');
     adminHelperClass.conditionQuery('Name','equals','Testing,Testing123','Edit functionality');
     adminHelperClass.conditionQuery('Name','equals','','Edit functionality');
     adminHelperClass.conditionQuery('Name','equals',null,'Edit functionality');
     adminHelperClass.conditionQuery('Name','contains','Testing,testing123','Edit functionality');
     adminHelperClass.conditionQuery('Name','contains','','Edit functionality');
     adminHelperClass.conditionQuery('Name','contains',null,'Edit functionality');
     adminHelperClass.conditionQuery('Name','does not Contain','Testing','Edit functionality');
     adminHelperClass.conditionQuery('Name','starts with','Testing','Edit functionality');
     
     adminHelperClass.conditionQuery('Territory__c','not equal to','Atlantic','Edit functionality');
     adminHelperClass.conditionQuery('Territory__c','equals','Atlantic','Edit functionality');
     adminHelperClass.conditionQuery('Territory__c','contains','Atlantic,Cenral CA','Edit functionality');
     adminHelperClass.conditionQuery('Territory__c','does not Contain','Atlantic,Cenral CA','Edit functionality');
     adminHelperClass.conditionQuery('Territory__c','starts with','Atlantic,','Edit functionality');
     
     
     adminHelperClass.conditionQuery('Webcast_Attended__c','includes','Testing','Edit functionality');
     adminHelperClass.conditionQuery('Webcast_Attended__c','includes','Testing;Testing123','Edit functionality');
     adminHelperClass.conditionQuery('Webcast_Attended__c','includes','Testing,Testing123','Edit functionality');
     adminHelperClass.conditionQuery('Webcast_Attended__c','excludes','Testing','Edit functionality');
     adminHelperClass.conditionQuery('Webcast_Attended__c','excludes','Testing;Testing123','Edit functionality');
     adminHelperClass.conditionQuery('Webcast_Attended__c','excludes','Testing,Testing123','Edit functionality');
     adminHelperClass.conditionQuery('Webcast_Attended__c','equals','Testing','Edit functionality');
     adminHelperClass.conditionQuery('Webcast_Attended__c','not equal to','Testing','Edit functionality');
     
     adminHelperClass.conditionQuery('Alternative_Investments_Check__c','equals','true','Edit functionality');
     
     adminHelperClass.conditionQuery('REIT_Investments_Count__c','less or equal','0','Edit functionality');
     adminHelperClass.conditionQuery('REIT_Investments_Count__c','greater or equal','0','Edit functionality');
     adminHelperClass.conditionQuery('REIT_Investments_Count__c','equals','0','Edit functionality');
     adminHelperClass.conditionQuery('REIT_Investments_Count__c','not equal to','0','Edit functionality');
     adminHelperClass.conditionQuery('REIT_Investments_Count__c','less than','0','Edit functionality');
     adminHelperClass.conditionQuery('REIT_Investments_Count__c','greater than','0','Edit functionality');
     
     adminHelperClass.conditionQuery('Allocated_to_RE_Expired_On_Contact__c','not equal to','12/02/2016','Edit functionality');
     adminHelperClass.conditionQuery('Allocated_to_RE_Expired_On_Contact__c','equals','12/02/2016','Edit functionality');
     adminHelperClass.conditionQuery('Allocated_to_RE_Expired_On_Contact__c','greater than','12/02/2016','Edit functionality');
     adminHelperClass.conditionQuery('Allocated_to_RE_Expired_On_Contact__c','less than','12/02/2016','Edit functionality');
     adminHelperClass.conditionQuery('Allocated_to_RE_Expired_On_Contact__c','less or equal','12/02/2016','Edit functionality');
     adminHelperClass.conditionQuery('Allocated_to_RE_Expired_On_Contact__c','greater or equal','12/02/2016','Edit functionality');
     
     
     adminHelperClass.conditionQuery('Contact_Status_Changed_on__c','not equal to','12/02/2016 04:15 AM','Edit functionality');
     adminHelperClass.conditionQuery('Contact_Status_Changed_on__c','equals','12/02/2016 04:15 AM','Edit functionality');
     adminHelperClass.conditionQuery('Contact_Status_Changed_on__c','less than','12/02/2016 04:15 AM','Edit functionality');
     adminHelperClass.conditionQuery('Contact_Status_Changed_on__c','greater than','12/02/2016 04:15 AM','Edit functionality');
     adminHelperClass.conditionQuery('Contact_Status_Changed_on__c','less or equal','12/02/2016 04:15 AM','Edit functionality');
     adminHelperClass.conditionQuery('Contact_Status_Changed_on__c','greater or equal','12/02/2016 04:15 AM','Edit functionality');
    
     
     adminHelperClass.validate('Create Call List','Testing','','FieldName1','FieldName2','FieldName3','FieldName4','FieldName5','FieldName6','FieldName7','FieldName8','FieldName9','FieldName10','Operator1','Operator2','Operator3','Operator4','Operator5','Operator6','Operator7','Operator8','Operator9','Operator10');
     adminHelperClass.validate('Create Call List','','','--None--','--None--','--None--','--None--','--None--','--None--','--None--','--None--','--None--','--None--','--None--','--None--','--None--','--None--','--None--','--None--','--None--','--None--','--None--','--None--');
    
     Test.StopTest();      
     }
 }