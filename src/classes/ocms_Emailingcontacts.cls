public without sharing class ocms_Emailingcontacts {
public contact c;
public contact c1;
public string aname;
public string template;
public string comments;
public boolean send{get;set;}
public boolean sent{get;set;}

public ocms_Emailingcontacts()
{
aname=System.currentPageReference().getParameters().get('IN'); 

if(aname!=null && aname!='Not Assigned')
{   
c=[select id,firstname,lastname,phone,email,mailingstate,mailingcity from contact where name=:aname and email!=null limit 1];
}
string id1=System.currentPageReference().getParameters().get('contactid');
if(id1!=null)
{   
c1=[select id,email,firstname,lastname,phone,Mailingcity,Mailingstate from contact where id=:id1];
}
template=System.currentPageReference().getParameters().get('Template');
comments=System.currentPageReference().getParameters().get('com');
}

public pagereference sendemail()
{
if(System.currentPageReference().getParameters().get('IN')!=null && System.currentPageReference().getParameters().get('IN')!='Not Assigned')
{
if(c1!=null)
{    
string cname=c1.firstname+' '+c1.lastname+' '+' Request';
list<Contact_Us_Response__c> ct1=[select id,name from Contact_Us_Response__c where Last_Mailed_Date__c=:system.today() and name=:cname and category__c=:template limit 1];

if(ct1.size()>0)
{
sent=true;
send=false;
}
else
{
sent=false;
send=true;

Contact_Us_Response__c ct=new Contact_Us_Response__c();
ct.name=c1.firstname+' '+c1.lastname+' '+' Request';
ct.First_Name__c=c1.firstname;
ct.last_name__c=c1.lastname;
ct.email__c=c1.email;
ct.phone_number__c=c1.phone;
ct.city__c=c1.Mailingcity;
ct.state__c=c1.Mailingstate;
ct.category__c=template;
ct.Internal_wholesaler_email__c=c.email;
ct.Internal_wholesaler__c=c.firstname+' '+c.lastname;
ct.Last_Mailed_Date__c=system.today();
if(comments!=null)
{
ct.comments__c=comments;}
else{
ct.comments__c='';
}
insert ct;
}
}
}

else
{

string cname=c1.firstname+' '+c1.lastname+' '+' Request';
list<Contact_Us_Response__c> ct1=[select id,name from Contact_Us_Response__c where Last_Mailed_Date__c=:system.today() and name=:cname and category__c=:template limit 1];

if(ct1.size()>0)
{
sent=true;
send=false;
}
else
{
if(c1!=null)
{ 
sent=false;
send=true;

Map<String,Alternate_Email_in_absenece_of_IW__c> allCodes = Alternate_Email_in_absenece_of_IW__c.getAll();
Alternate_Email_in_absenece_of_IW__c code = allCodes.values();

Contact_Us_Response__c ct=new Contact_Us_Response__c();
ct.name=c1.firstname+' '+c1.lastname+' '+' Request';
ct.First_Name__c=c1.firstname;
ct.last_name__c=c1.lastname;
ct.email__c=c1.email;
ct.phone_number__c=c1.phone;
ct.city__c=c1.Mailingcity;
ct.state__c=c1.Mailingstate;
ct.category__c=template;
ct.Internal_wholesaler_email__c=code.email__c;
ct.Internal_wholesaler__c=c1.firstname+' '+c1.lastname;
ct.Last_Mailed_Date__c=system.today();
if(comments!=null)
{
ct.comments__c=comments;}
else{
ct.comments__c='';
}
insert ct;
}
}
}
return null;
}
}