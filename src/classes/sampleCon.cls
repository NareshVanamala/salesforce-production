public with sharing class sampleCon 
{

    /*public PageReference exportToExcel2() {
        return page.generatingexcel23;
    }*/


    /*public PageReference exportToExcel() {
        return page.GeneratingExcel;
    }*/
    
     /*public PageReference exportToExcel1() {
        return page.GeneratingExcel2;
    }*/

   /* String country = 'None';
    public String Calllistvlaues1{get;set;} 
    public List<SelectOption> CallListValues {set;get;}
    public String calllist {get;set;}     
    public integer totalcontactsCreated{set;get;}
    public integer totalcontactsAssigned{set;get;}
    public integer totalcontactsUnAssigned{set;get;}
    transient  list<Contact_Call_List__c> lsttask;
    private List<ContactCountWrappercls> ContactListCls ;
    //public List<ContactCountWrapperclsforCallandInternal> ContactListClsforCallandInternal {set;get;}
    public List<ContactCountWrapperclsforCallandInternal> ContactListClsforCallandInternal;
    
   public List<SelectOption> UserListValues { get; set; }
   public String Ulist { get; set; }
    public Integer totalcontactsAssigned1{ get; set; }
   public Integer totalcontactsCompleted1{ get; set; }
   public Integer totalcontactsNotCompleted1{ get; set; }
   public Integer totalSkippedContacts{ get; set; }
   private List<ContactCountWrappercls120> ContactListCls1 ;
    
    public List<ContactCountWrappercls> getContactListCls(){
      return ContactListCls;
    }
     public List<ContactCountWrappercls120> getContactListCls1()
   {
      return ContactListCls1;
   }
   public List<ContactCountWrapperclsforCallandInternal> getContactListClsforCallandInternal(){
      return ContactListClsforCallandInternal;
    }
    public PageReference test()
    {
     return null;
    }
    
    public sampleCon()
    {
      set<String> call = new Set<String>();
      List<String> lscall = new List<String>();
      try{
            List<Automated_Call_List__c> tcall = [select ID,Name,Automated_Call_list_Name__c,IsDeleted__c,Archived__c from Automated_Call_List__c where IsDeleted__c=false and Archived__c=false];
            for(Automated_Call_List__c t : tcall)
            call.add(t.Name); 
            call.add('--None--');
            CallListValues = new List<SelectOption>();
            lscall.AddAll(call);
            lscall.sort();
            for(String s : lscall)
            CallListValues.add(new SelectOption(s,s));
            if(CallListValues.size()==0)
            CallListValues.add(new SelectOption('None','None'));
        }catch(Exception e){}
        
        set<String> call1 = new Set<String>();
        List<String> lscall1 = new List<String>();
        List<Profile> plist = new List<Profile>();
        List<Id> pid= new List<Id>();
        plist = [select id,name from Profile where name='Internal Sales' or name ='External Sales'];
         For(integer i=0;i<plist.size();i++)
             pid.add(plist[i].id);
        try{
            List<User>userlist1 = [select ID,Name from User where  ProfileId in : pid and IsActive = true ];
            for(User  t : userlist1 )
            call1.add(t.Name); 
            call1.add('--None--');
            UserListValues = new List<SelectOption>();
            lscall1.AddAll(call1);
            lscall1.sort();
            for(String s : lscall1)
            UserListValues.add(new SelectOption(s,s));
            if(UserListValues.size()==0)
            UserListValues.add(new SelectOption('None','None'));
        }catch(Exception e){}        
      }
      
     public List<SelectOption> getItems() {
     List<SelectOption> options = new List<SelectOption>();
     options.add(new SelectOption('None','None'));
     options.add(new SelectOption('Calllist','Calllist'));
     options.add(new SelectOption('WholeSaler','WholeSaler'));
     return options;
        }
            
        public String getCountry() {
            return country;
        }
            
        public void setCountry(String country) {
            this.country = country;
        }
         
     public void AdviserCallList()
      {
        ContactListCls = new List<ContactCountWrappercls>();
       List<AggregateResult> groupedResults1=[select CallList_Name__c,IsDeleted__c, COUNT(id) cnt from Contact_Call_List__c where CallList_Name__c=:calllist and IsDeleted__c=false  GROUP BY CallList_Name__c,IsDeleted__c];
       List<AggregateResult> groupedResults2=[select CallList_Name__c,IsDeleted__c, COUNT(id) cnt1 from Contact_Call_List__c where (CallList_Name__c=:calllist and Owner__c!=null and IsDeleted__c=false) GROUP BY CallList_Name__c,IsDeleted__c];
        
       
        System.debug('*****Size Of Aggregate Result******\n'+groupedResults1);
        If(groupedResults1.size()>0)
        {
        totalcontactsCreated =(integer)groupedResults1[0].get('cnt');
        }
        else {
         totalcontactsCreated = 0;
         }
        If(groupedResults2.size()>0)
        {
         totalcontactsAssigned=(integer)groupedResults2[0].get('cnt1');
         }
         else {
         totalcontactsAssigned = 0;
         }
         
   totalcontactsUnAssigned = totalcontactsCreated - totalcontactsAssigned ;
  System.debug('Heap Size--->0: ' + Limits.getHeapSize() + ' <---> ' + Limits.getLimitHeapSize());
  // lsttask=[select id,Call_Complete__c,CallList_Name__c,Owner__c from Contact_Call_List__c where CallList_Name__c=:calllist];
  System.debug('Heap Size--->1: ' + Limits.getHeapSize() + ' <---> ' + Limits.getLimitHeapSize());
  integer subtotal1;
  integer subtotal;
        
        Map<Id,User> usermap = new Map<Id,User>([select Id,Name,Territory__c from User]);
        System.debug('******STRAT*********\n'+System.now());
        List<AggregateResult> groupedResults=[select CallList_Name__c,Owner__c,Call_Complete__c, ReasonToSkip__c,Assigned_Date__c,IsDeleted__c, COUNT(id) cnt from Contact_Call_List__c where (CallList_Name__c=:calllist and Owner__c!=null and IsDeleted__c=false) GROUP BY Owner__c, Call_Complete__c, ReasonToSkip__c, CallList_Name__c,Assigned_Date__c,IsDeleted__c order by Owner__c  ASC , CallList_Name__c ASC, Assigned_Date__c ASC];
        System.debug('Heap Size--->2: ' + Limits.getHeapSize() + ' <---> ' + Limits.getLimitHeapSize());
        System.debug('******END********\n'+System.now());
        system.debug('Check the aggregate result....'+ groupedResults);
        String prevName;
        String prevCallList;
        integer prevRemaining =0;
        integer prevContacted =0;
        integer prevSkipped=0;
        Integer prevCount=0;
        integer contacted =0;
        integer remaining =0;
        integer skipped=0;
        integer firstEntry = 0;
        integer totalcreated=0;
        Date previousassigneddate=null;
         Date assignedToday;
        if(groupedResults.size()>0)
        {
            for(AggregateResult ar : groupedResults)
            {
                String asgned= (String)ar.get('Owner__c');
                //String call= (String)ar.get('CallList_Name__c');
                Integer cnt = (Integer)ar.get('cnt');
                Boolean Status=(Boolean)ar.get('Call_Complete__c');
                assignedToday=(Date)ar.get('Assigned_Date__c');
                String name=null;
                totalcreated=cnt;
                if(asgned!=null)
                    name = usermap.get(asgned).Name;
                if((Boolean.valueof(ar.get('Call_Complete__c')) == True)&&(String.valueof(ar.get('ReasonToSkip__c')) ==null ))
                {
                    contacted = integer.valueof(ar.get('cnt'));
                    remaining = 0;
                    skipped=0;
                }
                if(Boolean.valueof(ar.get('Call_Complete__c')) == False)
                {
                    remaining = integer.valueof(ar.get('cnt'));
                    contacted = 0;
                    skipped=0;
                }
                if((Boolean.valueof(ar.get('Call_Complete__c')) == True)&&(String.valueof(ar.get('ReasonToSkip__c'))!=null))
                {
                    skipped=integer.valueof(ar.get('cnt'));
                    contacted =0;
                    remaining = 0;
                }
                
                if(name!=null && prevName !=null)
                {
                  // Check to see if we are still dealing with same name and call list
                  if (prevName == name) 
                  {
                   // we are still same call list
                   // Check the status
                   // remaining = prevRemaining;
                   // contacted= prevcontacted;
                   prevCount= prevCount+ totalcreated;
                   totalcreated= prevCount; 
                //added by snehal to solve the issue if assigned date is dfferent for contacts  
                   prevContacted=prevContacted+contacted ;
                   if (prevContacted > 0)  
                    contacted = prevContacted;
                   prevRemaining=prevRemaining+remaining;
                    if (prevRemaining > 0)
                   remaining = prevRemaining; 
                    prevSkipped= prevSkipped+ skipped;
                    if(prevSkipped>0)
                     skipped=prevSkipped;
                        
                   }
                  else{                        
                        // dealing with new call list
                       // system.debug('We are inserting in 1st else  loop inside for.. '+ prevName+ prevCount + prevContacted+ prevRemaining);
                        ContactCountWrappercls  tskcls = new ContactCountWrappercls (prevName, prevCount, prevContacted ,prevRemaining,prevSkipped,previousassigneddate);
                        ContactListCls.add(tskcls);
                        //system.debug('Check the list priya'+ ContactListCls);
                    }                   
                }
                prevRemaining = remaining;
                prevContacted = contacted;
                prevSkipped = skipped;     
                //System.debug('name : ' + name +' call list ' + call + ' prevName ' + prevName + ' prev call list ' + prevCallList + ' contacted:' + contacted + 'remaining ' + remaining + ' prevcontacted ' + prevContacted + 'prevremaining ' + prevremaining);
                prevName = name;
                prevCount= totalcreated;
                prevSkipped= skipped;
                previousassigneddate=assignedToday;
                
               //prevCallList = call;
            }
            System.debug('******END********\n'+System.now());            
            // For the last entry, log the record   
            //system.debug('We are inserting outside for.loop. '+ prevName+ totalcreated+ prevContacted+ prevRemaining);
            ContactCountWrappercls tskcls = new ContactCountWrappercls (prevName, totalcreated, prevContacted ,prevRemaining,prevSkipped,previousassigneddate);
            ContactListCls.add(tskcls);
            // system.debug('Lets check the list.....ninad'+ ContactListCls);
        }
    }
    public void AdviserCallList1()
     {
       //ContactListCls = new List<ContactCountWrappercls>();
       ContactListCls1 = new List<ContactCountWrappercls120>();
        //totalcontactsAssignedlist= new list<Contact_Call_List__c>();
        List<AggregateResult> groupedResults10=[select  Owner__c ,IsDeleted__c, COUNT(id) cnt from Contact_Call_List__c where  Owner__r.Name =:Ulist and IsDeleted__c=false and Snoozed__c = False and CallListOrder__r.IsDeleted__c=false and CallListOrder__r.Archived__c=false group by  Owner__c,IsDeleted__c];
        List<AggregateResult> groupedResults20=[select  Owner__c ,IsDeleted__c, COUNT(id) cnt1 from Contact_Call_List__c where(Owner__r.Name =:Ulist and Call_Complete__c=true and ReasonToSkip__c=null and IsDeleted__c=false and Snoozed__c = False and CallListOrder__r.IsDeleted__c=false and CallListOrder__r.Archived__c=false)group by  Owner__c,IsDeleted__c ];
        List<AggregateResult> groupedResults30=[select  Owner__c ,IsDeleted__c, COUNT(id) cnt2 from Contact_Call_List__c where(Owner__r.Name =:Ulist and Call_Complete__c=true and ReasonToSkip__c!=null and IsDeleted__c=false and Snoozed__c = False and CallListOrder__r.IsDeleted__c=false and CallListOrder__r.Archived__c=false)group by  Owner__c,IsDeleted__c ];

        If(groupedResults10.size()>0)
        {
        totalcontactsAssigned1  =(integer)groupedResults10[0].get('cnt');
        }
        else {
         totalcontactsAssigned1 = 0;
         }
        If(groupedResults20.size()>0)
        {
         totalcontactsCompleted1=(integer)groupedResults20[0].get('cnt1');
         }
         else {
         totalcontactsCompleted1= 0;
         }
         If(groupedResults30.size()>0)
        {
       totalSkippedContacts=(integer)groupedResults30[0].get('cnt2');
        }
        else {
         totalSkippedContacts= 0;
         }
         totalcontactsNotCompleted1= totalcontactsAssigned1-(totalcontactsCompleted1+totalSkippedContacts);
         system.debug('<<<<<<<<<EK WAR PANKHAVARUNI PHIRO TUJHA HAT>>>>>>>>>'+totalcontactsNotCompleted1);
         system.debug('<<<<<<<<<EK WAR PANKHAVARUNI PHIRO TUJHA HAT>>>>>>>>>'+totalSkippedContacts);
         
     Map<Id,User> usermap1 = new Map<Id,User>([select Id,Name,Territory__c from User]);
        System.debug('******STRAT*********\n'+System.now());
        List<AggregateResult> groupedResults11=[select CallList_Name__c,Owner__c,Call_Complete__c, ReasonToSkip__c,Assigned_Date__c,IsDeleted__c, COUNT(id) cnt from Contact_Call_List__c where (CallList_Name__c!=null and Owner__r.Name =:Ulist and IsDeleted__c=false and Snoozed__c = False and CallListOrder__r.IsDeleted__c=false and CallListOrder__r.Archived__c=false ) GROUP BY CallList_Name__c ,Assigned_Date__c, Call_Complete__c, ReasonToSkip__c ,Owner__c,IsDeleted__c order by  CallList_Name__c,Owner__c  ASC];
        System.debug('Heap Size--->2: ' + Limits.getHeapSize() + ' <---> ' + Limits.getLimitHeapSize());
        System.debug('******END********\n'+System.now());
        system.debug('Check the aggregate result....'+ groupedResults11);
        String prevName1;
        String prevCallList1;
        integer prevRemaining1 =0;
        integer prevContacted1 =0;
        integer prevSkipped1=0;
        Integer prevCount1=0;
        integer contacted1 =0;
        integer remaining1 =0;
        integer skipped1=0;
        integer firstEntry1 = 0;
        integer totalcreated1=0;
        Date previousassigneddate=null;
        Date assignedDate1;
        if(groupedResults11.size()>0)
        {
            for(AggregateResult ar : groupedResults11)
            {
               String asgned1= (String)ar.get('Owner__c');
                String call1= (String)ar.get('CallList_Name__c');
                Integer cnt1 = (Integer)ar.get('cnt');
                Boolean Status1=(Boolean)ar.get('Call_Complete__c');
                assignedDate1=(date)ar.get('Assigned_Date__c');
                String name1=null;
                totalcreated1=cnt1;
                if(asgned1!=null)
                    name1 = usermap1.get(asgned1).Name;
                if((Boolean.valueof(ar.get('Call_Complete__c')) == True)&&(String.valueof(ar.get('ReasonToSkip__c')) ==null ))
                {
                    contacted1 = integer.valueof(ar.get('cnt'));
                    remaining1 = 0;
                    skipped1=0;
                }
                if(Boolean.valueof(ar.get('Call_Complete__c')) == False)
                {
                    remaining1 = integer.valueof(ar.get('cnt'));
                    contacted1 = 0;
                    skipped1=0;
                }
                if((Boolean.valueof(ar.get('Call_Complete__c')) == True)&&(String.valueof(ar.get('ReasonToSkip__c'))!=null))
                {
                    skipped1=integer.valueof(ar.get('cnt'));
                    contacted1 =0;
                    remaining1 = 0;
                }
                
                if(call1!=null && prevCallList1!=null)
                {
                  // Check to see if we are still dealing with same name and call list
                  if (prevCallList1 == call1) 
                  {
                   // we are still same call list
                   // Check the status
                   // remaining = prevRemaining;
                   // contacted= prevcontacted;
                   prevCount1= prevCount1+ totalcreated1;
                   //previousassigneddate=assignedDate1;
                   totalcreated1= prevCount1;
                   prevContacted1 =prevContacted1 +contacted1 ;
                   if (prevContacted1 > 0)  
                    contacted1 = prevContacted1;
                   prevRemaining1 =prevRemaining1 +remaining1 ;
                    if (prevRemaining1 > 0)
                    remaining1 = prevRemaining1; 
                    prevSkipped1= prevSkipped1+ skipped1;
                    if(prevSkipped1>0)
                     skipped1=prevSkipped1;
                        
                   }
                  else{                        
                        // dealing with new call list
                        system.debug('We are inserting in 1st else  loop inside for.. '+ prevCallList1 + prevCount1 + prevContacted1+ prevRemaining1);
                        ContactCountWrappercls120  tskcls11 = new ContactCountWrappercls120 (prevCallList1 , prevCount1, prevContacted1 ,prevRemaining1,prevSkipped1,previousassigneddate);
                        ContactListCls1.add(tskcls11);
                        //system.debug('Check the list priya'+ ContactListCls1);
                    }                   
                }
                prevRemaining1 = remaining1;
                prevContacted1 = contacted1;
                prevSkipped1 = skipped1;     
                System.debug('name : ' + name1 +' call list ' + call1 + ' prevName ' + prevName1 + ' prev call list ' + prevCallList1 + ' contacted:' + contacted1 + 'remaining ' + remaining1 + ' prevcontacted ' + prevContacted1 + 'prevremaining ' + prevremaining1);
                prevName1 = name1;
                prevCount1= totalcreated1;
                prevSkipped1= skipped1;
                prevCallList1 = call1;
                previousassigneddate=assignedDate1;
            }
            System.debug('******END********\n'+System.now());            
            // For the last entry, log the record   
            system.debug('We are inserting outside for.loop. '+ prevName1+ totalcreated1+ prevContacted1+ prevRemaining1);
            ContactCountWrappercls120 tskcls11 = new ContactCountWrappercls120 (prevCallList1 , totalcreated1, prevContacted1 ,prevRemaining1,prevSkipped1,previousassigneddate);
            ContactListCls1.add(tskcls11);
            // system.debug('Lets check the list.....ninad'+ ContactListCls1);
        }
    
    
    }
           
    public class ContactCountWrappercls 
    {  
        public String AssignedTo {set;get;}
        public Integer completed1{set;get;}
        public Integer remaining {set;get;}
        public Integer count {set;get;} 
        public Integer skipped1 {set;get;}
        public Date assinedToday {set;get;}
        //public String status1{set;get;}
        public ContactCountWrappercls (String assign,Integer CreatedContacts ,integer contacted1 ,Integer remaining1,Integer skipped , Date assigneddate){
            AssignedTo  = assign;
            count= CreatedContacts ;
            //count = cnt;
            completed1=contacted1;
            remaining=remaining1;
            skipped1 =skipped ;
            assinedToday =assigneddate;
        }
    }
    
    public class ContactCountWrappercls120 
    {  
        public String AssignedTo120 {set;get;}
        public Integer completed120{set;get;}
        public Integer remaining120 {set;get;}
        public Integer count120 {set;get;} 
        public Integer skipped120 {set;get;}
        public date assignedDate{set;get;}
        //public String status1{set;get;}
        public ContactCountWrappercls120 (String assign,Integer CreatedContacts ,integer contacted1 ,Integer remaining1,Integer skipped,date assignedDate1){
            AssignedTo120  = assign;
            count120= CreatedContacts ;
            //count = cnt;
            completed120=contacted1;
            remaining120=remaining1;
            skipped120 =skipped ;
            assignedDate=assignedDate1;
            
        }
      
    
    } 
     
   public void Results1forCallandInternal()
    {
      ContactListClsforCallandInternal = new List<ContactCountWrapperclsforCallandInternal>();
      Map<Id,User> usermap = new Map<Id,User>([select Id,Name,Territory__c from User]);
      List<AggregateResult> groupedResultsforCallandInternal=[select CallList_Name__c,Owner__c,Call_Complete__c,ReasonToSkip__c,Assigned_Date__c,IsDeleted__c, COUNT(id) cnt from Contact_Call_List__c where (CallList_Name__c!=null and Owner__c!=null and IsDeleted__c=false ) GROUP BY CallList_Name__c, Call_Complete__c,ReasonToSkip__c, Owner__c,Assigned_Date__c,IsDeleted__c order by Owner__c  ASC , CallList_Name__c ASC,Assigned_Date__c ASC];
        //List<AggregateResult> groupedResultsforCallandInternal =[select Call_list__c,OwnerId,status,COUNT(id) cnt from task where (Call_list__c!=null and OwnerId!=null and (status='Not Started' or status='Open')) GROUP BY ROLLUP(Call_list__c ,OwnerId,status)];
       
       system.debug('groupedResultsgroupedResultsgroupedResultsgroupedResultsgroupedResults'+groupedResultsforCallandInternal.size());
        system.debug('groupedResultsgroupedResultsgroupedResultsgroupedResultsgroupedResults'+groupedResultsforCallandInternal);
        String prevNameforCallandInternal;
        String prevCallListforCallandInternal;
        integer prevRemainingforCallandInternal =0;
        integer prevContactedforCallandInternal =0;
        integer prevSkipped1forCallandInternal=0;
        integer contactedforCallandInternal =0;
        integer remainingforCallandInternal =0;
        integer firstEntryforCallandInternal = 0;
        integer skippedforCallandInternal=0;
        integer totalcreatedforCallandInternal=0;
        Integer prevCount1forCallandInternal=0;
        date prevassigneddateforCallandInternal=null;
        date assigneddateforCallandInternal;
       
        for (AggregateResult ar : groupedResultsforCallandInternal)
        {
          String asgnedforCallandInternal= (String)ar.get('Owner__c');
          String callforCallandInternal= (String)ar.get('CallList_Name__c');
          Integer cntforCallandInternal = (Integer)ar.get('cnt');  
          Boolean StatusforCallandInternal=(Boolean)ar.get('Call_Complete__c');
          assigneddateforCallandInternal=(date)ar.get('Assigned_Date__c');
          totalcreatedforCallandInternal=cntforCallandInternal;
         
          String nameforCallandInternal=null;
          if(asgnedforCallandInternal!=null)
          nameforCallandInternal = usermap.get(asgnedforCallandInternal).Name;
          if(Boolean.valueof(ar.get('Call_Complete__c')) == True)
           {
            contactedforCallandInternal = integer.valueof(ar.get('cnt'));
            remainingforCallandInternal = 0;
            skippedforCallandInternal=0;
            }
            if(Boolean.valueof(ar.get('Call_Complete__c')) == False)
            {
                remainingforCallandInternal = integer.valueof(ar.get('cnt'));
                contactedforCallandInternal = 0;
                skippedforCallandInternal=0;
            }
            if((Boolean.valueof(ar.get('Call_Complete__c')) == True)&&(String.valueof(ar.get('ReasonToSkip__c'))!=null))
                {
                    skippedforCallandInternal=integer.valueof(ar.get('cnt'));
                    contactedforCallandInternal =0;
                    remainingforCallandInternal = 0;
                }
            
            if(nameforCallandInternal!=null && prevNameforCallandInternal !=null)
            {
              // Check to see if we are still dealing with same name and call list
              if (prevNameforCallandInternal == nameforCallandInternal && prevCallListforCallandInternal == callforCallandInternal) {
               // we are still same call list
               // Check the status
               // remaining = prevRemaining;
               // contacted= prevcontacted;
               prevCount1forCallandInternal= prevCount1forCallandInternal+ totalcreatedforCallandInternal;
                   totalcreatedforCallandInternal= prevCount1forCallandInternal;
               if (prevContactedforCallandInternal > 0)  
                contactedforCallandInternal = prevContactedforCallandInternal;
                if (prevRemainingforCallandInternal > 0)
                remainingforCallandInternal = prevRemainingforCallandInternal;
                prevSkipped1forCallandInternal=prevSkipped1forCallandInternal+skippedforCallandInternal;
                if(prevSkipped1forCallandInternal>0)
                 skippedforCallandInternal=prevSkipped1forCallandInternal;    
               }
              else
                 {
                    // dealing with new call list
                    ContactCountWrapperclsforCallandInternal  tskclsforCallandInternal = new ContactCountWrapperclsforCallandInternal (prevNameforCallandInternal, prevCallListforCallandInternal,prevCount1forCallandInternal, prevContactedforCallandInternal ,prevRemainingforCallandInternal,prevSkipped1forCallandInternal,prevassigneddateforCallandInternal);
                    ContactListClsforCallandInternal.add(tskclsforCallandInternal);
                }
               
            }
            prevRemainingforCallandInternal = remainingforCallandInternal;
            prevContactedforCallandInternal = contactedforCallandInternal;  
            prevSkipped1forCallandInternal= skippedforCallandInternal; 
            prevCount1forCallandInternal=totalcreatedforCallandInternal;
            prevassigneddateforCallandInternal=assigneddateforCallandInternal;
              
            //System.debug('name : ' + name +' call list ' + call + ' prevName ' + prevName + ' prev call list ' + prevCallList + ' contacted:' + contacted + 'remaining ' + remaining + ' prevcontacted ' + prevContacted + 'prevremaining ' + prevremaining);
            prevNameforCallandInternal = nameforCallandInternal;
            prevCallListforCallandInternal = callforCallandInternal;
        }
        
        // For the last entry, log the record        
       ContactCountWrapperclsforCallandInternal tskclsforCallandInternal = new ContactCountWrapperclsforCallandInternal (prevNameforCallandInternal, prevCallListforCallandInternal,prevCount1forCallandInternal, prevContactedforCallandInternal ,prevRemainingforCallandInternal,prevSkipped1forCallandInternal,assigneddateforCallandInternal );
        ContactListClsforCallandInternal.add(tskclsforCallandInternal);
        
 
        System.debug('NINAD NINAD NINAD NINAD Test'+ContactListClsforCallandInternal);
        // List<AggregateResult> groupedResults=[select Call_list__c,OwnerId,status, COUNT(id) cnt from task where (Call_list__c!=null and OwnerId!=null ) GROUP BY Call_list__c, status, ownerid  order by name, Call_list__c];
        
        //return groupedResults;
    }
     
    public class ContactCountWrapperclsforCallandInternal 
    {
    
        public String AssignedToforCallandInternal {set;get;}
        public String Call1forCallandInternal {set;get;}
        public Integer completed1forCallandInternal{set;get;}
        public Integer remainingforCallandInternal {set;get;}
        public Integer SkippedforCallandInternal {set;get;}
        public Integer countforCallandInternal {set;get;} 
        public String status1forCallandInternal{set;get;}
        public date assignedDate2forcallandInternal{set;get;}
        
      public ContactCountWrapperclsforCallandInternal (String assignforCallandInternal,String callforCallandInternal, integer TotalContacts,integer contacted1forCallandInternal ,Integer remaining1forCallandInternal,Integer SkippedContacts,date assigneddateforcall )
      {        
      AssignedToforCallandInternal  = assignforCallandInternal;
      Call1forCallandInternal =  callforCallandInternal;
      countforCallandInternal =TotalContacts;
     // Status1=status;
      //count = cnt;
      completed1forCallandInternal=contacted1forCallandInternal;
      remainingforCallandInternal=remaining1forCallandInternal;
      SkippedforCallandInternal =SkippedContacts;
      assignedDate2forcallandInternal=assigneddateforcall; 
     }
       
        
        
        }*/
        
        }