@isTest
private class TestforEventsConferenceClass 
 {
      public static testMethod void TestforEventsConferenceClass () 
      {
          Account a= new Account();
          a.name='Testaccount1';
          //RecordType rtypes = [Select Name, Id From RecordType where isActive=true and sobjecttype='Account'and name='National_Accounts'];
          //a.recordtypeid=rtypes.id;
          insert a;  
          
          Conference_Events__c ct= new Conference_Events__c();
          ct.Account__c=a.id;
          ct.Cole_Participation__c='12';
          ct.Date__c=system.today();
          ct.Location__c='Hyderabad';
          ct.name='Testingthe Program';
          insert ct;
          
          Conference_Events__c ct1= new Conference_Events__c();
          ct1.Account__c=a.id;
          ct1.Cole_Participation__c='12';
          ct1.Date__c=system.today();
          ct1.Location__c='Hyderabad';
          ct1.name='Testingtestthe Program';
          insert ct1;
          
          
         System.assertEquals(ct.Account__c,a.id);

              
        list<Conference_Events__c> updatedlist1  =new list<Conference_Events__c>();  
          
         list<Conference_Events__c> memberList=[select id,Cole_Participation__c, name from Conference_Events__c where name='Testingthe Program'];
         if(memberList.size()>0)
        {
             for(Integer i=0;i<memberList.size();i++)
            {
                if(memberList[i].id==ct.id)
                {
                  memberList[i].Cole_Participation__c='12'; 
                  updatedlist1.add(memberList[i]); 
                                    
                }
            }
                 update updatedlist1;          
        }
         ApexPages.currentPage().getParameters().put('id',a.id);
         ApexPages.StandardController controller = new ApexPages.StandardController(a); 
         eventsConferenceClass x = new eventsConferenceClass(controller);  
         x.newProgram();
         x.savenewProg();
         x.editrecord();
         //x.viewRecord();
         x.SaveRecord();
         x.removecon();
         x.cancelnewProg();          

       }
         
          
           
 }