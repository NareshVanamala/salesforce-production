@isTest(seealldata=true)
public with sharing class ocms_Vkit_ColeCapitalContact_test{
 
static testMethod void testocmsColeCapitalContact() {

 profile pf=[select id,name from profile where name='Cole Capital Community'];
 profile pf1=[select id,name from profile where name='Internal Sales'];
       // user u=[select id,username,contactid,firstname,lastname from user where profileid=:pf.id limit 1];
               
        account a=new account();
        a.name='test';
        insert a;
        
     
    
    
    User Internal = new User();
    Internal.FirstName = 'Test2221';
    Internal.LastName = 'Guy2221';
    Internal.Phone = '01234567891';
    Internal.Email = 'testguy21@testyourcode.com';
    Internal.Alias = 'HMan';
    Internal.ProfileId =pf1.id;
    Internal.UserName = 'testguy222@testyourcode.com';
        Internal.TimeZoneSidKey = 'GMT';
    Internal.LocaleSidKey = 'en_US';
    Internal.EmailEncodingKey = 'ISO-8859-1';
    Internal.LanguageLocaleKey = 'en_US';
    insert Internal ;
    
    
    User External= new User();
    External.FirstName = 'Test2122';
    External.LastName = 'Guy2212';
    External.Phone = '01234567189';
    External.Email = 'testgu1y11@testyourcode.com';
    External.Alias = 'HtMan';
     External.ProfileId =pf1.id;
    External.UserName = 'testingguy223@testyourcode.com';
     External.TimeZoneSidKey = 'GMT';
    External.LocaleSidKey = 'en_US';
    External.EmailEncodingKey = 'ISO-8859-1';
    External.LanguageLocaleKey = 'en_US';
    insert External;
    
   System.assertEquals(External.ProfileId,pf1.id);

       list<contact> clist=new list<contact>();
      
        Contact c= new Contact();
        c.firstName='sandeep';
        c.lastName='m';
        c.HomePhone='9999999999';
        c.title='Director';
        c.phone='9999999999';
        c.accountid=a.id;
        c.email='test@testunique.com';
        c.Image_URL__c='/01p500000001k7V';
        c.Internal_Wholesaler__c='sandeep m';
        c.Wholesaler__c='sandeep m';
        c.Regional_Territory_Manager__c='sandeep m';
        c.Internal_Wholesaler1__c=Internal.id; 
        c.Sales_Director__c=External.id;
    
        
        insert c;
    
    User user = new User();
    user.FirstName = 'Test222';
    user.LastName = 'Guy222';
    user.Phone = '0123456789';
    user.Email = 'testguy2@testyourcode.com';
    user.CommunityNickname = 'testguy22@testyourcode.com';
    user.Alias = 'HelloMan';
    user.UserName = 'test@code.com';
    user.ProfileId =  pf.Id;
    user.contactid=c.id;
    user.TimeZoneSidKey = 'GMT';
    user.LocaleSidKey = 'en_US';
    user.EmailEncodingKey = 'ISO-8859-1';
    user.LanguageLocaleKey = 'en_US';

    insert user;
    
    
     system.runas(user)
        {
     
    ocms_Vkit_ColeCapitalContact sc=new ocms_Vkit_ColeCapitalContact();
    
    
    
  try
  {  
     
     list<sobject> ProductList=sc.getProductList();
     sc.writeControls();
     sc.getHTML();
     sc.writeListView();
  }
  catch(exception e){} 
  }
   }
 static testMethod void testocmsColeCapitalContact1() {

 profile pf=[select id,name from profile where name='Cole Capital Community'];
       // user u=[select id,username,contactid,firstname,lastname from user where profileid=:pf.id limit 1];
               
        account a=new account();
        a.name='test';
        insert a;
       
     
        Contact c= new Contact();
        c.firstName='sandeep';
        c.lastName='m';
        c.HomePhone='9999999999';
        c.title='Director';
        c.phone='9999999999';
        c.accountid=a.id;
        c.email='test@test1.com';
        c.Image_URL__c='/01p500000001k7V';
        c.Internal_Wholesaler__c='sandeep m';
        c.Wholesaler__c='sandeep m';
        c.Regional_Territory_Manager__c='sandeep m';
        
        insert c;
    
    User user = new User();
    user.FirstName = 'Test222';
    user.LastName = 'Guy222';
    user.Phone = '0123456789';
    user.Email = 'testguy2@testyourcode1.com';
    user.CommunityNickname = 'testguy2221@testyourcode.com';
    user.Alias = 'HelloMan';
    user.UserName = 'testguy224@testyourcode.com';
    user.ProfileId =  pf.Id;
    user.contactid=c.id;
    user.TimeZoneSidKey = 'GMT';
    user.LocaleSidKey = 'en_US';
    user.EmailEncodingKey = 'ISO-8859-1';
    user.LanguageLocaleKey = 'en_US';

    insert user;
       System.assertEquals(user.ProfileId,pf.Id);

     system.runas(user)
        {
     
    ocms_Vkit_ColeCapitalContact sc=new ocms_Vkit_ColeCapitalContact();
    
    
    
  try
  {  
     
     list<sobject> ProductList=sc.getProductList();
     sc.writeControls();
     sc.getHTML();
     sc.writeListView();
  }
  catch(exception e){} 
  }
   }  
}