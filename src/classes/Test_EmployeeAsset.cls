@isTest
public class Test_EmployeeAsset
{
     static testmethod void myTest_Method1()
    {
        
        
        list<Employee__c > clist= new list<Employee__c >();
       
        Employee__c emp = new Employee__c();
        emp.name = 'Test';
        emp.Start_Date__c= system.today();
        emp.Supervisor__c = 'Supervisor';
        emp.First_Name__c ='N';
        emp.Title__c ='Title';
        emp.Last_Name__c ='Test';
        emp.Move_User_To__c ='move user';
        emp.Office_Location__c ='Hyderabad';
        emp.Employee_Role__c  ='Developer';
        emp.HR_Manager_Status__c ='Submitted for Approval';
        emp.Employee_Status__c='On Boarding inProcess';
        emp.Department__c ='26-Information Technology';
        emp.Active_Directory_Group__c='Sec-26-Information Technology';
        emp.Active_Directory_Group_2__c=  '';
        emp.Active_Directory_Group_3__c = '';
        emp.Active_Directory_Group_4__c = '';
        emp.Active_Directory_Group_5__c = '';
        emp.Active_Directory_Group_6__c = '';
        //clist.add(emp);  
    
    
        Employee__c emp1 = new Employee__c();
        emp1.name = 'Test';
        emp1.Start_Date__c= system.today();
        emp1.Supervisor__c = 'Supervisor';
        clist.add(emp1);
    
        insert clist;
        
        system.assert(clist != Null);

    
       Asset_Inventory__c cte=new Asset_Inventory__c();
       cte.Name='General';
       cte.Asset_Type__c='Building';
       cte.Application_Owner__c=userinfo.getUserid();
       cte.Application_Implementor__c=userinfo.getUserid();
       insert cte;
       
      system.assert(cte != Null);

       
       Asset_Inventory__c cte1=new Asset_Inventory__c();
       cte1.Name='Executive';
       cte1.Asset_Type__c='Parking';
       cte1.Application_Owner__c=userinfo.getUserid();
       cte1.Application_Implementor__c=userinfo.getUserid();
       insert cte1;
    
       system.assert(cte1 != Null);

    
        EmployeeAseet batch = new EmployeeAseet();
        Id batchId = Database.executeBatch(batch,1);
       // batch.execute(clist);
    
    
     }
     
     
   }