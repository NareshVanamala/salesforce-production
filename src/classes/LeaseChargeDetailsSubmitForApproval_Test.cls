@isTest(SeeAllData = true)
private class LeaseChargeDetailsSubmitForApproval_Test
{
   User user =[SELECT Id, Username FROM User where name='Naresh Vanamala'];
   static testMethod void testDoGet() {
       MRI_PROPERTY__c mriPropObj = new MRI_PROPERTY__c();
       mriPropObj.Name='Test';
       mriPropObj.Asset_Manager__c='00550000002tACW';
       mriPropObj.Property_Manager__c='00550000002tACW';
       mriPropObj.Director_Vice_President__c='00550000002tACW';
       mriPropObj.SVP__c='00550000002tACW';
       mriPropObj.Property_ID__c='458577';
       mriPropObj.Property_Type__c='Multi-Tenant';
       insert mriPropObj;
       
        Lease__c leaseobj =new Lease__c();
        leaseobj.name = 'Test Lease';
        leaseobj.Lease_ID__c='15644';
        leaseobj.MRI_PROPERTY__c = mriPropObj.id;
        leaseobj.MRI_PM__c = userinfo.getuserid();
        leaseobj.Lease_Status_Time_Stamp__c= Datetime.now();
        leaseobj.Lease_Status__c='Active';
        leaseobj.Lease_Category__c='Lease Category 1';
        leaseobj.Income_Category__c='ADM-Administrative Fee';
        leaseobj.Web_Ready__c='Pending Approval';
        insert leaseobj;
        
        List<Lease_Charges_Request__c> LCR = new List<Lease_Charges_Request__c>();
 
        Lease_Charges_Request__c leaseCharReqobj = new  Lease_Charges_Request__c();
        leaseCharReqobj.Lease__c=leaseobj.id;
        leaseCharReqobj.Source_Code__c='test';
        leaseCharReqobj.Approval_Status__c='Not Submitted for Approval';
        leaseCharReqobj.Unique_ID__c='12536';
        leaseCharReqobj.MRI_PROPERTY__c = mriPropObj.id;
        LCR.add(leaseCharReqobj);
        //insert leaseCharReqobj;
        
        Lease_Charges_Request__c leaseCharReqobj1 = new  Lease_Charges_Request__c();
        leaseCharReqobj1.Lease__c=leaseobj.id;
        leaseCharReqobj1.Source_Code__c='test1';
        leaseCharReqobj1.Approval_Status__c='Not Submitted for Approval';
        leaseCharReqobj1.MRI_PROPERTY__c = mriPropObj.id;
        LCR.add(leaseCharReqobj1);
        //insert leaseCharReqobj1;
        insert LCR;
        
        Lease_Charge_Details__c leaseCharDetailsobj = new  Lease_Charge_Details__c();
        leaseCharDetailsobj.LeaseChargesRequest__c=leaseCharReqobj.id;
        leaseCharDetailsobj.Charge_Code__c='MGT - Management Fee';
        leaseCharDetailsobj.Charge_Type__c='RC - Rent Modification (non-CPI)';
        leaseCharDetailsobj.Description__c='Test';
        leaseCharDetailsobj.Support_Required__c = 'Yes';
        insert leaseCharDetailsobj; 

        Test.startTest();
        System.assertEquals(mriPropObj.id,leaseobj.MRI_PROPERTY__c);
        for (Lease_Charges_Request__c a : LCR) 
        {
            Integer memberCount = LCR.size();
            System.assertEquals(2, memberCount);
        }

        System.assertEquals(leaseCharReqobj.id,leaseCharDetailsobj.LeaseChargesRequest__c);
        
        ApexPages.CurrentPage().getparameters().put('id', leaseCharReqobj.id);
        ApexPages.CurrentPage().getparameters().put('id', leaseCharReqobj1.id);
        ApexPages.StandardController StandardsObjectController  =  new ApexPages.StandardController(leaseCharReqobj);
        LeaseChargeDetailsMassSubmitForApproval coninvattctrl   = new LeaseChargeDetailsMassSubmitForApproval(StandardsObjectController);
        LeaseChargeDetailsMassSubmitForApproval.MassSubmit(leaseCharReqobj.id);
        LeaseChargeDetailsMassSubmitForApproval.MassSubmit(leaseCharReqobj1.id);
        
        Test.stopTest();
    }
  
  }