@isTest
public with sharing class GetURL_test {
    
    @isTest static void testMethodOne(){
        //Data preparation
        GetURL getURLObj = new GetURL();
        getURLObj.sname = 'testname';
        getURLobj.tabid = '5';
        PageReference pageRef = new PageReference('http://www.google.com/myPage?sname=bob&tabid=1');        
        PageReference pageRef2 = new PageReference('http://www.google.com/Preview??sname=bob&tabid=1');
        PageReference pageRef3 = new PageReference('http://www.google.com/Debug??sname=bob&tabid=1');
        PageReference pageRef4 = new PageReference('http://www.google.com/');
        PageReference pageRef5 = new PageReference('http://www.google.com/');
        
        System.assertEquals(getURLObj.sname,'testname');

        //Begin testing 
        Test.startTest();
        
        getURLObj.escapeDoubleQuotes('http://www.google.com');
        
        Test.setCurrentPage(pageRef);
        Test.setCurrentPageReference(pageRef);
        getURLObj.getURL('firstRun');
        
        pageRef2.getParameters().put('sname','sname');
        pageRef2.getParameters().put('tabid','tabid');
        Test.setCurrentPage(pageRef2);
        Test.setCurrentPageReference(pageRef2);
        getURLObj.getURL('Preview');        
        
        pageRef3.getParameters().put('sname','sname');
        pageRef3.getParameters().put('tabid','tabid');
        Test.setCurrentPage(pageRef3);
        Test.setCurrentPageReference(pageRef3);
        getURLObj.getURL('Debug');
        
        pageRef4.getParameters().put('sname','sname');
        Test.setCurrentPage(pageRef4);
        Test.setCurrentPageReference(pageRef4);
        getURLObj.getURL('onlySName');
        
        Test.setCurrentPage(pageRef5);
        Test.setCurrentPageReference(pageRef5);
        getURLObj.getURL('neitherAttribute');
        
        Test.stopTest();

    }
    
}