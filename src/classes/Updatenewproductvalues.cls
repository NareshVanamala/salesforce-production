global class Updatenewproductvalues implements Database.Batchable<SObject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String Query;
        if(Test.isRunningTest()){
            system.debug('count of existing records is......' + [SELECT count() from National_Account_Helper__c]);
            Query = 'SELECT Id,JanIncomeNavA__c,FebIncomeNAVA__c,MarIncomeNAVA__c,AprilIncomeNAVA__c,MayIncomeNAVA__c,JuneIncomeNAVA__c,JulyIncomeNAVA__c,'+
                    'AugIncomeNAVA__c,SepIncomeNAVA__c,OctIncomeNAVA__c,NovIncomeNAVA__c,DecIncomeNAVA__c,JanIncomeNAVI__c,FebIncomeNAVI__c,MarIncomeNAVI__c,'+
                    'AprIncomeNAVI__c,MayIncomeNAVI__c,JuneIncomeNAVI__c,JulyIncomeNAVI__c,AugIncomeNAVI__c,SepIncomeNAVI__c,OctIncomeNAVI__c,NovIncomeNAVI__c,'+
                    'DecIncomeNAVI__c,janCCITII__c,FebCCITII__c,MarCCITII__c,AprCCITII__c,MayCCITII__c,JuneCCITII__c,JulyCCITII__c,AugCCITII__c,SepCCITII__c,'+
                    'octCCITII__c,NovCCITII__c,DecCCITII__c,JanCCPTV__c,FebCCPTV__c,MarCCPTV__c,AprCCPTV__c,MayCCPTV__c,JuneCCPTV__c,JulyCCPTV__c,AugCCPTV__c,'+
                    'SepCCPTV__c,OctCCPTV__c,NovCCPTV__c,DecCCPTV__c,IncomeNAVALastyear__c,IncomeNAVILastyear__c,CCITIILastyear__c,CCPTVLastyear__c,'+
                    'CurrentYearMinus2IncomeNAVA__c,CurrentYearMinus2IncomeNAVI__c,CurrentYearMinus2CCITII__c,CurrentYearMinus2CCPTV__c,'+
                    'CurrentyearMinus3IncomeNAVA__c,CurrentyearMinus3IncomeNAVI__c,CurrentyearMinus3CCITII__c,CurrentyearMinus3CCPTV__c,'+
                    
                    
                    'AprilCCIT__c,AprilCCPT__c,AprilCCPTII__c,AprilCCPTIII__c,AprilCCPTIV__c,AprilIncomeNav__c,AugustCCIT__c,AugustCCPT__c,AugustCCPTII__c,'+
                    'AugustCCPTIII__c,AugustCCPTIV__c,AugustIncomeNav__c,CCITlastyear__c,CCPTIIILastYear__c,CCPTIILastYear__c,CCPTIVlastyear__c,CCPT_LastYear__c,'+
                    'CurrentYearMinus2CCIT__c,CurrentYearMinus2CCPT__c,CurrentYearMinus2CCPTII__c,CurrentYearMinus2CCPTIII__c,CurrentYearMinus2CCPTIV__c,'+
                    'CurrentYearMinus2IncomeNav__c,CurrentyearMinus3CCIT__c,CurrentYearMinus3CCPT__c,CurrentYearMinus3CCPTII__c,CurrentYearMinus3CCPTIII__c,'+
                    'CurrentYearMinus3CCPTIV__c,CurrentYearMinus3IncomeNav__c,FebCCIT__c,FebCCPT__c,FebCCPTII__c,FebCCPTIII__c,FebCCPTIV__c,FebIncomeNav__c,'+
                    'INCOMENAVLastyear__c,JanCCIT__c,JanCCPT__c,JanCCPTII__c,JanCCPTIII__c,JanCCPTIV__c,JanIncomeNav__c,JulyCCIT__c,JulyCCPT__c,JulyCCPTII__c,'+
                    'JulyCCPTIII__c,JulyCCPTIV__c,JulyIncomeNav__c,JuneCCIT__c,JuneCCPT__c,JuneCCPTII__c,JuneCCPTIII__c,JuneCCPTIV__c,JuneIncomeNav__c,MarCCIT__c,'+
                    'MarCCPT__c,MarCCPTII__c,MarCCPTIII__c,MarCCPTIV__c,MarIncomeNav__c,MayCCIT__c,MayCCPT__c,MayCCPTII__c,MayCCPTIII__c,MayCCPTIV__c,MayIncomeNav__c,'+
                    'NovCCIT__c,NovCCPT__c,NovCCPTII__c,NovCCPTIII__c,NovIncomeNav__c,OctCCIT__c,OctCCPT__c,OctCCPTII__c,OctCCPTIII__c,OctCCPTIV__c,OctIncomeNav__c,'+
                    'SeptCCIT__c,SeptCCPT__c,SeptCCPTII__c,SeptCCPTIII__c,SeptCCPTIV__c,SeptIncomeNav__c,DecCCIT__c,DecCCPT__c,DecCCPTII__c,DecCCPTIII__c,DecCCPTIV__c,'+
                    'DecIncomeNav__c,NovCCPTIV__c  FROM  National_Account_Helper__c Limit 1';
                    
            system.debug('Myquery is......' + Query);
            return Database.getQueryLocator(Query);         
        }
        else{
            system.debug('count of existing records is......' + [SELECT count() from National_Account_Helper__c]);
            Query = 'SELECT Id,JanIncomeNavA__c,FebIncomeNAVA__c,MarIncomeNAVA__c,AprilIncomeNAVA__c,MayIncomeNAVA__c,JuneIncomeNAVA__c,JulyIncomeNAVA__c,'+
                    'AugIncomeNAVA__c,SepIncomeNAVA__c,OctIncomeNAVA__c,NovIncomeNAVA__c,DecIncomeNAVA__c,JanIncomeNAVI__c,FebIncomeNAVI__c,MarIncomeNAVI__c,'+
                    'AprIncomeNAVI__c,MayIncomeNAVI__c,JuneIncomeNAVI__c,JulyIncomeNAVI__c,AugIncomeNAVI__c,SepIncomeNAVI__c,OctIncomeNAVI__c,NovIncomeNAVI__c,'+
                    'DecIncomeNAVI__c,janCCITII__c,FebCCITII__c,MarCCITII__c,AprCCITII__c,MayCCITII__c,JuneCCITII__c,JulyCCITII__c,AugCCITII__c,SepCCITII__c,'+
                    'octCCITII__c,NovCCITII__c,DecCCITII__c,JanCCPTV__c,FebCCPTV__c,MarCCPTV__c,AprCCPTV__c,MayCCPTV__c,JuneCCPTV__c,JulyCCPTV__c,AugCCPTV__c,'+
                    'SepCCPTV__c,OctCCPTV__c,NovCCPTV__c,DecCCPTV__c,IncomeNAVALastyear__c,IncomeNAVILastyear__c,CCITIILastyear__c,CCPTVLastyear__c,'+
                    'CurrentYearMinus2IncomeNAVA__c,CurrentYearMinus2IncomeNAVI__c,CurrentYearMinus2CCITII__c,CurrentYearMinus2CCPTV__c,'+
                    'CurrentyearMinus3IncomeNAVA__c,CurrentyearMinus3IncomeNAVI__c,CurrentyearMinus3CCITII__c,CurrentyearMinus3CCPTV__c,'+
                    
                    
                    'AprilCCIT__c,AprilCCPT__c,AprilCCPTII__c,AprilCCPTIII__c,AprilCCPTIV__c,AprilIncomeNav__c,AugustCCIT__c,AugustCCPT__c,AugustCCPTII__c,'+
                    'AugustCCPTIII__c,AugustCCPTIV__c,AugustIncomeNav__c,CCITlastyear__c,CCPTIIILastYear__c,CCPTIILastYear__c,CCPTIVlastyear__c,CCPT_LastYear__c,'+
                    'CurrentYearMinus2CCIT__c,CurrentYearMinus2CCPT__c,CurrentYearMinus2CCPTII__c,CurrentYearMinus2CCPTIII__c,CurrentYearMinus2CCPTIV__c,'+
                    'CurrentYearMinus2IncomeNav__c,CurrentyearMinus3CCIT__c,CurrentYearMinus3CCPT__c,CurrentYearMinus3CCPTII__c,CurrentYearMinus3CCPTIII__c,'+
                    'CurrentYearMinus3CCPTIV__c,CurrentYearMinus3IncomeNav__c,FebCCIT__c,FebCCPT__c,FebCCPTII__c,FebCCPTIII__c,FebCCPTIV__c,FebIncomeNav__c,'+
                    'INCOMENAVLastyear__c,JanCCIT__c,JanCCPT__c,JanCCPTII__c,JanCCPTIII__c,JanCCPTIV__c,JanIncomeNav__c,JulyCCIT__c,JulyCCPT__c,JulyCCPTII__c,'+
                    'JulyCCPTIII__c,JulyCCPTIV__c,JulyIncomeNav__c,JuneCCIT__c,JuneCCPT__c,JuneCCPTII__c,JuneCCPTIII__c,JuneCCPTIV__c,JuneIncomeNav__c,MarCCIT__c,'+
                    'MarCCPT__c,MarCCPTII__c,MarCCPTIII__c,MarCCPTIV__c,MarIncomeNav__c,MayCCIT__c,MayCCPT__c,MayCCPTII__c,MayCCPTIII__c,MayCCPTIV__c,MayIncomeNav__c,'+
                    'NovCCIT__c,NovCCPT__c,NovCCPTII__c,NovCCPTIII__c,NovIncomeNav__c,OctCCIT__c,OctCCPT__c,OctCCPTII__c,OctCCPTIII__c,OctCCPTIV__c,OctIncomeNav__c,'+
                    'SeptCCIT__c,SeptCCPT__c,SeptCCPTII__c,SeptCCPTIII__c,SeptCCPTIV__c,SeptIncomeNav__c,DecCCIT__c,DecCCPT__c,DecCCPTII__c,DecCCPTIII__c,DecCCPTIV__c,'+
                    'DecIncomeNav__c,NovCCPTIV__c  FROM  National_Account_Helper__c';
                    
            system.debug('Myquery is......' + Query);
            return Database.getQueryLocator(Query); 
        }
    }
    global void execute(Database.BatchableContext BC, List<National_Account_Helper__c> scope){
    
        List<National_Account_Helper__c> nahlist= new List<National_Account_Helper__c>();
        
        for(National_Account_Helper__c nah:scope){
            
            //if(nah.AprilCCIT__c==null){
                nah.AprilCCIT__c=0.00;
            //}
            //else if(nah.AprilCCPT__c==null){
                nah.AprilCCPT__c=0.00;
            //}
            //else if(nah.AprilCCPTII__c==null){
                nah.AprilCCPTII__c=0.00;
            //}
            //else if(nah.AprilCCPTIII__c==null){
                nah.AprilCCPTIII__c=0.00;
            //}
            //else if(nah.AprilCCPTIV__c==null){
                nah.AprilCCPTIV__c=0.00;
            //}
            //else if(nah.AprilIncomeNav__c==null){
                nah.AprilIncomeNav__c=0.00;
            //}
            //else if(nah.AugustCCIT__c==null){
                nah.AugustCCIT__c=0.00;
            //}
            //else if(nah.AugustCCPT__c==null){
                nah.AugustCCPT__c=0.00;
            //}
            //else if(nah.AugustCCPTII__c==null){
                nah.AugustCCPTII__c=0.00;
            //}
            //else if(nah.AugustCCPTIII__c==null){
                nah.AugustCCPTIII__c=0.00;
            //}
            //else if(nah.AugustCCPTIV__c==null){
                nah.AugustCCPTIV__c=0.00;
            //}
            //else if(nah.AugustIncomeNav__c==null){
                nah.AugustIncomeNav__c=0.00;
            //}
            //else if(nah.CCITlastyear__c==null){
                nah.CCITlastyear__c=0.00;
            //}
            //else if(nah.CCPTIIILastYear__c==null){
                nah.CCPTIIILastYear__c=0.00;
            //}
            //else if(nah.CCPTIILastYear__c==null){
                nah.CCPTIILastYear__c=0.00;
            //}
            //else if(nah.CCPTIVlastyear__c==null){
                nah.CCPTIVlastyear__c=0.00;
            //}
            //else if(nah.CCPT_LastYear__c==null){
                nah.CCPT_LastYear__c=0.00;
            //}
            //else if(nah.CurrentYearMinus2CCIT__c==null){
                nah.CurrentYearMinus2CCIT__c=0.00;
            //}
            //else if(nah.CurrentYearMinus2CCPT__c==null){
                nah.CurrentYearMinus2CCPT__c=0.00;
            //}
            //else if(nah.CurrentYearMinus2CCPTII__c==null){
                nah.CurrentYearMinus2CCPTII__c=0.00;
            //}
            //else if(nah.CurrentYearMinus2CCPTIII__c==null){
                nah.CurrentYearMinus2CCPTIII__c=0.00;
            //}
            //else if(nah.CurrentYearMinus2CCPTIV__c==null){
                nah.CurrentYearMinus2CCPTIV__c=0.00;
            //}
            //else if(nah.CurrentYearMinus2IncomeNav__c==null){
                nah.CurrentYearMinus2IncomeNav__c=0.00;
            //}
            //else if(nah.CurrentyearMinus3CCIT__c==null){
                nah.CurrentyearMinus3CCIT__c=0.00;
            //}
            //else if(nah.CurrentYearMinus3CCPT__c==null){
                nah.CurrentYearMinus3CCPT__c=0.00;
            //}
            //else if(nah.CurrentYearMinus3CCPTII__c==null){
                nah.CurrentYearMinus3CCPTII__c=0.00;
            //}
            //else if(nah.CurrentYearMinus3CCPTIII__c==null){
                nah.CurrentYearMinus3CCPTIII__c=0.00;
            //}
            //else if(nah.CurrentYearMinus3CCPTIV__c==null){
                nah.CurrentYearMinus3CCPTIV__c=0.00;
            //}
            //else if(nah.CurrentYearMinus3IncomeNav__c==null){
                nah.CurrentYearMinus3IncomeNav__c=0.00;
            //}
            //else if(nah.FebCCIT__c==null){
                nah.FebCCIT__c=0.00;
            //}
            //else if(nah.FebCCPT__c==null){
                nah.FebCCPT__c=0.00;
            //}
            //else if(nah.FebCCPTII__c==null){
                nah.FebCCPTII__c=0.00;
            //}
            //else if(nah.FebCCPTIII__c==null){
                nah.FebCCPTIII__c=0.00;
            //}
            //else if(nah.FebCCPTIV__c==null){
                nah.FebCCPTIV__c=0.00;
            //}
            //else if(nah.FebIncomeNav__c==null){
                nah.FebIncomeNav__c=0.00;
            //}
            //else if(nah.INCOMENAVLastyear__c==null){
                nah.INCOMENAVLastyear__c=0.00;
            //}
            //else if(nah.JanCCIT__c==null){
                nah.JanCCIT__c=0.00;
            //}
            //else if(nah.JanCCPT__c==null){
                nah.JanCCPT__c=0.00;
            //}
            //else if(nah.JanCCPTII__c==null){
                nah.JanCCPTII__c=0.00;
            //}
            //else if(nah.JanCCPTIII__c==null){
                nah.JanCCPTIII__c=0.00;
            //}
            //else if(nah.JanCCPTIV__c==null){
                nah.JanCCPTIV__c=0.00;
            //}
            //else if(nah.JanIncomeNav__c==null){
                nah.JanIncomeNav__c=0.00;
            //}
            //else if(nah.JulyCCIT__c==null){
                nah.JulyCCIT__c=0.00;
            //}
            //else if(nah.JulyCCPT__c==null){
                nah.JulyCCPT__c=0.00;
            //}
            //else if(nah.JulyCCPTII__c==null){
                nah.JulyCCPTII__c=0.00;
            //}
            //else if(nah.JulyCCPTIII__c==null){
                nah.JulyCCPTIII__c=0.00;
            //}
            //else if(nah.JulyCCPTIV__c==null){
                nah.JulyCCPTIV__c=0.00;
            //}
            //else if(nah.JulyIncomeNav__c==null){
                nah.JulyIncomeNav__c=0.00;
            //}
            //else if(nah.JuneCCIT__c==null){
                nah.JuneCCIT__c=0.00;
            //}
            //else if(nah.JuneCCPT__c==null){
                nah.JuneCCPT__c=0.00;
            //}
            //else if(nah.JuneCCPTII__c==null){
                nah.JuneCCPTII__c=0.00;
            //}
            //else if(nah.JuneCCPTIII__c==null){
                nah.JuneCCPTIII__c=0.00;
            //}
            //else if(nah.JuneCCPTIV__c==null){
                nah.JuneCCPTIV__c=0.00;
            //}
            //else if(nah.JuneIncomeNav__c==null){
                nah.JuneIncomeNav__c=0.00;
            //}
            //else if(nah.MarCCIT__c==null){
                nah.MarCCIT__c=0.00;
            //}
            //else if(nah.MarCCPT__c==null){
                nah.MarCCPT__c=0.00;
            //}
            //else if(nah.MarCCPTII__c==null){
                nah.MarCCPTII__c=0.00;
            //}
            //else if(nah.MarCCPTIII__c==null){
                nah.MarCCPTIII__c=0.00;
            //}
            //else if(nah.MarCCPTIV__c==null){
                nah.MarCCPTIV__c=0.00;
            //}
            //else if(nah.MarIncomeNav__c==null){
                nah.MarIncomeNav__c=0.00;
            //}
            //else if(nah.MayCCIT__c==null){
                nah.MayCCIT__c=0.00;
            //}
            //else if(nah.MayCCPT__c==null){
                nah.MayCCPT__c=0.00;
            //}
            //else if(nah.MayCCPTII__c==null){
                nah.MayCCPTII__c=0.00;
            //}
            //else if(nah.MayCCPTIII__c==null){
                nah.MayCCPTIII__c=0.00;
            //}
            //else if(nah.MayCCPTIV__c==null){
                nah.MayCCPTIV__c=0.00;
            //}
            //else if(nah.MayIncomeNav__c==null){
                nah.MayIncomeNav__c=0.00;
            //}
            //else if(nah.NovCCIT__c==null){
                nah.NovCCIT__c=0.00;
            //}
            //else if(nah.NovCCPT__c==null){
                nah.NovCCPT__c=0.00;
            //}
            //else if(nah.NovCCPTII__c==null){
                nah.NovCCPTII__c=0.00;
            //}
            //else if(nah.NovCCPTIII__c==null){
                nah.NovCCPTIII__c=0.00;
            //}
            //else if(nah.OctCCIT__c==null){
                nah.OctCCIT__c=0.00;
            //}
            //else if(nah.OctCCPT__c==null){
                nah.OctCCPT__c=0.00;
            //}
            //else if(nah.OctCCPTII__c==null){
                nah.OctCCPTII__c=0.00;
            //}
            //else if(nah.OctCCPTIII__c==null){
                nah.OctCCPTIII__c=0.00;
            //}
            //else if(nah.OctCCPTIV__c==null){
                nah.OctCCPTIV__c=0.00;
            //}
            //else if(nah.OctIncomeNav__c==null){
                nah.OctIncomeNav__c=0.00;
            //}
            //else if(nah.SeptCCIT__c==null){
                nah.SeptCCIT__c=0.00;
            //}
            //else if(nah.SeptCCPT__c==null){
                nah.SeptCCPT__c=0.00;
            //}
            //else if(nah.SeptCCPTII__c==null){
                nah.SeptCCPTII__c=0.00;
            //}
            //else if(nah.SeptCCPTIII__c==null){
                nah.SeptCCPTIII__c=0.00;
            //}
            //else if(nah.SeptCCPTIV__c==null){
                nah.SeptCCPTIV__c=0.00;
            //}
            //else if(nah.SeptIncomeNav__c==null){
                nah.SeptIncomeNav__c=0.00;
            //}
            //else if(nah.DecCCIT__c==null){
                nah.DecCCIT__c=0.00;
            //}
            //else if(nah.DecCCPT__c==null){
                nah.DecCCPT__c=0.00;
            //}
            //else if(nah.DecCCPTII__c==null){
                nah.DecCCPTII__c=0.00;
            //}
            //else if(nah.DecCCPTIII__c==null){
                nah.DecCCPTIII__c=0.00;
            //}
            //else if(nah.DecCCPTIV__c==null){
                nah.DecCCPTIV__c=0.00;
            //}
            //else if(nah.DecIncomeNav__c==null){
                nah.DecIncomeNav__c=0.00;
            //}
            //else if(nah.NovCCPTIV__c==null){
                nah.NovCCPTIV__c=0.00;
            //}
            
            nah.JanIncomeNavA__c =0.00;
            nah.FebIncomeNAVA__c=0.00;
            nah.MarIncomeNAVA__c=0.00;
            nah.AprilIncomeNAVA__c=0.00;
            nah.MayIncomeNAVA__c=0.00;
            nah.JuneIncomeNAVA__c=0.00;
            nah.JulyIncomeNAVA__c=0.00;
            nah.AugIncomeNAVA__c=0.00;
            nah.SepIncomeNAVA__c=0.00;
            nah.OctIncomeNAVA__c=0.00;
            nah.NovIncomeNAVA__c=0.00;
            nah.DecIncomeNAVA__c=0.00;
            nah.JanIncomeNAVI__c=0.00;
            nah.FebIncomeNAVI__c=0.00;
            nah.MarIncomeNAVI__c=0.00;
            nah.AprIncomeNAVI__c=0.00;
            nah.MayIncomeNAVI__c=0.00;
            nah.JuneIncomeNAVI__c=0.00;
            nah.JulyIncomeNAVI__c=0.00;
            nah.AugIncomeNAVI__c=0.00;
            nah.SepIncomeNAVI__c=0.00;
            nah.OctIncomeNAVI__c=0.00;
            nah.NovIncomeNAVI__c=0.00;
            nah.DecIncomeNAVI__c=0.00;
            nah.janCCITII__c=0.00;
            nah.FebCCITII__c=0.00;
            nah.MarCCITII__c=0.00;
            nah.AprCCITII__c=0.00;
            nah.MayCCITII__c=0.00;
            nah.JuneCCITII__c=0.00;
            nah.JulyCCITII__c=0.00;
            nah.AugCCITII__c=0.00;
            nah.SepCCITII__c=0.00;
            nah.octCCITII__c=0.00;
            nah.NovCCITII__c=0.00;
            nah.DecCCITII__c=0.00;
            nah.JanCCPTV__c=0.00;
            nah.FebCCPTV__c=0.00;
            nah.MarCCPTV__c=0.00;
            nah.AprCCPTV__c=0.00;
            nah.MayCCPTV__c=0.00;
            nah.JuneCCPTV__c=0.00;
            nah.JulyCCPTV__c=0.00;
            nah.AugCCPTV__c=0.00;
            nah.SepCCPTV__c=0.00;
            nah.OctCCPTV__c=0.00;
            nah.NovCCPTV__c=0.00;
            nah.DecCCPTV__c=0.00;
            
            nah.IncomeNAVALastyear__c=0.00;
            nah.IncomeNAVILastyear__c=0.00;
            nah.CCITIILastyear__c=0.00;
            nah.CCPTVLastyear__c=0.00;
            nah.CurrentYearMinus2IncomeNAVA__c=0.00;
            nah.CurrentYearMinus2IncomeNAVI__c=0.00;
            nah.CurrentYearMinus2CCITII__c=0.00;
            nah.CurrentYearMinus2CCPTV__c=0.00;
            nah.CurrentyearMinus3IncomeNAVA__c=0.00;
            nah.CurrentyearMinus3IncomeNAVI__c=0.00;
            nah.CurrentyearMinus3CCITII__c=0.00;
            nah.CurrentyearMinus3CCPTV__c =0.00;
    
            nahlist.add(nah);
        }
        if(!nahlist.isEmpty())
        {
            if (Schema.sObjectType.National_Account_Helper__c.isUpdateable()) 

            update nahlist;
        }
    
    }
    global void finish(Database.BatchableContext BC){   

    }
}