global with sharing class RC_UpdateRskPrtfliofrmProperties implements Database.Batchable<SObject>
{
    /*global Portfolio__c pf = new Portfolio__c ();
    List<Deal__c> pfd = new List<Deal__c>();
    List<Deal__c> pfd11 = new List<Deal__c>();
    Deal__c DealNamelist = new Deal__c();
    List<Deal__c> pfdForstatus = new List<Deal__c>();
    set<Id> portids = new Set<Id>();
    set<String>Cityset= new set<String>();*/
    //String City = null;
    
 global Database.QueryLocator start(Database.BatchableContext BC)  
 {
        String Query;
        Query = 'Select id from RC_Risk_Portfolio__c';
        return Database.getQueryLocator(Query);
       
 }

  global void execute(Database.BatchableContext BC,List<sObject> scope)  
  {
  
      Map<Id, List<RC_Property__c>> Rc_PortfoliotoProperty = new Map<Id, List<RC_Property__c>>();
      List<RC_Risk_Portfolio__c> portlist = new List<RC_Risk_Portfolio__c>();
      Map<Id, RC_Risk_Portfolio__c> portfolioMap = new Map<Id, RC_Risk_Portfolio__c>();
      List<RC_Property__c> scopeProperties = new List<RC_Property__c>();
      Set<Id> RiskPortfolioids = new Set<Id>();
      
  
        for(sObject a:scope)
          RiskPortfolioids.add(a.Id);
      
      scopeProperties =  [Select id,Risk_Portfolio__c,Status__c,Deal_Type__c,Portfolio__c,Contract_Price__c,Year_1_NOI__c,Cap_Rate__c,Total_Square_Footage__c,Price_PSF_Total_Price__c,Rent_PSF__c  from RC_Property__c where Risk_Portfolio__c in:RiskPortfolioids ];
      
      for(RC_Property__c rd : scopeProperties )
      {    
           if(Rc_PortfoliotoProperty.containsKey(rd.Risk_Portfolio__c ))
           {
             Rc_PortfoliotoProperty.get(rd.Risk_Portfolio__c ).add(rd);
           }
           else
           {
             Rc_PortfoliotoProperty.put(rd.Risk_Portfolio__c , new List<RC_Property__c>{rd});
           } 
      }
 
      portlist=[select id,Properties__c ,Cap_Rate__c,Contract_Price__c,Deal_Type__c,Price_PSF__c,Rent_PSF__c,Total_SF__c,Year_1_NOI__c from RC_Risk_Portfolio__c where id IN :Rc_PortfoliotoProperty.keyset()]; 
       
      for(RC_Risk_Portfolio__c rcs:portlist) 
        portfolioMap.put(rcs.id,rcs); 
        
      for(Id rep : Rc_PortfoliotoProperty.keyset())
      {
          
        list<RC_Property__c> deallist= new list<RC_Property__c>();
        Integer Reviewingcount=0;
        Integer Marketed=0; 
        Integer LOISent=0;
        Integer SignedLOI=0; 
        Integer EscrowPending=0;
        Integer EscrowStudy=0;
        Integer EscrowHard=0;
        Integer ClosedOwned=0;
        Integer closedsold=0;
        Integer OnHold=0;
        Integer Lost=0;
        Integer Dead=0;
        Integer DeadLegal=0;
        Integer UnderContractSignedPSA=0;
        String DealType = null;
        decimal Contractprice=0;
        decimal noi=0;
        
        decimal caprate=0.00;
        String Caprate1=null;
        decimal caprateavg = 0;
        Integer totalsf=0;
        
        List<Integer> rentpsfavglist = new List<Integer>();
        String rentpsf1=null;
        decimal rentpsf=0.00;
        decimal rentpsfavg = 0;
        
        List<Integer> pricepsfavglist = new List<Integer>();
        String pricepsf1=null;
        decimal pricepsf=0.00;
        decimal pricepsfavg = 0;
        
        List<Integer> caprateavglist = new List<Integer>();
        if(portfolioMap.containsKey(rep))
        {
            deallist=Rc_PortfoliotoProperty.get(rep);
            deallist.sort();
            integer sizedeallist=deallist.size();
            for(RC_Property__c rct : deallist )
            {
              //***********************this is for status update ********************************************
                if(rct.Status__c!= null)
                {
                   if(rct.Status__c=='Reviewing')
                       Reviewingcount = Reviewingcount+1;
                   if(rct.Status__c=='Marketed')
                       Marketed= Marketed+1;     
                   if(rct.Status__c=='LOI Sent')
                       LOISent= LOISent+1;      
                   if(rct.Status__c=='Signed LOI')
                       SignedLOI= SignedLOI+1; 
                   if(rct.Status__c=='Under Contract/ Signed PSA')
                       UnderContractSignedPSA= UnderContractSignedPSA+1;    
                   if(rct.Status__c=='Escrow Pending')
                       EscrowPending= EscrowPending+1; 
                   if(rct.Status__c=='Escrow Study')
                       EscrowStudy= EscrowStudy+1; 
                   if(rct.Status__c=='Escrow Hard')
                       EscrowHard= EscrowHard+1;  
                   if(rct.Status__c=='Closed/Owned')
                       ClosedOwned= ClosedOwned+1;  
                   if(rct.Status__c=='Closed/Sold')
                       closedsold= closedsold+1;          
                   if(rct.Status__c=='On Hold')
                       OnHold= OnHold+1; 
                   if(rct.Status__c=='Lost')
                       Lost= Lost+1; 
                   if(rct.Status__c=='Dead')
                       Dead= Dead+1; 
                   if(rct.Status__c=='Dead-Legal')
                       DeadLegal= DeadLegal+1;    
                  }
                 //***********************this is for status update finished ******************************************** 
                  //***********************this is for deal Type started ******************************************** 
                 if(rct.Deal_Type__c != null && rct.Status__c!='Dead' && rct.Status__c!='Dead-Legal')
                {
                    DealType = rct.Deal_Type__c;
                    System.debug('$$$$$$' + DealType);
                } 
               //***********************this is for deal Type finished******************************************** 
                //***********************this is for Contract price started ******************************************** 
                if(rct.Contract_Price__c != null && rct.Status__c!='Dead' && rct.Status__c!='Dead-Legal')
                {
                    Contractprice += rct.Contract_Price__c;
                    System.debug('$$$$$$' + Contractprice);
                }
                //***********************this is for Contract price finished ********************************************
               //***********************this is for Noi Started ********************************************
                  if(rct.Year_1_NOI__c != null && rct.Status__c!='Dead' && rct.Status__c!='Dead-Legal')
                {
                    noi += rct.Year_1_NOI__c;
                    System.debug('$$$$$$' + noi);
                }
               //***********************this is for Noi finished ********************************************
                if(rct.CAP_Rate__c!= null && rct.Status__c!='Dead' && rct.Status__c!='Dead-Legal')
                {
                    caprateavglist.add(Integer.valueof(rct.CAP_Rate__c));
                    caprate += rct.CAP_Rate__c;
                    caprateavg = caprate / caprateavglist.size();
                    System.debug('$$$$$$' + caprateavg);
                }
                if(rct.Total_Square_Footage__c!= null && rct.Status__c!='Dead' && rct.Status__c!='Dead-Legal')
                {
                    totalsf += Integer.valueof(rct.Total_Square_Footage__c);
                    System.debug('$$$$$$' + totalsf);
                }
      
               if(rct.Rent_PSF__c!= null && rct.Status__c!='Dead' && rct.Status__c!='Dead-Legal')
                {
                    rentpsfavglist.add(Integer.valueof(rct.Rent_PSF__c));
                    rentpsf += rct.Rent_PSF__c;
                    rentpsfavg = rentpsf / rentpsfavglist.size();
                    System.debug('$$$$$$' + rentpsf);
                }   
                if(rct.Price_PSF_Total_Price__c!= null && rct.Status__c!='Dead' && rct.Status__c!='Dead-Legal')
                {
                    pricepsfavglist.add(Integer.valueof(rct.Price_PSF_Total_Price__c));
                    pricepsf += rct.Price_PSF_Total_Price__c;
                    pricepsfavg = pricepsf / pricepsfavglist.size();
                    System.debug('$$$$$$' + pricepsfavg);
                }
       }
        
          //Updating status of portfolio//    
              if(Reviewingcount>0)
              portfolioMap.get(rep).Status__c ='Reviewing';
            else if(Marketed>0)
               portfolioMap.get(rep).Status__c ='Marketed';
            else if(LOISent>0)
               portfolioMap.get(rep).Status__c ='LOI Sent';
            else if(SignedLOI>0)
               portfolioMap.get(rep).Status__c ='Signed LOI';               
            else if(UnderContractSignedPSA>0)
               portfolioMap.get(rep).Status__c ='Under Contract/ Signed PSA';
             else if(EscrowPending>0)
               portfolioMap.get(rep).Status__c ='Escrow Pending';
            else if(EscrowStudy>0)
               portfolioMap.get(rep).Status__c ='Escrow Study';   
            else if(EscrowHard>0)
               portfolioMap.get(rep).Status__c ='Escrow Hard';
            else if(ClosedOwned>0)
               portfolioMap.get(rep).Status__c ='Closed/Owned';
            else if(closedsold>0)
               portfolioMap.get(rep).Status__c ='Closed/Sold';
            else if(OnHold>0)
               portfolioMap.get(rep).Status__c ='On Hold';
            else if(Lost>0)
               portfolioMap.get(rep).Status__c ='Lost';
            else if(Dead>0)
               portfolioMap.get(rep).Status__c ='Dead';                
              else if(DeadLegal>0)
               portfolioMap.get(rep).Status__c ='Dead-Legal';    
        
     //update the status finshed
     
        if(Contractprice != null)
            {
               // Contractprice=Integer.valueof(groupedResults[0].get('CPRICE'));
               Contractprice = Contractprice.setscale(2);
                portfolioMap.get(rep).Contract_Price__c=Contractprice;
                //System.debug('$$$$$$' + Contractprice);
            }
            if(totalsf != null)
            {                
                portfolioMap.get(rep).Total_SF__c = totalsf;
            }
            if(noi != null)
            {               
            noi = noi.setscale(2); 
                portfolioMap.get(rep).Year_1_NOI__c = noi;
            }
            
            if(pricepsf != null)
            {
               pricepsf1 = string.valueOf(pricepsfavg);
               pricepsf =  decimal.valueOf(pricepsf1);
               pricepsf = pricepsf.setscale(2);
               portfolioMap.get(rep).Price_PSF__c = '$' + String.valueOf(pricepsf);
            }
            if(caprate != null)
            {
               caprate1 = string.valueOf(caprateavg);
               caprate =  decimal.valueOf(caprate1);
               caprate = caprate.setscale(2);
               portfolioMap.get(rep).Cap_Rate__c = String.valueOf(caprate)+'%';
            }
            
            if(DealType != null)
            {
                portfolioMap.get(rep).Deal_Type__c= DealType;
            } 
             portfolioMap.get(rep).Properties__c=string.valueof(sizedeallist);
 
   }
   update portfolioMap.values();
 } 
     
  }   
 global void finish(Database.BatchableContext BC)
 {
    
    
 }


}