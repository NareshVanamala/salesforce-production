global class JSON2Apex {
    public static void consumeObject(JSONParser parser) {
        Integer depth = 0;
        do {
            JSONToken curr = parser.getCurrentToken();
            if (curr == JSONToken.START_OBJECT || 
                curr == JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == JSONToken.END_OBJECT ||
                curr == JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }

    public class d {
        public List<Results_Z> results {get;set;}

        public d(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'results') {
                            results = new List<Results_Z>();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                results.add(new Results_Z(parser));
                            }
                        } else {
                            System.debug(LoggingLevel.WARN, 'd consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class metadata_Z {
        public String type {get;set;}

        public metadata_Z(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'type') {
                            type = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'metadata_Z consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public d d {get;set;}

    public JSON2Apex(JSONParser parser) {
    
        while (parser.nextToken() != JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != JSONToken.VALUE_NULL) {
                    if (text == 'd') {
                        d = new d(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'Root consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    public class Results_Z {
        public metadata metadata {get;set;}
        public Integer KeyDoc {get;set;}
        public Integer KeyFile {get;set;}
        public String FilingType {get;set;}
        public String Title {get;set;}
        public Object ShortDescription {get;set;}
        public Object FullText {get;set;}
        public String ReleaseDateTime {get;set;}
        public String ReleaseDateTimeFormatted {get;set;}
        public Integer ReleaseDay {get;set;}
        public Integer ReleaseMonth {get;set;}
        public Integer ReleaseYear {get;set;}
        public String ReleaseTime {get;set;}
       // public Object ReleaseType {get;set;}
        public String Format {get;set;}
        public RelatedDocs RelatedDocs {get;set;}

        public Results_Z(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                   //String text = '{"d":{"results":[{"__metadata":{"id":"http://data.snl.com/IRWebAPI/V1/PressReleases/PressReleases(13281131)","uri":"http://data.snl.com/IRWebAPI/V1/PressReleases/PressReleases(13281131)","type":"SNL.SNLWebX.IRWebAPI.Common.Models.PressReleases.PressReleaseEntity"},"KeyDoc":13281131,"KeyFile":11711596,"FilingType":"Press Release","Title":"American Realty Capital Properties Executive Management Team to Ring the Bell on NASDAQ","ShortDescription":null,"FullText":null,"ReleaseDateTime":"2011-09-08T17:44:00","ReleaseDateTimeFormatted":"9/8/2011","ReleaseDay":8,"ReleaseMonth":9,"ReleaseYear":2011,"ReleaseTime":"17:44:00","ReleaseType":null,"Format":"XML","RelatedDocs":{"__metadata":{"type":"Collection(SNL.SNLWebX.IRWebAPI.Common.Models.Common.RelatedDoc)"},"results":[]}}]}}';
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'metadata') {
                            metadata = new metadata(parser);
                        } else if (text == 'KeyDoc') {
                            KeyDoc = parser.getIntegerValue();
                        } else if (text == 'KeyFile') {
                            KeyFile = parser.getIntegerValue();
                        } else if (text == 'FilingType') {
                            FilingType = parser.getText();
                        } else if (text == 'Title') {
                            Title = parser.getText();
                        } else if (text == 'ShortDescription') {
                          //  ShortDescription = new Object(parser);
                        } else if (text == 'FullText') {
                          //  FullText = new Object(parser);
                        } else if (text == 'ReleaseDateTime') {
                            ReleaseDateTime = parser.getText();
                        } else if (text == 'ReleaseDateTimeFormatted') {
                            ReleaseDateTimeFormatted = parser.getText();
                        } else if (text == 'ReleaseDay') {
                            ReleaseDay = parser.getIntegerValue();
                        } else if (text == 'ReleaseMonth') {
                            ReleaseMonth = parser.getIntegerValue();
                        } else if (text == 'ReleaseYear') {
                            ReleaseYear = parser.getIntegerValue();
                        } else if (text == 'ReleaseTime') {
                            ReleaseTime = parser.getText();
                        } else if (text == 'ReleaseType') {
                  //          ReleaseType = new Object(parser);
                        } else if (text == 'Format') {
                            Format = parser.getText();
                        } else if (text == 'RelatedDocs') {
                            RelatedDocs = new RelatedDocs(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Results_Z consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class RelatedDocs {
        public metadata_Z metadata {get;set;}
        public List<Results> results {get;set;}

        public RelatedDocs(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'metadata') {
                            metadata = new metadata_Z(parser);
                        } else if (text == 'results') {
                            results = new List<Results>();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                results.add(new Results(parser));
                            }
                        } else {
                            System.debug(LoggingLevel.WARN, 'RelatedDocs consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Results {

        public Results(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        {
                            System.debug(LoggingLevel.WARN, 'Results consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class metadata {
        public String id {get;set;}
        public String uri {get;set;}
        public String type {get;set;}

        public metadata(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'id') {
                            id = parser.getText();
                        } else if (text == 'uri') {
                            uri = parser.getText();
                        } else if (text == 'type') {
                            type = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'metadata consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public static JSON2Apex parse(String json) {
        return new JSON2Apex(System.JSON.createParser(json));
    }
    }