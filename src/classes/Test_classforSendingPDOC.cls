@isTest
 private class Test_classforSendingPDOC
 {
    
       
    static testmethod void test() 
    {
    
       list<Contact_InvestorList__c>  clist= new list<Contact_InvestorList__c>(); 
        Account a = new Account();
        a.name = 'Test';
        Insert a; 
        
        Contact c1 = new Contact();
        c1.lastname = 'Cockpit';
        c1.firstname='test';
        c1.Accountid = a.id;
        c1.Relationship__c ='Accounting';
        c1.Contact_Type__c = 'Cole';
        c1.Wholesaler_Management__c = 'Producer A';
        c1.Territory__c ='Chicago';
        c1.RIA_Territory__c ='Southeast Region';
        c1.RIA_Consultant_Picklist__c ='Brian Mackin';
        c1.Internal_Consultant_Picklist__c ='Ben Satterfield';
        c1.Wholesaler__c='Andrew Garten';
        c1.Regional_Territory_Manager__c ='Andrew Garten';
        c1.Internal_Wholesaler__c ='Keith Severson';
        c1.Territory_Zone__c = 'NV-50';
        c1.Next_Planned_External_Appointment__c = system.now();
       // c1.recordtypeid = rrrecordtype;
        c1.mailingstate='OH';
        c1.email='sandeep.mariyala@polarisft.com';
        c1.Pdoc_Ready__c=true;
        insert c1;  
       
        Contact_InvestorList__c cinl= new Contact_InvestorList__c();
        cinl.Advisor_Name__c=c1.id;
        cinl.Send_Pdoc_complete__c=false;
        //insert cinl;
        clist.add(cinl);
        insert clist;
        
         Attachment attachment= new Attachment(); 
        attachment.OwnerId = UserInfo.getUserId();  
        attachment.ParentId = cinl.Id;// the record the file is attached to   
        String nameFile = 'hello';                
        attachment.Name = nameFile;
        Blob contentFile = Blob.valueOf('Unit Test Attachment Body');
        attachment.body=contentFile;
        insert attachment;
            
         
         Task t= new Task();
         t.whoid=c1.id;
         t.status='In Progress';
         t.subject='Pdoc Requested';
         insert t;
                
       
        Test.startTest();
        classforSendingPDOC np = new classforSendingPDOC();
        Database.BatchableContext bc;
    
        np.start(bc);
        np.execute(bc,clist);
        np.finish(bc);
        Test.stopTest();     
   }
   
}