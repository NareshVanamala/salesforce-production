global with sharing class Cockpit_updateLastActivityDate
{
    @InvocableMethod
    public static void UpdateLastActivitydate(list<string>tskid){
       
        system.debug('Am I getting the correct string'+tskid);
        Map<string, string>newTasktoContactMap=new Map<string,string>();
        Map<string, string>completedTasktoContactMap=new Map<string,string>();
        Set<id>newWhoidSet=new set<id>();
        Set<id>CompletedWhoidSet=new set<id>();
        list<Contact>NewTaskwhoidlist=new list<Contact>();
        list<Contact>CompletedTaskwhoidlist=new list<Contact>();
        map<id,contact>NewTaskContactMap=new map<id,Contact>();
        map<id,contact>CompletedTaskContactMap=new map<id,Contact>();
        //get the id of the task
        list<task>tlist=[select id, subject,status,whoid,ownerid ,ActivityDate from Task where id in:tskid];
        //get the contact ids form this.
        if(tlist.size()>0){
           for(Task objTask:tlist){
               if((objTask.status=='Not started')||(objTask.status=='In Progress')||(objTask.status=='New')){
                   newTasktoContactMap.put(objTask.id,objTask.whoid);
                   newWhoidSet.add(objTask.whoid);
               }
               if(objTask.status=='Completed'){
                  completedTasktoContactMap.put(objTask.id,objTask.whoid);
                  CompletedWhoidSet.add(objTask.whoid);
               }  
           }  
                 
        }
        NewTaskwhoidlist=[select id,name, account.name,Cockpit_New_Task_Due_Date__c,Cockpit_Task_Completed_Date__c from Contact where id in:newWhoidSet];
        CompletedTaskwhoidlist=[select id,name, account.name,Cockpit_New_Task_Due_Date__c,Cockpit_Task_Completed_Date__c from Contact where id in:CompletedWhoidSet];
        if(NewTaskwhoidlist.size()>0){
           for(Contact ct:NewTaskwhoidlist)
               NewTaskContactMap.put(ct.id,ct);
        }
        if(CompletedTaskwhoidlist.size()>0){ 
           for(Contact ct:CompletedTaskwhoidlist)
               CompletedTaskContactMap.put(ct.id,ct); 
        } 
      
        for(Task objTask : tlist){
            if((objTask.status=='Not started')||(objTask.status=='In Progress')||(objTask.status=='New')){
               NewTaskContactMap.get(newTasktoContactMap.get(objTask.id)).Cockpit_New_Task_Due_Date__c=objTask.ActivityDate; 
            }
            if(objTask.status=='Completed'){
               CompletedTaskContactMap.get(completedTasktoContactMap.get(objTask.id)).Cockpit_Task_Completed_Date__c=system.today();
            } 
        }
        // get all the CCL records of the related contacts and update the last activity date.
        list<Contact_Call_List__c>updatecclisist=new list<Contact_Call_List__c>();
        list<Automated_Call_List__c>Aclllist=new list<Automated_Call_List__c>();
        list<Automated_Call_List__c>updateacllist=new list<Automated_Call_List__c>();
        set<id>acutomatedcallidset=new set<id>();
         
        list<Contact_Call_List__c>cclisist=[select id, name, Contact__c,Last_Activity_Date__c,Automated_Call_List__c from Contact_Call_List__c where Contact__c in:CompletedWhoidSet FOR UPDATE];
        if(cclisist.size()>0){
           for(Contact_Call_List__c ct:cclisist){
               ct.Last_Activity_Date__c=system.today(); 
               acutomatedcallidset.add(ct.Automated_Call_List__c);
               updatecclisist.add(ct);
           }
        }  
       /* Aclllist=[select id,name,Last_Activity_Date__c from Automated_Call_List__c where id in:acutomatedcallidset FOR UPDATE];
        if(Aclllist.size()>0){
           for(Automated_Call_List__c acty:Aclllist){
               acty.Last_Activity_Date__c =system.today();
               updateacllist.add(acty);
           }  
        }*/  
        if(NewTaskContactMap.values()!=null){ 
           if(Schema.sObjectType.Contact.isUpdateable())   
              update NewTaskContactMap.values();
        }
            
        if(CompletedTaskContactMap.values()!=null){   
           if(Schema.sObjectType.Contact.isUpdateable())   
              update CompletedTaskContactMap.values();
        }
       
        if(updatecclisist.size()>0){
           if(Schema.sObjectType.Contact_Call_List__c.isUpdateable())   
              update updatecclisist;
        }
        
        /*if(updateacllist.size()>0){
           if(Schema.sObjectType.Automated_Call_List__c.isUpdateable())
              update updateacllist;  
        }*/
    } 
    
}