@isTest
private class Testsubscribedpage {

    static testmethod void test() { 
  
   Id rrrecordtype = [select Id, Name,DeveloperName from RecordType where DeveloperName ='NonInvestorRepContact' and SobjectType = 'Contact'].id;
    string hide='test';
      Account a = new Account();
       a.name = 'Test';
       insert a; 
  
      Contact c1 = new Contact();
       c1.lastname = 'Cockpit';
       c1.Accountid = a.id;
       c1.Relationship__c ='Accounting';
       c1.Contact_Type__c = 'Cole';
       c1.Wholesaler_Management__c = 'Producer A';
       c1.Territory__c ='Chicago';
       c1.RIA_Territory__c ='Southeast Region';
       c1.RIA_Consultant_Picklist__c ='Brian Mackin';
       c1.Internal_Consultant_Picklist__c ='Ben Satterfield';
       c1.Wholesaler__c='Andrew Garten';
       c1.Regional_Territory_Manager__c ='Andrew Garten';
       c1.Internal_Wholesaler__c ='Keith Severson';
       c1.Territory_Zone__c = 'NV-50';
       c1.Next_Planned_External_Appointment__c = system.now();
       c1.recordtypeid = rrrecordtype;
       c1.mailingstate='OH';
       c1.email='sandeep.mariyala@polarisft.com';
       c1.subscribed__c=true;
       c1.Control_Group__c=true;
       insert c1; 
       Id urlid=c1.id;
       
        System.assertEquals(c1.Accountid,a.id);

       
        pagereference pageref = system.Currentpagereference();
        pageref.getParameters().put('id',c1.id); 
        
      ApexPages.StandardController controller = new ApexPages.StandardController(c1);
      updatecontact sc=new updatecontact(controller);
      sc.updatesubscribed();  
        
        
       }
     }