global with sharing class BatchDeleteShoppingCartItems implements Database.Batchable<sObject>{

global Database.QueryLocator start(Database.BatchableContext BC)
{
    //string contactID = '0035000002JwVTY';
    String query = 'Select Id from Forms_Literature_Shopping_Cart__c';
    return Database.getQueryLocator(query);
}
global void execute(Database.BatchableContext BC, List<Forms_Literature_Shopping_Cart__c> scope)
{      
      system.debug('Testing Database.BatchableContext'+scope);           
      if (Forms_Literature_Shopping_Cart__c.sObjectType.getDescribe().isDeletable()) 
    {          
        delete scope;
    }   

}   
global void finish(Database.BatchableContext BC)
{
}
}