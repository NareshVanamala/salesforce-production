@isTest()
public class InvestorAttachmentcontroller_Test{
     static testmethod void testunit()
    {
        try{
        test.starttest();
        
      
        Account acc = new Account();
        acc.Name = 'Test Prospect Account';      
        Insert acc;

        Contact c = new Contact();
        c.firstname='RSVP';
        c.lastname='test';
        c.accountid=acc.id;
        c.MailingCity='testcity';
        c.MailingState='teststate';
        c.email='test@mail.com';       
        insert c;
        
        
        System.assertNotEquals(acc.Name,c.lastname);

        attachment attobj=new attachment();
        attobj.name='test';
        attobj.body=blob.valueof('str');
       attobj.parentid=c.id;
        insert attobj;
        
        Investor_Attachment__c cAtt = new Investor_Attachment__c();
    cAtt.Contact__c = c.Id;
    cAtt.Type__c = 'Investor List';
    cAtt.Sub_Type__c = 'CSV File';
    cAtt.Name = 'test investor attachment';
    insert cAtt;
        
        attachment attobj1=new attachment();
        attobj1.name='test';
        attobj1.body=blob.valueof('str');
        attobj1.parentid=cAtt.id;
        insert attobj1;
        
              
        ApexPages.currentPage().getParameters().put('id',c.id);
        ApexPages.StandardController stdcon = new ApexPages.StandardController(c);
        InvestorAttachmentController attc = new InvestorAttachmentController(stdcon);
        attc.filename = 'Test File';
        attc.fileBody = Blob.ValueOf('Test Boby');
        attc.CA  =cAtt;
        attc.Contact  = c;
        
        attc.processUpload();
      
        attc.back();
        attc.cancel();
        
        test.stoptest();
         }
        catch(Exception e){}
    } 
}