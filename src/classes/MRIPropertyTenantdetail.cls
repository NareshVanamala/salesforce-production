public with sharing class MRIPropertyTenantdetail{



    public MRIPropertyTenantdetail() {

    }

    set<id> leaseids = new set<id>();
    public map<String,list<Lease_Sales_Data__c>> mapToLeaseSales{get;set;}
    public map<String,list<Financial_Data__c>> mapToFinancial{get;set;}
    public map<string,set<Lease_Sales_Data__c>> grossSalesListMap{get;set;}
    Map<Id,Lease__c> leaseMap = new Map<Id,Lease__c>();
    private list<Lease_Sales_Data__c> grosssales {get;set;}
    private list<Financial_Data__c> Financialssales {get;set;}
    public map<string,set<Financial_Data__c>> financeListMap{get;set;}
    public List<lease__c> mriPropLeaseList{get;set;}
    List<Lease__c> leaseList = new List<Lease__c>();
    public Integer CurrentYear {get;set;} 
    public Integer PreviousYear {get;set;} 
    public Integer twoYearsBack {get;set;} 
    public Integer threeYearsBack {get;set;} 
    public Integer fourYearsBack {get;set;}
    public map<id,set<Property_Contact_Leases__c>> PropConName {get;set;} // Added for Displaying Property contacts details
    public map<id,set<Property_Contact_Leases__c>> PropConName1 {get;set;}
    public MRIPropertyTenantdetail(ApexPages.StandardController controller) {
        CurrentYear = Date.Today().Year();
        PreviousYear = CurrentYear-1;
        twoYearsBack = PreviousYear-1;
        threeYearsBack = twoYearsBack-1;
        fourYearsBack = threeYearsBack-1;
        mapToLeaseSales = new map<String,list<Lease_Sales_Data__c>>();
        mapToFinancial = new  map<String,list<Financial_Data__c>>();
        Financialssales = new list<Financial_Data__c>();
        grosssales = new  list<Lease_Sales_Data__c>();
        PropConName = new map<id,set<Property_Contact_Leases__c>>(); // Added for Displaying Property contacts details
        PropConName1 = new map<id,set<Property_Contact_Leases__c>>(); // Added for Displaying Property contacts details
        String MriPropId = apexpages.currentpage().getparameters().get('id');
        system.debug('mri Property Id'+MriPropId);
         mriPropLeaseList = new List<lease__c> ();
         set<id> leaseSetIds = new Set<id>();
         List<List<Lease__c>> LeaseList = new List<List<Lease__c>>();
         grossSalesListMap = new map<string,set<Lease_Sales_Data__c>>();
         financeListMap = new map<string,set<Financial_Data__c>>();
         MRI_Property__c mripropObj = [Select id,Name,(Select Id,Name,MasterOccupant_ID__c,Store__c,Year_Tab_1__c,Year_Tab_2__c,Year_Tab_3__c,Year_Tab_4__c,Year_Tab_5__c,MRI_PROPERTY__r.Common_Name__c,MRI_PROPERTY__r.Name,SuiteId__c,MRI_PROPERTY__r.Property_Manager__r.Name,MRI_PROPERTY__r.Property_ID__c,Financials_Type__c,Financials_Frequency__c,Sales_Frequency__c ,Tenant_Name__c,Lease_Status__c,MRI_Lease_ID__c,Sales_Report_Required__c,Financials_Required__c  from Lease__r where Lease_Status__c = 'Active' and (Financials_Required__c=true OR Sales_Report_Required__c=true)) from MRI_Property__c where id =:MriPropId];
         if(!mripropObj.Lease__r.isEmpty()){
                    LeaseList.add(mripropObj.Lease__r);
            }
            if(LeaseList !=null && LeaseList.size()>0){
                 for(List<Lease__c> leaselst : LeaseList){
                mriPropLeaseList.addAll(leaselst);
           }
            }
             if(mriPropLeaseList != null && mriPropLeaseList.size()>0){
                for(Lease__c leaseObj :mriPropLeaseList){
                leaseSetIds.add(leaseObj.id);
                }
            }
            if((mriPropLeaseList == null && mriPropLeaseList.size()<0)|| mriPropLeaseList.isEmpty()){
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Gross Sales and Financials Not Required'));
            }
            set<string> leaseMasteroccpIds =  new set<string>();
            if(leaseSetIds!=null && leaseSetIds.size()>0){
                for(Lease__c leaseObj:[Select Id,MasterOccupant_ID__c,Store__c,Lease_Status__c,Lease_ID__c,Sales_Report_Required__c,Financials_Required__c from Lease__c where id in :leaseSetIds]){
                         leaseMap.put(leaseObj.id,leaseObj);
                         leaseMasteroccpIds.add(leaseObj.MasterOccupant_ID__c);
                }
            }
             if(leaseMasteroccpIds !=null && leaseMasteroccpIds.size()>0){
                          grosssales = [Select id,Month__c,lease__r.MasterOccupant_ID__c,lease__r.id,Sales_Collected__c,Sales_Collection__c,Period_Year__c from Lease_Sales_Data__c where Lease__r.MasterOccupant_ID__c in :leaseMasteroccpIds and SalesType__c = 'GROSS' and Period_Year__c >=: fourYearsBack order by Period__c asc];
                          Financialssales = [Select id,Period_Year__c,Year__c,lease__r.MasterOccupant_ID__c,Fins_Collected__c,Lease__r.id from Financial_Data__c where Lease__r.id in :leaseSetIds and Period_Year__c >=: fourYearsBack and Period_Year__c <=: CurrentYear];
                          system.debug('grosssales'+ grosssales );
                          set<id> grossSalesLeaseIds = new set<id>(); // Added for Displaying Property contacts details
                          if(grosssales !=null && grosssales.size()>0){
                          for(Lease_Sales_Data__c leaseSalObj:grosssales){
                                grossSalesLeaseIds.add(leaseSalObj.lease__r.id);// Added for Displaying Property contacts details
                                if(grossSalesListMap.get(leaseSalObj.lease__r.MasterOccupant_ID__c)!=null){
                                    grossSalesListMap.get(leaseSalObj.lease__r.MasterOccupant_ID__c).add(leaseSalObj);
                                  
                                }
                                else{
                                    grossSalesListMap.put(leaseSalObj.lease__r.MasterOccupant_ID__c,new set<Lease_Sales_Data__c>{leaseSalObj});
                                   
                                }
                            }
                        }
                        // Start : Added for Displaying Property contacts details
                        if(grossSalesLeaseIds !=null && grossSalesLeaseIds.size()>0){
                            for(Property_Contact_Leases__c propConLease :[select Lease__c,Property_Contacts__r.Name,id,Lease__r.id,Property_Contacts__r.Email_Address__c,Property_Contacts__r.Phone__c
                                                                            from Property_Contact_Leases__c where Lease__r.id in :grossSalesLeaseIds and Contact_Type__c INCLUDES ('Gross Sales')]){
                            if(PropConName.get(propConLease.Lease__r.id)!=null){
                                    PropConName.get(propConLease.Lease__r.id).add(propConLease);
                                    
                                }
                                else{
                                    PropConName.put(propConLease.Lease__r.id,new set<Property_Contact_Leases__c>{propConLease});
                                  

                                }
                            }
                        } 
                        for(string leaseSetId :leaseSetIds){
                            if(PropConName.get(leaseSetId)==null){
                                PropConName.put(leaseSetId,new set<Property_Contact_Leases__c>{});
                                
                            }
                        }
                        
                        // End : Added for Displaying Property contacts details
                        // Start : Added for Displaying Property contacts details
                        if(grossSalesLeaseIds !=null && grossSalesLeaseIds.size()>0){
                            for(Property_Contact_Leases__c propConLease :[select Lease__c,Property_Contacts__r.Name,id,Lease__r.id,Property_Contacts__r.Email_Address__c,Property_Contacts__r.Phone__c
                                                                            from Property_Contact_Leases__c where Lease__r.id in :grossSalesLeaseIds and Contact_Type__c INCLUDES ('Financials')]){
                            if(PropConName1.get(propConLease.Lease__r.id)!=null){
                                    PropConName1.get(propConLease.Lease__r.id).add(propConLease);
                                   
                                }
                                else{
                                    PropConName1.put(propConLease.Lease__r.id,new set<Property_Contact_Leases__c>{propConLease});
                                    

                                }
                            }
                        } 
                        for(string leaseSetId :leaseSetIds){
                            if(PropConName1.get(leaseSetId)==null){
                                PropConName1.put(leaseSetId,new set<Property_Contact_Leases__c>{});
                                
                            }
                        }
                        
                        // End : Added for Displaying Property contacts details
                         for(string leaseId:leaseMasteroccpIds){
                                if(grossSalesListMap.get(leaseId)==null){
                                    grossSalesListMap.put(leaseId,new set<Lease_Sales_Data__c>{});
                                }
                         }
                    if(Financialssales !=null && Financialssales.size()>0){
                    for(Financial_Data__c financeObj:Financialssales){
                                if(financeListMap.get(financeObj.lease__r.Id)!=null){
                                    financeListMap.get(financeObj.lease__r.Id).add(financeObj);
                                }
                                else{
                                    financeListMap.put(financeObj.lease__r.Id,new set<Financial_Data__c>{financeObj});
                                }
                          }
                    }
                         for(string leaseId:leaseSetIds){
                                if(financeListMap.get(leaseId)==null){
                                    financeListMap.put(leaseId,new set<Financial_Data__c>{});
                                }
                         }}
                    
                    }}