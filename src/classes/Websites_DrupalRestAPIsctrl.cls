public with sharing class Websites_DrupalRestAPIsctrl{
    public String vpr_status{get; set;}
    public String vp_status{get; set;}
    public String ccp_status{get; set;}
    public String ccc_status{get; set;}
    public static final String COMPLETED= 'Completed ';
    public static final String SUCCESSFULLY= 'successfully. ';
    public static final String WITH_FAILURE= 'with failures. ';
    public static final String SUCCESS= 'Success';
    public String batchId{get; set;}

   public Websites_DrupalRestAPIsctrl() {
      vpr_status= vp_status= ccp_status= '';
    }

    
   public void VEREITPressReleases(){
       String VEREITPressReleasesEndPointURL = Websites_HttpClass.DrupalEndPointURLs('VEREIT Press Releases');
       system.debug('VEREITPressReleasesEndPointURL'+VEREITPressReleasesEndPointURL);
       string jsonResponse=Websites_HttpClass.makeCallSync(VEREITPressReleasesEndPointURL,Websites_HttpClass.COLECAPITAL_DRUPAL_TOKEN,Websites_HttpClass.METHOD_POST,Websites_HttpClass.TIMEOUT_MAX,Websites_HttpClass.ACCEPT,'');
       system.debug('jsonResponse'+jsonResponse);

       Response res= parse(jsonResponse);
       vpr_status= COMPLETED + SUCCESSFULLY;

       if(!res.status.equalsIgnoreCase(SUCCESS)){
        vpr_status= COMPLETED + WITH_FAILURE;      
       }
       
       
   }
   public void VEREITProperties(){
       String VEREITPropertiesEndPointURL = Websites_HttpClass.DrupalEndPointURLs('VEREIT Properties');
       system.debug('VEREITPropertiesEndPointURL'+VEREITPropertiesEndPointURL);
       string jsonResponse=Websites_HttpClass.makeCallSync(VEREITPropertiesEndPointURL,Websites_HttpClass.COLECAPITAL_DRUPAL_TOKEN,Websites_HttpClass.METHOD_POST,Websites_HttpClass.TIMEOUT_MAX,Websites_HttpClass.ACCEPT,'');
       system.debug('jsonResponse'+jsonResponse);
       
       Response res= parse(jsonResponse);
       vp_status= COMPLETED + SUCCESSFULLY;

       if(!res.status.equalsIgnoreCase(SUCCESS)){
        vp_status= COMPLETED + WITH_FAILURE;      
       }
   
   }
   public void ColeCapitalProperties(){
       String ColeCapitalPropertiesEndPointURL = Websites_HttpClass.DrupalEndPointURLs('Cole Capital Properties');
       system.debug('ColeCapitalPropertiesEndPointURL'+ColeCapitalPropertiesEndPointURL);
       string jsonResponse=Websites_HttpClass.makeCallSync(ColeCapitalPropertiesEndPointURL,Websites_HttpClass.COLECAPITAL_DRUPAL_TOKEN,Websites_HttpClass.METHOD_POST,Websites_HttpClass.TIMEOUT_MAX,Websites_HttpClass.ACCEPT,'');
       system.debug('jsonResponse'+jsonResponse);
       
       Response res= parse(jsonResponse);
       ccp_status= COMPLETED + SUCCESSFULLY;

       if(!res.status.equalsIgnoreCase(SUCCESS)){
        ccp_status= COMPLETED + WITH_FAILURE;      
       }
   
   }

   public void ColeCapitalContacts(){
       Websites_CCDrupalContactJob job= new Websites_CCDrupalContactJob();

       Integer batchSize=Test.isRunningTest() ? 1 : 200;
       if(!Test.isRunningTest())
       batchId= database.executeBatch(job, batchSize);
       ccc_status= 'Processing Job, JobId: '+'<a href="/apexpages/setup/listAsyncApexJobs.apexp">'+batchId+'</a>';
   }

   public class Response {
       public String status;
  }

  
   public static Response parse(String json) {
       return (Response) System.JSON.deserialize(json, Response.class);
  }
}