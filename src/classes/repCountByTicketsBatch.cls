global class repCountByTicketsBatch implements Database.Batchable<SObject>
{
       
        global integer  ticket1to3col1=0;  
        global  integer ticket4to24col1=0;
        global  integer ticket25morecol1=0;
        global  integer ticket1to3col2=0; 
        global  integer ticket4to24col2 =0;
        global  integer ticket25morecol2 =0;
        global  integer ticket1to3col3 =0;
        global  integer ticket4to24col3 =0;
        global  integer ticket25morecol3 =0;
        
        
    global Database.QueryLocator start(Database.BatchableContext BC)  
    {
                     
        String Query;
        system.debug('This is start method');  
        Id recdBrokerDealer=[Select id,name,DeveloperName from RecordType where DeveloperName='BrokerDealerAccount' and SobjectType = 'Account'].id; 
         // Id  recdBrokerDealer='0013000000BlAy2';
        Query = 'SELECT Id,accountid, Total_Cole_Tickets_Only_Funds_in_SF__c,Last_Producer_Date__c FROM Contact WHERE account.recordtypeid=:recdBrokerDealer';
        system.debug('Myquery is......' + Query);
        return Database.getQueryLocator(Query);
        
    }
    global void execute(Database.BatchableContext BC, List<Contact> scope)  
    {
    
         
        Map<Id, List<Contact>> accounContacts = new Map<Id, List<Contact>>();
        List<National_Account_Helper__c> nahListExisting = new List<National_Account_Helper__c>();
        List<National_Account_Helper__c> UpdatednahListExisting = new List<National_Account_Helper__c>();
        Map<Id, National_Account_Helper__c> accountNAHMap = new Map<Id, National_Account_Helper__c>();
        Date d1 = Date.today() - 30;
        Date d2 = Date.today() - 60;
        Date d3 = Date.today() - 180; 
        nahListExisting = [select Account__c,X30To60days1to3Ticlets__c,X30To60days25moreTickets__c,X30To60days4to24Tickets__c,X60To180days1to3Tickets__c,X60to180days25moreTickets__c,X60to180days4to24Tickets__c,X180Plusdays1to3Tickets__c,X180Plusdays25moreTickets__c,X180Plusdays4to24Tickets__c FROM National_Account_Helper__c ]; 
        System.debug('The list of Nh list..'+nahListExisting.size());
        for(Contact  rct: scope)
        {
          if((rct.Last_Producer_Date__c > d2) && (rct.Last_Producer_Date__c<d1))
          {
              if((rct.Total_Cole_Tickets_Only_Funds_in_SF__c>=1)&&(rct.Total_Cole_Tickets_Only_Funds_in_SF__c<=3)) 
                ticket1to3col1= ticket1to3col1+1;
              else if((rct.Total_Cole_Tickets_Only_Funds_in_SF__c >=4)&&(rct.Total_Cole_Tickets_Only_Funds_in_SF__c <=24)) 
                ticket4to24col1 = ticket4to24col1 + 1;
              else if(rct.Total_Cole_Tickets_Only_Funds_in_SF__c >=25)
                ticket25morecol1 = ticket25morecol1 + 1;
           }
            if((rct.Last_Producer_Date__c > d3)&&(rct.Last_Producer_Date__c<d2))
            {
               if((rct.Total_Cole_Tickets_Only_Funds_in_SF__c>=1)&&(rct.Total_Cole_Tickets_Only_Funds_in_SF__c<=3)) 
                ticket1to3col2= ticket1to3col2+1;
               else if((rct.Total_Cole_Tickets_Only_Funds_in_SF__c >=4)&&(rct.Total_Cole_Tickets_Only_Funds_in_SF__c <=24)) 
                 ticket4to24col2 = ticket4to24col2 + 1;
               else if(rct.Total_Cole_Tickets_Only_Funds_in_SF__c >=25)
                ticket25morecol2 = ticket25morecol2 + 1;
             }
             if(rct.Last_Producer_Date__c <d3)
            {
               if((rct.Total_Cole_Tickets_Only_Funds_in_SF__c>=1)&&(rct.Total_Cole_Tickets_Only_Funds_in_SF__c<=3)) 
                ticket1to3col3= ticket1to3col3+1;
               else if((rct.Total_Cole_Tickets_Only_Funds_in_SF__c >=4)&&(rct.Total_Cole_Tickets_Only_Funds_in_SF__c <=24)) 
                 ticket4to24col3 = ticket4to24col3 + 1;
               else if(rct.Total_Cole_Tickets_Only_Funds_in_SF__c >=25)
                ticket25morecol3 = ticket25morecol3 + 1;
             }
        
        
        
        
          }
          for(National_Account_Helper__c nh:nahListExisting )
             {     
                   nh.X30To60days1to3Ticlets__c += ticket1to3col1;
                   nh.X30To60days4to24Tickets__c += ticket4to24col1;
                   nh.X30To60days25moreTickets__c += ticket25morecol1;
                  
                   nh.X60To180days1to3Tickets__c += ticket1to3col2;  
                   nh.X60to180days4to24Tickets__c += ticket4to24col2;
                   nh.X60to180days25moreTickets__c += ticket25morecol2; 
                   
                   nh.X180Plusdays1to3Tickets__c += ticket1to3col3;  
                   nh.X180Plusdays4to24Tickets__c += ticket4to24col3;
                   nh.X180Plusdays25moreTickets__c += ticket25morecol3; 
                   UpdatednahListExisting.add(nh); 
             
             }
               update   UpdatednahListExisting;  


          
         system.debug('the velue of '+ticket1to3col2+ticket4to24col1+ticket25morecol1);  
           
         }
     global void finish(Database.BatchableContext BC)  
    {
       /*AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'skalamkar@colecapital.com',ninad.tambe@colereit.com };
        String[] bccAddresses = new String[] {'ntambe@colecapital.com','ravi.bhogadi@colereit.com'};
        String subject ='Rep Count By Tickets Batch Job completed';
        email.setSubject(subject);
        email.setBccSender(true);
        email.setToAddresses( toAddresses );
        email.setBccAddresses(bccAddresses); 
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});  */    
       
    }
    
  }