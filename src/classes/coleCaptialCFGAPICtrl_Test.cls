@isTest(SeeAllData=true)
  private class coleCaptialCFGAPICtrl_Test{
    static testmethod void CaptialTestMethod() {
    try{
     list<contact> conList = new list<contact> ();
     conList = [select id,Accountid,mailingstate,RecordType.Name,Name from contact limit 1];
     String contactId = conList[0].id;
     Forms_Literature_List__c literatureList = new Forms_Literature_List__c();
     literatureList.Contact__c = contactId;
     literatureList.List_Name__c ='Testing ListName';
     insert literatureList;
     list<Forms_Literature_Bookmarks__c> bookMarkList = new list<Forms_Literature_Bookmarks__c>();
     Forms_Literature_Bookmarks__c  bookmarkObj = new Forms_Literature_Bookmarks__c();
     bookmarkObj.Title__c = 'title';                   
     bookmarkObj.Description__c = 'Description';
     bookmarkObj.Contact__c = contactId;
     bookmarkObj.Download_File_URL__c = 'Download File';                   
     bookmarkObj.Max_Order__c = 20;
     bookmarkObj.Published_Date__c = system.Today();                     
     bookmarkObj.Expiration_Date__c = '06/01/2016';
     bookmarkObj.FINRA_PDF__c = 'Finra PDF';
     bookmarkObj.Forms_Literature_List__c = literatureList.id;
     bookmarkObj.External_Id__c = contactId + bookmarkObj.Forms_Literature_List__c;
     bookMarkList.add(bookmarkObj);   
     insert bookMarkList;
     
     Forms_Literature_Shopping_Cart__c  cartObj = new Forms_Literature_Shopping_Cart__c();
     cartObj.Title__c = 'title';                   
     cartObj.Description__c = 'Description';
     cartObj.Contact__c = contactId;
     cartObj.Download_File_URL__c = 'Download File';                   
     cartObj.Max_Order__c = 20;
     cartObj.Published_Date__c = '06/01/2016';                     
     cartObj.Expiration_Date__c = '06/01/2016';
     cartObj.FINRA_PDF__c = 'Finra PDF';
     //cartObj.Forms_Literature_List__c = literatureList.id;
     cartObj.External_Id__c = contactId;   
     insert cartObj;
     
     System.assertNotEquals(bookmarkObj.Title__c,cartObj.Description__c);

     coleCaptialCFGAPICtrl coleCtrl = new coleCaptialCFGAPICtrl();
     coleCtrl.contactId =  conList[0].id;
     coleCtrl.accountId =  conList[0].Accountid;
     coleCtrl.mailingState =  conList[0].mailingState;
     coleCtrl.usergroup =  conList[0].RecordType.Name.replaceAll(' ','%20'); 
     coleCtrl.createList('Testing');
     coleCtrl.renameLiteratureList(literatureList.id,'Testing');
     coleCtrl.addToMultipleBookmark('Testing','CCPT V INVESTOR PRESENTATION*--*APPROVED FOR GENERAL PUBLIC. NOT FOR USE IN MA or OH.*--*https://mp-advisor.dstweb.com/library/showfile?productID=C9F1E0B3B55E2F04C5741029918EFDDD&attachment=1*--*0*--*PT-CCPT5-INV-05*--*https://mp-advisor.dstweb.com//includes/images/altProdImage.gif*--*5/23/2016*--*12/31/1969*--**--*https://mp-advisor.dstweb.com/library/showfile?sku=B8A6EC9AA107724C87AB2A19790BE9A1&attachment=0*--**--;--*CCPT V SUPPLEMENT NO. 2 - 5.20.16*--*Dated 5.20.16*--*https://mp-advisor.dstweb.com/library/showfile?productID=DEEA2992A01CBEB0E28A85D3913D0F81&attachment=1*--*30*--*CCPT5-SUP-2D*--*https://mp-advisor.dstweb.com/inventory_images/cole_th_CCPT5-SUP-2D.jpg*--*5/23/2016*--*12/31/1969*--**--*testing*--*testing*--*');
     coleCtrl.addToMultipleBookmark('Testing ListName','CCPT V INVESTOR PRESENTATION*--*APPROVED FOR GENERAL PUBLIC. NOT FOR USE IN MA or OH.*--*https://mp-advisor.dstweb.com/library/showfile?productID=C9F1E0B3B55E2F04C5741029918EFDDD&attachment=1*--*0*--*PT-CCPT5-INV-05*--*https://mp-advisor.dstweb.com//includes/images/altProdImage.gif*--*5/23/2016*--*12/31/1969*--**--*https://mp-advisor.dstweb.com/library/showfile?sku=B8A6EC9AA107724C87AB2A19790BE9A1&attachment=0*--**--;--*CCPT V SUPPLEMENT NO. 2 - 5.20.16*--*Dated 5.20.16*--*https://mp-advisor.dstweb.com/library/showfile?productID=DEEA2992A01CBEB0E28A85D3913D0F81&attachment=1*--*30*--*CCPT5-SUP-2D*--*https://mp-advisor.dstweb.com/inventory_images/cole_th_CCPT5-SUP-2D.jpg*--*5/23/2016*--*12/31/1969*--**--*testing*--*testing*--*');
     coleCtrl.addToMultipleBookmark('Default','CCPT V INVESTOR PRESENTATION*--*APPROVED FOR GENERAL PUBLIC. NOT FOR USE IN MA or OH.*--*https://mp-advisor.dstweb.com/library/showfile?productID=C9F1E0B3B55E2F04C5741029918EFDDD&attachment=1*--*0*--*PT-CCPT5-INV-05*--*https://mp-advisor.dstweb.com//includes/images/altProdImage.gif*--*5/23/2016*--*12/31/1969*--**--*https://mp-advisor.dstweb.com/library/showfile?sku=B8A6EC9AA107724C87AB2A19790BE9A1&attachment=0*--**--;--*CCPT V SUPPLEMENT NO. 2 - 5.20.16*--*Dated 5.20.16*--*https://mp-advisor.dstweb.com/library/showfile?productID=DEEA2992A01CBEB0E28A85D3913D0F81&attachment=1*--*30*--*CCPT5-SUP-2D*--*https://mp-advisor.dstweb.com/inventory_images/cole_th_CCPT5-SUP-2D.jpg*--*5/23/2016*--*12/31/1969*--**--*testing*--*testing*--*');
     coleCtrl.addToBookmark('Naresh 531','CCPT V INVESTOR PRESENTATION','APPROVED FOR GENERAL PUBLIC. NOT FOR USE IN MA or OH.','https://mp-advisor.dstweb.com/library/showfile?productID=C9F1E0B3B55E2F04C5741029918EFDDD&attachment=1',0,'PT-CCPT5-INV-05','https://mp-advisor.dstweb.com//includes/images/altProdImage.gif','5/23/2016','12/31/1969','*---*https://mp-advisor.dstweb.com/library/showfile?sku=B8A6EC9AA107724C87AB2A19790BE9A1&attachment=0',false);
     coleCtrl.addToBookmark('Testing ListName','CCPT V INVESTOR PRESENTATION','APPROVED FOR GENERAL PUBLIC. NOT FOR USE IN MA or OH.','https://mp-advisor.dstweb.com/library/showfile?productID=C9F1E0B3B55E2F04C5741029918EFDDD&attachment=1',0,'PT-CCPT5-INV-05','https://mp-advisor.dstweb.com//includes/images/altProdImage.gif','5/23/2016','12/31/1969','*---*https://mp-advisor.dstweb.com/library/showfile?sku=B8A6EC9AA107724C87AB2A19790BE9A1&attachment=0',false);
     coleCtrl.addToBookmark('Default','CCPT V INVESTOR PRESENTATION','APPROVED FOR GENERAL PUBLIC. NOT FOR USE IN MA or OH.','https://mp-advisor.dstweb.com/library/showfile?productID=C9F1E0B3B55E2F04C5741029918EFDDD&attachment=1',0,'PT-CCPT5-INV-05','https://mp-advisor.dstweb.com//includes/images/altProdImage.gif','5/23/2016','12/31/1969','*---*https://mp-advisor.dstweb.com/library/showfile?sku=B8A6EC9AA107724C87AB2A19790BE9A1&attachment=0',false);
     coleCtrl.getBookmarkList();
     coleCtrl.getBookmarks(literatureList.id,'title');
     coleCtrl.getBookmarks('All','title1');
     coleCtrl.getBookmarkListsWithSKU('CCPT5-123');
     coleCtrl.getBookmarkListsWithOutSKU('CCPT5-123');
     coleCtrl.removeItemFromCart(cartObj.id);
     coleCtrl.removeItemFromCart('All');
     coleCtrl.removeItemFromBookmarks(bookMarkList[0].id);
     coleCtrl.removeLiteratureList(literatureList.id);
     coleCtrl.getCartList();
     coleCtrl.updateItemInCart(cartObj.id,'250');
     coleCtrl.getContactData();
     coleCtrl.saveCart('CCPT V PORTFOLIO OVERVIEW 4Q.15','INVESTOR APPROVED. NO STATE RESTRICTIONS.' ,'https://mp-advisor.dstweb.com/library/showfile?productID=E8915BD9FDD69A95263511D8BD0FC1BE&attachment=1',50,5,'CCPT5-PORTOVR-7','https://mp-advisor.dstweb.com/inventory_images/cole_th_CCPT5-PORTOVR-7.jpg','5/11/2016','12/31/1969','*---*https://mp-advisor.dstweb.com/library/showfile?sku=291C84C3E1CA3D9550EF8765017EF1A3&attachment=0',false);
     coleCtrl.updateAllItemsInCart(cartObj.id+',5,50');
     coleCtrl.updateAllItemsInCart(cartObj.id+',0,50');
     //coleCtrl.coleCaptialCFGAPI('pagename');
     //coleCtrl.submitOrder('jsonOrder');
     coleCtrl.sendEmail('sampe@vereit.com','Notes','CCPT V INVESTOR PRESENTATION$$&&https://mp-advisor.dstweb.com/library/showfile?productID=C9F1E0B3B55E2F04C5741029918EFDDD&attachment=1&&$$CCPT V SUPPLEMENT NO. 2 - 5.20.16$$&&https://mp-advisor.dstweb.com/library/showfile?productID=DEEA2992A01CBEB0E28A85D3913D0F81&attachment=1&&$$');    
     coleCtrl.coleCaptialCFGAPI('testing');
     coleCtrl.submitOrder('testing');
     }
     catch(Exception e){
     }
    }
   
}