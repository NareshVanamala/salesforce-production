@isTest

public class TestOps_Advisor_Wholesaler_Reporting{

static testmethod void TestOps_Advisor_Reporting()
{

  test.startTest();
  
      List<String> tostr = new list<String>();
      tostr.add('sometoaddress@email.com');
      tostr.add('sometoaddress1@email.com');
      List<String> ccstr = new list<String>();
      ccstr.add('someccaddress@email.com');
      ccstr.add('someccaddress1@email.com');
      ccstr.add('someccaddress2@email.com');
     
      Messaging.InboundEmail email = new Messaging.InboundEmail() ;
      Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    
      // setup the data for the email

      email.subject = 'Emial Subject';
      email.fromAddress = 'somefromaddress@email.com';
      email.toAddresses = tostr;
      email.ccAddresses = ccstr;
      email.plainTextBody = 'email body\n2225256325\nTitle';

      // add an Binary attachment

      Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();

      attachment.body = blob.valueOf('my attachment text');

      attachment.fileName = 'textfileone.txt';

      attachment.mimeTypeSubType = 'text/plain';

      email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };

      Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();

      attachmenttext.body = 'my attachment text';

      attachmenttext.fileName = 'textfiletwo3.txt';

      attachmenttext.mimeTypeSubType = 'texttwo/plain';

      email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };

      System.assertEquals(attachmenttext.body,'my attachment text');


     // call the email service class and test it with the data in the testMethod

      Ops_Advisor_Reporting testInbound = new Ops_Advisor_Reporting();
    
      testInbound.handleInboundEmail(email, env);
        
   test.stoptest();

 }
 
 static testmethod void TestOps_Wholesaler_Reporting()
{

  test.startTest();
  
      List<String> tostr = new list<String>();
      tostr.add('sometoaddress@email.com');
      tostr.add('sometoaddress1@email.com');
      List<String> ccstr = new list<String>();
      ccstr.add('someccaddress@email.com');
      ccstr.add('someccaddress1@email.com');
      ccstr.add('someccaddress2@email.com');
     
      Messaging.InboundEmail email = new Messaging.InboundEmail() ;
      Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    
      // setup the data for the email

      email.subject = 'Emial Subject';
      email.fromAddress = 'somefromaddress@email.com';
      email.toAddresses = tostr;
      email.ccAddresses = ccstr;
      email.plainTextBody = 'email body\n2225256325\nTitle';

      // add an Binary attachment

      Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();

      attachment.body = blob.valueOf('my attachment text');

      attachment.fileName = 'textfileone.txt';

      attachment.mimeTypeSubType = 'text/plain';

      email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };

      Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();

      attachmenttext.body = 'my attachment text';

      attachmenttext.fileName = 'textfiletwo3.txt';

      attachmenttext.mimeTypeSubType = 'texttwo/plain';

      email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };

       
          System.assertEquals(attachmenttext.body,'my attachment text');

     // call the email service class and test it with the data in the testMethod

      Ops_Wholesaler_Reporting testInbound = new Ops_Wholesaler_Reporting();
    
      testInbound.handleInboundEmail(email, env);
        
   test.stoptest();

 }
}