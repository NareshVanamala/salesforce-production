global without sharing class ocms_ARSelfRegistration extends cms.ContentTemplateController implements cms.ServiceInterface  {

    //Variables 
    private static String COMMUNITY_PROFILE_NAME = 'Cole Capital Community';

    global ocms_ARSelfRegistration() { }

    // constructor for content editor
    global ocms_ARSelfRegistration(cms.CreateContentController cc) {
        super(cc);
    }

    // constructor for generator
    global ocms_ARSelfRegistration(cms.GenerateContent gc) {
        super(gc);
    }

    /**
     * @description required by cms.ServiceInterface
     * @return System.Type
     */
    public System.Type getType() {
        return ocms_ARSelfRegistration.class;
    }

    /**
     * @description service endpoint required by cms.ServiceInterface
     * @param params - parameters describe the request and any additional options
     *          params.action - string name identifying the specific action being requested
     * @return String – JSON response
     */
    global String executeRequest(Map<String, String> params) {
        String response = '{"success": false}';
        String action = params.get('action');

        if (action == 'registerUser') {
            response = registerPortalUser(params.get('firstname'),
                                          params.get('lastname'),
                                          params.get('email'),
                                          params.get('phone'),
                                          params.get('account')
                                         );
        } 
        else if (action == 'validateAccount') {
            response = validateAccount(params.get('accountName'));
        }

        return response;
    }

    /**
    * @description content generator for the American Realty 
    *               self registration content
    **/ 
    global override String getHTML() {
        String html ='<div class="serverError"></div>' +
        '<div class="serverSuccess"></div>' +
        '<table>' +
                '<tr>' +
                    '<td>First Name</td>' +
                    '<td><input class="formtext" type="text" name="firstname" id="firstname" /></td>' +
                '</tr>' +
                '<tr>' +
                    '<td>Last Name</td>' +
                    '<td><input class="formtext" type="text" name="lastname" id="lastname" /></td>' +
                '</div>' +
                '<tr>' +
                    '<td>Email</td>' +
                    '<td><input class="formtext" type="text" name="email" id="email" /></td>' +
                '</tr>' +
                '<tr>' +
                    '<td>Phone</td>' +
                    '<td><input class="formtext" type="text" name="phone" id="phone" /></td>' +
                '</tr>' +
                '<tr>' +
                    '<td><input class="ocmsLoginButton" type="button" value="Register" name="registerbtn" onclick="regUser();"/></td>' +
                    '<td></td>' +
                '</tr>'+    
        '</table>' +
        '<script>' +
            'function regUser() {' +
                '$(\'.serverError\').text(\'\');'+
                'var firstName = $(\'input[id=firstname]\').val();' +
                'var lastName = $(\'input[id=lastname]\').val();'+
                'var email = $(\'input[id=email]\').val();'+
                'var phone = $(\'input[id=phone]\').val();'+
                'var account = ' + '\'' + accountName + '\'' +';' +

                'var serviceParams = {action: \'registerUser\', '+ 
                                     'firstname: firstName, '+
                                     'lastname: lastName, '+
                                     'email: email, '+
                                     'phone: phone, '+
                                     'account: account '+
                                    '};'+

                '$.orchestracmsRestProxy.doAjaxServiceRequest('+
                   '\'ocms_ARSelfRegistration\','+
                    'serviceParams,'+
                    'function (testStatus, json, xhr) {'+
                        'if (json.success) {';
                        if (sPage != '') {
                           html += 'window.location.href = \'' + sPage + '\';';
                        } else {
                            html += '$(\'.serverError\').text(\'You have successfully created a new account. Please check your email for information.\');';
                        }
                        html += '} else {'+
                            '$(\'.serverError\').text(json.message);'+
                        '}'+
                    '}'+
                '); '+
            '}' + 
        '</script>';
        return html;
    }

    /**
     * @description registers a portal user and sets the provided information.
     * @param {String} firstname    required
     * @param {String} lastname     required
     * @param {String} email        required, is also validated via pattern
     * @param {String} phone        required, is also validated via pattern
     * @return {String}  json response
     */
    private String registerPortalUser(String firstname, String lastname, String email, String phone, String account) {

        String response = '';
        //system.debug('con123456'+email);

        Pattern emailPattern;
        Matcher emailMatcher;
        if (!String.isEmpty(email)) {
            emailPattern = Pattern.compile('^[_A-Za-z0-9-\']+(\\.[_A-Za-z0-9-\']+)*@[A-Za-z0-9][_A-Za-z0-9-\']*(\\.[_A-Za-z0-9-\']+)*(\\.[A-Za-z]{2,})$');
            emailMatcher = emailPattern.matcher(email);
        }

        String formattedPhone = phone.replaceAll('[^0-9]', '');
        List<User> users;
        List<Contact> con = [SELECT Id,AccountId  FROM Contact WHERE Email = :email LIMIT 1];
        String accountId;
        //system.debug('con123'+con);

        if (!con.isEmpty()) {
            users = [SELECT Id FROM User WHERE Username = :email];
            accountId = (String) con[0].get('AccountId');
        }

        Profile communityProfile = [SELECT Id FROM Profile WHERE Name = :COMMUNITY_PROFILE_NAME LIMIT 1];  

        // validate that values are not empty
        if (String.isEmpty(firstname)) {
            response = '{"success": false, "message":"First Name is required."}';
        } else if (String.isEmpty(lastname)) {
            response = '{"success": false, "message":"Last Name is required."}';
        } else if (String.isEmpty(email) || !emailMatcher.matches()) {
        //} else if (String.isEmpty(email)) {
            response = '{"success": false, "message":"A valid email is required."}';
        } else if (String.isEmpty(phone) || formattedPhone.length()!=10) {
            response = '{"success": false, "message":"A valid phone number is required."}';
        } else if (con.isEmpty()) {
            response = '{"success": false, "message":"Must be a client before you can register."}';
        } else if (!users.isEmpty()) {
            response = '{"success":false, "message":"That email address already exists. Please login or reset your password."}';
        } else if (communityProfile == null) {
            response = '{"success":false, "message":"Error creating user, Please contact Administrator."}';
        } else {
            //system.debug('email nickName'+email);
            String userEmail = email;
            String userNickname ='';
            //system.debug('userEmail'+userEmail);
            if(userEmail.length() > 39){
            userNickname = userEmail.subString(0,39);
            }
            else{
                userNickname=userEmail;
            }
            system.debug('userNickname'+userNickname);
            // create new user with provided values
            User u = new User(
                Username = email,
                Email = email,
                FirstName = firstname,
                LastName = lastname,
                CommunityNickname = userNickname,
                Phone = formattedPhone,
                ContactId = con[0].Id,
                ProfileId = communityProfile.Id
            );
            system.debug('user Created'+u);

        //    List<Account> accountList = [SELECT Id FROM Account WHERE Name = :con[0].AccountId LIMIT 1];
    
            if (accountId  != null) {
             //   accountId = (String) accountList[0].get('Id');

                try {
                    String userId = Site.createPortalUser(u, accountId, null);
                    System.debug('New user ID = ' + userId);
                    if (userId == null) {
                        for(ApexPages.Message m: ApexPages.getMessages()) {
                            System.debug('•••••••••• ' + m);
                        }
                        response = '{"success":false, "message":"Error creating user, Please contact site Administrator."}';
                    } 
                    else {
                       response = '{"success":true, "message":"You will receive your password via email."}';
                    }
                } 
                catch (Exception e) {
                    response = '{"success":false, "message":"Error creating user, Please contact site Administrator."}';
                }
            } else {
                // account query returned 0 or 2+
                response = '{"success":false, "message":"There was an error creating your user. Account not found."}';
            }

        }

        return response;
    }

    /**
    * @description This will validate the account that is entered in the 
    *               the edit page
    * @return String - response true or false  
    **/
    private String validateAccount(String accountName) {
        String response = '';
        Account acc = [SELECT Id, Name FROM Account WHERE Id = :accountName LIMIT 1];
        if (acc != null) {
            response = '{"success":true, "message":"Valid Account."}';
        } else {
            response = '{"success":false, "message":"Invalid Account."}';
        }

        return response;
    }

    /**
    * @description Methods to get the success page string from content layout instance
    **/
    public String successPageString {
        get {
            return this.getProperty('successPage');
        }
        set;
    }

    public cms.Link successPageCMSLink {
        get { 
            if (this.successPageCMSLink == null) {
                if (this.successPageString != null) {
                    this.successPageCMSLink = new cms.Link(this.successPageString);
                }
            }
            return successPageCMSLink;
        }
        set;
    }

    public String sPage {
        get{
            String sp = '';

            if (sPage == null) {
                cms.Link link = this.successPageCMSLink;
                sp = link.targetPage;
                sp = sp.replace('#tab', '%23tab');    
            }            

            return sp;
        }
        set;
    }

    /**
    * @description This will get the account name from  the content layout instance. 
    **/ 
    public String accountName {
        get {
            return this.getProperty('accountName');
        }
        set;
    }


}