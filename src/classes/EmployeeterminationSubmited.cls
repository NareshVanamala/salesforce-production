public with sharing class EmployeeterminationSubmited {

    public PageReference home() {
        
        PageReference NewHireFormPage = new PageReference ('/apex/employeeuserchangeform');
        NewHireFormPage .setRedirect(true);
        return NewHireFormPage;
        
    }

}