global class Approve_Through_Email implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        system.debug('Email Service Execution started');
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        DataBase.executeBatch(new Approval_Job(email.plainTextBody),20);
        return result;
    }
}