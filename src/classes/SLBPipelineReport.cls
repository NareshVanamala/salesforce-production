public class SLBPipelineReport
{
   /*
    public PageReference exportToExcel()
    {
        return page.SLBPipelinetoexcel;
    }
    public List<DealCountWrappercls> PortfolioListCls {set;get;}
    Public SLBPipelineReport()
    {
       PortfolioListCls= new list<DealCountWrappercls>(); 
    }
    public void getDeals()
    {
        Map<Id, REthinkAP__Portfolio__c > portfoliomap = new Map<Id, REthinkAP__Portfolio__c >([select Id,Name from REthinkAP__Portfolio__c]);
        List<AggregateResult>ttldealsPortfolio=[select COUNT(id)cnt,SUM(REthinkAP__Total_SF__c)SF,AVG(REthinkAP__Total_SF__c)SFproperty,REthinkAP__Portfolio_Deal__c,SUM(REthinkAP__Total_Price__c)Pr,AVG(REthinkAP__Total_Price__c)pricePerproperty,REthinkAP__Deal_Status__c,REthinkAP__Deal_Type__c,MAX(REthinkAP__Date_Identified__c)dateidentfied,REthinkAP__Source__c,AVG(REthinkAP__CAP_Rate__c)caprate,AVG(REthinkAP__Rent_PSF__c)rentpersf from REthinkAP__Deal__c where(REthinkAP__Portfolio_Deal__c!=null and REthinkAP__Deal_Type__c='Leaseback') group by REthinkAP__Portfolio_Deal__c,REthinkAP__Deal_Status__c,REthinkAP__Deal_Type__c,REthinkAP__Source__c order by REthinkAP__Portfolio_Deal__c];
        system.debug('get the grouped result......'+ttldealsPortfolio);
        Integer cnt1=0;
        Integer totaldeals=0;
        Decimal totalSF=0;
        Decimal totalPrice=0;
        String Chkstatus=null;
        String dealtype=null;
        String mypropertytype=null;
        date dateidentified=null;
        String source=null;
        decimal caprate=0;
        decimal myrentperSF=0;
        Decimal term;
        String rentbump=null;
        decimal mysfperproperty=0;
        decimal myPriceperproperty=0;
        String dealid;
        integer sizeoflist=0;
        for (AggregateResult ar : ttldealsPortfolio) 
        {
            term=0;
            cnt1 = (Integer)ar.get('cnt');  
            String Tenantid=(String)ar.get('REthinkAP__Portfolio_Deal__c');
            REthinkAP__Deal__c mydeal=[select id,REthinkAP__Property_Type__c,REthinkAP__Portfolio_Deal__c from REthinkAP__Deal__c where(REthinkAP__Property_Type__c!=null and REthinkAP__Deal_Type__c='Leaseback' and REthinkAP__Portfolio_Deal__c=:Tenantid)limit 1];    
            system.debug('get the propertytype....'+mydeal.REthinkAP__Property_Type__c);
            List<REthinkAP__Rent_Roll__c>ttlrentrolls=[select id,REthinkAP__Deal__r.REthinkAP__Portfolio_Deal__r.Id,REthinkAP__Remaining_Term__c,REthinkAP__Deal__c,REthinkAP__Rent_Bump__c from REthinkAP__Rent_Roll__c where REthinkAP__Deal__r.REthinkAP__Portfolio_Deal__r.Id=:Tenantid];
            system.debug('Check the list size+++'+ttlrentrolls.size());
            system.debug('Check the list size Tenantid +++'+Tenantid);
            if(ttlrentrolls.size()>0)
            {
                sizeoflist= ttlrentrolls.size();
                for(REthinkAP__Rent_Roll__c rtrl:ttlrentrolls)
                {
                    if(rtrl.REthinkAP__Remaining_Term__c!= null)
                    {
                    term=term+(rtrl.REthinkAP__Remaining_Term__c);
                    }
                    if(rtrl.REthinkAP__Rent_Bump__c!= null)
                    {
                    rentbump=(rtrl.REthinkAP__Rent_Bump__c);
                    }
                 }
            
                 term= term/sizeoflist;
            } 
            system.debug('Check the term value+++'+term);
            String tenantName=null;
            if(Tenantid!=null)
            tenantName= portfoliomap.get(Tenantid).name; 
            system.debug('Check the list size Tenantid1 +++'+portfoliomap.get(Tenantid));   
            totalSF =(decimal)ar.get('SF');
            totalPrice=(Decimal )ar.get('pr');
            //decimal sfperproperty=(decimal)ar.get('SFperproperty');
            Chkstatus=(String)ar.get('REthinkAP__Deal_Status__c');
            dealtype=(String)ar.get('REthinkAP__Deal_Type__c');
            mypropertytype= mydeal.REthinkAP__Property_Type__c;
            dateidentified=(Date)ar.get('dateidentfied');
            source=(String)ar.get('REthinkAP__Source__c');
            caprate=(Decimal)ar.get('caprate');
            myrentperSF=(Decimal)ar.get('rentpersf');
            //term=(decimal)ar.get('rentroll');
            //rentbump=(String)ar.get('Rent_Bump__c ');
            mysfperproperty=(decimal)ar.get('SFproperty');
            myPriceperproperty=(decimal)ar.get('pricePerproperty');
            
           DealCountWrappercls deallistcls = new DealCountWrappercls(tenantName,Chkstatus,dealtype,mypropertytype,dateidentified,source,cnt1,totalSF,mysfperproperty,totalPrice,myPriceperproperty,caprate,myrentperSF,term,rentbump);
           system.debug('Check the value....'+deallistcls);
           PortfolioListCls.add(deallistcls ); 
           system.debug('Check the value for PortfolioListCls....'+PortfolioListCls);
       }
         
 }    
    public class DealCountWrappercls 
    {
    
        public DealCountWrappercls(String Portfolio,String prnStatus,String type,string ChkpropType,date identifieddate,String source,Integer count,Decimal totalSF,decimal Chksfperproperty,Decimal totalPrice,decimal chkproceperproperty,Decimal CAPrate,Decimal ChkRentPERSF,decimal TermRentRoll,String ChkRentbump)
        {        
            Portfl = Portfolio;
            count1  = count;
            totalsft= totalSF;
            avgprice= totalPrice;
            FNSFperproperty=Chksfperproperty;
            MYstatus=prnStatus;
            dealtype= type;
            FnProptype= ChkpropType; 
            DtIdentified=identifieddate;
            source1= source;
            Caprate1= CAPrate;
            Fnrentpersf= ChkRentPERSF;
            MyRentBump= ChkRentbump;
            Term1=TermRentRoll;
            FnPriceperproperty=chkproceperproperty;
        }
        public String Portfl{set;get;}
        public Integer count1 {set;get;} 
        public decimal totalsft{set;get;}
        public decimal avgprice{set;get;}
        public decimal FNSFperproperty{set;get;} 
        public string MYstatus{set;get;}
        public string dealtype{set;get;} 
        public string FnProptype{set;get;} 
        public date DtIdentified{set;get;} 
        public string source1{set;get;} 
        public decimal caprate1{set;get;}
        public decimal Fnrentpersf{set;get;}
        public string MyRentBump{set;get;}
        public Decimal Term1{set;get;}
        public Decimal FnPriceperproperty{set;get;}
     
     }
      */  
 }