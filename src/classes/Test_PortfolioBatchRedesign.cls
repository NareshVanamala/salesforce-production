@isTest
public class Test_PortfolioBatchRedesign
{ 
    public static testmethod void myTestMethod1()
    {
        Test.StartTest();
        
        Account a = new Account();
        a.name='test';
        insert a;
        
        Contact c = new Contact();
        c.firstname='test';
        c.lastname='contact';
        insert c;
        
        Portfolio__c testportfolio= new Portfolio__c();    
        testportfolio.name='testPortfolio';
        insert testportfolio;


         Deal__c testDeal1= new Deal__c();
         testDeal1.name='TestDeal1';
         testDeal1.City__c='Hyderabad'; 
         testDeal1.State__c='Andhra Pradesh';
         testDeal1.Total_SF__c=458;
         testDeal1.Vacant_SF__c=450;
         testdeal1.Tenant_s__c='Ninads';
         testdeal1.RR_SPRating__c='NN';
         testdeal1.Property_Type__c='Industrial';
         testdeal1.Deal_Type__c='Existing';
         testdeal1.Primary_Use__c='Community';
         testdeal1.Number_Of_Properties__c='5';
         testdeal1.Cole_Initials__c='NP';
         testdeal1.Date_Identified__c=system.today(); 
         testdeal1.Go_Hard_Date__c= system.today();
         testdeal1.Estimated_COE_Date__c=system.today();         
         testdeal1.Contract_Price__c=6788.00;
         testdeal1.Going_NOI__c=6788.00; 
         testdeal1.CAP_Rate__c=89;
         testdeal1.RR_Reimb_Type__c='NN';
         testdeal1.RR_RemainingTerm__c='10.7';
         testdeal1.RR_Rent_Bump__c='10.7';
         testdeal1.Portfolio_Deal__c=testportfolio.id;
         testdeal1.Seller_s_Broker__c = c.id;
         testdeal1.Seller_s_Broker_Company__c = a.id;
         testdeal1.Source__c = 'source';
         //testdeal1.Price_PSF_Contract_Price__c =67567567.00;
         //testdeal1.Rent_PSF__c =67567.00;
         testdeal1.Property_Owner_Contact__c = c.id;
         testdeal1.Property_Owner_Company__c = a.id;
         testdeal1.Cole_Initials__c ='rere/erer';
         testdeal1.Closing_Date__c=system.today() + 1;
         testdeal1.Hard_Date__c=system.today();
         testdeal1.LOI_Sent_Date__c=system.today();
         testdeal1.Deal_Status__c='Reviewing';
         testdeal1.Zip_Code__c='64051';
         testdeal1.Ownership_Interest__c='Fee Simple';
         testdeal1.HVAC_Warranty_Status__c ='Received';
         testdeal1.Create_Workspace__c =true; 
         insert testDeal1;


         Deal__c testDeal2= new Deal__c();
         testDeal2.name='TestDeal2';
         testDeal2.City__c='Hyderabad'; 
         testDeal2.State__c='Andhra Pradesh';
         testDeal2.Total_SF__c=458;
         testDeal2.Vacant_SF__c=450;
         testDeal2.Tenant_s__c='Ninads';
         testDeal2.RR_SPRating__c='NN';
         testDeal2.Property_Type__c='Industrial';
         testDeal2.Deal_Type__c='Existing';
         testDeal2.Primary_Use__c='Community';
         testDeal2.Number_Of_Properties__c='5';
         testDeal2.Cole_Initials__c='NP';
         testDeal2.Date_Identified__c=system.today(); 
         testDeal2.Go_Hard_Date__c= system.today();
         testDeal2.Estimated_COE_Date__c=system.today();
         testDeal2.Contract_Price__c=6788.00;
         testDeal2.Going_NOI__c=6788.00; 
         testDeal2.CAP_Rate__c=89;
         testDeal2.RR_Reimb_Type__c='NN';
         testDeal2.RR_RemainingTerm__c='10.7';
         testDeal2.RR_Rent_Bump__c='10.7';
         testDeal2.Portfolio_Deal__c=testportfolio.id;
         testDeal2.Seller_s_Broker__c = c.id;
         testDeal2.Seller_s_Broker_Company__c = a.id;
         testDeal2.Source__c = 'source';
         //testdeal1.Price_PSF_Contract_Price__c =67567567.00;
         //testdeal1.Rent_PSF__c =67567.00;
         testdeal1.Property_Owner_Contact__c = c.id;
         testDeal2.Property_Owner_Company__c = a.id;
         testDeal2.Cole_Initials__c ='rere/erer';
         testDeal2.Closing_Date__c=system.today() + 1;
         testDeal2.Hard_Date__c=system.today();
         testDeal2.LOI_Sent_Date__c=system.today();
         testdeal2.Deal_Status__c='Escrow Study';
         testdeal2.Zip_Code__c='64059';
         testdeal2.Ownership_Interest__c='Fee Simple';
         testdeal2.HVAC_Warranty_Status__c ='Received';
         testdeal2.Create_Workspace__c =true; 
         insert testDeal2; 
         
         System.assertEquals(testDeal2.Property_Owner_Company__c,a.id);

          
         testDeal1.City__c=null;  
         testDeal1.State__c=null;
         testDeal1.Total_SF__c=null;
         testDeal1.Vacant_SF__c=null;
         testdeal1.Tenant_s__c=null;
         testdeal1.RR_SPRating__c=null;
         /*testdeal1.Property_Type__c=null;
         testdeal1.Deal_Type__c=null;
         testdeal1.Primary_Use__c=null;
         testdeal1.Number_Of_Properties__c=null;
         testdeal1.Cole_Initials__c=null;
         testdeal1.Date_Identified__c=null; 
         testdeal1.Go_Hard_Date__c= null;
         testdeal1.Estimated_COE_Date__c=null;         
         testdeal1.Contract_Price__c=null;
         testdeal1.Going_NOI__c=null; 
         testdeal1.CAP_Rate__c=null;
         testdeal1.RR_Reimb_Type__c=null;
         testdeal1.RR_RemainingTerm__c=null;
         testdeal1.RR_Rent_Bump__c=null;
         testdeal1.Portfolio_Deal__c=null;
         testdeal1.Seller_s_Broker__c = null;
         testdeal1.Seller_s_Broker_Company__c = null;
         testdeal1.Source__c = null;
         //testdeal1.Price_PSF_Contract_Price__c =67567567.00;
         //testdeal1.Rent_PSF__c =67567.00;
         testdeal1.Property_Owner_Contact__c = null;
         testdeal1.Property_Owner_Company__c = null;
         testdeal1.Cole_Initials__c =null;
         testdeal1.Closing_Date__c=null;
         testdeal1.Hard_Date__c=null;
         testdeal1.LOI_Sent_Date__c=null;
         testdeal1.Deal_Status__c=null;*/
         update testdeal1;
         
         testDeal2.City__c=null; 
         testDeal2.State__c=null;
         testDeal2.Total_SF__c=null;
         testDeal2.Vacant_SF__c=null;
         testdeal2.Tenant_s__c=null;
         update testdeal2;
  
         Deal__c testDeal13= new Deal__c();
         testDeal13.name='TestDeal13';
         testDeal13.City__c='Hyderabad'; 
         testDeal13.State__c='A.P';
         testDeal13.Total_SF__c=5677;
         testDeal13.Vacant_SF__c=5600;
         testdeal13.Tenant_s__c='Polaris';
         testdeal13.Portfolio_Deal__c=testportfolio.id;
         testDeal13.Zip_Code__c='64056';
         testdeal13.Ownership_Interest__c='Fee Simple';
         testdeal13.HVAC_Warranty_Status__c ='Received';
         testdeal1.Create_Workspace__c =true; 

         insert testdeal13; 

         update testdeal13;
         
         PortfolioBatchRedesign batch1 = new PortfolioBatchRedesign();
         Id batchId1 = Database.executeBatch(batch1);
    }



}