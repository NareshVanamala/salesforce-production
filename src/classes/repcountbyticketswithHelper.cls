public with sharing class repcountbyticketswithHelper {

    public transient Integer ticket1to3col1 {set;get;}
    public transient Integer ticket4to24col1 {set;get;}
    public transient Integer ticket25morecol1 {set;get;}
    public transient Integer rowtotalcol1 {set;get;}
    public transient Integer firstrowtotal {set;get;}
    
    public transient Integer ticket1to3col2 {set;get;}
    public transient Integer ticket4to24col2 {set;get;}
    public transient Integer ticket25morecol2 {set;get;}
    public transient Integer rowtotalcol2 {set;get;}
    public transient Integer secondrowtotal {set;get;}
    
    public transient Integer ticket1to3col3 {set;get;}
    public transient Integer ticket4to24col3 {set;get;}
    public transient Integer ticket25morecol3 {set;get;}
    public transient Integer rowtotalcol3 {set;get;}
    public transient Integer thirdrowtotal{set;get;}
    
    public Integer lastrowtotal{set;get;}
    
    public repcountbyticketswithHelper (ApexPages.StandardController controller) {
    
       
       ticket1to3col1 =0;
       ticket4to24col1 =0;
       ticket25morecol1 =0;
       rowtotalcol1 =0;
       firstrowtotal=0;
       
       ticket1to3col2 =0;
       ticket4to24col2 =0;
       ticket25morecol2 =0;
       rowtotalcol2 =0;
       secondrowtotal =0;
       
       ticket1to3col3 =0;
       ticket4to24col3 =0;
       ticket25morecol3 =0;
       rowtotalcol3 =0;
       thirdrowtotal =0;
       
       lastrowtotal=0;
       
       Account a=[select id, name from Account where id = :ApexPages.currentPage().getParameters().get('id')];
       list<National_Account_Helper__c> nahListExisting=[ select Account__c,X30To60days1to3Ticlets__c,X30To60days4to24Tickets__c,X30To60days25moreTickets__c,X60To180days1to3Tickets__c,X60to180days25moreTickets__c,X60to180days4to24Tickets__c,X180Plusdays1to3Tickets__c,X180Plusdays25moreTickets__c,X180Plusdays4to24Tickets__c from National_Account_Helper__c where Account__c=:a.id ];
       system.debug('check the size..'+nahListExisting.size());
   
       ticket1to3col1 =ticket1to3col1 +integer.valueOf(nahListExisting[0].X30To60days1to3Ticlets__c);
       ticket4to24col1 =ticket4to24col1 +integer.valueOf(nahListExisting[0].X30To60days4to24Tickets__c);
       ticket25morecol1 =ticket25morecol1 +integer.valueOf(nahListExisting[0].X30To60days25moreTickets__c);
      
      
       
       ticket1to3col2 =ticket1to3col2+integer.valueOf(nahListExisting[0].X60To180days1to3Tickets__c);
        ticket4to24col2 =ticket4to24col2+integer.valueOf(nahListExisting[0].X60to180days4to24Tickets__c);
       ticket25morecol2 =ticket25morecol2+integer.valueOf(nahListExisting[0].X60to180days25moreTickets__c);
      
       
       
       ticket1to3col3 =ticket1to3col3 +integer.valueOf(nahListExisting[0].X180Plusdays1to3Tickets__c);
       ticket4to24col3 =ticket4to24col3+integer.valueOf(nahListExisting[0].X180Plusdays4to24Tickets__c) ;
       ticket25morecol3 =ticket25morecol3+integer.valueOf(nahListExisting[0].X180Plusdays25moreTickets__c);
      
      
      
       rowtotalcol1 =rowtotalcol1+ ticket1to3col1+ticket4to24col1+ticket25morecol1 ;
       firstrowtotal=firstrowtotal+ticket1to3col1+ticket1to3col2 +ticket1to3col3 ;
      
       rowtotalcol2 =rowtotalcol2 +ticket1to3col2 +ticket4to24col2 +ticket25morecol2 ;
       secondrowtotal= secondrowtotal+ticket4to24col1+ ticket4to24col2 +ticket4to24col3 ;
      
       rowtotalcol3 =rowtotalcol3 +ticket1to3col3 +ticket4to24col3 +ticket25morecol3 ;
       thirdrowtotal =thirdrowtotal +ticket25morecol1 +ticket25morecol2 +ticket25morecol3 ;
             
       lastrowtotal=lastrowtotal+firstrowtotal+secondrowtotal+thirdrowtotal ;

}
    

}