@isTest (SeeAllData=true)
private class ocms_WebPropertyService_test {
    
    @isTest static void test_method_one() {
        ocms_WebPropertyService wps = new ocms_WebPropertyService();
        
        wps.gettype();

        Map<String,String> sendMap = new Map<String,String>();
        sendMap.put('action','getPortfolioList');
        wps.executeRequest(sendMap);
        System.assert(sendMap!=Null);

        sendMap.clear();
        sendMap.put('action','getIndustryList');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getTenantList');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getStateList');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getCityList');
        sendMap.put('state','TX');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getResultTotal');
        sendMap.put('portfolio','--Cole Credit Property Trust III');
        sendMap.put('state','TX');
        sendMap.put('city','Houston');
        sendMap.put('tenantIndustry','Office');
        sendMap.put('tenant','**VACANT**');
        sendMap.put('filterRetail','true');
        sendMap.put('filterMultiTenant','true');
        sendMap.put('filterOffice','true');
        sendMap.put('filterIndustrial','true');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getPropertyList');
        sendMap.put('portfolio','--Cole Credit Property Trust III');
        sendMap.put('state','TX');
        sendMap.put('city','Houston');
        sendMap.put('tenantIndustry','Office');
        sendMap.put('tenant','**VACANT**');
        sendMap.put('filterRetail','true');
        sendMap.put('filterMultiTenant','true');
        sendMap.put('filterOffice','true');
        sendMap.put('filterIndustrial','true');
        wps.executeRequest(sendMap);



        wps.loadResponse();
    }
    
 
}