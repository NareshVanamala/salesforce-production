public with sharing class sendemail{
Id casid;
public Case cas{get;set;}
public string note{get;set;}
public boolean success{get;set;}
public void Init()
{
 success=false;
 cas= new Case();
  casid= ApexPages.currentPage().getParameters().get('id');
   cas=[select OwnerId,Owner.name,Reason,Description,CaseNumber,Status,Subject,SuppliedEmail,Note__c,Priority,Type from Case where id =: casid];
//   cas=[select OwnerId,Owner.name,Reason,Description,CaseNumber,Status,Subject,SuppliedEmail,Priority,Type from Case where id =: casid];
  
}

public pagereference send()
{
     List<string> addlist= new list<String>();
     addlist.add(cas.SuppliedEmail);
     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
   system.debug('++++cas.Note__c:'+cas.Note__c);
    mail.setUseSignature(false);
    mail.setToAddresses(addlist);
    mail.setSubject('Response to case number ' +cas.CaseNumber );
    String casbody='';
    casbody=casbody+cas.Note__c;
    casbody=casbody+'<br/>';
    casbody=casbody+'<a href="mailto:isrgrequest@ib8qmmlbobtcrbp2655r4uju.31qaaeae.5.case.salesforce.com?subject=Response to case number ';
    //casbody=casbody+'<a href="mailto:rashmi.b@6gk0ggftyh7e66dh2qyypz4z.rgiqwmas.r.case.sandbox.salesforce.com?subject=Response to case number ';
    casbody=casbody+cas.CaseNumber;
    casbody=casbody+'">Click here to reply</a>';
      
    mail.setHtmlBody(casbody);
   // mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
 
    // Send the email
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
     cas.Email_sent__c ='The email is sent from '+UserInfo.getUserName() +' to '+cas.SuppliedEmail;
     if (Schema.sObjectType.Case.isUpdateable()) 

     update cas;
       success= true;
    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Email with Document sent to '+email));
     System.debug('+++++successbefore'+success);
    /*List<Messaging.SendEmailResult> results = 
    Messaging.sendEmail(new Messaging.Email[] { mail });
if (!results.get(0).isSuccess()) {
    
  
    System.debug('+++++success'+success);
}*/
    return null;
}



}