global class MRI_updatePropmanagerToAssignedToOnTasks{

   @InvocableMethod
   public static void MRI_updatePropmanagerToAssignedToOnTasks(list<string>MRIPropIds){ 
   Map<id,id>mapPropManagerToPropId=new Map<id,id>();
   list<task>updateTaskList = new list<task>();
   list<MRI_PROPERTY__c>MRIPropertiesList = new list<MRI_PROPERTY__c>();
     MRIPropertiesList = [Select Id,Name,Property_Manager__c from MRI_PROPERTY__c where id in:MRIPropIds]; 
     system.debug('MRIPropertiesList'+MRIPropertiesList);
     for(MRI_PROPERTY__c MRI: MRIPropertiesList){
            mapPropManagerToPropId.put(MRI.Id,MRI.Property_Manager__c);
     }
     List<Task> OpenTaskslist = Database.query('select id,OwnerId,whatid  from task where whatid in:MRIPropIds and status!=\'Completed\' and IsRecurrence != TRUE and Admin_Task__c != TRUE and'+ system.label.MRI_updatePropmanagerToAssignedToOnTasks);

     if(OpenTaskslist.size()>0){

        for(task tsk:OpenTaskslist){
             
            if((tsk.whatid!=null)&&(mapPropManagerToPropId.get(tsk.whatid)!=null))
               tsk.ownerId=mapPropManagerToPropId.get(tsk.whatid);
               tsk.Assigned_To_Got_Changed__c=TRUE;
               updateTaskList.add(tsk); 
            
                                 
        }
    }
          
    if(updateTaskList.size()>0)
        update updateTaskList;
    
   }
}