global class YesrTodateTesting implements Database.Batchable<SObject>
{
global Database.QueryLocator start(Database.BatchableContext BC)  
{
    String Query;
    system.debug('This is start method');  
    Query = 'SELECT id,Year_To_Date__c from Contact' ;
    system.debug('Myquery is......' + Query);
    return Database.getQueryLocator(Query);
}
global void execute(Database.BatchableContext BC, List<Contact> scope)  
{
  list<Contact>updatelist= new list<contact>();
  for(Contact ri : scope)
  {
      ri.Year_To_Date__c=0;
      updatelist.add(ri);
    }
    
 If (Schema.sObjectType.Contact.isUpdateable())
   
   update updatelist;
}
global void finish(Database.BatchableContext BC)  
{
   
}



}