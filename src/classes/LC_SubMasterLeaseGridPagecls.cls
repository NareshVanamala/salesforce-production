public with sharing class LC_SubMasterLeaseGridPagecls 
{
    public ID refid{get;set;}
    public List<Helper_for_Master_Lease__c> hmlList {get;set;}

    public LC_SubMasterLeaseGridPagecls (ApexPages.StandardController Controller) 
    {
        refid=ApexPages.currentPage().getParameters().get('ID');
        
       
        hmlList = [Select id,Name__c,Lease__c,Lease_Opp_ID__c,Lease_Opportunity__c,Master_Lease_Id__c,MRI_Property__c,MRI_PROPERTY__r.Name,Lease__r.Master_Lease_ID__c,MRI_Property__r.Property_ID__c,Lease__r.Suite_Sqft__c,Going_on_NOI1__c,GLA_Suite_SQFT__c,Lease_Expiration_Date__c,LTC_Name__c,Property_Owner_SPE__c,Lease_Id__c,Building_Id__c,Additional_Information__c,
        Lease__r.MRI_Lease_ID__c,Lease__r.LeaseId__c,Lease__r.Center_Name__c,Lease__r.Tenant_Name__c,MRI_Property__r.Common_Name__c,MRI_Property__r.NOI__c,Lease__r.Expiration_Date__c,MRI_Property__r.Property_Owner_SPE__c ,MRI_PROPERTY__r.Property_Owner_SPE__r.Name, Lease__r.Name
        From Helper_for_Master_Lease__c where  Lease_Opportunity__c=:refid ];
        
    }
    public void recupdate() 
    {
        if(!hmlList.isEmpty())
        {
            update hmlList;
        }
    }
    
    public PageReference reset() {

        PageReference newpage = new PageReference(System.currentPageReference().getURL());

    newpage.setRedirect(true);

    return newpage;

    }

    public Void delete1() 
    {
        id delname = ApexPages.CurrentPage().getParameters().get('delname');
        Helper_for_Master_Lease__c  cmp=[select name, id from Helper_for_Master_Lease__c where id=:delname];
        
         if (Helper_for_Master_Lease__c.sObjectType.getDescribe().isDeletable()) 
                {
                        delete cmp;
                }
     
     
    }
}