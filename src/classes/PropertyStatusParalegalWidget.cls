Public with sharing class PropertyStatusParalegalWidget
{
   
   public String username{get;set;}
   public String username1{get;set;}
   public List<User> ulist;
   public List<User> ulist2;
   private User user;
   set<id> UserIds = new set<id>();
   public PropertyStatusParalegalWidget(){
   }
    public PropertyStatusParalegalWidget(ApexPages.StandardController controller)
   {
            this.user= (User)controller.getRecord();
   }
   
   //Getter and Setter for Viewname
    public String getusername1() 
    {
      return username1;
    }
    public void setusername1(String username1) 
    {
      this.username1 = username1;
    }
   
       //Search Call List from call List drop down box. --Start---
    public List<SelectOption> getUserList()
    {
        List<SelectOption> options = new List<SelectOption>();
        ulist = [select id, name,Profile.Name ,UserRole.Name  from User where (Profile.Name = 'Deal Central - Legal Team' or Profile.Name = 'Deal Central - Legal' or Profile.Name = 'Deal Central - Legal Team (Edit Reports)' or Profile.Name = 'Deal Central - Legal Team (Salesforce)' or  Profile.Name = 'Beta - Deal Central - Legal Team' or Profile.Name = 'RE Legal Profile'  )  and ( IsActive = true) and ( UserRole.Name Like '%legal%' )];
       
        For(integer i=0;i<ulist.size();i++)
        {
            options.add(new SelectOption(ulist[i].id,ulist[i].name));
        }
        return options;
        
    } // -- End---
    
    Public PageReference RunReport(){
        List<String> selUserNames = new List<String>();
        String finUserNames='';
        userName1 = userName1.replace('[','');
        userName1 = userName1.replace(']','');
        System.Debug('zzz'+userName1);
        selUserNames.addAll(username1.split(','));
        System.debug('%%%%' + selUserNames);
        
        
        userName1 = userName1.replace(' ','');
        System.debug('RESULT' + userName1);
        PageReference ref = new PageReference('/apex/PropertyStatusParalegal?paralegalIds='+ userName1);
     return ref;
    }
   
   
  }