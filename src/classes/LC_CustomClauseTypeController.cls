public with sharing class LC_CustomClauseTypeController
{

    public string phoneNumberPassToTextBox{get;set;}
    public List<LC_Clauses__c> results{get;set;}
    public string searchString{get;set;}  
    public List<cContact>contactList {get; set;}
    public List<cContact>selectedcontactList {get; set;}
    public list<LC_ClauseType__c> ClauseTypelist{get; set;}
    public String leaseOppid{get; set;}
    
    
    public void SearchRecord()
    {
        showsearch=true;  
        shownew=false;  
        system.debug('This is my searchstring'+searchString);
        system.debug('This is what I selected'+Snooze);
        runSearch();  
     }
     
      @testvisible private void runSearch()
      { 
        system.debug('I am in runsearch and This is my searchstring'+searchString);
        system.debug('I am in runsearch and This is what I selected'+Snooze);
          results = performSearch(searchString);
           system.debug('This is my results'+results );
          
          
            contactList = new List<cContact>();
            for(LC_Clauses__c c:results)
            {
            // As each contact is processed we create a new cContact object and add it to the contactList
                contactList.add(new cContact(c));
            }
      
        
        system.debug('****************'+contactList);
          system.debug('this is my results'+results );
      } 
       private List<LC_Clauses__c> performSearch(string searchString)
       {
         
          leaseOppid= ApexPages.currentPage().getParameters().get('Id');
          system.debug('This is my leaseOpportnity'+leaseOppid);
          
          list<LC_ClauseType__c> lcclaustype=[select id,name,Lease_Opportunity__c from LC_ClauseType__c where Lease_Opportunity__c=:leaseOppid];
            system.debug('Check the list size'+lcclaustype);
             set<string>myOppname=new set<String>();
            for(LC_ClauseType__c LCT:lcclaustype)
             myOppname.add(LCT.name);

            system.debug(myOppname);
             
                   String soql = 'select id, name,Clause_Type__c ,Clause_Body__c from LC_Clauses__c where name not in:myOppname';
                   //String soql = 'select id, name,Clause_Type__c ,Clause_Body__c from LC_Clauses__c ';
             if((searchString == '')||(searchString == null)) 
           { 
            // soql = soql +  ' and name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\'';
            soql=soql+ ' order by Clause_Type__c ASC' ;
             soql = soql + ' limit 200';
           }       

           if(searchString != '' && searchString != null && Snooze=='Name') 
           { 
            // soql = soql +  ' and name LIKE \'%' + searchString +'%\'';
            soql=soql+ ' and name=:searchString' ;
             soql = soql + ' limit 200';
           }
           if(searchString != '' && searchString != null && Snooze=='All fields' )
           {
               // soql = soql + '  where (name LIKE \'%' + searchString +'%\')'+'OR'+'(Address__c LIKE \'%' + searchString +'%\')+'OR'+'(City__c LIKE \'%' + searchString +'%\'))' +'OR'+ '(Notes__c LIKE \'%' + searchString +'%\'))' + 'OR'+ '(Address__c LIKE \'%' + searchString +'%\'))' +'OR'+ '(State__c LIKE \'%' + searchString +'%\'))' + 'OR'+ '(Zip__c LIKE \'%' + searchString +'%\'))' +'OR'+ '(Phone_Number__c LIKE \'%' + searchString +'%\'))';
                soql = soql +  ' and ((name LIKE \'%' + String.escapeSingleQuotes(searchString)+'%\')'+'OR'+'(Clause_Type__c LIKE \'%' + String.escapeSingleQuotes(searchString)+'%\'))';
                
           }          
           System.debug(soql);
           return database.query(soql); 
        }

    public PageReference Save()
    {
       String leaseOppid2= ApexPages.currentPage().getParameters().get('Id');
       LC_Clauses__c crc= new LC_Clauses__c();
       LC_ClauseType__c RTC= new LC_ClauseType__c();
       
       if((vdr.Name=='')||(vdr.Name==null))
       {
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Enter Clause Name')); 
       }
       else
       {
           crc.Name=vdr.Name;
           crc.Clause_Type__c =vdr.Clause_Type__c ;
           if (Schema.sObjectType.LC_Clauses__c.isCreateable())

           insert crc;
           
            
           RTC.name=vdr.Name;
           RTC.Clauses__c=crc.id;
           RTC.Lease_Opportunity__c=leaseOppid2;
           RTC.Clause_Description__c=vdr.Clause_Body__c;
           RTC.ClauseType_Name__c=vdr.Clause_Type__c ;
           if (Schema.sObjectType.LC_ClauseType__c.isCreateable())

           insert RTC;
       }   
       
       String lcid= ApexPages.currentPage().getParameters().get('Id');
       PageReference LseOptyPage = new PageReference('/' + lcid);
       LseOptyPage .setRedirect(true);
       return LseOptyPage ;
    }
    

    public PageReference createNewRecord()
    {
      system.debug('Woh jo hamase keh na sake...') ;
       // Vendor__c vr1= new Vendor__c ();
         showsearch=false;  
         shownew=true;
        
        return null;
    }

   public LC_Clauses__c vdr{get; set;}
   public boolean showsearch{get; set;}
   public boolean shownew{get; set;}
   public string Snooze {get; set;}
   //public List<user> results{get;set;} 
   
    public LC_CustomClauseTypeController(ApexPages.StandardController controller) 
    
    {     
         results=new list<LC_Clauses__c>();
         selectedcontactList=new list<cContact>();
        showsearch=true;  
        shownew= false;
      vdr= new LC_Clauses__c();
     runSearch();  
    }
   public LC_CustomClauseTypeController()
   {
         results=new list<LC_Clauses__c>();
         selectedcontactList=new list<cContact>();
        showsearch=true;  
        shownew= false;
      vdr= new LC_Clauses__c();
     runSearch();  
     
   }
   
   
   public List<SelectOption> getSnoozeradio()
  {
    List<SelectOption> snoozeoptions = new List<SelectOption>(); 
    snoozeoptions.add(new SelectOption('Name','Name')); 
    snoozeoptions.add(new SelectOption('All fields','All fields')); 
    return snoozeoptions ;
 }
    public String getResults() {
        return null;
    }
    
    public List<cContact> getContacts()
    {
        if(contactList == null)
         {
            contactList = new List<cContact>();
            for(LC_Clauses__c c:results) {
            // As each contact is processed we create a new cContact object and add it to the contactList
                contactList.add(new cContact(c));
            }
        }
        
        system.debug('****************'+contactList);
        return contactList;
        
    }


    public String getSnooze() 
    {
        return null;
    }
    
    
     public PageReference AddClause()
     {
       
        leaseOppid= ApexPages.currentPage().getParameters().get('Id');
        ClauseTypelist=new list<LC_ClauseType__c>();
        system.debug('In the AddClause'+contactList);
        selectedcontactList =new list<cContact>();
        if(contactList.size()>0)
        {
              for(cContact ct:contactList)
              {
                 if(ct.selected==true)
                  selectedcontactList.add(ct);
              
              }
        }   
        system.debug('get me the slected records'+selectedcontactList);    
        if(selectedcontactList.size()>0)
        {
          for(cContact ct:selectedcontactList)
          {
             LC_ClauseType__c LCV=new LC_ClauseType__c();
             LCV.name=ct.con.name;
             LCV.ClauseType_Name__c=ct.con.Clause_Type__c;
             LCV.Lease_Opportunity__c=leaseOppid;
             LCV.Clauses__c=ct.con.id; 
             LCV.Clause_Description__c=ct.con.Clause_Body__c;
             ClauseTypelist.add(LCV);
          
          }
        
        } 
        
          if(ClauseTypelist.size()>0)
          insert ClauseTypelist;
        
         PageReference LseOptyPage = new PageReference('/' + LeaseOppid);
         LseOptyPage .setRedirect(true);
         return LseOptyPage ;
        //return null;
    }
    public PageReference Back()
    {
       
       system.debug('Am i getting the id'+leaseOppid);
       String lcid= ApexPages.currentPage().getParameters().get('Id');
       PageReference LseOptyPage = new PageReference('/' + lcid);
       LseOptyPage .setRedirect(true);
       return LseOptyPage ;
    }
    
    
    
    
     public class cContact
     {
        public LC_Clauses__c con {get; set;}
        public Boolean selected {get; set;}

        //This is the contructor method. When we create a new cContact object we pass a Contact that is set to the con property. We also set the selected value to false
        public cContact(LC_Clauses__c c)
        {
            con = c;
            selected = false;
       }
    }

    


}