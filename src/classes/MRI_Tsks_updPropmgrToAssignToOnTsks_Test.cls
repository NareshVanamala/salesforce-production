@isTest
public class MRI_Tsks_updPropmgrToAssignToOnTsks_Test{
  
 static testMethod void MRITsksMethod(){
        MRI_Property__c mriProp = new MRI_Property__c();
        mriProp.Name = 'Sample PropCon';
        mriProp.AvgLeaseTerm__c =72.00;
        mriProp.Property_Id__c ='12345';
        mriProp.OwnerId =UserInfo.getuserid();
        mriProp.Property_Manager__c=UserInfo.getuserid();
        insert mriProp;
        
        Task tsk = new Task();
        tsk.whatid = mriProp.id;
        tsk.subject='test';
        tsk.status = 'New';
        tsk.IsRecurrence = False;
        //tsk.Admin_Task__c = False;
        tsk.RecordTypeId='01238000000E9nb';
        insert tsk;
        list<string> tskId = new list<string>();
        tskId .add(tsk.id);
        
        MRI_Tasks_updatePropmngrToAssignToOnTsks.MRI_Tasks_updatePropmngrToAssignToOnTsks(tskId );
   }

}