Public class getComments{
   
    public Static void GetmetheApprovalhistory(Id contactid){
        system.debug('Ia m in future method...Welcome to future method');
        List<Asset__c> Assets=[Select Clarizen__c,c.Previous_Options__c,c.Employee__c,c.Bank_Options__c,c.Policy_Tech_Role__c,c.Ecova_Options__c,c.SECURITY_GROUPS__c,c.ROLES__c,c.Id,c.Incident_No__c,c.Final_Step_Procssed__c,c.Tenrox__c,c.OneSource_Options__c,c.Chatham_Options__c,c.Argus_enterprise_options__c,c.Access_Name__c,c.MRI_options_c__c,c.Workiva__c,c.Tableau_Sites_List_options__c,c.DST_Options__c,c.Asset_Status__c,c.Additional_Email_Address__c,(Select Id, IsPending, ProcessInstanceId, TargetObjectId, StepStatus, OriginalActorId, ActorId, RemindersSent, Comments, IsDeleted, CreatedDate, CreatedById, SystemModstamp From ProcessSteps where stepstatus='Approved' or stepstatus='Rejected' order by createddate DESC limit 1) 
         From Asset__c  c where id=:contactid and Final_Step_Procssed__c=true ];
        string empId = Assets[0].Employee__c;
        List<Employee__c> UpdateEmployeeList=new list<Employee__c>();
        Employee__c emp =[select id,Active_Directory_Group__c,Active_Directory_Group_2__c,Active_Directory_Group_3__c,Active_Directory_Group_4__c,Active_Directory_Group_5__c,Active_Directory_Group_6__c,Active_Directory_Group_7__c,Active_Directory_Group_8__c from Employee__c where id=:empId limit 1];
        
        system.debug('get the assets.'+Assets);
        String commentStr='';
        string status='';
        
        if (Assets.size()>0){
          Asset__c cs =Assets[0];
          for (ProcessInstanceHistory ps : cs.ProcessSteps){
                
                  system.debug('What is my step stattus'+ps.stepstatus);
                //commentStr+='\nComment from user ' + ps.ActorId + ' : ' + ps.comments;
                  commentStr=ps.comments;
                  status=ps.stepstatus;
                  
                  //system.debug('HIIIIIIIIIIIIIIIII'+commentStr);
          }
            
             if(commentStr!=null){
               Incident_No__c ct1= new Incident_No__c();
                 ct1.Asset__c =Assets[0].id;
                if(commentStr.length()>=80)
                  ct1.name=  commentStr.substring(0,79) ;  
                 else  
                 ct1.name=  commentStr;  
                
                 if(status=='Approved'){
                     ct1.Status__c='Added On' + system.today();
                     ct1.Date__c=system.today();
                     ct1.Operation__c='Added';
                  }
                 else if(status=='Rejected')  {
                      ct1.Status__c='Rejected On' + system.today();
                      ct1.Date__c=system.today();
                      ct1.Operation__c='Rejected';
                  } 
                 if (Schema.sObjectType.Incident_No__c.isCreateable()) 
                   insert ct1;  
                 }     
                     
                    if(((status=='Rejected')&&(cs.Asset_Status__c=='InActive')) || Test.isRunningTest()){ 
                     
                        if((cs.Access_Name__c=='Ecova')&&(cs.Ecova_Options__c!=null))
                         cs.Ecova_Options__c=null;
                      
                        else if((cs.Access_Name__c=='MRI')&&(cs.MRI_options_c__c!=null))
                         cs.MRI_options_c__c=null;
                         
                        else if((cs.Access_Name__c=='Tableau')&&(cs.Tableau_Sites_List_options__c!=null))
                         cs.Tableau_Sites_List_options__c=null;  
                         
                        else if((cs.Access_Name__c=='Workiva')&&(cs.Workiva__c!=null))
                         cs.Workiva__c=null;    
                         
                        else if((cs.Access_Name__c=='Argus Enterprise')&&(cs.Argus_enterprise_options__c!=null))
                         cs.Argus_enterprise_options__c=null;   
                         
                        else if((cs.Access_Name__c=='Tenrox')&&(cs.Tenrox__c!=null))
                         cs.Tenrox__c=null;   
                          
                        else if((cs.Access_Name__c=='Chatham')&&(cs.Chatham_Options__c!=null))
                         cs.Chatham_Options__c=null;
                        
                        else if((cs.Access_Name__c=='OneSource')&&(cs.OneSource_Options__c!=null))
                         cs.OneSource_Options__c=null;
                            
                        else if((cs.Access_Name__c=='DST')&&(cs.DST_Options__c!=null))
                         cs.DST_Options__c=null;
                 
                        else if((cs.Access_Name__c=='PBCS')&&(cs.SECURITY_GROUPS__c!=null))
                         cs.SECURITY_GROUPS__c=null;   
                             
                        else if((cs.Access_Name__c=='PBCS')&&(cs.ROLES__c!=null))
                         cs.ROLES__c=null;

                        else if((cs.Access_Name__c=='PolicyTech')&&(cs.Policy_Tech_Role__c!=null))
                         cs.Policy_Tech_Role__c=null; 

                        else if((cs.Access_Name__c=='US Bank')&&(cs.Bank_Options__c!=null))
                         cs.Bank_Options__c=null; 

                        else if((cs.Access_Name__c=='JP Morgan')&&(cs.Bank_Options__c!=null))
                         cs.Bank_Options__c=null; 

                        else if((cs.Access_Name__c=='Bank of America')&&(cs.Bank_Options__c!=null))
                         cs.Bank_Options__c=null;
                         
                        else if((cs.Access_Name__c=='Additional Mailbox-1')&&(cs.Additional_Email_Address__c!=null))
                        cs.Additional_Email_Address__c=null; 
                        
                        else if((cs.Access_Name__c=='Additional Mailbox-2')&&(cs.Additional_Email_Address__c!=null))
                        cs.Additional_Email_Address__c=null;    
                        
                        else if((cs.Access_Name__c=='Additional Mailbox-3')&&(cs.Additional_Email_Address__c!=null))
                        cs.Additional_Email_Address__c=null;    
                        
                        else if((cs.Access_Name__c=='Additional Mailbox-4')&&(cs.Additional_Email_Address__c!=null))
                        cs.Additional_Email_Address__c=null;  
                    
                        else if((cs.Access_Name__c=='IT Infrastructure RBAC')&&(cs.Asset__c!=null))
                        cs.Asset__c=null;

                        else if((cs.Access_Name__c=='Clarizen')&&(cs.Clarizen__c!=null))
                        cs.Clarizen__c=null;                    
                        
                        else if((cs.Access_Name__c=='ADP Admin Access Portal')&&(cs.Asset__c!=null))
                        cs.Asset__c=null; 

                        else if((cs.Access_Name__c=='Callidus Cloud')&&(cs.Callidus_Cloud__c!=null))
                        cs.Callidus_Cloud__c=null;                  
                        
                        else if(cs.Access_Name__c=='Active Directory Group1' && emp.Active_Directory_Group__c != null && empId != null && empId != ''){
                         emp.Active_Directory_Group__c = cs.Previous_Options__c;
                         updateEmployeeList.add(emp);        
                        }
                        
                        else if(cs.Access_Name__c=='Active Directory Group2' && emp.Active_Directory_Group_2__c != null && empId != null && empId != ''){
                         emp.Active_Directory_Group_2__c=null;
                         updateEmployeeList.add(emp);        
                        }
                        
                        else if(cs.Access_Name__c=='Active Directory Group3' && emp.Active_Directory_Group_3__c != null && empId != null && empId != ''){
                         emp.Active_Directory_Group_3__c=null;
                         updateEmployeeList.add(emp);        
                        }
                        
                        else if(cs.Access_Name__c=='Active Directory Group4' && emp.Active_Directory_Group_4__c != null && empId != null && empId != ''){
                         emp.Active_Directory_Group_4__c=null;
                         updateEmployeeList.add(emp);        
                        }
                        
                        else if(cs.Access_Name__c=='Active Directory Group5' && emp.Active_Directory_Group_5__c != null && empId != null && empId != ''){
                         emp.Active_Directory_Group_5__c=null;
                         updateEmployeeList.add(emp);        
                        }
                        
                        else if(cs.Access_Name__c=='Active Directory Group6' && emp.Active_Directory_Group_6__c != null && empId != null && empId != ''){
                         emp.Active_Directory_Group_6__c=null;
                         updateEmployeeList.add(emp);        
                        }
                        
                        else if(cs.Access_Name__c=='Active Directory Group7' && emp.Active_Directory_Group_7__c != null && empId != null && empId != ''){
                         emp.Active_Directory_Group_7__c=null;
                         updateEmployeeList.add(emp);        
                        }
                        
                        else if(cs.Access_Name__c=='Active Directory Group8' && emp.Active_Directory_Group_8__c != null && empId != null && empId != ''){
                         emp.Active_Directory_Group_8__c=null;
                         updateEmployeeList.add(emp);        
                        }
                        
                                         
                  }
                
                 cs.Final_Step_Procssed__c =false;
                 if(Schema.sObjectType.Asset__c.isUpdateable()) 
                    update cs; 
                 
                if(updateEmployeeList.size()>0){
                   if(Schema.sObjectType.Employee__c.isUpdateable())
                      update updateEmployeeList;
                }
             }
          
         system.debug('Now check the commentstr'+commentStr); 
       }
     public Static void GetmetheApprovalhistory1(Id contactid){
        system.debug('Ia m in future method...Welcome to future method');
        List<Asset__c> Assets=[Select c.MRI_options_c__c,c.Workiva__c,c.Tableau_Sites_List_options__c,c.DST_Options__c,c.Tenrox__c,c.OneSource_Options__c,c.Chatham_Options__c,c.Argus_enterprise_options__c,c.Ecova_Options__c,c.SECURITY_GROUPS__c,c.ROLES__c, c.Id,c.Incident_No__c,c.Final_Step_Procssed__c,c.Removal_Final_Step_processed__c,(Select Id, IsPending, ProcessInstanceId, TargetObjectId, StepStatus, OriginalActorId, ActorId, RemindersSent, Comments, IsDeleted, CreatedDate, CreatedById, SystemModstamp From ProcessSteps where stepstatus='Approved' or stepstatus='Rejected' order by createddate DESC limit 1) 
        From Asset__c  c where id=:contactid and Removal_Final_Step_processed__c=true];
      //  system.debug('get the assets list.'+Assets[0].ProcessSteps);
        String commentStr='';
        string status='';

        if (Assets.size()>0){
             Asset__c cs =Assets[0];
            for (ProcessInstanceHistory ps : cs.ProcessSteps){
                 status=ps.stepstatus;
                //commentStr+='\nComment from user ' + ps.ActorId + ' : ' + ps.comments;
                  commentStr=ps.comments;
             }
             
             if(commentStr!=null){
                 Incident_No__c ct1= new Incident_No__c();
                 ct1.Asset__c =Assets[0].id;
                 if(commentStr.length()>=80)
                  ct1.name=  commentStr.substring(0,79) ;  
                 else  
                 ct1.name=  commentStr;         
                 if(status=='Approved'){
                     ct1.Status__c='Removed On' + system.today();
                     ct1.Date__c=system.today();
                     ct1.Operation__c='Removed';          
                 }
                 else if(status=='Rejected')  {
                      ct1.Status__c='Rejected On' + system.today();
                      ct1.Date__c=system.today();
                      ct1.Operation__c='Rejected';  
                 } 
                 if(Schema.sObjectType.Incident_No__c.isCreateable())
                    insert ct1;
                 cs.Removal_Final_Step_processed__c=false;
                 if(Schema.sObjectType.Asset__c.isUpdateable())
                    update cs; 
             }
          }
         system.debug('Now check the commentstr'+commentStr); 
       } 
       
        
       public Static void GetmetheApprovalhistory11(Id contactid){
        system.debug('Ia m in future method...Welcome to future method');
        //List<Employee__c>EmpAssets=[Select c.Classification__c,c.EE__c,c.Id,c.Seating_location__c,c.HR_Manager_Status__c,c.HR_step_one_approval_received__c,c.Start_Date__c,c.name,c.Department__c,c.Title__c,c.MGR__r.name,c.Employee_Type__c,(Select Id, IsPending, ProcessInstanceId, TargetObjectId, StepStatus, OriginalActorId, ActorId, RemindersSent, Comments, IsDeleted, CreatedDate, CreatedById, SystemModstamp From ProcessSteps where stepstatus='Approved' ) 
        //From Employee__c  c where id=:contactid and Employee_Final_Step_Processed__c=true and HR_step_one_approval_received__c=true] ;
        List<Employee__c>EmpAssets=[Select c.Classification__c,c.EE__c,c.Id,c.Seating_location__c,c.HR_Manager_Status__c,c.HR_step_one_approval_received__c,c.Start_Date__c,c.name,c.Department__c,c.Title__c,c.MGR__r.name,c.Employee_Type__c From Employee__c  c where id=:contactid and Employee_Final_Step_Processed__c=true and HR_step_one_approval_received__c=true] ;
        //system.debug('get the assets list.'+EmpAssets[0].ProcessSteps);
       // String commentStr='';
       // string classfication='';
       // String EEnumber='';
        if (EmpAssets.size()>0){
            Employee__c cs =EmpAssets[0];
           /* for (ProcessInstanceHistory ps : cs.ProcessSteps){
                //commentStr+='\nComment from user ' + ps.ActorId + ' : ' + ps.comments;
                  commentStr=ps.comments;
             }
             
             if(commentStr!=null){
             
                 list<string> A=commentStr.split(' ');
                 system.debug('Whats the value'+A);
                 cs.EE__c=A[0];
                 for(integer i=1;i<A.size();i++){
                    // cs.Classification__c=A[1]+' '+A[2];
                      if(cs.Classification__c!=null)
                       cs.Classification__c=cs.Classification__c+' '+ A[i];
                      else if(cs.Classification__c==null)
                       cs.Classification__c=A[i];
                  }  
                   classfication= cs.Classification__c;
                   EEnumber= A[0];
                  
             }*/
             cs.Employee_Final_Step_Processed__c=false;
             cs.Send_New_Hire_Notification__c=true;
             cs.HR_Approval_Date__c=system.today();
             if(Schema.sObjectType.Employee__c.isUpdateable())
                update cs; 
             
          if(cs.Employee_Type__c!='Service Account'){
           //system.debug('These are My Comments'+commentStr); 
           Email_addresses__c ec=Email_addresses__c.getvalues('NewHireNotofication');
           string toaddress=ec.To_Address__c;
           string toaddress1=ec.To_Address_1__c;
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                  String[] toAddresses = new String[] {toaddress,toaddress1};
                  mail.setToAddresses(toAddresses);
                  Integer month=cs.Start_Date__c.month();
                  Integer day=cs.Start_Date__c.day();
                  Integer year=cs.Start_Date__c.year();
                  string dateyesr=string.valueof(year);
                  dateyesr=dateyesr.right(2);
                  mail.setSubject('New Hire');
                  mail.setPlainTextBody
                    ('Name' + ':'+' '+cs.Name + '\n' +
                     'Title'+' '+':'+' '+cs.Title__c+'\n'+
                    'Department'+' '+':'+' '+cs.Department__c+'\n'+
                     'Start date'+' '+':'+' '+month+'/'+day+'/'+dateyesr+'\n'+
                    'Reports to'+' '+':'+''+cs.MGR__r.name+'\n'+
                    'EE#'+' '+':'+' '+cs.EE__c +'\n'+
                    'Classification'+' '+':'+' '+cs.Employee_Type__c+'\n'+
                    'Location'+' '+':'+' '+cs.Seating_location__c+'\n'
                    );
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
         
         }
         
         
         
       }   
       
       public Static void UpdatetheOptionchange(Id contactid){
     
        system.debug('This is the future class');
        List<Asset__c> OptionChangeAssetlist=[select Clarizen__c,Policy_Tech_Role__c,Bank_Options__c,Employee__c,Ecova_Options__c,id,Previous_Options1__c,SECURITY_GROUPS__c,ROLES__c,Chatham_Options__c,OneSource_Options__c,Tenrox__c,Argus_enterprise_options__c,name,MRI_options_c__c,Workiva__c,Previous_Options__c,Tableau_Sites_List_options__c,Removal_Final_Step_processed__c,Access_Name__c,Additional_Email_Address__c from Asset__c where Option_Change_Rejected__c=true and id=:contactid limit 1];
        List<Asset__c> UpdateOptionChangeAssetlist=new list<Asset__c>();
        List<Employee__c> UpdateEmployeeList=new list<Employee__c>();
        string empId = OptionChangeAssetlist[0].Employee__c;
        //if(empId != null && empId != '')
        Employee__c emp =[select id,Active_Directory_Group__c,Active_Directory_Group_2__c,Active_Directory_Group_3__c,Active_Directory_Group_4__c,Active_Directory_Group_5__c,Active_Directory_Group_6__c,Active_Directory_Group_7__c,Active_Directory_Group_8__c from Employee__c where id=:empId limit 1];
        
         if(OptionChangeAssetlist[0].Access_Name__c=='IT Infrastructure RBAC'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Asset__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;  
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);         
         } 
         else if(OptionChangeAssetlist[0].Access_Name__c=='Clarizen'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Clarizen__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;  
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);         
         }
         else if(OptionChangeAssetlist[0].Access_Name__c=='ADP Admin Access Portal'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Asset__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;  
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);         
         }
         else if(OptionChangeAssetlist[0].Access_Name__c=='Callidus Cloud'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Callidus_Cloud__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;  
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);         
         } 
         else if(OptionChangeAssetlist[0].Access_Name__c=='Ecova'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Ecova_Options__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;  
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);         
         }      
         else if(OptionChangeAssetlist[0].Access_Name__c=='MRI'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].MRI_options_c__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false; 
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);     
          }            
         else if(OptionChangeAssetlist[0].Access_Name__c=='Tableau'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Tableau_Sites_List_options__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Workiva'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Workiva__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);    
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Argus Enterprise'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Argus_enterprise_options__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]); 
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Tenrox'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Tenrox__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]); 
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Chatham'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Chatham_Options__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;         
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);   
          } 
          else if(OptionChangeAssetlist[0].Access_Name__c=='DST'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].DST_Options__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);     
          } 
          else if(OptionChangeAssetlist[0].Access_Name__c=='PBCS'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].SECURITY_GROUPS__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions; 
             string orinialOptions1=OptionChangeAssetlist[0].Previous_Options1__c;
             OptionChangeAssetlist[0].ROLES__c=orinialOptions1;
             OptionChangeAssetlist[0].Additional_Instructions_Requests1__c=orinialOptions1;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);    
          }   
          else if(OptionChangeAssetlist[0].Access_Name__c=='OneSource'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].OneSource_Options__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);                  
          } 
          else if(OptionChangeAssetlist[0].Access_Name__c=='PolicyTech'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Policy_Tech_Role__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);                  
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='US Bank'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Bank_Options__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);                  
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='JP Morgan'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Bank_Options__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);                  
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Bank of America'){
             string orinialOptions=OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Bank_Options__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);                  
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Additional Mailbox-1'){
             string orinialOptions=OptionChangeAssetlist[0].Additional_Email_Address__c;
             OptionChangeAssetlist[0].Additional_Email_Address__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);                  
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Additional Mailbox-2'){
             string orinialOptions=OptionChangeAssetlist[0].Additional_Email_Address__c;
             OptionChangeAssetlist[0].Additional_Email_Address__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);                  
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Additional Mailbox-3'){
             string orinialOptions=OptionChangeAssetlist[0].Additional_Email_Address__c;
             OptionChangeAssetlist[0].Additional_Email_Address__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);                  
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Additional Mailbox-4'){
             string orinialOptions=OptionChangeAssetlist[0].Additional_Email_Address__c;
             OptionChangeAssetlist[0].Additional_Email_Address__c=orinialOptions;
             OptionChangeAssetlist[0].Additional_Instructions_Requests__c=orinialOptions;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);                  
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Active Directory Group1' && empId != null && empId != ''){
             emp.Active_Directory_Group__c = OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]); 
             updateEmployeeList.add(emp);        
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Active Directory Group2' && empId != null && empId != ''){
             emp.Active_Directory_Group_2__c = OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);   
             updateEmployeeList.add(emp);            
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Active Directory Group3' && empId != null && empId != ''){
             emp.Active_Directory_Group_3__c = OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]); 
             updateEmployeeList.add(emp);
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Active Directory Group4' && empId != null && empId != ''){
             emp.Active_Directory_Group_4__c = OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);   
             updateEmployeeList.add(emp);
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Active Directory Group5' && empId != null && empId != ''){
             emp.Active_Directory_Group_5__c = OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]); 
             updateEmployeeList.add(emp);            
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Active Directory Group6' && empId != null && empId != ''){
             emp.Active_Directory_Group_6__c = OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);  
             updateEmployeeList.add(emp);
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Active Directory Group7' && empId != null && empId != ''){
             emp.Active_Directory_Group_7__c = OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);   
             system.debug('emp.Active_Directory_Group_7__c'+emp.Active_Directory_Group_7__c);
             updateEmployeeList.add(emp);
          }
          else if(OptionChangeAssetlist[0].Access_Name__c=='Active Directory Group8' && empId != null && empId != ''){
             emp.Active_Directory_Group_8__c = OptionChangeAssetlist[0].Previous_Options__c;
             OptionChangeAssetlist[0].Option_Change_Rejected__c=false;
             UpdateOptionChangeAssetlist.add(OptionChangeAssetlist[0]);  
             updateEmployeeList.add(emp);            
          }
          if(UpdateOptionChangeAssetlist.size()>0){
             if(Schema.sObjectType.Asset__c.isUpdateable())
                update UpdateOptionChangeAssetlist;
          }
         
          if(updateEmployeeList.size()>0 ){
             if(Schema.sObjectType.Employee__c.isUpdateable())
                update updateEmployeeList;
          }
     
     
     }

}