@isTest
private class Test_addAttachment 
{
   public static testMethod void TestforaddAttachment() 
   {        
        Account acc = new Account();
        acc.Name = 'Test Prospect Account';
        Insert acc;
        Profile p = [select id from profile where name='System Administrator'];
        User us=new User(alias = 'sandym1', email='sandym1@noemail.com',
            emailencodingkey='UTF-8', lastname='Sandymar', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id, country='India',
            timezonesidkey='America/Los_Angeles', username='sandym1@noemail.com');
        insert us;   
           
        Event_Automation_Request__c ear = new Event_Automation_Request__c();
        ear.Name='BDE13006';
        ear.Event_Name__c='testeventname';
        ear.Start_Date__c=System.Today();
        ear.End_Date__c=System.Today();
        ear.Broker_Dealer_Name__c=acc.id;
        ear.Event_Location__c='testlocation';
        ear.Key_Account_Manager_Name_Requestor__c=us.id;
        ear.Event_Venue__c='testvenue';
        ear.testingfield__c='';
        ear.Sponsor_Cost__c=50000;
        insert ear;
        System.assertEquals( ear.Broker_Dealer_Name__c,acc.id);

        
        
        list<attachment> attachmentlist = new list<attachment>();
        
        String nameFile = 'hello';
        Attachment attachment= new Attachment(); 
        attachment.OwnerId = UserInfo.getUserId();  
        attachment.ParentId = ear.Id;// the record the file is attached to   
        attachment.Name = nameFile;     
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');  
        attachment.body=bodyBlob;   
        attachmentlist.add(attachment);
         
        String namefile1='hello123';
        Attachment attachment1= new Attachment(); 
        attachment1.OwnerId = UserInfo.getUserId();  
        attachment1.ParentId = ear.Id;// the record the file is attached to   
        attachment1.Name = nameFile1;     
        Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body123');  
        attachment1.body=bodyBlob1;   
        attachmentlist.add(attachment1);
         
        insert attachmentlist;
        
        Event_Automation_Request__c ear1 = [select id,name,event_name__c,start_date__c,end_date__c,broker_dealer_name__c,event_location__c,Key_Account_Manager_Name_Requestor__c,Event_Venue__c,testingfield__c,(select id,name,parentid,ownerid from attachments) from event_automation_request__c where id=:ear.id]; 
        ear1.testingfield__c=ear1.testingfield__c+','+attachment.id;
        update ear1;
        string attid=ear1.attachments[0].id;
        pagereference pageref = system.Currentpagereference();
        pageref.getParameters().put('id',ear1.id); 
        
        id sid=ear1.attachments[1].id;
        ApexPages.StandardController controller = new ApexPages.StandardController(ear1);
        addAttachment sc = new addAttachment(controller);
        sc.displaylist=sc.getrecordstodisplay();
        sc.displaylist[1].wchecked=true;
        sc.selectedrecords();
        sc.selectedlist=sc.getlist();
        sc.attPage();
        sc.removeatt();
        
        
      }
}