public class Approval_Records_Preparation implements Database.Batchable<sObject>,Database.Stateful{

    string csvFile;
    string csvFile1;
    map<String, Schema.SObjectType> schemamap;
    map<String, Schema.SObjectField> fieldMap;
    list<string> fieldLst;
    string objPreFix; 
    string commaSepratedFields = '';
    string fieldNames = '';
    string rowInfo = '';
    string rowInfo1 = '';
    integer i=0;
    public Approval_Records_Preparation(){
        
        objPreFix = MRI_Property__c.sObjectType.getDescribe().getKeyPrefix();
        
        schemamap = Schema.getGlobalDescribe();
        fieldMap = schemamap.get('MRI_Property__c').getDescribe().fields.getmap();
        fieldLst = new list<string>();
        //fieldLst.add('id');
        fieldLst.add('Tenant_Name__c');
        fieldLst.add('PropertyID__c');
       // fieldLst.add('Website_Status__c');
        fieldLst.add('Property_Type_Group__c');
        fieldLst.add('City__c');
        fieldLst.add('State__c');
        fieldLst.add('SqFt__c');
       // fieldLst.add('Web_Ready__c');
        
        //Add all the field api names as shown above

        
    
        for(String fieldName : fieldLst){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
            
        }
        for(string s : fieldLst){
            if(i ==0 ){
                fieldNames = s;
                i= i+1;
            }else{
                fieldNames = fieldNames + ',' + s;
            }
        }
        
        
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        string query = 'select '+commaSepratedFields+ ',Website_Status__c, Property_ID__c,Web_Ready__c from MRI_Property__c where (Fund1__c=\'ARCP\' or Fund1__c=\'CCPT3\' or Fund1__c=\'MS1P7\') And (Web_Ready__c=\'Approved\' or Web_Ready__c=\'Pending Approval\' or Web_Ready__c=\'Pending Deleted\' or Web_Ready__c=\'Rejected\') ';
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext BC, List<MRI_Property__c> scope){
        
        set<string> scopeIds = new set<string>();
        set<string> propIdsSet = new set<string>();
        for(MRI_Property__c m : scope){
            if(m.Web_Ready__c =='Rejected'){
                propIdsSet.add(m.Property_ID__c);
            }
        }
        map<string,Web_Properties__c> mapToWebProp = new map<string,Web_Properties__c> ();
        if(propIdsSet !=null && propIdsSet.size()>0){
            for(Web_Properties__c webPropObj : [select id,Property_ID__c from Web_Properties__c where Property_ID__c in :propIdsSet]){
                    mapToWebProp.put(webPropObj.Property_ID__c,webPropObj);
            }
        }
       
        for(MRI_Property__c m : scope){
            if(m.Web_Ready__c =='Pending Approval' || m.Web_Ready__c =='Pending Deleted'){
                scopeIds.add(m.id);
               
            }
            
            i = 0;
            
            if((m.Web_Ready__c== 'Approved' || m.Web_Ready__c =='Pending Approval' || (m.Web_Ready__c =='Rejected' && (mapToWebProp.get(m.Property_ID__c) !=null))) && m.Website_Status__c=='Owned' ||test.isRunningTest()){ //Include original condition here
                
                rowInfo1 = rowInfo1 + '\n';
                for(string s : fieldLst){
                    if(i ==0){
                        
                            rowInfo1 = rowInfo1 + checkNull(string.valueOf(m.get(s)));
                    
                        i = i+1;
                    }else{
                    
                             rowInfo1 = rowInfo1 + ',' +checkNull(string.valueOf(m.get(s)));
                      
                    }
                }
            }
        }
        
        
        
        
        
        if(!scopeIds.isEmpty() || test.isRunningTest()){
            map<id,ProcessInstance> pInst = new map<id,ProcessInstance>([SELECT Id, TargetObjectId FROM ProcessInstance where TargetObjectId=:scopeIds and status != 'Approved']);
            
            set<id> processInstanceIds = new set<id>();        
            set<id> mriIds = new set<id>();
           
            for(ProcessInstance p : pInst.values()){
                processInstanceIds.add(p.id);
            }
            
            if(!processInstanceIds.isEmpty() || test.isRunningTest()){
                
                list<ProcessInstanceWorkitem> pWorkItem = new list<ProcessInstanceWorkitem>([select id, ProcessInstanceId from ProcessInstanceWorkitem where ProcessInstanceId=:processInstanceIds]);
                
                for(ProcessInstanceWorkitem p : pWorkItem ){
                    if(pInst.get(p.ProcessInstanceId) != null){
                        mriIds.add(pInst.get(p.ProcessInstanceId).TargetObjectId);
                    }
                }
            }
            
            
            
            if(!mriIds.isEmpty() ||test.isrunningtest()){
            
                list<MRI_Property__c> mriLst = new list<MRI_Property__c>();
                
                string qry = 'select '+commaSepratedFields+ ' from MRI_Property__c where (Sale_Date__c < THIS_QUARTER OR Sale_Date__c = NULL) and id in: mriIds ';
                system.debug('qry'+qry);
                mriLst = dataBase.query(qry);
                system.debug(mriLst.size());
                
                
                for(MRI_Property__c m : mriLst){
                    rowInfo = rowInfo + '\n';
                    i = 0;
                    if(rowInfo.contains(string.valueOf(m.id))){
                        continue;
                    }
                    for(string s : fieldLst){
                        if(i ==0){
                            
                                rowInfo = rowInfo + checkNull(string.valueOf(m.get(s)));
                            
                            i = i+1;
                        }else{
                            
                                rowInfo = rowInfo + ',' +checkNull(string.valueOf(m.get(s)));
                            
                        }
                    }
                }     
                
                
                system.debug('csvFile>>>>'+csvFile);
            }
        }

    }

 

    public void finish(Database.BatchableContext BC){
    
        if(rowInfo == null){
            rowInfo = 'No Records pending for approval';    
        }
    
        if(csvFile == null){
            csvFile = fieldNames + rowInfo;
        }else{
            csvFile = csvFile + rowInfo;
        }
        
        
        if(csvFile1 == null){
            csvFile1 = fieldNames + rowInfo1;
        }else{
            csvFile1 = csvFile1 + rowInfo1;
        }
        
        
        string replyEmailAddress = Label.MRI_Approval_Email;
        Messaging.EmailFileAttachment efa1 = new Messaging.EmailFileAttachment();
        efa1.setFileName('Pending.csv');
        efa1.setBody(blob.valueOf(csvFile));
         
        Messaging.EmailFileAttachment efa2 = new Messaging.EmailFileAttachment();
        efa2.setFileName('Processed.csv');
        efa2.setBody(blob.valueOf(csvFile1));
        
        //User recipient = [SELECT id, firstname FROM User where id = '00550000002tACW' LIMIT 1];
        
        EmailTemplate et = [SELECT id FROM EmailTemplate WHERE developerName = 'MRI_Property_Approval_Details'];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setTemplateID(et.Id); 
        mail.setSaveAsActivity(false);
        mail.setTargetObjectId(system.label.Mass_Approve_Email_ID);
        mail.setReplyTo(replyEmailAddress);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa1,efa2});
        
     
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
        
    }
    
    public static string checkNull(string s){
        string emptyString = '';
        if(s != null){
            if(s.contains(',')){
                s = s.replace(',','');
            }
            return s;
        }else{
            return emptyString;
        }
    }

 

    
}