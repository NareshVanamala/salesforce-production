@isTest private class LC_CustomClauseTypeController_Test
{
    static testmethod void test123() 
    {
    
         test.starttest();
         map<string, string>IdtoLeasevsuitmap= new map<string, String>(); 
         MRI_PROPERTY__c mc= new MRI_PROPERTY__c();
         mc.name='Test_MRI_Property';
         insert mc; 
       
         Lease__c myLease= new Lease__c();
         myLease.name='Test-lease';
         myLease.Tenant_Name__c='Test-Tenant';
         myLease.SuiteId__c='AOTYU';
         mylease.Lease_ID__c='A046677';
         myLease.MRI_PROPERTY__c=mc.id;
         insert mylease;
         
         System.assertNotEquals(mc.name,myLease.name);

                
         LC_LeaseOpprtunity__c lc1= new LC_LeaseOpprtunity__c();
         lc1.name='TestOpportunity';
         lc1.Lease_Opportunity_Type__c='Lease - New';
         lc1.MRI_Property__c=mc.id;
         lc1.Lease__c=mylease.id;
         lc1.Current_Date__c=system.today();
         lc1.LC_OtherOpportunity_Description__c='Thisis my description';
         insert lc1;
         
         
          list<LC_Clauses__c >clist= new list<LC_Clauses__c >();
           LC_Clauses__c Lc_C= new LC_Clauses__c();
           Lc_C.name='TestSnehal';
           Lc_C.Clause_Type__c='TestingSnehal';
           clist.add(Lc_C);
           insert clist;
           
           System.assertNotEquals(lc1.name,Lc_C.name);

           
           ApexPages.StandardController controller = new ApexPages.StandardController(lc1); 
           ApexPages.currentPage().getParameters().put('id',lc1.id);
           LC_CustomClauseTypeController csr=new LC_CustomClauseTypeController(controller);
           csr.searchString='TestSnehal';
           csr.Snooze='Name'; 
           csr.showsearch=true; 
            csr.results=clist;

           csr.getSnoozeradio();
           csr.getSnooze();
           csr.getResults();
           csr.SearchRecord();
           csr.runSearch();
           csr.save();
           csr.Back();
           csr.getContacts();
           csr.AddClause();
           csr.createNewRecord();
           csr.searchString='Hyderbad';
           csr.Snooze='All fields';
           csr.SearchRecord();
           csr.runSearch();
           csr.save();
           //clist=csr.getSnoozeradio();
            LC_CustomClauseTypeController csr1= new  LC_CustomClauseTypeController(); 
           
           LC_CustomClauseTypeController csr11=new LC_CustomClauseTypeController(controller); 
           csr11.showsearch=false;  
           csr11.shownew= false;
           csr11.createNewRecord(); 
    }
    
 }