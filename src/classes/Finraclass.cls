public with sharing class Finraclass{

    Public List<Finra__c> memberList{get;set;}
    private Finra__c deletelist1;
    private Finra__c updatedlist1;
    private List<Finra__c> updatemelist;
    private List<Finra__c> deleteemelist;
    
    private integer removepos{get;set;}
    
    public string conid{get;set;}
    private Id recId{get;set;} 
    
    public string fileName{get;set;} 
    public transient Blob fileBody{get;set;}
    
    public Attachment attachment{get;set;}
    public Attachment myatt{get;set;}
    public string nameFile{get;set;}
    public string decriptionOfFile{get;set;}
    public Blob contentFile{get;set;}
    public Boolean showInputFile{get;set;}
     
    private Case a;  
        public Finraclass(ApexPages.StandardController controller) {
        addrow = new Finra__c();
        //myAttachment = new Attachment();

        a =(Case)controller.getrecord();
        showInputFile = false;
        //a =[select id from Account where id=:ApexPages.currentPage().getParameters().get('id')];
        //a=[select id from Account where id= '001P000000adMsB'];
        memberList =[select name, id,(Select Id,Name,description from Attachments) Attmts,Case__c ,Date_Time__c,Description__c,LastModifiedDate from Finra__c where Case__c=:a.id order by LastModifiedDate DESC];  
        deletelist1= new Finra__c();  
        updatedlist1= new Finra__c();
        updatemelist=new list<Finra__c>();
        deleteemelist=new list<Finra__c>();
        recId= controller.getRecord().id;
        myatt=new attachment();
        
       
    }
    public Pagereference removecon(){
        if(memberList.size()>0)
        {       
            string paramname= Apexpages.currentPage().getParameters().get('node');
            system.debug('Check the paramname'+paramname);
            for(Integer i=0;i<memberList.size();i++)
            {
                if(memberList[i].id==conid)
                {
                    removepos=i;
                    //deletelist1.id=memberList[i].id;
                    deleteemelist.add(memberList[i]);
                }
            }
            if(removepos!=null)
            memberList.remove(removepos);
            //system.debug('check the deletelist..'+deletelist1);
            if(deleteemelist.size()>0)
            {
            
               if (Finra__c.sObjectType.getDescribe().isDeletable()) 
                {
                        delete deleteemelist;
                }         
             deleteemelist.clear();
            }           
        }
        return null;
   }
   
   // Edit Record method ....
   Pagereference p;
   public Id recordId {set;get;}
   public Boolean edit {set;get;}   
   public Pagereference editRecord(){
       recordId = conid;
       edit = true;
       //p = new Pagereference('/apex/AdvancedProgrampage?id='+ApexPages.currentPage().getParameters().get('id'));
       return null;
   }
   
  /* public Pagereference viewRecord(){
       recordId = conid;   
      myatt=[select id, name, parentid from Attachment where parentid=:recordId ];
        system.debug('check the attachement..'+myatt);
       p = new Pagereference('/'+myatt.id);
      // p.setRedirect(true);
       return p;
   }*/
   
   public Pagereference SaveRecord(){
      if(memberList.size()>0)
        {
             for(Integer i=0;i<memberList.size();i++)
            {
                if(memberList[i].id==conid)
                {
                   /*updatedlist1.id= memberList[i].id;
                   updatedlist1.name= memberList[i].name;
                   updatedlist1.Description__c= memberList[i].Description__c;*/
                   updatemelist.add(memberList[i]);   
                }
            }
                 if(updatemelist.size()>0)
                 {
                 update updatemelist;
                 updatemelist.clear();
                 }
        }
       edit = false;
       return null;
       
   }
    //adding new record
    public Boolean addnew{set;get;}
    public Finra__c addrow {set;get;}

    public Pagereference newProgram(){
        attachment =  new Attachment();
        addrow= new Finra__c();
        addnew=true;        
        showInputFile = true;
        return null;    
    }
    public Pagereference savenewProg(){             
        system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>nameFile>>>>>>>>>>>>>>>>>>>>>>>>>>>'+nameFile);
        system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>contentFile>>>>>>>>>>>>>>>>>>>>>>>>>>>'+contentFile);        
        try{            
            addnew=true;
            addrow.Case__c = a.id;
            //System.debug('********description' + addrow.LastModifiedDate);
            //if(addrow.Description__c!= null){
            //   a.Finra__c = addrow.LastModifiedDate;
            //   System.debug('********Finra__c' +addrow.LastModifiedDate );
            //}          
            if((nameFile==null) && (contentFile==null))
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please add attachment'));
            else if(contentFile.size()>5242880)
            {
            system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> >5MB..');
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Attachment size is not greater than 5MB'));            
            }
            else
            {   
                addnew=false;
                if (Schema.sObjectType.Finra__c.isCreateable())

                insert addrow;
                //update a;
                
                attachment.OwnerId = UserInfo.getUserId();
                attachment.ParentId = addrow.Id;// the record the file is attached to
                attachment.Name = nameFile;
                attachment.Description =decriptionOfFile;
                attachment.Body = contentFile;   
                //attachment.IsPrivate = true;
            
                system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>contentFile.Size>>>>>>>>>>>>>>>>>>>>>>>>>>> if '+contentFile.size());
                system.debug('\n myAttachment.name--->'+attachment.name);
                system.debug('check the attachement..'+ attachment);
                
                try
                 {
                    if((attachment.Name!=null) &&(attachment.Body!=null))                               
                    insert attachment;
                    attachment.Body=null;
                    attachment.clear();
                     nameFile=null;
                     contentFile=null;                   
                     memberList =[select name, id,(Select Id,Name,Description from Attachments) Attmts,Case__c,Date_Time__c,Description__c,LastModifiedDate from Finra__c where Case__c=:a.id order by LastModifiedDate DESC];                
                     if(a.parentid != null)
                     {
                       
                     }
                      
                 }
                 catch (DMLException e)
                 {
                    system.debug('check the attachement..'+ attachment);
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
                  }
                return ApexPages.currentPage();
            }           
        }
        catch(Exception e){
            system.debug('Exception --->'+e);
        } 
          
       // pagereference   p = new Pagereference('/'+ApexPages.currentPage().getParameters().get('id')) ;
       contentFile=null;
       nameFile=null;
       system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> End');
       return null;
           
    }
    public Pagereference cancelnewProg(){
        addnew=false;
        return null;    
    }
      
   

    
 }