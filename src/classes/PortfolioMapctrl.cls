global virtual with sharing class PortfolioMapctrl{

    private List<SObject> propertyList;
    private String html;
    public string webready='Approved';
    public string multitenant='%Multi-Tenant%';
    public Map<String, String> stateMap = new Map<String, String>(); 
    public set<string> programid=new set<string>();
    public set<string> RegionstateList123=new set<string>();
    //global PortfolioMapctrl(cms.CoreController stdController) {
    global PortfolioMapctrl() {
        programid.add('ARCP');
        programid.add('CCPT3');
        programid.add('CGPRED');
        programid.add('CVCRE1');
        programid.add('STFRED');
        programid.add('STFRE2');
        programid.add('CGPRE2');
        programid.add('MS1P7');
       
     Map<String, Website_States__c> websiteStatesCustSetting = Website_States__c.getAll();
        List<String> stateshrtname = new List<String>();
        stateshrtname.addAll(websiteStatesCustSetting.keySet());
        stateshrtname.sort();
        for(string stateshort:stateshrtname){
            Website_States__c websiteStates = websiteStatesCustSetting.get(stateshort);
                stateMap.put(websiteStates.State__c,websiteStates.State_Full_Name__c);
        }
    system.debug('******stateMap*****'+stateMap);
    } 
    public list<string> regionBasedStates(string region){
        string Regionstates ='';
        list<string> RegionstateList =new list<string>();
        Map<String, Portfolio_Region__c> regionCustSetting = Portfolio_Region__c.getAll();
        List<String> regionNames = new List<String>();
        regionNames.addAll(regionCustSetting.keySet());
        regionNames.sort();
        for(string regioname:regionNames){
            Portfolio_Region__c selRegion = regionCustSetting.get(regioname);
            if(region==selRegion.Region__c){
                Regionstates =selRegion.States__c;
                
              }
        }
        string [] RegionstateListSplit = Regionstates.split(',');
        for(string stateName: RegionstateListSplit){
            RegionstateList.add(stateName);
        }
        return RegionstateList;
    }
    public Integer getResultTotal(string region,String state,String tenantIndustry, String tenant, String[] filters){
        
        String query;
        String Pre;
        list<string> RegionstateList =new list<string>();
        System.Debug('region123'+region);
        System.Debug('programid123'+programid);
        //Portfolio_Region__c regionCustSetting = new Portfolio_Region__c();
        if(region!=null && region!='All' && region!='undefined' && region!=''){
            for(string selStates : regionBasedStates(region)){
                RegionstateList.add(selStates);
             }
        }
        System.Debug('*****RegionstateList*****'+RegionstateList);
        System.Debug('programid123'+programid);
   
        query = 'SELECT COUNT() FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Program_Id__c in: programid  and Status__c=\'Owned\'';
        
        if(!RegionstateList.isempty()){
         query += ' AND State__c in: RegionstateList';
        }
        
        system.debug('******query*******'+query);
        if(state != null && state != '' && state != 'all'){
            state = state.escapeEcmaScript();
            query += ' AND State__c = \'' + state + '\'';
        }

        if(tenantIndustry != null && tenantIndustry != '' && tenantIndustry != 'all'){
            tenantIndustry = tenantIndustry.escapeEcmaScript();
            query += ' AND Sector__c = \'' + tenantIndustry + '\'';
        }

        if(tenant != null && tenant != '' && tenant != 'all'){
            tenant = tenant.escapeEcmaScript();
            query += ' AND Web_Name__c = \'' + tenant + '\'';
        }

        if(filters != null && filters.size() > 0){
            query += ' AND ( ';
            Boolean first = true;
            for(String f: filters){
                if(first){
                    first=false;
                }else{
                    query += ' OR ';
                }

                query += 'Property_Type_Group__c = \'' + f + '\'';

            }
            query += ' )';
        }
        system.debug('getresulttotal'+query );
        return Database.countQuery(query);

    }
   public List<SObject> getgroupbyStates(string region,String state,String tenantIndustry, String tenant, String[] filters){
        
        String query;
        list<string> RegionstateList =new list<string>();
        System.Debug('region123'+region);
      if(region!=null && region!='All' && region!='undefined'&& region!=''){
            for(string selStates : regionBasedStates(region)){
                RegionstateList.add(selStates);
             }
        }
        query = 'SELECT COUNT(id) TotalStates,State__c FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Program_Id__c in: programid  and Status__c=\'Owned\'';
        
        if(!RegionstateList.isempty()){
            query += ' AND State__c in : RegionstateList';
        }
        system.debug('******query*******'+query);
        if(state != null && state != '' && state != 'all'){
            state = state.escapeEcmaScript();
            query += ' AND State__c = \'' + state + '\'';
        }
        if(tenantIndustry != null && tenantIndustry != '' && tenantIndustry != 'all'){
            tenantIndustry = tenantIndustry.escapeEcmaScript();
            query += ' AND Sector__c = \'' + tenantIndustry + '\'';
        }

        if(tenant != null && tenant != '' && tenant != 'all'){
            tenant = tenant.escapeEcmaScript();
            query += ' AND Web_Name__c= \'' + tenant + '\'';
        }

        if(filters != null && filters.size() > 0){
            query += ' AND ( ';
            Boolean first = true;
            for(String f: filters){
                if(first){
                    first=false;
                }else{
                    query += ' OR ';
                }

                query += 'Property_Type_Group__c = \'' + f + '\'';

            }
            query += ' )';
        }
            query += ' group by State__c';
            system.debug('groupbystates'+query );
            return Database.query(query);

    }
   public List<SObject> getPropertyList(string region,String state,String tenantIndustry, String tenant, String[] filters){

        List<SObject> propertyList;
        String query;
        list<string> RegionstateList =new list<string>();
        System.Debug('region123'+region);
           if(region!=null && region!='All' && region!='undefined'&& region!=''){
            for(string selStates : regionBasedStates(region)){
                RegionstateList.add(selStates);
             }
        }
        query = 'SELECT Id,Name,Web_Name__c, City__c,Property_Type__c, State__c,SqFt__c FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Program_Id__c in: programid and Status__c = \'Owned\'';
        
         if(!RegionstateList.isempty()){
            query += ' AND State__c in :RegionstateList';
        }
         system.debug('******query1*******'+query);
        if(state != null && state != '' && state != 'all'){
            state = state.escapeEcmaScript();
            query += ' AND State__c = \'' + state + '\'';
        }

        if(tenantIndustry != null && tenantIndustry != '' && tenantIndustry != 'all'){
            tenantIndustry = tenantIndustry.escapeEcmaScript();
            query += ' AND Sector__c = \'' + tenantIndustry + '\'';
        }

        if(tenant != null && tenant != '' && tenant != 'all'){
            tenant = tenant.escapeEcmaScript();
            query += ' AND Web_Name__c= \'' + tenant + '\'';
        }

        if(filters != null && filters.size() > 0){
            query += ' AND ( ';
            Boolean first = true;
            for(String f: filters){
                if(first){
                    first=false;
                }else{
                    query += ' OR ';
                }

                query += 'Property_Type_Group__c = \'' + f + '\'';

            }
            query += ' )';
        }
        System.Debug('query123456'+query );
       
        propertyList = Database.query(query);
        System.Debug('propertyList123'+propertyList );
       
        return propertyList;
    }
    
  
    public String getStateList(string region){

        List<SObject> stateList;
        List<String> outList = new List<String>();
        String response = '';
        Boolean first = true;
        list<string> RegionstateList =new list<string>();
        System.Debug('state12345612'+region);
        if(region!=null && region!='All' && region!='undefined'&& region!=''){
            for(string selStates : regionBasedStates(region)){
                RegionstateList.add(selStates);
             }
        }
        System.Debug('RegionstateList1111'+RegionstateList);
        String query = 'SELECT State__c FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Program_Id__c in: programid and Status__c = \'Owned\''; 
        
        if(!RegionstateList.isempty()){
            query += ' AND State__c in : RegionstateList';
        }
        
        query += ' GROUP BY State__c ORDER BY State__c ASC';
        system.debug('state123456'+query );
        stateList = Database.query(query);
        system.debug('stateList123'+query );
        stateList = sanitizeList(stateList, 'State__c');

        response+='[';

        for(SOBject s : stateList){
            if(first){
                first=false;
            }else{
                response+=', ';
            }
            response+='{"short":"' + String.valueOf(s.get('State__c')) + '", ';
            if(stateMap.get(String.valueOf(s.get('State__c'))) != null)
                response+='"long":"' + stateMap.get(String.valueOf(s.get('State__c'))) + '"}';
            else
                response+='"long":"' + String.valueOf(s.get('State__c')) + '"}';
        }

        response+=']';
        system.debug('state123456'+response);
        return response;

    }
    
     
    public List<SObject> getIndustryList(string region){

        List<SObject> industryList;
        list<string> RegionstateList =new list<string>();
        System.Debug('region123'+region);
       if(region!=null && region!='All' && region!='undefined'&& region!=''){
            for(string selStates : regionBasedStates(region)){
                RegionstateList.add(selStates);
             }
        }
    
        String query = 'SELECT Sector__c FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Program_Id__c in: programid and Status__c = \'Owned\''; 
        if(!RegionstateList.isempty()){
            query += ' AND State__c in : RegionstateList';
        }
        query += ' GROUP BY Sector__c ORDER BY Sector__c ASC';
        system.debug('industry123'+query);
        industryList = Database.query(query);
        system.debug('industry123'+query);
        return sanitizeList(industryList, 'Sector__c');

    }
    
    public List<SObject> getTenantList(string region){

       List<SObject> tenantList;
        list<string> RegionstateList =new list<string>();
        System.Debug('region123'+region);
        if(region!=null && region!='All' && region!='undefined'&& region!=''){
            for(string selStates : regionBasedStates(region)){
                RegionstateList.add(selStates);
             }
        }
 
       string query;
       query = 'SELECT Website_Tenant_Name__c FROM Web_Properties__c WHERE Web_Ready__c=\'' + webready + '\' and Program_Id__c in: programid and Status__c = \'Owned\''; 
      if(!RegionstateList.isempty()){
            query += ' AND State__c in : RegionstateList';
        }
       
       //query +=' and (NOT Property_Type_Group__c LIKE \'%Anchored Shopping Centers%\') GROUP BY Website_Tenant_Name__c';
       query +=' GROUP BY Website_Tenant_Name__c';
        system.debug('tenant123'+query);     
       tenantList = Database.query(query);
       return sanitizeList(tenantList, 'Website_Tenant_Name__c');

    }
 
    private List<SObject> sanitizeList(List<SObject> inList, String field){

        Integer ctr = 0;

        while(ctr < inList.size()){
            if(inList[ctr].get(field) == null){
                inList.remove(ctr);
            } else {
                ctr++;
            }
        }
        return inList;

    }
 

}