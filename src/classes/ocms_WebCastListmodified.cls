global virtual with sharing class ocms_WebCastListmodified extends cms.ContentTemplateController implements cms.ServiceInterface
{
    private List<SObject> ProductList;
    private List<SObject> ProductList1;
    private String html;
   
   /* Service methods */
    public List<SObject> getProductList()
    {
        List<SObject> ProductList;
        set<id> cmpid=new set<id>();
        user usr = [Select Id, firstname, lastname, contactId from user where id=:UserInfo.getUserId()];
        list<CampaignMember> cm=[select id,CampaignId,ContactId,Status from CampaignMember where ContactId=:usr.contactid];                
        for(CampaignMember cm1:cm)
        {
        cmpid.add(cm1.CampaignId);
        } 
         String query = 'select Join_Webcast__c,Title__c,Add_to_Calender__c,Is_Webcast__c,Is_Event__c,Is_Webcast_Replay__c,id,Campaigns__c,Contacts__c,Description__c,Email_Address__c,iCalendar_URL__c,meeting_ID__c,Name__c,Start_Date_Time__c,Status__c,Viewble_on_website__c,Webcast_Replay_URL__c,WebEx_Join_URL__c, Name from WebCast__c where Campaigns__c IN:cmpid AND Is_Webcast_Replay__c=false AND Is_Webcast__c=true';
     
        ProductList= Database.query(query );
        return ProductList;
       
    }
     public List<SObject> getProductList1()
    {
        List<SObject> ProductList1;
        list<WebCast__c>ctlist;
        Contact C;
        String type='Webcast';
        list<string>campaignidlist=new list<String>();
        set<id> cmpid=new set<id>();
        user usr = [Select Id, firstname, lastname, contactId from user where id=:UserInfo.getUserId()];
        list<CampaignMember> cm=[select id,CampaignId,ContactId,Status from CampaignMember where ContactId=:usr.contactid];                
        for(CampaignMember cm1:cm)
        {
        cmpid.add(cm1.CampaignId);
        } 
        String query1 = 'select Join_Webcast__c,Title__c,Add_to_Calender__c,Is_Webcast__c,Is_Event__c, Is_Webcast_Replay__c,id,Campaigns__c,Contacts__c,Description__c,Email_Address__c,iCalendar_URL__c,meeting_ID__c,Name__c,Start_Date_Time__c,Status__c,Viewble_on_website__c,Webcast_Replay_URL__c,WebEx_Join_URL__c, Name from WebCast__c where Campaigns__c IN:cmpid AND Is_Webcast_Replay__c=true AND Is_Webcast__c=true';
        ProductList1= Database.query(query1);
        return ProductList1;
       
    }
    
  
    @TestVisible private List<SObject> sanitizeList(List<SObject> inList, String field)
    {
        Integer ctr = 0;
        while(ctr < inList.size())
        {
            if(inList[ctr].get(field) == null)
            {
                inList.remove(ctr);
            } 
            else 
            {
                ctr++;
            }
         }
         return inList;
    }
    
    global String executeRequest(Map <String, String> p) {
      String response = '{"success": false}';
      if (p.get('action') == 'getUpcomingWebcastCount') {
        Integer newMessagesCount = 0;
        newMessagesCount = getUpcomingWebcastCount();
        response = '{"success": true, "count":' + newMessagesCount + ' }';
      }
      return response;
    }
    
   @TestVisible private Integer getUpcomingWebcastCount(){
      ProductList= getProductList();
      Integer count = ProductList.size();
      return count;
    }
    
    global System.Type getType() { return ocms_PortalMessage.class;}
    
      /* Create Content */
   @TestVisible private String writeControls()
    {
        String html = '';
        return html;
     }
    @TestVisible private String writeListView()
     {
         String html = '';
         html += '<article class="topMar7 wrapper teer3">'+
          '<div class="container">'+
          '<ul class="tabs">'+
          '<li><a href="#tab1">Webcast </a></li>'+
          '<li><a href="#tab2">Webcast Replay</a></li>'+
          '</ul>'+
          '<div class="tab_container">'+
          '<div class="tab_content" id="tab1">'+
          '<h1>Upcoming Webcast</h1>'+
                '<table class=" table table-border clear web-table" width="100%">'+
                '<tbody>'+
                '<tr>'+
                '<td width="20%">'+
                 '<strong>Date / Time</strong>'+
                  '</td>'+
                   '<td width="20%">'+
                    '<strong>Title</strong>'+
                     '</td>'+
                      '<td width="50%">'+
                      '<strong>Description</strong>'+
                       '</td>'+
                      '<td width="15%">'+
                       '<strong>Status</strong>'+
                        '</td>'+
                        '<td width="20%">'+
                        '<strong>&nbsp;</strong>'+
                        '</td>'+
                        '<td width="20%">'+
                        '<strong>&nbsp;</strong>'+
                        '</td>'+
                        '</tr>';
                        
         if (ProductList!= null && ProductList.size() > 0)
         {
         Map<string,string> webexjoinmap=new map<string,string>(); 
         Map<string,string> cmmemstatus=new map<string,string>(); 
         set<id> cmpid=new set<id>();
         user usr = [Select Id, firstname, lastname, contactId from user where id=:UserInfo.getUserId()];
         list<CampaignMember> cm=[select id,CampaignId,ContactId,Status from CampaignMember where ContactId=:usr.contactid];                
         for(CampaignMember cm1:cm)
         {
         cmpid.add(cm1.CampaignId);
         cmmemstatus.put(cm1.CampaignId,cm1.status);
         } 
         list<Campaign> cm11=[select id,WebEx_Join_URL__c,WebEx_Replay_URL__c from Campaign where id in:cmpid];
         for(Campaign cm2:cm11)
         {
         webexjoinmap.put(cm2.Id,cm2.WebEx_Join_URL__c);
         }   
         for(SObject pl : ProductList)
             {
                       String myid= String.valueOf(pl.get('id'));
                       string myid1=webexjoinmap.get(string.valueof(pl.get('Campaigns__c')));
                       string reg=cmmemstatus.get(String.valueOf(pl.get('Campaigns__c')));
                       html += '<tr>'+
                            '<td>'+pl.get('Start_Date_Time__c')+'</td>'+
                            '<td>'+pl.get('Title__c')+'</td>'+
                            '<td>'+pl.get('Description__c')+'</br><h3><a href="/Webcast_detail?id='+myid+'">View Details</a></h3></td>'+
                            '<td>'+cmmemstatus.get(String.valueOf(pl.get('Campaigns__c')))+'</td>';
                            if(reg!='Registered')
                            {
                             html += '<td>'+'<input class="webcast-button" type="button" value="Register" name="Register" onclick="Register(\''+myid+'\');"/>'+'</td>';
                            }
                            
                            else if((reg=='Registered')&&(pl.get('Join_Webcast__c')==true))
                            {
                               html += '<td>'+'<a class="webcast-button" href="'+myid1+'" target="_blank">Join Webcast</a>'+'</td>';
                            
                            }
                            
                            else if((reg=='Registered')&&(pl.get('Add_to_Calender__c')==true))
                            {
                               html += '<td>'+'<a class="webcast-button" href="/ocmsicsattachmenttest?str='+myid+'" target="_blank"> Add to Calendar </a>'+'</td>';
                            
                            }
                            html += ' </tr>';
             }

         }
                  html += ' </tbody>'+
                ' </table>'+
                ' </div>'+
                '<div class="tab_content" id="tab2">'+
                '<h1>Webcast Replays</h1>'+
                '<table class="table table-border clear web-table" width="100%">'+
                '<tbody>'+
                '<tr>'+
                '<td width="20%">'+
                 '<strong>Date / Time</strong>'+
                  '</td>'+
                  '<td width="20%">'+
                    '<strong>Title</strong>'+
                     '</td>'+
                   '<td width="50%">'+
                    '<strong>Description</strong>'+
                     '</td>'+
                      '<td width="15%">'+
                       '<strong>Status</strong>'+
                        '</td>'+
                        '<td width="20%">'+
                        '<strong>&nbsp;</strong>'+
                        '</td>'+
                        '<td width="20%">'+
                        '<strong>&nbsp;</strong>'+
                        '</td>'+
                        '</tr>';
         if (ProductList1!= null && ProductList1.size() > 0)
         {
         Map<string,string> webexreplaymap=new map<string,string>(); 
         Map<string,string> cmmemstatus=new map<string,string>(); 
         set<id> cmpid=new set<id>();
         user usr = [Select Id, firstname, lastname, contactId from user where id=:UserInfo.getUserId()];
         list<CampaignMember> cm=[select id,CampaignId,ContactId,Status from CampaignMember where ContactId=:usr.contactid];                
         for(CampaignMember cm1:cm)
         {
         cmpid.add(cm1.CampaignId);
         cmmemstatus.put(cm1.CampaignId,cm1.status);
         } 
         list<Campaign> cm11=[select id,WebEx_Join_URL__c,WebEx_Replay_URL__c from Campaign where id in:cmpid];
         for(Campaign cm2:cm11)
         {
         webexreplaymap.put(cm2.Id,cm2.WebEx_Replay_URL__c);
         }
             for(SObject pl : ProductList1)
             {
              String myid= String.valueOf(pl.get('id'));
              String myid2= webexreplaymap.get(string.valueof(pl.get('Campaigns__c')));
                        html += '<tr>'+
                            '<td>'+pl.get('Start_Date_Time__c')+'</td>'+
                            '<td>'+pl.get('Title__c')+'</td>'+
                            '<td>'+pl.get('Description__c')+'</td>'+
                            '<td>'+cmmemstatus.get(String.valueOf(pl.get('Campaigns__c')))+'</td>'+
                           '<td>'+'<a class="webcast-button" href="'+myid2+'">View Replay</a>'+'</td>'+
                           ' </tr>';
             }

         }
                  html += ' </tbody>'+
                ' </table>'+
                ' </div>'+
                ' </div>'+
                ' <p>&nbsp;</p>'+
                '</div>'+
                '</article>';
                         
         return html;
        }
        
       @TestVisible private String writeMapView(){
         return '';
        }
       
        
        global override virtual String getHTML()
        {
            String html = '';
            
            html += writeControls();
            ProductList= getProductList();
            ProductList1=getProductList1();
            html += writeListView();
            html += '<script src="/resource/ocms_WebPropertyList/js/ocms_Webcastlist.JS" type="text/javascript"></script>';
            return html;
        }

}