global class ClassForSendingInvestorlist implements Database.Batchable<SObject>
{
   global String Query;
   global String Templateid;
   global set<id>parentidset;
   global list<Attachment>attachlist;
   global set<id>contactidset;
   //global String currentactionplan;
   //global string dealidimp;
   //global String portfolioname;
    global Map<Id, List<Attachment>> parentattachementMap  = new Map<Id, List<Attachment>>();
    global Map<Id, List<Investor_Attachment__c >> parentInverortMap  = new Map<Id, List<Investor_Attachment__c >>();
    global map<id,id> contactidmap= new map<id,id>();
    global list<Contact>scope;
    global string Type;
    global string subtype;
    global list<Investor_Attachment__c >contactInvestorlist;
    global id contactid;
   
   global ClassForSendingInvestorlist(ID Campaignid  )  
   {  
        contactInvestorlist= new list<Investor_Attachment__c >();
        parentidset= new set<id>();
        attachlist=new list<Attachment>();
        
        Campaign currentcampaign=[select id from Campaign where id=:Campaignid]; 
        list<CampaignMember>cmblist=[select id,campaignid,ContactId from CampaignMember where campaignid=:Campaignid];
        system.debug('This is my campaign'+currentcampaign+'This is id'+Campaignid); 
        contactidset= new set<id>();  
        
         for(CampaignMember cmb:cmblist)
           contactidset.add(cmb.ContactId); 
           
           system.debug('Get me the set size'+contactidset);
        
                          
        Investment_List_Email_details__c pdcemail=Investment_List_Email_details__c.getvalues('Email Attachment Details');
        system.debug('This is it'+pdcemail);
         if(pdcemail!=null)
         {
            Type=pdcemail.Attachment_Type__c;
            subtype= pdcemail.Attachment_SubType__c;
            Templateid=pdcemail.Email_Template_Id__c;
          }
        //EmailTemplate emptempl=[SELECT Id,IsActive,Name,Subject FROM EmailTemplate where id=:Templateid];
          Query='select id,Sending_Investorlist_complete__c,Send_Investorlist_attachment__c,Trigger_Investment_Email__c,Internal_Wholesaler__c from contact where id in:contactidset'; 
         //return database.getQueryLocator(query);   
         system.debug('What is the problem'+Query);
      
     }
     
      global Database.QueryLocator start(Database.BatchableContext BC)  
    {  
      system.debug('This is start method');
      system.debug('Myquery is......'+Query);
      return Database.getQueryLocator(Query);  
    }  
      global void execute(Database.BatchableContext BC,List<Contact> scope)  
      { 
         //first delet the attachment list of the email template
           system.debug('check me'+scope.size());
           list<Attachment>Deleteattacemplist=new list<Attachment>();
           Deleteattacemplist=[select Body,BodyLength,ContentType,Description,Id,Name,OwnerId,ParentId FROM Attachment where ParentId=:Templateid]; 
          
          //system.debug('First check'+Deleteattacemplist);
          if(Deleteattacemplist.size()>0)
           delete Deleteattacemplist;
           
            //system.debug('First check'+Deleteattacemplist.size());
         //finished deleting the atatchmentlist from emial template  
          
          
          list<Attachment>Insertlist=new list<Attachment>();
          list<Attachment>fetchattachsertlist=new list<Attachment>();
         
          list<Investor_Attachment__c >scope1=new list<Investor_Attachment__c >();
          list<Contact>Updatelist= new list<Contact>();
          list<Contact>fetchinglist= new list<Contact>();
          list<Investor_Attachment__c >fetchingInverorlistlist= new list<Investor_Attachment__c >();
          set<id>fetchset=new set<id>(); 
          set<id>cuidert=new set<id>();
        
          for(Contact cri:scope)
            cuidert.add(cri.id);
            
         string interwholesaler=scope[0].Internal_Wholesaler__c; 
         system.debug('This is the name of the Internal wholealer'+interwholesaler);
         User U= [select id , name from user where name=:interwholesaler];
         system.debug('This is my user'+U);
       
          scope1=[select Attachment_Id__c,Contact__c,Id,Name,Sub_Type__c,Type__c FROM Investor_Attachment__c where Contact__c in:cuidert and Sub_Type__c=:subtype and Type__c=:Type];
          //get the contacts to update and invester attachmnt list to copy attachments.
         for(Investor_Attachment__c Acl : scope1)
              parentidset.add(Acl.id); 
           //got the attachements of each Investor
           attachlist=[SELECT Body,BodyLength,ContentType,Description,Id,Name,OwnerId,ParentId FROM Attachment where ParentId in:parentidset];
         //system.debug('Attachmentlist'+attachlist);
         //let form the map of aattachment and invsetor attachment
         if(attachlist.size()>0)
        {
           for(Attachment acty:attachlist)
            {
                  attachment a = new attachment();
                  a.name=acty.name;
                  a.body=acty.body;
                  a.parentid=Templateid;
                  insertlist.add(a);  
              }
          }
          if(insertlist.size()>0)
          insert  insertlist;
      //now trigger the workflow rule.
          
           for(Contact cri:scope)
           {
              cri.Trigger_Investment_Email__c =true;
              Updatelist.add(cri);
         
           }
          update  Updatelist;  
          
       //now set the sending complete flag true.   
        list<contact>updatelist1=new list<contact>();
        
          for(Contact cri:scope)
         {
           cri.Sending_Investorlist_complete__c=true;
           updatelist1.add(cri);
         
         } 
          
          update updatelist1;
          list<task>tlist=new list<Task>();
           for(Contact cri:scope)
         {
           Task t=new Task();
           t.subject='Sent Investor list';
           t.status='completed';
           t.whoid=cri.id;
           t.ownerid=Userinfo.getUserid();
           tlist.add(t);
         } 
          insert tlist;
          
          
     }     
    global void finish(Database.BatchableContext BC)  
    { 
      //  AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
               
       
       
                
          
    }
    
  }