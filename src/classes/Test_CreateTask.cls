@isTest
private class Test_CreateTask{
   static testMethod void Testcreatetask() {
    test.startTest();
   Deal__c d =new Deal__c();
   d.name = 'deal';
   d.Property_Type__c = 'Retail';
   d.Tenancy__c= 'Multi-Tenant';
   d.Primary_Use__c = 'Anchor';
   insert d;
    
    Template__c temp =new Template__c();
        temp.name = 'test';
        temp.deal__c = d.id;
        insert temp;
    
    Template__c temp1 =new Template__c();
        temp1.name = 'test';
        temp1.deal__c = d.id;
        temp1.Mass_Assign_To__c = '00550000001CetM';
        insert temp1;
     system.assertequals(temp1.deal__c, d.id);
    
    Task_Plan__c ct= new Task_Plan__c();
    ct.Date_Completed__c= Date.newInstance(1999,12, 12);
    ct.Date_Needed__c= Date.newInstance(1999,12, 12);
    ct.Date_Ordered__c= Date.newInstance(1999,12, 12);
    ct.Date_Received__c= Date.newInstance(1999,12, 12);
    ct.Priority__c='Medium';
    ct.Status__c = 'completed';
    ct.Template__c = temp1.id;
    ct.Assigned_To__c='00550000001CetM';
    insert ct;
   
    Task T = new Task();
   
    T.Date_Completed__c= ct.Date_Completed__c;
    T.Date_Needed__c=ct.Date_Needed__c;
    T.Date_Ordered__c=  ct.Date_Ordered__c;
    T.Date_Received__c= ct.Date_Received__c;
    T.Priority=ct.Priority__c;
    T.Status = ct.Status__c;
    T.Action_Plan_Task__c=ct.id;
    
   Insert t;
   
   
    Task_Plan__c ct1= new Task_Plan__c();
    ct1.Date_Completed__c= Date.newInstance(2000,12, 12);
    ct1.Date_Needed__c= Date.newInstance(2000,12, 12);
    ct1.Date_Ordered__c= Date.newInstance(2000,12, 12);
    ct1.Date_Received__c= Date.newInstance(2000,12, 12);
    ct1.Priority__c='High';
    ct1.Status__c = 'completed';
    ct1.Template__c = temp.id;
     
    //ct1.id='a3TW00000008rL2';
    ct1.Assigned_To__c='00550000001CetM';
    insert ct1;
    delete ct;
    
     ct1.Status__c = 'In Progress';
   update ct1;
   
    Task T1 = new Task();
  
    T1.Date_Completed__c= ct1.Date_Completed__c;
    T1.Date_Needed__c=ct1.Date_Needed__c;
    T1.Date_Ordered__c=  ct1.Date_Ordered__c;
    T1.Date_Received__c= ct1.Date_Received__c;
    T1.Priority=ct1.Priority__c;
    T1.Status = ct1.Status__c;
    T1.Action_Plan_Task__c=ct1.id;
   Insert T1;
   
   
        system.assertequals(T1.Action_Plan_Task__c,ct1.id);

   test.stopTest();
   
    }
    }