global virtual without sharing class ocms_DailyNAVList extends cms.ContentTemplateController{

    private List<SObject> DailyNAVList;
    private String html;
   
       
    public List<SObject> getDailyNAVList(String sharetype, String period){

        List<SObject> DailyNAVList;
        String query;
        Date d1;
        if(period == '1 Month')
        {
        d1=system.today().addMonths(-1);
        }
        if(period == '3 Months')
        {
        d1=system.today().addMonths(-3);
        }
        if(period == '6 Months')
        {
        d1=system.today().addMonths(-6);
        }
        if(period == '1 Year')
        {
        d1=system.today().addMonths(-12);
        }
        if(period == 'Year to Date')
        {
        integer i=system.today().year();
        d1= Date.newInstance(i, 1, 1);
        }
        string dailyNav='DailyNav';
        query = 'SELECT Id, Date_As_Of__c,Createddate_Year__c, Createddate_Month__c, Createddate_Day__c, Name,Daily_NAV__c,Type_of_Share__c, Date_Paid__c, Daily_Distribution__c, Net_Asset_Value__c, Annualized_Yield__c FROM Distributions__c where Category__c=\'' + dailyNav + '\'';
        
       
      if(sharetype!= null && sharetype!= ''){
            query += ' AND Type_of_Share__c = \'' + sharetype + '\'';
        }

         if(sharetype!= null && sharetype!= '' && period=='Since Inception'){
            query += ' AND Type_of_Share__c= \'' + sharetype + '\'';
        } 
        
         if(period!= null && period!= '' && period!= 'Since Inception'){
            query += ' AND  Date_As_Of__c>=:d1';
        }

            query +=' order by Date_As_Of__c ASC'; 
            
        DailyNAVList= Database.query(query);
        return DailyNAVList;
    }
     
    @TestVisible private String writeControls(){
        String html = '';
          
        html+='<article class="topMar7 wrapper teer3"><h1>DAILY NAV</h1>'+
              '<div class="clear">'+
              '<select class="ocms_sharetype">'+
              '<option selected="selected" value="Class W Shares">Class W Shares</option>'+
              '<option value="Class I Shares">Class I Shares</option>'+
              '<option value="Class A Shares">Class A Shares</option>'+
              '</select></div>';
              
        html+='<div class="ocms_selectedshare" style="display:block"></div>'; 
        
        html+='<div class="sub-title boxNav" id="" style="float:left; width:250px; background:#f0f0f0; border:solid 2px #999; padding:10px; font-size:14px; color:#333; line-height:30px;"></div>';
        
        html+='<div class="ocms_selectedshare1" style="display:block"></div>';   
           
        html+='<div class="timeDropDown">Time Period &nbsp;<select class="ocms_period" onchange="zoomChart()">'+
              '<option selected="selected" value="Since Inception">Since Inception</option>'+
              '<option value="Year to Date">Year to Date</option>'+
              '<option value="1 Month">1 Month</option>'+
              '<option value="3 Months">3 Months</option>'+
              '<option value="6 Months">6 Months</option>'+
              '<option value="1 Year">1 Year</option>'+
              '</select></div>';
                 
        html+='<div style="float:left !important; width:100%">'; 
        
        html+='<div id="chartContainer1" style="width:100%; height: 400px; margin: 0 auto; max-width:66em !important;">&nbsp;</div>';
        
        html+='<div class="clear">&nbsp;</div> <p>The net asset value per share is shown as of the close of business on the date specified. Purchases or redemptions received in good order on that date before the close of business were processed at the net asset value per share. Purchases or redemptions received in good order on a subsequent business day before the close of business will be processed at the net asset value shown per share as of the close of business on that subsequent business day. See the prospectus for additional information.<br />'+
                '<br />' +
                'As of September 30, 2014, our NAV was $126,899,673. As of October 1, 2014, the redemption limit for the quarter ending December 31, 2014 was 10% of our NAV as of September 30, 2014. Given that sales of our common stock have exceeded redemption requests quarter to date, the redemption limit as of October 31, 2014 has not been reduced below 10% of our NAV as of September 30, 2014. The redemption limit will be updated by us on the first business day of each month or sooner if deemed necessary by us. See &quot;Risk Factors&quot; in the prospectus, as supplemented, for the limitations and risks associated with our redemption plan, including the risk that the redemption plan may have insufficient liquidity to meet all redemption requests..<br />'+
                '<br />' +
                '* Cole Income NAV is designed to provide daily liquidity to investors under most investing conditions. Each calendar quarter, net redemptions are generally limited to 5% of total net assets at the end of the previous quarter. If net redemptions reach the five percent limit, no further redemptions will be processed that quarter. We will begin accepting redemption requests again on the first business day of the next calendar quarter, but will apply the five percent quarterly limitation on redemptions on a per-stockholder basis, instead of a first-come, first-served basis. The Board of Directors has discretion to further limit or suspend redemptions, if in the best interests of shareholders of Cole Income NAV.<br />'+
                '<br />'+
                'Our net asset value, as determined by our fund accountant (&ldquo;NAV&rdquo;), is not prepared in accordance with Generally Accepted Accounting Principles (&ldquo;GAAP&rdquo;). Investors should refer to our financial statements and accompanying footnotes and disclosure under the heading &ldquo;Management&rsquo;s Discussion and Analysis of Financial Condition and Results of Operations&rdquo;, in the accompanying prospectus, as supplemented, for our net book value on a per share basis in accordance with GAAP, which our stockholders&rsquo; equity divided by shares outstanding as of the date of measurement. Our NAV is calculated using a detailed set of valuation methodologies, as described under the heading &ldquo;Valuation Policies&rdquo; in the accompanying prospectus, as supplemented. When the fair value of our assets and liabilities is calculated for the purposes of determining our NAV per share, the calculation is done using the fair value methodologies detailed within the FASB Accounting Standards Codification under Topic 820, Fair Value Measurements and Disclosures.<br />'+
                '<br />'+
                'There are certain factors which cause NAV to be different from net book value on a GAAP basis. Most significantly, for NAV purposes our real estate properties and real estate liabilities, which are the largest components of NAV, are valued by our independent valuation expert on a rolling quarterly basis, or sooner if an event occurs which many materially impact the previously provided value of the affected asset. For GAAP purposes, these assets and liabilities are generally recorded at depreciated or amortized cost. Other factors, including straight-lining of rent for GAAP purposes and the treatment of acquisition related expenses and organization and offering costs, will cause our GAAP net book value to be different from our NAV.<br />'+
                '<br />'+
                'No rule or regulation mandates the manner for calculating NAV. While our NAV calculation methodologies are consistent with standard industry practices for valuing private real estate funds, they involve significant professional judgment in the application of both observable and unobservable attributes, and there is no established practice among publicly-offered REITs, listed or unlisted, for calculating NAV. As a result, our methodologies or assumptions may differ from other REITs&rsquo; methodologies or assumptions.</p>';
 
               
         html+='</div></article>';   
         html+='<style type="text/css">.boxNav{ float:left; width:250px; background:#f0f0f0; border:solid 2px #999; padding:10px; font-size:14px; color:#333; line-height:30px;}.boxNav strong{ font-size:34px;}'+
'</style>';
                     
        return html;
    }
    
    global override virtual String getHTML(){
        String html = '';
        
        
        html += writeControls();
        html += '<script src="/resource/ocms_DailyNAVList" type="text/javascript"></script>';
        return html;
    }
   

}