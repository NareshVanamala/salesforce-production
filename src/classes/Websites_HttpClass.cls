public with sharing class Websites_HttpClass {
    
      public static final String METHOD_GET= 'GET';
      public static final String METHOD_POST= 'POST';
      public static final Integer TIMEOUT_MAX= 120000;
      public static final String ACCEPT= system.label.Websites_Drupal_Version;
      public static final String COLECAPITAL_DRUPAL_TOKEN= system.label.Websites_Drupal_Token;
      
      public static void insertDrupalEnpointURLsForTest(){
          List<Websites_Drupal_Endpoint_URLs__c> eList = new List<Websites_Drupal_Endpoint_URLs__c>();

          
             Websites_Drupal_Endpoint_URLs__c e1= new Websites_Drupal_Endpoint_URLs__c(name= 'Cole Capital Properties_Production', End_Point_URL__c='http://www.colecapital.com/vereit/api/property');
             Websites_Drupal_Endpoint_URLs__c e2= new Websites_Drupal_Endpoint_URLs__c(name= 'Cole Capital Properties_Stage', End_Point_URL__c='http://uat.colecapital.com/vereit/api/property');
             Websites_Drupal_Endpoint_URLs__c e3= new Websites_Drupal_Endpoint_URLs__c(name= 'Cole Capital Contacts_Production', End_Point_URL__c='http://www.colecapital.com/vereit/api/contact');
             Websites_Drupal_Endpoint_URLs__c e4= new Websites_Drupal_Endpoint_URLs__c(name= 'Cole Capital Contacts_Stage', End_Point_URL__c='http://uat.colecapital.com/vereit/api/contact');
             Websites_Drupal_Endpoint_URLs__c e5= new Websites_Drupal_Endpoint_URLs__c(name= 'Cole Capital Users_Production', End_Point_URL__c='http://www.colecapital.com/vereit/api/user');
             Websites_Drupal_Endpoint_URLs__c e6= new Websites_Drupal_Endpoint_URLs__c(name= 'Cole Capital Users_Stage', End_Point_URL__c='http://uat.colecapital.com/vereit/api/user');
             Websites_Drupal_Endpoint_URLs__c e7= new Websites_Drupal_Endpoint_URLs__c(name= 'VEREIT Properties_Production', End_Point_URL__c='http://www.vereit.com/vereit/api/property-pull');
             Websites_Drupal_Endpoint_URLs__c e8= new Websites_Drupal_Endpoint_URLs__c(name= 'VEREIT Properties_Stage', End_Point_URL__c='http://uat.vereit.com/vereit/api/property-pull');
             Websites_Drupal_Endpoint_URLs__c e9= new Websites_Drupal_Endpoint_URLs__c(name= 'VEREIT Press Releases_Production', End_Point_URL__c='http://www.vereit.com/vereit/api/press-release-pull');
             Websites_Drupal_Endpoint_URLs__c e10= new Websites_Drupal_Endpoint_URLs__c(name= 'VEREIT Press Releases_Stage', End_Point_URL__c='http://uat.vereit.com/vereit/api/press-release-pull');
             
             eList.add(e1);
             eList.add(e2);
             eList.add(e3);
             eList.add(e4);
             eList.add(e5);
             eList.add(e6);
             eList.add(e7);
             eList.add(e8);
             eList.add(e9);
             eList.add(e10);
            
             insert eList;
           
      }
      public static string DrupalEndPointURLs(String objectName){
          Organization org=[select Id,IsSandbox from Organization limit 1];
          Websites_Drupal_Endpoint_URLs__c EndPointURLs = new Websites_Drupal_Endpoint_URLs__c();
            
            system.debug(logginglevel.info, '--------EndPointURLs: '+EndPointURLs);

           if(objectName == 'Cole Capital Properties')
             EndPointURLs= Websites_Drupal_Endpoint_URLs__c.getvalues(!org.IsSandbox ? 'Cole Capital Properties_Production' : 'Cole Capital Properties_Stage'); 
           else if(objectName == 'Cole Capital Contacts')
              EndPointURLs= Websites_Drupal_Endpoint_URLs__c.getvalues(!org.IsSandbox ? 'Cole Capital Contacts_Production' : 'Cole Capital Contacts_Stage');  
           else if(objectName == 'Cole Capital Users')
              EndPointURLs= Websites_Drupal_Endpoint_URLs__c.getvalues(!org.IsSandbox ? 'Cole Capital Users_Production' : 'Cole Capital Users_Stage');  
           else if(objectName == 'VEREIT Properties')
              EndPointURLs= Websites_Drupal_Endpoint_URLs__c.getvalues(!org.IsSandbox ? 'VEREIT Properties_Production' : 'VEREIT Properties_Stage');  
           else if(objectName == 'VEREIT Press Releases')
             EndPointURLs= Websites_Drupal_Endpoint_URLs__c.getvalues(!org.IsSandbox ? 'VEREIT Press Releases_Production' : 'VEREIT Press Releases_Stage');
             
             return EndPointURLs.End_Point_URL__c;

          /*if(org.IsSandbox == false){
             if(objectName == 'Cole Capital Properties')
                EndPointURLs=Websites_Drupal_Endpoint_URLs__c.getvalues('Cole Capital Properties_Production'); 
             if(objectName == 'Cole Capital Contacts')
                EndPointURLs=Websites_Drupal_Endpoint_URLs__c.getvalues('Cole Capital Contacts_Production');  
             if(objectName == 'Cole Capital Users')
                EndPointURLs=Websites_Drupal_Endpoint_URLs__c.getvalues('Cole Capital Users_Production');  
             if(objectName == 'VEREIT Properties')
                EndPointURLs=Websites_Drupal_Endpoint_URLs__c.getvalues('VEREIT Properties_Production');  
             if(objectName == 'VEREIT Press Releases')
                EndPointURLs=Websites_Drupal_Endpoint_URLs__c.getvalues('VEREIT Press Releases_Production');
             DrupalEndPointURL=EndPointURLs.End_Point_URL__c;
          }
          else{
             if(objectName == 'Cole Capital Properties')
                EndPointURLs=Websites_Drupal_Endpoint_URLs__c.getvalues( 'Cole Capital Properties_Stage'); 
             if(objectName == 'Cole Capital Contacts')
                EndPointURLs=Websites_Drupal_Endpoint_URLs__c.getvalues('Cole Capital Contacts_Stage');  
             if(objectName == 'Cole Capital Users')
                EndPointURLs=Websites_Drupal_Endpoint_URLs__c.getvalues('Cole Capital Users_Stage');  
             if(objectName == 'VEREIT Properties')
                EndPointURLs=Websites_Drupal_Endpoint_URLs__c.getvalues('VEREIT Properties_Stage');  
             if(objectName == 'VEREIT Press Releases')
                EndPointURLs=Websites_Drupal_Endpoint_URLs__c.getvalues('VEREIT Press Releases_Stage');            
             DrupalEndPointURL=EndPointURLs.End_Point_URL__c;
          }*/
          //return DrupalEndPointURL;
      }
      
      @future(callout=true)
      public static void makeCall(String endpoint, String token, String method, Integer timeout, String accept, String body){
            HttpResponse res;
            system.debug('body123'+body);
            try{
                  Http h = new Http();
                  HttpRequest req = new HttpRequest();
                  req.setEndpoint(endpoint);
                  req.setHeader('Authorization', +token);
                  req.setHeader('Accept',accept);
                  req.setMethod(method);
                  req.setTimeout(timeout);
                  req.setBody(body);
                  res = h.send(req);
                  system.debug('body1234'+body);
                  system.debug(logginglevel.info, '\n-----*****response body*****-----\n'+res.getBody());
            }
            catch(CalloutException e){
                  system.debug(logginglevel.info, '-----------CalloutException: '+e.getMessage());
                  
                  if(res!=null)
                        system.debug(logginglevel.info, '-----------CalloutException: '+res.getStatus()+', Error Code: '+ res.getStatusCode());
            }
       
      }

      
      public static string makeCallSync(String endpoint, String token, String method, Integer timeout, String accept, String body){
            HttpResponse res;
            string jsonResponse='';
            system.debug('body123'+body);
            try{
                  Http h = new Http();
                  HttpRequest req = new HttpRequest();
                  req.setEndpoint(endpoint);
                  req.setHeader('Authorization', +token);
                  req.setHeader('Accept',accept);
                  req.setMethod(method);
                  req.setTimeout(timeout);
                  req.setBody(body);
                  res = h.send(req);
                  system.debug('body1234'+body);
                  string valuetoShow = 'Drupal ' + res.getBody();
                  system.debug('body'+res.getBody());
                  jsonResponse =res.getBody();
                  system.debug(logginglevel.info, '\n-----*****response body*****-----\n'+res.getBody());
            }
            catch(CalloutException e){
                  system.debug(logginglevel.info, '-----------CalloutException: '+e.getMessage());
                  
                  if(res!=null)
                        system.debug(logginglevel.info, '-----------CalloutException: '+res.getStatus()+', Error Code: '+ res.getStatusCode());
            }
            return jsonResponse;
       
      }

      public class Mock implements HttpCalloutMock{
        String body='';
        Integer status=200;

        public Mock(String body, Integer status){
          this.body= body;
          this.status= status;
        }
        public HTTPResponse respond(HTTPRequest req){
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(status);
            res.setBody(body);
            
            return res;
        }
    }


}