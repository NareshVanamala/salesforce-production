global with sharing class LeaseChargeDetailsPdfExtn{
    public static string pdfname {get;set;}
    Public Lease_Charges_Request__c leaseChargeRequestObj {get;set;}
    public LeaseChargeDetailsPdfExtn(ApexPages.StandardController controller) {
        leaseChargeRequestObj = new Lease_Charges_Request__c ();
        leaseChargeRequestObj = (Lease_Charges_Request__c)controller.getRecord();
        //createAttachment();
    } 
    List<Lease_Charge_Details__c> leaseChargeDetailList = null;
    Public List<Lease_Charge_Details__c> getLeaseChargeDetail() {
        if(leaseChargeDetailList == null && leaseChargeRequestObj!= null){ 
            leaseChargeDetailList = [select Id,Charge_Code__c, Charge_Type__c,LeaseId_TenanatName__c,Description__c ,LeaseChargesRequest__c ,Start_Date__c,End_Date__c,Gross_Amount__c,NetAmount__c,Sales_Tax__c,Source_Code__c,Support_Required__c ,Type__c from Lease_Charge_Details__c where LeaseChargesRequest__c=:leaseChargeRequestObj.Id];
        }
       
        
        return leaseChargeDetailList;
    } 
     webservice static void createAttachment(Id leasechargeId){
        if(leasechargeId!=null){
            List<Lease_Charges_Request__c> pdf = new List<Lease_Charges_Request__c>();
            pdf = [select id,Lease__r.MRI_PROPERTY__r.Property_ID__c ,Lease__r.SuiteId__c,Lease__r.Lease_Status__c,Lease__r.MRI_Lease_ID__c,Lease__r.LeaseId_TenanatName__c,Lease__r.Occupant_Name__c,Lease__r.MRI_PROPERTY__r.Common_Name__c,Lease__r.Lease_ID__c,Pdf_Name__c from Lease_Charges_Request__c where Id =:leasechargeId];
            pdfname = pdf[0].Pdf_Name__c;
            List<Attachment> attList = new List<Attachment>();
            attList = [select id ,ParentId from Attachment where ParentId =:leasechargeId];
            if(attList!=null && attList.size()>0){
                delete attList;
            }
            
            Blob leaseChargeDetailBody;
            PageReference pdfPage = new PageReference('/apex/LeaseChargesRequest?Id='+leasechargeId);
            pdfPage.getParameters().put('id',leasechargeId);
            if (Test.IsRunningTest())
            {
            leaseChargeDetailBody=Blob.valueOf('UNIT.TEST');
            }
            else
            {
            leaseChargeDetailBody = pdfPage.getContentAsPdf();
            }
           /*  Attachment attObj = new Attachment();
            attObj.ParentId = leasechargeId;
            attObj.Body = leaseChargeDetailBody;
            attObj.Name = pdfname;
            insert attObj; */
        }
            /*PageReference reRend = new PageReference('/'+leaseChargeRequestObj.id);
            return reRend;*/
    }
}