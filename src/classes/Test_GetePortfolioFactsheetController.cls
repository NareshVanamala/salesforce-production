@isTest
public class Test_GetePortfolioFactsheetController{
static testMethod void GetePortfolioFactsheetControllerTest()
{
  Test.StartTest();
  
  List<GetePortfolioPropertyFactsheetController.MyWrapper> deals = new List<GetePortfolioPropertyFactsheetController.MyWrapper>();
  list<GetePortfolioPropertyFactsheetController.MyWrapper>deallist = new list<GetePortfolioPropertyFactsheetController.MyWrapper>();
    list<GetePortfolioPropertyFactsheetController.MyWrapper>wraplist = new list<GetePortfolioPropertyFactsheetController.MyWrapper>();

  Map<Id, List<Rent_Roll__c>>dealrentRoll;
  Map<Id, List<Loan_Relationship__c>>dealLoanRelation;
  list<Rent_Roll__c>rrlist= new list<Rent_Roll__c>();
  list<Loan_Relationship__c>lanlist= new list<Loan_Relationship__c>();
  list<Deal__c>dealnewlist= new list<Deal__c>();
  Account a= new Account();
  a.name='Testaccount1';
  insert a; 
  
  Portfolio__c ct2= new Portfolio__c();
  ct2.name='TestPortfolio'; 
  insert ct2;
  
  Portfolio__c port = new Portfolio__c();
  port.name = 'Testport';
 // port.Address__c = d.Address__c;
  //port.City__C =d.City__C;
  //port.State__c = d.State__c;
  insert port;
  
  
  Deal__c d = new Deal__c();
  d.name = 'TestDeal';
  d.Address__c = 'Northeast';
  d.City__C = 'Kansas';
  d.State__c = 'AZ';
  d.Portfolio_Deal__c=port.id;
  //insert d;
  dealnewlist.add(d);
  
 system.assertequals(d.Portfolio_Deal__c,port.id);

  
  Task t = new Task();
  t.status = 'New';
  t.subject ='Left Voice Email';
  t.Date_Ordered__c = System.Today();
  t.ActivityDate = System.Today();
  t.Description = 'comments';
  t.Date_Received__c = System.Today();
  t.whatid = d.id;
  insert t;
  
  
  
  Deal__c d1 = new Deal__c();
  d1.name = 'TestDeal';
  d1.Address__c = 'Northeast';
  d1.City__C = 'Kansas';
  d1.State__c = 'AZ';
  d1.Portfolio_Deal__c = port.id;
  //insert d1;
  dealnewlist.add(d1);
  insert dealnewlist;
  
  Loan__c lc= new Loan__c();
  lc.name='testloan';
  insert lc;
  
  Rent_Roll__c rent = new Rent_Roll__c();
  rent.Name_of_Tenant__c = 'Tenant';
  rent.Suite_ID__c = 'rtrt454545654';
  rent.Floor__c = '5';
  rent.SF__c = 10;
  rent.store__c ='rtre';
  rent.Lease_Expiration__c = System.today();
  rent.Tenant_Type_ID__c = 'fgfg45345345';
  rent.Lease_Code__c = 'fgfg';
  rent.Reimb_Code__c='tytyt';
  rent.S_P_Rating__c ='AAA';
  rent.Deal__c = d1.id;
  //insert rent;
  rrlist.add(rent);
  Rent_Roll__c rent1 = new Rent_Roll__c();
  rent1.Name_of_Tenant__c = 'Tenant1';
  rent1.Suite_ID__c = 'rtrt454545654';
  rent1.Floor__c = '5';
  rent1.SF__c = 10;
  rent1.store__c ='rtre';
  rent.Lease_Expiration__c = System.today();
  rent1.Tenant_Type_ID__c = 'fgfg45345345';
  rent1.Lease_Code__c = 'fgfg';
  rent1.Reimb_Code__c='tytyt';
  rent1.S_P_Rating__c ='AAA';
  rent1.Deal__c = d1.id;
  //insert rent1;
  rrlist.add(rent1);
  insert rrlist;
  
  Loan_Relationship__c loan = new Loan_Relationship__c();
  loan.Deal__c = d1.id;
  loan.Lender__c = a.id;
  loan.Loan_Amount__c = 10000;
  loan.LTV__c = 98989;
  loan.test__c = 'test';
  loan.Loan__c = lc.id;
  //insert loan;
  
  
   system.assertequals(loan.Loan__c,lc.id);

  lanlist.add(loan);
  insert lanlist;
  
  ApexPages.currentPage().getParameters().put('id',port.id);
  
  GetePortfolioPropertyFactsheetController p = new GetePortfolioPropertyFactsheetController();
  GetePortfolioPropertyFactsheetController.MyWrapper wrap= new GetePortfolioPropertyFactsheetController.MyWrapper();
  wrap.name = d.name;
  wrap.City = d.City__C;
  wrap.State = d.State__C;
  wrap.Rentrolllist=rrlist;
  wrap.Loanrealtionshiplist=lanlist;
  deallist.add(wrap);
  p.deals=deallist;
  for(Deal__c ct:dealnewlist)
  {
      GetePortfolioPropertyFactsheetController.MyWrapper wrap1= new GetePortfolioPropertyFactsheetController.MyWrapper();
      wrap1.city=ct.City__C;
      wrap1.name=ct.name; 
  
  
  }
  
  p.getdeals();
  Test.StopTest();
  }
  }