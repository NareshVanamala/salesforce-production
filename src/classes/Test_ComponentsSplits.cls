@isTest
private class Test_ComponentsSplits
 {
   public static testMethod void TestforComponentsSplits () 
   {
      
       Account acc = new Account();
        acc.Name = 'Test Prospect Account';
        Insert acc;
        
        //Set up user
        User us = [SELECT Id FROM User limit 1];          
           
        Event_Automation_Request__c ear = new Event_Automation_Request__c();
        ear.Name='BDE13006';
        ear.Event_Name__c='testeventname';
        ear.Start_Date__c=System.Today();
        ear.End_Date__c=System.Today();
        ear.Broker_Dealer_Name__c=acc.id;
        ear.Event_Location__c='testlocation';
        ear.Key_Account_Manager_Name_Requestor__c=us.id;
        ear.Event_Venue__c='testvenue';
        insert ear;
        
        system.assertequals(ear.Broker_Dealer_Name__c,acc.id);

        //id, Region_Split__c,name,Split__c from Event_Request_Split__c
        Event_Request_Split__c ers = new Event_Request_Split__c();
        ers.Region_Split__c='Central     RIA';
        ers.Split__c=50;
        //ers.Department_Split__c='Human Resources';
        //ers.Department_Split_Percentage__c =50; 
        ers.Fund_Split__c='3700-CCPT I';
        ers.Fund_Split_Percentage__c=50;
        ers.name='reg';
        ers.Event_Automation_Request__c=ear.id;            
        insert ers;
   
        Event_Request_Split__c ers1 = new Event_Request_Split__c();
        ers1.Region_Split__c='Midwest     RIA';
        ers1.Split__c=50;
        //ers.Department_Split__c='Accounting';
        //ers.Department_Split_Percentage__c =50; 
        ers1.Fund_Split__c='CCPT III';
        ers1.Fund_Split_Percentage__c=50;
        ers1.name='reg12';
        ers1.Event_Automation_Request__c=ear.id;            
        insert ers1;
        
        system.assertequals(ers1.Event_Automation_Request__c, ear.id);

        
        FundSplitController sc = new FundSplitController();
        sc.quoteId1=ear.id;
        sc.getFundsplits();       
   
   }
   
 }