public with sharing class AdvancedProgramController1 {

    Public List<Program__c> memberList{get;set;}
    public Program__c deletelist1;
    public Program__c updatedlist1;
    public integer removepos{get;set;}
    public string conid{get;set;}
    public Id recId{get;set;} 
    
    public string fileName{get;set;} 
    public Blob fileBody{get;set;}
    
    public Attachment attachment{get;set;}
    public Attachment myatt{get;set;}
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    public Boolean showInputFile{get;set;}
     
    public Account a;  
        public AdvancedProgramController1(ApexPages.StandardController controller) {
        addrow = new Program__c();
        //myAttachment = new Attachment();

        a =(Account)controller.getrecord();
        showInputFile = false;
        //a =[select id from Account where id=:ApexPages.currentPage().getParameters().get('id')];
        //a=[select id from Account where id= '001P000000adMsB'];
        memberList =[select name, id,(Select Id,Name from Attachments) Attmts,Account__c,Assets__c,Category__c,Cole_Product__c from Program__c where Account__c=:a.id order by LastModifiedDate DESC];  
        deletelist1= new Program__c();  
        updatedlist1=new Program__c();
        recId= controller.getRecord().id;
        myatt=new attachment();
    }
    public Pagereference removecon(){
        if(memberList.size()>0)
        {
        
            string paramname= Apexpages.currentPage().getParameters().get('node');
            system.debug('Check the paramname'+paramname);
            for(Integer i=0;i<memberList.size();i++)
            {
                if(memberList[i].id==conid)
                {
                    removepos=i;
                    deletelist1.id=memberList[i].id;
                  
                }
            }
            memberList.remove(removepos);
            system.debug('check the deletelist..'+deletelist1);
            
            if (Program__c.sObjectType.getDescribe().isDeletable()) 

            {
                delete  deletelist1;       
            }           
        }
       return ApexPages.currentPage();
   }
   
   // Edit Record method ....
   Pagereference p;
   public Id recordId {set;get;}
   public Boolean edit {set;get;}   
   public Pagereference editRecord(){
       recordId = conid;
       edit = true;
       //p = new Pagereference('/apex/AdvancedProgrampage?id='+ApexPages.currentPage().getParameters().get('id'));
       return null;
   }
   
   public Pagereference viewRecord(){
       recordId = conid;   
      myatt=[select id, name, parentid from Attachment where parentid=:recordId ];
        system.debug('check the attachement..'+myatt);
       p = new Pagereference('/'+myatt.id);
      // p.setRedirect(true);
       return p;
   }
   
   public Pagereference SaveRecord(){
      if(memberList.size()>0)
        {
             for(Integer i=0;i<memberList.size();i++)
            {
                if(memberList[i].id==conid)
                {
                   updatedlist1.id= memberList[i].id;
                   updatedlist1.Name= memberList[i].Name;
                   updatedlist1.Assets__c= memberList[i].Assets__c;
                   updatedlist1.Category__c=memberList[i].Category__c;
                   updatedlist1.Cole_Product__c= memberList[i].Cole_Product__c;
                }
            }
            
            if (Schema.sObjectType.Program__c.isUpdateable()) 

                 update updatedlist1;          
        }
       edit = false;
       return null;
       
   }
    //adding new record
    public Boolean addnew{set;get;}
    public Program__c addrow {set;get;}

    public Pagereference newProgram(){
        attachment =  new Attachment();
        addrow= new Program__c();
        addnew=true;        
        showInputFile = true;
        return null;    
    }
    public Pagereference savenewProg(){
        addnew=false;
        try{
            addrow.Account__c = a.id;
            if(addrow.name!=null)
            insert addrow;
           else
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Enter Name of the Program'));

            system.debug('Added --->'+addrow.Id);
           // memberList = [select name, id,(Select Id,Name from Attachments) Attmts,Account__c,Assets__c,Category__c,Cole_Product__c from Program__c where Account__c=:a.id order by LastModifiedDate DESC]; 
                        
            attachment.OwnerId = UserInfo.getUserId();
            attachment.ParentId = addrow.Id;// the record the file is attached to
            attachment.Name = nameFile;
            attachment.Body = contentFile;   
            //attachment.IsPrivate = true;
            
            system.debug('\n myAttachment.name--->'+attachment.name);
            system.debug('check the attachement..'+ attachment);
            try {
                if((attachment.Name!=null) &&(attachment.Body!=null))
                insert attachment;
                memberList = [select name, id,(Select Id,Name from Attachments) Attmts,Account__c,Assets__c,Category__c,Cole_Product__c from Program__c where Account__c=:a.id order by LastModifiedDate DESC];
            } catch (DMLException e) {
                system.debug('check the attachement..'+ attachment);
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
            }
       
        }catch(Exception e){
            system.debug('Exception --->'+e);
        } 
          
        return ApexPages.currentPage(); 
           
    }
    public Pagereference cancelnewProg(){
        addnew=false;
        return null;    
    }  
   

    
 }