@isTest (SeeAllData=true)
private class ocms_WebPropertyDetail_test {
    
    @isTest static void test_getHTML() {

        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        
        Web_Properties__c wp=new Web_Properties__c();
        wp.name='test';
        wp.Long_Description__c='hi';
        wp.Tenant_Profile__c='hii';
        wp.Property_Type__c='hi';
        wp.sector__c='hi';
        wp.ProgramName__c='lowe';
        wp.Date_Acquired__c=system.today();
        wp.sqFt__c=2000;
        insert wp;
        System.assertEquals(wp.ProgramName__c,'lowe');

        System.currentPageReference().getParameters().put('propertyId', wp.id); //Modify this value to point to a web_property__c record with values.

        ocms_WebPropertyDetail detail = new ocms_WebPropertyDetail();
        detail.getHTML();
    }
    
    @isTest static void test_getHTML1() {

        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        
        Web_Properties__c wp=new Web_Properties__c();
        wp.name='test';
        wp.Long_Description__c='';
        wp.Tenant_Profile__c='';
        wp.Property_Type__c='hi';
        wp.sector__c='hi';
        wp.ProgramName__c='lowe';
        wp.Date_Acquired__c=system.today();
        wp.sqFt__c=2000;
        insert wp;
        System.assertEquals(wp.ProgramName__c,'lowe');

        
        System.currentPageReference().getParameters().put('propertyId', wp.id); //Modify this value to point to a web_property__c record with values.

        ocms_WebPropertyDetail detail = new ocms_WebPropertyDetail();
        detail.getHTML();
    }
    
}