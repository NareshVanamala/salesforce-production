public class StatusOfCallList{

/*
     public PageReference exportToExcel()
     {
        return page.GeneratingExcel;
      }

   
    public List<SelectOption> CallListValues {set;get;}
    public String calllist {get;set;}     
    public integer totalcontactsCreated{set;get;}
    public integer totalcontactsAssigned{set;get;}
    public integer totalcontactsUnAssigned{set;get;}
    private List<ContactCountWrappercls> ContactListCls ;
    
    public List<ContactCountWrappercls> getContactListCls(){
      return ContactListCls;
    }
    transient  list<Contact_Call_List__c> totalcontactsAssignedlist;
    transient  list<Contact_Call_List__c> totalcontactsUnAssignedlist;
    transient  list<Contact_Call_List__c> lsttask;

    public StatusOfCallList(){
        set<String> call = new Set<String>();
        List<String> lscall = new List<String>();
        try{
            List<Automated_Call_List__c> tcall = [select ID,Name,Automated_Call_list_Name__c from Automated_Call_List__c where Name != null];
            for(Automated_Call_List__c t : tcall)
            call.add(t.Name); 
            call.add('--None--');
            CallListValues = new List<SelectOption>();
            lscall.AddAll(call);
            lscall.sort();
            for(String s : lscall)
            CallListValues.add(new SelectOption(s,s));
            if(CallListValues.size()==0)
            CallListValues.add(new SelectOption('None','None'));
        }catch(Exception e){}
    }
    
    public void AdviserCallList(){
        ContactListCls = new List<ContactCountWrappercls>();
        totalcontactsAssignedlist= new list<Contact_Call_List__c>();
        totalcontactsUnAssignedlist= new list<Contact_Call_List__c>();
        lsttask = new list<Contact_Call_List__c>();
        totalcontactsUnAssignedlist.clear();
        totalcontactsAssignedlist.clear();
        lsttask.clear();
        ContactListCls.clear();
        List<AggregateResult> groupedResults1=[select CallList_Name__c, COUNT(id) cnt from Contact_Call_List__c where CallList_Name__c=:calllist  GROUP BY CallList_Name__c];
        
        List<AggregateResult> groupedResults2=[select CallList_Name__c, COUNT(id) cnt1 from Contact_Call_List__c where (CallList_Name__c=:calllist and Owner__c!=null) GROUP BY CallList_Name__c];
        
       
        System.debug('*****Size Of Aggregate Result******\n'+groupedResults1);
        If(groupedResults1.size()>0)
        {
        totalcontactsCreated =(integer)groupedResults1[0].get('cnt');
        }
        else {
         totalcontactsCreated = 0;
         }
        If(groupedResults2.size()>0)
        {
         totalcontactsAssigned=(integer)groupedResults2[0].get('cnt1');
         }
         else {
         totalcontactsAssigned = 0;
         }
         
          totalcontactsUnAssigned = totalcontactsCreated - totalcontactsAssigned ;
          
        
        System.debug('Heap Size--->0: ' + Limits.getHeapSize() + ' <---> ' + Limits.getLimitHeapSize());
       // lsttask=[select id,Call_Complete__c,CallList_Name__c,Owner__c from Contact_Call_List__c where CallList_Name__c=:calllist];
        System.debug('Heap Size--->1: ' + Limits.getHeapSize() + ' <---> ' + Limits.getLimitHeapSize());
         integer subtotal1;
          integer subtotal;
        
        
        //totalcontactsAssignedlist=[select id,Call_Complete__c,CallList_Name__c,Owner__c from Contact_Call_List__c where CallList_Name__c=:calllist and Owner__c!=null];
        //totalcontactsUnAssignedlist=[select id,Call_Complete__c,CallList_Name__c,Owner__c from Contact_Call_List__c where CallList_Name__c=:calllist and Owner__c=null];
        //Integer totcontUnAss=0;
        //Integer totcontAss=0;
        //for(Contact_Call_List__c ctr:lsttask){
          //  if(ctr.Owner__c ==null){  
            //    totcontUnAss=totcontUnAss+1;    
              //  //totalcontactsUnAssignedlist.add(ctr);
            //}       
           //else{
             //  totcontAss=totcontAss+1;
               //totalcontactsAssignedlist.add(ctr);
          // }
       // }
        
        //totalcontactsCreated=lsttask.size();        
        //totalcontactsAssigned=totcontAss;//totalcontactsAssignedlist.size();
        //totalcontactsUnAssigned=totcontUnAss;// totalcontactsUnAssignedlist.size();
         Map<Id,User> usermap = new Map<Id,User>([select Id,Name,Territory__c from User where Name != null]);
        System.debug('******STRAT*********\n'+System.now());
        List<AggregateResult> groupedResults=[select CallList_Name__c,Owner__c,Call_Complete__c, ReasonToSkip__c, COUNT(id) cnt from Contact_Call_List__c where (CallList_Name__c=:calllist and Owner__c!=null ) GROUP BY Owner__c, Call_Complete__c, ReasonToSkip__c, CallList_Name__c  order by Owner__c  ASC , CallList_Name__c ASC];
        System.debug('Heap Size--->2: ' + Limits.getHeapSize() + ' <---> ' + Limits.getLimitHeapSize());
        System.debug('******END********\n'+System.now());
        system.debug('Check the aggregate result....'+ groupedResults);
        String prevName;
        String prevCallList;
        integer prevRemaining =0;
        integer prevContacted =0;
        integer prevSkipped=0;
        Integer prevCount=0;
        integer contacted =0;
        integer remaining =0;
        integer skipped=0;
        integer firstEntry = 0;
        integer totalcreated=0;
        if(groupedResults.size()>0)
        {
            for (AggregateResult ar : groupedResults)
            {
                String asgned= (String)ar.get('Owner__c');
                //String call= (String)ar.get('CallList_Name__c');
                Integer cnt = (Integer)ar.get('cnt');
                Boolean Status=(Boolean)ar.get('Call_Complete__c');
                String name=null;
                totalcreated=cnt;
                if(asgned!=null)
                    name = usermap.get(asgned).Name;
                if((Boolean.valueof(ar.get('Call_Complete__c')) == True)&&(String.valueof(ar.get('ReasonToSkip__c')) ==null ))
                {
                    contacted = integer.valueof(ar.get('cnt'));
                    remaining = 0;
                    skipped=0;
                }
                if(Boolean.valueof(ar.get('Call_Complete__c')) == False)
                {
                    remaining = integer.valueof(ar.get('cnt'));
                    contacted = 0;
                    skipped=0;
                }
                if((Boolean.valueof(ar.get('Call_Complete__c')) == True)&&(String.valueof(ar.get('ReasonToSkip__c'))!=null))
                {
                    skipped=integer.valueof(ar.get('cnt'));
                    contacted =0;
                    remaining = 0;
                }
                
                if(name!=null && prevName !=null)
                {
                  // Check to see if we are still dealing with same name and call list
                  if (prevName == name) 
                  {
                   // we are still same call list
                   // Check the status
                   // remaining = prevRemaining;
                   // contacted= prevcontacted;
                   prevCount= prevCount+ totalcreated;
                   totalcreated= prevCount;
                   
               //added by snehal to solve the issue calllist assigned twice...    
                   prevContacted = prevContacted + contacted ;
               //only one line....    
                   if (prevContacted > 0)  
                    contacted = prevContacted;
                    system.debug('check the contacts...'+contacted);
                  prevRemaining = prevRemaining + remaining ;
                    if (prevRemaining > 0)
                    remaining = prevRemaining; 
                    prevSkipped= prevSkipped+ skipped;
                    if(prevSkipped>0)
                     skipped=prevSkipped;
                        
                   }
                  else{                        
                        // dealing with new call list
                       // system.debug('We are inserting in 1st else  loop inside for.. '+ prevName+ prevCount + prevContacted+ prevRemaining);
                        ContactCountWrappercls  tskcls = new ContactCountWrappercls (prevName, prevCount, prevContacted ,prevRemaining,prevSkipped );
                        ContactListCls.add(tskcls);
                        //system.debug('Check the list priya'+ ContactListCls);
                    }                   
                }
                prevRemaining = remaining;
                prevContacted = contacted;
                prevSkipped = skipped;     
                //System.debug('name : ' + name +' call list ' + call + ' prevName ' + prevName + ' prev call list ' + prevCallList + ' contacted:' + contacted + 'remaining ' + remaining + ' prevcontacted ' + prevContacted + 'prevremaining ' + prevremaining);
                prevName = name;
                prevCount= totalcreated;
                prevSkipped= skipped;
               //prevCallList = call;
            }
            System.debug('******END********\n'+System.now());            
            // For the last entry, log the record   
            //system.debug('We are inserting outside for.loop. '+ prevName+ totalcreated+ prevContacted+ prevRemaining);
            ContactCountWrappercls tskcls = new ContactCountWrappercls (prevName, totalcreated, prevContacted ,prevRemaining,prevSkipped);
            ContactListCls.add(tskcls);
            // system.debug('Lets check the list.....ninad'+ ContactListCls);
        }
    }
         
     public class ContactCountWrappercls 
    {  
        public String AssignedTo {set;get;}
        public Integer completed1{set;get;}
        public Integer remaining {set;get;}
        public Integer count {set;get;} 
        public Integer skipped1 {set;get;}
        //public String status1{set;get;}
        public ContactCountWrappercls (String assign,Integer CreatedContacts ,integer contacted1 ,Integer remaining1,Integer skipped ){
            AssignedTo  = assign;
            count= CreatedContacts ;
            //count = cnt;
            completed1=contacted1;
            remaining=remaining1;
            skipped1 =skipped ;
        }
    }*/
}