public class sendEmailtousersProcessor {
@future
public static void processAccounts5(List<String> str ) {
List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
String samp = System.Label.Current_Org_Url;
For(Task_plan__c tp : [Select id , Name,Notify_Emails__c ,Action_Plan_Name__c,Deal_Name__c,Loan_Name__c,Date_Received__c,Template__c,TaskRelatedto__c,TaskRelatedToloan__c from Task_plan__c  Where Id in :str ]){
   If(tp.TaskRelatedto__c!=null)
   {
   List<String> strVal = new List<String>();
   String names12 = tp.Notify_Emails__c;
   strVal = names12.split(',');
   Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
   email.setToAddresses(strVal );
   email.setReplyTo('skalamkar@colecapital.com');
   email.setSenderDisplayName('Action Plan Tasks');
 //  email.setSubject('Action Plan Task'+' "'+tp.Name +'"'+''+' for'+' "'+tp.Action_Plan_Name__c+'"'+' Received');
 email.setSubject('Action Plan Task'+' "'+tp.Name +'"'+''+' for'+' "'+tp.Deal_name__c+'"'+' Received');
   email.setPlainTextBody('The task,' + ' "'+ tp.Name+'"'+'for'+' "'+tp.Deal_name__c+'"'+' was completed on'+' '+tp.Date_Received__c+'\r\n'+' \r\n'+''+samp+'/'+TP.TaskRelatedto__c+'\r\n'+ '\r\n'+ 'Thank you');
   emails.add(email);
   }
   If(tp.TaskRelatedToloan__c!=null)
   {
    List<String> strVal = new List<String>();
   String names12 = tp.Notify_Emails__c;
   strVal = names12.split(',');
   Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
   email.setToAddresses(strVal );
   email.setReplyTo('skalamkar@colecapital.com');
   email.setSenderDisplayName('Action Plan Tasks');
    email.setSubject('Action Plan Task'+' "'+tp.Name +'"'+''+' for'+' "'+tp.Loan_Name__c+'"'+' Received');
   email.setPlainTextBody('The task,' + ' "'+ tp.Name+'"'+'for'+' "'+tp.Loan_Name__c+'"'+' was completed on'+' '+tp.Date_Received__c+'\r\n'+' \r\n'+''+samp+'/'+TP.TaskRelatedToloan__c+'\r\n'+ '\r\n'+ 'Thank you');
   emails.add(email);
   }
    }
   Messaging.sendEmail(emails);
    }
  }