@isTest
private class Test_fundSplitclass
{
   public static testMethod void TestforfundSplitclass() 
   {     
        Account acc = new Account();
        acc.Name = 'Test Prospect Account';
        Insert acc;
        
        //Set up user
        User us = [SELECT Id FROM User limit 1];          
           
        Event_Automation_Request__c ear = new Event_Automation_Request__c();
        ear.Name='BDE13006';
        ear.Event_Name__c='testeventname';
        ear.Start_Date__c=System.Today();
        ear.End_Date__c=System.Today();
        ear.Broker_Dealer_Name__c=acc.id;
        ear.Event_Location__c='testlocation';
        ear.Key_Account_Manager_Name_Requestor__c=us.id;
        ear.Event_Venue__c='testvenue';
        insert ear;
        
         system.assertequals(ear.Broker_Dealer_Name__c,acc.id);

        //id, Region_Split__c,name,Split__c from Event_Request_Split__c
        Event_Request_Split__c ers = new Event_Request_Split__c();
        ers.Region_Split__c='Central RIA';
        ers.name='reg';
        ers.Event_Automation_Request__c=ear.id;
        ers.Fund_Split__c='CCPT III';
        ers.Fund_Split_Percentage__c =100;
        ers.Split__c=100;
        insert ers;
        
        system.assertequals(ers.Event_Automation_Request__c,ear.id);
 
        
        ApexPages.currentPage().getParameters().put('id',ear.id); 
        
        ApexPages.StandardController controller = new ApexPages.StandardController(ear);
        fundSplitclass sc = new fundSplitclass(controller);              
        
        sc.updatedlist1=ers;
        sc.removecon();
        sc.editRecord();       
        sc.SaveRecord();
        sc.newProgram();
        sc.savenewProg();
        sc.cancelnewProg();       
    }
}