@ISTEST
private class workspaceclassTest
{

    static testMethod void workspaceTest() 
    {
        Test.startTest();
        Deal__c d = new Deal__c();
        d.Name = 'Test';
        d.Build_to_Suit_Type__c = 'Forward';
        d.HVAC_Warranty_Status__c ='Received';
        d.Ownership_Interest__c = 'Free Simple';
        d.Zip_Code__c ='98033';
        d.Build_to_Suit_Type__c ='Current';
        d.Estimated_COE_Date__c = date.today();
        d.HVAC_Warranty_Status__c ='Received';
        d.Ownership_Interest__c = 'Fee Simple';
        d.Roof_Warranty_Status__c = 'Received';
        d.Zip_Code__c = '98033';
        d.workspace_Date__c= system.now();
        insert d;
        system.assertequals(d.HVAC_Warranty_Status__c,'Received');

        ApexPages.StandardController sc = new ApexPages.StandardController(d);
        ApexPages.currentPage().getParameters().put('id',d.Id);
        workspaceclass cont= new workspaceclass(sc) ;
        cont.createworkspace();    
        Test.stopTest();
    }
}