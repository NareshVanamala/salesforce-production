@isTest(seeAlldata =true)
public class PC_InlinePageMRIProp_Test{
    static testMethod void testmethod1(){
       Test.setCurrentPageReference(new PageReference('Page.myPage'));


        
        MRI_Property__c mrpObj = new MRI_Property__c();
        mrpObj.Name = 'testmrp';
        mrpObj.Fund1__c = 'ARCP';
        mrpObj.Date_Acquired__c = Date.today();
        mrpObj.Common_Name__c = 'cname';
        mrpObj.Property_Id__c = '1234';
        insert mrpObj;
        System.currentPageReference().getParameters().put('id', mrpObj.id);
        
         list<Lease__c> leaseLst = new list<Lease__c>();
        Lease__c lObj1 = new Lease__c();
        lObj1.MRI_Property__c = mrpObj.Id;
        lObj1.Tenant_Name__c = 'testTenent1';
        lObj1.Lease_Status__c = 'Active';
        insert lObj1 ;
        //leaseLst.add(lObj1);
        
        Lease__c lObj2 = new Lease__c();
        lObj2.MRI_Property__c = mrpObj.Id;
        lObj2.Tenant_Name__c = 'testTenent1';
        lObj2.Lease_Status__c = 'Active';        
        lObj2.previous_Lease__c =lObj1.id;
        insert lObj2 ;

        // leaseLst.add(lObj2);

       // insert leaseLst;

        Property_Contacts__c pcObj1 = new Property_Contacts__c();
        pcObj1.Name = 'test1';
        pcObj1.Company_Name__c ='Test Company Name';
        pcObj1.Email_Address__c = 'Test@gmail.com';
        pcObj1.Phone__c = '123456789';
        pcObj1.Address__c = 'Test Address';
        pcObj1.City__c = 'Test City';
        pcObj1.State__c = 'Test State';
        pcObj1.Zipcode__c = '12345';
        pcObj1.Local_Tenant_Name__c = 'Test Local tenant Name';
        pcObj1.Notes__c =  'Test Notes';
        pcObj1.MRI_Property__c = mrpObj.Id;
        insert pcObj1;

        list<Property_Contact_Leases__c > pconlist = new list<Property_Contact_Leases__c >();

        Property_Contact_Leases__c pclObj1 = new Property_Contact_Leases__c(Lease__c = lObj1.Id);
        pclObj1.Lease__c = lObj1.Id;
        pclObj1.Property_Contacts__c = pcObj1.Id;
        pclObj1.Contact_Type__c = 'Developer;Financials;Gross Sales';
        pconlist.add(pclObj1 );
        insert pconlist ;

        Property_Contact_Buildings__c pcbObj1 = new Property_Contact_Buildings__c ();
        pcbObj1 .center_Name__C= mrpObj.Id;
        pcbObj1 .Property_Contacts__c = pcObj1.Id;
        pcbObj1 .Contact_Type__c = 'Dining Services';
        insert pcbObj1 ;
        
       
       PC_InlinePageMRIProp ctrl = new PC_InlinePageMRIProp(new ApexPages.StandardController(mrpObj));
       PC_InlinePageMRIProp ctrlo = new PC_InlinePageMRIProp();
       ctrl.mripropid=ApexPages.currentPage().getParameters().get(mrpObj.Id);
       ctrl.mriprop = mrpObj;
       ctrl.propcontacts = pconlist ;
       PC_InlinePageMRIProp.mywrapper ctrl1 = new PC_InlinePageMRIProp.mywrapper(lObj2.id,pcObj1.id,mrpObj.id,lObj1.id,pcObj1.Name,pcObj1.Company_Name__c,pcObj1.Email_Address__c,pcObj1.Phone__c,pcObj1.Address__c,pcObj1.City__c,pcObj1.State__c,pcObj1.Zipcode__c,pclObj1.Contact_Type__c,pcObj1.Local_Tenant_Name__c,mrpObj.Common_Name__c,pcObj1.Notes__c);

       ctrl.ObjectName ='Lease';
       Lease__c ls = [select id,MRI_PROPERTY__c,Lease_Status__c from Lease__c where MRI_PROPERTY__c=:mrpObj.id  limit 1];
       if(ls!=Null){
       set <id> iset = new set<id>();
       iset.add(ls.id);
       ctrl.myobjectidset= iset ;
       }
       
         for(PC_InlinePageMRIProp.mywrapper ct : ctrl.mylist){
                ct.selected=True;
                ct.selrecId = pcbObj1.Property_Contacts__r.id;
                ct.recordid= pcbObj1.id;
                ct.MRIRecordId = mrpObj.id;
                ct.LeaseRecordId = lObj1.id;
                ct.PropertyContactName=pcbObj1.Property_Contacts__r.Name;
                ct.PropertyContactCompanyname=pcbObj1.Property_Contacts__r.Company_Name__c;
                ct.EmailAddress=pcbObj1.Property_Contacts__r.Email_Address__c;
                ct.Phone =pcbObj1.Property_Contacts__r.Phone__c;
                ct.Address=pcbObj1.Property_Contacts__r.Address__c;
                ct.City=pcbObj1.Property_Contacts__r.City__c;
                ct.State=pcbObj1.Property_Contacts__r.State__c;
                ct.Zipcode=pcbObj1.Property_Contacts__r.Zipcode__c;
                ct.ContactType=pcbObj1.Contact_Type__c;
                ct.LocalTenant=pcbObj1.center_name__r.Tenant_Name__c;
                ct.CommonName=pcbObj1.center_name__r.Common_Name__c;
                ct.Notes=pcbObj1.Property_Contacts__r.Notes__c;
                
                PC_InlinePageMRIProp.mywrapper  w= new PC_InlinePageMRIProp.mywrapper(ct.recordid,ct.selrecId,ct.MRIRecordId,ct.LeaseRecordId,ct.PropertyContactName,ct.PropertyContactCompanyname,ct.EmailAddress,ct.Phone,ct.Address,ct.City,ct.State,ct.Zipcode,ct.ContactType,ct.LocalTenant,ct.CommonName,ct.Notes); 
                ctrl.mylist.add(w); 

              }
       
    }   
   static testMethod void testmethod2(){
    
        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        MRI_Property__c mrpObj = new MRI_Property__c();
        mrpObj.Name = 'testmrp';
        mrpObj.Fund1__c = 'ARCP';
        mrpObj.Date_Acquired__c = Date.today();
        mrpObj.Common_Name__c = 'cname';
        mrpObj.Property_Id__c = '1234';
        insert mrpObj;
        System.currentPageReference().getParameters().put('id', mrpObj.id);
        
        list<Lease__c> leaseLst = new list<Lease__c>();
        Lease__c lObj1 = new Lease__c();
        lObj1.MRI_Property__c = mrpObj.Id;
        lObj1.Tenant_Name__c = 'testTenent1';
        lObj1.Lease_Status__c = 'Active';
        insert lObj1 ;

        
        Lease__c lObj2 = new Lease__c();
        lObj2.MRI_Property__c = mrpObj.Id;
        lObj2.Tenant_Name__c = 'testTenent1';
        lObj2.Lease_Status__c = 'Active';        
        lObj2.previous_Lease__c =lObj1.id;
        insert lObj2 ;


        Property_Contacts__c pcObj1 = new Property_Contacts__c();
        pcObj1.Name = 'test1';
        pcObj1.Company_Name__c ='Test Company Name';
        pcObj1.Email_Address__c = 'Test@gmail.com';
        pcObj1.Phone__c = '123456789';
        pcObj1.Address__c = 'Test Address';
        pcObj1.City__c = 'Test City';
        pcObj1.State__c = 'Test State';
        pcObj1.Zipcode__c = '12345';
        pcObj1.Local_Tenant_Name__c = 'Test Local tenant Name';
        pcObj1.Notes__c =  'Test Notes';
        pcObj1.MRI_Property__c = mrpObj.Id;
        insert pcObj1;

        list<Property_Contact_Leases__c > pconlist = new list<Property_Contact_Leases__c >();

        Property_Contact_Leases__c pclObj1 = new Property_Contact_Leases__c(Lease__c = lObj1.Id);
        pclObj1.Lease__c = lObj1.Id;
        pclObj1.Property_Contacts__c = pcObj1.Id;
        pclObj1.Contact_Type__c = 'Developer;Financials;Gross Sales';
        pconlist.add(pclObj1 );
        insert pconlist ;

        Property_Contact_Buildings__c pcbObj1 = new Property_Contact_Buildings__c ();
        pcbObj1 .center_Name__C= mrpObj.Id;
        pcbObj1 .Property_Contacts__c = pcObj1.Id;
        pcbObj1 .Contact_Type__c = 'Dining Services';
        insert pcbObj1 ;
        
       
       PC_InlinePageMRIProp ctrl = new PC_InlinePageMRIProp(new ApexPages.StandardController(mrpObj));
       PC_InlinePageMRIProp ctrlo = new PC_InlinePageMRIProp();
       ctrl.mripropid=ApexPages.currentPage().getParameters().get(mrpObj.Id);
       ctrl.mriprop = mrpObj;
       ctrl.propcontacts = pconlist ;
       PC_InlinePageMRIProp.mywrapper ctrl1 = new PC_InlinePageMRIProp.mywrapper(lObj2.id,pcObj1.id,mrpObj.id,lObj2.id,pcObj1.Name,pcObj1.Company_Name__c,pcObj1.Email_Address__c,pcObj1.Phone__c,pcObj1.Address__c,pcObj1.City__c,pcObj1.State__c,pcObj1.Zipcode__c,pclObj1.Contact_Type__c,pcObj1.Local_Tenant_Name__c,mrpObj.Common_Name__c,pcObj1.Notes__c);
       ctrl.ObjectName ='MRI Property';

       for(PC_InlinePageMRIProp.mywrapper ct : ctrl.mylist)
        {
               
                ct.selected=true;
                ct.selrecId =pcbObj1 .id;
                ct.recordid= pcbObj1 .Property_Contacts__r.id;
                ct.MRIRecordId = mrpObj.id;
                ct.LeaseRecordId = lObj2.id;
                ct.PropertyContactName=pcbObj1 .Property_Contacts__r.Name;
                ct.PropertyContactCompanyname=pcbObj1 .Property_Contacts__r.Company_Name__c;
                ct.EmailAddress=pcbObj1 .Property_Contacts__r.Email_Address__c;
                ct.Phone =pcbObj1 .Property_Contacts__r.Phone__c;
                ct.Address=pcbObj1 .Property_Contacts__r.Address__c;
                ct.City=pcbObj1 .Property_Contacts__r.City__c;
                ct.State=pcbObj1 .Property_Contacts__r.State__c;
                ct.Zipcode=pcbObj1 .Property_Contacts__r.Zipcode__c;
                ct.ContactType=pcbObj1 .Contact_Type__c;
                ct.LocalTenant=pcbObj1.center_name__r.Tenant_Name__c;
                ct.CommonName=pcbObj1.center_name__r.Common_Name__c;
                ct.Notes=pcbObj1 .Property_Contacts__r.Notes__c;
                
                PC_InlinePageMRIProp.mywrapper  w= new PC_InlinePageMRIProp.mywrapper(ct.recordid,ct.selrecId,ct.MRIRecordId,ct.LeaseRecordId,ct.PropertyContactName,ct.PropertyContactCompanyname,ct.EmailAddress,ct.Phone,ct.Address,ct.City,ct.State,ct.Zipcode,ct.ContactType,ct.LocalTenant,ct.CommonName,ct.Notes); 
                ctrl.mylist.add(w); 
                

              }
        
    }   
}