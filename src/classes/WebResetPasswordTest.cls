public class WebResetPasswordTest
{


    static testMethod void myPage_Test()

    {

       try
       {
        Account newAccount = new Account (name='XYZ Organization');
        insert newAccount;
        //create first contact
        Contact myContact = new Contact (FirstName='Joe',LastName='Schmoe',Email='bplumb@colecapital.com',AccountId=newAccount.id);
        insert myContact;
        system.assertequals(myContact.AccountId,newAccount.id);

        
        ApexPages.StandardController sc = new ApexPages.standardController(myContact);

        // create an instance of the controller
        WebResetPassword wt = new WebResetPassword(sc);
        wt.SendResetPassword();
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GeoCodeDummyResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        } 
        catch(Exception e)
        {
        }
}

}