Public with Sharing class workspaceclass{
public Deal__c mydeal {get;set;}
private Apexpages.StandardController controller; 

public workspaceclass(ApexPages.StandardController controller) {
this.controller = controller;
mydeal = [select id,create_workspace__C from Deal__c where id =:apexpages.currentpage().getparameters().get('id')];
}
Public pagereference createworkspace()
//Public List<Deal__c> createworkspace()
{
ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Workspace is being created and indexed in Autonomy, please wait for an email confirmation');
apexpages.addmessage(myMsg);
Mydeal.Create_Workspace__c =true;
//Mydeal.workspace_Date__c =System.Now();

if (Schema.sObjectType.Deal__c .isUpdateable()) 

update mydeal;
return null;

}
}