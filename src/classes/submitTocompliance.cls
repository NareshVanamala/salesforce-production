public with sharing class submitTocompliance {

     Case c;
    public String msg{get;set;}
    public Boolean flag{get;set;}
    public submitTocompliance(ApexPages.StandardController controller)
    {
       msg='Submit to Compliance';
       c =(Case)controller.getrecord();
       flag = false;
   
   system.debug('check the case'+c);
   }
   
   public void createCase(){
     System.debug('****' + c.id);
     
     Case c1;
     c1 = [select id, Origin,Channel__c,CaseNumber,Target_Audience__c,Response__c,OwnerId,Owner.name,Reason,Description,Status,Subject,Priority,Type from Case where id =: c.id];
     RecordType RecType = [Select Id,name From RecordType  Where name= 'Compliance Case Management'];
     System.debug('&&&&&&&' + RecType.id);
     System.debug('********' + c1.CaseNumber);
     Case c2 = new Case();
     c2.Status = 'New';
     c2.OwnerId = c1.OwnerId;
     c2.Subject = c1.Subject;
     c2.Priority= c1.Priority;
     c2.RecordTypeId = RecType.id;
     c2.Origin = c1.Origin;
     c2.Channel__c = c1.Channel__c;
     c2.Target_Audience__c= c1.Target_Audience__c;
     c2.Description = c1.Description;
     c2.Response__c = c1.Response__c;
     c2.Marketing_Case_Number__c = c1.CaseNumber;
     if (Schema.sObjectType.Case.isCreateable())

     insert c2;
     flag=true;
     msg='Case Created';
 
   }
}