@isTest
private class ContactCheckBoxTest {

private static testMethod void sendtest() {
test.startTest();
        Account Ac= new Account();
        Ac.Name = 'Test';
        Ac.X1031_Selling_Agreement__c='Income NAV – I';
        Insert Ac;
        
        Contact con =  new Contact();
        con.FirstName = 'Pratyush';
        con.LastName = 'Reddy';
        con.Email = 'Pratyush.reddy@colereit.com';
        con.RIA_Consultant_Picklist__c='Pratyush';
        con.Internal_Consultant_Picklilst__c='Pratyush';
        con.Internal_Wholesaler__c='Pratyush';
        con.Wholesaler__c ='Pratyush';
        con.Inav_referal__c =true;
        
        insert con;
        
        System.assertNotEquals(Ac.Name,con.FirstName);
        
        
If(con.Inav_Referal__c)
{ 
If(con.Account.X1031_Selling_Agreement__c!=null)
{     
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'Pratyush.reddy@colereit.com'};
        mail.setToAddresses(toAddresses);
        mail.setCCAddresses(new String[]{'Pratyush.reddy@colereit.com'});
        mail.setSubject('My Subject');
        mail.setUseSignature(false);
        mail.setPlainTextBody('message');
        // Send the email
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
} 
}     
    ApexPages.StandardController sc = new ApexPages.StandardController(con);
    ApexPages.currentPage().getParameters().put('id',con.Id);

      PageReference pageRef = sc.view();
      ContactCheckBox controller = new ContactCheckBox(sc) ;
      controller.changeSection();
      controller.save();
      controller.cancel();
    test.stopTest();
    
    
    
}
private static testMethod void sendtest1() {
test.startTest();
    Account Ac1= new Account();
        Ac1.Name = 'Test1';
        Ac1.X1031_Selling_Agreement__c='Income NAV – A';
        Insert Ac1;
        
       Contact con1 =  new Contact();
        con1.FirstName = 'Pawan';
        con1.LastName = 'Karakala';
        con1.Email = 'pawan.Karakala@colereit.com';
        con1.RIA_Consultant_Picklist__c='Pawan';
        con1.Internal_Consultant_Picklilst__c='Karakala';
        con1.Internal_Wholesaler__c='Pratyush';
        con1.Wholesaler__c ='Ninad';
        con1.Inav_referal__c =false;
        insert con1;
        System.assertNotEquals(Ac1.Name,con1.FirstName);
 If(!con1.Inav_Referal__c)
 {
  If(con1.Account.X1031_Selling_Agreement__c!=null)
{    
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'Pratyush.reddy@colereit.com'};
        mail.setToAddresses(toAddresses);
        mail.setCCAddresses(new String[]{'Pratyush.reddy@colereit.com'});
        mail.setSubject('My Subject');
        mail.setUseSignature(True);
        mail.setPlainTextBody('message');
        // Send the email
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
 }
 }     
    ApexPages.StandardController sc = new ApexPages.StandardController(con1);
    ApexPages.currentPage().getParameters().put('id',con1.Id);

      PageReference pageRef = sc.view();
      ContactCheckBox controller = new ContactCheckBox(sc) ;
      controller.changeSection();
      controller.save();
      controller.cancel();
    test.stopTest();
    
    
    
}
        
    }