global virtual with sharing class ocms_BusinessSummaryNew extends cms.ContentTemplateController
{
    private list<SObject> ProductList;
    private list<SObject> ProductList1;
    private String html;
   
  
    public list<SObject> getProductList()
    {
        list<SObject> ProductList;
        User u = [Select Id, firstname, lastname, contactId from user where Id=:UserInfo.getUserId()];
        string cid=u.contactid ;
        ProductList=[select id,CCPT_V__c,CCIT__c,Income_NAV__c from Contact WHERE Id=:cid];
        return ProductList;
       
    }
    
    public list<SObject> getProductList1()
    {
        list<SObject> ProductList;
        user u = [Select Id, firstname, lastname, contactId from user where Id=:UserInfo.getUserId()];
        string cid=u.contactid ;
        ProductList=[select id,CCPT_V__c,CCIT__c,Income_NAV__c from Contact WHERE Id=:cid];
        return ProductList;
       
    }
    
    @TestVisible private String writeControls()
    {
        String html = '';
        return html;
    }
    @TestVisible private String writeListView()
    {
         String html = '';
         
        
        html += '<article class="topMar7 wrapper teer3">'+
        '<h1>MY COLE BUSINESS SUMMARY</h1>'+
        '<p class="clear"><strong>Note:</strong> This summary may not be a complete record of your Cole business. To view your clients accounts and the most current and complete information, please login to DST Vision or contact your Cole team or the Cole Sales Desk at <strong>(866) 341-2653.</strong></p> &nbsp;'+
        '<div class="column-50">&nbsp;'+
        '<table cellpadding="0" cellspacing="0" class="table table-border tabheight40">'+
        '<tbody>'+    
        '<tr>'+
        '<td><strong>Investment Program</strong></td>'+
        '<td><strong>Total</strong></td>'+
        '</tr>'+
        '<tr>'+
        '<td>Cole Credit Property Trust V, Inc.</td>'+
        '<td>'+ProductList[0].get('CCPT_V__c')+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Cole Corporate Income Trust, Inc.</td>'+
        '<td>'+ProductList[0].get('CCIT__c')+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Cole Real Estate Income Strategy (Daily NAV), Inc.</td>'+
        '<td>'+ProductList[0].get('Income_NAV__c')+'</td>'+
        '</tr>'+
        '</tbody>'+
        '</table>'+
        '</div>'+
        '<div class="column-50">'+
        '<div id="canvas-holder"><canvas height="300" id="chart-area" style="margin-left:40px;" width="300"> </canvas></div>'+
        '</div>'+
        '</div>'+
        '</article>';
        
         return html;
    }
     
    @TestVisible private String writeListView1()
    {
         String html = '';
         
        
        html += '<article class="topMar7 wrapper teer3">'+
        '<h1>MY COLE BUSINESS SUMMARY1 (with hardcoding ID)</h1>'+
        '<p class="clear"><strong>Note:</strong> This summary may not be a complete record of your Cole business. To view your clients accounts and the most current and complete information, please login to DST Vision or contact your Cole team or the Cole Sales Desk at <strong>(866) 341-2653.</strong></p> &nbsp;'+
        '<div class="column-50">&nbsp;'+
        '<table cellpadding="0" cellspacing="0" class="table table-border tabheight40">'+
        '<tbody>'+    
        '<tr>'+
        '<td><strong>Investment Program</strong></td>'+
        '<td><strong>Total</strong></td>'+
        '</tr>'+
        '<tr>'+
        '<td>Cole Credit Property Trust V, Inc.</td>'+
        '<td>'+ProductList1[0].get('CCPT_V__c')+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Cole Corporate Income Trust, Inc.</td>'+
        '<td>'+ProductList1[0].get('CCIT__c')+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Cole Real Estate Income Strategy (Daily NAV), Inc.</td>'+
        '<td>'+ProductList1[0].get('Income_NAV__c')+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>User ID</td>'+
        '<td>'+UserInfo.getUserId()+'</td>'+
        '</tr>'+
        '</tbody>'+
        '</table>'+
        '</div>'+
        '<div class="column-50">'+
        '<div id="canvas-holder"><canvas height="300" id="chart-area" style="margin-left:40px;" width="300"> </canvas></div>'+
        '</div>'+
        '</div>'+
        '</article>';
        
         return html;
    }
     
    @TestVisible private String writeMapView()
    {
         return '';
    }
        
    global override virtual String getHTML()
    {
            String html = '';
            
            html += writeControls();
            ProductList= getProductList();
            if(ProductList.size()>0)
            {
            html += writeListView();
            }
            else
            {
            ProductList1= getProductList1();
            html += writeListView1();
            }
            return html;
    }   
}