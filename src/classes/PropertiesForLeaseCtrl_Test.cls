@isTest(SeeAllData=true)
private class PropertiesForLeaseCtrl_Test{
    
    @isTest static void test_getResultTotal() {
        
        PropertiesForLeaseCtrl propertyList = new PropertiesForLeaseCtrl();

        String state = 'TX';
        String city = '';
        String[] filterArray1 = new List<String>();
        filterArray1.add('Power Centers');
        String[] filterArray2 = new List<String>();
        filterArray2.add('1');
        propertyList.getResultTotal(state,city,filterArray1 ,filterArray2 );
        filterArray2.add('2501');
        propertyList.getResultTotal(state,city,filterArray1 ,filterArray2 );
        /*filterArray2.add('50001');
        propertyList.getResultTotal(state,city,filterArray1 ,filterArray2 );
        filterArray2.add('10001');
        propertyList.getResultTotal(state,city,filterArray1 ,filterArray2 );
        filterArray2.add('15001');
        propertyList.getResultTotal(state,city,filterArray1 ,filterArray2 );
        filterArray2.add('25001');
        propertyList.getResultTotal(state,city,filterArray1 ,filterArray2 );*/

        state = 'TX';
        city = 'Houston';      
        propertyList.getResultTotal(state,city,filterArray1 ,filterArray2);

        system.assert(propertyList != Null );
    }
    
    @isTest static void test_getPropertyList() {

        PropertiesForLeaseCtrl propertyList = new PropertiesForLeaseCtrl ();

        String state = 'TX';
        String city = 'Houston';
         String[] filterArray1 = new List<String>();
        filterArray1.add('Power Centers');
        String[] filterArray2 = new List<String>();
        filterArray2.add('1');
        propertyList.getPropertyList(state,city,'Name', '25', '0',filterArray1,filterArray2); 
        filterArray2.add('2501');
        propertyList.getPropertyList(state,city,'Name', '25', '0',filterArray1,filterArray2);
        /*filterArray2.add('50001');
        propertyList.getPropertyList(state,city,'Name', '25', '0',filterArray1,filterArray2);
        filterArray2.add('10001');
        propertyList.getPropertyList(state,city,'Name', '25', '0',filterArray1,filterArray2);
        filterArray2.add('15001');
        propertyList.getPropertyList(state,city,'Name', '25', '0',filterArray1,filterArray2);
        filterArray2.add('25001');
        propertyList.getPropertyList(state,city,'Name', '25', '0',filterArray1,filterArray2);*/
        
         system.assert(propertyList != Null );

    }

    @isTest static void test_getMethods() {

        PropertiesForLeaseCtrl propertyList = new PropertiesForLeaseCtrl ();

     //   propertyList.getsqft('TX','Houston','Name','25', '0');

        propertyList.getStateList('TX');

        propertyList.getCityList('TX');
        
          system.assert(propertyList != Null );

        
    }

   
    
}