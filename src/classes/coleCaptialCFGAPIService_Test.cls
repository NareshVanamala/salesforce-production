@isTest (SeeAllData=true)
private class coleCaptialCFGAPIService_Test{
    
    @isTest static void test_method_one() {
        try{
         coleCaptialCFGAPIService  wps = new coleCaptialCFGAPIService ();
         wps.gettype();
            
         list<contact> conList = new list<contact> ();
         conList = [select id,Accountid,mailingstate,RecordType.Name,Name from contact limit 1];
         String contactId = conList[0].id;
         Forms_Literature_List__c literatureList = new Forms_Literature_List__c();
         literatureList.Contact__c = contactId;
         literatureList.List_Name__c ='Testing ListName';
         insert literatureList;
         list<Forms_Literature_Bookmarks__c> bookMarkList = new list<Forms_Literature_Bookmarks__c>();
         Forms_Literature_Bookmarks__c  bookmarkObj = new Forms_Literature_Bookmarks__c();
         bookmarkObj.Title__c = 'title';                   
         bookmarkObj.Description__c = 'Description';
         bookmarkObj.Contact__c = contactId;
         bookmarkObj.Download_File_URL__c = 'Download File';                   
         bookmarkObj.Max_Order__c = 20;
         bookmarkObj.Published_Date__c = system.Today();                     
         bookmarkObj.Expiration_Date__c = '06/01/2016';
         bookmarkObj.FINRA_PDF__c = 'Finra PDF';
         bookmarkObj.Forms_Literature_List__c = literatureList.id;
         bookmarkObj.External_Id__c = contactId + bookmarkObj.Forms_Literature_List__c;
         bookMarkList.add(bookmarkObj);   
         insert bookMarkList;
         
         Forms_Literature_Shopping_Cart__c  cartObj = new Forms_Literature_Shopping_Cart__c();
         cartObj.Title__c = 'title';                   
         cartObj.Description__c = 'Description';
         cartObj.Contact__c = contactId;
         cartObj.Download_File_URL__c = 'Download File';                   
         cartObj.Max_Order__c = 20;
         cartObj.Published_Date__c = '06/01/2016';                     
         cartObj.Expiration_Date__c = '06/01/2016';
         cartObj.FINRA_PDF__c = 'Finra PDF';
         //cartObj.Forms_Literature_List__c = literatureList.id;
         cartObj.External_Id__c = contactId;   
         insert cartObj;
         
         System.assertNotEquals(bookmarkObj.Title__c,cartObj.Description__c);

         coleCaptialCFGAPICtrl coleCtrl = new coleCaptialCFGAPICtrl();
         coleCtrl.contactId =  conList[0].id;
         coleCtrl.accountId =  conList[0].Accountid;
         coleCtrl.mailingState = conList[0].mailingState;
         coleCtrl.usergroup =  conList[0].RecordType.Name.replaceAll(' ','%20'); 

        Map<String,String> sendMap = new Map<String,String>();
        sendMap.put('action','getcoleCaptialCFGAPIList');
        sendMap.put('pageName','pagename');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getsaveCartList');
        sendMap.put('cartDetails','BENEFITING FROM REAL ESTATE EXPERTS AND A PROVEN PROCESS BROCHURE*&INVESTOR APPROVED. NO STATE RESTRICTIONS.*&https://mp-advisor.dstweb.com/library/showfile?productID=97EFC2EA121C0899F2678287C7498113&attachment=1*&250*&5*&BRO-00CREE-0002*&https://mp-advisor.dstweb.com/inventory_images/cole_th_BRO-CREE-02.jpg*&5/23/2016*&12/31/1969*&*---*https://mp-advisor.dstweb.com/library/showfile?sku=A6F4C4F1CBF8544B8DD6DB7724309DF4&attachment=0*&false');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getCartList');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','removeItemFromCart');
        sendMap.put('cartObjId','testing');
        wps.executeRequest(sendMap);
        
        sendMap.clear();
        sendMap.put('action','removeItemFromBookmarks');
        sendMap.put('bookmarkObjId','testing');
        wps.executeRequest(sendMap);
        
        sendMap.clear();
        sendMap.put('action','removeLiteratureList');
        sendMap.put('listId','testing');
        wps.executeRequest(sendMap);
        
        sendMap.clear();
        sendMap.put('action','UpdateItemInCart');
        sendMap.put('updateCartValue',cartObj.id+',5,50');
        wps.executeRequest(sendMap);
        
        sendMap.clear();
        sendMap.put('action','getBookMarkList');
        sendMap.put('bookMarkDetails','Default*&CCPT V KIT - INVESTOR - GENERAL*&MAIL DIRECTLY TO CLIENT/INVESTOR. NOT FOR USE IN MA or OH.*&*&50*&CCPT5-KIT-INV*&https://mp-advisor.dstweb.com/inventory_images/cole_th_CCPT5-KIT-INV.jpg*&6/1/2016*&12/31/1969*&*&false');
        wps.executeRequest(sendMap);
        
        sendMap.clear();
        sendMap.put('action','createLiteratureList');
        sendMap.put('listId','testing');
        wps.executeRequest(sendMap);
        
        sendMap.clear();
        sendMap.put('action','renameLiteratureList');
        sendMap.put('literatureListName',literatureList.id+'*&Testing');
        wps.executeRequest(sendMap);
        
        sendMap.clear();
        sendMap.put('action','getBookmarkData');
        wps.executeRequest(sendMap);
        
        sendMap.clear();
        sendMap.put('action','getBookMarks');
        sendMap.put('listName',literatureList.id+'*&title');
        wps.executeRequest(sendMap);
        
        sendMap.clear();
        sendMap.put('action','getContactData');
        wps.executeRequest(sendMap);
        
        sendMap.clear();
        sendMap.put('action','getSubmitOrder');
        sendMap.put('jsonBody','testing56445');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getsendEmail');
        sendMap.put('emailListDetails','sample@gmail.com*&testing*&BENEFITING FROM REAL ESTATE EXPERTS AND A PROVEN PROCESS BROCHURE$$&&https://mp-advisor.dstweb.com/library/showfile?productID=97EFC2EA121C0899F2678287C7498113&attachment=1$$&&');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','updateAllItemsInCart');
        sendMap.put('updateAllCartValues',cartObj.id+',5,50');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getBookmarkName');
        sendMap.put('withskuName','testing56445');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getBookmarkListsWithOutSKU');
        sendMap.put('withOutskuName','testing56445');
        wps.executeRequest(sendMap);
        
        sendMap.clear();
        sendMap.put('action','addToMultipleBookMarks');
        sendMap.put('bookMarkJson','testing56445*&CCPT V INVESTOR PRESENTATION*--*APPROVED FOR GENERAL PUBLIC. NOT FOR USE IN MA or OH.*--*https://mp-advisor.dstweb.com/library/showfile?productID=C9F1E0B3B55E2F04C5741029918EFDDD&attachment=1*--*0*--*PT-CCPT5-INV-05*--*https://mp-advisor.dstweb.com//includes/images/altProdImage.gif*--*5/23/2016*--*12/31/1969*--**--*https://mp-advisor.dstweb.com/library/showfile?sku=B8A6EC9AA107724C87AB2A19790BE9A1&attachment=0*--**--;--*CCPT V SUPPLEMENT NO. 2 - 5.20.16*--*Dated 5.20.16*--*https://mp-advisor.dstweb.com/library/showfile?productID=DEEA2992A01CBEB0E28A85D3913D0F81&attachment=1*--*30*--*CCPT5-SUP-2D*--*https://mp-advisor.dstweb.com/inventory_images/cole_th_CCPT5-SUP-2D.jpg*--*5/23/2016*--*12/31/1969*--**--*testing*--*testing*--*');
        wps.executeRequest(sendMap);
        
        
        wps.loadResponse();
        }
        catch(Exception e){
            
        }
    }
    
   
}