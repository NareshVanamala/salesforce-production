@isTest(seealldata=true)
public with sharing class test_campaignmemberstatuscontroller {
 
  static testMethod void testcampaignmemberstatuscontroller() {
  try {
    boolean abc=true;
 Profile pro = [select Id from Profile where Name = 'system administrator'];
    
    User user = new User();
    user.FirstName = 'Test2222';
    user.LastName = 'Guy2222';
    user.Phone = '0123456789';
    user.Email = 'testguy2222@testyourcode.com';
    user.CommunityNickname = 'testguy2222@testyourcode.com';
    user.Alias = 'HelloMan';
    user.UserName = 'testguy2222@testyourcode.com';
    user.ProfileId =  pro.Id;
    user.TimeZoneSidKey = 'GMT';
    user.LocaleSidKey = 'en_US';
    user.EmailEncodingKey = 'ISO-8859-1';
    user.LanguageLocaleKey = 'en_US';

    insert user;
    
    Campaign cm1=new Campaign();
    cm1.name='test';
    insert cm1;
    
    
    webcast__c wc=new webcast__c();
    wc.name='test';
    wc.Campaigns__c=cm1.id;
    insert wc;
    system.assertequals(wc.Campaigns__c,cm1.id);


    Account a = new Account(Name='Test Account Name');
      insert a;

    Contact c = new Contact(LastName = 'Contact Last Name',Internal_Wholesaler__c='test',Wholesaler__c='test',Regional_Territory_Manager__c='test', AccountId = a.id);
    c.Email = 'testguy3@testyourcode.com';
    insert c;
    
    CampaignMember cm=new CampaignMember();
    cm.contactid=c.id;
    cm.Campaignid=cm1.id;
    cm.status='RSVP';
    insert cm;
    
     ApexPages.currentPage().getParameters().put('str',wc.id);
    campaignmemberstatuscontroller sc=new campaignmemberstatuscontroller();
  
  try
  {  sc.abc=false;
     sc.updatestatus();
  }
  catch(exception e){} 
  }
  catch(exception e){
  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Pleae make sure that you have a contact on your name!'));
     
  } 
   }
  
  static testMethod void testcampaignmemberstatuscontroller1() {
  try {
    boolean abc=true;
 Profile pro = [select Id from Profile where Name = 'system administrator'];
    
    User user = new User();
    user.FirstName = 'Test2222';
    user.LastName = 'Guy2222';
    user.Phone = '0123456789';
    user.Email = 'testguy2222@testyourcode.com';
    user.CommunityNickname = 'testguy2222@testyourcode.com';
    user.Alias = 'HelloMan';
    user.UserName = 'testguy2222@testyourcode.com';
    user.ProfileId =  pro.Id;
    user.TimeZoneSidKey = 'GMT';
    user.LocaleSidKey = 'en_US';
    user.EmailEncodingKey = 'ISO-8859-1';
    user.LanguageLocaleKey = 'en_US';
    insert user;
    
    system.assertequals(user.ProfileId,pro.Id);

    
    Campaign cm1=new Campaign();
    cm1.name='test';
    insert cm1;

    webcast__c wc=new webcast__c();
    wc.name='test';
    wc.Campaigns__c=cm1.id;
    insert wc;
   
    Account a = new Account(Name='Test Account Name');
    insert a;

    Contact c = new Contact(LastName = 'Contact Last Name',Internal_Wholesaler__c='test',Wholesaler__c='test',Regional_Territory_Manager__c='test', AccountId = a.id);
    c.Email = 'testguy3@testyourcode.com';
    insert c;
    
    system.assertequals(c.AccountId,a.id);

    CampaignMember cm=new CampaignMember();
    cm.contactid=c.id;
    cm.Campaignid=cm1.id;
    cm.status='Registered';
    insert cm;
    
     ApexPages.currentPage().getParameters().put('id',wc.id);
    campaignmemberstatuscontroller sc=new campaignmemberstatuscontroller();
  
  try
  {  sc.abc=true;
     sc.updatestatus();
  }
  catch(exception e){} 
  }
  catch(exception e){
  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Pleae make sure that you have a contact on your name!'));
     
  } 
   }
    
}