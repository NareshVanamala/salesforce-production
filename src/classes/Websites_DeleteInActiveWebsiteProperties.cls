public with sharing class Websites_DeleteInActiveWebsiteProperties{
    
    @InvocableMethod
    public static void deleteInActiveWebsiteProperties(List<Id> propRecordIds) {
        list<Website_Properties__c> websiteProperties= new list<Website_Properties__c>();
        websiteProperties = [Select id from Website_Properties__c where MRI_Property__c in: propRecordIds];
        system.debug('websiteProperties'+websiteProperties);
        if(websiteProperties.size()>0){
          if(Schema.sObjectType.Website_Properties__c.isDeletable())         
            delete websiteProperties;
        }
        
                
    }
}