public with sharing class TenantDetailctrl{
    Map<id,Set<Id>> propLeaseIdSetMap = new Map<Id,Set<Id>>();
    Set<Id> leaseIdSet = new Set<Id>();
    public Map<id,Property_Contacts__c>  properyContactMap{get;set;}
    Map<Id,Lease__c> leaseMap = new Map<Id,Lease__c>();
    public Map<id,List<Lease__c>> propLeaseListMap{get;set;}
    private List<Property_Contact_Leases__c> propConLeaseList {get;set;}
    private list<Lease_Sales_Data__c> grosssales {get;set;}
    private list<Financial_Data__c> Financialssales {get;set;}
    public  map<string,set<Lease_Sales_Data__c>> grossSalesListMap{get;set;}
    public  map<string,set<Financial_Data__c>> financeListMap{get;set;}
    public string propContactId{get;set;}
    public Integer count {get;set;}
    public Integer index = 15;
    public Integer start = 0;  
    public Boolean nextBool {get;set;}
    public Boolean prevBool {get;set;}
    public string SelectedavgLeaseTerm{get;set;}
    public String propFund{get;set;}
    public map<string,string> PropConfilters {get;set;}
    public Boolean isError{get;set;}
    public Integer CurrentYear {get;set;} 
    public Integer PreviousYear {get;set;} 
    public Integer twoYearsBack {get;set;} 
    public Integer threeYearsBack {get;set;} 
    public Integer fourYearsBack {get;set;}
    public string tenantName {get;set;} 
    public string propCronSearch {get;set;}
    public string propManager {get;set;}
    public string propId{get;set;}
    public string sample {get;set;}
    public List<string> PropConOrderedlist {get;set;} 
    public string PropContactName{get;set;}
    public TenantDetailctrl(){
        propLeaseListMap = new Map<id,List<Lease__c>>();
        properyContactMap = new Map<id,Property_Contacts__c>();
        propConLeaseList = new List<Property_Contact_Leases__c>();
        grossSalesListMap = new map<string,set<Lease_Sales_Data__c>>();
        financeListMap = new map<string,set<Financial_Data__c>>();
        Financialssales = new list<Financial_Data__c>();
        grosssales = new  list<Lease_Sales_Data__c>();
        PropConfilters = new map<string,string>();
        PropConOrderedlist = new List<string> ();
        prevBool = true;
        nextBool = false;
        isError = false;
        CurrentYear = Date.Today().Year();
        PreviousYear = CurrentYear-1;
        twoYearsBack = PreviousYear-1;
        threeYearsBack = twoYearsBack-1;
        fourYearsBack = threeYearsBack-1;
        propContactId = Apexpages.Currentpage().getparameters().get('id');
       // propcontactsfilter(prevBool,nextBool,index,start,propFund,SelectedavgLeaseTerm,propContactId,tenantName,propCronSearch,propManager,propId);
      
     }
      public List<SelectOption> getFunds() {
        List<SelectOption> fundsOptions = new List<SelectOption>();
       //  fundsOptions.add(new SelectOption('None','None'));
        for (Fund__c fundObj : Fund__c.getAll().values()){
            fundsOptions.add(new SelectOption(fundObj.Name,fundObj.Name));
        }
        return fundsOptions ;
    }
    public List<SelectOption> getAvgLeaseTerm() {
        List<SelectOption> avgLeaseTermOptions = new List<SelectOption>();
           //avgLeaseTermOptions.add(new SelectOption('0','None'));
        for (Avgleaseterm__c avgLeaseObj : Avgleaseterm__c.getAll().values()){
           avgLeaseTermOptions.add(new SelectOption(avgLeaseObj.Name,avgLeaseObj.Avg_Lease_Term__c));
        }
       return avgLeaseTermOptions ;
    }
    public list<SelectOption> getPropManagers(){
        List<SelectOption> propManagerOptions = new List<SelectOption>();
         set<SelectOption> propManagerOptionSet = new set<SelectOption>();
        set<string> propManagersSet = new set<string>();
        List<AggregateResult> mriProp = new List<AggregateResult>();
        mriProp  =  [select Property_Manager__c propmanagerId,Property_Manager__r.Name propmanagername from MRI_Property__c where Property_Manager__c!=null group by Property_Manager__c,Property_Manager__r.Name order by Property_Manager__r.Name asc limit 10000];
        propManagerOptions.add(new selectOption('', ' None '));
        for(AggregateResult aggResult : mriProp){
             propManagerOptions.add(new SelectOption(String.valueOf(aggResult.get('propmanagerId')),String.valueOf(aggResult.get('propmanagername'))));
        }
        return propManagerOptions;
    }
     
      public void doSearch(){
       grossSalesListMap.clear();
        propLeaseListMap.clear();
        propConLeaseList.clear();
        propLeaseIdSetMap.clear();
        financeListMap.clear();
        PropConOrderedlist.clear();
        PropConfilters.clear();
        start = 0;
        index = 15;
        prevBool = true;
        nextBool = false;
        propcontactsfilter(prevBool,nextBool,index,start,propFund,SelectedavgLeaseTerm,propContactId,tenantName,propCronSearch,propManager,propId);
      }
      public void PropCronSearch(){
        grossSalesListMap.clear();
        propLeaseListMap.clear();
        propConLeaseList.clear();
        propLeaseIdSetMap.clear();
        financeListMap.clear();
        PropConOrderedlist.clear();
        PropConfilters.clear();
        start = 0;
        index = 15;
        prevBool = true;
        nextBool = false;
        propcontactsfilter(prevBool,nextBool,index,start,propFund,SelectedavgLeaseTerm,propContactId,tenantName,propCronSearch,propManager,propId);
      }
      public void tosave() { 
        List<Lease_Sales_Data__c> updateLeaseSales = new List<Lease_Sales_Data__c>();
        List<Financial_Data__c> updateFinanceData = new List<Financial_Data__c>();
        updateLeaseSales.clear();
        updateFinanceData.clear(); 
        if(updateFinanceData.isEmpty() ||(updateFinanceData==null && updateFinanceData.size()>0)){
           for(string financedata : financeListMap.keyset()){
                updateFinanceData.addAll(financeListMap.get(financedata));
            }
           
         if(updateFinanceData.size()>0){
             update updateFinanceData;
            }
          Financialssales.clear(); 
            } 
            List<Lease__C> updateLeaseList = new List<Lease__C>();
            set<Lease__C> updatedleaseset = new set<Lease__C>();
            List<Lease__C> latestLeaseList = new List<Lease__C>();      
            if(propLeaseListMap !=null && propLeaseListMap.size()>0){
                for(List<lease__C> leaseList: propLeaseListMap.Values()){
                    updateLeaseList.addAll(leaseList);
                }
                
                for(Lease__c leaseObj : updateLeaseList){
                    updatedleaseset.add(leaseObj);
                }
                latestLeaseList.addAll(updatedleaseset);
                update latestLeaseList;
            }
            latestLeaseList.clear();
            
          }

    Public PageReference generateExcel(){ 
        PageReference pageRef = null;
        pageRef =  new PageReference('/apex/GenerateExcel?id='+propContactId);
        pageRef.setRedirect(true);
        return pageRef; 
          }
    public PageReference CurrentPage(){
        PageReference currPage = Page.TenantDetails;
        currPage.setRedirect(true);
        return currPage; 
    }
    Public void exportToExcel(){
         propcontactsfilter(prevBool,nextBool,index,start,propFund,SelectedavgLeaseTerm,propContactId,tenantName,propCronSearch,propManager,propId);
         set<string> propconsets = new set<string>();
         if(PropConOrderedlist!=null && PropConOrderedlist.size()>0){
             propconsets.addAll(PropConOrderedlist);
              for(Property_Contacts__c propContObj:[select id,name from Property_Contacts__c where id in:propconsets order by name asc]){
                   PropContactName = propContObj.name;
                }
         }      
    }
    public void next(){   
        grossSalesListMap.clear();
        propLeaseListMap.clear();
        propConLeaseList.clear();
        propLeaseIdSetMap.clear();
        financeListMap.clear();
        PropConOrderedlist.clear();
        PropConfilters.clear();
        index = index + 15;
        start = start + 15;
        system.debug('next count values'+count);
        if(index > count)
        {   system.debug('next index values'+index);
            index = Math.Mod(count,15) + start;
            system.debug('updated next index values'+index);
            propcontactsfilter(prevBool,nextBool,index,start,propFund,SelectedavgLeaseTerm,propContactId,tenantName,propCronSearch,propManager,propId);
            nextBool = true;
            prevBool = false;        
            index = start + 15;  
            system.debug('last updated next index values'+index);           
        }
        else
        {   
            propcontactsfilter(prevBool,nextBool,index,start,propFund,SelectedavgLeaseTerm,propContactId,tenantName,propCronSearch,propManager,propId);
            prevBool = false;
        } 
   }
    public void previous()
    {   grossSalesListMap.clear();
        propLeaseListMap.clear();
        propConLeaseList.clear();
        propLeaseIdSetMap.clear();
        financeListMap.clear();
        PropConOrderedlist.clear();
        PropConfilters.clear();
        if(start > 15)
        {  
            index = index - 15;
            start = start - 15;  
            propcontactsfilter(prevBool,nextBool,index,start,propFund,SelectedavgLeaseTerm,propContactId,tenantName,propCronSearch,propManager,propId);
            prevBool = false;
            nextBool = false;
        }    
        else
        {  
            index = index - 15;
            start = start - 15;
            propcontactsfilter(prevBool,nextBool,index,start,propFund,SelectedavgLeaseTerm,propContactId,tenantName,propCronSearch,propManager,propId);
            prevBool = true;
            nextBool = false;        
        }   
    }
    public void propcontactsfilter(boolean prevBool,Boolean nextBool,integer index,integer start,string propFund,string SelectedavgLeaseTerm ,String PropCon,string tenName,string propConVar,string propMang,string propertyId){
   
        Integer AvgLeaseTerm;
        String PropertyContactName;
        String LeaseTenantName;
         system.debug('propFund'+propFund);
        system.debug('SelectedavgLeaseTerm123'+SelectedavgLeaseTerm );
        if(SelectedavgLeaseTerm !=null &&  SelectedavgLeaseTerm != 'None')
        {
              AvgLeaseTerm = integer.valueof(SelectedavgLeaseTerm);
              system.debug('AvgLeaseTerm123'+AvgLeaseTerm );
        }
       string  query = 'select id,Lease__c,Lease__r.Sales_Report_Required__c,Lease__r.Financials_Required__c,Lease__r.MRI_PROPERTY__r.Property_Manager__r.Name,Lease__r.MRI_PROPERTY__r.Property_Id__c,Property_Contacts__r.Name,';
               query+='Lease__r.MRI_PROPERTY__r.Fund1__c,Name,Property_Contacts__c,Contact_Type__c from Property_Contact_Leases__c ';
               query+='where (Lease__r.Financials_Required__c=true OR Lease__r.Sales_Report_Required__c=true) and Lease__r.Lease_Status__c = \'Active\' and Contact_Type__c includes(\'Financials;Gross Sales\',\'Financials\',\'Gross Sales\')';
            if(PropCon != null){
               query+=' and Property_Contacts__r.id =\''+String.escapeSingleQuotes(PropCon)+'\'';
            }
            if((propFund != null && propFund != 'None') && (AvgLeaseTerm != null && AvgLeaseTerm != 0)){
                query+=' and Lease__r.MRI_PROPERTY__r.Fund1__c =\''+String.escapeSingleQuotes(propFund)+'\'';
                query+=' and Lease__r.Remaining_Lease_Term__c >=:AvgLeaseTerm';
            }
            /* if((propFund == 'None') && (AvgLeaseTerm != null && AvgLeaseTerm != 0)){
                query+=' and Lease__r.Remaining_Lease_Term__c >=:AvgLeaseTerm';
            }  */
           /*  if((propFund == null || propFund == 'None') && (AvgLeaseTerm  == 'None')){
              
                query+=' and Lease__r.Remaining_Lease_Term__c >=:AvgLeaseTerm';
            } */
             if((propFund != null && propFund != 'None')){
                query+=' and Lease__r.MRI_PROPERTY__r.Fund1__c =\''+String.escapeSingleQuotes(propFund)+'\'';
            } 
            if(AvgLeaseTerm !=null && AvgLeaseTerm != 0){
                query+=' and Lease__r.Remaining_Lease_Term__c >=:AvgLeaseTerm';
                 
           }
            if(tenName !=null && tenName!=''){
                LeaseTenantName = tenName+'%';
                query+=' and Lease__r.Tenant_Name__c LIKE \''+String.escapeSingleQuotes(LeaseTenantName)+'\'';
            }
            if(propConVar !=null && propConVar!=''){
                PropertyContactName = propConVar+'%';
                query+=' and Property_Contacts__r.Name LIKE \''+String.escapeSingleQuotes(PropertyContactName)+'\'';
            }
            if(propMang !=null &&   propMang !=''){
                query+=' and Lease__r.MRI_PROPERTY__r.Property_Manager__c =\''+String.escapeSingleQuotes(propMang)+'\'';
            } 
            if(propId !=null && propId !=''){
                query+=' and Lease__r.MRI_PROPERTY__r.Property_Id__c =\''+String.escapeSingleQuotes(propId)+'\'';
            }
            
            query+=' order by Property_Contacts__r.Name asc';
            system.debug('dynamic query'+query);
            propConLeaseList = Database.query(query);      
        if(propConLeaseList!=null && propConLeaseList.size()>0){
            string contactType = '';
            for(Property_Contact_Leases__c propContObj:propConLeaseList){
                    leaseIdSet.add(propContObj.Lease__c);
                    PropConfilters.put(propContObj.Property_Contacts__c+'#'+propContObj.Lease__c,propContObj.Contact_Type__c);
                    system.debug('filters'+PropConfilters);
                    contactType = propContObj.Contact_Type__c;
                if((propContObj.Lease__r.Sales_Report_Required__c==true && (contactType.contains('Gross Sales'))) || (propContObj.Lease__r.Financials_Required__c==true && (contactType.contains('Financials')))){
                     if(propLeaseIdSetMap.get(propContObj.Property_Contacts__c)!=null){
                        propLeaseIdSetMap.get(propContObj.Property_Contacts__c).add(propContObj.Lease__c);    
                    }
                    else{
                        propLeaseIdSetMap.put(propContObj.Property_Contacts__c,new Set<Id>{propContObj.Lease__c});
                    }
                }       
                   
            }
             set<string> leaseMasteroccpIds =  new set<string>();
            if(leaseIdSet!=null && leaseIdSet.size()>0){
                for(Lease__c leaseObj:[Select Id,Year_Tab_1__c,Year_Tab_2__c,Year_Tab_3__c,Year_Tab_4__c,Year_Tab_5__c,Acquisition_Date__c,Name,MRI_Lease_ID__c,Store__c,MasterOccupant_ID__c,Last_Reported_Date__c,Lease_ID__c,Financials_Type__c,Sales_Frequency__c,Financials_Frequency__c,Tenant_Name__c,MRI_PROPERTY__r.Name,MRI_PROPERTY__r.Property_Manager__r.Name,MRI_PROPERTY__r.Property_ID__c,Sales_Report_Required__c,Lease_Status__c,Financials_Required__c,(select id,Contact_Type__c,Lease__c,Property_Contacts__c from Property_Contact_Leases__r) from Lease__c where id in:leaseIdSet and (Financials_Required__c=true OR Sales_Report_Required__c=true) and Lease_Status__c = 'Active']){
                        for(Property_Contact_Leases__c proConLeaseObj : leaseObj.Property_Contact_Leases__r){
                            string filterValue = '';
                            if(PropConfilters.get(proConLeaseObj.Property_Contacts__c+'#'+proConLeaseObj.Lease__c) !=null){
                                filterValue = PropConfilters.get(proConLeaseObj.Property_Contacts__c+'#'+proConLeaseObj.Lease__c);
                                if((leaseObj.Sales_Report_Required__c==true && (filterValue.contains('Gross Sales'))) || ( leaseObj.Financials_Required__c==true && (filterValue.contains('Financials')))){
                                leaseMap.put(leaseObj.id,leaseObj);
                                leaseMasteroccpIds.add(leaseObj.MasterOccupant_ID__c);
                            }
                            }
                     }  
                }
            }
             if(leaseMasteroccpIds !=null && leaseMasteroccpIds.size()>0){
                          grosssales = [Select id,Period__c,Month__c,lease__r.MasterOccupant_ID__c,lease__r.id,Sales_Collected__c,Sales_Collection__c,Period_Year__c from Lease_Sales_Data__c where Lease__r.MasterOccupant_ID__c in :leaseMasteroccpIds and SalesType__c = 'GROSS' and Period_Year__c >=: fourYearsBack order by Period__c asc];
                          Financialssales = [Select id,Year__c,Period_Year__c,Month__c ,Period__c,Fins_Collected__c,Lease__r.id from Financial_Data__c where Lease__r.id in :leaseIdSet and Period_Year__c >=: fourYearsBack and Period_Year__c <=: CurrentYear order by Period__c desc];
                          for(Lease_Sales_Data__c leaseSalObj:grosssales){
                                if(grossSalesListMap.get(leaseSalObj.lease__r.MasterOccupant_ID__c)!=null){
                                    grossSalesListMap.get(leaseSalObj.lease__r.MasterOccupant_ID__c).add(leaseSalObj);
                                }
                                else{
                                    grossSalesListMap.put(leaseSalObj.lease__r.MasterOccupant_ID__c,new set<Lease_Sales_Data__c>{leaseSalObj});
                                    system.debug('grossSalesListMap2'+ grossSalesListMap);
                                }
                               }
                         for(string leaseId:leaseMasteroccpIds){
                                if(grossSalesListMap.get(leaseId)==null){
                                    grossSalesListMap.put(leaseId,new set<Lease_Sales_Data__c>{});
                                }
                         }
                        
                         for(Financial_Data__c financeObj:Financialssales){
                                if(financeListMap.get(financeObj.lease__r.Id)!=null){
                                    financeListMap.get(financeObj.lease__r.Id).add(financeObj);
                                }
                                else{
                                    financeListMap.put(financeObj.lease__r.Id,new set<Financial_Data__c>{financeObj});
                                    
                                }
                          }
                          for(string leaseId:leaseIdSet){
                                if(financeListMap.get(leaseId)==null){
                                    financeListMap.put(leaseId,new set<Financial_Data__c>{});
                                }
                         }
                    
                    }
            if(propLeaseIdSetMap!=null && propLeaseIdSetMap.size()>0){
                    set<id> PropertyContactSet = new set<id>();
                    set<id> tempPropertyContactSet = new set<id>();
                    list<id> tempPropertyContactlist = new list<id>();
                    tempPropertyContactlist.clear();
                    tempPropertyContactSet.clear();
                    PropertyContactSet.clear();
                    PropConOrderedlist.clear();
                    tempPropertyContactSet = propLeaseIdSetMap.keyset();
                    count = tempPropertyContactSet.size();
                    tempPropertyContactlist.addAll(tempPropertyContactSet);
                    tempPropertyContactlist.sort();
                    system.debug('start value==='+start);
                    if(start==-15){
                        start = 0;
                    }
                    system.debug('start value'+start);
                    system.debug('count value'+count);
                    if(count<index){
                        system.debug('index value'+index);
                        index = Math.Mod(count,15) + start;
                        system.debug('updated index value'+index);
                    }
                     for(Integer i = start; i<index; i++)
                    {   if(tempPropertyContactlist.get(i)!=null) 
                        PropertyContactSet.add(tempPropertyContactlist.get(i));
                    }
                for(Property_Contacts__c propContObj:[select id,name,Last_Request__c,Last_Request_Notes__c,Email_Address__c,Phone__c from Property_Contacts__c where id in:PropertyContactSet order by name asc]){
                    properyContactMap.put(propContObj.id,propContObj);
                    PropConOrderedlist.add(propContObj.id);
                    
                }
                system.debug('properyContactMap'+properyContactMap.size());
                for(Id propContId:PropertyContactSet){
                     if(propLeaseIdSetMap.get(propContid)!=null && propLeaseIdSetMap.get(propContid).size()>0){
                        List<lease__c> leaseList = new List<Lease__c>();
                        for(Id leaseId:propLeaseIdSetMap.get(propContid)){
                            leaseList.add(leaseMap.get(leaseId));
                        }
                        leaseList.sort();
                        propLeaseListMap.put(propContId,leaseList);
                     }
                }
                for(Id propContId:PropertyContactSet){
                    if(propLeaseListMap.get(propContId)==null){
                        propLeaseListMap.put(propContId,new List<Lease__c>{});
                    }
                }
                
            }
        }
                if((propLeaseListMap ==null && propLeaseListMap.size()==0)||(propLeaseListMap.isEmpty())){
                    isError = true;
                    system.debug('Display Error messages'+isError);
                }
       }
}