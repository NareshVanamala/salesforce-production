global virtual with sharing class WebPropertiesctrl extends cms.ContentTemplateController {
//extend the cms.ContentTemplateController for all custom content

// Constructor for Edit Page
global WebPropertiesctrl(cms.CreateContentController cc) {
 if(!test.isRunningTest())
    {
    super(cc);
    }
}


    // Default Constructor
    global WebPropertiesctrl(){}
 
    // getHTML method for returning programmatically generated HTML to the Generate Page
    global virtual override String getHTML() { // For generated markup 
        return '';
    }
 
    // Methods to get the attribute values for the content

 
     @remoteAction
    global static boolean getWebproperties(string webPropId,String brochureLink,String aerialLink,String sitePlanLink,String demographicsLink,string mainImage,string image1,string image2,string image3,string image4,string image5)
    {   System.debug('*******webPropId*********'+webPropId);
        List<Web_Properties__c> webPropList = new List<Web_Properties__c> ();
        for(Web_Properties__c webPropObj : [select id,name,Demographics__c,Brochure__c,Aerial__c,Site_Plan1__c,Main_Image__c,Image1__c,Image2__c,Image3__c,Image4__c,Image5__c from Web_Properties__c where id =:webPropId limit 1]){
            if(brochureLink!=null && brochureLink!='' && brochureLink!='[]' ){
                brochureLink = brochureLink.replace('[','');
                brochureLink = brochureLink.replace(']','');
                webPropObj.Brochure__c = brochureLink;
            }
             if(aerialLink!=null && aerialLink!='' && aerialLink!='[]'){
                aerialLink = aerialLink.replace('[','');
                aerialLink = aerialLink.replace(']','');
                webPropObj.Aerial__c = aerialLink;
            }
             if(sitePlanLink!=null && sitePlanLink!='' && sitePlanLink!='[]'){
                sitePlanLink = sitePlanLink.replace('[','');
                sitePlanLink = sitePlanLink.replace(']','');
                webPropObj.Site_Plan1__c = sitePlanLink;
            }
             if(demographicsLink!=null && demographicsLink!='' && demographicsLink!='[]'){
                demographicsLink = demographicsLink.replace('[','');
                demographicsLink = demographicsLink.replace(']','');
                webPropObj.Demographics__c = demographicsLink;
            }
            if(mainImage!=null && mainImage!='' && mainImage!='[]'){
                mainImage = mainImage.replace('[','');
                mainImage = mainImage.replace(']','');
                webPropObj.Main_Image__c = mainImage;
            }
             if(image1!=null && image1!='' && image1!='[]'){
                image1 = image1.replace('[','');
                image1 = image1.replace(']','');
                webPropObj.Image1__c = image1;
            }
             if(image2!=null && image2!=''&& image2!='[]'){
                image2 = image2.replace('[','');
                image2 = image2.replace(']','');
                webPropObj.Image2__c = image2;
            }
             if(image3!=null && image3!='' && image3!='[]'){
                image3 = image3.replace('[','');
                image3 = image3.replace(']','');
                webPropObj.Image3__c = image3;
            }
             if(image4!=null && image4!=''&& image4!='[]'){
                image4 = image4.replace('[','');
                image4 = image4.replace(']','');
                webPropObj.Image4__c = image4;
            }
            if(image5!=null && image5!=''&& image5!='[]'){
                image5 = image5.replace('[','');
                image5 = image5.replace(']','');
                webPropObj.Image5__c = image5;
            }
            webPropList.add(webPropObj);
        }
        try {
        update webPropList; 
    } catch (DmlException e) {
        System.debug(e.getMessage());
    }

        return true;
    }
    //Need to test if the site has a prefix in order to render the proper URL for the file since OrchestraCMS does not store that in the attribute
     public cms.Link BrochureLinkObj{
        get{
            return this.getPropertyLink('BrochureLinkObj');
        }
     }
      public cms.Link AerialLinkObj{
        get{
            return this.getPropertyLink('AerialLinkObj');
        }
     }
      public cms.Link SitePlanLinkObj{
        get{
            return this.getPropertyLink('SitePlanLinkObj');
        }
     }
      public cms.Link DemoGraphicsLinkObj{
        get{
            return this.getPropertyLink('DemoGraphicsLinkObj');
        }
     }
      
        public cms.Link DefaultWebProperties{
        get {       
            return this.getPropertyLink('DefaultWebProperties');
        }
    }   
    //Image
     public cms.Link MainImageLinkObj{
        get {       
            return this.getPropertyLink('MainImageLinkObj');
        }
    }   
     public cms.Link ImageLinkObj1{
        get {       
            return this.getPropertyLink('ImageLinkObj1');
        }
    }   
     public cms.Link ImageLinkObj2{
        get {       
            return this.getPropertyLink('ImageLinkObj2');
        }
    }    public cms.Link ImageLinkObj3{
        get {       
            return this.getPropertyLink('ImageLinkObj3');
        }
    }    public cms.Link ImageLinkObj4{
        get {       
            return this.getPropertyLink('ImageLinkObj4');
        }
    }   
    public cms.Link ImageLinkObj5{
        get {       
            return this.getPropertyLink('ImageLinkObj5');
        }
    }   
   
    

 
    //method for outputting the generated markup with a picture
 
}