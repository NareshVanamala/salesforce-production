@isTest
public class CP_AccProdclsTest {
static testMethod void CP_AccProdclsTest () {
CP_HelperAccConProdcls hp = new CP_HelperAccConProdcls ();
account acc= new Account();
RecordType rt = [select id,Name from RecordType where SobjectType='Account' and Name='Broker Dealer' Limit 1];
acc.recordTypeId=rt.id;
acc.Name='Test Account';
insert acc;
List<New_Product__c> npList = new List<New_Product__c>();

New_Product__c np = new New_Product__c();
np.As_of_Date_Text__c='test1';
//np.name='test';
npList.add(np);
New_Product__c np1 = new New_Product__c();
np1.As_of_Date_Text__c='test1';
npList.add(np1);

insert npList;

Account_Product__c ap = new Account_Product__c();
ap.Account__c=acc.id;
ap.Product__c=np.id;
insert ap;

PageReference pageRef = Page.CP_AccProd;
pageRef.getParameters().put('curid',acc.id);
Test.setCurrentPageReference(pageRef);
string str;
ApexPages.StandardController stdcon = new ApexPages.StandardController(np);
CP_AccProdcls cpa = new CP_AccProdcls(stdcon);
cpa.search();
System.assertEquals(null,str);
cpa.str='Test';
cpa.search();


//hp.selectprod.add(np1);
//cpa.addacco();
//System.assertEquals(true, hp.selectprod.size()>0);
List<New_Product__c> alist=hp.getMemb();
hp.Searchpro(np.name);
hp.Searchpro(np1.name);
hp.Searchpro(np.name);
boolean b3=hp.getprev();
boolean b4=hp.getnxt();
hp.beginning();
hp.previous();
hp.next();
hp.end();
boolean b=hp.getprev();
boolean b2=hp.getnxt();
CP_HelperAccConProdcls.productWrapperClass ww=new CP_HelperAccConProdcls.productWrapperClass(np,false,'test');
for (CP_HelperAccConProdcls.productWrapperClass  wc : hp.pwcList) {
    wc.np  = np;
    wc.selectedvalue=true;
    wc.status='test';
    wc.isFailed=false;
}
hp.addCon(ap.id, true);
hp.addCon(ap.id, false);
hp.addAcc(ap.id, true);
hp.isFailed=false;
hp.selectprod.add(np);
hp.selectprod.add(np1);
hp.Searchpro(np.id);
cpa.curid= ApexPages.CurrentPage().getParameters().put('id',ap.id);
hp.addAcc(ap.id, false);
//cpa.cls.isFailed=false;
//System.assertEquals(true, hp.selectprod.size()>0);
//System.assertEquals(true, cpa.cls.isFailed==false);
cpa.addacco();
cpa.Clear();
}
  static testMethod void CP_ConProdclsTest1 () {
        CP_HelperAccConProdcls hp = new CP_HelperAccConProdcls ();
        account acc = new account();
        acc.name='Test acc con';
        insert acc;

        Contact con= new Contact();
        con.FirstName='Test First Name';
        con.LastName='Test Last Name';
        con.AccountId=acc.id;
        insert con;
        
        New_Competitor__c nc = new New_Competitor__c();
        nc.name = 'test test';
        insert nc;

        List<New_Product__c> npList = new List<New_Product__c>();
        
        New_Product__c np = new New_Product__c();
        np.Average_Lease_term__c='lease';
        np.Competitor__c = nc.id;
        insert np;
        //npList.add(np);
        New_Product__c np1 = new New_Product__c();
        np1.Average_Lease_term__c='lease1';
        insert np1;
        //npList.add(np1);
        //insert npList; 

        Contact_Product__c cp = new Contact_Product__c();
        cp.Contact__c=con.id;
        cp.Product__c=np.id;
        insert cp;
        
        PageReference pageRef = Page.CP_AccProd;  
        ApexPages.currentPage().getParameters().put('Id',acc.id);
        Test.setCurrentPage(pageRef);       
        Boolean selected;
        string str;
        test.startTest();
        Test.setCurrentPage(Page.CP_AccProd);
        ApexPages.StandardController stdcon = new ApexPages.StandardController(np);
        CP_AccProdcls cpc = new CP_AccProdcls (stdcon);
        //cpc.add('ABC');

        cpc.search();
        System.assertEquals(null,str);
        cpc.str='Test';
        cpc.selected =true;
        cpc.search();
        cpc.cls.isFailed=true;
        cpc.curid = acc.id;
        cpc.cls.selectprod.add(np);
        cpc.cls.Searchpro(str);
        cpc.cls.pwcList[0].selectedValue = true;
        cpc.addacco();
        cpc.cls.pwcList[0].selectedValue = false;
        cpc.addacco();
        //hp.isFailed=true;
        hp.selectprod.add(np);
        hp.selectprod.add(np1);
        System.assertEquals(true,hp.selectprod.size()>0);
        cpc.Clear();
        test.stopTest();
    }


}