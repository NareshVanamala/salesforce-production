public with sharing class EC_SearchOrCreateEntityClass{


    public Entity__c objenty{get;set;}
    public List<Entity__c> results{get;set;} 
    public string searchString{get;set;} 
    public Deal__c objdeal{get;set;}
    public boolean refreshPage{get;set;}
    public String dealId{get;set;}
    public String entyId{get;set;}
    
    public EC_SearchOrCreateEntityClass() 
    {
        objenty=new Entity__c();
        objdeal=new Deal__c ();
        dealId = System.currentPageReference().getParameters().get('id');
        system.debug('dealId '+dealId );
        searchString = System.currentPageReference().getParameters().get('lksrch');
        runSearch();
        refreshPage= False;
    }
    
    public PageReference search() 
    {
        runSearch();
        return null;
    }
    private void runSearch() 
    {   
        results = performSearch(searchString);               
    } 
    private List<Entity__c> performSearch(string searchString) 
    {
        String soql = 'select id,name,Category__c,Entity_Type__c,Address__c,State__c,Status__c,Active_Entity__c,Current_Manager__c,Depositor__c,Workspace_Description__c,Workspace_Email__c from Entity__c';
        if(searchString != '' && searchString != null)
        soql = soql +  ' where name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\'';
        soql = soql + ' limit 100';
        System.debug(soql);
        return database.query(soql);  
    }

    public pagereference Save()
    {   
            if(objenty!=Null)
                insert objenty;
            if(dealId !=Null)
                objdeal.Id = dealId;
                system.debug('objenty '+objenty);
            if(objenty!=Null)
            {
                objdeal.Property_Owner_SPEL__c = objenty.id;
                if (Schema.sObjectType.Deal__c.isUpdateable()) 

                update objdeal;
                objenty=null; 
            }               
  PageReference p =new PageReference('/'+dealId);
  //refreshpage= True;
  p.setRedirect(true);
  return p;
   
   //return null;
    
    }
    public  pagereference dealUpd()
    {
            objdeal.Id = dealId ;
            objdeal.Property_Owner_SPEL__c = entyId;
            System.debug('objdeal----------'+objdeal);
            if (Schema.sObjectType.Deal__c.isUpdateable()) 

            update objdeal;
         PageReference DealPage =new PageReference('/'+objdeal.Id);
          //refreshpage= True;
          DealPage.setRedirect(true);
          return DealPage;
           
     }
    public PageReference CancelEntity() 
    {  
         objdeal.Id = dealId ;
         PageReference pg =new PageReference('/'+objdeal.Id);
        //Pagereference pg =  new Pagereference(system.Label.Domain_Name+dealId); 
        pg.setRedirect(true);
        return pg;
    }
    
    public string getFormTag() 
    {
        return System.currentPageReference().getParameters().get('frm');
    }
    
    public string getTextBox() 
    {
        return System.currentPageReference().getParameters().get('txt');
    }
}