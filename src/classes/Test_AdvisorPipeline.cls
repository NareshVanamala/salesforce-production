@isTest
public class Test_AdvisorPipeline{
static testMethod void AdvisorPipelineTest()
{
    Account ac = new Account();
    ac.name = 'testaccount';
    insert ac;  
     
    Contact ct = new Contact();
    ct.accountid = ac.id;
    ct.FirstName = 'Priya';
    ct.LastName ='COLE';
    ct.Advisor_Pipeline__c = 'Green';
    insert ct;
    
    System.assertEquals(ct.accountid, ac.id);

    Task t = new Task();
    t.whoid = ct.id;
    t.Advisor_Pipeline__c='Red';
    insert t;
    
    Task t1 = new Task();
    t1.whoid = ct.id;
    t1.Advisor_Pipeline__c='Green';
    insert t1;
    
    Task t2 = new Task();
    t2.whoid = ct.id;
    t2.Advisor_Pipeline__c='Yellow';
    insert t2;
    
    Task t3 = new Task();
    t3.whoid = ct.id;
    t3.Advisor_Pipeline__c='NQ';
    insert t3;
    
    ct.Advisor_Pipeline__c = 'Red';
    update ct;
}
}