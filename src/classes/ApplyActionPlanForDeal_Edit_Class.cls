public with sharing class ApplyActionPlanForDeal_Edit_Class 
{
    public String objectId{get;set;}
    public Template__c actionPlan{get;set;}
    public List<Task_Plan__c> actionPlanTasks{get;set;}
    public List<Task_Plan__c> actionPlansubTasks{get;set;}
    public Deal__c currentdeal{get;set;}
    public Loan__c currentLoan{get;set;}
    public Map<String,List<Task_Plan__c>> mapTask{get;set;}
    public List<Integer> RollCnt{get;set;}
    public List<Task_Plan__c> ChildTask{get;set;}
    public Boolean applyActionPlan{get;set;}
    public List<Task_Plan__c> actionPlanTasks1{get;set;}
    //public Map<String,List<Task_Plan__c>> mapTask{get;set;}
    map<integer, integer> parindexmap = new map<integer, integer>();
    map<integer, string> PIM = new map<integer, string>();
    
    
    public ApplyActionPlanForDeal_Edit_Class(ApexPages.StandardController controller)
    { 
        Map<Id, Integer> IdIntMap = new Map<Id, Integer>();
        Map<id, Task_Plan__c> internmap = new map<id, Task_Plan__c>();
        
        ChildTask = new List<Task_Plan__c>();
        objectId =  apexpages.currentpage().getparameters().get('id');
        applyActionPlan=true;
        Integer inst;
        this.actionPlan= (Template__c)controller.getRecord(); 
        if(objectId !=null)
        {
        
            actionPlan =[Select Id ,Mass_Assign_To__c ,Name,Is_Template__c,RelatedObjectName__c,Deal__c,Loan__c from Template__c Where Id=:objectId];
            //if(actionplan.RelatedObjectName__c == 'Deal__C')
            //{
            try
            {
            currentdeal =[Select Id ,Accenture_Due_Date__c from Deal__c Where id=:actionPlan.Deal__c] ;
            }
            catch(exception e)
            {
            
            }
            
            //}
          //  if(actionplan.RelatedObjectName__c == 'Loan__c')
           // {
           try{
           
            currentLoan = [select Id,Paid_off_Date__c from Loan__c where id=:actionplan.Loan__c];
            }
            catch(exception e)
            {}
           // }
            system.debug('>>>>>>'+currentdeal);
            system.debug('<<<<<<'+currentLoan);
        }
            mapTask = new Map<String , List<Task_Plan__c>>();
            actionPlanTasks=[Select Index__c, Name, id, Field_to_update_Date_Received__c,Status__c,Date_Needed__c,Date_Received__c,Date_Ordered__c,Parent__c, Field_to_update_for_date_needed__c,Template__r.Mass_Assign_To__c, Notify_Emails__c, Priority__c, 
                Comments__c, (Select Index__c, Name,  Field_to_update_Date_Received__c ,Parent_Index__c,
                Status__c,Date_Needed__c,Date_Received__c,Date_Ordered__c,Parent__c,Parent__r.Index__c,
                Field_to_update_for_date_needed__c, Notify_Emails__c, Priority__c, 
                Comments__c from Task_Plans__r) from Task_Plan__c where Template__c=:apexpages.currentpage().getparameters().get('id') and Parent__c =null order by Index__c ASC];
        
            if(actionPlanTasks.size()>0)
            {
                for(Task_Plan__c  sobj :actionPlanTasks){
                    IdIntMap.put(sobj.id, integer.valueof(sobj.Index__c-1));
                    PIM.put(integer.valueof(sobj.Index__c), sobj.Name); 
                    internmap.put(sobj.id, sobj);
                }
                
                actionPlansubTasks=[Select Index__c, Name, Template__c, Field_to_update_Date_Received__c,Status__c,Date_Needed__c,Date_Received__c,Date_Ordered__c,Parent__c,Field_to_update_for_date_needed__c, Notify_Emails__c, Priority__c, Parent_Index__c,
                Comments__c,Create_Task__c  from Task_Plan__c where Template__c=:apexpages.currentpage().getparameters().get('id') and Parent__c!=null order by Index__c ASC];
                for(Task_Plan__c  taskplan :actionPlansubTasks){
                    if(internmap.containskey(taskplan.parent__c)){
                        taskplan.Parent_Index__c=internmap.get(taskplan.parent__c).index__c;
                        if(!mapTask.ContainsKey(internmap.get(taskplan.parent__c).Name))
                        {                             
                          mapTask.put(internmap.get(taskplan.parent__c).Name, new List<Task_Plan__c>());
                          taskplan.index__c = 1;
                          mapTask.get(internmap.get(taskplan.parent__c).Name).add(taskplan);
                        }
                        else{
                           integer count=mapTask.get(internmap.get(taskplan.parent__c).Name).size();
                           taskplan.index__c=count+1;
                           mapTask.get(internmap.get(taskplan.parent__c).Name).add(taskplan);
                        }
                    }
                }
                
                for(Task_Plan__c  sobj :actionPlanTasks){
                    if(!mapTask.Containskey(sobj.Name)){
                        mapTask.put(sobj.Name, new List<Task_Plan__c>());
                        mapTask.get(sobj.Name).add(new Task_Plan__c(Index__c=0));
                    }
                }                
            }
  }
   Task_Plan__c TP =null;
   public pagereference addTask(){
       TP = new Task_Plan__c();
       TP.create_Task__c =true;
       TP.Index__c=actionPlanTasks.size()+1;
       TP.Name = 'Task'+(TP.Index__c);
       TP.Template__c=objectId;
       TP.Priority__c='Medium';
       TP.status__c='Not Started';
       actionPlanTasks.add(TP);  
       
       PIM.put(integer.valueof(TP.Index__c), TP.Name);
       
       mapTask.put(TP.Name, new List<Task_Plan__c>());
       mapTask.get(TP.Name).add(new Task_Plan__c(parent_Index__c=TP.index__c, Template__c=objectId, Priority__c='Medium', status__c='Not Started'));
            
       return null;    
    }


  public List<SelectOption> getdateFields(){
        List<SelectOption> piclistValue=new List<SelectOption>();
        String OldType;
        if(OldType!=actionPlan.RelatedObjectName__c && actionPlan.RelatedObjectName__c!=null){
            //getarrangetask();
        }
        if(actionPlan.RelatedObjectName__c==null){actionPlan.RelatedObjectName__c='Deal__c';}
        try{
        //piclistValue.add(new selectoption('None', 'None'));
        if(actionPlan.RelatedObjectName__c == 'Deal__c'){
        piclistValue.add(new selectoption('--NONE--','--NONE--'));
            for(Schema.SObjectField F : Schema.SObjectType.Deal__c.fields.getMap().values()) {
                if(String.valueof(f.getDescribe().getType())=='Date'){
                    piclistValue.add(new selectoption(f.getDescribe().getName(), f.getDescribe().getLabel()));
                }
            }
        }
        else if(actionPlan.RelatedObjectName__c == 'Loan__c'){
            piclistValue.add(new selectoption('--NONE--','--NONE--'));
                for(Schema.SObjectField F : Schema.SObjectType.Loan__c.fields.getMap().values()) {
                    if(String.valueof(f.getDescribe().getType())=='Date'){
                        piclistValue.add(new selectoption(f.getDescribe().getName(), f.getDescribe().getLabel()));
                    }
                }
            }
        }
        catch(exception e){}
        OldType=actionPlan.RelatedObjectName__c;
        return piclistValue;        
    }
    public pagereference addsubTask()
    {
        Map<Id, Integer> IdIntMap = new Map<Id, Integer>();
        RollCnt = new List<Integer>();
        system.debug('check the subtasks list..'+actionPlansubTasks);
       system.debug('check the tasks also..'+actionPlanTasks); 
       Integer inst;
       TP=new Task_Plan__c();
       TP.create_task__C =true;
       actionPlansubTasks.add(TP);
       Integer ParentIndex=Integer.Valueof(apexpages.currentpage().getparameters().get('parentindex'));
       system.debug('Give me ParentIndex..'+ParentIndex);
          system.debug('Now check the subtasks list..'+actionPlansubTasks);
          system.debug('get me the maptask..'+ParentIndex);
           if(!mapTask.ContainsKey(PIM.get(ParentIndex)))
           {
               mapTask.put(PIM.get(ParentIndex), new List<Task_Plan__c>());
               mapTask.get(PIM.get(ParentIndex)).add(new Task_Plan__c(Index__c=1,create_task__c =true, Template__c=objectId, parent_index__c=ParentIndex));
            }
          else{
            Integer countrec = mapTask.get(PIM.get(ParentIndex)).size();
            if((countrec-1)==0){countrec=1;}
            else{countrec++;}
            system.debug('Check the countrec..'+countrec);
            mapTask.get(PIM.get(ParentIndex)).add(new Task_Plan__c(Template__c=objectId, create_task__c =true, Index__c=countrec+1,Name='SubTask'+(countrec),Priority__c='Medium', status__c='Not Started', parent_index__c=ParentIndex));
          }
        system.debug('Now check the map for subtask index..'+mapTask);
        return null;  
    }
public pagereference changeTemplate(){
  return null;
  }
public PageReference Save()
    {
    if ((Schema.sObjectType.Template__c.isCreateable())&&(Schema.sObjectType.Template__c.isUpdateable()))


    upsert actionplan;
   
       
  LIST<Task_Plan__c> MainTaskPlans = new LIST<Task_Plan__c>();
  Task_Plan__c ThT;
        for(Task_Plan__c tp : actionPlanTasks){
          If(TP.Date_Received__c!=null)
            {
                system.debug('$$$$$$$$$$$$$$'+TP);
                TP.Status__c='Completed';
                
            } 
             
             
        
             if(tp.Date_Received__c!=null)
           {    
            if(currentdeal!=null)
            {
            if(tp.Field_to_update_Date_Received__c=='Appraisal_Date__c' )
            currentdeal.Appraisal_Date__c=tp.Date_Received__c;
            If(tp.Field_to_update_Date_Received__c=='Accenture_Due_Date__c')
             currentdeal.Accenture_Due_Date__c=tp.Date_Received__c;
            if(tp.Field_to_update_Date_Received__c=='Go-Hard Date')
             currentdeal.Go_Hard_Date__c=tp.Date_Received__c;
            if(tp.Field_to_update_Date_Received__c=='Rep_Burn_Off__c')
             currentdeal.Rep_Burn_Off__c=tp.Date_Received__c;
            if(tp.Field_to_update_Date_Received__c=='Site_Visit_Date__c')
             currentdeal.Site_Visit_Date__c=tp.Date_Received__c;
            if(tp.Field_to_update_Date_Received__c=='Passed_Date__c')
             currentdeal.Passed_Date__c=tp.Date_Received__c;
            if(tp.Field_to_update_Date_Received__c=='Estimated_COE_Date__c')
             currentdeal.Estimated_COE_Date__c=tp.Date_Received__c;
            if(tp.Field_to_update_Date_Received__c=='Hard_Date__c')
             currentdeal.Hard_Date__c=tp.Date_Received__c;
            if(tp.Field_to_update_Date_Received__c=='Estimated_SP_Date__c')
             currentdeal.Estimated_SP_Date__c=tp.Date_Received__c;
            if(tp.Field_to_update_Date_Received__c=='Purchase_Date__c')
             currentdeal.Purchase_Date__c=tp.Date_Received__c;
            if(tp.Field_to_update_Date_Received__c=='Estimated_Hard_Date__c')
             currentdeal.Estimated_Hard_Date__c=tp.Date_Received__c;  
            if(tp.Field_to_update_Date_Received__c=='Contract_Date__c')
             currentdeal.Contract_Date__c=tp.Date_Received__c;
            if(tp.Field_to_update_Date_Received__c=='Closing_Date__c')
             currentdeal.Closing_Date__c=tp.Date_Received__c;
            if(tp.Field_to_update_Date_Received__c=='LOI_Signed_Date__c')
             currentdeal.LOI_Signed_Date__c=tp.Date_Received__c;
            if(tp.Field_to_update_Date_Received__c=='LOI_Sent_Date__c')
             currentdeal.LOI_Sent_Date__c=tp.Date_Received__c;
            if(tp.Field_to_update_Date_Received__c=='Lease_Abstract_Date__c')
             currentdeal.Lease_Abstract_Date__c=tp.Date_Received__c;  
            if(tp.Field_to_update_Date_Received__c=='Open_Escrow_Date__c')
             currentdeal.Open_Escrow_Date__c=tp.Date_Received__c;
             
             if(tp.Field_to_update_Date_Received__c=='Tenant_ROFR_Waiver__c')
             currentdeal.Tenant_ROFR_Waiver__c=tp.Date_Received__c;
             if(tp.Field_to_update_Date_Received__c=='Date_Audit_Completed__c')
             currentdeal.Date_Audit_Completed__c=tp.Date_Received__c; 
             if(tp.Field_to_update_Date_Received__c=='Date_Identified__c')
             currentdeal.Date_Identified__c=tp.Date_Received__c; 
             if(tp.Field_to_update_Date_Received__c=='Investment_Committee_Approval__c')
             currentdeal.Investment_Committee_Approval__c=tp.Date_Received__c;
             if(tp.Field_to_update_Date_Received__c=='Part_1_Start_Date__c')
             currentdeal.Part_1_Start_Date__c=tp.Date_Received__c;
             if(tp.Field_to_update_Date_Received__c=='Estoppel__c')
             currentdeal.Estoppel__c=tp.Date_Received__c; 
              if(tp.Field_to_update_Date_Received__c=='Dead_Date__c')
             currentdeal.Dead_Date__c=tp.Date_Received__c;
             if(tp.Field_to_update_Date_Received__c=='nd_Estimated_SP_Date__c')
             currentdeal.nd_Estimated_SP_Date__c=tp.Date_Received__c;
             if(tp.Field_to_update_Date_Received__c=='nd_SP_End_Date__c')
             currentdeal.nd_SP_End_Date__c=tp.Date_Received__c;
             } 
             if(currentLoan!=null)
             {    
             if(tp.Field_to_update_Date_Received__c=='Paid_Off_Date__c')
             currentLoan.Paid_Off_Date__c=tp.Date_Received__c;
             if(tp.Field_to_update_Date_Received__c=='COE_Date__c')
             currentLoan.COE_Date__c=tp.Date_Received__c;
             }
             }
           if(tp.Date_Needed__c!=null){ 
            if(currentdeal!=null){  
             if(tp.Field_to_update_for_date_needed__c=='Dead_Date__c')
             currentdeal.Dead_Date__c=tp.Date_Needed__c;
             if(tp.Field_to_update_for_date_needed__c=='Appraisal_Date__c')
             currentdeal.Appraisal_Date__c=tp.Date_Needed__c;
            If(tp.Field_to_update_for_date_needed__c=='Accenture_Due_Date__c')
             currentdeal.Accenture_Due_Date__c=tp.Date_Needed__c;
             system.debug('$$$$'+currentdeal.Accenture_Due_Date__c);
            if(tp.Field_to_update_for_date_needed__c=='Go-Hard Date')
             currentdeal.Go_Hard_Date__c=tp.Date_Needed__c;
            if(tp.Field_to_update_for_date_needed__c=='Rep_Burn_Off__c')
             currentdeal.Rep_Burn_Off__c=tp.Date_Needed__c;
            if(tp.Field_to_update_for_date_needed__c=='Site_Visit_Date__c')
            currentdeal.Site_Visit_Date__c=tp.Date_Needed__c;
           // if(T.Field_to_update_for_date_needed__c=='Query_Sorted_Date__c')
            // currentdeal.Query_Sorted_Date__c=T.Rep_Burn_Off__c;
            if(tp.Field_to_update_for_date_needed__c=='Passed_Date__c')
             currentdeal.Passed_Date__c=tp.Date_Needed__c;
            if(tp.Field_to_update_for_date_needed__c=='Estimated_COE_Date__c')
             currentdeal.Estimated_COE_Date__c=tp.Date_Needed__c;
            if(tp.Field_to_update_for_date_needed__c=='Hard_Date__c')
             currentdeal.Hard_Date__c=tp.Date_Needed__c;
            if(tp.Field_to_update_for_date_needed__c=='Estimated_SP_Date__c')
             currentdeal.Estimated_SP_Date__c=tp.Date_Needed__c;
            if(tp.Field_to_update_for_date_needed__c=='Purchase_Date__c')
             currentdeal.Purchase_Date__c=tp.Date_Needed__c;
            if(tp.Field_to_update_for_date_needed__c=='Estimated_Hard_Date__c')
             currentdeal.Estimated_Hard_Date__c=tp.Date_Needed__c;  
            if(tp.Field_to_update_for_date_needed__c=='Contract_Date__c')
             currentdeal.Contract_Date__c=tp.Date_Needed__c;
            if(tp.Field_to_update_for_date_needed__c=='Closing_Date__c')
             currentdeal.Closing_Date__c=tp.Date_Needed__c;
             if(tp.Field_to_update_for_date_needed__c=='LOI_Signed_Date__c')
             currentdeal.LOI_Signed_Date__c=tp.Date_Needed__c;
             if(tp.Field_to_update_for_date_needed__c=='LOI_Sent_Date__c')
             currentdeal.LOI_Sent_Date__c=tp.Date_Needed__c;
             if(tp.Field_to_update_for_date_needed__c=='Lease_Abstract_Date__c')
             currentdeal.Lease_Abstract_Date__c=tp.Date_Needed__c;  
             // if(T.Field_to_update_for_date_needed__c=='Lease_Expiration__c')
             //currentdeal.Lease_Expiration__c=T.Date_Needed__c;
              if(tp.Field_to_update_for_date_needed__c=='Open_Escrow_Date__c')
             currentdeal.Open_Escrow_Date__c=tp.Date_Needed__c;
            // if(tp.Field_to_update_for_date_needed__c=='Purchase_Date__c')
            // currentdeal.Purchase_Date__c=tp.Date_Needed__c;
             if(tp.Field_to_update_for_date_needed__c=='Tenant_ROFR_Waiver__c')
             currentdeal.Tenant_ROFR_Waiver__c=tp.Date_Needed__c;
             if(tp.Field_to_update_for_date_needed__c=='Date_Audit_Completed__c')
             currentdeal.Date_Audit_Completed__c=tp.Date_Needed__c; 
             if(tp.Field_to_update_for_date_needed__c=='Date_Identified__c')
             currentdeal.Date_Identified__c=tp.Date_Needed__c; 
             if(tp.Field_to_update_for_date_needed__c=='Investment_Committee_Approval__c')
             currentdeal.Investment_Committee_Approval__c=tp.Date_Needed__c;
             if(tp.Field_to_update_for_date_needed__c=='Part_1_Start_Date__c')
             currentdeal.Part_1_Start_Date__c=tp.Date_Needed__c;
             if(tp.Field_to_update_for_date_needed__c=='Estoppel__c')
             currentdeal.Estoppel__c=tp.Date_Needed__c; 
              if(tp.Field_to_update_for_date_needed__c=='nd_Estimated_SP_Date__c')
             currentdeal.nd_Estimated_SP_Date__c=tp.Date_Needed__c; 
              if(tp.Field_to_update_for_date_needed__c=='nd_SP_End_Date__c')
             currentdeal.nd_SP_End_Date__c=tp.Date_Needed__c; 
          }  
         
           if(currentLoan!=null)
           {
           system.debug('************'+currentLoan);
           if(tp.Field_to_update_for_date_needed__c=='Paid_Off_Date__c')
             currentLoan.Paid_Off_Date__c=tp.Date_Needed__c;
             if(tp.Field_to_update_for_date_needed__c=='COE_Date__c')
             currentLoan.COE_Date__c=tp.Date_Needed__c;
             }
             }
        if(tp.Index__c!=null && tp.Name != null){
            
            If(tp.Template__r.Mass_Assign_To__c!=null)
            {
               if(actionplan.Mass_Assign_To__c!=null)
               {
                tp.Assigned_TO__c = actionplan.Mass_Assign_To__c;
                }
                else
                {
                tp.Assigned_TO__c =tp.Template__r.Mass_Assign_To__c;
                }
                
            }
             If(tp.Template__r.Mass_Assign_To__c==null)
             {
             if(actionplan.Mass_Assign_To__c!=null)
             
             {
              tp.Assigned_TO__c = actionplan.Mass_Assign_To__c;
             }
             else
             {
             tp.Assigned_To__c=Userinfo.getUserId();
             }
             }  
                if(tp.Template__c == null && tp.id == Null){
                    tp.Template__c = actionPlan.id;
             
                }
                MainTaskPlans.add(tp);
                
            }
        }     
       if(MainTaskPlans.size()>0){
          if (Schema.sObjectType.Task_Plan__c.isUpdateable())
            Upsert MainTaskPlans;
       }
       if(currentDeal!=null){
          if (Schema.sObjectType.Deal__c.isUpdateable()) 
           Update currentDeal;
       }
       if(currentLoan!=null){
         if (Schema.sObjectType.Loan__c.isUpdateable()) 
           Update currentLoan;
       }
  List<Task_Plan__c> ChildPlanlist = new List<Task_Plan__c>();
        
        for(Task_Plan__c stp:ActionPlanSubTasks){
           
        If(stp.Template__r.Mass_Assign_To__c!=null){
               if(actionplan.Mass_Assign_To__c!=null){
                stp.Assigned_TO__c = actionplan.Mass_Assign_To__c;
               }
               else{
                stp.Assigned_TO__c =tp.Template__r.Mass_Assign_To__c;
               }
                
            }
        If(stp.Template__r.Mass_Assign_To__c==null){
              if(actionplan.Mass_Assign_To__c!=null){
              stp.Assigned_TO__c = actionplan.Mass_Assign_To__c;
              }
              else{
              stp.Assigned_To__c=Userinfo.getUserId();
              }
        }  
        If(stp.Date_Received__c!=null){
              system.debug('$$$$$$$$$$$$$$'+stp);
              stp.Status__c='Completed';  
        }
        }
        if(maptask.keyset().size()>0){           
            for(Task_Plan__c partskpln: MainTaskPlans){ 
                system.debug('___________AUDIT Test__1___'+partskpln.Name);
                if(maptask.Containskey(partskpln.Name)){
                    for(Task_Plan__c chldtp:mapTask.get(partskpln.Name)){
                        chldtp.Parent__c=partskpln.id;
                        
                        chldtp.Parent_Index__c=partskpln.Index__c;  
                        if(chldtp.template__c!=null && chldtp.Name != null){                      
                            ChildPlanlist.add(chldtp);  
                            system.debug('_____________AUDIT Test__1____'+chldtp.Status__c+'======'+chldtp.Name);                      
                        }
                    }
                }
            }
            
            Set<String> IdSet = new Set<String>();
            for(Task_Plan__c APT:ChildPlanlist){
                system.debug('###########################----1--'+APT.id);
                IdSet.add(APT.id);
            } 
            system.debug('======================'+ChildPlanlist.size());
            if(ChildPlanlist.size()>0){
            
            if( (Schema.sObjectType.Task_Plan__c.isCreateable())&&(Schema.sObjectType.Task_Plan__c.isUpdateable()))


                database.upsert(ChildPlanlist);
            }
            
        //    system.debug('Check the list of childplanlist..////'+ChildPlanlist[0].TaskRelatedto__c);
            for(Task_Plan__c APT:ChildPlanlist){
                system.debug('###########################------'+APT.Name);
                IdSet.add(APT.id);
            }    
            system.debug('--------sahith----'+IdSet);
            Map<id, Task> TaskPlanmap = new Map<id, Task>();
            for(Task T:[Select id, whatid, Subject, Action_Plan_Task__c, Status, Priority, ActivityDate, Date_Ordered__c,X18_digit_Id__c , Date_Received__c, Date_Needed__c, Date_Completed__c, Description, Ownerid from Task where X18_digit_Id__c =:IdSet and Action_Plan_Task__c != null]){
                TaskPlanmap.put(T.X18_digit_Id__c , T);
                system.debug('0000000000000000000000-------000000000------'+T.Action_Plan_Task__c+'-------'+T.Subject+'----'+T.id);
            }
            system.debug('........APPLY...................'+TaskPlanmap.keyset());
            
            task t;
            List<Task> inserttasklist = new List<Task>();
            for(Task_Plan__c ct :ChildPlanlist){
                system.debug('...................1........'+TaskPlanmap.Containskey(ct.id)+'...............'+(ct.TaskRelatedto__c!=null || ct.TaskRelatedtoLoan__c!=null));
                //if(ct.TaskRelatedto__c!=null || ct.TaskRelatedtoLoan__c!=null){             
                    t= new Task();                    
                    if(TaskPlanmap.Containskey(ct.id)){t=TaskPlanmap.get(ct.id);}
                    t.subject = ct.Name;
                   // if(ct.Template__r.Mass_Assign_To__c!=null){t.Ownerid= ct.Template__r.Mass_Assign_To__c;}
                   // if(ct.Template__r.Mass_Assign_To__c==null){t.Ownerid= ct.Assigned_To__c;}
                   if(ct.Assigned_To__c!=null){t.Ownerid= ct.Assigned_To__c;}
                    if(ct.Comments__c!=null){t.Description=ct.Comments__c;}
                    if(ct.Date_Completed__c!=null){t.Date_Completed__c=ct.Date_Completed__c;}
                    if(ct.Date_Needed__c!=null){t.Date_Needed__c=ct.Date_Needed__c;}
                    if(ct.Date_Received__c!=null){t.Date_Received__c=ct.Date_Received__c;}
                    if(ct.Date_Ordered__c!=null){t.Date_Ordered__c=ct.Date_Ordered__c;}
                    if(ct.Date_Needed__c!=null){t.ActivityDate = ct.Date_Needed__c;}
                    if(ct.Priority__c!=null){t.Priority=ct.Priority__c;}
                    if(ct.Status__c!=null){t.Status=ct.Status__c;}
                    t.Action_Plan_Task__c= ct.id;
                     if(ct.TaskRelatedto__c!=null){t.whatid=ct.TaskRelatedto__c;}
                    if(ct.TaskRelatedToloan__c!=null){
                        t.whatid=ct.TaskRelatedToloan__c;
                        }
                    inserttasklist.add(t);
              //  }
                system.debug('++++++++++++++++++++++++++'+inserttasklist.size());
            }
            if(inserttasklist.size()>0){
                if ((Schema.sObjectType.Task.isCreateable())&&(Schema.sObjectType.Task.isUpdateable()))

                upsert inserttasklist;
                for(Task tsk:inserttasklist){
                    system.debug('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'+tsk.id);
                }
            }
        }
        
      
       return new Pagereference('/apex/ActionPlanView_new?id='+actionplan.id);
        
    }   
    
     public PageReference Cancel()
    {
       // return new Pagereference('https://cs13.salesforce.com/a3U/o');
        return new Pagereference('/apex/ActionPlanView_new?id='+actionplan.id);
    }   
  
}