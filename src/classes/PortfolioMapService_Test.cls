@isTest (SeeAllData=true)
private class PortfolioMapService_Test{
    
    @isTest static void test_method_one() {
        PortfolioMapService wps = new PortfolioMapService();
        
        wps.gettype();

        Map<String,String> sendMap = new Map<String,String>();
        sendMap.put('action','getIndustryList');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getTenantList');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getStateList');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getResultTotal');
        sendMap.put('state','TX');
        //sendMap.put('city','Houston');
        sendMap.put('tenantIndustry','Office');
        sendMap.put('tenant','**VACANT**');
        sendMap.put('filterRetail','true');
        sendMap.put('filterMultiTenant','true');
        sendMap.put('filterOffice','true');
        sendMap.put('filterIndustrial','true');
        sendMap.put('filterOther','true');
        wps.executeRequest(sendMap);

        sendMap.clear();
        sendMap.put('action','getPropertyList');
        sendMap.put('state','TX');
        //sendMap.put('city','Houston');
        sendMap.put('tenantIndustry','Office');
        sendMap.put('tenant','**VACANT**');
        sendMap.put('filterRetail','true');
        sendMap.put('filterMultiTenant','true');
        sendMap.put('filterOffice','true');
        sendMap.put('filterIndustrial','true');
        sendMap.put('filterOther','true');
        wps.executeRequest(sendMap);
        
        sendMap.clear();
        sendMap.put('action','getgroupbyStates');
        sendMap.put('state','TX');
        //sendMap.put('city','Houston');
        sendMap.put('tenantIndustry','Office');
        sendMap.put('tenant','**VACANT**');
        sendMap.put('filterRetail','true');
        sendMap.put('filterMultiTenant','true');
        sendMap.put('filterOffice','true');
        sendMap.put('filterIndustrial','true');
        sendMap.put('filterOther','true');
        wps.executeRequest(sendMap);
        
        sendMap.clear();
        sendMap.put('action','getPropertyListByState');
        sendMap.put('state','TX');
        //sendMap.put('city','Houston');
        sendMap.put('tenantIndustry','Office');
        sendMap.put('tenant','**VACANT**');
        sendMap.put('filterRetail','true');
        sendMap.put('filterMultiTenant','true');
        sendMap.put('filterOffice','true');
        sendMap.put('filterIndustrial','true');
        sendMap.put('filterOther','true');
          system.assert(sendMap != Null );
        wps.executeRequest(sendMap);



        wps.loadResponse();
    }
    
   
}