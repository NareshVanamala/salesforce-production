public with sharing class PC_Popup_MRIProp_BuildingContacts extends PC_PropContactsHelperClass{

    public ApexPages.StandardController controller {get; set;}
    public MRI_PROPERTY__c mriprop{get;set;}
    public List<Property_Contact_Buildings__c> propcontacts{get;set;}
    public set<id> lsidset=new set<id>();
    public set<id> pcidset=new set<id>();
    public String id {get;set;}
    public String ids {get;set;}
    public String MRIId  {get;set;}
    public List<String> strs = new List<String>();
    public List<String> selectedleaseids = new List<String>();
    //public list<Lease__c>leaselist{get;set;}
    public List<Property_Contacts__c> results{get;set;}
    public string Searchtext{get;set;}  
    public List<cContact>PropertyContactList {get; set;}
    public List<cContact>selectedPropertycontactList {get; set;}
    public list<Property_Contacts__c>PContactlist{get; set;}
    public String leaseOppid{get; set;}
    public string SearchBy {get; set;}
    public boolean showsearch{get; set;}
    public boolean shownew{get; set;}
    public boolean showContactType{get; set;}
    public boolean ShowJustContactType{get; set;}
    public Property_Contacts__c vdr{get; set;}
    public Property_Contact_Buildings__c pcl{get; set;}
    public String soql;
    public string MRIPropid{get; set;}
    public MRI_PROPERTY__c  leaserec{get; set;} 
    public String showNewButton { get; set; }
    public String message{ get; set; }
    public List<Property_Contact_Buildings__c> Selectedcons{ get; set; }
    public List<mywrapper> mylist {get; set;}
    public List<Property_Contact_Buildings__c> lidlist {get; set;} 
    public set<ID> idset {get; set;}
    public String collegeString{get;set;}
    public boolean refreshPage{ get; set;}
    
    public List<cLease>leaseList {get; set;}
    public MRI_PROPERTY__c mripid;
    
    public set<ID> mriset{get; set;}
    public Property_Contact_Buildings__c prconl{get; set;}
    public boolean showexist{get;set;}
    
    public List<massapply>massapplylist {get; set;}
    public boolean showmain{get;set;}
    public boolean showexist2{get;set;}
    public String mystring{get;set;}
    
    public string contactId;
    public List<Property_Contact_Buildings__c> pclist{get;set;}
    public integer clspop{get; set;}
    public string showmycontype{get; set;} 
    public String MRIRecordId;
    public String PCBId;
    public PC_Popup_MRIProp_BuildingContacts(ApexPages.StandardController controller){
        ShowJustContactType = false;
        refreshPage = false;
        MRIRecordId = apexpages.currentpage().getparameters().get('id');
        PCBId = apexpages.currentpage().getparameters().get('PCBId');
        system.debug('PCBId'+PCBId);
        prconl=new Property_Contact_Buildings__c(); 
        if(MRIRecordId != null && MRIRecordId != '' && PCBId !='' && PCBId != null){
           ShowJustContactType = true;
           showContactType = true;
           system.debug('showContactType'+showContactType);
           Property_Contact_Buildings__c PCBObj = [select id,Contact_Type__c from Property_Contact_Buildings__c where id=:PCBId limit 1];
           system.debug('PCBObj'+PCBObj);
           prconl.Contact_Type__c = PCBObj.Contact_Type__c;
        }
        this.controller = controller;
        showmycontype='';
        this.mriprop = (MRI_PROPERTY__c )controller.getRecord();
        mripid=mriprop;
        string MRIId=mriprop.id;
        clspop=0;
        system.debug('<<<<<give me id>>>>'+MRIId+mriprop );
        for(MRI_PROPERTY__c ls:[select id,Name,Common_Name__c from MRI_PROPERTY__c where id=:mriprop.id ])    
            lsidset.add(ls.id);
                           
    
        results=new list<Property_Contacts__c>();
        selectedPropertycontactList =new list<cContact>();
        showsearch=true;  
        shownew= false;
        showContactType = false;
        vdr= new Property_Contacts__c();
        pcl= new Property_Contact_Buildings__c(); 
        runSearch(); 
        message = System.currentPagereference().getParameters().get('msg');
        
        mylist=getmylist();
        idset=new set<ID>();
        
        mriset=new set<ID>();
        
        
        showexist=false;
        showmain=true;
        
        mystring='';
        
        contactId='';
        pclist=new List<Property_Contact_Buildings__c>();
    }
    


    public class mywrapper {
        public Property_Contact_Buildings__c pcls {get; set;}
        public boolean selected {get; set;}

        public mywrapper(Property_Contact_Buildings__c pcls) {
            this.pcls = pcls;
            selected = false; //If you want all checkboxes initially selected, set this to true
        }
    }
    

    public List<mywrapper> getmylist() {
        if (mylist == null) {
            mylist = new List<mywrapper>();
            if(!lsidset.isEmpty()) {
                for (Property_Contact_Buildings__c foo : [select id,Property_Contacts__r.id,Property_Contacts__r.MRI_Property__c ,Property_Contacts__r.Name,Property_Contacts__r.Company_Name__c,
                                Property_Contacts__r.Email_Address__c,Property_Contacts__r.Phone__c,Property_Contacts__r.Address__c,
                                Property_Contacts__r.City__c,Property_Contacts__r.State__c,Property_Contacts__r.Zipcode__c,Contact_Type__c,Center_Name__r.Tenant_Name__c,Center_Name__c,Center_Name__r.Name,Center_Name__r.Common_Name__c,
                                Property_Contacts__r.Notes__c from Property_Contact_Buildings__c where Center_Name__c IN:lsidset]) {
                    mylist.add(new mywrapper(foo));
                }
            }
        }
        return mylist;
    }
    public void processSelectedcons() {
        system.debug('$$$$$$$$$');
        Selectedcons = new List<Property_Contact_Buildings__c>();
        for (mywrapper mywrp : mylist) {
            if (mywrp.selected = true) {
                Selectedcons.add(mywrp.pcls); // This adds the wrapper wFoo's real Foo__c
            }
        }
        system.debug('$$$$$$$$$'+Selectedcons);
        for(Property_Contact_Buildings__c conid:Selectedcons)
            idset.add(conid.id);
        system.debug('$$$$$$$$$'+idset);
        collegeString = '';
        for(String s:idset) {
           collegeString += (collegeString==''?'':',')+s;
        }
        system.debug('$$$$$$$$$'+collegeString);
        showPopup();
    }
    
    public boolean displayPopup {get; set;}     
    
    public void closePopup() {        
        displayPopup = false;    
    }     
    public void showPopup() {        
        displayPopup = true;    
    }
    
    public ID selectedVersion {get; set;}
    
    public PageReference massapply() {
        system.debug('$$$$$$$$$');
        processSelectedcons();
        PageReference pgref=new PageReference('/apex/mass_apply');
        pgref.setRedirect(true);
        return pgref;
    }
    public PageReference enable() {
        system.debug('$$$$$$$$$$$$');
        selectedVersion = System.currentPageReference().getParameters().get('checked');
        system.debug('$$$$$$$$$$$$'+selectedVersion);
        for(cContact v : PropertyContactList){
            if(v.con.Id == selectedVersion){
                v.selected = true;
            }
            else{
                v.selected =false; 
            }
        }
        return null;
    }
    public PageReference Save1(){    
        mystring=ApexPages.currentPage().getParameters().get('cid');
        system.debug('$$$'+mystring);
        showexist2=true;
        system.debug('$$$$'+showexist2);
        shownew=false;
        showContactType = true;     
        system.debug('$$$$'+shownew);
        leaseList=getleaseList();
        return null;
    }
    public PageReference Save(){     
        String duplicateContactTypeMsg='';
        //String MRIId = apexpages.currentpage().getparameters().get('Id');
        String PropertContactId = apexpages.currentpage().getparameters().get('PropConID');
        MRIRecordId = apexpages.currentpage().getparameters().get('id');
        PCBId = apexpages.currentpage().getparameters().get('PCBId');
        system.debug('outer prconl.Contact_Type__c'+prconl.Contact_Type__c);
        if(prconl.Contact_Type__c == null || prconl.Contact_Type__c == '')
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select contact type'));
        else if(MRIRecordId != null && MRIRecordId != '' && prconl.Contact_Type__c != null && prconl.Contact_Type__c != '' && PCBId != '' && PCBId != null){
           system.debug('MRIID123'+MRIId);
           PageReference pgref;
           list<MRI_PROPERTY__c> selMRIList = [select id,Tenant_Name__c,Name,Common_Name__c from MRI_PROPERTY__c where id =:MRIRecordId];       
           system.debug('selMRIList1234'+selMRIList);
           system.debug('prconl.Contact_Type__c'+prconl.Contact_Type__c);
           system.debug('PCBId'+PCBId);
           duplicateContactTypeMsg = insertPropConBuildings(selMRIList,PropertContactId,prconl.Contact_Type__c,false,PCBId);
           system.debug('duplicateContactTypeMsg123'+duplicateContactTypeMsg);
           if(duplicateContactTypeMsg == null || duplicateContactTypeMsg == ''){
              refreshPage = true;
              clspop=1; 
              pgref=new PageReference('/apex/PC_Redirectpage?id='+clspop);   
              pgref.setRedirect(true); 
              system.debug('Inner pgref'+pgref);
              return pgref;
           }
           else{
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,duplicateContactTypeMsg));
              return null;
           }
           system.debug('OUTER pgref'+pgref);
        }
        else{
        string propConId='';
        list<MRI_PROPERTY__c> selMRIList = new list<MRI_PROPERTY__c>();
        PageReference pgref;
        string selectedleaseid; 
        system.debug('leaseList'+leaseList);
        system.debug('prconl.Contact_Type__c'+prconl.Contact_Type__c);
        for(cLease llist:leaseList){
         if(llist.selected==true)
             selMRIList.add(llist.leas);
        }
        if(selMRIList.size()==0)
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a Building'));
        else if(prconl.Contact_Type__c == null || prconl.Contact_Type__c == '')
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select contact type'));
        else{
           if(vdr.name!= null && vdr.name!=''){
                Property_Contacts__c pc=new Property_Contacts__c();
                system.debug('vdr.name'+vdr.name);
                pc.Address__c=vdr.Address__c;
                pc.Company_Name__c = vdr.Company_Name__c ;
                pc.Department__c = vdr.Department__c ;
                pc.Notes__c = vdr.Notes__c ;
                pc.Email_Address__c = vdr.Email_Address__c ;
                pc.Phone__c = vdr.Phone__c ;
                pc.City__c = vdr.City__c ;
                pc.Address_Line_2__c = vdr.Address_Line_2__c;
                pc.Cell_Phone__c = vdr.Cell_Phone__c;
                pc.Fax_Number__c= vdr.Fax_Number__c;
                pc.Title__c = vdr.Title__c;
                pc.State__c = vdr.State__c ;
                pc.Zipcode__c = vdr.Zipcode__c ;
                pc.name=vdr.name;
                
                if (Schema.sObjectType.Property_Contacts__c.isCreateable())

                insert pc;
                propConId=pc.id;
            }      
           // if(selectedPropertycontactList[0].con.id != null && selectedPropertycontactList[0].con.id != ''){
            else{
                propConId = selectedPropertycontactList[0].con.id;
                system.debug('selectedPropertycontactList[0].con.id'+selectedPropertycontactList[0].con.id); 
            }
        
           duplicateContactTypeMsg = insertPropConBuildings(selMRIList,propConId,prconl.Contact_Type__c,false,null);
           if(duplicateContactTypeMsg == null || duplicateContactTypeMsg == ''){
              refreshPage = true;
              clspop=1; 
              pgref=new PageReference('/apex/PC_Redirectpage?id='+clspop);   
              pgref.setRedirect(true); 
              return pgref;
           }
           else{
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,duplicateContactTypeMsg));
              return null;
           }
     }
    }
        return null;
   
    }
  
    public PageReference createNewRecord()
    {
        String mid=ApexPages.currentPage().getParameters().get('cid');
        system.debug('$$$'+mid);
        shownew=true; 
        showsearch=false; 
        showexist=false;
        showmain=false;
        showexist2=false;
        showContactType = true;
        return null;
    }
    
    @testvisible private void runSearch()
    { 
        results = performSearch(Searchtext);
        PropertyContactList = new List<cContact>();
        for(Property_Contacts__c c:results)
        {
            PropertyContactList.add(new cContact(c));
        }
    }  
    private List<Property_Contacts__c> performSearch(string Searchtext){
        soql = 'select id,Name,Company_Name__c,Email_Address__c, Phone__c,MRI_Property__c from Property_Contacts__c';
        system.debug('$$$$$$'+soql);
        system.debug('$$$$$$'+SearchBy);        
        if((Searchtext== '')||(Searchtext== null)) 
        { 
            soql=soql+ ' order by name ASC' ;
            soql = soql + ' limit 200';
            system.debug('$$$$$$'+soql);
        }      
        if(Searchtext!= '' && Searchtext!= null && SearchBy =='Name') 
        { 
            soql=soql+ ' where name=:Searchtext' ;
            soql = soql + ' limit 200';
            system.debug('$$$$$$'+soql);
        }
        else if(Searchtext!= '' && Searchtext!= null && SearchBy =='All fields' )
        {
            soql = soql +  ' where ((name LIKE \'%' + Searchtext+'%\')'+'OR'+'(Address__c LIKE \'%' + Searchtext+'%\'))';
            system.debug('$$$$$$'+soql);
        }
        if(Searchtext!= '' && Searchtext!= null && SearchBy==null){
            soql = soql +  ' where ((name LIKE \'%' + Searchtext+'%\')'+'OR'+'(Address__c LIKE \'%' + Searchtext+'%\'))';
            system.debug('$$$$$$'+soql);
        }
       return database.query(soql); 
    }
    public List<SelectOption> getSnoozeradio()
    {
        List<SelectOption> snoozeoptions = new List<SelectOption>(); 
        snoozeoptions.add(new SelectOption('Name','Name')); 
        snoozeoptions.add(new SelectOption('All fields','All fields')); 
        return snoozeoptions ;
    }
     
    public List<cContact> getContacts() {
        if(PropertyContactList == null){
            PropertyContactList = new List<cContact>();
            for(Property_Contacts__c c:results) {
               PropertyContactList.add(new cContact(c));
            }
        }
        return PropertyContactList ;
    }
    public void SearchRecord(){
        showsearch=true;  
        shownew=false;  
        showexist=false;
        showContactType = false;
        runSearch();  
    }
    

    public PageReference ApplyPropertyContacts() {
    
       
        if(PropertyContactList.size()>0)
        {
              for(cContact ct:PropertyContactList)
              {
                 if(ct.selected==true)
                  selectedPropertycontactList.add(ct);
                  mriset.add(ct.con.MRI_Property__c);
              }
        }   
        system.debug('$$$'+mriset);
        
        if(selectedPropertycontactList.isEmpty() && selectedPropertycontactList.size()==0 ){
            //system.debug('$$$'+selectedPropertycontactList[0].con.id);
            shownew=false; 
            showsearch=true; 
            showexist=false;  
            showContactType = true;         
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a Contact'));
            return null;                        
        }
        shownew=false; 
        showsearch=false; 
        showexist=true;
        showContactType = true;
       
        leaseList=getleaseList();
        
        return null;    
    }  
        public PageReference getSelected() {
            contactId = ApexPages.currentPage().getParameters().get('selconid');
            system.debug('$$$$$'+contactId);
            return null;
        }
    
    public Boolean getshownewcon() {
        boolean b;
        if(PropertyContactList.size()>0){
            for(cContact Ver : PropertyContactList){
                if(Ver.selected == true){
                    b= true;
                }
                else{
                    b= false;
                }
            }
        }
        return b;
    }

    public class cContact{
        public Property_Contacts__c con {get; set;}
        public Boolean selected {get; set;}

        public cContact(Property_Contacts__c c)
        {
            con = c;
            selected = false;
       }
    }
    public class cLease{
        public MRI_PROPERTY__c leas {get; set;}
        public Boolean selected {get; set;}

        public cLease(MRI_PROPERTY__c c)
        {
            leas = c;
            selected = false;
       }
    }
    public List<cLease> getleaseList() {
    
      system.debug('Welcome to getlease method');
        system.debug('$$$');
        system.debug('$$$'+mriset);     
        system.debug('<<<<<give me id>>>>'+MRIId+mriprop );
        MRI_PROPERTY__c mripid2=(MRI_PROPERTY__c )controller.getRecord();
        system.debug('$$$'+mripid2);
        String mid=ApexPages.currentPage().getParameters().get('cid');
        system.debug('$$$'+mid);        
        if (leaseList == null) {
            leaseList = new List<cLease>();
            for (MRI_PROPERTY__c foo : [select id,Tenant_Name__c,Name,Common_Name__c from MRI_PROPERTY__c where id =:mid ]) {
                leaseList.add(new cLease(foo));
            }
        }
        system.debug('$$$'+leaseList);
        return leaseList;
    }   
    public class massapply{
        public MRI_PROPERTY__c leas {get; set;}
        public Boolean selected {get; set;}

        public massapply(MRI_PROPERTY__c c)
        {
            leas = c;
            selected = false;
       }
    }
    public List<massapply> getmassapplylist() {
       // system.debug('$$$'+Selectedcons[0].Lease__r.Tenant_Name__c);    
        if (massapplylist == null) {
            massapplylist = new List<massapply>();
           
            for (MRI_PROPERTY__c foo : [select id,Tenant_Name__c,Name,Common_Name__c  from MRI_PROPERTY__c
                                LIMIT 3])                               
                massapplylist.add(new massapply(foo));
            
        }
        system.debug('$$$'+massapplylist);
        return massapplylist;
    } 



    }