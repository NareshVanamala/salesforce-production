public with sharing class CPFinalController
{
    public PageReference Save() {
        return null;
    }
   // public Competitors_Products__c Cpt {get; set; }
    public contact ct {get; set; }
    public string contactid{ get; set; }
    public string searchString {get;set;}
    public List<Products__c> autoresults{get;set;}
    public List<cContact> contactList {get; set;}
    public List<cContact> selectedcontactList {get; set;}
    public list<cContact> contacts{get; set;}
    //public list<Competitors_Products__c >insertlist;
    public List<Products__c >samemaillist1{set;get;}
    public id selectedconid{get;set;}
    //public List<Competitors_Products__c>samemaillist11{set;get;}
    public list<Products__c> deletelist1;
    public list<Products__c>existinglist{set;get;}
    
    public CPFinalController(ApexPages.StandardController controller)
   {
   
     //Cpt=new Competitors_Products__c(); 
     ct= new Contact();
     contactid=ApexPages.currentPage().getParameters().get('id');
     contactList = new List<cContact>();
     system.debug('Can I have contactid'+contactid);
     system.debug('Can I get search string'+searchString);
     //insertlist=new list<Competitors_Products__c >();
     existinglist=new list<Products__c >();
     Contact c =[select id from Contact where id = :ApexPages.currentPage().getParameters().get('id')];  
     samemaillist1=[select name, id,Strategy__c,Portfolio_Cap_Rate__c,Dividend__c,Talking_Points__c from Products__c where Contact__c=:ApexPages.currentPage().getParameters().get('id')];
        
      system.debug('Check my list'+samemaillist1);
      selectedconid=c.id;
      deletelist1= new list<Products__c>();
   }
   
    public void delete1()
    {
        id delname = ApexPages.CurrentPage().getParameters().get('delname');
        Products__c cmp=[select name, id,Strategy__c,Portfolio_Cap_Rate__c,Dividend__c,Talking_Points__c from Products__c where id=:delname];
          if (Products__c.sObjectType.getDescribe().isDeletable()) 
                {
                        delete cmp;
                }
       
        id conid=apexpages.currentpage().getparameters().get('id');
        if(conid!=null)
        {
            samemaillist1=[select name, id,Strategy__c,Portfolio_Cap_Rate__c,Dividend__c,Talking_Points__c from Products__c where Contact__c=:conid];
        }
    }
    public void saveRecord()
    {
        id addid = ApexPages.CurrentPage().getParameters().get('Addname');
        
        existinglist=[select id,Contact__c,name from Products__c where Products__c=:addid and Contact__c=:contactid];
        system.debug('get me the size of the list'+existinglist);
        Products__c cmtp=[select name,id,Competitor__c,Competitor__r.name,Total_Properties_Loans__c,Offering_size__c,Occupancy_Rate__c,Price_Per_Share__c,MFFO_Ratio__c,Launch_Date__c,FFO_Ratio__c,Expected_Close_Date__c,Diversification__c,Average_Lease_term__c,Strategy__c,Portfolio_Cap_Rate__c,Dividend__c,Talking_Points__c,Products__c from Products__c where id=:addid];
              if(existinglist.size()==0)
              {
                   Products__c cpp= new Products__c();
                   cpp.Is_Master_Record__c = false;
                   cpp.name=cmtp.name;
                   cpp.Products__c=cmtp.id;
                   cpp.Competitor__c=cmtp.Competitor__c;
                   cpp.Average_Lease_term__c=cmtp.Average_Lease_term__c;
                   cpp.Contact__c=contactid;
                   cpp.Diversification__c=cmtp.Diversification__c;
                   cpp.Dividend__c=cmtp.Dividend__c;
                   cpp.Expected_Close_Date__c=cmtp.Expected_Close_Date__c;
                   cpp.FFO_Ratio__c= cmtp.FFO_Ratio__c;
                   cpp.Launch_Date__c= cmtp.Launch_Date__c;
                   cpp.MFFO_Ratio__c=cmtp.MFFO_Ratio__c;
                   cpp.Occupancy_Rate__c=cmtp.Occupancy_Rate__c;
                   cpp.Offering_size__c=cmtp.Offering_size__c;
                   cpp.Portfolio_Cap_Rate__c=cmtp.Portfolio_Cap_Rate__c;
                   cpp.Strategy__c=cmtp.Strategy__c;
                   cpp.Talking_Points__c= cmtp.Talking_Points__c;
                   cpp.Total_Properties_Loans__c=cmtp.Total_Properties_Loans__c;
                   cpp.Price_Per_Share__c = cmtp.Price_Per_Share__c;
                   
                   if (Schema.sObjectType.Products__c.isCreateable())
                   insert cpp;
                 }
                 
                else
                {
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Selected Competitor already exists for this contact'));
                
                }   
               
        }
          public void search() 
         {
             contacts=new list<cContact>();
             system.debug('get me the value of the searchstring'+searchString);
             string[] str = new string[]{'%'+searchString+'%'};
             system.debug('Get me the string'+str);
             autoresults=[select  name,id,Average_Lease_term__c,Contact__c,Competitor__r.name,Diversification__c,Price_Per_Share__c,Dividend__c,Expected_Close_Date__c,FFO_Ratio__c,Launch_Date__c,MFFO_Ratio__c,Occupancy_Rate__c,Offering_size__c,Portfolio_Cap_Rate__c,Strategy__c,Talking_Points__c,Total_Properties_Loans__c from Products__c where((Competitor__r.name Like :str)or(name like:str)) and Is_Master_Record__c=true];
             system.debug('Check my list'+autoresults);
            if(autoresults.size()>0)
           {
                for(Products__c c:autoresults)
                {
               // As each contact is processed we create a new cContact object and add it to the contactList
              // cContact c1= new cContact(); 
                contacts.add(new cContact(c));
                }
       
           }
           
           else If(autoresults.Size()==0)
           {
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Your search did not return any results'));
           }
                  
         }
      
     public List<Products__c> getconList()
    {
      // update samemaillist1;
      this.samemaillist1=[select Competitor__r.name, name, id,Strategy__c,Portfolio_Cap_Rate__c,Price_Per_Share__c,Competitor__c,Dividend__c,Talking_Points__c from Products__c where Contact__c=:ApexPages.currentPage().getParameters().get('id')];
      system.debug('Check my list'+samemaillist1);
      return samemaillist1;
       
    
    } 
    public class cContact
    {
        public Products__c con {get; set;}
        public Boolean selected {get; set;}

        //This is the constructor method. When we create a new cContact object we pass a Contact that is set to the con property. We also set the selected value to false
        public cContact(Products__c c) {
            con = c;
            selected = false;
       }


    }

  
}