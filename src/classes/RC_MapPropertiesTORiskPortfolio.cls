global class RC_MapPropertiesTORiskPortfolio
{
    @InvocableMethod
    public static void MapProperties(list<String>portfolioid  )
    {
        
        list<Deal__c>deallist= new list<Deal__c>();
        list<RC_Property__c>propertylist=new list<RC_Property__c>();
        list<RC_Property__c>UpdatePropertyList= new list<RC_Property__c>();
        RC_Risk_Portfolio__c rcport;
        rcport=[select id,name,Related_Portfolio__c from RC_Risk_Portfolio__c where Related_Portfolio__c in:portfolioid];
        set<id>dealids=new set<id>();
           deallist=[select id, name, Portfolio_Deal__c from Deal__c where Portfolio_Deal__c in:portfolioid];
            if(deallist.size()>0)
            
               for(Deal__c dc:deallist)
                   dealids.add(dc.id);
            
                propertylist=[select id,name,Risk_Portfolio__c,Related_Deal__c from RC_Property__c where Related_Deal__c in:dealids];
                if(propertylist.size()>0)
                {
                     
                      for(RC_Property__c rct:propertylist)
                      {
                        rct.Risk_Portfolio__c=rcport.id; 
                        UpdatePropertyList.add(rct);
                      
                      }
                        if (Schema.sObjectType.RC_Property__c.isUpdateable()) 

                         update  UpdatePropertyList;
                }
        
        
        
        
      
     }
     
}