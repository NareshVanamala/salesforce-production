global with sharing class Cockpit_SnoozeContactCallLists implements Database.Batchable<SObject>{
    global final String Query;  
    global datetime snoozetime;
    global list<string>cids;
    global Cockpit_SnoozeContactCallLists (list<String>Cclidset,datetime snoozedate){  
        snoozetime=snoozedate;
        system.debug('This is the snoozed time'+snoozetime);
        cids=Cclidset;
        system.debug('This is my time'+snoozetime);
        system.debug('Check the list size'+Cclidset);
        Query='select id,Contact__r.Snooze_Time__c,Contact__r.Snoozed__c,Automated_Call_List__r.Name,Automated_Call_List__r.id,Call_Complete__c,CallList_Name__c, Contact__r.Name,contact__c from Contact_Call_List__c where id in : cids';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){  
        system.debug('This is start method');
        system.debug('Myquery is......'+Query);
        return Database.getQueryLocator(query);  
    } 
    
    global void execute(Database.BatchableContext BC,List<contact_call_list__c> scope){  
        List<contact_call_list__c> callist = new List<contact_call_list__c>();
        set<id>contactidset=new set<id>();
        if(scope.size()>0){
           for(Contact_Call_List__c clt:scope)
               contactidset.add(clt.contact__c);                   
        }     
        //snooze the contact.   
        list<Contact>snoozeContactlist=[select id,Snooze_Time__c,Snoozed__c from Contact where id in:contactidset] ;
        list<Contact>updateContactlist=new list<Contact>();
        if(snoozeContactlist.size()>0){
           for(Contact cty:snoozeContactlist){    
               cty.Snoozed__c=true; 
               cty.Snooze_Time__c=snoozetime;
               updateContactlist.add(cty);
           }
        }  
        try{
           if(Schema.sObjectType.Contact.isUpdateable())
              update updateContactlist;
           }
        catch(system.dmlexception e){
           System.debug('contacts not snoozed: ' + e);
        }     
        list<Contact_Call_List__c>snoozeCcllist=[select Snoozed__c,Snooze_Time__c,id,Contact__c from Contact_Call_List__c where Contact__c in :contactidset and Call_Complete__c=false and IsDeleted__c=false]; 
        list<Contact_Call_List__c>UpdateCCLList=new list<Contact_Call_List__c>(); 
        if(snoozeContactlist.size()>0){
           for(Contact_Call_List__c cty:snoozeCcllist){    
               cty.Snoozed__c=true; 
               cty.Snooze_Time__c=snoozetime;
               UpdateCCLList.add(cty);
           }
        } 
        try{
           if(Schema.sObjectType.Contact_Call_List__c.isUpdateable())
              update UpdateCCLList;
        }
        catch(system.dmlexception e){
           System.debug('contact call lists are not snooze: ' + e);
        }                       
    }
    
    global void finish(Database.BatchableContext BC){ 

    }
}