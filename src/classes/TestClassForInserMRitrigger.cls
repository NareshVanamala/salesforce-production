@isTest
public class TestClassForInserMRitrigger
{ 
    public static testmethod void myTestMethod1()
    {
       Mri_property__c m= new Mri_property__c();
        m.name='Test';
        m.Address__c = '2325 E camelback Rd';
        m.city__c   = 'Phoenix';
        m.state__c  ='AZ';
        m.Zip_code__c='85016';
        m.property_id__c = 'AC2001';
        m.web_ready__c ='Approved';
        insert  m;
       
        
        Lease__c L= new Lease__c();
        L.name='AC2001';
        L.Industry__c = 'Retail - Specialty';
        L.Lease_id__c ='AC0011_008149_A';
        L.Mri_property__c =m.id;
      
        insert  L;
        
        System.assertEquals(L.Mri_property__c,m.id);

        
        web_properties__c w= new web_properties__c();
        w.name=m.name;
        w.Address__c =  m.Address__c;
        w.city__c   = m.city__c;
        w.state__c  =m.state__c;
        w.Zip_code__c=m.Zip_code__c;
        w.property_id__c = m.property_id__c;
        insert  w;
        
        System.assertEquals(w.property_id__c,m.property_id__c);

       
   }
}