global class calllistinsertsforcm implements Database.Batchable<SObject>
{

/* global Automated_call_List__c LockedCL ; 
global  String Query;  
global CallList__c call;
global List<calllist__c> callistname ;
global List<contact_call_list__c> callistname1  ;
global Map<Integer, String> filterMap = new Map<Integer, String>();
global calllistinsertsforcm (Set<Id> clIds)  
{  
  system.debug('Welcome to Batch apex method...');
callistname = [Select Id, Name From calllist__c Where Id in: clIds];
call=[select id,Name,CampaignName__c, CampaignName__r.id,Contact_Type__c,Custom_Logic__c,Field_API__c, Field_API1__c, Field_API2__c,Field_API3__c,Field_API4__c,Operator__c,Operator1__c,Operator2__c,Operator3__c,Operator4__c,Value__c,Value1__c,Value2__c,Value3__c,Value4__c from  CallList__c  where id=:callistname[0].id ];
LockedCL = [Select Id, Name From Automated_call_List__c Where Name =: callistname[0].Name];
} */ 
global Database.QueryLocator start(Database.BatchableContext BC)  
{  
  /*system.debug('This is start method');
 try{ 
        filterMap.clear();
        String condn1=null;
        String condn2=null;
        String condn3=null;
        String condn4=null;
        String condn5=null;
        string conditions =null;
        string cond1;
        list<Contact>conlist= new list<Contact>();  
        list<Contact_Call_List__c >contactcalllist=new list<Contact_Call_List__c >();  
        String conquery = 'select Id,ContactId,CampaignId from CampaignMember';
        System.debug('*******:QUERY:********\n'+conquery);
   
        GerateDynamicQueryforcm generateQuery =  new GerateDynamicQueryforcm();
        if(call.Field_API__c!=null && call.Field_API__c!='' && call.Operator__c!='' && call.Operator__c!=null)
        condn1= generateQuery.conditionquery(call.Field_API__c,call.Operator__c,call.Value__c);
        if(call.Field_API1__c!=null && call.Field_API1__c!='' && call.Operator1__c!='' && call.Operator1__c!=null)
        condn2= generateQuery.conditionquery(call.Field_API1__c,call.Operator1__c,call.Value1__c);
        if(call.Field_API2__c!=null && call.Field_API2__c!='' && call.Operator2__c!='' && call.Operator2__c!=null)
        condn3= generateQuery.conditionquery(call.Field_API2__c,call.Operator2__c,call.Value2__c);
        if(call.Field_API3__c!=null && call.Field_API3__c!='' && call.Operator3__c!='' && call.Operator3__c!=null)
        condn4= generateQuery.conditionquery(call.Field_API3__c,call.Operator3__c,call.Value3__c);
        if(call.Field_API4__c!=null && call.Field_API4__c!='' && call.Operator4__c!='' && call.Operator4__c!=null)
        condn5= generateQuery.conditionquery(call.Field_API4__c,call.Operator4__c,call.Value4__c);
        
        if(condn1 != null  && call.Field_API__c != null)
          filterMap.put(1,condn1);
        if( condn2 != null && call.Field_API1__c != null)
            filterMap.put(2,condn2);
        if( condn3 != null && call.Field_API2__c != null)
            filterMap.put(3,condn3);
        if(condn4 != null && call.Field_API3__c != null)
            filterMap.put(4,condn4);
        if(condn5 != null && call.Field_API4__c != null)
            filterMap.put(5,condn5);
        if(String.isNotBlank(call.Custom_Logic__c)){
            conditions = call.Custom_Logic__c;
            for(Integer i = 1; i <= filterMap.size(); i++){
                if(conditions.contains(String.valueOf(i))){
                    conditions = conditions.replace(String.valueOf(i),filterMap.get(i));
                }
            }
            conquery += ' WHERE (' + conditions + ') ';
        } else{
            If(condn1 != null  && call.Field_API__c != null)  
                conquery=conquery+' where '+condn1;
            if( condn2 != null && call.Field_API1__c != null)
                conquery=conquery+' AND '+ condn2;
            if( condn3 != null && call.Field_API2__c != null)
                conquery=conquery+' AND '+condn3;
            if(condn4 != null && call.Field_API3__c != null)
                conquery=conquery+' AND '+condn4;
            if(condn5 != null && call.Field_API4__c != null)
                conquery=conquery+' AND '+condn5; 
        }     

        If(condn1 != null  && call.Field_API__c != null)
          conquery=conquery+' AND ' + 'CampaignId =' + '\''+call.CampaignName__c+'\'';
        else
          conquery=conquery+' Where ' + 'CampaignId =' + '\''+call.CampaignName__c+'\'';
        
        
         query =  conquery;       
System.debug('*******:QUERY:********\n'+Query);
  system.debug('Myquery is......'+Query);
return Database.getQueryLocator(query);  

}
catch(Exception e)
{
 query='Select Id from Contact limit 0'; 
 Delete lockedCL;
 Delete callistname;
 system.debug('Check the Exception.....'+e);
 Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
 String[] toAddresses = new String[] {'nvanamala@colecapital.com'};
 mail.setToAddresses(toAddresses);

 mail.setSubject('Exception Message');
 mail.setPlainTextBody
 ('CallList ' + call.Name + ' ' +
  'has not been created because of .' + e.getMessage());
 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
 
  return Database.getQueryLocator(query);
  
}*/
return null;
}  

global void execute(Database.BatchableContext BC,List<CampaignMember> scope)  
{  
 /*system.debug('Check the name of the call list name:'+call);
List<contact_call_list__c> callist = new List<contact_call_list__c>();
If(scope.size()>0)
{
for(CampaignMember c : scope) {
contact_call_list__c cls = new contact_call_list__c();
cls.CallList_Name__c = call.name ;
cls.Contact__c = c.ContactId;
cls.campId__c = call.CampaignName__c;
callist.add(cls);

}
try {         
            insert callist;
          
        } catch (system.dmlexception e) {
            System.debug('ccl not inserted: ' + e);
        }
}*/
}

global void finish(Database.BatchableContext BC)  
{ 
/*AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];

 Automated_Call_List__c unlockcl = [Select Id, Name, TempLock__c  from Automated_Call_List__c where Name = :call.Name];
 system.debug('Name of Locked Cl' +unlockcl.name);
 unlockcl.TempLock__c  = null;
  update unlockcl;

 List<Contact_call_list__c> checkifinsert = [Select Id from Contact_Call_list__c where calllistorder__c = :unlockcl.Id limit 1];
If(checkifinsert.size()>0)
{
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
String[] toAddresses = new String[] {a.CreatedBy.Email};
String[] bccAddresses = new String[] {'nvanamala@colecapital.com'};
mail.setToAddresses(toAddresses);
mail.setBccSender(true);
mail.setBccAddresses(bccAddresses);
mail.setSubject('Call List generate process ' + a.Status);
mail.setPlainTextBody
('CallList ' + call.Name + ' ' +
'has been created');
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}*/
}
}