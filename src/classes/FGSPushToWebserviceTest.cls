@isTest
private class FGSPushToWebserviceTest {
    static testMethod void myUnitTest() {
        Contact c0 = new Contact(FirstName = 'Test', LastName = 'Contact');
        insert c0;
        
        Contact c1 = new Contact(FirstName = 'Test', LastName = 'Contact', Company_Name__c = 'Test Company', Full_Name__c = 'Full Name', MailingStreet = 'Test Street', MailingCity = 'Test City', MailingState = 'Test State', mailingPostalCode = 'Test Code', email = 'Testemail@testcompany.com');
        insert c1;
        
        System.assertEquals(c0.FirstName,c1.FirstName);

        
        Kit_Request__c k = new Kit_Request__c();
        insert k;
        
        FGSPushToWebservice pusher = new FGSPushToWebservice();
        
        test.startTest();
            // Test with incomplete contact
            String result0 = pusher.createDoc(c0.id, k.id);
            // Test with complete contact
            String result1 = pusher.createDoc(c1.id, k.id);
            // Test sending a doc
            String wsresult = FGSPushToWebservice.sendDoc(c0.id, k.id);
        test.stopTest();
    }
}