@isTest
 private class Test_repCountByTicketsBatch 
 {
    public static testMethod void testrepCountByTicketsBatch() 
    {
        Id recdBrokerDealer=[Select id,name,DeveloperName from RecordType where DeveloperName='BrokerDealerAccount' and SobjectType = 'Account'].id; 
        Map<id,National_Account_Helper__c> newmapaccount= new Map<id,National_Account_Helper__c>();
        Account a = new Account();
        a.name = 'testaccount';
        a.recordtypeid=recdBrokerDealer;
        insert a;
        List<National_Account_Helper__c> nahListExisting = new List<National_Account_Helper__c>();
        List<REIT_Investment__c> reitnewlist = new List<REIT_Investment__c>();

        List<National_Account_Helper__c> updatednahListExisting = new List<National_Account_Helper__c>();
        nahListExisting = [select Account__c, X180Plusdays25moreTickets__c  ,X180Plusdays4to24Tickets__c,X180Plusdays1to3Tickets__c  ,X60to180days25moreTickets__c  ,X60to180days4to24Tickets__c  ,X60To180days1to3Tickets__c  ,X30To60days25moreTickets__c  ,X30To60days1to3Ticlets__c , X30To60days4to24Tickets__c  FROM National_Account_Helper__c WHERE Account__c=:a.id]; 
        
        for(National_Account_Helper__c  nh:nahListExisting)
        {
                   nh.X30To60days1to3Ticlets__c = 0;
                   nh.X30To60days4to24Tickets__c  = 0;
                   nh.X30To60days25moreTickets__c  = 0;
                  
                   nh.X60To180days1to3Tickets__c  = 0;  
                   nh.X60to180days4to24Tickets__c  = 0;
                   nh.X60to180days25moreTickets__c  = 0;
                   
                   nh.X180Plusdays1to3Tickets__c  = 0;  
                   nh.X180Plusdays4to24Tickets__c  = 0;
                   nh.X180Plusdays25moreTickets__c  = 0;
                   UpdatednahListExisting.add(nh);
        }
         update   UpdatednahListExisting;  
         
        Date d1 = Date.today() - 30;
        Date d2 = Date.today() - 60;
        Date d3 = Date.today() - 180; 
        
        Date d11 = date.newinstance(2013, 6, 17);
        
        Date d12 = date.newinstance(2013, 4, 17);
        
        Date d13 = date.newinstance(2013, 1, 17);
         
        Contact c1= new Contact();
        c1.accountid=a.id;
        c1.firstname='xxx';
        c1.lastname='yyy';
        insert c1;
        
        Contact c2= new Contact();
        c2.accountid=a.id;
        c2.firstname='hello';
        c2.lastname='world';
        insert c2;
        
        Contact c3= new Contact();
        c3.accountid=a.id;
        c3.firstname='hi';
        c3.lastname='contact';
        insert c3;
        
        List<Contact> scope = new List<Contact>(); 
           scope.add(c3);
        
       
        
        REIT_Investment__c ct= new REIT_Investment__c();
        ct.Rep_Contact__c = c1.id;
        
        ct.Deposit_Date__c= d11;
        //ct.Fund__c='3700';
        ct.Current_Capital__c=15000;
        insert ct;
        
        //updatednahListExisting[0].CCITlastyear__c= ct.Current_Capital__c;
        REIT_Investment__c ct1= new REIT_Investment__c();
        ct1.Rep_Contact__c = c2.id;
        //date d1 = date.newInstance(Date.today()-60);
        ct1.Deposit_Date__c= d12;
        //ct1.Fund__c='3700';
        ct1.Current_Capital__c=15000;
        insert ct1; 
        
        REIT_Investment__c ccptlastyear= new REIT_Investment__c();
        ccptlastyear.Rep_Contact__c = c3.id;
        //date d2 = date.newInstance(Date.today()-180);
        ccptlastyear.Deposit_Date__c= d13;
        //ccptlastyear.Fund__c='3700';
        ccptlastyear.Original_Capital__c=15000;
        insert ccptlastyear;
        
         Test.StartTest();
         repCountByTicketsBatch batch1 = new repCountByTicketsBatch();
         Id batchId1 = Database.executeBatch(batch1,200);
          Database.BatchableContext BC;
          batch1.execute(BC,scope);   
        
        
        }
    }