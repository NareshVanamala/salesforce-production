public with sharing class Websites_DrupalInvocableOnContact implements Database.AllowsCallouts{
    
    @InvocableMethod(label='invokeContact' description='')
    public static void invokeContact (List<Id> contactIds) {
        if(Test.isRunningTest() && (Websites_Drupal_Endpoint_URLs__c.getAll().Values()==null || Websites_Drupal_Endpoint_URLs__c.getAll().Values().isEmpty())){
            Websites_HttpClass.insertDrupalEnpointURLsForTest();
        }
        if(Test.isRunningTest() && !isFromTestClass){
            Test.setMock(HttpCalloutMock.class, new Websites_HttpClass.Mock('{"status": "SUCCESS"}', 200));
        }

       system.debug(logginglevel.info, '---------*****----- \ncontact created/modified: '+contactIds);
       String ColeCapitalContactsEndPointURL=Websites_HttpClass.DrupalEndPointURLs('Cole Capital Contacts');
        try{
            Websites_HttpClass.makeCall(ColeCapitalContactsEndPointURL, 
                                    Websites_HttpClass.COLECAPITAL_DRUPAL_TOKEN, 
                                    Websites_HttpClass.METHOD_POST,
                                    Websites_HttpClass.TIMEOUT_MAX, 
                                    Websites_HttpClass.ACCEPT, 
                                    JSON.serialize(contactIds));
        }catch(CalloutException e){
            system.debug(logginglevel.info, '-----------CalloutException: '+e.getStackTraceString());
            system.debug(logginglevel.info, '-----------CalloutException: '+e.getMessage());
        }
        
                
    }

    public static  boolean isFromTestClass=false;
    public static void invokeContactTest (List<Id> contactIds) {
        isFromTestClass= true;
        invokeContact(contactIds);
    }
}