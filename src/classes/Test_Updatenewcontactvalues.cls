@isTest
public class Test_Updatenewcontactvalues
{
    static testmethod void myTestMethod1()
    {    
        Test.StartTest();
        
            Id recdBrokerDealer=[Select id,name,DeveloperName from RecordType where DeveloperName='BrokerDealerAccount' and SobjectType = 'Account'].id; 
            Account a = new Account();
            a.name = 'testaccount12';
            a.recordtypeid=recdBrokerDealer;
            insert a;  
            
            Contact c = new Contact();  
            Id recdBrokerDealer2=[Select id,name,DeveloperName from RecordType where DeveloperName='Institutional' and  SobjectType = 'Contact'].id; 
            c.recordtypeid=recdBrokerDealer2;
            c.AccountId=a.id;
            c.FirstName = 'Testthis';             
            c.LastName = 'Updatenewcontactvalues';                         
            c.Email = 'contact@example.com';
            c.Effective_Snooze_Until_Date__c=System.Today()- 1;
            insert c; 
            system.assertequals(c.AccountId,a.id);

            Id myidvar= Database.executeBatch(new Updatenewcontactvalues());
           
            SchedularForUpdatenewcontactvalues p = new SchedularForUpdatenewcontactvalues();
            String sch = '0 0 8 13 2 ?';
            system.schedule('SchedularForUpdatenewcontactvalues', sch, p);
            
        Test.StopTest();
    }

}