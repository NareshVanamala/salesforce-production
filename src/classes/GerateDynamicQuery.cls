public class GerateDynamicQuery {
 /*   

    String Fieldname='--None--';
    String Fieldname1='--None--';
    String Fieldname2='--None--';
    String Fieldname3='--None--';
    String Fieldname4='--None--';
    String Operator='';
    String Operator1='';
    String Operator2='';
    String Operator3='';
    String Operator4='';
    String value='';
    
    public Map<String,String> mapfieldnames{get; set;}

    public Boolean flag{get;set;}
    public String vname{get;set;}
    public Boolean success{get;set;}
    public Integer cmCount;
    
        
    //At the page load we will initialize the picklist  with the contacts fields.--Start
    public GerateDynamicQuery()
    {
       flag=false;
       success=false;
       cmCount = 0;
       
        mapfieldnames = new Map<String,String>();
        mapfieldnames.put('--None--','--None--');
        mapfieldnames.put('Account.Name','STRING');
        
       
       Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
       Schema.SObjectType sobjType = gd.get('contact');
       Schema.DescribeSObjectResult r = sobjType.getDescribe();
       Map<String,Schema.SObjectField> M = r.fields.getMap();
       System.debug('+++m'+M);
       for(String fieldName1 : M.keyset())
        {
            Schema.SObjectField field = M.get(fieldName1);                                                    
            Schema.DescribeFieldResult fieldDesc = field.getDescribe();                    
            System.debug('+++++label'+fieldDesc.getLabel());
            System.debug('+++++label'+fieldDesc.getname());
            mapfieldnames.put(fieldDesc.getname(),String.ValueOf(fieldDesc.gettype()));                                        
        }    
        
        Schema.SObjectType sobjType1 = gd.get('Account');
       Schema.DescribeSObjectResult r1 = sobjType1.getDescribe();
       Map<String,Schema.SObjectField> M1 = r1.fields.getMap();
       System.debug('+++m'+M1);
       for(String fieldName1 : M1.keyset())
        {
            Schema.SObjectField field = M1.get(fieldName1);                                                    
            Schema.DescribeFieldResult fieldDesc = field.getDescribe();                    
            System.debug('+++++label'+fieldDesc.getLabel());
            System.debug('+++++label'+fieldDesc.getname());
            mapfieldnames.put('Account.'+fieldDesc.getname(),String.ValueOf(fieldDesc.gettype()));                                        
        }       
    } 
    
    // Get the contact records based on the datatype. --Start--
    public String conditionquery(String FieldName,String Operator,String Value)
    {
        string condn='';
        if((FieldName != null  && Operator != null && Value != null))
        {
            List<String> fields = new List<String>();
            System.debug('*****maptype'+ mapfieldnames.get(FieldName)); 
            System.debug('++++++entering Assign');
            System.debug('+++++datatype'+mapfieldnames.get(FieldName));
            if(Operator == 'contains')
            {
              if(value.contains(','))
              {
                String[] valsplit=Value.split(',');
                condn='(' ;
                For(integer i=0;i<valsplit.size();i++)
                {
                   condn=condn + FieldName + ' like ' + '\''+ '%' + valsplit[i].trim() + '%' + '\'';
                   if(i < valsplit.size()-1)
                    condn=condn + 'or' + ' ';
                   System.debug('*******' + condn);
                }
                  condn=condn + ')';
              }
              else
                condn=FieldName + ' like '+ '\''+ '%' + Value + '%' + '\'';
            }
            if(Operator == 'equals')
            {
                if((String.valueof(mapfieldnames.get(FieldName)) == 'CURRENCY') || (String.valueof(mapfieldnames.get(FieldName)) == 'DOUBLE') || (String.valueof(mapfieldnames.get(FieldName)) == 'BOOLEAN'))      
                  condn=FieldName + ' = '+ Value;
                else if( (String.valueof(mapfieldnames.get(FieldName)) == 'DATE'))
                { 
                  String[] dtarray=Value.split('/');
                  if(dtarray[0].length()==1)
                    dtarray[0]='0'+dtarray[0];
                  if(dtarray[1].length()==1)
                    dtarray[1]='0'+dtarray[1];
                  String d= dtarray[2]+'-'+dtarray[0]+'-'+ dtarray[1];
                  System.debug('+++++date'+d);
                  condn=FieldName + ' = '+ d;        
                }        
                else if( (String.valueof(mapfieldnames.get(FieldName)) == 'DATETIME'))
                { 
                  String[] dtdate=Value.split(' ');
                  String dateval= dtdate[0];
                  String timeval = dtdate[1];        
                  string dtarray1=dateval;
                  String[] dtarray=dtarray1.split('/');               
                  if(dtarray[0].length()==1)
                    dtarray[0]='0'+dtarray[0];
                  if(dtarray[1].length()==1)
                    dtarray[1]='0'+dtarray[1];
                  String d= dtarray[2]+'-'+dtarray[0]+'-'+ dtarray[1];
                  System.debug('+++++date'+d);        
                  String tmarray1=timeval;
                  String[]tmarray=tmarray1.split(':');    
                  
                  if(tmarray[0].length()==1)
                    tmarray[0]='0'+tmarray[0];
                    String Hour=tmarray[0];
                 
                  if(tmarray[1].length()==1)
                     tmarray[0]='0'+tmarray[1];
                    String Min=tmarray[1];
                    string second = '00';
                
                  if(dtdate.get(2)=='PM')
                    Hour=String.ValueOf(Integer.ValueOf(Hour) + 12);
                    
                   String d1=Hour+':'+Min+':'+second;
                   String d2=d+'T'+d1+'Z';
                   condn=FieldName + ' = '+ d2;      
                }               
                else if(value.contains(','))
                {
                   String[] valsplit=Value.split(',');
                   condn='(' ;
                   For(integer i=0;i<valsplit.size();i++)
                   {
                     condn=condn + FieldName + ' = ' + '\''+ valsplit[i].trim()+ '\'';
                     if(i < valsplit.size()-1)
                      condn=condn + 'or' + ' ';
                      System.debug('*******' + condn);
                   }
                     condn=condn + ')';
                }          
                else
                  condn=FieldName + ' = '+'\''+ Value + '\'';
            }
            if(Operator == 'does not contain')
            {
                if(value.contains(','))
                {
                   String[] valsplit=Value.split(',');
                   condn='(' ;
                   For(integer i=0;i<valsplit.size();i++)
                   {
                     condn=condn + '(' + ' NOT '+ FieldName + ' like ' + '\''+ '%' + valsplit[i].trim() + '%' + '\'' + ')';
                     if(i < valsplit.size()-1)
                       condn=condn + ' ' + 'AND' + ' ';
                       System.debug('*******' + condn);
                   }
                    condn=condn + ')';
                }
                else
                   condn='( NOT '+FieldName+ ' like '+'\''+ '%' + Value + '%' + '\')';
            }
            if(Operator == 'not equal to')
            {
                if((String.valueof(mapfieldnames.get(FieldName)) == 'CURRENCY') || (String.valueof(mapfieldnames.get(FieldName)) == 'DOUBLE'))      
                      condn=FieldName + '<> '+ Value;
                else if( (String.valueof(mapfieldnames.get(FieldName)) == 'DATE'))
                {
                      String[] dtarray=Value.split('/');
                      if(dtarray[0].length()==1)
                        dtarray[0]='0'+dtarray[0];
                      if(dtarray[1].length()==1)
                        dtarray[1]='0'+dtarray[1];
                      String d= dtarray[2]+'-'+dtarray[0]+'-'+ dtarray[1];
                      System.debug('+++++date'+d);
                      condn=FieldName + ' <> '+ d;        
                } 
                else if( (String.valueof(mapfieldnames.get(FieldName)) == 'DATETIME'))
                { 
                      String[] dtdate=Value.split(' ');
                      String dateval= dtdate[0];
                      String timeval = dtdate[1];
                     
                      string dtarray1=dateval;
                      String[] dtarray=dtarray1.split('/');
                            
                      if(dtarray[0].length()==1)
                        dtarray[0]='0'+dtarray[0];
                      if(dtarray[1].length()==1)
                        dtarray[1]='0'+dtarray[1];
                      String d= dtarray[2]+'-'+dtarray[0]+'-'+ dtarray[1];
                      System.debug('+++++date'+d);
                     
                      String tmarray1=timeval;
                      String[]tmarray=tmarray1.split(':');
                     
                      if(tmarray[0].length()==1)
                        tmarray[0]='0'+tmarray[0];
                        
                      String Hour=tmarray[0];
                     
                      if(tmarray[1].length()==1)
                        tmarray[0]='0'+tmarray[1];
                        
                      String Min=tmarray[1];
                      string second = '00';
                    
                      if(dtdate.get(2)=='PM')
                        Hour=String.ValueOf(Integer.ValueOf(Hour) + 12);
                        
                        String d1=Hour+':'+Min+':'+second;
                        String d2=d+'T'+d1+'Z';
                        condn=FieldName + ' <> '+ d2;        
                }
                else if(value.contains(','))
                {
                
                     String[] valsplit=Value.split(',');
                     condn='(' ;
                     For(integer i=0;i<valsplit.size();i++)
                     {
                         condn=condn + FieldName + ' <> ' + '\''+ valsplit[i].trim()+ '\'';
                         if(i < valsplit.size()-1)
                         condn=condn + 'AND' + ' ';
                         System.debug('*******' + condn);
                     }
                     condn=condn + ')';
                } 
                else
                    condn=FieldName + ' <> '+'\''+ Value + '\'';
            }
            if(Operator == 'starts with')
            {
                if(value.contains(','))
                {
                  String[] valsplit=Value.split(',');
                  condn='(' ;
                  For(integer i=0;i<valsplit.size();i++)
                  {
                       condn=condn + FieldName+ ' like '+'\''+ valsplit[i].trim()+ '%\'';
                       if(i < valsplit.size()-1)
                       condn=condn + 'or' + ' ';
                       System.debug('*******' + condn);
                  }
                 condn=condn + ')';
                } 
                else
                  condn=FieldName+ ' like '+'\''+ Value + '%\'';
            }
            if(Operator == 'less than')
            {
                if((String.valueof(mapfieldnames.get(FieldName)) == 'CURRENCY') || (String.valueof(mapfieldnames.get(FieldName)) == 'DOUBLE'))      
                     condn=FieldName + ' < '+ Value;
                else if((String.valueof(mapfieldnames.get(FieldName)) == 'DATE'))
                {
                     String[] dtarray=Value.split('/');
                     if(dtarray[0].length()==1)
                     dtarray[0]='0'+dtarray[0];
                     if(dtarray[1].length()==1)
                     dtarray[1]='0'+dtarray[1];
                     String d= dtarray[2]+'-'+dtarray[0]+'-'+ dtarray[1];
                     System.debug('+++++date'+d);
                     condn=FieldName + ' < '+ d;        
                }
                else if( (String.valueof(mapfieldnames.get(FieldName)) == 'DATETIME'))
                { 
                  String[] dtdate=Value.split(' ');
                  String dateval= dtdate[0];
                  String timeval = dtdate[1];
                 
                  string dtarray1=dateval;
                  String[] dtarray=dtarray1.split('/');
                        
                  if(dtarray[0].length()==1)
                    dtarray[0]='0'+dtarray[0];
                  if(dtarray[1].length()==1)
                    dtarray[1]='0'+dtarray[1];
                  String d= dtarray[2]+'-'+dtarray[0]+'-'+ dtarray[1];
                  System.debug('+++++date'+d);
                 
                  String tmarray1=timeval;
                  String[]tmarray=tmarray1.split(':');
                 
                  if(tmarray[0].length()==1)
                    tmarray[0]='0'+tmarray[0];
                  String Hour=tmarray[0];
                 
                  if(tmarray[1].length()==1)
                    tmarray[0]='0'+tmarray[1];
                  String Min=tmarray[1];
                  string second = '00';
                
                  if(dtdate.get(2)=='PM')
                    Hour=String.ValueOf(Integer.ValueOf(Hour) + 12);
                    
                  String d1=Hour+':'+Min+':'+second;
                  String d2=d+'T'+d1+'Z';
                  condn=FieldName + ' < '+ d2;
                
                }   
                else
                   condn=FieldName + ' < '+'\''+ Value + '\'';
            }           
            if(Operator == 'greater than')
            {
                if((String.valueof(mapfieldnames.get(FieldName)) == 'CURRENCY') || (String.valueof(mapfieldnames.get(FieldName)) == 'DOUBLE'))      
                   condn=FieldName + ' > '+ Value;
                else if( (String.valueof(mapfieldnames.get(FieldName)) == 'DATE'))
                {
                   String[] dtarray=Value.split('/');
                   if(dtarray[0].length()==1)
                    dtarray[0]='0'+dtarray[0];
                   if(dtarray[1].length()==1)
                     dtarray[1]='0'+dtarray[1];
                   String d= dtarray[2]+'-'+dtarray[0]+'-'+ dtarray[1];
                   System.debug('+++++date'+d);
                   condn=FieldName + ' > '+ d;        
                }
                else if( (String.valueof(mapfieldnames.get(FieldName)) == 'DATETIME'))
                {
                   String[] dtdate=Value.split(' ');
                   String dateval= dtdate[0];
                   String timeval = dtdate[1];        
                   string dtarray1=dateval;
                   String[] dtarray=dtarray1.split('/');
                        
                   if(dtarray[0].length()==1)
                     dtarray[0]='0'+dtarray[0];
                   if(dtarray[1].length()==1)
                     dtarray[1]='0'+dtarray[1];
                   String d= dtarray[2]+'-'+dtarray[0]+'-'+ dtarray[1];
                   System.debug('+++++date'+d);
                 
                   String tmarray1=timeval;
                   String[]tmarray=tmarray1.split(':');
                 
                   if(tmarray[0].length()==1)
                     tmarray[0]='0'+tmarray[0];
                   String Hour=tmarray[0];
                 
                   if(tmarray[1].length()==1)
                     tmarray[0]='0'+tmarray[1];
                   String Min=tmarray[1];
                   string second = '00';
                
                   if(dtdate.get(2)=='PM')
                     Hour=String.ValueOf(Integer.ValueOf(Hour) + 12);
                     
                   String d1=Hour+':'+Min+':'+second ;
                   String d2=d+'T'+d1+'Z';
                   condn=FieldName + ' > '+ d2;       
                }
                else
                   condn=FieldName + ' > '+'\''+ Value + '\'';
            }
            if(Operator == 'less or equal')
            {
                if((String.valueof(mapfieldnames.get(FieldName)) == 'CURRENCY') || (String.valueof(mapfieldnames.get(FieldName)) == 'DOUBLE'))      
                    condn=FieldName + '<= '+ Value;
                else if( (String.valueof(mapfieldnames.get(FieldName)) == 'DATE'))
                {
                    String[] dtarray=Value.split('/');
                    if(dtarray[0].length()==1)
                     dtarray[0]='0'+dtarray[0];
                    if(dtarray[1].length()==1)
                     dtarray[1]='0'+dtarray[1]; 
                    String d= dtarray[2]+'-'+dtarray[0]+'-'+ dtarray[1];
                    System.debug('+++++date'+d);
                    condn=FieldName + ' <= '+ d;        
                }
                else if( (String.valueof(mapfieldnames.get(FieldName)) == 'DATETIME'))
                { 
                     String[] dtdate=Value.split(' ');
                     String dateval= dtdate[0];
                     String timeval = dtdate[1];
                     
                     string dtarray1=dateval;
                     String[] dtarray=dtarray1.split('/');
                            
                     if(dtarray[0].length()==1)
                      dtarray[0]='0'+dtarray[0];
                     if(dtarray[1].length()==1)
                      dtarray[1]='0'+dtarray[1];
                     String d= dtarray[2]+'-'+dtarray[0]+'-'+ dtarray[1];
                     System.debug('+++++date'+d);
                     
                     String tmarray1=timeval;
                     String[]tmarray=tmarray1.split(':');
                     
                     if(tmarray[0].length()==1)
                      tmarray[0]='0'+tmarray[0];
                     String Hour=tmarray[0];
                     
                     if(tmarray[1].length()==1)
                      tmarray[0]='0'+tmarray[1];
                     String Min=tmarray[1];
                     string second = '00';
                    
                     if(dtdate.get(2)=='PM')
                       Hour=String.ValueOf(Integer.ValueOf(Hour) + 12);
                       
                     String d1=Hour+':'+Min+':'+second;
                     String d2=d+'T'+d1+'Z';
                     condn=FieldName + ' <= '+ d2;       
                } 
                else
                 condn=FieldName + ' <= '+'\''+ Value + '\'';
            }
            if(Operator == 'greater or equal')
            {
                if((String.valueof(mapfieldnames.get(FieldName)) == 'CURRENCY') || (String.valueof(mapfieldnames.get(FieldName)) == 'DOUBLE'))      
                    condn=FieldName + ' >= '+ Value;
                else if( (String.valueof(mapfieldnames.get(FieldName)) == 'DATE'))
                {
                    String[] dtarray=Value.split('/');
                    if(dtarray[0].length()==1)
                      dtarray[0]='0'+dtarray[0];
                    if(dtarray[1].length()==1)
                      dtarray[1]='0'+dtarray[1];
                    String d= dtarray[2]+'-'+dtarray[0]+'-'+ dtarray[1];
                    System.debug('+++++date'+d);
                    condn=FieldName + ' >= '+ d;        
                }
                else if( (String.valueof(mapfieldnames.get(FieldName)) == 'DATETIME'))
                { 
                    String[] dtdate=Value.split(' ');
                    String dateval= dtdate[0];
                    String timeval = dtdate[1];
                 
                    string dtarray1=dateval;
                    String[] dtarray=dtarray1.split('/');
                        
                    if(dtarray[0].length()==1)
                     dtarray[0]='0'+dtarray[0];
                    if(dtarray[1].length()==1)
                     dtarray[1]='0'+dtarray[1];
                    String d= dtarray[2]+'-'+dtarray[0]+'-'+ dtarray[1];
                    System.debug('+++++date'+d);
                 
                    String tmarray1=timeval;
                    String[]tmarray=tmarray1.split(':');
                 
                    if(tmarray[0].length()==1)
                      tmarray[0]='0'+tmarray[0];
                    String Hour=tmarray[0];
                 
                    if(tmarray[1].length()==1)
                      tmarray[0]='0'+tmarray[1];
                    String Min=tmarray[1];
                    string second = '00';
                
                    if(dtdate.get(2)=='PM')
                      Hour=String.ValueOf(Integer.ValueOf(Hour) + 12);
                    String d1=Hour+':'+Min+':'+second;
                    String d2=d+'T'+d1+'Z';
                    condn=FieldName + ' >= '+ d2;        
                } 
                else
                  condn=FieldName + ' >= '+'\''+ Value + '\'';
            }
            if(Operator == 'includes')
            {    
                if(value.contains(';'))
                {
                   String[] valsplit=Value.split(';');
                   condn=FieldName+ ' includes(';
                   For(integer i=0;i<valsplit.size();i++)
                   {
                     condn=condn+'\''+ valsplit[i]+'\'';
                     if(i < valsplit.size()-1)
                        condn=condn+',';
                      System.debug('*******' + condn);
                   }
                   condn=condn+')' + ' ' + 'and' + ' ' + FieldName + ' <> null';  
                }
                else if(value.contains(','))                
                {
                    String[] valsplit=Value.split(',');
                    condn=FieldName+ ' includes(';
                    For(integer i=0;i<valsplit.size();i++)
                    {
                      condn=condn+'\''+ valsplit[i]+'\'';
                       if(i < valsplit.size()-1)
                         condn=condn+',';
                      System.debug('*******' + condn);
                    }
                     condn=condn+')' + ' ' + 'and' + ' ' + FieldName + ' <> null';     
                }
                else
                    condn=FieldName+'  includes('+ '\'' + value + '\'' + ')';
            }
            if(Operator == 'excludes')
            {
                if(value.contains(';'))
                {
                    String[] valsplit=Value.split(';');
                    condn=FieldName+ ' excludes(';
                    For(integer i=0;i<valsplit.size();i++)
                    {
                      condn=condn+'\''+ valsplit[i]+'\'';
                       if(i < valsplit.size()-1)
                       condn=condn+',';
                    }
                    condn=condn+')' + ' ' + 'and' + ' ' + ' ' + FieldName + ' <> null';
                }
                else if(value.contains(','))
                {
                    String[] valsplit=Value.split(',');
                    condn=FieldName+ ' excludes(';
                    For(integer i=0;i<valsplit.size();i++)
                    {
                     condn=condn+'\''+ valsplit[i]+'\'';
                     if(i < valsplit.size()-1)
                     condn=condn+',';
                    }
                    condn=condn+')' + ' ' + 'and' + ' ' + ' ' + FieldName + ' <> null';
                }
                else
                   condn=FieldName+'  excludes('+ '\'' + value + '\'' + ')';
            }  
             system.debug('++++condnquery :'+condn);   
        }
        if((FieldName != null  && Operator != null && Value == null))
        {
              List<String> fields = new List<String>();
              if(Operator == 'equals')
                condn=FieldName + ' = '+ Null;
              if (Operator == 'not equal to')
                condn=FieldName + ' <> '+ Null;
              if(Operator == 'contains')   
                condn=FieldName + ' like '+ '\''+ '%' + Null + '%' + '\''; 
        } 
               return condn;
    }  // --End of Condition query  method
    
*/
}