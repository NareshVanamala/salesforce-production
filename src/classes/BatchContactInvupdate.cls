global class BatchContactInvupdate implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //String query = 'SELECT Id,Pdoc__c,Attachment_Id__c FROM Contact_InvestorList__c where Attachment_Id__c=null' ;
        
        String query = 'SELECT Attachment_Id__c,Investor_Last_Name__c,Pdoc_Ready__c,Pdoc__c FROM Contact_InvestorList__c WHERE Pdoc_Ready__c = true AND Attachment_Id__c = null ';
        
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Contact_InvestorList__c> scope) {
        List<Contact_InvestorList__c> coninvList = new List<Contact_InvestorList__c>();
         for(Contact_InvestorList__c ConInv : scope)
         {
             ConInv.Pdoc__c = true; 
             coninvList.add(ConInv);           
         }
         if (Schema.sObjectType.Contact_InvestorList__c.isUpdateable()) 

         update coninvList;
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}