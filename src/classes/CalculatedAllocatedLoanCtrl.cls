public with sharing class CalculatedAllocatedLoanCtrl{
    public string loanId{get;set;}
    public list<Loan_Relationship__c> loanRelationShipList {get;set;}
    public CalculatedAllocatedLoanCtrl(ApexPages.StandardController stdController) {
        loanId = stdController.getRecord().id;
        loanRelationShipList = new list<Loan_Relationship__c>();
        for(Loan_Relationship__c loanRelObj : [select id,Deal__r.name,Purchase_Price__c,Loan_Amount__c,Ratio__c,Loan__r.Total_Loan_Amount__c,Loan__r.Total_Purchase_Price__c from Loan_Relationship__c where Loan__c =:loanId limit 1000]){
                decimal totalLoanAmount = 0.0;
                totalLoanAmount =  loanRelObj.Loan__r.Total_Loan_Amount__c;  
                decimal ratio = loanRelObj.Ratio__c;
                if(totalLoanAmount  == null)
                {
                totalLoanAmount = 0.0;
                }
                if(ratio == null)
                {
                ratio=0.0;
                }
                loanRelObj.Loan_Amount__c = ratio * totalLoanAmount ;
                system.debug('totalLoanAmount '+totalLoanAmount );
                loanRelationShipList.add(loanRelObj);
        }
    }
    public pageReference Save(){
    
    if (Schema.sObjectType.Loan_Relationship__c.isUpdateable()) 

    update loanRelationShipList;
    PageReference page = new PageReference('/'+loanId);
    page.setRedirect(true);
    return page;
    }
    Public pageReference cancel(){
    PageReference page = new PageReference('/'+loanId);
    page.setRedirect(true);
    return page;
    }
    
}