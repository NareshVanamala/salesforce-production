global class schedularforbusinessprofileContact 
{
 /*implements System.Schedulable public schedularforbusinessprofileContact()
  global void execute(SchedulableContext sc)
 {
   List<Contact>Expirysixmonths=new list<Contact>();
   List<Contact>Expiryoneyesr=new List<Contact>();
   date beforSixmonths=date.newInstance(Date.today().Year(),date.today().month()-6,date.today().day());
   date beforeoneYear= date.newInstance(date.today().year()-1,date.today().month(),date.today().day());
   Expirysixmonths=[Select Id, Name,LastModifiedDate from Contact where LastModifiedDate =:beforSixmonths And RecordTypeId = '012300000004rLn'];
   Expiryoneyesr=[Select Id, Name, LastModifiedDate from Contact where LastModifiedDate =:beforeoneYear And RecordTypeId = '012300000004rLn'];
 
  For Business profile question section
 
 List<Business_Profile_Tracking__c> tBusinessProfilequestion = [select ID,Contact__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from Business_Profile_Tracking__c where (Name='Total AUM Updated'OR Name='Total AUM Created' OR Name='Clients with RE in Portfolio Created Updated'or Name='Clients with RE in Portfolio Created' Or Name='Other RE Exposure Updated' or Name='Other RE Exposure Created'or name='Allocated to RE' or name=''Allocated to RE Updated'or name='BD Top Producer Updated' or name='BD Top Producer Created'or name='Top REIT Producer Created'or name='Top REIT Producer Updated') AND Contact__c  in : Expiryoneyesr];
   
List<id>Aumidlist=new list<id>();
List<Contact>AumACCist=new list<Contact>();
list<Contact>updatedAUM=new list<Contact>();

List<id>clientsinREidlist=new list<id>();
List<Contact>clientsinREACClist=new list<Contact>();
List<Contact>updatedClientsinReAcc=new list<Contact>();

List<id>Rexposureidlist=new list<id>();
List<Contact>RexposureACClist =new list<Contact>();
List<Contact>updatedReExposure=new list<Contact>();

List<id>AllocatedtoREidlist=new list<id>();
List<Contact>AllocatedtoREACCist=new list<Contact>();
List<Contact>updatedallocatetore=new list<Contact>();

List<id>Topreitproduceridlist=new list<id>();
List<Contact>TopreitproducerACCist=new list<Contact>();
List<Contact>updateTopreitproducer=new list<Contact>();


List<id>BDTopproduceridlist=new list<id>();
List<Contact>BDTopproducerACCist=new list<Contact>();
List<Contact>BDupdateTopproducer=new list<Contact>();

list<id>alternateInvestementid=new list<id>();
List<Contact>alternateInvestementACCist=new list<Contact>();
List<Contact>UpdatedAlternateInvestementACC=new list<Contact>();

list<id>OtheralternateInvestementid=new list<id>();
List<Contact>OtheralternateInvestementACCist=new list<Contact>();
List<Contact>UpdatedOtherAlternateInvestementACC=new list<Contact>();



 List<BusinessProfileContactTracking__c> tBusinessProfilequestion = [select ID,Contact__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from BusinessProfileContactTracking__c where (Name='Total AUM Updated'OR Name='Total AUM Created' OR Name='% of Clients with RE in Portfolio Updated'or Name='Clients with RE in Portfolio Created' Or Name='RE Exposure Through Updated' or Name=' RE Exposure Through  Created'or name='Allocated to RE' or name='Allocated to RE Updated'or name='BD Top Producer Updated' or name='BD Top Producer Created'or name='Alternative Investments(Firm Provided) Updated' or name='Alternative Investments(Firm Provided) Created') AND Contact__c  in : Expiryoneyesr];

List<Business_Profile_Tracking__c> tsizeOfBusiness = [select ID,Account__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from Business_Profile_Tracking__c where (Name='Total AUM Updated'or name='Total AUM Created' OR Name='Clients with RE in Portfolio Created'or name='% of Clients with RE in Portfolio Updated' Or Name ='Other RE Exposure Updated'or name='Other RE Exposure Created' or name='Allocated to RE' or name='Allocated to RE Updated'or name='RE Exposure Through Updated'or name='RE Exposure Through Created') AND  Account__c in : Expiryoneyesr];
 for(BusinessProfileContactTracking__c ct2 : tBusinessProfilequestion )
     {
          if(ct2.name=='Total AUM Updated' || ct2.name=='Total AUM Created')
             Aumidlist.add(ct2.Contact__c);
          if(ct2.name=='Clients with RE in Portfolio Created'||ct2.name=='% of Clients with RE in Portfolio Updated') 
               clientsinREidlist.add(ct2.Contact__c);
           if(ct2.name=='RE Exposure Through Updated'|| ct2.name==' RE Exposure Through Created')     
                Rexposureidlist.add(ct2.Contact__c);
          if(ct2.name=='BD Top Producer Updated'|| ct2.name=='BD Top Producer Created')     
                BDTopproduceridlist.add(ct2.Contact__c);
           if(ct2.name=='Top REIT Producer Created'|| ct2.name=='Top REIT Producer Updated')     
                Topreitproduceridlist.add(ct2.Contact__c);
           if(ct2.name=='Allocated to RE'|| ct2.name=='Allocated to RE Updated')     
                AllocatedtoREidlist.add(ct2.Contact__c); 
           if(ct2.name=='Alternative Investments(Firm Provided) Updated'|| ct2.name=='Alternative Investments(Firm Provided) Created')     
                alternateInvestementid.add(ct2.Contact__c); 
        if(ct2.name=='Other Alternate Investments Updated'|| ct2.name=='Other Alternate Investments Created')     
                OtheralternateInvestementid.add(ct2.Contact__c);          
           
        }
        alternateInvestementACCist=[select id,Alternative_Investments_Firm_Provided__c ,Alternative_Investments_Check__c from Contact where id in: alternateInvestementid];
         for(Contact ct1: alternateInvestementACCist)
         {
            ct1.Alternative_Investments_Check__c=true; 
            ct1.Alternative_Investments_Firm_Provided__c=null;
            UpdatedAlternateInvestementACC.add(ct1);
           }
           helper.run=false;
           if(UpdatedAlternateInvestementACC.size()>0)
           update UpdatedAlternateInvestementACC;
     
        OtheralternateInvestementACCist=[select id,Other_Alternate_Investments__c,Other_Alternate_Investments_Check__c from Contact where id in: OtheralternateInvestementid];          
        for(Contact ct2: OtheralternateInvestementACCist)
        {
            ct2.Other_Alternate_Investments_Check__c=true; 
            ct2.Other_Alternate_Investments__c=null;
            UpdatedOtherAlternateInvestementACC.add(ct2);
        
        }
        helper.run=false;
        if(UpdatedOtherAlternateInvestementACC.size()>0)
        update UpdatedOtherAlternateInvestementACC;
        
      AumACCist=[select id, TotalAUMCheck__c,BusSize_Total_AUM__c  from Contact where id in: Aumidlist];
      for(Contact acc1: AumACCist) 
      {
           acc1.TotalAUMCheck__c=true;
           acc1.BusSize_Total_AUM__c=null;
          updatedAUM.add(acc1);
       } 
       helper.run=false;
       if(updatedAUM.size()>0)
        update updatedAUM;
       
       clientsinREACClist=[select id, of_Clients_with_RE_in_Portfoliocheck__c,TOP5_of_Clients_with_RE_in_Portfolio__c from Contact where id in: clientsinREidlist]; 
      
        for(Contact acc2: clientsinREACClist) 
      {
          acc2.of_Clients_with_RE_in_Portfoliocheck__c =true;
          acc2.TOP5_of_Clients_with_RE_in_Portfolio__c  =null;
          updatedClientsinReAcc.add(acc2);
       }
       helper.run=false;
       if(updatedClientsinReAcc.size()>0)
       update updatedClientsinReAcc;
       
       
       RexposureACClist =[select id,RE_Exposure_Through_check__c,Bus_Size_RE_Exposure_Through__c,Other_RE_Exposure__c from Contact where id in: Rexposureidlist];
        for(Contact acc3:RexposureACClist) 
      {
           acc3.RE_Exposure_Through_check__c =true;
           acc3.Bus_Size_RE_Exposure_Through__c =null;
           acc3.Other_RE_Exposure__c=null; 
          updatedReExposure.add(acc3);
        }
        helper.run=false;
        if(updatedReExposure.size()>0)
        update updatedReExposure;
        helper.run=true;
        
       AllocatedtoREACCist=[select id, Allocated_to_RE_check__c,Bus_Size_Allocated_to_RE__c from Contact where id in: AllocatedtoREidlist];
        for(Contact acc4: AllocatedtoREACCist) 
      {
           acc4.Allocated_to_RE_check__c  =true;
           acc4.Bus_Size_Allocated_to_RE__c  =null;
           updatedallocatetore.add(acc4);
        }
         helper.run=false;
        if(updatedallocatetore.size()>0)
        update updatedallocatetore;
        helper.run=true;
           
      TopreitproducerACCist=[select id,Top_REIT_Producer__c,Top_REIT_Producer_check__c from Contact where id in: Topreitproduceridlist];
         for(Contact ct1: TopreitproducerACCist)
         {
             ct1.Top_REIT_Producer_check__c  =true;
             ct1.Top_REIT_Producer__c =null; 
             updateTopreitproducer.add(ct1);
         }
        helper.run=false;  
         if(updateTopreitproducer.size()>0)
         update updateTopreitproducer; 
        helper.run=true; 
    
      BDTopproducerACCist=[select id,BD_Top_Producer__c,BD_Top_Producer_check__c from Contact where id in: BDTopproduceridlist];
         for(Contact ct1: BDTopproducerACCist)
         {
             ct1.BD_Top_Producer_check__c  =true;
             ct1.BD_Top_Producer__c =null; 
             BDupdateTopproducer.add(ct1);
         }
        helper.run=false; 
        if(BDupdateTopproducer.size()>0) 
         update BDupdateTopproducer; 
        helper.run=true;
    
             for Total New Sales Last Year:
          List<BusinessProfileContactTracking__c > TtotalNewLastyear= [select ID, Contact__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from BusinessProfileContactTracking__c where (name='Total Sales Last Year Updated' or name='Total Sales Last Year Created'or Name='Fee Based (Non Commission) Updated'or name='Fee Based (Non Commission) Created' ) AND  Contact__c in : Expiryoneyesr];
         
        list<id>Totalsaleslastyearid=new list<id>();
        list<Contact>TotalsaleslastyearAcc= new list<Contact>(); 
        list<Contact>updatedTotalsaleslastyearAcc= new list<Contact>(); 
    
       list<id>FeeBasednoncommisionid=new list<id>();
       list<Contact>FeeBasednoncommisionAcc= new list<Contact>(); 
       list<Contact>updatedFeeBasednoncommisionAcc= new list<Contact>(); 

           for(BusinessProfileContactTracking__c ct1: TtotalNewLastyear)
            {
      
             if(ct1.name=='Total Sales Last Year Created' || ct1.name=='Total Sales Last Year Updated')
             Totalsaleslastyearid.add(ct1.Contact__c);
             
              if(ct1.name=='Fee Based (Non Commission) Created'||ct1.name=='Fee Based (Non Commission) Updated')
              FeeBasednoncommisionid.add(ct1.Contact__c);
            }
            
            TotalsaleslastyearAcc=[select id,Total_Sales_Last_Year__c ,Total_Sales_Last_Year_Check__c from Contact where id in: Totalsaleslastyearid];
                   for(Contact c1: TotalsaleslastyearAcc)
                   {
                       c1.Total_Sales_Last_Year_Check__c=true;  
                       c1.Total_Sales_Last_Year__c=null; 
                        updatedTotalsaleslastyearAcc.add(c1);   
                   }
                   helper.run=false;
                   if(updatedTotalsaleslastyearAcc.size()>0)
                   update updatedTotalsaleslastyearAcc;
                   
            FeeBasednoncommisionAcc=[select id,Fee_Based_Non_Commission_check__c,Fee_Based_Non_Commission__c  from Contact where id in:FeeBasednoncommisionid]; 
              for(Contact conts: FeeBasednoncommisionAcc)
            {
                 conts.Fee_Based_Non_Commission_check__c  = TRUE;
                 conts.Fee_Based_Non_Commission__c =null;
                 updatedFeeBasednoncommisionAcc.add(conts);
               }  
             helper.run=false;
            if(updatedFeeBasednoncommisionAcc.size()>0)
            update updatedFeeBasednoncommisionAcc;
            helper.run=true;
       for stage of business section
       List<BusinessProfileContactTracking__c > TStageOfBusiness= [select ID, Contact__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from BusinessProfileContactTracking__c where (Name='Business Stage Updated'  or Name='How Can We Help Updated'or name='How_would_you_describe_your_client_base__c Updated' or name='Business Stage Created' or name='How Can We Help Created'or name='How would you describe your client base Created') AND  Contact__c in : Expirysixmonths];
       list<id>businesstageIdlist=new list<id>();
       list<Contact>businesstageAcclist=new list<Contact>();
       list<Contact>updatdBusinessStage=new List<Contact>();
      
       list<id>otherbusinesstageIdlist=new list<id>();
       list<Contact>otherbusinesstageAcclist=new list <Contact>();
    list<Contact>updatdotherBusinessStage=new List<Contact>();
       
       
       list<id>howcanwehelpIdlist=new list<id>();
       list<Contact>howcanwehelpAcclist=new list <Contact>();
       list<Contact>updatdhowcanwehelp=new List<Contact>();
       
       list<id>howwouldyoudescribeurclientbase=new list<id>();
       list<Contact>howwouldyoudescribeurclientbaseconlist=new list<Contact>();
       list<Contact>Updatehowyoudescribeurclientbase=new list<Contact>();
     
      for(BusinessProfileContactTracking__c x :TStageOfBusiness)
       {
              if(x.name=='Business Stage Updated' ||x.name=='Business Stage Created')
              businesstageIdlist.add(x.Contact__c);
              
              if(x.name=='Other Business Stage Updated' ||x.name=='Other Business Stage Created')
              otherbusinesstageIdlist.add(x.Contact__c);
              
              if(x.name=='How Can We Help Updated' ||x.name=='How Can We Help Created')
              howcanwehelpIdlist.add(x.Contact__c);
              
              
             if(x.name=='How_would_you_describe_your_client_base__c Updated' || x.name=='How would you describe your client base Created')
              howwouldyoudescribeurclientbase.add(x.Contact__c);
      
          }
       businesstageAcclist =[select id, Business_Stage__c,Other_Business_Stage__c ,How_Can_We_Help__c ,Business_Stage_check__c,How_Can_We_Help_check__c from Contact where id in :businesstageIdlist];
       for(Contact accounts : businesstageAcclist )
            {
             accounts.Business_Stage_check__c = TRUE;
           accounts.Business_Stage__c=null;
           accounts.Other_Business_Stage__c=null; 
             updatdBusinessStage.add(accounts);
             }
             helper.run=false;
             if(updatdBusinessStage.size()>0)
            update updatdBusinessStage;
            helper.run=true;
            
       otherbusinesstageAcclist=[select id,Business_Stage__c,Other_Business_Stage__c ,How_Can_We_Help__c, Business_Stage_check__c,Other_Business_Stage_check__c,How_Can_We_Help_check__c from Contact where id in : otherbusinesstageIdlist];
  
             for(Contact accounts : otherbusinesstageAcclist)
            {
             accounts.Other_Business_Stage_check__c = TRUE;
             accounts.Other_Business_Stage__c=null;
             updatdotherBusinessStage.add(accounts);
             }
             helper.run=false;
            update updatdotherBusinessStage; 
            helper.run=true;
  
         howcanwehelpAcclist =[select id,Business_Stage__c,Other_Business_Stage__c ,How_Can_We_Help__c ,Business_Stage_check__c,How_Can_We_Help_check__c from Contact where id in : howcanwehelpIdlist];

          for(Contact accounts : howcanwehelpAcclist )
            {
             accounts.How_Can_We_Help_check__c= TRUE;
             accounts.How_Can_We_Help__c  =null;
             updatdhowcanwehelp.add(accounts);
             }
              helper.run=false;
            if(updatdhowcanwehelp.size()>0)
            update updatdhowcanwehelp;
            helper.run=true; 
            
       howwouldyoudescribeurclientbaseconlist=[select id,How_would_you_describe_your_client_base__c,Howwouldusescribeyourclientcheck__c from Contact where id in: howwouldyoudescribeurclientbase];     
           for(Contact contsrec : howwouldyoudescribeurclientbaseconlist)
            {
             contsrec.Howwouldusescribeyourclientcheck__c= TRUE;
             accounts.How_Can_We_Help__c  =null;
             Updatehowyoudescribeurclientbase.add(contsrec);
             }
              helper.run=false;
            if(Updatehowyoudescribeurclientbase.size()>0)
            update Updatehowyoudescribeurclientbase;
            helper.run=true;  
       Top 3 Product Categories used Last Year
         list<id>IncomeProductsUsedid=new list<id>();
         list<Contact>IncomeProductsUsedACC=new list<Contact>();
         list<Contact>UpdateIncomeProductsUsed=new list<Contact>();
         
  List<BusinessProfileContactTracking__c > Top3productCategory= [select ID, Contact__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from BusinessProfileContactTracking__c where (Name='Income Products Used Updated'or name='Income Products Used Created') AND  Contact__c in : Expiryoneyesr];
    for(BusinessProfileContactTracking__c x: Top3productCategory)
            {
              IncomeProductsUsedid.add(x.Contact__c);
             }
      
   IncomeProductsUsedACC=[select id,Income_Products_UsedCheck__c from Contact where id in: IncomeProductsUsedid];    
       for(Contact act: IncomeProductsUsedACC)
            {
             act.Income_Products_UsedCheck__c= TRUE;
            act.Used_Nonlisted_REIT_before__c=null;
             UpdateIncomeProductsUsed.add(act); 
                       
            }
            helper.run=false;
            if(UpdateIncomeProductsUsed.size()>0)
            update UpdateIncomeProductsUsed;
                    
         Have you used a non-listed REIT before? section
        
         list<id>Nonlistedreitid=new list<id>();
         list<Contact>NonlistedreitACC=new list<Contact>();
         list<Contact>UpdateNonlistedREITs=new list<Contact>();
         
          list<id>howlongid=new list<id>();
         list<Contact>howLongACC=new list<Contact>();
         list<Contact>UpdatedHowlongACC=new list<Contact>();
         
        list<id>whatreitid=new list<id>();
         list<Contact>whatreitACC=new list<Contact>();
         list<Contact>UpdatewhatREITs=new list<Contact>();
         
         List<BusinessProfileContactTracking__c > Thaveyouusednonlisted= [select ID, Contact__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from BusinessProfileContactTracking__c where ( Name='What REITs Updated'or name='What REITs Created') AND  Contact__c in : Expiryoneyesr];
            for(BusinessProfileContactTracking__c x: Thaveyouusednonlisted)
            {
              if(x.name=='Used REIT Before Created' ||x.name=='Used REIT Before Updated')
              Nonlistedreitid.add(x.Contact__c);
              
              if(x.name=='How Long Created' ||x.name=='How Long Updated')
              howlongid.add(x.Contact__c);
              
              if(x.name=='What REITs Created' ||x.name=='What REITs Updated')
              whatreitid.add(x.Contact__c);
                         
            }
        NonlistedreitACC=[select id,Used_Nonlisted_REIT_before_check__c,Used_Nonlisted_REIT_before__c from Contact where id in: Nonlistedreitid];    
            for(Contact act: NonlistedreitACC)
            {
             act.Used_Nonlisted_REIT_before_check__c = TRUE;
            act.Used_Nonlisted_REIT_before__c=null;
             UpdateNonlistedREITs.add(act); 
                       
            }
            helper.run=false;
            if(UpdateNonlistedREITs.size()>0)
            update UpdateNonlistedREITs;
            helper.run=true;
            
         howLongACC=[select id,How_Long__c,How_Long_check__c from Contact where id in: howlongid];   
            for(Contact act: howLongACC)
            {
            act.How_Long_check__c = TRUE;
            act.How_Long__c =null;
            UpdatedHowlongACC.add(act); 
            }
            helper.run=false;
            if(UpdatedHowlongACC.size()>0)
             update UpdatedHowlongACC;
            helper.run=true;
         whatreitACC=[select id,What_REITs_check__c,What_REITs__c from contact where id in: whatreitid];   
            for(Contact act: whatreitACC)
            {
            act.What_REITs_check__c = TRUE;
            act.What_REITs__c  =null;
            UpdatewhatREITs.add(act); 
            }
             helper.run=false;
             if(UpdatewhatREITs.size()>0)
            update UpdatewhatREITs; 
            helper.run=true;
   
         Do you think you would like to invest in our products? section
      
      list<id>wouldyouinvestid=new list<id>();
      list<Contact>wouldyouinvestACC=new list<Contact>();
      list<Contact>UpdatedwouldyouinvestAcc=new list<Contact>();
      
      list<id>whynotid=new list<id>();
      list<Contact>whynotACC=new list<Contact>();
      list<Contact>UpdatedwhynotAcc=new list<Contact>();
      
      
      List<BusinessProfileContactTracking__c>TInteresttoinvest= [select ID, Contact__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from BusinessProfileContactTracking__c where (Name='Would You Invest Updated' OR Name='WHy Not Updated'or name='WHy Not Created'or name='Would You Invest Created') AND  Contact__c in : Expirysixmonths];
            for(BusinessProfileContactTracking__c x: TInteresttoinvest)
            {
                if(x.name=='Would You Invest Created' || x.name=='Would You Invest Updated')
                wouldyouinvestid.add(x.Contact__c);
                
                if(x.name=='WHy Not Created' || x.name=='WHy Not Updated')
                whynotid.add(x.Contact__c);
                
            }
         wouldyouinvestACC =[select id,Would_you_invest_check__c,Would_you_invest__c from Contact where id in: wouldyouinvestid];
            for(Contact accounts : wouldyouinvestACC )
            {
            accounts.Would_you_invest_check__c = TRUE;
            accounts.Would_you_invest__c =null;
            UpdatedwouldyouinvestAcc.add(accounts);  
             }
             helper.run=false;
             if(UpdatedwouldyouinvestAcc.size()>0)
         update UpdatedwouldyouinvestAcc;
         helper.run=true;
      whynotACC =[select id,Why_Not_check__c,Inv_Why_Not__c from Contact where id in: whynotid];
      for(Contact accounts : whynotACC )
            {
            accounts.Why_Not_check__c= TRUE;
            accounts.Inv_Why_Not__c =null;
            accounts.Why_Not_Other__c=null; 
            UpdatedwhynotAcc.add(accounts);  
            
            }
            helper.run=false;
            if(UpdatedwhynotAcc.size()>0)
            update UpdatedwhynotAcc;
            helper.run=true;
  What changes have you made in the volatility of the Products section
 
            list<id>Volatilityproductid=new list<id>();
            list<Contact>VolatilityproducACC=new list<Contact>();
            list<Contact>UpdateVolatility=new list<Contact>();
            
           list<id>Businessprofileid=new list<id>();
            list<Contact>BusinessprofileACC=new list<Contact>();
            list<Contact>UpdateBusinessprofile=new list<Contact>();
            
            
            
            list<id>productProfileid=new list<id>();
            list<Contact>productProfileACC=new list<Contact>();
            list<Contact>UpdateproductProfile=new list<Contact>();
            
            
            list<id>Clientprofileid=new list<id>();
            list<Contact>ClientprofileACC=new list<Contact>();
            list<Contact>UpdatedClientprofile=new list<Contact>();
            
            
            
         List<BusinessProfileContactTracking__c>TvolatilityProduct= [select ID,Contact__c,name,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c from BusinessProfileContactTracking__c where (Name='Changes in Volitality Updated' or name='Changes in Volitality Created') AND  Contact__c in : Expirysixmonths];
            for(BusinessProfileContactTracking__c x: TvolatilityProduct)
            {
             if(x.name=='Changes in Volitality Updated'||x.name=='Changes in Volitality Created') 
               Volatilityproductid.add(x.Contact__c);
               
              if(x.name=='Business Profile Updated'|| x.name=='Business Profile Created')
               Businessprofileid.add(x.Contact__c);
               
               if(x.name=='Product Profile Created'|| x.name=='Product Profile Updated')
               productProfileid.add(x.Contact__c);
               
               
               if(x.name=='Client Profile Created'|| x.name=='Client Profile Updated')
               Clientprofileid.add(x.Contact__c);
                          
               
            }
         VolatilityproducACC =[select id,What_Changes_in_Volitilaty__c,Changes_in_Volitality_check__c from Contact where id in: Volatilityproductid];
            for(Contact accounts : VolatilityproducACC )
            {
            accounts.Changes_in_Volitality_check__c= TRUE;
          accounts.What_Changes_in_Volitilaty__c=null;
            UpdateVolatility.add(accounts);  
             }
          helper.run=false;
          if(UpdateVolatility.size()>0)
         update UpdateVolatility;
         helper.run=true;
         
        BusinessprofileACC=[select id,Business_Profile__c,Business_Profile_check__c from Contact where id in : Businessprofileid];
           for(Contact accounts : BusinessprofileACC)
            {
            accounts.Business_Profile_check__c  = TRUE;
             accounts.Business_Profile__c =null;
            UpdateBusinessprofile.add(accounts);  
            }
          helper.run=false;
          if(UpdateBusinessprofile.size()>0)
         update UpdateBusinessprofile;
        helper.run=true;
         
         productProfileACC=[select id,Product_Profile__c,Product_Profile_check__c from Contact where id in : productProfileid];
         
            for(Contact accounts : productProfileACC)
            {
            accounts.Product_Profile_check__c = TRUE;
          accounts.Product_Profile__c=null;
            UpdateproductProfile.add(accounts);  
            }
          helper.run=false;
         if(UpdateproductProfile.size()>0)
         update UpdateproductProfile;
         helper.run=true;
         
       ClientprofileACC=[select id,Client_Profile__c,Client_Profile_check__c from Contact where id in: Clientprofileid];  
           for(Contact accounts : ClientprofileACC)
            {
            accounts.Client_Profile_check__c = TRUE;
          accounts.Client_Profile__c=null;
            UpdatedClientprofile.add(accounts);  
            }
          helper.run=false;
          if(UpdatedClientprofile.size()>0)
         update UpdatedClientprofile;
         helper.run=true; 
         

     }*/      
  }