global virtual without sharing class ocms_BusinessSumamry extends cms.ContentTemplateController
{
    private SObject ProductList;
    private String html;
    
    public SObject getProductList()
    {
        SObject ProductList;
        User U=[select id,firstname,lastname,email,contactid from User where id=:UserInfo.getUserId()];
        ID uid=U.contactid;
        ProductList= [select id,CCPT_V__c,CCIT__c,CCIT_II__c,Income_NAV__c from Contact WHERE Id=:uid];
        return ProductList;
    }
       
         /* Create Content */
   @TestVisible private String writeControls()
    {
        String html = '';
        return html;
    }
    @TestVisible private String writeListView()
     {
         String html = '';
         
      //  html += '<input type="hidden" id="CCPT-V" value="' + ProductList.get('CCPT_V__c') + '" />';
    //    html += '<input type="hidden" id="CCIT" value="' + ProductList.get('CCIT__c') + '" />';
     //   html += '<input type="hidden" id="CINAV" value="' + ProductList.get('Income_NAV__c') + '" />';
        
        html += '<article class="topMar7 wrapper teer3">'+
        '<h1>MY COLE BUSINESS SUMMARY</h1>'+
        '<p class="clear"><strong>Note:</strong> This summary may not be a complete record of your Cole business. To view your clients accounts and the most current and complete information, please login to DST Vision or contact your Cole team or the Cole Sales Desk at <strong>(866) 341-2653.</strong></p> &nbsp;'+
        '<div class="column-50">&nbsp;'+
        '<table cellpadding="0" cellspacing="0" class="table table-border tabheight40">'+
        '<tbody>'+    
        '<tr>'+
        '<td><strong>Investment Program</strong></td>'+
        '<td><strong>Total</strong></td>'+
        '</tr>'+
        '<tr>'+
        '<td>Cole Credit Property Trust V (CCPT V), Inc.</td>'+
        '<td><p id="CCPT">'+ProductList.get('CCPT_V__c')+'</p></td>'+
        '</tr>'+
        '<tr>'+
        '<td>Cole Office & Industrial REIT (CCIT II), Inc.</td>'+
        '<td><p id="CCIT">'+ProductList.get('CCIT_II__c')+'</p></td>'+
        '</tr>'+
        '<tr>'+
        '<td>Cole Real Estate Income Strategy (Daily NAV), Inc.</td>'+
        '<td><p id="CINAV">'+ProductList.get('Income_NAV__c')+'</p></td>'+
        '</tr>'+
        '</tbody>'+
        '</table>'+
        '</div>'+
        '<div class="column-50">'+
        '<div id="canvas-holder"><canvas height="300" id="chart-area" style="margin-left:40px;" width="300"> </canvas></div>'+
        '</div>'+
        '</article>';
        
         return html;
        }
        
      @TestVisible private String writeMapView(){
         return '';
        }
        
        
        
        global override virtual String getHTML()
        {
            String html = '';
            
            html += writeControls();
            ProductList= getProductList();
            html += writeListView();
            html += '<script src="/resource/ocms_WebPropertyList/js/ocms_businesssummary.JS" type="text/javascript"></script>';
            return html;
        }

}