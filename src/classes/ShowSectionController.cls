public with sharing class ShowSectionController
{
    public Event_Automation_Request__c eve{get;set;}
    public Event_Automation_Request__c eve1{get;set;}
    Apexpages.StandardController controller;
    String eventId{get;set;}
    public boolean flag{get;set;}
    public boolean flag1{get;set;}
    public Event_Automation_Request__c objCase=null;
    public ShowSectionController(ApexPages.StandardController controller) {
        System.debug('-------------->');  
        this.controller = controller;
        Event_Automation_Request__c c = (Event_Automation_Request__c)controller.getRecord();      
        eve = new Event_Automation_Request__c(); 
        objCase = c;       
        flag = false;
        flag1 = false;
        eventId = ApexPages.CurrentPage().getParameters().get('id');
        System.debug('------ID-------->'+eventId); 
        if(eventId != NULL){
         eve1 = [select id,Rep_Name_FBO__r.name,Rep_Name_FBO__c,BD_Form__c,Agenda__c,Copy_of_invitation__c,Attendee_list__c,Copy_of_invoice_or_receipt__c,Mailing_Address__c,Rep_Name__c,Rep_DBA__c,Payment_Method__c,name from Event_Automation_Request__c where id=:eventId];
         system.debug('Check the record..'+eve1.Payment_Method__c);
         if(eve1.Rep_Name_FBO__c!=null) 
          eve1.Rep_Name__c=eve1.Rep_Name_FBO__r.name;
         System.debug('***' +eve1);
         if(eve1.Payment_Method__c != null){
            c.Payment_Method__c = eve1.Payment_Method__c;
         }
        System.debug('------ID1-------->'+eve1.Rep_Name__c + '------ID2-------->' + eve1.Rep_DBA__c);     
        }
        if(objCase.Payment_Method__c== 'Direct Pay to Vendor (Concur Reimbursement)')  {
            flag = True;
            flag1 = false;
        }
       if(objCase.Payment_Method__c== 'Back Office BD Reimbursement'){
            flag1 = True;
            flag = false;
            }
       if((objCase.Payment_Method__c != 'Direct Pay to Vendor (Concur Reimbursement)') && (objCase.Payment_Method__c != 'Back Office BD Reimbursement'))
       {
            flag = False;
            flag1 = false;
       }
        //flag1 = false;
    }
    
   /*public ShowSectionController()
    {
        eventId = ApexPages.CurrentPage().getParameters().get('id');
        System.debug('------ID-------->'+eventId); 
        if(eventId != NULL){
         eve1 = [select id, Mailing_Address__c,Rep_Name__c,Rep_DBA__c,Payment_Method__c,name from Event_Automation_Request__c where id=:eventId];
         System.debug('***' +eve1);
        System.debug('------ID1-------->'+eventId);     
        }
        //flag1 = false;
    }*/
    
    public void hideSectionOnChange()
    {
       if(objCase.Payment_Method__c== 'Direct Pay to Vendor (Concur Reimbursement)')  {
            flag = True;
            flag1 = false;
        }
       if(objCase.Payment_Method__c== 'Back Office BD Reimbursement'){
            flag1 = True;
            flag = false;
            }
       if((objCase.Payment_Method__c != 'Direct Pay to Vendor (Concur Reimbursement)') && (objCase.Payment_Method__c != 'Back Office BD Reimbursement'))
       {
            flag = False;
            flag1 = false;
       }
    }
    
    public pagereference saveChanges() {
    
     
    try{
        System.debug('Welcome to save method');
        Controller.save();
        if (Schema.sObjectType.Event_Automation_Request__c.isUpdateable()) 
        update eve1;
        if (Schema.sObjectType.Event_Automation_Request__c.isUpdateable()) 
        update objCase;
        System.debug('???' +eve1);
        
        PageReference pageRef1 = new PageReference('/apex/BDPicklistChangeOutput');
        return pageRef1;
    }catch(Exception e){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter values in all fields');
        ApexPages.addMessage(myMsg);    
    }
    return null;    
    }
 
     public pagereference Edit1() { 
      PageReference pageRef = new PageReference('/apex/BDPicklistChange');
     return pageRef;
 }
    
     
}