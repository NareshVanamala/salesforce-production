@istest(seeAllData=true)
public class Test_comproductsforAccount 
{
    public static testmethod void comprotest11(){
    id strRecordTypeId =[Select id,name,DeveloperName from RecordType where DeveloperName='NonInvestorRepContact' and SobjectType = 'contact'].id;
    string s;
    string competitor='ARC';
    string competitor1='ARC';
    string color='white';
    string abc='ARC New York Recovery REIT;ARCT V;';
    string ac='ARC New York Recovery REIT;ARCT V;';
    string j='';
   //string pr = 'Apple REIT Seven - CLOSED, +  ,Apple REIT Seven - CLOSED';
    Competitor_code__c  compsetting=Competitor_code__c.getvalues(competitor);
    if(compsetting == null)
    {             
          compsetting = new Competitor_code__c(Name= 'CustomValues');
          compsetting.Competitor_Name__c ='Apple';
          compsetting.Product__c='Apple REIT Seven - CLOSED';
          insert compsetting;
    }
    
       Account a = new Account();
        a.name = 'Test';
        a.X1031_Selling_Agreement__c = 'CCIT II';
        insert a;
        a.X1031_Selling_Agreement__c = 'CCIT I';
        update a;
       
       Competitor_Product__c cop = new Competitor_Product__c();
        cop.Account_Name__c = a.id;
        cop.Competitors__c = 'Apple';
        cop.Product__c = 'Apple REIT Seven - CLOSED';
        cop.Selected_Producs__c = 'Apple REIT Seven - CLOSED';  
        insert cop;
        
       system.assertequals(cop.Account_Name__c,a.id);

         
        Competitor_Product__c cop1 = new Competitor_Product__c();
        cop1.Account_Name__c = a.id;
        cop1.Competitors__c = 'AmReit';
        cop1.Product__c = 'ARC Daily Net Asset Value';
        cop1.Selected_Producs__c = 'Apple REIT Seven - CLOSED';      
        insert cop1;
        
        Competitor_Product__c cop2 = new Competitor_Product__c();
        cop2.Account_Name__c = a.id;
        cop2.Competitors__c = 'ARC';
        cop2.Product__c = 'BH Multifamily REIT I-CLOSED';
        cop2.Selected_Producs__c = 'Apple REIT Seven - CLOSED';  
        insert cop2;
        
    
    Apexpages.standardcontroller sc;
    comproductsforAccount  com = new comproductsforAccount(sc);
    ApexPages.CurrentPage().getParameters().put('id',a.id);
     ApexPages.currentPage().getParameters().put('conrec', a.Id);
      ApexPages.currentPage().getParameters().put('Competitor','Apple');
       ApexPages.currentPage().getParameters().put('Productval1','Apple REIT Seven - CLOSED');   
       ApexPages.currentPage().getParameters().put('edt', cop.Id);
        
    com.call();
   try{
   list<string> a123=new list<string>{'AmReit','AmReit'};
    com.Competitor=a123;
    com.cmpt=cop;
    com.id1=cop.id;
    com.editvalue=cop.id;
    com.savechanges();
    }
    catch(Exception e){}
    com.Cancelupdate();
    com.addtotable();
    com.id1=cop.id;
    ApexPages.currentPage().getParameters().put('edt', cop.Id);
    com.editrow();
    com.productval1.add('s1');
    com.id1=cop.id;
    com.editvalue=cop.id;
    ApexPages.currentPage().getParameters().put('edt', cop.Id);
    com.UpdateRow();
    com.id1=cop.id;
    com.editvalue=cop.id;
    ApexPages.currentPage().getParameters().put('edt', cop.Id);
    com.deleteproduct();
    com.Cancelchoice();
    com.updateProduct();
    com.getquoteId1();
    com.setquoteId1(s);
    com.id1=cop.id;
     ApexPages.currentPage().getParameters().put('dlt', cop.Id);
    com.deleterow();
    }
public static testmethod void comprotest12(){
 id strRecordTypeId =[Select id,name,DeveloperName from RecordType where DeveloperName='NonInvestorRepContact' and SobjectType = 'contact'].id;
    string s;
    string competitor;
        Account a = new Account();
        a.name = 'Test';
        a.X1031_Selling_Agreement__c = 'CCIT II';
        insert a;
        a.X1031_Selling_Agreement__c = 'CCIT I';
        update a;
        
               system.assertequals(a.name,'Test');

comproductsforAccount  com1 = new comproductsforAccount();
com1.call();
ApexPages.currentPage().getParameters().put('conrec', '');
ApexPages.currentPage().getParameters().put('conrec', a.Id);
}


}