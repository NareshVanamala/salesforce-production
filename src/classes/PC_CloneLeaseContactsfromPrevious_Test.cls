@isTest(seeAlldata =true)

private class PC_CloneLeaseContactsfromPrevious_Test{

    private static testMethod void doTest() {

        Test.startTest();
        
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
        Account acc = new Account (
        Name = 'newAcc1'
        );  
        insert acc;
        Contact con = new Contact (
        AccountId = acc.id,
        LastName = 'portalTestUser'
        );
        insert con;
        Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
         
         
        User newUser = new User(
        profileId = p.id,
        username = 'newUser0615201720511@arcpreit.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = con.id
        );
        insert newUser;
        
        MRI_Property__c mrpObj = new MRI_Property__c();
        mrpObj.Name = 'testmrp';
        mrpObj.Property_Manager__c = newUser.id;
        mrpObj.Fund1__c = 'ARCP';
        mrpObj.Date_Acquired__c = Date.today();
        mrpObj.Common_Name__c = 'cname';
        mrpObj.Property_Id__c = '1234';
        insert mrpObj;
        
         MRI_Property__c mrpObj1 = new MRI_Property__c();
        mrpObj1 .Name = 'testmrp1';
        mrpObj1 .Property_Manager__c = newUser.id;
        mrpObj1 .Fund1__c = 'ARCP';
        mrpObj1 .Date_Acquired__c = Date.today();
        mrpObj1 .Common_Name__c = 'cname1';
        mrpObj1 .Property_Id__c = '12345';
        insert mrpObj1 ;
        
        list<Lease__c> leaseLst = new list<Lease__c>();
        
        Lease__c lObj1 = new Lease__c();
        lObj1.MRI_Property__c = mrpObj.Id;
        lObj1.Tenant_Name__c = 'testTenent1';
        lObj1.Lease_Status__c = 'Active';
        lObj1.MasterOccupant_ID__c ='M001';
        insert lObj1;
        
        Lease__c lObj2 = new Lease__c();
        lObj2.MRI_Property__c = mrpObj.Id;
        lObj2.Tenant_Name__c = 'testTenent1';
        lObj2.Lease_Status__c = 'Active';
        lObj2.MasterOccupant_ID__c ='M001';
        lObj2.Previous_Lease__c = lObj1.id;
        insert lObj2;

        Lease__c lObj3 = new Lease__c();
        lObj3.MRI_Property__c = mrpObj.Id;
        lObj3.Tenant_Name__c = 'testTenent1';
        lObj3.Lease_Status__c = 'Active';
        lObj3.MasterOccupant_ID__c  ='M003';
        insert lObj3;

        
        Property_Contacts__c pcObj1 = new Property_Contacts__c();
        pcObj1.Name = 'test1';
        pcObj1.MRI_Property__c = mrpObj.Id;
        insert pcObj1;
        
        Property_Contacts__c pcObj2 = new Property_Contacts__c();
        pcObj2.Name = 'test2';
        pcObj2.MRI_Property__c = mrpObj.Id;
        insert pcObj2;
        
        Property_Contact_Leases__c pclObj1 = new Property_Contact_Leases__c(Lease__c = lObj1.Id);
        pclObj1.Property_Contacts__c = pcObj1.Id;
        pclObj1.Common_Name__c ='walmart';
        pclObj1.External_ID__c = 'E001';
        pclObj1.Local_tenant_Name__c   = 'Test Tenant name';
        pclObj1.Property_ID__c ='P001';
        pclObj1.Contact_Type__c = 'Developer';
        insert pclObj1;
        
        Property_Contact_Leases__c pclObj2 = new Property_Contact_Leases__c(Lease__c = lObj2 .Id);
        pclObj2.Property_Contacts__c = pcObj2.Id;
        pclObj2.Contact_Type__c = 'Developer';
        insert pclObj2;
         list<string> plseid = new  list<string>();
         plseid.add(String.valueOf(lObj2.id));
         list<string> lseid = new  list<string>();
         lseid = plseid;

       PC_CloneLeaseContactsfromPreviousLease classObj = new PC_CloneLeaseContactsfromPreviousLease();
      
       PC_CloneLeaseContactsfromPreviousLease.CloneLeaseContacts(lseid);
        Test.stopTest();

       
    }
}