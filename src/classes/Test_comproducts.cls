@istest(seeAllData=true)
public class Test_comproducts
{
    public static testmethod void comprotest(){
    id strRecordTypeId =[Select id,name,DeveloperName from RecordType where DeveloperName='NonInvestorRepContact' and SobjectType = 'contact'].id;
    string s;
    string competitor;
   //string pr = 'Apple REIT Seven - CLOSED, +  ,Apple REIT Seven - CLOSED';
    Competitor_code__c  compsetting=Competitor_code__c.getvalues(competitor);
    if(compsetting == null)
    {             
          compsetting = new Competitor_code__c(Name= 'CustomValues');
          compsetting.Competitor_Name__c ='Apple';
          compsetting.Product__c='Apple REIT Seven - CLOSED';
          insert compsetting;
    }
    
       Account a = new Account();
        a.name = 'Test';
        a.X1031_Selling_Agreement__c = 'CCIT II';
        insert a;
       
        Contact c1 = new Contact();
        c1.lastname = 'Cockpit';
        c1.Accountid = a.id;
        c1.Relationship__c ='Accounting';
        c1.Contact_Type__c = 'Cole';
        c1.Wholesaler_Management__c = 'Producer A';
        c1.Territory__c ='Chicago';
        c1.RIA_Territory__c ='Southeast Region';
        c1.RIA_Consultant_Picklist__c ='Brian Mackin';
        c1.Internal_Consultant_Picklist__c ='Ben Satterfield';
        c1.Wholesaler__c='Andrew Garten';
        c1.Regional_Territory_Manager__c ='Andrew Garten';
        c1.Internal_Wholesaler__c ='Aaron Williams';
        c1.Territory_Zone__c = 'NV-50';
        c1.Next_Planned_External_Appointment__c = system.now();
        c1.recordtypeid = strRecordTypeId;
        insert c1;
        c1.Territory_Zone__c = 'NV-51';
        update c1;
    
         Competitor_Product__c cop = new Competitor_Product__c();
        cop.Contact_Name__c = c1.id;
        cop.Competitors__c = 'Apple';
        cop.Product__c = 'Apple REIT Seven - CLOSED';
        cop.Selected_Producs__c = 'Apple REIT Seven - CLOSED'; 
        insert cop;
        
        system.assertequals(cop.Contact_Name__c,c1.id);

         
        Competitor_Product__c cop1 = new Competitor_Product__c();
        cop1.Contact_Name__c = c1.id;
        cop1.Competitors__c = 'AmReit';
        cop1.Product__c = 'ARC Daily Net Asset Value';
        cop1.Selected_Producs__c = 'Apple REIT Seven - CLOSED';      
        insert cop1;
        
                system.assertequals(cop1.Contact_Name__c,c1.id);

        Competitor_Product__c cop2 = new Competitor_Product__c();
        cop2.Contact_Name__c = c1.id;
        cop2.Competitors__c = 'ARC';
        cop2.Product__c = 'BH Multifamily REIT I-CLOSED';
        cop2.Selected_Producs__c = 'Apple REIT Seven - CLOSED'; 
        insert cop2;
        
        system.assertequals(cop2.Contact_Name__c,c1.id);


    Apexpages.standardcontroller sc;
    comproducts  com = new comproducts(sc);
    ApexPages.CurrentPage().getParameters().put('id',c1.id);
     ApexPages.currentPage().getParameters().put('conrec', c1.Id);
      ApexPages.currentPage().getParameters().put('Competitor','Apple');
       ApexPages.currentPage().getParameters().put('Productval1','Apple REIT Seven - CLOSED');   
       ApexPages.currentPage().getParameters().put('edt', cop.Id);
        
    com.call();
   try{
   list<string> a123=new list<string>{'AmReit','AmReit'};
   com.Competitor=a123;
   list<selectoption> abc=new list<selectoption>();
   abc.add(new SelectOption('AmReit','AmReit'));
   abc.add(new SelectOption('AmReit1','AmReit1')); 
   com.comp1=abc;
   com.comp=abc;
    com.cmpt=cop;
    com.id1=cop.id;
    com.editvalue=cop.id;
    com.savechanges();}
    catch(Exception e){}
    com.Cancelupdate();
    com.addtotable();
    com.editrow();
    com.productval1.add('s1');
    com.UpdateRow();
    com.Cancelchoice();
    com.updateProduct();
    com.getquoteId1();
    com.setquoteId1(s);
    com.id1=cop.id;
     ApexPages.currentPage().getParameters().put('dlt', cop.Id);
    com.deleterow();
    }
public static testmethod void comprotest1(){
 id strRecordTypeId =[Select id,name,DeveloperName from RecordType where DeveloperName='NonInvestorRepContact' and SobjectType = 'contact'].id;
    string s;
    string competitor;
        Account a = new Account();
        a.name = 'Test';
        a.X1031_Selling_Agreement__c = 'CCIT II';
        insert a;
       
        Contact c1 = new Contact();
        c1.lastname = 'Cockpit';
        c1.Accountid = a.id;
        c1.Relationship__c ='Accounting';
        c1.Contact_Type__c = 'Cole';
        c1.Wholesaler_Management__c = 'Producer A';
        c1.Territory__c ='Chicago';
        c1.RIA_Territory__c ='Southeast Region';
        c1.RIA_Consultant_Picklist__c ='Brian Mackin';
        c1.Internal_Consultant_Picklist__c ='Ben Satterfield';
        c1.Wholesaler__c='Andrew Garten';
        c1.Regional_Territory_Manager__c ='Andrew Garten';
        c1.Internal_Wholesaler__c ='Aaron Williams';
        c1.Territory_Zone__c = 'NV-50';
        c1.Next_Planned_External_Appointment__c = system.now();
        c1.recordtypeid = strRecordTypeId;
        insert c1;
        c1.Territory_Zone__c = 'NV-51';
        update c1;
        
        system.assertequals(c1.Accountid,a.id);

        
comproducts  com1 = new comproducts();
com1.call();
ApexPages.currentPage().getParameters().put('conrec', '');

}


}