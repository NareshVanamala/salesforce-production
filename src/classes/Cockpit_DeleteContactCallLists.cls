global with sharing class Cockpit_DeleteContactCallLists implements Database.Batchable<SObject>{
   global final String Query;  
   global Automated_call_list__c locked ;
   global Cockpit_DeleteContactCallLists (String automatedCallListName){  
      Automated_call_list__c acl = [Select Id, Name from Automated_call_list__c where Name =:automatedCallListName];
      locked =  [Select Id, Name, Templock__c from Automated_Call_List__c where Id = :acl.ID ];
      locked.Templock__c = 'General Lock';
      if(Schema.sObjectType.Automated_Call_List__c.isUpdateable())
         update locked;
      system.debug('update locked'+locked);
      Query = 'Select Id from contact_call_list__c Where Call_Complete__c = false and Automated_Call_List__c=\''+acl.ID+'\'';
   }
   global Database.QueryLocator start(Database.BatchableContext BC)  {  
      system.debug('This is start method');
      system.debug('Myquery is......'+Query);
      return Database.getQueryLocator(query);  
   }  

   global void execute(Database.BatchableContext BC,List<contact_call_list__c> scope){  
      List<contact_call_list__c> callist = new List<contact_call_list__c>();
      for(contact_call_list__c clsss : scope){
          callist.add(clsss);
      }
      try{
         if(contact_call_list__c.sObjectType.getDescribe().isDeletable())
            delete callist;
      }
      catch(system.dmlexception e){
         System.debug('ccl not deleted: ' + e);
      }
   }

   global void finish(Database.BatchableContext BC){ 
      ID ACLID = locked.Id;
      Cockpit_InsertContactCallLists ICCL = new Cockpit_InsertContactCallLists(ACLID);
      Id batchId = Database.executeBatch(ICCL);
   }
}