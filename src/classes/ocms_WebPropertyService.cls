global with sharing class ocms_WebPropertyService implements cms.ServiceInterface {

    public String JSONResponse {get; set;}
    private String action {get; set;}
    private Map<String, String> parameters {get; set;}
    private ocms_WebPropertyList wpl = new ocms_WebPropertyList();

    global ocms_WebPropertyService() {
        
    }

    global System.Type getType(){
        return ocms_WebPropertyService.class;
    }

    global String executeRequest(Map<String, String> p) {
        this.action = p.get('action');
        this.parameters = p; 
        this.LoadResponse();    
        return this.JSONResponse;
    }

    global void loadResponse(){
        if (this.action == 'getPortfolioList'){
            this.getPortfolioList();
        } else if (this.action == 'getIndustryList'){
            this.getIndustryList(parameters.get('portfolio'));
        } else if (this.action == 'getTenantList'){
            this.getTenantList(parameters.get('portfolio'));
        } else if (this.action == 'getStateList'){
            this.getStateList(parameters.get('portfolio'));
        } else if (this.action == 'getCityList'){
            this.getCityList(parameters.get('state'),parameters.get('portfolio'));
        } else if (this.action == 'getResultTotal'){

            String portfolio = parameters.get('portfolio');
            String state = parameters.get('state');
            String city = parameters.get('city');
            String tenantIndustry = parameters.get('tenantIndustry');
            String tenant = parameters.get('tenant');
            String[] filters = new List<String>();

            if(parameters.get('filterRetail') != null && parameters.get('filterRetail') != ''){
                filters.add(parameters.get('filterRetail'));
            }
            if(parameters.get('filterMultiTenant') != null && parameters.get('filterMultiTenant') != ''){
                filters.add(parameters.get('filterMultiTenant'));
            }
            if(parameters.get('filterOffice') != null && parameters.get('filterOffice') != ''){
                filters.add(parameters.get('filterOffice'));
            }
            if(parameters.get('filterIndustrial') != null && parameters.get('filterIndustrial') != ''){
                filters.add(parameters.get('filterIndustrial'));
            }

            this.getResultTotal(portfolio, state, city, tenantIndustry, tenant, filters);

        } else if (this.action == 'getPropertyList'){

            String portfolio = parameters.get('portfolio');
            String state = parameters.get('state');
            String city = parameters.get('city');
            String tenantIndustry = parameters.get('tenantIndustry');
            String tenant = parameters.get('tenant');
            String[] filters = new List<String>();
            String sortBy = parameters.get('sortBy');
            String rLimit = parameters.get('limit');
            String offset = parameters.get('offset');

            if(parameters.get('filterRetail') != null && parameters.get('filterRetail') != ''){
                filters.add(parameters.get('filterRetail'));
            }
            if(parameters.get('filterMultiTenant') != null && parameters.get('filterMultiTenant') != ''){
                filters.add(parameters.get('filterMultiTenant'));
            }
            if(parameters.get('filterOffice') != null && parameters.get('filterOffice') != ''){
                filters.add(parameters.get('filterOffice'));
            }
            if(parameters.get('filterIndustrial') != null && parameters.get('filterIndustrial') != ''){
                filters.add(parameters.get('filterIndustrial'));
            }

            this.getPropertyList(portfolio, state, city, tenantIndustry, tenant, filters, sortBy, rLimit, offset);
            
        }
    }

    private void getResultTotal(String portfolio, String state, String city, String tenantIndustry, String tenant, String[] filters){
        this.JSONResponse = JSON.serialize(wpl.getResultTotal(portfolio, state, city, tenantIndustry, tenant, filters));
    }

    private void getPropertyList(String portfolio, String state, String city, String tenantIndustry, String tenant, String[] filters, String sortBy, String rLimit, String offset){
        this.JSONResponse = JSON.serialize(wpl.getPropertyList(portfolio, state, city, tenantIndustry, tenant, filters, sortBy, rLimit, offset));
    }

    private void getPortfolioList(){
        this.JSONResponse = JSON.serialize(wpl.getPortfolioList());
    }

    private void getIndustryList(string portfolio){
        this.JSONResponse = JSON.serialize(wpl.getIndustryList(portfolio));
    }

    private void getTenantList(string portfolio){
        this.JSONResponse = JSON.serialize(wpl.getTenantList(portfolio));
    }

    private void getStateList(string portfolio){
        this.JSONResponse = wpl.getStateList(portfolio);
    }

    private void getCityList(String state,string portfolio){
        this.JSONResponse = JSON.serialize(wpl.getCityList(state,portfolio));
    }

}