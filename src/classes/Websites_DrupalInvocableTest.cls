@isTest(SeeAllData=true)
public class Websites_DrupalInvocableTest{
    
    static Integer s_num = 1;

    public static String getFakeId(Schema.SObjectType sot){
        String result = String.valueOf(s_num++);
        return sot.getDescribe().getKeyPrefix() + '0'.repeat(12-result.length()) + result;
   }
    //Websites_DrupalInvocableOnUser.cls

    @isTest
    static void invokeUserTest(){
        
        List<Id> userIds= new List<Id>();
        userIds.add(getFakeId(User.SobjectType));

        Test.startTest();

        String body= '{"status": "SUCCESS"}';
    
        Test.setMock(HttpCalloutMock.class, new Websites_HttpClass.Mock(body, 200));
        Websites_DrupalInvocableOnUser.invokeUserTest(userIds);

        body= '{"status": "FAIL"}';
        Test.setMock(HttpCalloutMock.class, new Websites_HttpClass.Mock(body, 302));
        Websites_DrupalInvocableOnUser.invokeUserTest(userIds);

        Test.stopTest();
    }

    //Websites_DrupalInvocableOnContact.cls
    @isTest
    static void invokeContactTest(){
        
        List<Id> userIds= new List<Id>();
        userIds.add(getFakeId(Contact.SobjectType));

        Test.startTest();

        String body= '{"status": "SUCCESS"}';
        Test.setMock(HttpCalloutMock.class, new Websites_HttpClass.Mock('{"status": "SUCCESS"}', 200));
        
        Websites_DrupalInvocableOnContact.invokeContactTest(userIds);

        body= '{"status": "FAIL"}';
        Test.setMock(HttpCalloutMock.class, new Websites_HttpClass.Mock(body, 302));
        Websites_DrupalInvocableOnContact.invokeContactTest(userIds);

        Test.stopTest();
    }

    //Websites_DrupalInvocableOnAccount.cls
    @isTest
    static void invokeAccountContactsTest(){
        
        List<Id> accIds= new List<Id>();
        accIds.add(getFakeId(Account.SobjectType));

        Test.startTest();

        String body= '{"status": "SUCCESS"}';
    
        Test.setMock(HttpCalloutMock.class, new Websites_HttpClass.Mock(body, 200));
        Websites_DrupalInvocableOnAccount.invokeAccountContactsTest(accIds);

        body= '{"status": "FAIL"}';
        Test.setMock(HttpCalloutMock.class, new Websites_HttpClass.Mock(body, 302));
        Websites_DrupalInvocableOnAccount.invokeAccountContactsTest(accIds);

        Test.stopTest();
    }

}