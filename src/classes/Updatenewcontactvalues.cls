global with sharing class Updatenewcontactvalues implements Database.Batchable<SObject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String Query;
        String todaydate = String.valueOf(system.today());
        if(Test.isRunningTest()){
            //system.debug('count of existing records is......' + [SELECT count() from Contact]);
            Query = 'SELECT Id,Effective_Snooze_Until_Date__c,Snooze_Period__c  FROM  Contact Where Effective_Snooze_Until_Date__c <'+ todaydate +'Limit 1';
                    
            system.debug('Myquery is......' + Query);
            return Database.getQueryLocator(Query);         
        }
        else{
            //system.debug('count of existing records is......' + [SELECT count() from Contact]);
            Query = 'SELECT Id,Effective_Snooze_Until_Date__c,Snooze_Period__c  FROM  Contact Where Effective_Snooze_Until_Date__c <'+ todaydate;
                    
            system.debug('Myquery is......' + Query);
            return Database.getQueryLocator(Query); 
        }
    }
    global void execute(Database.BatchableContext BC, List<Contact> scope){
    
        List<Contact> Conlist= new List<Contact>();
        system.debug('$$$$$$');
        for(Contact nah:scope){
        
            nah.Effective_Snooze_Until_Date__c= null;
            nah.Snooze_Period__c= null;
            system.debug('$$$$$$'+nah);
            Conlist.add(nah);
        }
        
        if(!Conlist.isEmpty()){
        
        if (Schema.sObjectType.Contact.isUpdateable()) 
        
            update Conlist;
        }
    
    }
    global void finish(Database.BatchableContext BC){   

    }
}