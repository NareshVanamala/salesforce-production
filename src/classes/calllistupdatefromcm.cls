global class calllistupdatefromcm implements Database.Batchable<SObject>{

  
/*global String Query;  
global CallList__c call;
global List<User> uslist;
date assigneddate;
global List<Contact_Call_List__c> cclist;
global Automated_call_list__c locked ;
global map<id, date>MapOFUserIDTODATe;
global Map<Integer, String> filterMap = new Map<Integer, String>();
global calllistupdatefromcm (Set<Id> clIds, Set<Id> owners,map<id,date>mapodIdToDate)  
{  
uslist =  [ Select Id, Name , Territory__c from User where ID in:owners];
MapOFUserIDTODATe =mapodIdToDate.clone();
 System.debug('userlist'+uslist);
  system.debug('Welcome to Batch apex method...');
  system.debug('Check the map.....'+ mapodIdToDate);
 List<calllist__c> callistname = [Select Id, Name From calllist__c Where Id in: clIds];
//Query= 'select id,Name,CampaignName__c,CampaignName__r.id,Contact_Type__c,Field_API__c, Field_API1__c, Field_API2__c,Field_API3__c,Field_API4__c,Operator__c,Operator1__c,Operator2__c,Operator3__c,Operator4__c,Value__c,Value1__c,Value2__c,Value3__c,Value4__c from  CallList__c';  
 //CallList__c call ; 
 
call=[select id,Name,CampaignName__c, CampaignName__r.id,Contact_Type__c,Custom_Logic__c,Field_API__c, Field_API1__c, Field_API2__c,Field_API3__c,Field_API4__c,Operator__c,Operator1__c,Operator2__c,Operator3__c,Operator4__c,Value__c,Value1__c,Value2__c,Value3__c,Value4__c from  CallList__c  where id=:callistname[0].id ];
Automated_call_list__c acl = [Select Id, Name from Automated_call_list__c where Name =:callistname[0].Name];
cclist = [Select Id ,Contact__c from contact_call_list__c Where Call_Complete__c = true and CallListOrder__c =: acl.id];
locked =  [Select Id, Name, Templock__c from Automated_Call_List__c where Id = :acl.ID ];
}  */
global Database.QueryLocator start(Database.BatchableContext BC)  
{  

  /*try
  {
    filterMap.clear();
    String condn1=null;
    String condn2=null;
    String condn3=null;
    String condn4=null;
    String condn5=null;
    string conditions =null;
    string cond1;
    list<Contact>conlist= new list<Contact>();  
    list<Contact_Call_List__c >contactcalllist=new list<Contact_Call_List__c >();  
    String conquery = 'select Id,ContactId,CampaignId,Contact.Territory__c from CampaignMember';
        System.debug('*******:QUERY:********\n'+conquery);
   
        GerateDynamicQueryforcm generateQuery =  new GerateDynamicQueryforcm();
        if(call.Field_API__c!=null && call.Field_API__c!='' && call.Operator__c!='' && call.Operator__c!=null)
        condn1= generateQuery.conditionquery(call.Field_API__c,call.Operator__c,call.Value__c);
        if(call.Field_API1__c!=null && call.Field_API1__c!='' && call.Operator1__c!='' && call.Operator1__c!=null)
        condn2= generateQuery.conditionquery(call.Field_API1__c,call.Operator1__c,call.Value1__c);
        if(call.Field_API2__c!=null && call.Field_API2__c!='' && call.Operator2__c!='' && call.Operator2__c!=null)
        condn3= generateQuery.conditionquery(call.Field_API2__c,call.Operator2__c,call.Value2__c);
        if(call.Field_API3__c!=null && call.Field_API3__c!='' && call.Operator3__c!='' && call.Operator3__c!=null)
        condn4= generateQuery.conditionquery(call.Field_API3__c,call.Operator3__c,call.Value3__c);
        if(call.Field_API4__c!=null && call.Field_API4__c!='' && call.Operator4__c!='' && call.Operator4__c!=null)
        condn5= generateQuery.conditionquery(call.Field_API4__c,call.Operator4__c,call.Value4__c);   
        If(condn1 != null  && call.Field_API__c != null)
          filterMap.put(1,condn1);
        if( condn2 != null && call.Field_API1__c != null)
            filterMap.put(2,condn2);
        if( condn3 != null && call.Field_API2__c != null)
            filterMap.put(3,condn3);
        if(condn4 != null && call.Field_API3__c != null)
            filterMap.put(4,condn4);
        if(condn5 != null && call.Field_API4__c != null)
            filterMap.put(5,condn5);           
        if(String.isNotBlank(call.Custom_Logic__c)){
            conditions = call.Custom_Logic__c;
            for(Integer i = 1; i <= filterMap.size(); i++){
                if(conditions.contains(String.valueOf(i))){
                    conditions = conditions.replace(String.valueOf(i),filterMap.get(i));
                }
            }
            conquery += ' WHERE (' + conditions + ') ';
        }else{
            If(condn1 != null  && call.Field_API__c != null)  
                conquery=conquery+' where '+condn1;
            if( condn2 != null && call.Field_API1__c != null)
                conquery=conquery+' AND '+ condn2;
            if( condn3 != null && call.Field_API2__c != null)
                conquery=conquery+' AND '+condn3;
            if(condn4 != null && call.Field_API3__c != null)
                conquery=conquery+' AND '+condn4;
            if(condn5 != null && call.Field_API4__c != null)
                conquery=conquery+' AND '+condn5; 
        }         

        if(condn1 != null  && call.Field_API__c != null)
          conquery=conquery+' AND ' + 'CampaignId =' + '\''+call.CampaignName__c+'\'';
        else
          conquery=conquery+' Where ' + 'CampaignId =' + '\''+call.CampaignName__c+'\'';
          
          //Edit Issue 
          List<String> conids= new List<String>();
            String str = '';
            String glue = '(';
            for(integer i=0;i<cclist.size();i++){
                  conids.add('\''+cclist[i].Contact__c+'\'');
                  str = str + glue + '\''+cclist[i].Contact__c+'\'';
                  glue = ',';
            }
            if(cclist.size()>0){
                str += ')';
                if(condn1 != null  && call.Field_API__c != null)
                     conquery= conquery+' AND '+ 'Contactid Not in '+ str;
                   else
                     conquery= conquery+' AND '+ 'Contactid Not in'+ str;
                }   
            
            
            Query= conquery;
                    
                 
        System.debug('*******:QUERY:********\n'+Query);
        System.debug('++++++++++++++++Check the Map, are you here, be with me always....'+ MapOFUserIDTODATe);
        system.debug('This is start method');
        system.debug('Myquery is......'+Query);
        return Database.getQueryLocator(query); 
      }
      
  catch(Exception e)
{
 query='Select Id from Contact limit 0'; 
 locked.Templock__c = '';
 system.debug('Check the Exception.....'+e);
 Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
 String[] toAddresses = new String[] {'nvanamala@colecapital.com'};
 //String[] bccAddresses = new String[] {'priya.skrish@gmail.com','skalamkar@colecapital.com'};
 mail.setToAddresses(toAddresses);
// mail.setBccSender(true);
 //mail.setBccAddresses(bccAddresses);
 mail.setSubject('Exception Message');
 mail.setPlainTextBody
 ('CallList ' + call.Name + ' ' +
  'has not been edited because of .' + e.getMessage());
 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
 
  return Database.getQueryLocator(query);
    
    
    
    }*/
    return null;
         
}  

global void execute(Database.BatchableContext BC,List<CampaignMember> scope)  
{  
 /*system.debug('Check the name of the call list name:'+call);
List<contact_call_list__c> callist = new List<contact_call_list__c>();

for(CampaignMember c : scope) {
contact_call_list__c cls = new contact_call_list__c();
cls.CallList_Name__c = call.name ;
cls.Contact__c = c.ContactId;
cls.campId__c = call.CampaignName__c;
for(User u : uslist)
 {
 
    if(u.Territory__c != null)
    {
    if(u.Territory__c == c.Contact.Territory__c)
    {
      assigneddate = MapOFUserIDTODATe.get(u.id);
      cls.owner__c = u.Id;
      cls.Assigned_Date__c= assigneddate;
      System.debug('User Id aggregation'+ u.Id);
    }
    }
 }
     
callist.add(cls);

}
try {
           insert callist;
        } catch (system.dmlexception e) {
            System.debug('ccl not inserted: ' + e);
        }*/
}


global void finish(Database.BatchableContext BC)  
{ 
/*AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
 locked.Templock__c = '';
 update locked;
 
// Send an email to the Apex job's submitter notifying of job completion.  

Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
String[] toAddresses = new String[] {a.CreatedBy.Email};
String[] bccAddresses = new String[] {'nvanamala@colecapital.com'};
mail.setToAddresses(toAddresses);
mail.setBccSender(true);
mail.setBccAddresses(bccAddresses);
mail.setSubject('Call List Update process ' + a.Status);
mail.setPlainTextBody
('CallList ' + call.Name + ' ' +
'has been Updated');
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/

}














}