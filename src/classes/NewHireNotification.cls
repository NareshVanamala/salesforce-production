global class NewHireNotification implements Database.Batchable<SObject>,Database.Stateful
{
       global String Query;
   global NewHireNotification ( )  
   {  
         date d= system.today();
         Query='select id,HR_Approval_Date__c,Send_New_Hire_Notification__c,name from Employee__c where HR_Approval_Date__c!=null and Send_New_Hire_Notification__c=true'; 
        //return database.getQueryLocator(query);   
         system.debug('What is the problem'+Query);
      
    }
     
      global Database.QueryLocator start(Database.BatchableContext BC)  
    {  
      system.debug('This is start method');
      system.debug('Myquery is......'+Query);
      return Database.getQueryLocator(Query);  
    }  
      global void execute(Database.BatchableContext BC,List<Employee__c> Scope)  
      { 
         
          list<Employee__c >updatelist=new list<Employee__c>();
           system.debug('scope...'+Scope);
          //first delet  the attachment list of the email template
          for(Employee__c ecp:Scope) 
         {
             updatelist.add(ecp);
           
         }
         update updatelist;
         
         
     }     
    global void finish(Database.BatchableContext BC)  
    { 
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
       
    }
    
  }