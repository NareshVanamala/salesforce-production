@isTest
public class Test_ShowSectionController
{
    public static testmethod void myTestMethod1()
    {
         
        Account a= new Account();
        a.name='TestAccount1';
        insert a;
        
        Contact ct= new Contact();
        ct.accountid=a.id;
        ct.firstname='TestContact1';
        ct.lastname='Testingggg';
        insert ct; 
         
         Id EventRequest=[Select id,name,DeveloperName from RecordType where DeveloperName='ReimbursementRecordType' and SobjectType = 'Event_Automation_Request__c'].id; 
         
         
         Event_Automation_Request__c ect= new Event_Automation_Request__c();
         ect.Rep_Name_FBO__c=ct.id;
         ect.recordtypeid=EventRequest;
         ect.start_date__c=system.today();
         insert ect;
         
        system.assertequals(ect.Rep_Name_FBO__c,ct.id);

         
         ApexPages.CurrentPage().getParameters().put('id',ect.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(ect);
        ShowSectionController sc = new ShowSectionController(controller);
         ect.Payment_Method__c= 'Direct Pay to Vendor (Concur Reimbursement)';
         ect.Rep_Name__c=ect.Rep_Name_FBO__c;
         try{
         update ect;
         }
         catch(exception e)
         {
         }
         sc.flag = True;
         sc.flag1 = false;
         sc.hideSectionOnChange(); 
         sc.Edit1();
         sc.saveChanges();
         
         
        /* ect.Payment_Method__c='Direct Pay to Vendor (Concur Reimbursement);
         
         
         ect.Attendee_list__c=
         ect.Copy_of_invoice_or_receipt__c=
         ect.Mailing_Address__c=
         ect.Rep_Name__c=
         ect.Rep_DBA__c=
         ect. Payment_Method__c=*/
        
         
         
         
         

    }

}