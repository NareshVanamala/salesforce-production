@isTest (SeeAllData=true)
private class PortfolioMapctrl_Test{
    
    @isTest static void test_getResultTotal() {
        
        PortfolioMapctrl propertyList = new PortfolioMapctrl();
        string region = 'Pacific Southwest';
        String state = 'TX';
        String city = 'Houston';
        String tenantIndustry = '';
        String tenant = '';
        String[] filterArray = new List<String>();
        filterArray.add('Office');
        
        propertyList.getResultTotal(region,state,tenantIndustry,tenant,filterArray);
        region = 'Pacific Northwest';
        propertyList.getResultTotal(region,state,tenantIndustry,tenant,filterArray);
        region = 'Midwest';
        propertyList.getResultTotal(region,state,tenantIndustry,tenant,filterArray);
        region = 'Northeast';
        propertyList.getResultTotal(region,state,tenantIndustry,tenant,filterArray);
        region = 'Mid-Atlantic';
        propertyList.getResultTotal(region,state,tenantIndustry,tenant,filterArray);
        region = 'Southeast';
        propertyList.getResultTotal(region,state,tenantIndustry,tenant,filterArray);
        region = 'Southwest';
        propertyList.getResultTotal(region,state,tenantIndustry,tenant,filterArray);
        region = 'Canada';
        propertyList.getResultTotal(region,state,tenantIndustry,tenant,filterArray);
        state = '';
        city = '';
        tenantIndustry = 'Office';
        
        propertyList.getResultTotal(region,state,tenantIndustry,tenant,filterArray);

        tenantIndustry = '';
        tenant = '**VACANT**';

        propertyList.getResultTotal(region,state,tenantIndustry,tenant,filterArray);             

         system.assert(propertyList != Null );

    }
    
    @isTest static void test_getPropertyList() {

       PortfolioMapctrl propertyList = new PortfolioMapctrl();
       
        string region = 'Pacific Southwest';
        String state = 'TX';
        String city = 'Houston';
        String tenantIndustry = '';
        String tenant = '';
        String[] filterArray = new List<String>();
        filterArray.add('Office');
        propertyList.getPropertyList(region,state,tenantIndustry,tenant,filterArray);
        
        region = 'Pacific Northwest';
        propertyList.getPropertyList(region,state,tenantIndustry,tenant,filterArray);
        region = 'Midwest';
        propertyList.getPropertyList(region,state,tenantIndustry,tenant,filterArray);
        region = 'Northeast';
        propertyList.getPropertyList(region,state,tenantIndustry,tenant,filterArray);
        region = 'Mid-Atlantic';
        propertyList.getPropertyList(region,state,tenantIndustry,tenant,filterArray);
        region = 'Southeast';
        propertyList.getPropertyList(region,state,tenantIndustry,tenant,filterArray);
        region = 'Southwest';
        propertyList.getPropertyList(region,state,tenantIndustry,tenant,filterArray);
        region = 'Canada';
        propertyList.getPropertyList(region,state,tenantIndustry,tenant,filterArray);
        
        
        state = '';
        city = '';
        tenantIndustry = 'Office';
        filterArray.add('Industrial');
        region = 'Southwest';
        propertyList.getPropertyList(region,state,tenantIndustry,tenant,filterArray);

        tenantIndustry = '';
        tenant = '**VACANT**';
        propertyList.getPropertyList(region,state,tenantIndustry,tenant,filterArray);

        system.assert(propertyList != Null );
 
    }

    @isTest static void test_getMethods() {
        String[] filterArray = new List<String>();
        filterArray.add('Office');
        filterArray.add('Industrial');
        PortfolioMapctrl propertyList = new PortfolioMapctrl();

        propertyList.getStateList('Midwest');
        propertyList.getStateList('Pacific Southwest');
        propertyList.getStateList('Pacific Northwest');
        propertyList.getStateList('Northeast');
        propertyList.getStateList('Mid-Atlantic');
        propertyList.getStateList('Southeast');
        propertyList.getStateList('Southwest');
        propertyList.getStateList('Canada');
        
        propertyList.getgroupbyStates('Midwest','ND','Office','**VACANT**',filterArray);
        propertyList.getgroupbyStates('Pacific Southwest','AZ','Office','**VACANT**',filterArray);
        propertyList.getgroupbyStates('Pacific Northwest','AK','Office','**VACANT**',filterArray);
        propertyList.getgroupbyStates('Northeast','NY','Office','**VACANT**',filterArray);
        propertyList.getgroupbyStates('Mid-Atlantic','KY','Office','**VACANT**',filterArray);
        propertyList.getgroupbyStates('Southeast','MS','Office','**VACANT**',filterArray);
        propertyList.getgroupbyStates('Southwest','TX','Office','**VACANT**',filterArray);
        propertyList.getgroupbyStates('Canada','AB','Office','**VACANT**',filterArray);

        //propertyList.getCityList('TX');

        propertyList.getIndustryList('Pacific Southwest');
        propertyList.getIndustryList('Pacific Northwest');
        propertyList.getIndustryList('Northeast');
        propertyList.getIndustryList('Mid-Atlantic');
        propertyList.getIndustryList('Southeast');
        propertyList.getIndustryList('Southwest');
        propertyList.getIndustryList('Canada');
        propertyList.getIndustryList('Midwest');

        propertyList.getTenantList('Pacific Southwest');
        propertyList.getTenantList('Pacific Northwest');
        propertyList.getTenantList('Northeast');
        propertyList.getTenantList('Mid-Atlantic');
        propertyList.getTenantList('Southeast');
        propertyList.getTenantList('Southwest');
        propertyList.getTenantList('Canada');
        propertyList.getTenantList('Midwest');
        
       system.assert(propertyList != Null );

    }

    
}