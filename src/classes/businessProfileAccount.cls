global class  businessProfileAccount 
{
    //implements Database.Batchable<SObject>,Database.Stateful
    /*global String Query;
    global String BusinessProfileAccountlist;
    global Database.QueryLocator start(Database.BatchableContext BC)  
    {
        Query = 'select id,Account__c,Business_Type__c,Comments__c,Modified_By__c,Modified_on__c,name,LastModifiedDate from Business_Profile_Tracking__c';
       BusinessProfileAccountlist ='Business Profile id,Business Type,ContactId\n';
        return Database.getQueryLocator(Query);
    }
    global void execute(Database.BatchableContext BC,List<Business_Profile_Tracking__c> scope)  
    {   
        List<id>Aumidlist=new list<id>();
         List<Account>AumACCist=new list<Account>();
         list<Account>updatedAUM=new list<Account>();
         
         List<id>clientsinREidlist=new list<id>();
         List<Account>clientsinREACClist=new list<Account>();
         List<Account>updatedClientsinReAcc=new list<Account>();
         
         List<id>AllocatedtoREidlist=new list<id>();
         List<Account>AllocatedtoREACCist=new list<Account>();
         List<Account>updatedallocatetore=new list<Account>();
         
         List<id>REExposureThroughidlist=new list<id>();
         List<Account>REExposureThrougACCist=new list<Account>();
         List<Account>updateReExposure=new list<Account>();
         
         list<id>totalsaleslstyearid=new list<id>();
         list<Account>totalsaleslstyearAcc= new list<Account>(); 
         list<Account>updatedtotalsaleslstyearAcc= new list<Account>(); 
         
         list<id>FeeBasednoncommisionid=new list<id>();
         list<Account>FeeBasednoncommisionAcc= new list<Account>(); 
         list<Account>updatedFeeBasednoncommisionAcc= new list<Account>();
         
        list<id>businesstageIdlist=new list<id>();
        list<account>businesstageAcclist=new list<Account>();
        list<Account>updatdBusinessStage=new List<Account>();
        
       list<id>howcanwehelpIdlist=new list<id>();
       list<account>howcanwehelpAcclist=new list <Account>();
       list<Account>updatdhowcanwehelp=new List<Account>();
       
       list<id>HowwouldYouDescribeYourClientBaseidlist= new list<id>();
       list<Account>HowwouldYouDescribeYourClientAcclist=new list<Account>();
       list<Account>UpdatedHowwouldYouDescribeYourClientAccliat=new list<Account>();
       list<id>whatreitid=new list<id>();
       list<Account>whatreitACC=new list<Account>();
       list<Account>UpdatewhatREITs=new list<Account>();
       list<id>wouldyouinvestid=new list<id>();
       list<Account>wouldyouinvestACC=new list<Account>();
       list<Account>UpdatedwouldyouinvestAcc=new list<Account>();
       list<id>whynotid=new list<id>();
       list<Account>whynotACC=new list<Account>();
       list<Account>UpdatedwhynotAcc=new list<Account>();
        list<id>Volatilityproductid=new list<id>();
        list<Account>VolatilityproducACC=new list<Account>();
        list<Account>UpdateVolatility=new list<Account>();
       list<id>income=new list<id>();
       list<Account>incomeacc=new list<Account>();
       list<Account>Updateincome=new list<Account>(); 
        
       
           
        for(Business_Profile_Tracking__c objCon : scope) 
        {                        
            if(objCon.Business_Type__c=='Total AUM' && objCon.LastModifiedDate<= system.today().adddays(-1))
                AUMidlist.add(objCon.Account__c);
            if(objCon.Business_Type__c=='% of Clients with RE in Portfolio' && objCon.LastModifiedDate<= system.today().adddays(-1))
                clientsinREidlist.add(objCon.Account__c); 
            if(objCon.Business_Type__c=='Allocated to RE' && objCon.LastModifiedDate<= system.today().adddays(-1))
                AllocatedtoREidlist.add(objCon.Account__c);
            if(objCon.Business_Type__c=='RE Exposure Through' && objCon.LastModifiedDate<= system.today().adddays(-1))
                REExposureThroughidlist.add(objCon.Account__c);   
            if(objCon.Business_Type__c=='Total Sales Last Year' && objCon.LastModifiedDate<= system.today().adddays(-1))
                totalsaleslstyearid.add(objCon.Account__c); 
            if(objCon.Business_Type__c=='Fee Based (Non Commission)' && objCon.LastModifiedDate<= system.today().adddays(-1))
                FeeBasednoncommisionid.add(objCon.Account__c);  
            if(objCon.Business_Type__c=='Business Stage' && objCon.LastModifiedDate<= system.today().adddays(-1))
                businesstageIdlist.add(objCon.Account__c);   
            if(objCon.Business_Type__c=='How Can We Help?' && objCon.LastModifiedDate<= system.today().adddays(-1))
                howcanwehelpIdlist.add(objCon.Account__c); 
            if(objCon.Business_Type__c=='How would you describe your client base?' && objCon.LastModifiedDate<= system.today().adddays(-1))
                HowwouldYouDescribeYourClientBaseidlist.add(objCon.Account__c);  
            if(objCon.Business_Type__c=='What REITs?' && objCon.LastModifiedDate<= system.today().adddays(-1))
                whatreitid.add(objCon.Account__c);     
            if(objCon.Business_Type__c=='Would You Invest?' && objCon.LastModifiedDate<= system.today().adddays(-1))
                wouldyouinvestid.add(objCon.Account__c);                            
            if(objCon.Business_Type__c=='WHy Not?' && objCon.LastModifiedDate<= system.today().adddays(-1))
                whynotid.add(objCon.Account__c);
           if(objCon.Business_Type__c=='Changes in Volitality' && objCon.LastModifiedDate<= system.today().adddays(-1))
                Volatilityproductid.add(objCon.Account__c);    
           if(objCon.Business_Type__c=='Income Products Used' && objCon.LastModifiedDate<= system.today().adddays(-1))
                income.add(objCon.Account__c); 
        }            
        AumACCist=[select id, BusSize_Total_AUM_Check__c,BusSize_Total_AUM__c  from Account where id in: Aumidlist];
        for(Account acc1: AumACCist) 
         {
           acc1.BusSize_Total_AUM_Check__c=true;
           updatedAUM.add(acc1);
         } 
        clientsinREACClist=[select id, of_Clients_with_RE_in_Portfolio_Check__c,TOP5_of_Clients_with_RE_in_Portfolio__c  from Account where id in: clientsinREidlist]; 
         for(Account acc2: clientsinREACClist) 
         {
           acc2.of_Clients_with_RE_in_Portfolio_Check__c =true;
            updatedClientsinReAcc.add(acc2);
         }
        AllocatedtoREACCist=[select id, Allocated_to_RE_check__c,Bus_SizeAllocated_to_RE__c from Account where id in: AllocatedtoREidlist];
       for(Account acc4: AllocatedtoREACCist) 
       {
           acc4.Allocated_to_RE_check__c  =true;
           updatedallocatetore.add(acc4);
        }
        REExposureThrougACCist=[select id,RE_Exposure_Through_check__c, Othe_RE_Exposure__c ,RE_Exposure_Through__c from Account where id in:REExposureThroughidlist];  
        for(Account acc5: REExposureThrougACCist) 
        {
           acc5.RE_Exposure_Through_check__c  =true;
           updateReExposure.add(acc5);
        }
        totalsaleslstyearAcc=[select id,Total_Sales_Last_Year_Check__c,Fee_Based_Non_Commission_check__c,Total_Sales_Last_Year_RIA__c from Account where id in:totalsaleslstyearid]; 
         for(Account accounts : totalsaleslstyearAcc)
         {
            accounts.Total_Sales_Last_Year_Check__c = TRUE;
            updatedtotalsaleslstyearAcc.add(accounts);
         }  
         FeeBasednoncommisionAcc=[select id,Total_Sales_Last_Year_Check__c,Fee_Based_Non_Commission_check__c,Fee_Based_Non_Commission__c  from Account where id in:FeeBasednoncommisionid]; 
         for(Account accounts : FeeBasednoncommisionAcc)
          {
                 accounts.Fee_Based_Non_Commission_check__c  = TRUE;
                 updatedFeeBasednoncommisionAcc.add(accounts);
          } 
         businesstageAcclist =[select id,Business_Stage__c,Other_Business_Stage__c ,How_Can_We_Help__c ,Business_Stage_check__c,How_Can_We_Help_check__c from Account where id in :businesstageIdlist];
          for(Account accounts : businesstageAcclist )
            {
             accounts.Business_Stage_check__c = TRUE;
             updatdBusinessStage.add(accounts);
             }  
         howcanwehelpAcclist =[select id,Business_Stage__c,Other_Business_Stage__c ,How_Can_We_Help__c ,Business_Stage_check__c,How_Can_We_Help_check__c from Account where id in : howcanwehelpIdlist];
          for(Account accounts : howcanwehelpAcclist )
            {
             accounts.How_Can_We_Help_check__c= TRUE;
             updatdhowcanwehelp.add(accounts);
             }   
           HowwouldYouDescribeYourClientAcclist=[select id,How_would_you_describe_your_client_base__c,HowWdUDescribURClientcheck__c from Account where id in: HowwouldYouDescribeYourClientBaseidlist];     
           for(Account accounts : HowwouldYouDescribeYourClientAcclist)
            {
             accounts.HowWdUDescribURClientcheck__c  = TRUE;
             UpdatedHowwouldYouDescribeYourClientAccliat.add(accounts);
             }
          whatreitACC=[select id,What_REITs_check__c,What_REITs__c from Account where id in: whatreitid];   
             for(Account act: whatreitACC)
            {
            act.What_REITs_check__c = TRUE;
            UpdatewhatREITs.add(act); 
            }
          wouldyouinvestACC =[select id,Would_you_invest_check__c,Would_you_invest__c from Account where id in: wouldyouinvestid];
            for(Account accounts : wouldyouinvestACC )
            {
            accounts.Would_you_invest_check__c = TRUE;
            UpdatedwouldyouinvestAcc.add(accounts);  
             } 
          whynotACC =[select id,Why_Not_check__c,Inv_Why_Not__c from Account where id in: whynotid];
           for(Account accounts : whynotACC)
            {
            accounts.Why_Not_check__c= TRUE;
            UpdatedwhynotAcc.add(accounts);  
            }  
           VolatilityproducACC =[select id,Changes_in_Volitality_check__c from Account where id in: Volatilityproductid];
            for(Account accounts : VolatilityproducACC )
            {
            accounts.Changes_in_Volitality_check__c= TRUE;
             UpdateVolatility.add(accounts);  
             }  
           incomeacc =[select id,Income_Products_UsedCheck__c from Account where id in: income];
            for(Account accountsss : incomeacc)
            {
                accountsss.Income_Products_UsedCheck__c = TRUE;
                updateincome.add(accountsss);
            }           
          try
        {
           helper.run=false;
           if(updatedAUM.size()>0)
           update updatedAUM;
           if(updatedClientsinReAcc.size()>0)
           update updatedClientsinReAcc;
           if(updatedallocatetore.size()>0)
           update updatedallocatetore;
           if(updateReExposure.size()>0)
           update updateReExposure;
           if(updatedtotalsaleslstyearAcc.size()>0)
           update updatedtotalsaleslstyearAcc;
           if(updatedFeeBasednoncommisionAcc.size()>0)
           update updatedFeeBasednoncommisionAcc; 
           if(updatdBusinessStage.size()>0)
           update updatdBusinessStage;
           if(updatdhowcanwehelp.size()>0)
           update updatdhowcanwehelp;
           if(UpdatedHowwouldYouDescribeYourClientAccliat.size()>0)
           update UpdatedHowwouldYouDescribeYourClientAccliat;
            if(UpdatewhatREITs.size()>0)
            update UpdatewhatREITs;
           if(UpdatedwouldyouinvestAcc.size()>0)
            update UpdatedwouldyouinvestAcc; 
           if(UpdatedwhynotAcc.size()>0)
            update UpdatedwhynotAcc;  
           if(UpdateVolatility.size()>0)
            update UpdateVolatility;     
           if(updateincome.size()>0)
            update updateincome;  
         }
         
         catch(system.dmlexception e)
         {
              System.debug('Not updated the records ' + e);
         }   
         
    }    
    global void finish(Database.BatchableContext BC)  
    { 
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
      Messaging.EmailFileAttachment csvAttc1 = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(BusinessProfileContactlist);
        string csvname= 'Business Profile Tracking Contactlist.csv';
        csvAttc1.setFileName(csvname);
        csvAttc1.setBody(csvBlob);
         Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'ksingh@colecapital.com'};
        String[] bccAddresses = new String[] {'ntambe@colecapital.com','skalamkar@colecapital.com'};
        String subject ='BusinessProfileTrackingAccount list';
        email.setSubject(subject);
        email.setBccSender(true);
        email.setToAddresses( toAddresses );
        email.setBccAddresses(bccAddresses);
        email.setPlainTextBody('Business Profile aging process has been completed');
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc1});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});       
       
    } */         
        
          
        





}