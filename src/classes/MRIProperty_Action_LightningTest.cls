@isTest
private class MRIProperty_Action_LightningTest {
	
	@isTest static void consolidatedPropertiesTest() {
		Test.startTest();
			MRIProperty_Action_Lightning.consolidatedProperties();
		Test.stopTest();
	}
	
	@isTest static void pollBatchTest() {
		String batchId= MRIProperty_Action_Lightning.consolidatedProperties();
		Test.startTest();
			MRIProperty_Action_Lightning.pollBatch(batchId);
		Test.stopTest();
	}


	@isTest(SeeAllData=true)
    static void getAccountRestrictionTest() {
		Account acc= [select id from Account limit 1][0];
		Test.startTest();
			MRIProperty_Action_Lightning.getAccountRestriction(acc.id);
		Test.stopTest();
	}
	
	@isTest(SeeAllData=true)
    static void getProjectTest() {
		Project__c proj= [select id from Project__c limit 1][0];
		Test.startTest();
			MRIProperty_Action_Lightning.getProject(proj.id);
		Test.stopTest();
	}
	
	@isTest(SeeAllData=true)
    static void getLeaseChargesRequestRecordsTest() {
		Lease_Charges_Request__c lcr= [select id from Lease_Charges_Request__c limit 1][0];
		Test.startTest();
			MRIProperty_Action_Lightning.getLeaseChargesRequestRecords(lcr.id);
		Test.stopTest();
	}

	@isTest(SeeAllData=true)
    static void getLeaseOpportunityRecordsTest() {
		LC_LeaseOpprtunity__c lopp= [select id from LC_LeaseOpprtunity__c limit 1][0];
		Test.startTest();
			MRIProperty_Action_Lightning.getLeaseOpportunityRecords(lopp.id);
		Test.stopTest();
	}

}