@isTest(seeAlldata =true)
public class PC_PropContactsHelperClass_Test{
    static testMethod void testmethod1(){
        boolean MassApplyFunctionality = true;
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
        Account acc = new Account (
        Name = 'newAcc1'
        );  
        insert acc;
        Contact con = new Contact (
        AccountId = acc.id,
        LastName = 'portalTestUser'
        );
        insert con;
        Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
         
        User newUser = new User(
        profileId = p.id,
        username = 'newUser0615201720511@arcpreit.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = con.id
        );
        insert newUser;
        list<MRI_Property__c>MRIList = new list<MRI_Property__c>();
        
        MRI_Property__c mrpObj = new MRI_Property__c();
        mrpObj.Name = 'testmrp';
        mrpObj.Property_Manager__c = newUser.id;
        mrpObj.Fund1__c = 'ARCP';
        mrpObj.Date_Acquired__c = Date.today();
        mrpObj.Common_Name__c = 'cname';
        mrpObj.Property_Id__c = '1234';
        insert mrpObj;
        
        MRIList.add(mrpObj);
        
        list<Lease__c> leaseList = new list<Lease__c>();
        
        Lease__c lObj = new Lease__c();
        lObj.MRI_Property__c = mrpObj.Id;
        lObj.Tenant_Name__c = 'testTenent1';
        lObj.Lease_Status__c = 'Active';
        insert lObj;
        
        leaseList.add(lObj);
              
        
        Property_Contacts__c pcObj = new Property_Contacts__c();
        pcObj.Name = 'test1';
        pcObj.MRI_Property__c = mrpObj.Id;
        insert pcObj;
        
        
        Property_Contact_Buildings__c pcb = new Property_Contact_Buildings__c();
        pcb.Center_Name__c= mrpObj.Id;
        pcb.Property_Contacts__c = pcObj.Id;
        pcb.Contact_Type__c = 'Dining Services;Electricity;Elevators;Facilities';
        insert pcb;
        
        Property_Contact_Leases__c pcl = new Property_Contact_Leases__c();
        pcl.Lease__c= lObj.Id;
        pcl.Property_Contacts__c = pcObj.Id;
        pcl.Contact_Type__c = 'TPM Admin;JV Partner-Primary';
        insert pcl;
        
        list<String> contTypeList = new list<String>();
        contTypeList.add(pcb.Property_Contacts__c);
        
        PC_PropContactsHelperClass propCon = new PC_PropContactsHelperClass();
        propCon.insertPropConLeases(leaseList,pcObj.id,pcl.Contact_Type__c,MassApplyFunctionality,pcl.Id);
        propCon.insertPropConLeases(leaseList,pcObj.id,pcl.Contact_Type__c+';Financials - Corporate;Assignee;Assignor',MassApplyFunctionality,pcl.Id);
        propCon.insertPropConBuildings(MRIList,pcObj.id,pcb.Contact_Type__c,MassApplyFunctionality,pcb.Id);
        propCon.insertPropConBuildings(MRIList,pcObj.id,pcb.Contact_Type__c+';HVAC;Janitorial',MassApplyFunctionality,pcb.Id);
        propCon.getuniqueContacttypes(contTypeList,contTypeList);
        propCon.compareid(pcObj.id,pcObj.id);
        

  }
}