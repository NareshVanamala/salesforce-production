public with sharing class CP_AccProdcls{
Account account;
public CP_HelperAccConProdcls cls {get;set;}
public string str {get;set;}
public Boolean selected {get;set;}
//public Boolean prev{get;set;}
//public Boolean nxt{get;set;}
public id curid;
    public CP_AccProdcls(ApexPages.StandardController controller) {
         cls=new CP_HelperAccConProdcls();
         curid= ApexPages.CurrentPage().getParameters().get('id');
         system.debug('******************************* CP_AccProdcls curid:'+curid); 
         str='';
    }
    public void search() {
        if(str!='') {
             cls.Searchpro('%' + str + '%'); 
             system.debug('******************************* CP_AccProdcls STR :'+str); 
         }
    }
    
    public PageReference addacco() {
        cls.addAcc(curid,selected);

         if(!cls.isFailed && cls.selectprod.size() > 0) {
            PageReference pageRef = new PageReference('/'+curid);
            pageRef.setRedirect(true);
            return pageRef;
        }
        
        return null;
    }
    public PageReference Clear() {
         PageReference pageRef = new PageReference('/apex/CP_AccProd?id='+curid);
            pageRef.setRedirect(true);
            return pageRef;
    }
    
}