global class Paradot_AutomationBatch 
{
    /*list<contact> contactlist = new list<contact>();
    list<contact> clist = new list<contact>();
    map<String,String>SalesdirectorUser=new map<String,String>();
    map<String,String>InternalWholeSalerUser=new map<String,String>();
    map<String,String>RegionalTerritoryManagerUser=new map<String,String>(); 
    list<String>UserNameList=new list<String>();
    set<String>UserNameSet=new set<String>(); 
    list<User>MyUserlist=new list<User>();
     list<User>InActiveUserlist=new list<User>();
    Map<String,id>UserNametoIdMap=new Map<String,id>();
    Map<String,id>InActiveUserNametoIdMap=new Map<String,id>();
    global Database.QueryLocator Start(Database.BatchableContext BC)
    {
            list<RecordType> rrrecordtype= new list<RecordType>(); 
            String Query;
            rrrecordtype = [select Id, Name,DeveloperName from RecordType where (DeveloperName ='NonInvestorRepContact' or DeveloperName='Registered_Investment_Advisor_RIA_Contact' or DeveloperName='Dually_Registered_Contact')and SobjectType = 'Contact'];
            Query = 'Select Id,Name,Wholesaler__c,Sales_Director__c,Internal_Wholesaler__c,Internal_Wholesaler1__c,Regional_Territory_Manager__c,Regional_Territory_Manager1__c from Contact where RecordTypeId in : rrrecordtype'; 
            System.debug('My Query Is.......'+Query);
            return Database.getQueryLocator(Query);
    }
    global void Execute(Database.BatchableContext BC,List<Contact> Scope)
    { 
        
         //User Us=[select id, name from User where name='Lindsey Tummond' and User_License__c!='Customer Community Plus' ];
         for(contact c:scope)
         {
            UserNameList.add(c.Wholesaler__c);
            UserNameList.add(c.Internal_Wholesaler__c);
            UserNameList.add(c.Regional_Territory_Manager__c);
         }
          UserNameSet.addAll(UserNameList);
          MyUserlist=[select id, name from User where name in:UserNameSet and IsActive=true and User_License__c!='Customer Community Plus'];
          InActiveUserlist=[select id, name from User where name in:UserNameSet and IsActive=false and User_License__c!='Customer Community Plus'];
             if(MyUserlist.size()>0)
             {
                for(User u:MyUserlist)
                 UserNametoIdMap.put(u.name,u.id);
             }
             if(InActiveUserlist.size()>0)
             {
                for(User u:InActiveUserlist)
                 InActiveUserNametoIdMap.put(u.name,u.id);
             }
             
          for(contact c:scope)
         {
            if((c.Wholesaler__c!=null)&&(UserNametoIdMap.get(c.Wholesaler__c)!=null)&&(c.Wholesaler__c!='Not Assigned'))
             c.Sales_Director__c=UserNametoIdMap.get(c.Wholesaler__c);
             
            else if((c.Wholesaler__c=='Not Assigned')||(c.Wholesaler__c==null)||(InActiveUserNametoIdMap.get(c.Wholesaler__c)!=null)) 
               c.Sales_Director__c=null;
             
            if((c.Internal_Wholesaler__c!=null)&&(UserNametoIdMap.get(c.Internal_Wholesaler__c)!=null)&&(c.Internal_Wholesaler__c!='Not Assigned'))
             c.Internal_Wholesaler1__c=UserNametoIdMap.get(c.Internal_Wholesaler__c);
           
            else if((c.Internal_Wholesaler__c=='Not Assigned')||(c.Internal_Wholesaler__c==null)||(InActiveUserNametoIdMap.get(c.Internal_Wholesaler__c)!=null)) 
              c.Internal_Wholesaler1__c=null;
             
             if((c.Regional_Territory_Manager__c!=null)&&(UserNametoIdMap.get(c.Regional_Territory_Manager__c)!=null)&&(c.Regional_Territory_Manager__c!='Not Assigned'))
               c.Regional_Territory_Manager1__c =UserNametoIdMap.get(c.Regional_Territory_Manager__c);
            
             else if((c.Regional_Territory_Manager__c=='Not Assigned')||(c.Regional_Territory_Manager__c==null)||(InActiveUserNametoIdMap.get(c.Regional_Territory_Manager__c)!=null)) 
              c.Regional_Territory_Manager1__c=null;
             
             contactlist.add(c); 
            
                                 
         }
          
         if(contactlist.size()>0)
           update contactlist;
         
          
    }          
    global void finish(Database.BatchableContext BC)
    {
       
       
   
   
   }
    
*/

}