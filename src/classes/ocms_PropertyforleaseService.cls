global with sharing class ocms_PropertyforleaseService implements cms.ServiceInterface {

    public String JSONResponse {get; set;}
    public String action {get; set;}
    public Map<String, String> parameters {get; set;}
    public ocms_propertiesforlease wpl = new ocms_propertiesforlease();

    global ocms_PropertyforleaseService() {
        
    }

    global System.Type getType(){
        return ocms_PropertyforleaseService.class;
    }

    global String executeRequest(Map<String, String> p) {
        this.action = p.get('action');
        this.parameters = p; 
        this.LoadResponse();    
        return this.JSONResponse;
    }

    global void loadResponse(){
        if (this.action == 'getStateList'){
            this.getStateList();
        } else if (this.action == 'getCityList'){
            this.getCityList(parameters.get('state'));
        } else if (this.action == 'getResultTotal'){

            String state = parameters.get('state');
            String city = parameters.get('city');

            this.getResultTotal(state, city);

        } else if (this.action == 'getPropertyList'){

            String state = parameters.get('state');
            String city = parameters.get('city');
            String sortBy = parameters.get('sortBy');
            String rLimit = parameters.get('limit');
            String offset = parameters.get('offset');

            this.getPropertyList(state, city, sortBy, rLimit, offset);
            
        }/* else if (this.action == 'getsqft'){

            String state = parameters.get('state');
            String city = parameters.get('city');
            String sortBy = parameters.get('sortBy');
            String rLimit = parameters.get('limit');
            String offset = parameters.get('offset');

            this.getsqft(state, city, sortBy, rLimit, offset);
            
        }*/
    }

    public void getResultTotal(String state, String city){
        this.JSONResponse = JSON.serialize(wpl.getResultTotal(state, city));
    }

    public void getPropertyList(String state, String city,String sortBy, String rLimit, String offset){
        this.JSONResponse = JSON.serialize(wpl.getPropertyList(state, city, sortBy, rLimit, offset));
    }

    public void getStateList(){
        this.JSONResponse = wpl.getStateList();
    }
    
  /*  public void getsqft(String state, String city,String sortBy, String rLimit, String offset){
        this.JSONResponse = wpl.getsqft(state, city, sortBy, rLimit, offset);
    }
    */
    
    public void getCityList(String state){
        this.JSONResponse = JSON.serialize(wpl.getCityList(state));
    }

}