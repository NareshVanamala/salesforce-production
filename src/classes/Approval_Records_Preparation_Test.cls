@isTest(seeAllData=false)
private class Approval_Records_Preparation_Test  {

    static testmethod void ApprovalRecords() {
        // The query used by the batch job.
        MRI_Property__c m =new MRI_Property__c();
        m.name='SampleMriproperty';
        m.Property_ID__c ='123456';
        m.Web_Ready__c ='Rejected';
        m.Fund1__c = 'ARCP';
        insert m;
        System.assertnotEquals(m.name,m.Property_Id__c);
        String query = 'SELECT Id,Name,Property_ID__c,Status__c,Web_Ready__c FROM MRI_Property__c  where (Fund1__c=\'ARCP\' or Fund1__c=\'CCPT3\') And (Web_Ready__c=\'Approved\' or Web_Ready__c=\'Pending Approval\' or Web_Ready__c=\'Pending Deleted\' limit 1';
                   

        Test.startTest();
        
       Approval_Records_Preparation  c = new Approval_Records_Preparation ();
       Database.executeBatch(c,1);
       Test.stopTest();

      
    }
}