public with sharing class MultiplePriorityController 
{
   /*public list<Automated_Call_List__c>tcalllist{set;get;}
   public list<Automated_Call_List__c>Updatedcalllist;
   public String calllist{set;get;} 
   public List<SelectOption> CallListValues {set;get;}
   public String calllist2 {get;set;}
   public List<SelectOption> CallListValues2 {set;get;}
   public String calllist3 {get;set;}
   public String calllist4 {get;set;}
   public String calllist5 {get;set;}
    public String calllist6 {get;set;}
   public String calllist7 {get;set;}
   public String calllist8 {get;set;}
    public String calllist9 {get;set;}
   public String calllist10 {get;set;}
   public String calllist11 {get;set;}
    public String calllist12 {get;set;}
   public String calllist13 {get;set;}
   public String calllist14 {get;set;}
   public String calllist15 {get;set;}
   public String calllist16 {get;set;}
   public String calllist17 {get;set;}
   public String calllist18 {get;set;}
   public String calllist19 {get;set;}
   public String calllist20 {get;set;} 
   
   public set<id> automatedCallset{get;set;}
   public boolean updatelist{get;set;}
   public boolean updatelist1{get;set;}
   public integer count{get;set;}
    public MultiplePriorityController()
   {
       updatelist=true;
       updatelist1=true;
       set<String> call = new Set<String>();
       List<String> lscall = new List<String>();
       tcalllist=new list<Automated_Call_List__c>();
        count=0;
        list<Automated_Call_List__c>priority1record=[select ID,Name,Automated_Call_list_Name__c,Global_Priority__c from Automated_Call_List__c where IsDeleted__c=false and Archived__c=false and (Global_Priority__c=1 or Global_Priority__c=2 or Global_Priority__c=3 or Global_Priority__c=4 or Global_Priority__c=5 or Global_Priority__c=6 or Global_Priority__c=7 or Global_Priority__c=8 or Global_Priority__c=9 or Global_Priority__c=10 or Global_Priority__c=11 or Global_Priority__c=12 or Global_Priority__c=13 or Global_Priority__c=14 or Global_Priority__c=15 or Global_Priority__c=16 or Global_Priority__c=17 or Global_Priority__c=18 or Global_Priority__c=19 or Global_Priority__c=20)];
        if(priority1record.size()>0)
        {
              for(Automated_Call_List__c act:priority1record)
              {
                if(act.Global_Priority__c==1)
                 calllist=act.name;
                if(act.Global_Priority__c==2)
                 calllist2=act.name;
                 if(act.Global_Priority__c==3)
                 calllist3=act.name;
                 if(act.Global_Priority__c==4)
                 calllist4=act.name;
                 if(act.Global_Priority__c==5)
                 calllist5=act.name;
                 if(act.Global_Priority__c==6)
                 calllist6=act.name;
                 if(act.Global_Priority__c==7)
                 calllist7=act.name;
                 if(act.Global_Priority__c==8)
                 calllist8=act.name;
                 if(act.Global_Priority__c==9)
                 calllist9=act.name;
                 if(act.Global_Priority__c==10)
                 calllist10=act.name;
                 if(act.Global_Priority__c==11)
                 calllist11=act.name;
                 if(act.Global_Priority__c==12)
                 calllist12=act.name;
                 if(act.Global_Priority__c==13)
                 calllist13=act.name;
                 if(act.Global_Priority__c==14)
                 calllist14=act.name;
                 if(act.Global_Priority__c==15)
                 calllist15=act.name;
                 if(act.Global_Priority__c==16)
                 calllist16=act.name;
                 if(act.Global_Priority__c==17)
                 calllist17=act.name;
                 if(act.Global_Priority__c==18)
                  calllist18=act.name;
                 if(act.Global_Priority__c==19)
                  calllist19=act.name;
                 if(act.Global_Priority__c==20)
                  calllist20=act.name;
                 
             }
            } 
  
       automatedCallset= new set<id>();
       Updatedcalllist=new list<Automated_Call_List__c>();
        try
        {
          
         list<Automated_Call_List__c>acl= new list<Automated_Call_List__c>();  
            
                  
          List<Automated_Call_List__c> tcall = [select ID,Name,Automated_Call_list_Name__c from Automated_Call_List__c where IsDeleted__c=false and Archived__c=false and Original_Call_list__c=null];
             for(Automated_Call_List__c st : tcall )
               {
                     acl.add(st);
               }

           
            for(Automated_Call_List__c t : acl)
            call.add(t.Name); 
            call.add('--None--');
            CallListValues = new List<SelectOption>();
            CallListValues.add(new SelectOption('--None--','--None--'));
            lscall.AddAll(call);
            lscall.sort();
            for(String s : lscall)
               CallListValues.add(new SelectOption(s,s));
           
            if(CallListValues.size()==0)
            CallListValues.add(new SelectOption('None','None'));
            
        }catch(Exception e){}
   } 
   
    public void AdviserCallList()
    {
        system.debug('Calllist1--'+calllist);
        system.debug('Calllist2--'+calllist2);
        system.debug('Calllist3--'+calllist3);
        system.debug('Calllist4--'+calllist4);
        system.debug('Calllist5--'+calllist5);
        tcalllist = [select ID,Name,Automated_Call_list_Name__c,PrioritySetter__c from Automated_Call_List__c where IsDeleted__c=false and Archived__c=false and((Name=:calllist)or(Name=:calllist2)or(Name=:calllist3)or(Name=:calllist4)or(Name=:calllist5)or(Name=:calllist6)or(Name=:calllist7)or(Name=:calllist8)or(Name=:calllist9)or(Name=:calllist10)or(Name=:calllist11)or(Name=:calllist12)or(Name=:calllist13)or(Name=:calllist14)or(Name=:calllist15)or(Name=:calllist16)or(Name=:calllist17)or(Name=:calllist18)or(Name=:calllist19)or(Name=:calllist20) )];
        system.debug('Check the call list'+tcalllist);
    }
  
   public PageReference cancel()
   {
        return null;
   }
   
   public PageReference setPriorityOfCallList()
   {
      list<string> alist = new list<string>();
      system.debug('Check the value here'+calllist);
      tcalllist = [select ID,Name,Automated_Call_list_Name__c,PrioritySetter__c from Automated_Call_List__c where IsDeleted__c=false and Archived__c=false and((Name=:calllist)or(Name=:calllist2)or(Name=:calllist3)or(Name=:calllist4)or(Name=:calllist5)or(Name=:calllist6)or(Name=:calllist7)or(Name=:calllist8)or(Name=:calllist9)or(Name=:calllist10)or(Name=:calllist11)or(Name=:calllist12)or(Name=:calllist13)or(Name=:calllist14)or(Name=:calllist15)or(Name=:calllist16)or(Name=:calllist17)or(Name=:calllist18)or(Name=:calllist19)or(Name=:calllist20) )];
      alist.add(calllist);
      alist.add(calllist2);
      alist.add(calllist3);
      alist.add(calllist4);
      alist.add(calllist5);
      alist.add(calllist6);
      alist.add(calllist7);
      alist.add(calllist8);
      alist.add(calllist9);
      alist.add(calllist10);
      alist.add(calllist11);
      alist.add(calllist12);
      alist.add(calllist13);
      alist.add(calllist14);
      alist.add(calllist15);
      alist.add(calllist16);
      alist.add(calllist17);
      alist.add(calllist18);
      alist.add(calllist19);
      alist.add(calllist20);
      system.debug('Hiiiiiiiiiiiiiiiiiii'+alist);
      if((calllist==null) && (calllist2==null) && (calllist3==null) && (calllist4==null) && (calllist5==null)&& (calllist6==null) && (calllist7==null) && (calllist8==null) && (calllist9==null) && (calllist10==null) && (calllist11==null) && (calllist12==null) && (calllist13==null) && (calllist14==null) && (calllist15==null) && (calllist16==null) && (calllist17==null) && (calllist18==null) && (calllist19==null) && (calllist20==null))
      {
      updatelist1=false;
      }
      
      if((calllist=='--None--') && (calllist2=='--None--') && (calllist3=='--None--') && (calllist4=='--None--') && (calllist5=='--None--') && (calllist6=='--None--') && (calllist7=='--None--') && (calllist8=='--None--') && (calllist9=='--None--') && (calllist10=='--None--') && (calllist11=='--None--') && (calllist12=='--None--') && (calllist13=='--None--') && (calllist14=='--None--') && (calllist15=='--None--') && (calllist16=='--None--') && (calllist17=='--None--') && (calllist18=='--None--') && (calllist19=='--None--') && (calllist20=='--None--'))
      {
      updatelist1=false;
      }
      
       if(updatelist1==false)
      {
       ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please Make Sure All calllists should not be None'));
      }
           
      if((updatelist==true) && (updatelist1==true))
      {
      for(Automated_Call_List__c ct:tcalllist)
      {
          if(ct.name==calllist)
          {
               ct.PrioritySetter__c=1;
               ct.PHandler__c=1; 
               ct.Global_Priority__c=1;
               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
          else if(ct.name==calllist2)
          {
               ct.PrioritySetter__c=2; 
               ct.PHandler__c=2;
               ct.Global_Priority__c=2;

               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
           if(ct.name==calllist3)
          {
               ct.PrioritySetter__c=3;
               ct.PHandler__c=3; 
               ct.Global_Priority__c=3;

               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
           if(ct.name==calllist4)
          {
               ct.PrioritySetter__c=4; 
               ct.PHandler__c=4;
               ct.Global_Priority__c=4;

               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
           if(ct.name==calllist5)
          {
               ct.PrioritySetter__c=5; 
               ct.PHandler__c=5;
               ct.Global_Priority__c=5;

               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
          
          if(ct.name==calllist6)
          {
               ct.PrioritySetter__c=6;
               ct.PHandler__c=6; 
               ct.Global_Priority__c=6;
               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
          else if(ct.name==calllist7)
          {
               ct.PrioritySetter__c=7; 
               ct.PHandler__c=7;
               ct.Global_Priority__c=7;

               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
           if(ct.name==calllist8)
          {
               ct.PrioritySetter__c=8;
               ct.PHandler__c=8; 
               ct.Global_Priority__c=8;
               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
           if(ct.name==calllist9)
          {
               ct.PrioritySetter__c=9; 
               ct.PHandler__c=9;
               ct.Global_Priority__c=9;
               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
           if(ct.name==calllist10)
          {
               ct.PrioritySetter__c=10; 
               ct.PHandler__c=10;
               ct.Global_Priority__c=10;
               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
          if(ct.name==calllist11)
          {
               ct.PrioritySetter__c=11;
               ct.PHandler__c=11; 
               ct.Global_Priority__c=11;
               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
          else if(ct.name==calllist12)
          {
               ct.PrioritySetter__c=12; 
               ct.PHandler__c=12;
               ct.Global_Priority__c=12;
               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
           if(ct.name==calllist13)
          {
               ct.PrioritySetter__c=13;
               ct.PHandler__c=13; 
               ct.Global_Priority__c=13;

               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
           if(ct.name==calllist14)
          {
               ct.PrioritySetter__c=14; 
               ct.PHandler__c=14;
               ct.Global_Priority__c=14;
               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
           if(ct.name==calllist15)
          {
               ct.PrioritySetter__c=15; 
               ct.PHandler__c=15;
               ct.Global_Priority__c=15;
               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
          if(ct.name==calllist16)
          {
               ct.PrioritySetter__c=16;
               ct.PHandler__c=16; 
               ct.Global_Priority__c=16;
               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
          else if(ct.name==calllist17)
          {
               ct.PrioritySetter__c=17; 
               ct.PHandler__c=17;
               ct.Global_Priority__c=17;
               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
           if(ct.name==calllist18)
          {
               ct.PrioritySetter__c=18;
               ct.PHandler__c=18; 
               ct.Global_Priority__c=18;
               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
           if(ct.name==calllist19)
          {
               ct.PrioritySetter__c=19; 
               ct.PHandler__c=19;
               ct.Global_Priority__c=19;
               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
           if(ct.name==calllist20)
          {
               ct.PrioritySetter__c=20; 
               ct.PHandler__c=20;
               ct.Global_Priority__c=20;
               Updatedcalllist.add(ct);
               automatedCallset.add(ct.id);
          }
      
      }
      try
      {
          system.debug('Check the Updatedcalllist'+Updatedcalllist);
          update Updatedcalllist;
         
      }
      catch(Exception e)
      {
      
      system.debug('Exception...'+ e);
      
      }
     
       system.debug('check the updatelist'+Updatedcalllist);
       SetfirstFivePriorityonCCL batch25 = new SetfirstFivePriorityonCCL(automatedCallset);
      Id batchId = Database.executeBatch(batch25); 
      
      PageReference ref1=new PageReference('/apex/calllistpage');
      return ref1 ;
      
      }
      updatelist=true;
      updatelist1=true;
      
      return null;
      
      
    }*/
    
    
          
}