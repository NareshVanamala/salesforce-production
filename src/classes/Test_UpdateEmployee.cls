@istest(seeAlldata=true)
public class Test_UpdateEmployee{
    static testmethod void UpdateEmployeetest(){
    Employee__c emp = new Employee__c();
    emp.name = 'Test';
    emp.First_Name__c='test';
    emp.HR_Manager_Status__c = 'Approved';
    emp.Employee_Status__c='On Boarding inProcess';
    insert emp;
     emp.Employee_Status__c='On Board';
     update emp;
    
    Employee__c emp1 = new Employee__c();
    emp1.name = 'Test';
    emp1.HR_Manager_Status__c = 'Approved';
    emp1.Employee_Status__c='On Board';
    emp1.First_Name__c='test';
    insert emp1;
    emp1.Employee_Status__c='On Boarding inProcess';
    update emp1;
    
    Employee__c emp2 = new Employee__c();
    emp2.name = 'Test';
    emp2.First_Name__c='test';
    emp2.HR_Manager_Status__c = 'Approved';
    emp2.Employee_Status__c='On Board';
    insert emp2;
     emp2.Employee_Status__c='Terminated';
     update emp2;
    
     Asset__c a = new Asset__c();
        a.Added_On__c= system.today();
        a.Asset_Status__c='Removal In Process';
        a.Employee__c = emp1.id;
        insert a;
    system.assertequals(a.Employee__c,emp1.id);

       a.Asset_Status__c= 'In Process';
      try{
       update a;
      }
      catch(Exception e){}
        
        
    
     //list<ProcessInstance> processInstances  = [select id from ProcessInstance where TargetObjectId =:emp.id];
       // system.assertequals(processInstances.size(),1);
    
    }
     static testmethod void UpdateEmployeetest1(){
     
     Employee__c emp = new Employee__c();
    emp.name = 'Test';
    emp.HR_Manager_Status__c = 'Approved';
    emp.Employee_Status__c='On Boarding inProcess';
    insert emp;
        system.assertequals( emp.name,'Test');

    emp.Employee_Status__c='On Board';
    try{
       update emp;
      }
      catch(Exception e){}
    
    
     Asset__c a = new Asset__c();
        a.Added_On__c= system.today();
        a.Asset_Status__c='Removal In Process';
        a.Employee__c = emp.id;
        insert a;
        system.assertequals(a.Employee__c,emp.id);

       a.Asset_Status__c= 'In Process';
       a.Final_Step_Procssed__c = true;
       a.Removal_Final_Step_processed__c = true;
     try{
       update a;
      }
      catch(Exception e){}  
         system.assertequals(a.Employee__c,emp.id);

     
     /*Asset__c a1 = new Asset__c();
        a1.Added_On__c= system.today();
        a1.Asset_Status__c='Removal In Process';
        a1.Employee__c = emp.id;
        a1.Terminating__c = false;
        insert a1;
      // a1.Asset_Status__c= 'In Process';
       a1.Termination_Status__c='Inactive';
       a1.Final_Step_Procssed__c = true;
       a1.Removal_Final_Step_processed__c = true;
     update a1;*/
    

              
       list<ProcessInstance> processInstances  = [select id from ProcessInstance where TargetObjectId =:a.id];
        system.assertequals(processInstances.size(),0);
        
         list<ProcessInstance> processInstances1  = [select id from ProcessInstance where TargetObjectId =:emp.id];
        system.assertequals(processInstances1.size(),0);
     }



}