public with sharing class IeTemplateController { 
   public IeTemplateController(cms.CoreController controller)
 { 
      Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge'); 
   }
    public IeTemplateController()
 { 
      
   }
}