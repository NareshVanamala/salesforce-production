@isTest()
  private class BatchContactInvupdate_Test { 
     
static testMethod void batchContactInvupdate_TestMethod (){
Profile prof = [select id from profile where name='system Administrator'];
     User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
   Account accRec = new Account(name='testName', Ownerid = usr.id,Type='Banker');
   insert accRec ;
  contact conobj = new contact();
  conobj.LastName ='Test Conatct';
  conobj.Relationship__c = 'Prospect';
  conobj.Contact_Type__c = 'Cole';
  conobj.Territory__c = 'West TX';
  conobj.RIA_Territory__c = 'West Region';
  conobj.AccountId = accRec.id;
   insert conobj;
   System.assertEquals(conobj.LastName,'Test Conatct');
  Contact_InvestorList__c coninv = new Contact_InvestorList__c(name='testName', Pdoc__c=true,Attachment_Id__c='',Advisor_Name__c=conobj.id);
   insert coninv ;
    String query = 'SELECT Attachment_Id__c,Investor_Last_Name__c,Pdoc_Ready__c,Pdoc__c FROM Contact_InvestorList__c WHERE Pdoc_Ready__c = true AND Attachment_Id__c = null ';
   Test.startTest();
       batchContactInvupdate batchconinv = new batchContactInvupdate();
       ID batchprocessid = Database.executeBatch(batchconinv);
       Test.stopTest();
  }
}