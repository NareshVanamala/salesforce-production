public with sharing class CM_CustomVendorLookupController
{
    public string phoneNumberPassToTextBox{get;set;}
    public List<Vendor__c > results{get;set;}
    public string searchString{get;set;}  
    public void SearchRecord()
    {
        showsearch=true;  
        shownew=false;  
        system.debug('This is my searchstring'+searchString);
        system.debug('This is what I selected'+Snooze);
        runSearch();  
     }
     
      @testvisible private void runSearch()
      { 
          results = performSearch(searchString);
          system.debug('this is my results'+results );
      } 
       private List<Vendor__c > performSearch(string searchString)
       {
          String soql = 'select id, name from Vendor__c ';
         
           if(searchString != '' && searchString != null && Snooze=='Name') 
           { 
             soql = soql +  ' where name LIKE \'%' +String.escapeSingleQuotes(searchString) +'%\'';
             soql = soql + ' limit 200';
           }
           if(searchString != '' && searchString != null && Snooze=='All fields' )
           {
               // soql = soql + '  where (name LIKE \'%' + searchString +'%\')'+'OR'+'(Address__c LIKE \'%' + searchString +'%\')+'OR'+'(City__c LIKE \'%' + searchString +'%\'))' +'OR'+ '(Notes__c LIKE \'%' + searchString +'%\'))' + 'OR'+ '(Address__c LIKE \'%' + searchString +'%\'))' +'OR'+ '(State__c LIKE \'%' + searchString +'%\'))' + 'OR'+ '(Zip__c LIKE \'%' + searchString +'%\'))' +'OR'+ '(Phone_Number__c LIKE \'%' + searchString +'%\'))';
                soql = soql +  ' where (name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\')'+'OR'+'(Address__c LIKE \'%' +String.escapeSingleQuotes(searchString) +'%\')'+'OR'+'(City__c LIKE \'%' + String.escapeSingleQuotes(searchString)+'%\')';
                
           }          
           System.debug(soql);
           return database.query(soql); 
        }

    public PageReference Save()
    {
       
       Vendor__c crc= new Vendor__c();
       if((vdr.Name=='')||(vdr.Name==null))
       {
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Enter Venor Name')); 
       }
       else
       {
           crc.Name=vdr.Name;
           crc.Phone_Number__c=vdr.Phone_Number__c;
           
           if (Schema.sObjectType.Vendor__c.isCreateable())
           insert crc;
           //phoneNumberPassToTextBox=crc.id;
           phoneNumberPassToTextBox=crc.Name;
       }   
       
        return null;
    }


    public PageReference createNewRecord()
    {
      system.debug('Woh jo hamase keh na sake...') ;
       // Vendor__c vr1= new Vendor__c ();
         showsearch=false;  
         shownew=true;
        
        return null;
    }

   public Vendor__c vdr{get; set;}
   public boolean showsearch{get; set;}
   public boolean shownew{get; set;}
   public string Snooze {get; set;}
   //public List<user> results{get;set;} 
   
   public CM_CustomVendorLookupController()
   {
     showsearch=true;  
     shownew= false;
      vdr= new Vendor__c();
   }
   
   
   public List<SelectOption> getSnoozeradio()
  {
    List<SelectOption> snoozeoptions = new List<SelectOption>(); 
    snoozeoptions.add(new SelectOption('Name','Name')); 
    snoozeoptions.add(new SelectOption('All fields','All fields')); 
    return snoozeoptions ;
 }
    public String getResults() {
        return null;
    }


    public String getSnooze() {
        return null;
    }


}