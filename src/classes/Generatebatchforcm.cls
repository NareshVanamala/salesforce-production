global class Generatebatchforcm implements Database.Batchable<SObject>,Database.Stateful
{
  
/*global String Query;  
global CallList__c call;
global String finalstr;

global Generatebatchforcm(Id clId)  
{  
  system.debug('Welcome to Batch apex method...');
  List<Automated_Call_List__c> callistname = [Select Id, Name From Automated_Call_List__c Where Id =: clId];
  Automated_Call_List__c acl = [Select id, name from Automated_Call_List__c where name =: callistname[0].Name];
  call=[select id,Name,CampaignName__c, CampaignName__r.id,Contact_Type__c,Field_API__c, Field_API1__c, Field_API2__c,Field_API3__c,Field_API4__c,Operator__c,Operator1__c,Operator2__c,Operator3__c,Operator4__c,Value__c,Value1__c,Value2__c,Value3__c,Value4__c from  CallList__c  where name =: acl.Name];       
} */ 
global Database.QueryLocator start(Database.BatchableContext BC)  
{  
   /*String condn1=null;
    String condn2=null;
    String condn3=null;
    String condn4=null;
    String condn5=null;
    string cond1;
    list<Contact>conlist= new list<Contact>();  
    list<Contact_Call_List__c >contactcalllist=new list<Contact_Call_List__c >();  
    String conquery = 'select Id,ContactId,CampaignId,Contact.Territory__c,Contact.Name from CampaignMember';
        System.debug('*******:QUERY:********\n'+conquery);
   
        GerateDynamicQueryforcm generateQuery =  new GerateDynamicQueryforcm();
        if(call.Field_API__c!=null && call.Field_API__c!='' && call.Operator__c!='' && call.Operator__c!=null)
        condn1= generateQuery.conditionquery(call.Field_API__c,call.Operator__c,call.Value__c);
        if(call.Field_API1__c!=null && call.Field_API1__c!='' && call.Operator1__c!='' && call.Operator1__c!=null)
        condn2= generateQuery.conditionquery(call.Field_API1__c,call.Operator1__c,call.Value1__c);
        if(call.Field_API2__c!=null && call.Field_API2__c!='' && call.Operator2__c!='' && call.Operator2__c!=null)
        condn3= generateQuery.conditionquery(call.Field_API2__c,call.Operator2__c,call.Value2__c);
        if(call.Field_API3__c!=null && call.Field_API3__c!='' && call.Operator3__c!='' && call.Operator3__c!=null)
        condn4= generateQuery.conditionquery(call.Field_API3__c,call.Operator3__c,call.Value3__c);
        if(call.Field_API4__c!=null && call.Field_API4__c!='' && call.Operator4__c!='' && call.Operator4__c!=null)
        condn5= generateQuery.conditionquery(call.Field_API4__c,call.Operator4__c,call.Value4__c);
        
        If(condn1 != null  && call.Field_API__c != null)  
            conquery=conquery+' where '+condn1;
        if( condn2 != null && call.Field_API1__c != null)
            conquery=conquery+' AND '+ condn2;
        if( condn3 != null && call.Field_API2__c != null)
            conquery=conquery+' AND '+condn3;
        if(condn4 != null && call.Field_API3__c != null)
            conquery=conquery+' AND '+condn4;
        if(condn5 != null && call.Field_API4__c != null)
            conquery=conquery+' AND '+condn5;              

        If(condn1 != null  && call.Field_API__c != null)
          conquery=conquery+' AND ' + 'CampaignId =' + '\''+call.CampaignName__c+'\'';
        else
          conquery=conquery+' Where ' + 'CampaignId =' + '\''+call.CampaignName__c+'\'';
                
            Query= conquery;
                    
                 
  System.debug('*******:QUERY:********\n'+Query);
  system.debug('This is start method');
  system.debug('Myquery is......'+Query);
  finalstr = 'CallListName , ContactIds , Territory , Name\n';
  return Database.getQueryLocator(query); */
  return null; 

}  
global void execute(Database.BatchableContext BC,List<CampaignMember> scope)  
{  
   /*system.debug('Check the name of the call list name:'+call);
   List<contact_call_list__c> callist = new List<contact_call_list__c>();

   for(CampaignMember c : scope) {
        contact_call_list__c cls = new contact_call_list__c();
        cls.CallList_Name__c = call.name ;
        cls.Contact__c = c.ContactId;
        cls.campId__c = call.CampaignName__c;
        callist.add(cls);
        string recordString = call.Name +','+c.ContactId+','+ c.Contact.Territory__c +','+ c.Contact.Name +'\n';
        finalstr = finalstr + recordString;
   }
   system.debug('&&&&&&&&&' + callist.size());
   System.debug('!!!!!!!' + finalstr);*/

}

global void finish(Database.BatchableContext BC)  
{ 
    /*System.debug('!!!!!!!finishfinal' + finalstr);
    AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
    Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
    blob csvBlob = Blob.valueOf(finalstr);
    string csvname= 'Report.csv';
    csvAttc.setFileName(csvname);
    csvAttc.setBody(csvBlob);
    Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
    String[] toAddresses = new String[] {a.CreatedBy.Email};   
    String[] bccAddresses = new String[] {'ntambe@colecapital.com','ksingh@colecapital.com'};
    String subject ='Contact CSV';
    email.setSubject(subject);
    email.setBccSender(true);
    email.setToAddresses( toAddresses );
    email.setBccAddresses(bccAddresses);
    email.setPlainTextBody('Contact CSV ');
    email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
    Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});*/
}
}