global with sharing class getEmployeesDetails {
 
   private integer counter=0; 
   private integer list_size=6;
   public integer total_size;
   public Employee__c objEditEmployee {get;set;}
   public Boolean isShow {get;set;}
//   public Marquee__c Marquee {get;set;}   
   
   public String empID { get; set; }
   public list<Employee__c> EmployeeVal { get; set; }
   public getEmployeesDetails() {
   total_size = [select count() from Employee__c]; 
   }
   
   public getEmployeesDetails(cms.CoreController controller) 
    {
     total_size = [select count() from Employee__c]; 
      //    objEditEmployee = [select id, Name, Title__c, Department__c,  Work_Email__c,Work_Wireless__c, Image__c, Telephone__c,  Employee_Office_location__c, EmployeeDetails__c, DepartmentObj__r.Description__c, DepartmentObj__r.Name from Employee__c where Employee_Status__c='On Board' order by id LIMIT 1];
      //Marquee = [select Name, Description__c from Marquee__c where Status__c=true];
     //Marquee = [select Name, Description__c from Marquee__c];
        User currentUser = new User();
        id loggedinuserid= UserInfo.getUserId();
        currentUser = [Select id,Ops_User__c from User where id=:loggedinuserid limit 1];
        if(currentUser!=null){
            isShow = currentUser.Ops_User__c;
        }
    }
     
   public Employee__c[] getEmployees() {
         try
         {
             List<Employee__c> Employee = new List<Employee__c>();
             Employee = [select id, Name, Title__c, Department__c, DepartmentObj__r.Name from Employee__c where id!='' AND Employee_Status__c ='On Board' AND Employee_Type__c = 'Full Time Employee' order by Name  ];//EmployeeDetails__c
             System.debug('employess'+Employee);
             System.debug('employess'+Employee.size());
             return Employee;
             
          } 
          catch (QueryException e) 
          {
        
              return null;
          }
   }
 
  /* public PageReference Beginning() { counter = 0;  return null; }
   public PageReference Previous() { counter -= list_size; return null; }
   public PageReference Next() { counter += list_size; return null; }
   public PageReference End() { counter = total_size - math.mod(total_size, list_size); return null; }
   */
 
   public Boolean getDisablePrevious() { 
      if (counter>0) return false; else return true;
   }
 
   public Boolean getDisableNext() { 
      if (counter + list_size < total_size) return false; else return true;
   }
 
   public Integer getTotal_size() { return total_size; }
   public Integer getPageNumber() { return counter/list_size + 1; }
   public Integer getTotalPages() { if (math.mod(total_size, list_size) > 0) { return total_size/list_size + 1; } else { return (total_size/list_size); } }
 
   

 @RemoteAction
   global static List<Employee__c> getEmployeeEvent(String empID) 
   {
//      return [select  id, Name, Title__c, Department__c,  Work_Email__c,Work_Wireless__c, Image__c, Telephone__c,  Employee_Office_location__c, EmployeeDetails__c, DepartmentObj__r.Description__c, DepartmentObj__r.Name from Employee__c where id=: empID];// 'a3P5000000000OMEAY' ];
         return [select  id, Name, Title__c, Department__c,  Work_Email__c,Work_Wireless__c, Image__c, Telephone__c,Start_Date__c, Employee_Office_location__c, EmployeeDetails__c, DepartmentObj__r.Description__c, DepartmentObj__r.Name from Employee__c where id=: empID];// 'a3P5000000000OMEAY' ];
   } 
   
}