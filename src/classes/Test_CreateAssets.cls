@isTest(seealldata=true)
private class Test_CreateAssets
{ 
    static testmethod void myTestMethod1()
    {
    
    User U=[select id , name from user limit 1];
    Asset_Inventory__c ct= new Asset_Inventory__c();
    ct.name='Testing'; 
    ct.Asset_Type__c='Software';   
    ct.Application_Owner__c=u.id; 
    ct.Application_Implementor__c=u.id;
    insert ct;
    
    system.assertequals(ct.Application_Implementor__c,u.id);

    
    User u2= [select id, name from user where id=:Userinfo.getuserid()];      
    
    ct.Application_Owner__c=u2.id; 
    update ct;
    
        system.assertequals(ct.Application_Owner__c,u2.id);


    }
}