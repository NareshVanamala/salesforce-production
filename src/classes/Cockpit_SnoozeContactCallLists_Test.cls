@isTest
public class Cockpit_SnoozeContactCallLists_Test 
{
     static testmethod void myTest_Method1()
    {
       Test.StartTest();
        Contact cockpitcontact1= new Contact();
         cockpitcontact1.firstname='cockpitcontact1FirstName';
         cockpitcontact1.lastname='cockpitcontact1lastName';
         cockpitcontact1.Territory__c='HeartLand';
         cockpitcontact1.Phone='4802375787'; 
         insert cockpitcontact1;
         
         system.debug(cockpitcontact1);
         System.assert(cockpitcontact1!= null);
             
         Contact cockpitcontact11= new Contact();
         cockpitcontact11.firstname='cockpitcontact11FirstName';
         cockpitcontact11.lastname='cockpitcontact11lastName';
         cockpitcontact11.Territory__c='Chesapeake';
         cockpitcontact11.Phone='4802375797'; 
         insert cockpitcontact11;   
         system.debug(cockpitcontact11); 
         System.assert(cockpitcontact11!= null); 
         
         Automated_Call_List__c CockpitAutomatedCalllist= new Automated_Call_List__c();
         CockpitAutomatedCalllist.name='CockpitAutomatedcallListName';
         CockpitAutomatedCalllist.Type_Of_Call_List__c='Campaign Call List';
         insert CockpitAutomatedCalllist;
         System.assert(CockpitAutomatedCalllist!= null);
       
         Automated_Call_List__c CockpitAutomatedCalllist1= new Automated_Call_List__c();
         CockpitAutomatedCalllist1.name='CockpitAutomatedcallListName1';
         CockpitAutomatedCalllist1.Type_Of_Call_List__c='Campaign Call List';
         insert CockpitAutomatedCalllist1;
         System.assert(CockpitAutomatedCalllist1!= null);
       
         Contact_Call_List__c cockpitCCL=new Contact_Call_List__c();
         cockpitCCL.Automated_Call_List__c=CockpitAutomatedCalllist.id;
         cockpitCCL.contact__c=cockpitcontact1.id;
         insert cockpitCCL;
         system.debug(cockpitCCL);
         System.assert(cockpitCCL!= null);
       
         Contact_Call_List__c cockpitCCL1=new Contact_Call_List__c();
         cockpitCCL1.Automated_Call_List__c=CockpitAutomatedCalllist1.id;
         cockpitCCL1.contact__c=cockpitcontact1.id;
         insert cockpitCCL1;
         system.debug(cockpitCCL1);    
         System.assert(cockpitCCL1!= null);
       
        list<string>mycids=new list<string>();
        mycids.add(cockpitCCL.id);
        mycids.add(cockpitCCL1.id);  
        datetime snoozedatetime=system.now();
       
       Cockpit_SnoozeContactCallLists batch = new Cockpit_SnoozeContactCallLists(mycids,snoozedatetime);
       Id batchId = Database.executeBatch(batch);
       
       
    }
  
  
  
}