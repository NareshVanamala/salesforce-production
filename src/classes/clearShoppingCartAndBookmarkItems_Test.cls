@isTest(SeeAllData=true)
public class clearShoppingCartAndBookmarkItems_Test{

static testmethod void clearTestMethod(){
     list<contact> conList = new list<contact> ();
     conList = [select id,Accountid,mailingstate,RecordType.Name,Name from contact limit 1];
     String contactId = conList[0].id;
     list<id> contIds= new list<id>(); 
     contIds.add(contactId);
     Forms_Literature_List__c literatureList = new Forms_Literature_List__c();
     literatureList.Contact__c = contactId;
     literatureList.List_Name__c ='Testing ListName';
     insert literatureList;
     Forms_Literature_Shopping_Cart__c  cartObj = new Forms_Literature_Shopping_Cart__c();
     cartObj.Title__c = 'title';                   
     cartObj.Description__c = 'Description';
     cartObj.Contact__c = contactId;
     cartObj.Download_File_URL__c = 'Download File';                   
     cartObj.Max_Order__c = 20;
     cartObj.Published_Date__c = '06/01/2016';                     
     cartObj.Expiration_Date__c = '06/01/2016';
     cartObj.FINRA_PDF__c = 'Finra PDF';
     //cartObj.Forms_Literature_List__c = literatureList.id;
     cartObj.External_Id__c = contactId;   
     insert cartObj;
     
     System.assertNotEquals(cartObj.Title__c,cartObj.Description__c);

     clearShoppingCartAndBookmarkItems.deleteShoppingCartAndBookmarkItems(contIds);
  
}
}