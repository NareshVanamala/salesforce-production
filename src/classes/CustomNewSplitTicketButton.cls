public with sharing class CustomNewSplitTicketButton 
{  
    private ApexPages.StandardController controller;
    public REIT_Investment__c objreit{get;set;}
    public SplitTickets__c spt{get;set;}
    public Contact con{get;set;}
    public Boolean refreshPage {get; set;}
    
    public PageReference showRep()
    {
        REIT_Investment__c objret = (REIT_Investment__c )controller.getRecord();
        objreit = [select id,Name,Rep_Contact__c,Rep_Contact__r.AccountId,Rep_Contact__r.Split_Profile__c,reitsplitcontact__c from REIT_Investment__c  where Id =:objret.id];
        system.debug('objreit-------------save---------------'+objreit);
        if(objreit !=null)
        {
            if(objreit.reitsplitcontact__c ==null)
            {
                PageReference mypage=new PageReference('/'+objreit.Id); 
                mypage.setRedirect(true);
                return mypage;  
            }
        }
        return null;
    }
    
    public CustomNewSplitTicketButton(ApexPages.StandardController controller) 
    {
        this.controller = controller;
        REIT_Investment__c objret = (REIT_Investment__c )controller.getRecord();
        spt=new SplitTickets__c();
        con = new Contact();
        if(objret !=null)
        objreit = [select id,Name,Rep_Contact__c,Rep_Contact__r.AccountId,Rep_Contact__r.Split_Profile__c,reitsplitcontact__c from REIT_Investment__c  where Id =:objret.id];
        system.debug('objreit----------------------------'+objreit);
        if(objreit !=null)
    spt.Rep__c= objreit.reitsplitcontact__c;
        spt.REIT_Investment__c  = objreit.Id;
        refreshPage=false;
        showRep();
    }
    public pagereference saveAndNew() 
    {
        try{
            
            if (Schema.sObjectType.SplitTickets__c.isCreateable())
            insert spt;
            system.debug('spt-----'+spt);
            objreit.reitsplitcontact__c =null;
            system.debug('objreit---1--'+objreit);
            if (Schema.sObjectType.REIT_Investment__c.isUpdateable()) 
            update objreit;
            system.debug('spt-----'+spt);
            system.debug('objreit-----'+objreit);
            Contact conObjec = new Contact();
            conObjec.Id = objreit.Rep_Contact__c;
            conObjec.Split_Profile__c = true;
            system.debug('conObjec-----'+conObjec);
            if (Schema.sObjectType.Contact.isUpdateable()) 
            update conObjec;  
            //REIT_Investment__c  objRep = new REIT_Investment__c();
            //objRep.Id = objreit.Id;
            spt.REIT_Investment__c  = objreit.Id;
            spt = new SplitTickets__c();             
           }
         catch(exception e){
         }
      PageReference p =new PageReference('/'+objreit.Id);
       refreshpage= True;
       return p; 
    }
 
    public pagereference CancelAndReturn() 
    {
        PageReference cancel = controller.cancel();
       controller.save();
       return cancel;
     
    }
    public PageReference lookupRedirect() 
    {
        return null;
    }

}