public with sharing class ERPendingExcelClass {
    public transient List<CampaignMember> campPendList{get;set;}
    public transient List<CampaignMember> campApprList{get;set;}
    public transient List<CampaignMember> campDeclList{get;set;}
    public String sMail{get;set;}
    public String calllist{get;set;}
    public date startdate{get;set;}
    public date enddate{get;set;}
    public List<Event_Automation_Request__c> earList{get;set;}
    set<id> earcampid = new set<id>();
    public ERPendingExcelClass()
    {
        List<Event_Automation_Request__c> earList = new List<Event_Automation_Request__c>();
        sMail=ApexPages.CurrentPage().getParameters().get('emid');
        calllist=ApexPages.CurrentPage().getParameters().get('calllist');
        for(Event_Automation_Request__c e:[Select Name,RecordTypeId,OwnerId,ID,Event_Type__c,Start_Date__c,End_Date__c,Event_Name__c,Event_Description__c,Broker_Dealer_Name__r.Name,Campaign__r.Name,Broker_Dealer_Name__r.Event_Approver__r.Email,Broker_Dealer_Name__r.Event_Approver__r.Name,Comments__c from Event_Automation_Request__c where Campaign__c!=null AND Event_Name__c=:calllist])
        {
            system.debug('>>>>>>>>>>>>>>>>>>eeeeeeeeeeeeeeeeeeeee>>>>>>>>>>>>>>>>'+e);
            earList.add(e);
            //added by sandeep
            earcampid.add(e.campaign__c);
        }
        //Modified by sandep
            campPendList=[select id, RecordTypeId, CampaignId ,Contact.FirstName,Contact.name, Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c,Broker_Dealer_Name__c ,contact.account.name from CampaignMember where CampaignId IN :earcampid and status='Pending' and Contact.Account.Event_Approver__r.email=:sMail ORDER BY contact.name];
            //campPendList=[select id from CampaignMember limit 10];
            campApprList=[select id, RecordTypeId, CampaignId ,Contact.FirstName,Contact.name, Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c,Broker_Dealer_Name__c,contact.account.name from CampaignMember where CampaignId IN :earcampid and status='Approved' and Contact.Account.Event_Approver__r.email=:sMail ORDER BY contact.name];
            campDeclList=[select id, RecordTypeId, CampaignId ,Contact.FirstName,Contact.name, Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c,Broker_Dealer_Name__c,contact.account.name from CampaignMember where CampaignId IN :earcampid and status='Declined' and Contact.Account.Event_Approver__r.email=:sMail ORDER BY contact.name];
        
        startdate=earList[0].Start_Date__c;
        enddate=earList[0].End_Date__c;        
    }
}