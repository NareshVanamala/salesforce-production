@isTest
private class Test_ERHomeClass
{

  public static testMethod void testCheckPendingMembers() 
  { 
     
     Account acc = new Account();
      acc.Name = 'Test Prospect Account';      
      Insert acc;

        Contact c = new Contact();
        c.firstname='RSVP';
        c.lastname='test';
        c.accountid=acc.id;
        c.MailingCity='testcity';
        c.MailingState='teststate';
        c.email='test@mail.com';
        insert c;

        Contact ct = new Contact();
        ct.firstname='Attend';
        ct.lastname='testing';
        c.accountid=acc.id;
        c.MailingCity='testcity';
        c.MailingState='teststate';
        ct.email='skalamkar@colecapital.com';
        insert ct;
        //Creating one more account...
       
        system.assertequals(c.accountid,acc.id);

        acc.Event_Approver__c=ct.id;
        update acc;
        User us = [SELECT Id FROM User limit 1];  

        Campaign cam = new Campaign();
        cam.name='Dinearound-National Conference-Cole Capital';
        cam.status='Pending';
        cam.status='Approved';
        cam.status='Rejected';
        cam.isActive=true;
        cam.parentid=null;
        insert cam;

        //lsttask1=[select id, contactid,CampaignId ,Contact.FirstName,Contact.name,contact.accountid,Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c  from CampaignMember where  CampaignId =:CalllistEvent.Campaign__r.id and status='Pending' and contact.Account.Event_Approver__c!=null limit 2000];

        Event_Automation_Request__c ear = new Event_Automation_Request__c();
        ear.Name='BDE13006';
        ear.Event_Name__c='testeventname';
        ear.Start_Date__c=System.Today();
        ear.End_Date__c=System.Today();
        ear.Broker_Dealer_Name__c=acc.id;
        ear.Event_Location__c='testlocation';
        ear.Key_Account_Manager_Name_Requestor__c=us.id;
        ear.Event_Venue__c='testvenue';
        ear.Campaign__c=cam.id;
        insert ear;

        system.assertequals(ear.Broker_Dealer_Name__c,acc.id);

        CampaignMember member= new CampaignMember();
        member.CampaignId = cam.id;
        member.ContactId = c.id;
        member.Status='Pending';
        insert member; 

        CampaignMember member1= new CampaignMember();
        member1.CampaignId = cam.id;
        member1.ContactId = ct.id;
        member1.Status='Pending';
        insert member1;

        system.assertequals(member1.ContactId,ct.id);

        ERHomeClass cttt= new ERHomeClass();  
        cttt.sMail='skalamkar@colecapital.com';  
        cttt.loginCheck();  
   


  }

}