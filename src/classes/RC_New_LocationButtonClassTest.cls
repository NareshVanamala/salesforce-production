@isTest
private class RC_New_LocationButtonClassTest
{
    static testmethod void test1() 
    {
         RC_Property__c PRT=new RC_Property__c();
         PRT.name='Amazon DC 2014 USAA~A11123';
         PRT.City__c='Hyderabad';
         PRT.Occupancy__c='Single-Tenant';
         PRT.Entity_Tenant_Code__c='12345';
        // PRT.Total_Square_Feet__c=789990;
         PRT.Insurable_Value__c=48789890;
         PRT.Known_Name__c='Amazon DC 2014 USAA~A11123';
         insert PRT;
         
         Account a= new Account();
         a.name='TestAccount';
         insert a;
        
         
         RC_Location__c LC=new RC_Location__c();
         LC.name='Amazon DC 2014 USAA~A11123';
         LC.Entity_Tenant_Code__c='12345';
         //LC.City__c='Hyderabad';
         //LC.Property__c=PRT.id;
         LC.Account__c=a.id;
        // insert LC;
        
        System.assertEquals(PRT.name,LC.name);

        string url1=System.Label.Current_Org_Url+'/a5F/e?nooverride=1&retURL=%2Fa5F%2Fo';
        PageReference acctPage = new PageReference(Url1);
        Test.setCurrentPage(acctPage); 
        ApexPages.StandardController controller = new ApexPages.StandardController(LC);
        RC_New_LocationButtonClass csr=new RC_New_LocationButtonClass(controller);
        csr.Gobacktostandardpage();
        
         
       
         RC_Location__c LC1=new RC_Location__c();
         LC1.name='Amazon DC 2014 USAA~A11123';
         LC1.Entity_Tenant_Code__c='12345';
         //LC.City__c='Hyderabad';
         LC1.Property__c=PRT.id;
         LC1.Account__c=a.id;
        // insert LC;
          String Url2=System.Label.Current_Org_Url+'/a5F/e?nooverride=1&CF00NR0000001JRYN_lkid='+PRT.id+'&CF00NR0000001JRYN='+PRT.Name+'&00NR0000001JRY8='+PRT.Entity_Tenant_Code__c+'&Name='+PRT.Name+'&00NR0000001JRYV='+PRT.Known_Name__c+'&00NR0000001JRYR='+PRT.Total_Square_Feet__c+'&00NR0000001JRYO='+PRT.Insurable_Value__c+'&retURL=%2F'+PRT.id+'';        

        PageReference acctPage1 = new PageReference(Url2);
        Test.setCurrentPage(acctPage1); 
        ApexPages.StandardController controller1 = new ApexPages.StandardController(LC1);
        RC_New_LocationButtonClass csr1=new RC_New_LocationButtonClass(controller1);
        csr1.Gobacktostandardpage();  
        
        
         RC_Property__c PRT2=new RC_Property__c();
         PRT2.name='Amazon DC 2014 USAA~A11123';
         PRT2.City__c='Hyderabad';
         PRT2.Occupancy__c='BTS';
         PRT2.Entity_Tenant_Code__c='12345';
        // PRT.Total_Square_Feet__c=789990;
         PRT2.Insurable_Value__c=48789890;
         PRT2.Known_Name__c='Amazon DC 2014 USAA~A11123';
         insert PRT2;
         
         RC_Location__c LC11=new RC_Location__c();
         LC11.name='Amazon DC 2014 USAA~A11123';
         LC11.Entity_Tenant_Code__c='12345';
         //LC.City__c='Hyderabad';
         LC11.Property__c=PRT2.id;
         LC11.Account__c=a.id;
        // insert LC;
        string  Url12= System.Label.Current_Org_Url+'/a5F/e?nooverride=1&CF00NR0000001JRYN='+PRT2.Name+'&CF00NR0000001JRYN_lkid='+PRT2.id+'&retURL=%2F'+PRT2.id+'';

        PageReference acctPage11 = new PageReference(Url12);
        Test.setCurrentPage(acctPage11); 
        ApexPages.StandardController controller12 = new ApexPages.StandardController(LC11);
        RC_New_LocationButtonClass csr111=new RC_New_LocationButtonClass(controller12);
        csr111.Gobacktostandardpage();  
        
        
         
         
     }
     
   }