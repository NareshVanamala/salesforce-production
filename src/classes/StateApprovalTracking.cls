public class StateApprovalTracking {

  public static Id adminId = '00550000001j5ty';
  public static void trackChanges(List<Case> newList, Map<Id,Case> oldMap) {
  
    List<Schema.FieldSetMember> trackedFields = SObjectType.Case.FieldSets.State_Approval_History.getFields();
    if (trackedFields.isEmpty()) return;
    
    List<State_Approval_History__c> fieldChanges = new List<State_Approval_History__c>();
    for (Case sNew : newList) {
      Case sOld = oldMap.get(sNew.Id);
      for (Schema.FieldSetMember fsm : trackedFields) {
      
        String fieldName  = fsm.getFieldPath();
        String fieldLabel = fsm.getLabel();
        
        if (sNew.get(fieldName) != sOld.get(fieldName)) {
        
          String oldValue = String.valueOf(sOld.get(fieldName));
          String newValue = String.valueOf(sNew.get(fieldName));
          if (oldValue != null && oldValue.length()>255) oldValue = oldValue.substring(0,255);
          if (newValue != null && newValue.length()>255) newValue = newValue.substring(0,255);
          
          State_Approval_History__c uft = new State_Approval_History__c();
          uft.State__c     = fieldLabel;
          uft.Case__c      = sNew.Id;
          uft.Approved_By__c = sNew.LastModifiedById;
          uft.Old_Value__c  = oldValue;
          uft.New_Value__c  = newValue;
          fieldChanges.add(uft);
        }        
      }
    }
    if (!fieldChanges.isEmpty()) {
      String jsonString = Json.serialize(fieldChanges);
      StateApprovalTracking.storeCaseTracking(jsonString);
    }
  }
  @future
  public static void storeCaseTracking(String jsonString){
    Type pType = List<State_Approval_History__c>.class;
    List<State_Approval_History__c> fieldHistory = (List<State_Approval_History__c>) JSON.deserialize(jsonString, pType);
    
    if (Schema.sObjectType.State_Approval_History__c.isCreateable())

    insert fieldHistory;
  } 
}