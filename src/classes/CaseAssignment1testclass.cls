@isTest
public class CaseAssignment1testclass{
static testMethod void  CaseAssignment1testclass() {
User u=[select id,name,email from user where id=: UserInfo.getUserId()];
Contact c= new Contact();
c.firstname='test';
c.lastname='test last';
c.Wholesaler__c=u.name;

insert c;


Contact c1= new Contact();
c1.firstname='test';
c1.lastname='test last';
c1.Wholesaler__c='test test1';
insert c1;
//case testcase=[select casenumber from case limit 1];

System.assertnotEquals(c.Wholesaler__c,c1.Wholesaler__c);  

Case case1= new Case(Reason='Report Request',SuppliedEmail='xyz@test.com',Status='New',Topic__c='Address',subject='sub',description='The information contained in this e-mail');
case1.Origin='Email';
insert case1;
System.debug('************************' + case1.description);
case1.Response__c ='abcd';
case1.Priority='High';
case1.Email_Sent__c='email sent';
case1.description='This e-Mail';
update case1;
case1.Target_Date__c=system.today();
case1.Contactid=c1.id;
case1.description='The information contained in this e-mail ';
update case1;


//insert case2;
Task t = new Task();
t.Whatid= case1.id;
t.status='Not Started';
t.Priority='Normal';
t.Whoid=c1.id;
t.Ownerid=u.id;
insert t;
t.ActivityDate=system.today();
update t;

t.status='Completed';
t.subject='Call';
update t;

System.assertnotEquals(case1.description,t.status);  

Attachment a=[select id,name,parentid,LastModifiedDate,Parent.name,body  from attachment limit 1];
Attachment a1=new Attachment();
a1.parentid=case1.id;
a1.name='test';
a1.body=a.body;
insert a1;

CaseComment com = new CaseComment();
com.parentid=case1.id;
com.CommentBody='test';
insert com;
System.assertEquals(com.CommentBody,'test');  

}
static testMethod void  CaseAssignment1testclass1() {
Case case2= new Case(Reason='Report Request',SuppliedEmail='abc@test.com',Status='New',Topic__c='Address',subject='sub',description='The information contained in this e-mail');
case2.Origin='Email';
insert case2;
//case2.contactid= '';
case2.Target_Date__c=system.today();

update case2;
System.assertEquals(case2.Origin,'Email');  


}


}