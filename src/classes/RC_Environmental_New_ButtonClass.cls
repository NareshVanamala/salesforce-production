public with sharing class RC_Environmental_New_ButtonClass
{
    public String recortypeid{get;set;}
    public String pid{get;set;}
    public  RC_Environmental__c mp{get;set;}
    public  RC_Environmental__c ProjectObj{get;set;}
    public RC_Location__c ppc{get;set;}
    
  Public RC_Environmental_New_ButtonClass(Apexpages.StandardController controller)
  {
     //recortypeid=ApexPages.CurrentPage().getParameters().get('RecordType');
     mp=(RC_Environmental__c)controller.getRecord();
     pid=mp.id;
   }
  
   public PageReference Gobacktostandardpage() 
   {  
     if((mp.Location__c==null))
     {
       //string url=System.Label.Current_Org_Url+'/a5E/e?nooverride=1&retURL=%2Fa5E%2Fo';
       string url=System.Label.Current_Org_Url+'/a4Y/e?nooverride=1&retURL=%2Fa4Y%2Fo';
       system.debug('Url'+Url); 
      PageReference acctPage = new PageReference(Url);
       acctPage.setRedirect(true); 
       return acctPage; 
     }  
     else if((mp.Location__c!=null))
     {
        
         system.debug('Check the Location'+mp.Location__c);
         string bid1;
         string bid=mp.Location__c;
         system.debug('Check the Location'+bid);
         ppc=[select id, Name,Account__c,Account__r.name from RC_Location__c where id=:bid];
         if(ppc!=null)
         id locid=ppc.id;
         bid1=ppc.Name;
         string accountid=ppc.Account__c;
         String accountName=ppc.Account__r.name;
         accountName=accountName.replaceAll('&','%26');
         String s2 = bid1.replaceAll('&', '%26');
        //String Url1=System.Label.Current_Org_Url+'/a5E/e?nooverride=1&CF00NR0000001JRXX_lkold='+bid+'&CF00NR0000001JRXX='+s2+'&CF00NR0000001JRX8_lkold='+accountid+'&CF00NR0000001JRX8='+accountName+'&retURL=%2F'+ppc.id+'';
          String Url1=System.Label.Current_Org_Url+'/a4Y/e?nooverride=1&CF00N50000003Lznv_lkid='+bid+'&CF00N50000003Lznv='+s2+'&CF00N50000003LznW_lkid='+accountid+'&CF00N50000003LznW='+accountName+'&retURL=%2F'+ppc.id+'';

         PageReference locPage= new PageReference(Url1);
         locPage.setRedirect(true); 
         return locPage; 
     }
     return null;
  }
  
}