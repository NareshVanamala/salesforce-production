global with sharing class Cockpit_updateTerritoryManagersOnContact{

   @InvocableMethod
   public static void Cockpit_updateTerritoryManagersOnContact(list<string>contactIds){
  
       list<string>excludeUserProfiles=new list<string>();
       list<String>UserNamesList=new list<String>();
       list<User>ActiveUserlist=new list<User>();
       set<String>UserNameSet=new set<String>(); 
       Map<String,id>mapUserNameToId=new Map<String,id>();
       list<contact>updateContactlist = new list<contact>();
       list<contact>contactList = new list<contact>();
       system.debug('contactIds'+contactIds);
       contactList = [Select Id,Name,Wholesaler__c,Sales_Director__c,Internal_Wholesaler__c,Internal_Wholesaler1__c,Regional_Territory_Manager__c,Regional_Territory_Manager1__c from Contact where id in:contactIds]; 
       system.debug('contactList'+contactList);
       for(contact con:contactList){
           UserNamesList.add(con.Wholesaler__c);
           UserNamesList.add(con.Internal_Wholesaler__c);
           UserNamesList.add(con.Regional_Territory_Manager__c);
       }
       UserNameSet.addAll(UserNamesList);
    
       excludeUserProfiles=system.label.Cockpit_Exclude_User_Profiles.split(';');
       ActiveUserlist=[select id, name from User where name in:UserNameSet and IsActive=true and profile.name not in:excludeUserProfiles];
       if(ActiveUserlist.size()>0){
          for(User usr:ActiveUserlist)
              mapUserNameToId.put(usr.name,usr.id);
       }
     
       for(contact c:contactList){
           if((c.Wholesaler__c!=null)&&(mapUserNameToId.get(c.Wholesaler__c)!=null)&&(c.Wholesaler__c!='Not Assigned'))
               c.Sales_Director__c=mapUserNameToId.get(c.Wholesaler__c);
           else 
               c.Sales_Director__c=null; 
           if((c.Internal_Wholesaler__c!=null)&&(mapUserNameToId.get(c.Internal_Wholesaler__c)!=null)&&(c.Internal_Wholesaler__c!='Not Assigned'))
               c.Internal_Wholesaler1__c=mapUserNameToId.get(c.Internal_Wholesaler__c);
           else 
               c.Internal_Wholesaler1__c=null;
           if((c.Regional_Territory_Manager__c!=null)&&(mapUserNameToId.get(c.Regional_Territory_Manager__c)!=null)&&(c.Regional_Territory_Manager__c!='Not Assigned'))
               c.Regional_Territory_Manager1__c =mapUserNameToId.get(c.Regional_Territory_Manager__c);
           else 
               c.Regional_Territory_Manager1__c=null;
             
           updateContactlist.add(c); 
            
                                 
         }
          
         if(updateContactlist.size()>0){
            if(Schema.sObjectType.contact.isUpdateable())
               update updateContactlist;
         }
    
}
}