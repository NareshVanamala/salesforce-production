@isTest(seeAllData=true)
public class WebPropertitesLocationCalloutsTest{
public static testmethod void testSetGeoLocationTrigger()
{
    Web_Properties__c tm =  new Web_Properties__c();
    tm.Name    = 'test name121';
    tm.Address__c = '1600 Amphitheatre Parkway';
    tm.City__c = 'Mountain View';
    tm.State__c    = 'CA';
    tm.Country__c ='USA';
    tm.Latitude_del__c=36.5864546;
    tm.Longitude_del__c=-108.6078704;
    tm.Location__Latitude__s=36.5864546;
    tm.Location__Longitude__s=-108.6078704;
    tm.Related_Resources__c='testing';
    insert tm;
    system.assertequals(tm.City__c,'Mountain View');
    tm.Country__c ='US';
    tm.Property_Id__c='0806123456';
    tm.Related_Resources__c='testing123456';
    update tm;
    system.assertequals(tm.Property_Id__c,'0806123456');

    
}
public static testmethod void testLocationCallout() {
try{
set<id> tmo = new set<id>();
    MRI_PROPERTY__c  mo = new MRI_PROPERTY__c () ;
        mo.Address__c ='2450 Atlanta Hwy Ste 703' ;
        mo.City__c='Atlanta';
        mo.Zip_code__c='30040';
        mo.State__c='GA';
        mo.Country__c= 'USA';  
        mo.Name ='Name' ;
        mo.AvgLeaseTerm__c  = 12 ;
        mo.Long_Description__c  = 'test' ; 
        mo.Web_Ready__c='Approved';
        mo.status__c='Owned';
        mo.Program_Id__c='ARCP';
        mo.Property_ID__c='PL1234789'; 
       // mo.Related_Resources__c='Testing';
        insert mo ; 
    Web_Properties__c tm =  new Web_Properties__c();
    tm.Name    = 'test name';
    tm.Address__c = '1600 Amphitheatre Parkway';
    tm.City__c = 'Mountain View';
    tm.State__c    = 'CA';
    tm.Country__c ='USA';
    tm.Latitude_del__c=36.5864546;
    tm.Longitude_del__c=-108.6078704;
    tm.Location__Latitude__s=36.5864546;
    tm.Location__Longitude__s=-108.6078704;
    //tm.Related_Resources__c='testing';
    insert tm;
    system.assertequals(tm.City__c,'Mountain View');

    tmo.add(tm.id);
     WebPropertitesLocationCallouts.getLocation(tmo);
     StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GeoCodeDummyResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        } 
        catch(Exception e)
        {
        }
//System.assertEquals(/*check for expected results here...*/);
}

}