public with sharing class addattachment {

    public addattachment(ERApprovalClass controller) {

    }
    
private integer removepos{get;set;}
public List<Attachment> deleteemelist1;
public id recid;
public list<attachment> selectedlist{get;set;}
public string attid{get;set;}
public list<attachment> selectedatt{get;set;}
//public List<mywrapper> deleteemelist;
//public list<mywrapper>  attachmentlist;   
public list<mywrapper> displaylist{get;set;}
public list<event_automation_request__c> list123 = new list<event_automation_request__c>();
public datetime d{get;set;}
public addAttachment(ApexPages.StandardController controller) 
{
        recId = ApexPages.currentPage().getParameters().get('id');
        deleteemelist1=new list<attachment>();
}    
    public list<mywrapper> getrecordstodisplay()
    {
    if(displaylist==null)
    {
    displaylist = new list<mywrapper>();
    for(Attachment a:[SELECT Id,Name,createddate,LastModifiedDate,CreatedBy.Name FROM Attachment WHERE ParentId =:recId ORDER BY LastModifiedDate DESC])
    {
    displaylist.add(new mywrapper(a));
    }
    }
    return displaylist;
    }

    public PageReference attPage()
    {   
        system.debug('>>>>>>>>>>>>>pageref>>>>>>>>>>>>>>>>>'+recId);
        
        return new PageReference('/apex/EARAttachments?id='+recId);
        //return null;        
    }
 
public pagereference selectedrecords()
    {
selectedatt=new list<attachment>();   
Event_automation_request__c ear=[select Id,Name,Testingfield__c,Event_Type__c,Start_Date__c,End_Date__c,Event_Name__c from Event_Automation_Request__c where id=:recId];     
     for(mywrapper ct:getrecordstodisplay())
     {
      if(ct.wchecked==true)
      {
        ct.wchecked=true;
        selectedatt.add(ct.att); 
      }
     }
     
     for(attachment at:selectedatt)
     {
     system.debug(at.name);
     ear.testingfield__c = ear.testingfield__c+','+at.id;
     }
     if (Schema.sObjectType.Event_automation_request__c .isUpdateable()) 
     update ear;
     displaylist=null;
     return null;
    }     
public list<attachment> getlist()
{
Event_automation_request__c er=[select Id,Name,Testingfield__c,Event_Type__c,Start_Date__c,End_Date__c,Event_Name__c from Event_Automation_Request__c where id=:recId];     
if(er.testingfield__c!=null){
string ab=er.testingfield__c.remove('null,');
list<id> eid = ab.split(',');
d=system.now();
list<attachment> selectedats = new list<attachment>();
selectedats=[SELECT Id,Name,createddate,LastModifiedDate,CreatedBy.Name FROM Attachment where Id in:eid ORDER BY Name];
selectedlist = new list<attachment>();
for(attachment ac:selectedats)
{
selectedlist.add(ac);
}
}
return selectedlist;
} 
 public Pagereference removeAtt(){
        //system.debug('>>>>>>>>>>>>>>>>>naList.size()>>>>>>>>>>>>>>>>'+displaylist.size());
        //system.debug('>>>>>>>>>>>>>>>>>naList>>>>>>>>>>>>>>>>'+displayList);
      if(displayList!=null)
      {
        if(displayList.size()>0)
        {        
            //string paramname= Apexpages.currentPage().getParameters().get('node');
            //system.debug('Check the paramname'+paramname);
            for(integer i=0;i<displaylist.size();i++)
            {
            if(displaylist[i].att.id==attid)
            {
            removepos=i;
            deleteemelist1.add(displaylist[i].att);
            }
            }
            if(removepos!=null) 
            {               
                system.debug('>>>>>>>>>>>>>>>>>removepos>>>>>>>>>>>>>>>>'+removepos);
                displaylist.remove(removepos);               
               
             }
       
       if(deleteemelist1.size()>0)
       {
           if (Contact.sObjectType.getDescribe().isDeletable())               
                 
           delete deleteemelist1;
    
       deleteemelist1.clear();
       }
       }
       }
      return null;
       } 
          
    
public class mywrapper{
public attachment att{get;set;}
public boolean wchecked{get;set;}
    public mywrapper(Attachment a)
    {
    this.att = a;
    this.wchecked = false;
    }

 }
}