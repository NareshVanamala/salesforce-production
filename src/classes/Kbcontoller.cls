global with sharing class Kbcontoller{


    public String callback { get; set; }

    public String sObjectType { get; set; }

   public Kbcontoller() {   }
   
   public Kbcontoller(cms.CoreController controller)     {    }
    
     @remoteAction 
    global static string getCategoriesJson(string sObjectType)
    {
        return JSON.serialize(Kbcontoller.getCategories(sObjectType));
    }


    Public static list<Schema.DescribeDataCategoryGroupStructureResult> getCategories(string sObjectType)
    {

        //the describing of categories requires pairs of sObject type, and category name. This holds a list of those pairs.
        list<Schema.DataCategoryGroupSObjectTypePair> pairs = new list<Schema.DataCategoryGroupSObjectTypePair>();

        //list of objects to describe, for this app we only take 1 sObject type at a time, as passed into this function.
        list<string> objects = new list<string>();
        objects.add(sObjectType);
        system.debug('objects'+objects);

        //describe the categories for this object type (knowledgeArticleVersion)
        List<Schema.DescribeDataCategoryGroupResult> describeCategoryResult =  Schema.describeDataCategoryGroups(objects);

        //add the found categories to the list.
        for(Schema.DescribeDataCategoryGroupResult s : describeCategoryResult)
        {
            Schema.DataCategoryGroupSObjectTypePair thisPair = new Schema.DataCategoryGroupSObjectTypePair();
            thisPair.sObject = sObjectType;
            thisPair.dataCategoryGroupName = s.getName();
            pairs.add(thisPair);          
          //  system.debug('dataCategoryGroupName'+thisPair.dataCategoryGroupName);
        }

        //describe the categories recursivly
        list<Schema.DescribeDataCategoryGroupStructureResult> results = Schema.describeDataCategoryGroupStructures(pairs,false);
      //   system.debug('dataCategoryGroupName*****************'+results[0].getTopCategories());
      // getAllCategories(results[0].getTopCategories());

        return results;
    }    
  /*  private static DataCategory[] getAllCategories(DataCategory [] categories)
    {
        if(categories.isEmpty())
        {
            return new DataCategory[]{};
        } 
        else
        {
            DataCategory [] categoriesClone = categories.clone();
            DataCategory category = categoriesClone[0];
            DataCategory[] allCategories = new DataCategory[]{category};
            list<string> categoriesNameList =new list<string>();
            categoriesClone.remove(0);
            categoriesClone.addAll(category.getChildCategories());
            allCategories.addAll(getAllCategories(categoriesClone));
            for(DataCategory dataCat :allCategories ){
                categoriesNameList.add(dataCat.getName());
            }
            system.debug('allCategories***************'+categoriesNameList);
            return allCategories;
        }
    }
   
*/
    @remoteAction 
    global static string GetArticles(string Category)
    {
           
           String qryString = 'SELECT Id,KnowledgeArticleId,title,ArticleType,Summary, UrlName, LastPublishedDate,LastModifiedBy.name FROM KnowledgeArticleVersion WHERE (PublishStatus = \'online\' and Language = \'en_US\')';
           qryString +=  'WITH DATA CATEGORY Intranet_Content__c AT ' + Category + ' order by LastPublishedDate DESC'; 
             System.debug('qq'+qryString);
            
           List<KnowledgeArticleVersion> articleList= Database.query(qryString);
               //System.debug('ff'+articleList);
            return JSON.serialize(articleList);
           // maxSize = articleList.size() ;
    }
    
    
    
       @remoteAction 
    global static string GetLatestArticles()
    {
           
           String qryString = 'SELECT Id,KnowledgeArticleId,title,ArticleType,Summary, UrlName, LastPublishedDate,LastModifiedBy.name  FROM KnowledgeArticleVersion WHERE (PublishStatus = \'online\' and Language = \'en_US\')';
           qryString +=  'WITH DATA CATEGORY Intranet_Content__c BELOW News_Updates__c order by LastPublishedDate DESC Limit 20'; 
           System.debug('qq123'+qryString);
            
           List<KnowledgeArticleVersion> articleList= Database.query(qryString);
            //   System.debug('ff'+articleList);
            return JSON.serialize(articleList);
           // maxSize = articleList.size() ;
    }
    
    @remoteAction 
    global static string SearchArticles(string keyword)
    {
       System.debug('keyword'+keyword);
        List<KnowledgeArticleVersion> optyList = new List<KnowledgeArticleVersion>();
        
         String qryString = 'FIND {srchkeyword} IN ALL FIELDS RETURNING KnowledgeArticleVersion';
                qryString += '(Id,KnowledgeArticleId,title,ArticleType,Summary, UrlName, LastPublishedDate,LastModifiedBy.name WHERE PublishStatus=\'online\' and Language = \'en_US\' order by LastPublishedDate DESC)';                   
                Boolean isShow;
              User currentUser = new User();
        id loggedinuserid= UserInfo.getUserId();
        currentUser = [Select id,Ops_User__c from User where id=:loggedinuserid limit 1];
        if(currentUser!=null){
            isShow = currentUser.Ops_User__c;
        }
                if(isShow){
                qryString += 'WITH DATA CATEGORY Intranet_Content__c BELOW ('+system.label.Intranet_Ops_User+')';
                }else{
                qryString += 'WITH DATA CATEGORY Intranet_Content__c BELOW ('+system.label.Intranet_Not_Ops_User+')';
                    
                }
           qryString  = qryString.replace('srchkeyword',keyword);  
           List<List<KnowledgeArticleVersion>> articleList = search.query(qryString);
           System.debug('qq'+qryString);
          
               
               optyList =  ((List<KnowledgeArticleVersion>)articleList[0]);
               System.debug('ff'+optyList);
            return JSON.serialize(optyList);
     }
    
    @remoteAction 
    global static string GetHomePageArtilce()
    {
           ///This mehod we give the latest artilce which need to be displayed on home page
           
            String qryString = 'SELECT Title,UrlName,Summary,Home_Main_Article__c,VideoUrl__c,VideoFrame__c,LastPublishedDate,LastModifiedBy.name FROM News_Updates__Kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') and Home_Main_Article__c = TRUE  order by LastPublishedDate DESC Limit 1'; 
                      
            News_Updates__kav articleList= Database.query(qryString); 
                     
            List<HomeArticles>  HomepageArticles = new  List<HomeArticles>();            
            if(articleList != null){
            
              HomeArticles ha = new HomeArticles();
              ha.ArtilceTitle = articleList.Title;
              ha.ArticleCategory = 'MainhomeArticle';
              ha.ArticleSummary = articleList.Summary;
              ha.ArticleCreatedBy = articleList.LastModifiedBy.name;
              ha.Artilcepublisheddate = articleList.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = articleList.VideoUrl__c;
              ha.VideoFrame = articleList.VideoFrame__c;
              ha.Articleurl = 'News_Updates#'+articleList.UrlName;
              HomepageArticles.Add(ha); 
            }
            
            qryString = 'SELECT Title,UrlName,Summary,Home_Main_Article__c,VideoUrl__c,LastPublishedDate,VideoFrame__c,LastModifiedBy.name FROM News_Updates__Kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') and Home_Page_Article_Category__c = \'RESpotlight\'  order by LastPublishedDate DESC Limit 1'; 
            articleList= Database.query(qryString);      
            if(articleList != null){
            
              HomeArticles ha = new HomeArticles();
              ha.ArtilceTitle = articleList.Title;
              ha.ArticleCategory = 'RESpotlight';
              ha.ArticleSummary = articleList.Summary;
              ha.ArticleCreatedBy = articleList.LastModifiedBy.name;
              ha.Artilcepublisheddate = articleList.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = articleList.VideoUrl__c;
              ha.Articleurl = 'News_Updates#'+articleList.UrlName;
              ha.VideoFrame = articleList.VideoFrame__c;
              HomepageArticles.Add(ha); 
            }
            
            qryString = 'SELECT Title,UrlName,Summary,Home_Main_Article__c,VideoUrl__c,LastPublishedDate,VideoFrame__c,LastModifiedBy.name FROM News_Updates__Kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') and Home_Page_Article_Category__c = \'Career Opportunities\'  order by LastPublishedDate DESC Limit 1'; 
            articleList= Database.query(qryString);      
            if(articleList != null){
            
              HomeArticles ha = new HomeArticles();
              ha.ArtilceTitle = articleList.Title;
              ha.ArticleCategory = 'Career Opportunities';
              ha.ArticleSummary = articleList.Summary;
              ha.ArticleCreatedBy = articleList.LastModifiedBy.name;
              ha.Artilcepublisheddate = articleList.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = articleList.VideoUrl__c;
              ha.Articleurl = 'News_Updates#'+articleList.UrlName;
              ha.VideoFrame = articleList.VideoFrame__c;
              HomepageArticles.Add(ha); 
            }   
           
            qryString = 'SELECT Title,UrlName,Summary,Home_Main_Article__c,VideoUrl__c,VideoFrame__c,LastPublishedDate,LastModifiedBy.name FROM News_Updates__Kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') and Home_Page_Article_Category__c = \'Videos\'  order by LastPublishedDate DESC Limit 1'; 
            articleList= Database.query(qryString);      
            if(articleList != null){
            
              HomeArticles ha = new HomeArticles();
              ha.ArtilceTitle = articleList.Title;
              ha.ArticleCategory = 'Videos';
              ha.ArticleSummary = articleList.Summary;
              ha.ArticleCreatedBy = articleList.LastModifiedBy.name;
              ha.Artilcepublisheddate = articleList.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = articleList.VideoUrl__c;
              ha.Articleurl = 'News_Updates#'+articleList.UrlName;
              ha.VideoFrame = articleList.VideoFrame__c;
              HomepageArticles.Add(ha); 
              system.debug('Videos123456'+ha);
            }
            system.debug('Videos1234'+HomepageArticles);
            
            
            qryString = 'SELECT Title,UrlName,Summary,Home_Main_Article__c,VideoUrl__c,VideoFrame__c,LastPublishedDate,LastModifiedBy.name FROM News_Updates__Kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') and Home_Page_Article_Category__c = \'VEREIT Mission\'  order by LastPublishedDate DESC Limit 1'; 
            articleList= Database.query(qryString);      
            if(articleList != null){
            
              HomeArticles ha = new HomeArticles();
              ha.ArtilceTitle = articleList.Title;
              ha.ArticleCategory = 'VEREIT Mission';
              ha.ArticleSummary = articleList.Summary;
              ha.ArticleCreatedBy = articleList.LastModifiedBy.name;
              ha.Artilcepublisheddate = articleList.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = articleList.VideoUrl__c;
              ha.Articleurl = 'News_Updates#'+articleList.UrlName;
              ha.VideoFrame = articleList.VideoFrame__c;
              HomepageArticles.Add(ha); 
            }
           
            qryString = 'SELECT Title,UrlName,Summary,Home_Main_Article__c,VideoUrl__c,VideoFrame__c,LastPublishedDate,LastModifiedBy.name FROM News_Updates__Kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') and  Home_Page_Article_Category__c = \'VEREIT Beat\'  order by LastPublishedDate DESC Limit 1'; 
            articleList= Database.query(qryString);      
            if(articleList != null){
            
              HomeArticles ha = new HomeArticles();
              ha.ArtilceTitle = articleList.Title;
              ha.ArticleCategory = 'VEREIT Beat';
              ha.ArticleSummary = articleList.Summary;
              ha.ArticleCreatedBy = articleList.LastModifiedBy.name;
              ha.Artilcepublisheddate = articleList.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = articleList.VideoUrl__c;
              ha.Articleurl = 'News_Updates#'+articleList.UrlName;
              ha.VideoFrame = articleList.VideoFrame__c;
              HomepageArticles.Add(ha); 
            }
            
            qryString = 'SELECT Title,UrlName,Summary,LastPublishedDate,LastModifiedBy.name FROM VERITAS_Award__kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') order by LastPublishedDate DESC Limit 1'; 
            VERITAS_Award__kav GGArtilces  = Database.query(qryString);      
            if(GGArtilces != null){
            
              HomeArticles ha = new HomeArticles();
              ha.ArtilceTitle = GGArtilces.Title;
              ha.ArticleCategory = 'VERITAS_Award';
              ha.ArticleSummary = GGArtilces.Summary;
              ha.ArticleCreatedBy = GGArtilces.LastModifiedBy.name;
              ha.Artilcepublisheddate = GGArtilces.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = '';
              ha.Articleurl = 'VERITAS_Award#'+GGArtilces.UrlName;
              HomepageArticles.Add(ha); 
            }
            
            qryString = 'SELECT Title,UrlName,Summary,LastPublishedDate,LastModifiedBy.name FROM Miscellaneous__kav WHERE (PublishStatus = \'online\' and Language = \'en_US\')and  Article_Category__c = \'External Links\'  order by LastPublishedDate DESC Limit 1'; 
            Miscellaneous__kav mmArtilces  = Database.query(qryString);      
            if(mmArtilces  != null){
            
              HomeArticles ha = new HomeArticles();
              ha.ArtilceTitle = mmArtilces.Title;
              ha.ArticleCategory = 'ExtLink';
              ha.ArticleSummary = mmArtilces.Summary;
              ha.ArticleCreatedBy = mmArtilces.LastModifiedBy.name;
              ha.Artilcepublisheddate = mmArtilces.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = '';
              ha.Articleurl = 'Miscellaneous#'+mmArtilces.UrlName;
              HomepageArticles.Add(ha); 
            }
            qryString = 'SELECT Title,UrlName,Summary,Home_Main_Article__c,VideoUrl__c,VideoFrame__c,LastPublishedDate,LastModifiedBy.name FROM News_Updates__Kav WHERE (PublishStatus = \'online\' and Language = \'en_US\') and  Home_Page_Article_Category__c = \'Lunch\'  order by LastPublishedDate DESC Limit 1'; 
            articleList= Database.query(qryString);        
            if(articleList != null){
            
              HomeArticles ha = new HomeArticles();
              ha.ArtilceTitle = articleList.Title;
              ha.ArticleCategory = 'Lunch';
              ha.ArticleSummary = articleList.Summary;
              ha.ArticleCreatedBy = articleList.LastModifiedBy.name;
              ha.Artilcepublisheddate = articleList.LastPublishedDate.format('MMMM d, yyyy');
              ha.videourl   = articleList.VideoUrl__c;
              ha.Articleurl = 'News_Updates#'+articleList.UrlName;
              ha.VideoFrame = articleList.VideoFrame__c;
              HomepageArticles.Add(ha); 
            }
            
            system.debug('HomepageArticles'+HomepageArticles);
            system.debug('HomepageArticles.size()'+HomepageArticles.size());
            return JSON.serialize(HomepageArticles);
           
    }
    
     
    public class HomeArticles {
    
      public string ArtilceTitle ;
      public string ArticleCategory;
      public string Articleurl;
      public string ArticleSummary;
      public string ArticleCreatedBy;
      public string Artilcepublisheddate;
      public string videourl;
      public string VideoFrame;
      public  HomeArticles(){}
    
    
    }
    
    // Vivek Class start Jan 29 2015
    @remoteAction 
    global static string GetMarquee()
    {
                      
            List<Marquee__c>  marqueeList = new  List<Marquee__c>(); 

            String qryString = 'SELECT Description__c FROM Marquee__c'; 
            
             marqueeList= Database.query(qryString);
               
            List<HomeMarquee>  HomepageMaquee = new  List<HomeMarquee>();
            
            if(marqueeList != null){
            
            for(Marquee__c cte:marqueeList){
              
               HomeMarquee ha = new HomeMarquee();
               ha.Description = cte.Description__c;
               HomepageMaquee.Add(ha); 
              
              }
            }
           
            return JSON.serialize(HomepageMaquee);

           
    }
    
     
    public class HomeMarquee {
    
      public string Description ;
      public  HomeMarquee(){}
    
    
    }
  /*  // Vivek Class End
    
    // Vivek Class start Feb 23 2015
    @remoteAction 
    global static string GetExtLnks()
    {
                      
            List<Resource_Links__c>  extLnks = new  List<Resource_Links__c>(); 

            String qryStrings = 'SELECT URL__c FROM Resource_Links__c'; 
            
             extLnks= Database.query(qryStrings);
               
            List<ExternalLinks>  ExtlLinksShow = new  List<ExternalLinks>();
            
            if(extLnks != null){
            
            for(Resource_Links__c cte:extLnks){
              
               ExternalLinks ha = new ExternalLinks();
               ha.URLlnk = cte.URL__c;
               ExtlLinksShow.Add(ha); 
              
              }
            }
           
            return JSON.serialize(ExtlLinksShow);

           
    }
    
     
    public class ExternalLinks {
    
      public string URLlnk ;
      public  ExternalLinks(){}
    
    
    } */
    // Vivek Class End Feb 23 2015
        
 }