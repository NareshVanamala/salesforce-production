public with sharing class Top10Producer1 {

    public list<Contact> conList{get;set;}
    public String name{get;set;}
    public String address{get;set;}
    public String city{get;set;}
    public String state{get;set;}
    public String zip{get;set;}
    public Decimal totalcole{get;set;}

    public Top10Producer1(ApexPages.StandardController controller)
    
     {
     Account a=[select id, name from Account where id = :ApexPages.currentPage().getParameters().get('id')];
     //Account a=[select id, name from Account where id = '001P000000adMsB'];
      conList=[select id,name,MailingStreet,Contact_Type__c,MailingCity,MailingState,MailingPostalCode,Total_Cole_Tickets_Only_Funds_in_SF__c,Total_Cole_Production_Funds_in_SF__c,Last_Producer_Date__c from Contact where Total_Cole_Production_Funds_in_SF__c!=0 and accountid=:a.id order by Total_Cole_Production_Funds_in_SF__c desc limit 10 ];
        system.debug('Check the list, .....'+conList);
    }

}