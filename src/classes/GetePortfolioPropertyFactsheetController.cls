Public with sharing class GetePortfolioPropertyFactsheetController
{
   Id dealid;
   Id Portfolioid;
  // Portfolio__c mypfd
   public list<MyWrapper>deals;
   public list<Rent_Roll__c>rentRolls;
   public list<Loan_Relationship__c>loans;
   public list<MyWrapper> wcampList{get;set;}
   public list<Deal__c >dealist;
   public Map<Id, List<Rent_Roll__c>>dealrentRoll;
   public Map<Id, List<Loan_Relationship__c>>dealLoanRelation;
   public set<id>dealidset;
   public  GetePortfolioPropertyFactsheetController() 
   {
       Portfolioid=ApexPages.currentPage().getParameters().get('id');
       deals=new list<MyWrapper>();
     //dealid=ApexPages.currentPage().getParameters().get('id');
      system.debug('check the id..'+Portfolioid);
      dealist=[select  Pipeline_Id__c,Year_1_NOI__c,Average_NOI__c,Outside_Counsel_Contact__c,Outside_Counsel_Contact__r.name,Outside_Counsel_Contact__r.Account.Name,Outside_Counsel_Contact__r.AccountId,Seller_s_Broker_Company__c,Seller_s_Broker_Company__r.name,Seller_s_Broker__r.email,Seller_s_Broker__r.Fax,Seller_s_Broker__r.Phone,Seller_s_Broker__r.MailingPostalCode,Seller_s_Broker__r.MailingState,Seller_s_Broker__r.MailingCity, Seller_s_Broker__r.Mailingstreet,Seller_s_Broker__r.name,Seller_s_Broker__c,Property_Owner_Contact__r.email,Seller_s_Counsel_Attorney__r.email,Seller_s_Counsel_Attorney__r.Fax,Seller_s_Counsel_Attorney__r.Phone,Seller_s_Counsel_Attorney__r.MailingPostalCode,Seller_s_Counsel_Attorney__r.Mailingstreet,Seller_s_Counsel_Attorney__r.MailingCity,Seller_s_Counsel_Attorney__r.MailingState, Seller_s_Counsel_Attorney__r.name,Property_Owner_Company__r.name,Seller_s_Counsel_Attorney__c,Seller_s_Counsel_Firm__r.name,Seller_Memo__c,Property_Owner_Contact__r.Fax,Property_Owner_Contact__r.Phone,Property_Owner_Contact__r.MailingPostalCode,Property_Owner_Contact__r.MailingState,Property_Owner_Contact__r.Mailingstreet,Property_Owner_Contact__r.MailingCity,Property_Owner_Contact__r.name,Property_Owner_Contact__c,Property_Owner_Contact__r.lastname,Property_Owner_Contact__r.firstname, Buyer_s_Broker__c,Additional_Deposit__c,Initial_Deposit__c,CAP_Rate__c,Price_PSF_Total_Price__c,Price_PSF_Contract_Price__c,Accenture_Due_Date__c,Part_1_Start_Date__c,GL_Entity_ID__c,Federal_ID__c,Property_Manager__c,Building_Name__c,Center_Name__c, Address__c,City__c,Property_Phone__c,Zip_Code__c,State__c,MRI_Property_Type__c,Tenancy__c,Ownership_Type__c,Year_Renovated__c,Year_Built__c,Total_SF__c,Total_Price__c,GAAP_Cap__c,Study_Period__c,Closing_Period__c,Estimated_COE_Date__c,Lease_Abstract_Notes__c,Lease_Abstract_Date__c ,Site_Visit_By__c,Site_Visit_Date__c,Site_Visit_By__r.name,Lease_Abstract_By__r.name,Site_Visit_Notes__c,id,Name,Fund__c,Paralegal__c,Contract_Price__c,Attorney__c,Deal_Status__c,MRI_Property_Type_picklist__c,Ownership_Interest__c,Property_Sub_Type__c,Primary_Use__c,Property_Location__c,Division__c,JV_Partner_Name__c,Number_of_Floors__c,Sale_Leaseback__c,Build_To_Suit__c,BTS_Completion_Date__c,Land_Acres__c,Parking_Spaces__c  from Deal__c where Portfolio_Deal__c=:Portfolioid];
      //system.debug('check the mydeal..'+mydeal);
      dealrentRoll= new map<Id, List<Rent_Roll__c>>();
      dealLoanRelation= new map<Id, List<Loan_Relationship__c>>();
      dealidset= new set<id>();
      for(Deal__c ct:dealist)
       dealidset.add(ct.id);
      loans=[select Deal__c,id,Loan_Amount__c,LTV__c,Lender__c,Lender__r.name,Rate_Spread__c,Fixed_Floating__c,Index__c,Amort_I_O__c from Loan_Relationship__c where Deal__c in:dealidset];
      rentRolls=[select id,Name_of_Tenant__c,Suite_ID__c,Floor__c,SF__c,Lease_Start__c,Lease_Expiration__c,store__c,Tenant_Type_ID__c,Nat_Tenant_ID__c,SIC_Group_Code__c,NAICS_Code__c,Lease_Type__c,Lease_Code__c,Reimb_Type__c,Reimb_Code__c,S_P_Rating__c,Deal__c from Rent_Roll__c where Deal__c in:dealidset];
        for(Rent_Roll__c ri :rentRolls )
        { 
           if(dealrentRoll.containsKey(ri.Deal__c))
             dealrentRoll.get(ri.Deal__c).add(ri);
          else
            dealrentRoll.put(ri.Deal__c, new List<Rent_Roll__c >{ri});
          } 
      
       for(Loan_Relationship__c ri :loans)
        { 
           if(dealLoanRelation.containsKey(ri.Deal__c))
             dealLoanRelation.get(ri.Deal__c).add(ri);
           else
            dealLoanRelation.put(ri.Deal__c, new List<Loan_Relationship__c >{ri});
            
         } 
      
   
   }
    public Portfolio__c myPortfolio
   { 
      get
     {     
         if (myPortfolio== null) 
           myPortfolio= new Portfolio__c ();   
           return myPortfolio; 
      }   
    set;  
  }
  
  
  
  /*public list<Deal__c>getdeals()
  {
  
      deals=[select Seller_s_Broker_Company__c,Seller_s_Broker__r.email,Seller_s_Broker__r.Fax,Seller_s_Broker__r.Phone,Seller_s_Broker__r.MailingPostalCode,Seller_s_Broker__r.MailingState,Seller_s_Broker__r.MailingCity, Seller_s_Broker__r.Mailingstreet,Seller_s_Broker__r.name,Seller_s_Broker__c,Property_Owner_Contact__r.email,Seller_s_Counsel_Attorney__r.email,Seller_s_Counsel_Attorney__r.Fax,Seller_s_Counsel_Attorney__r.Phone,Seller_s_Counsel_Attorney__r.MailingPostalCode,Seller_s_Counsel_Attorney__r.Mailingstreet,Seller_s_Counsel_Attorney__r.MailingCity,Seller_s_Counsel_Attorney__r.MailingState, Seller_s_Counsel_Attorney__r.name,Property_Owner_Company__c,Property_Owner_Company__r.name,Seller_s_Counsel_Attorney__c,Seller_s_Counsel_Firm__c,Seller_Memo__c,Property_Owner_Contact__r.Fax,Property_Owner_Contact__r.Phone,Property_Owner_Contact__r.MailingPostalCode,Property_Owner_Contact__r.MailingState,Property_Owner_Contact__r.Mailingstreet,Property_Owner_Contact__r.MailingCity,Property_Owner_Contact__r.name,Property_Owner_Contact__c,Property_Owner_Contact__r.lastname, Buyer_s_Broker__c,Additional_Deposit__c,Initial_Deposit__c,CAP_Rate__c,Price_PSF_Total_Price__c,Price_PSF_Contract_Price__c,Accenture_Due_Date__c,Part_1_Start_Date__c,GL_Entity_ID__c,Federal_ID__c,Property_Manager__c,Building_Name__c,Center_Name__c, Address__c,City__c,Property_Phone__c,Zip_Code__c,State__c,MRI_Property_Type__c,Tenancy__c,Ownership_Type__c,Year_Renovated__c,Year_Built__c,Total_SF__c,Total_Price__c,GAAP_Cap__c,Study_Period__c,Closing_Period__c,Estimated_COE_Date__c,Lease_Abstract_Notes__c,Lease_Abstract_Date__c ,Site_Visit_By__c,Site_Visit_Date__c,Site_Visit_By__r.name,Lease_Abstract_By__r.name,Site_Visit_Notes__c,id,Name,Fund__c,Paralegal__c,Contract_Price__c,Attorney__c,Deal_Status__c from Deal__c where Portfolio_Deal__c=:Portfolioid];
      return deals; 
  }*/
   /*public List<Rent_Roll__c>getrentRolls() {
    rentRolls=[select id,Name_of_Tenant__c,Suite_ID__c,Floor__c,SF__c,Lease_Start__c,Lease_Expiration__c,store__c,Tenant_Type_ID__c,Nat_Tenant_ID__c,SIC_Group_Code__c,NAICS_Code__c,Lease_Type__c,Lease_Code__c,Reimb_Code__c,S_P_Rating__c from Rent_Roll__c where Deal__c =:mydeal.id];
        //return SObjectType.Deal__c.FieldSets.Purchase_Report_Property_Status.getFields();
    return rentRolls;
    } */
    
    
 /* public List<Loan_Relationship__c>getloans() {
    loans=[select id,Loan_Amount__c,LTV__c,Lender__c,Lender__r.name,Rate_Spread__c,Fixed_Floating__c,Index__c,Amort_I_O__c from Loan_Relationship__c where Deal__c =:mydeal.id];
        //return SObjectType.Deal__c.FieldSets.Purchase_Report_Property_Status.getFields();
    return loans;
    } */
  
    public list<MyWrapper>getdeals()
     {
         for(Deal__c ct1: dealist)  
         {
             MyWrapper w= new MyWrapper();
             w.name=ct1.name;
             w.status=ct1.Deal_Status__c;
             w.Address=ct1.Address__c;
             w.City=ct1.City__c;
             w.State=ct1.State__c;
             w.Zipcode=ct1.Zip_Code__c;
             w.Propertyphone=ct1.Property_Phone__c;
             w.Tenanacy=ct1.Tenancy__c;
             //w.MRIPropertytype=ct1.MRI_Property_Type__c;
             w.MRIPropertytype=ct1.MRI_Property_Type_picklist__c;
             //w.Onwershiptype=ct1.Ownership_Type__c;
             w.Onwershiptype=ct1.Ownership_Interest__c;
             w.MRIPropertySubType=ct1.Property_Sub_Type__c;
             w.MRIPropertyUse=ct1.Primary_Use__c;
             w.MRIPropertyLocation=ct1.Property_Location__c;
             w.MRIOwnershipSubType =ct1.Ownership_Type__c;
             w.MRIBusinessDivision=ct1.Division__c;
             w.MRIJVPartnerName=ct1.JV_Partner_Name__c;
             w.MRINumberofFloors=ct1.Number_of_Floors__c;
             w.MRISaleLeaseback=ct1.Sale_Leaseback__c;
             w.MRIBuildtoSuit=ct1.Build_To_Suit__c;
             w.MRIBuildtoSuitCompletion=ct1.BTS_Completion_Date__c;
             w.MRILandAcreage=ct1.Land_Acres__c;
             w.MRIParkingSpacesAvailable=ct1.Parking_Spaces__c;
             w.YearBuil=ct1.Year_Built__c;
             w.YearRenovated=ct1.Year_Renovated__c;
             w.TotalSF=ct1.Total_SF__c;
             w.ContractPrice=ct1.Contract_Price__c;
             w.TotalPrice=ct1.Total_Price__c;
             w.GoinginNOI=ct1.Year_1_NOI__c;
             w.AverageNOI=ct1.Average_NOI__c;
             w.GAAPCap=ct1.GAAP_Cap__c;
             w.StudyPeriod=ct1.Study_Period__c;
             w.ClosingPeriod=ct1.Closing_Period__c;
             w.EstimatedCOEDate=ct1.Estimated_COE_Date__c;
             w.Fund=ct1.Fund__c;
             w.PipelineID=ct1.Pipeline_Id__c;
             w.BuildingName=ct1.Building_Name__c;
             w.CenterName=ct1.Center_Name__c;
             w.PropertyManager=ct1.Property_Manager__c;
             w.GLEntityID=ct1.GL_Entity_ID__c;
             w.FederalID=ct1.Federal_ID__c;
             w.Part1StartDate=ct1.Part_1_Start_Date__c;
             w.AccentureDueDate=ct1.Accenture_Due_Date__c;
             w.PricePSF=ct1.Price_PSF_Contract_Price__c;
             w.PricePSF1=ct1.Price_PSF_Total_Price__c;
             w.CapRate=ct1.CAP_Rate__c;
             w.InitialDeposit=ct1.Initial_Deposit__c;
             w.AdditionalDeposit=ct1.Additional_Deposit__c;
             w.BuyersBroker=ct1.Buyer_s_Broker__c;
             w.ppFirstName=ct1.Property_Owner_Contact__r.firstname;
             w.ppLastNam=ct1.Property_Owner_Contact__r.lastname;
             w.ppMaillingStreet=ct1.Property_Owner_Contact__r.Mailingstreet;
             w.ppMaillingCity=ct1.Property_Owner_Contact__r.MailingCity;
             w.ppMaillingStateProvience=ct1.Property_Owner_Contact__r.MailingState;
             w.ppMaillingZipPostalCode=ct1.Property_Owner_Contact__r.MailingPostalCode;
             w.ppBusinessPhone=ct1.Property_Owner_Contact__r.Phone;
             w.ppBusinessFax=ct1.Property_Owner_Contact__r.Fax;
             w.ppEmail=ct1.Property_Owner_Contact__r.email;
             w.ppSellerMemo=ct1.Seller_Memo__c;
             w.ppAccountName=ct1.Property_Owner_Company__r.name;
             w.SCFullName=ct1.Seller_s_Counsel_Attorney__r.name;
             w.SCMaillingStreet=ct1.Seller_s_Counsel_Attorney__r.Mailingstreet;
             w.SCMaillingCity=ct1.Seller_s_Counsel_Attorney__r.MailingCity; 
             w.SCMaillingStateProvience=ct1.Seller_s_Counsel_Attorney__r.MailingState;
             w.SCMaillingZipPostalCode=ct1.Seller_s_Counsel_Attorney__r.MailingPostalCode;
             w.SCBusinessPhone=ct1.Seller_s_Counsel_Attorney__r.Phone;
             w.SCBusinessFax=ct1.Seller_s_Counsel_Attorney__r.Fax;
             w.SCEmail=ct1.Seller_s_Counsel_Attorney__r.email;
             w.SCAccountName=ct1.Seller_s_Counsel_Firm__r.name;
             w.BDFullName=ct1.Seller_s_Broker__r.name;
             w.BDMaillingStreet=ct1.Seller_s_Broker__r.Mailingstreet;
             w.BDMaillingCity=ct1.Seller_s_Broker__r.MailingCity;
             w.BDMaillingStateProvience=ct1.Seller_s_Broker__r.MailingState;
             w.BDMaillingZipPostalCode=ct1.Seller_s_Broker__r.MailingPostalCode;
             w.BDBusinessPhone=ct1.Seller_s_Broker__r.Phone;
             w.BDBusinessFax=ct1.Seller_s_Broker__r.Fax;
             w.BDEmail=ct1.Seller_s_Broker__r.email;
             w.BDAccountName=ct1.Seller_s_Broker_Company__r.name;     
             w.Rentrolllist=dealrentRoll.get(ct1.id);
             w.Loanrealtionshiplist=dealLoanRelation.get(ct1.id);
             deals.add(w);
             }
           return deals;
         
  
  
     } 
  
  
   
    public class MyWrapper
    { 
        
        
       Public string name{get;set;}
       public string status{get;set;}  
       Public String Address{get;set;}
       Public String City{get;set;} 
       Public String State{get;set;}              
       Public String Zipcode{get;set;}  
       Public String Propertyphone{get;set;}  
       Public String Tenanacy{get;set;}
       Public String MRIPropertytype{get;set;}
       Public String Onwershiptype{get;set;}
       Public String YearBuil{get;set;}
       Public String YearRenovated{get;set;}
       Public Decimal TotalSF{get;set;}
       Public Decimal ContractPrice{get;set;}
       Public Decimal TotalPrice{get;set;}
       Public Decimal GoinginNOI{get;set;}
       Public Decimal AverageNOI{get;set;}
       Public Decimal GAAPCap{get;set;}
       Public Decimal StudyPeriod{get;set;}
       Public Decimal ClosingPeriod{get;set;}
       Public Date    EstimatedCOEDate{get;set;}
       Public String Fund{get;set;}
       Public String PipelineID{get;set;}
       Public String BuildingName{get;set;} 
       Public String CenterName{get;set;}
       Public String PropertyManager{get;set;} 
       Public String GLEntityID{get;set;} 
       Public String FederalID{get;set;} 
       Public Date   Part1StartDate{get;set;} 
       Public Date   AccentureDueDate{get;set;} 
       Public Decimal PricePSF{get;set;}
       Public Decimal PricePSF1{get;set;}
       Public Decimal CapRate{get;set;}
       Public Decimal InitialDeposit{get;set;}
       Public Decimal AdditionalDeposit{get;set;}
       Public String BuyersBroker{get;set;}
       Public String ppFirstName{get;set;}
       Public String ppLastNam{get;set;}
       Public String ppMaillingStreet{get;set;}
       Public String ppMaillingCity{get;set;}
       Public String ppMaillingStateProvience{get;set;}
       Public String ppMaillingZipPostalCode{get;set;}
       Public String ppBusinessPhone{get;set;}
       Public String ppBusinessFax{get;set;}
       Public String ppEmail{get;set;}
       Public String ppSellerMemo{get;set;}
       Public String ppAccountName{get;set;}
       Public String SCFullName{get;set;}  
       Public String SCMaillingStreet{get;set;}
       Public String SCMaillingCity{get;set;}
       Public String SCMaillingStateProvience{get;set;}
       Public String SCMaillingZipPostalCode{get;set;}
       Public String SCBusinessPhone{get;set;}
       Public String SCBusinessFax{get;set;}
       Public String SCEmail{get;set;}
       Public String SCAccountName{get;set;}
       Public String BDFullName{get;set;}
       Public String BDMaillingStreet{get;set;}
       Public String BDMaillingCity{get;set;}
       Public String BDMaillingStateProvience{get;set;}
       Public String BDMaillingZipPostalCode{get;set;}
       Public String BDBusinessPhone{get;set;}
       Public String BDBusinessFax{get;set;}
       Public String BDEmail{get;set;}
       Public String BDAccountName{get;set;}
       public string MRIPropertySubType {get;set;}
       public string MRIPropertyUse {get;set;}
       public string MRIPropertyLocation {get;set;}
       public string MRIOwnershipSubType {get;set;}
       public string MRIBusinessDivision{get;set;}
       public string MRIJVPartnerName{get;set;}
       public string MRINumberofFloors {get;set;}
       public string MRISaleLeaseback{get;set;}
       public string MRIBuildtoSuit{get;set;}
       public Date MRIBuildtoSuitCompletion{get;set;}
       public Decimal MRILandAcreage{get;set;}
       public Decimal MRIParkingSpacesAvailable{get;set;}
       public list<Rent_Roll__c>Rentrolllist{get;set;} 
       public list<Loan_Relationship__c>Loanrealtionshiplist{get;set;} 
       public MyWrapper(){
        Rentrolllist= new List<Rent_Roll__c>();
        Loanrealtionshiplist=new list<Loan_Relationship__c>();
        }
    }
  
  
   
}