@isTest(seeAllData=true)
private class massApplyLoanCtrl_Test {
    static testMethod void massApplyLoanCtrlTest() {
    Portfolio__c protObj= new Portfolio__c();
       protObj.name='testing';
       protObj.Old_Portfolio_DC_External_ID__c='1231123testing';
       insert protObj;
    Deal__c dealObj = new Deal__c();
       dealObj.name='test deal';
       dealObj.Cole_Initials__c='test cole';
       dealObj.Property_Type__c='Land';
       dealObj.Tenancy__c='Single-Tenant';
       dealObj.Primary_Use__c='Retail';
       dealObj.Contract_Price__c=20252;
       dealObj.Report_Deal_Name__c='testing';
       dealObj.Ownership__c =0;
       dealObj.Portfolio_Deal__c=protObj.id;
       insert dealObj;
       
       System.assertNotEquals(protObj.name,dealObj.name);
       
    Loan__c loanObj = new Loan__c();
       loanObj.Name ='test loan';
       loanObj.Status__c ='Prospective Loan';
       loanObj.Total_Loan_Amount__c=85858;
       insert loanObj;
    Loan_Relationship__c loanRelObj = new Loan_Relationship__c();
       loanRelObj.name = loanObj.Name+'-'+dealObj.Report_Deal_Name__c;
       loanRelObj.Loan__c = loanObj.id;
       loanRelObj.Deal__c = dealObj.id;
       loanRelObj.Loan_Amount__c = 200000;
       insert loanRelObj;
       
     System.assertNotEquals(loanObj.Name,loanRelObj.name);

       
    PageReference PageRef = Page.massApplyLoanPage;
    Test.setCurrentPage(PageRef);
    ApexPages.currentPage().getParameters().put('selectedRecords', dealObj.id);      
    
       massApplyLoanCtrl temp = new massApplyLoanCtrl();
       temp.selectedLoan.add(loanObj);
        temp.GetSelectedLoans();
        temp.getLoans();
        temp.dealPage();
        temp.search();
        temp.getSelected();
        temp.cancel();
    }
}