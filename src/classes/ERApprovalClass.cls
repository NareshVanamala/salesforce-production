public with sharing class ERApprovalClass 
{

    public String sMail{get;set;}
    public string approverAccount {get;set;}
    public string approvername {get;set;}
    public string approvernametitle {get;set;}
    public string approverComment {get;set;}
// public <Account> approverAccount {get;set;}    
    public List<Event_Automation_Request__c> earList{get;set;}
    public id earcampid;
    public transient List<CampaignMember> campList{get;set;}
    public transient List<CampaignMember> campList1{get;set;}
    public transient List<CampaignMember> campPendList{get;set;}
    public transient List<CampaignMember> campApprList{get;set;}
    public transient List<CampaignMember> campDeclList{get;set;}
    public transient List<CampaignMember> campAttendedlist{get;set;}
    public list<BDApprovalHelper__c>insertBDapprovalList;
//********************added by snehal
    public transient List<CampaignMember> MycamplList= new list<CampaignMember>();
    public transient List<Event_Automation_Request__c>myEventist= new list<Event_Automation_Request__c>();
    public set<id>camped= new set<id>();
//***************************added by Snehal finished
    public list<MyWrapper> wcampList{get;set;}
    public Boolean norecs{get;set;}
    public Boolean hasrecs{get;set;}
    public Boolean hasrecs1{get;set;}
    public Boolean showNext{get;set;}
    public Boolean showCheck{get;set;}
    public Boolean hasnorecs{get;set;} 
    public List<Attachment> attList{get;set;}
    public List<SelectOption> CallListValues {set;get;}
    public String CallList {set;get;}
    public Set<String> call = new Set<String>(); 
    public List<String> lscall = new List<String>();
    public Map<Id,String> idCommentMap = new Map<Id,String>(); 
    public String Name;
    public Boolean showcheckall{get;set;} 
    public Boolean showcheckall1{get;set;} 
    public Boolean bool{get;set;}
    public Boolean boolone{get;set;}
    public list<attachment> newlist{get;set;}
    public ERApprovalClass()
    { 
        hasrecs=false;
        hasrecs1=false;
        norecs=false;
        hasnorecs=true;
        try
        {
            sMail=ApexPages.CurrentPage().getParameters().get('emid');
            if((sMail!='')||(sMail!=null))
            {
                //get the call list names assigned to the logged in user//
//***************added by Snehal to get the list of events
                MycamplList=[select id,Contact.Account.name,Contact.Title,Contact.Account.Event_Approver__r.Title,Contact.Account.Event_Approver__r.name,CampaignId,status from CampaignMember where (Contact.Account.Event_Approver__r.email=:sMail OR Contact.Account.Event_Approver1__r.email=:sMail OR Contact.Account.Event_Approver2__r.email=:sMail) AND (status='Pending' OR status='Approved' OR  status='Declined' ) limit 40000 ];
                //MycamplList=[select id,Contact.Account.name,Contact.Title,Contact.Account.Event_Approver__r.Title,Contact.Account.Event_Approver__r.name,CampaignId,status from CampaignMember where Contact.Account.Event_Approver__r.email=:sMail AND status='Pending'];
                approverAccount=MycamplList[0].Contact.Account.name ;
                approvername=MycamplList[0].Contact.Account.Event_Approver__r.name; 
                approvernametitle = MycamplList[0].Contact.Account.Event_Approver__r.Title;
                system.debug('check the list...'+MycamplList.size());
                system.debug('>>>>>>>>>>>>>>>>>>>MycamplList>>>>>>>>>>>>>>>>>>>>>'+MycamplList.size()+' '+MycamplList);
                if(MycamplList.size()>0)
                {
                    for(CampaignMember ct:MycamplList) 
                        camped.add(ct.CampaignId);
                } 
                    system.debug('>>>>>>>>>>>>>>>>>>>camped>>>>>>>>>>>>>>>>>>>>>'+camped.size()+' '+camped);
                    myEventist=[select Id,Name,Event_Type__c,Start_Date__c,End_Date__c,Event_Name__c,Broker_Dealer_Name__r.Name,Campaign__r.Name,Broker_Dealer_Name__r.Event_Approver__r.Email,Broker_Dealer_Name__r.Event_Approver__r.Name,Comments__c from Event_Automation_Request__c where Campaign__c in:camped and Start_Date__c>today]; 
                    system.debug('>>>>>>>>>>>>>>>>>>>myEventist>>>>>>>>>>>>>>>>>>>>>'+myEventist.size()+' '+myEventist);
                    //**********************code added by Snehal finished
                    if(myEventist.size()>0)
                    {
                        hasrecs=true;
                        hasnorecs=false; 
                    }
                    // Add call lists name into picklist call list//
                    //First added all the vslues in the set to avoidd duplicates and then into the list and then into the picklist//
                    for(Event_Automation_Request__c t : myEventist)
                    {
                            call.add(t.Event_Name__c);
                    }
                    call.add('--None--');
                    CallListValues = new List<SelectOption>();
                    lscall.AddAll(call);
                    lscall.sort();
                    system.debug('Print my event list..'+lscall);
                    for(String s : lscall)
                    { 
                        if(s!=null) 
                        CallListValues.add(new SelectOption(s,s));
                     }
                    if(CallListValues.size()==0)
                    { 
                        CallListValues.add(new SelectOption('--None--','--None--'));
                    }
            } 
            }
            catch(Exception e){} 
        }
            public void AdviserCallList()
            {
            //system.debug('get the call list..'+calllist);
                  wcampList = new List<MyWrapper>(); 
                  insertBDapprovalList=new list<BDApprovalHelper__c>();
                  earList = new List<Event_Automation_Request__c>();
                  campPendList = new List<CampaignMember>();
                  campApprList = new List<CampaignMember>();
                  campDeclList = new List<CampaignMember>();
                  campAttendedlist= new list<CampaignMember>();
                  for(Event_Automation_Request__c e:[Select Name,RecordTypeId,OwnerId,ID,Event_Type__c,Start_Date__c,End_Date__c,Event_Name__c,Event_Description__c,Broker_Dealer_Name__r.Name,Campaign__r.Name,Broker_Dealer_Name__r.Event_Approver__r.Email,Broker_Dealer_Name__r.Event_Approver__r.Name,Comments__c from Event_Automation_Request__c where Campaign__c!=null AND Event_Name__c=:calllist])
                  {
//system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'+e.Broker_Dealer_Name__r.Event_Approver__r.Email);
//if(sMail==e.Broker_Dealer_Name__r.Event_Approver__r.Email)
//{ 
                    system.debug('>>>>>>>>>>>>>>>>>>eeeeeeeeeeeeeeeeeeeee>>>>>>>>>>>>>>>>'+e);
                    earList.add(e); 
//added by sandeep to get the set of campaign ids associated with Event Automation Request
                    earcampid=e.campaign__c;
//} 
                 }
                system.debug('------------earcampid---------'+earcampid);
//Modified by sandeep
                campPendList=[select id,contact.lastname, RecordTypeId, Contact.Name,CampaignId ,Contact.FirstName,Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,contact.account.name from CampaignMember where CampaignId=:earcampid and status='Pending' and (Contact.Account.Event_Approver__r.email=:sMail OR Contact.Account.Event_Approver1__r.email=:sMail OR Contact.Account.Event_Approver2__r.email=:sMail) ORDER BY contact.name limit 1000];
                system.debug('------------------pendinglist---------------'+camppendlist);
                campApprList=[select id, RecordTypeId, CampaignId ,Contact.FirstName,Contact.lastname,Contact.Name, Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,contact.account.name from CampaignMember where CampaignId=:earcampid and status='Approved' and (Contact.Account.Event_Approver__r.email=:sMail OR Contact.Account.Event_Approver1__r.email=:sMail OR Contact.Account.Event_Approver2__r.email=:sMail) ORDER BY contact.name limit 1000];
                campDeclList=[select id, RecordTypeId, CampaignId ,Contact.FirstName,Contact.lastname, Contact.Name,Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,contact.account.name from CampaignMember where CampaignId=:earcampid and status='Declined' and (Contact.Account.Event_Approver__r.email=:sMail OR Contact.Account.Event_Approver1__r.email=:sMail OR Contact.Account.Event_Approver2__r.email=:sMail) ORDER BY contact.name limit 1000];
                //campAttendedlist=[select id, RecordTypeId, CampaignId ,Contact.FirstName,Contact.lastname,Contact.Name, Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,contact.account.name from CampaignMember where CampaignId=:earcampid and status='Attended' and (Contact.Account.Event_Approver__r.email=:sMail OR Contact.Account.Event_Approver1__r.email=:sMail OR Contact.Account.Event_Approver2__r.email=:sMail) ORDER BY contact.name limit 1000];
                if(campPendList!= null && campPendList.size()>0 && earList!=null && earList.size()>0)
                {
                    for(CampaignMember comp:campPendList)
                    {
                        system.debug('>>>>>>>>>>>>>>>>>comp.id>>>>>>>>>>>>>>>>>'+comp.id);
                        MyWrapper w = new MyWrapper();
                        w.wName = comp.contact.name ;
                        //w.wAccountName = earList[0].Broker_Dealer_Name__r.Name;
                        w.wAccountName =comp.contact.account.name;
                        w.wCity = comp.Contact.MailingCity;
                        w.wState = comp.Contact.MailingState;
                        //w.wApproved=true;
                        w.wId = comp.id;
                        wcampList.add(w);
                    }
                        //String imageURL='/servlet/servlet.FileDownload?file=';
                        Event_automation_request__c ear=[Select Name,RecordTypeId,OwnerId,ID,Event_Type__c,Start_Date__c,End_Date__c,Event_Name__c,testingfield__c from event_automation_request__c where Campaign__c!=null AND Event_Name__c=:calllist];
                        set<id> selectedids= new set<id>();
                        if(ear.testingfield__c!=null)
                        {
                           
                           
                           /* integer j=(ear.testingfield__c.length()/18)-1;
                            for(integer i=0;i<=j;i++)
                            {
                                selectedids.add(ear.testingfield__c.substring(((i*18)+(i+5)),(((i+1)*18)+(i+5))));
                            }*/
                        
                        string a1=ear.testingfield__c.remove('null,');
                        list<id> a12=new list<id>();
                        a12=a1.split(',');
                        attList=[SELECT ContentType,Description,Id,Name,OwnerId,ParentId FROM Attachment where ParentId=:earList[0].id AND id in:a12 ORDER BY Name];
                        }
                        else
                        {
                        }
                    showNext=true;
                    norecs=true;
                }
                else
                {
                    Event_automation_request__c ea1=[Select Name,RecordTypeId,OwnerId,ID,Event_Type__c,Start_Date__c,End_Date__c,Event_Name__c,testingfield__c from event_automation_request__c where Campaign__c!=null AND Event_Name__c=:calllist];
                    set<id> selectedids1= new set<id>();
                    if(ea1.testingfield__c!=null)
                    {
                       /* integer j=(ea1.testingfield__c.length()/18)-1;
                        for(integer i=0;i<=j;i++)
                         {
                            selectedids1.add(ea1.testingfield__c.substring(((i*18)+(i+5)),(((i+1)*18)+(i+5))));
                         } */
                        
                        string a1=ea1.testingfield__c.remove('null,');
                        list<id> a12=new list<id>();
                        a12=a1.split(',');
                        attList=[SELECT ContentType,Description,Id,Name,OwnerId,ParentId FROM Attachment where ParentId=:earList[0].id AND id in:a12 ORDER BY Name];
                    }
                    hasrecs=true;
                    hasrecs1=false;
                    norecs=true;
                    showNext=false;
                } 
        }
    Public void doAction()
    { 
        Set<id> cidset = new Set<id>();
        Set<id> cidset1 = new Set<id>();
        for(MyWrapper w: wcampList )
        {
            system.debug('>>>>>>>>>>>>>>>>>w.wId>>>>>>>>>>>>>>>>>'+w.wId);
            //system.debug('>>>>>>>>>>>>>>>>>w.wApproved>>>>>>>>>>>>>>>>>'+w.wApproved);
            if(w.wApproved1==true)
            {
                cidset.add(w.wId); 
            }
        if(w.wApproved2==true)
            {
                cidset1.add(w.wId);
            } 
        idCommentMap.put(w.wId,w.wComment);
        system.debug('>>>>>>>>>>>>>>>>>cidset>>>>>>>>>>>>>>>>>'+cidset);
        }
        system.debug('>>>>>>>>>>>>>>>>>cidset.size()>>>>>>>>>>>>>>>>>'+cidset.size());
        if((cidset!= null && cidset.size()>0) || (cidset1!= null && cidset1.size()>0))
        {
            campList=[select Id, RecordTypeId, CampaignId ,Contact.Account.Event_Approver__r.name,Contact.FirstName,Contact.name,Contact.account.name,Contact.Email,Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c,Broker_Dealer_Name__c from CampaignMember where Id in :cidset ORDER BY contact.name];
            campList1=[select Id, RecordTypeId, CampaignId ,Contact.Account.Event_Approver__r.name,Contact.FirstName,Contact.name,Contact.account.name,Contact.Email,Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c,Broker_Dealer_Name__c from CampaignMember where Id in :cidset1 ORDER BY contact.name];
           if(campList.size()>0)
            { 
                string campeid=campList[0].CampaignId; 
                Event_Automation_Request__c Event1=[select id,Event_Name__c,Event_Description__c,End_Date__c,Start_Date__c from Event_Automation_Request__c where Campaign__c=:campeid limit 1];
                for(CampaignMember c:campList)
                {
                    system.debug('>>>>>>>>>>>>>>>>>c.id++Contact.name++c.status>>>>>>>>>>>>>>>>>'+c.id+''+Contact.name+''+c.status);
                    //updated by snehal to update the status of campaign memeber through site
                    BDApprovalHelper__c ct= new BDApprovalHelper__c();
                    ct.CampaignMemeberId__c=c.id; 
                    ct.CampaignMemeberName__c=c.Campaign.Name;
                    ct.Approver_Name__c=c.Contact.Account.Event_Approver__r.name;
                    ct.Date_Of_Approval__c=system.today();
                    ct.CampaignName__c=c.Campaign.Name;
                    ct.Contact_Name__c=c.Contact.name;
                    ct.Status__c='Approved';
                    ct.Comment__c=idCommentMap.get(c.id);
                    ct.Event_Id__c=Event1.id;
                    ct.Event_Name__c=Event1.Event_Name__c;
                    ct.Event_Description__c=Event1.Event_Description__c;
                    ct.Start_Date__c=Event1.Start_Date__c;
                    ct.End_Date__c=Event1.End_Date__c;
                    ct.Event__c=Event1.id;
                    ct.campaign__c=c.CampaignId;
                       insertBDapprovalList.add(ct);
                      Name=c.Contact.Account.name; 
                    // c.status = 'Approved';
                    system.debug('>>>>>>>>>>>>>>>>>c.status>>>>>>>>>>>>>>>>>'+c.status); 
                }
            } 
            if(campList1.size()>0)
            {
                string campeid=campList1[0].CampaignId; 
                Event_Automation_Request__c Event1=[select id,Event_Name__c,Event_Description__c,End_Date__c,Start_Date__c from Event_Automation_Request__c where Campaign__c=:campeid limit 1];
                for(CampaignMember c:campList1)
                {
                    system.debug('>>>>>>>>>>>>>>>>>c.id++Contact.name++c.status>>>>>>>>>>>>>>>>>'+c.id+''+Contact.name+''+c.status);
                    //updated by snehal to update the status of campaign memeber through site
                    BDApprovalHelper__c ct1= new BDApprovalHelper__c();
                    ct1.CampaignMemeberId__c=c.id; 
                    ct1.CampaignMemeberName__c=c.Campaign.Name;
                    ct1.Approver_Name__c=c.Contact.Account.Event_Approver__r.name;
                    ct1.Date_Of_Declined__c=system.today();
                    ct1.CampaignName__c=c.Campaign.Name;
                    ct1.Contact_Name__c=c.Contact.name;
                    ct1.Status__c='Declined';
                    ct1.Comment__c=idCommentMap.get(c.id);
                    ct1.Event_Id__c=Event1.id;
                    ct1.Event_Name__c=Event1.Event_Name__c;
                    ct1.Event_Description__c=Event1.Event_Description__c;
                    ct1.Start_Date__c=Event1.Start_Date__c;
                    ct1.End_Date__c=Event1.End_Date__c;
                    ct1.Event__c=Event1.id;
                    ct1.campaign__c=c.CampaignId;
                    insertBDapprovalList.add(ct1);
                     Name=c.Contact.Account.name; 
                    // c.status = 'Approved';
                        system.debug('>>>>>>>>>>>>>>>>>c.status>>>>>>>>>>>>>>>>>'+c.status); 
                    } 
                }
            }
            system.debug('>>>>>>>>>>>>>>>>>campList>>>>>>>>>>>>>>>>>'+campList);
            //update campList;
            //insert insertBDapprovalList;
            //system.debug(' check the list size...'+insertBDapprovalList.size());
            hasrecs=false;
            hasrecs1=true;
                showCheck=false; 
        }
public PageReference doAction1()
{ 
system.debug('Welcome to the method');
if(showCheck==false)
{
System.debug('not skipping');
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Certify')); 
} 
else if(showCheck==true)
{
//update campList;
if(insertBDapprovalList.size()>0)
insert insertBDapprovalList;
system.debug('check the list...'+insertBDapprovalList);

Email_addresses__c ec=Email_addresses__c.getvalues('ERApproval Email Address');
String EventGroupMail=ec.To_Address__c;

if(EventGroupMail != null)
{
DateTime sd=earList[0].Start_Date__c;
String startDate=sd.format('MMMMM dd, yyyy');
DateTime ed=earList[0].End_Date__c;
String endDate=ed.format('MMMMM dd, yyyy');
String description;
if(description==null)
{
description='';
}
else
{
description=earList[0].Comments__c;
}

String addRows=null;
for(BDApprovalHelper__c obj: insertBDapprovalList)
{ 
if(addRows!=null)
{
addRows=addRows+
'<tr><td style="padding:5px;">'
+obj.Contact_Name__c+
'</td><td style="padding:5px;">'
+obj.Status__c+
'</td><td style="padding:5px;">'
+obj.Comment__c+
'</td></tr>';
}
else
{
addRows=
'<tr><td style="padding:5px;">'
+obj.Contact_Name__c+
'</td><td style="padding:5px;">'
+obj.Status__c+
'</td><td style="padding:5px;">'
+obj.Comment__c+
'</td></tr>';
} 
} 

Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

/* for stage */
//String[] toAddresses = new String[] {EventGroupMail};
/* for prodution */
String[] toAddresses = new String[] {EventGroupMail};
mail.setToAddresses(toAddresses);
/* for stage */
//mail.setOrgWideEmailAddressId('0D2P0000000Cb0g');
/* for prodution */
mail.setOrgWideEmailAddressId('0D2500000004CuY');

//mail.setReplyTo('no-reply@colereit.com');
//mail.setSenderDisplayName('no-reply');

mail.setSubject('Event Members Approval Status: '+earList[0].name);
mail.setHtmlBody(
'Hi Event Group Member,</br></br>'+
'Please check below Event Members Approval Status Details:</br></br>'+
'Event Name:<b> '+earList[0].name+' </b></br>'+
'Start Date:<b> '+startDate+' </b></br>'+
'End Date:<b> '+endDate+' </b></br>'+
'Campaign:<b> '+earList[0].Campaign__r.Name+' </b></br>'+
'Broker/Dealer:<b> '+Name+' </b></br></br>'+
/*'Event Approver:<b> '+earList[0].Broker_Dealer_Name__r.Event_Approver__r.Name+' </b></br></br>'+*/

'Event Description:<b> '+description+' </b></br></br>'+

'<table border="1px" cellpadding="0" cellspacing="0" style="border:1px solid black;"><tr><td style="padding:5px;font-weight:bold">Member Name</td><td style="padding:5px;font-weight:bold">Status</td><td style="padding:5px;font-weight:bold">Comment</td></tr>'
+addRows+
'</table></br></br>'+

'Thanks</br>'+UserInfo.getFirstName()+' '+UserInfo.getLastName()+'</br>'
);
system.debug('check the addresses'+toAddresses );
system.debug('What is the email'+mail);
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
} 

// start - sending mail to pending campaign members 
/*
for(CampaignMember obj: campList)
{ 
if(obj.Contact.Email != null)
{
DateTime sd=earList[0].Start_Date__c;
String startDate=sd.format('MMMMM dd, yyyy');
DateTime ed=earList[0].End_Date__c;
String endDate=ed.format('MMMMM dd, yyyy');

Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

String[] toAddresses = new String[] {obj.Contact.Email};
mail.setToAddresses(toAddresses);
mail.setOrgWideEmailAddressId('0D2W000000000No');
mail.setSubject('Invitation request to an Event: '+earList[0].name); 
mail.setHtmlBody(
'Hi '+obj.Contact.Name+',</br></br>'+
'You are invited to an Event:<b> '+earList[0].name+' </b></br></br>'+
' Start Date:<b> '+startDate+' </b></br>'+
' End Date:<b> '+endDate+' </b></br>'+
' Campaign:<b> '+obj.Campaign.Name+' </b></br>'+
' Broker/Dealer:<b> '+obj.Contact.account.name+' </b></br></br>'+ 
'Thanks</br>'+earList[0].Broker_Dealer_Name__r.Event_Approver__r.Name
);
system.debug('check the addresses'+toAddresses );
//Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
} 
}
*/
// end - sending mail to pending campaign members

PageReference reference=new PageReference('/apex/ERApprovalPage?emid='+sMail);
reference.setRedirect(true);
return reference;
} 
return null;
}

public PageReference doAction2(){
norecs=true;
showNext=true;
PageReference reference=new PageReference('/apex/ERApprovalPage?emid='+sMail);
reference.setRedirect(true);
return reference; 
}

public PageReference doAction3(){
PageReference reference=new PageReference('/apex/ERHomePage');
reference.setRedirect(true);
return reference; 
}

/*public List<SelectOption> getItems() {
List<SelectOption> options = new List<SelectOption>(); 
options.add(new SelectOption('Approved','Approved'));
options.add(new SelectOption('Declined','Declined')); 
options.add(new SelectOption('Pending','Pending')); 
return options; 
}
*/
public class MyWrapper{ 
public String wName{get;set;}
public String wAccountName{get;set;}
public String wCity{get;set;}
public String wState{get;set;}
public Boolean wApproved{get;set;}
public Boolean wApproved1{get;set;}
public Boolean wApproved2{get;set;}
public Boolean wAttended{get;set;}
public String wComment{get;set;}
public Id wId{get;set;} 
}
}