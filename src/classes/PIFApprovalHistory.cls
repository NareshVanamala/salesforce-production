public with sharing class PIFApprovalHistory
 {
    public String quoteId1;
    public set<id>Subcaseid;
    public list<ProcessInstanceStep >plist;
    public list<ProcessInstance>plist1;
    Public list<PIF_Child__c>quote;
    public set<id>ProcessCaseid;
    public map<id,String> subjectCaseMap;
    public map<id, list<ProcessInstanceStep>> processinstancestepmap;
    public list<MyWrapper> wcampList;
    public String myValue;
  
    
    public List<MyWrapper> getwcampList () {
        
         system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> '+quoteId1);
         Subcaseid=new set<id>();
         quote=new list<PIF_Child__c>();
         ProcessCaseid= new set<id>();
         subjectCaseMap= new map<id,String>();
         processinstancestepmap= new map<id, list<ProcessInstanceStep>>();
         subjectCaseMap= new map<id,String>();
         plist= new list<ProcessInstanceStep>();
         plist1= new list<ProcessInstance>();
         wcampList= new list<MyWrapper>();

         wcampList= new list<MyWrapper>();

        quote=[select id,Name from PIF_Child__c where PIF_parent__c =:myValue and Name!=null]; 
         system.debug('---------myValue------------->'+myValue);
         system.debug('---------quote------------->'+quote.size());  
           
             for(PIF_Child__c c:quote)
             {
             Subcaseid.add(c.id);
             subjectCaseMap.put(c.id,c.Name);
             }
           system.debug('Check the map'+subjectCaseMap);   
           
         if(Subcaseid.size()>0)
        {
            plist1=[SELECT CreatedById,CreatedDate,Id,IsDeleted,LastModifiedById,LastModifiedDate,Status,SystemModstamp,TargetObjectId,TargetObject.Name FROM ProcessInstance where TargetObjectId in:Subcaseid ];
        }
         system.debug('Check the list of processInstance..'+plist1);  
        if(plist1.size()>0)
        {
            For(ProcessInstance Pc1:plist1){
              ProcessCaseid.add(Pc1.id);
              system.debug('>>>>>>>>>>>pc1>>>>>>>>>>>>> '+pc1);
              }
              //subjectCaseMap.put(Pc1.id,TargetObject.subject);
             plist=[Select SystemModstamp, StepStatus, ProcessInstanceId, OriginalActorId,Id, CreatedDate, CreatedById, Comments, ActorId From ProcessInstanceStep where StepStatus!='submitted'and ProcessInstanceId in:ProcessCaseid ];
             
        }
        system.debug('Check the list of processInstancesteps..'+plist);  
        for(ProcessInstanceStep ri:plist)
        {
          if(processinstancestepmap.containsKey(ri.ProcessInstanceId))
                 processinstancestepmap.get(ri.ProcessInstanceId).add(ri);
           else
               processinstancestepmap.put(ri.ProcessInstanceId, new List<ProcessInstanceStep>{ri});
             
        }
        system.debug('Check the map...'+processinstancestepmap);  
         MyWrapper w;
       for(ProcessInstance acc:plist1)
        {
            w = new MyWrapper();
            w.wAccseesRequestName=subjectCaseMap.get(acc.TargetObjectId) ;
            //w.wStatus= acc.StepStatus;
           // w.wApprover= acc.OriginalActorId;
            //w.wActualApprover=acc.ActorId; 
            //w.wDate= acc.SystemModstamp;
            //w.wComments= acc.Comments;
            w.wConList=processinstancestepmap.get(acc.id);
            system.debug('check the conlist'+w.wConList);
            wcampList.add(w);
        }
         
        system.debug('check the wrapperlist..'+wcampList);
        return wcampList;
    
    
    }
      
    public String getquoteId1() {
        system.debug('>>>>>>>>>>>>>> '+quoteId1);
        return quoteId1;
    }
    
     public void setquoteId1 (String s) {
         myValue=s;
     } 
  
     
  public class MyWrapper
  { 

    public String wAccseesRequestName{get;set;}
    public String wStatus{get;set;}
    public String wApprover{get;set;}
    public String wActualApprover{get;set;}
    public Date wDate{get;set;}
    public String wComments{get;set;}
    public List<ProcessInstanceStep> wConList{get;set;} 
    public MyWrapper()
    {
        this.wConList = new List<ProcessInstanceStep >();
     }
   }
     
      
 }