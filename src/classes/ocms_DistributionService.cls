public with sharing class ocms_DistributionService implements cms.ServiceInterface {

    public String JSONResponse {get; set;}
    private String action {get; set;}
    private Map<String, String> parameters {get; set;}
    private ocms_DistributionList wpl = new ocms_DistributionList();

    public ocms_DistributionService() {
        
    }

    public System.Type getType(){
        return ocms_DistributionService.class;
    }

    public String executeRequest(Map<String, String> p) {
        this.action = p.get('action');
        this.parameters = p; 
        this.LoadResponse();    
        return this.JSONResponse;
    }

    public void loadResponse(){
        
            if (this.action == 'getDistributionList'){

            String sharetype = parameters.get('sharetype');
            String year = parameters.get('year');
            


            this.getDistributionList(sharetype, year);
            
        }
    }

   
    private void getDistributionList(String sharetype, String year){
        this.JSONResponse = JSON.serialize(wpl.getDistributionList(sharetype, year));
    }

}