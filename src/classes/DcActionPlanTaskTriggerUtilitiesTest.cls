@isTest
private class DcActionPlanTaskTriggerUtilitiesTest {
 
 
  public static testMethod void testTrig1() {
      Test.startTest();
       Template__c ap = New Template__c(Name='Template');
       insert ap;
      
       update ap;
       delete ap;
       undelete ap;
      
       System.assertEquals(ap.Name,'Template');

        Task_Plan__c at =new Task_Plan__C();
        //at.id = 'a3TW00000009I5R';
        at.Name= 'COE Date';
        at.Priority__c='High';
        at.Assigned_To__c=Userinfo.getUserId();
        at.Date_ordered__c=Date.newInstance(1999,12, 12);
        at.Date_Received__c=Date.newInstance(1999,12, 12);
        at.Date_Completed__c=Date.newInstance(1999,12, 12);
        at.Field_to_update_Date_Received__c='Dead Date';
        at.Field_to_update_for_date_needed__c= 'Dead Date';
        at.Template__c = ap.id;
        insert at;
        
        Task_Plan__c at1 =new Task_Plan__C();
         //at1.id = 'a3TW00000009I5R';
         at1.Name= 'COE Date';
         at1.Priority__c='High';
         at1.Assigned_To__c=Userinfo.getUserId();
         at1.Date_ordered__c=Date.newInstance(1999,12, 12);
         at1.Date_Received__c=Date.newInstance(1999,12, 12);
         at1.Date_Completed__c=Date.newInstance(1999,12, 12);
         at1.Field_to_update_Date_Received__c='Dead Date';
         at1.Field_to_update_for_date_needed__c= 'Dead Date';
         at1.Template__c = ap.id;
         insert at1;
          
      // create the related Task
       Task t = new Task();
       t.Subject  = at.Name;
        t.Priority = at.Priority__c;
        t.OwnerId  = at.Assigned_To__c;
        t.Date_Received__c  = at.Date_received__c;
        t.Date_Ordered__c   = at.Date_ordered__c;
        t.Date_completed__c  = at.Date_completed__c;
        t.ActivityDate      = at.Date_Needed__c;
        t.Action_Plan_Task__c = at.Id; 
       insert t;
       t.Subject  = 'exampq';
       update t;
       delete t;
       
       
       Task t1 = new Task();
        t1.Subject  = at1.Name;
        t1.Priority = at1.Priority__c;
        t1.OwnerId  = at1.Assigned_To__c;
        t1.Date_Received__c  = at1.Date_received__c;
        t1.Date_Ordered__c   = at1.Date_ordered__c;
        t1.Date_completed__c  = at1.Date_completed__c;
        t1.ActivityDate      = at1.Date_Needed__c;
        t1.Action_Plan_Task__c = at1.Id; 
        insert t1;
        t1.Subject  = 'exampq';
        update t1;
        delete t1;
        undelete t1;
       
   }
   }