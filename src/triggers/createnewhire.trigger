trigger createnewhire on NewHireFormProcess__c (after update) 
{ 
    list<id> caseid= new list<id>();
    List<Asset__c> assetlist = new List<Asset__c>();
    Asset__c asset = new Asset__c();
    list<NewHireFormProcess__c> ChildCaselist= new list<NewHireFormProcess__c>();
    List<Employee__c>emplist= new list<Employee__c>();
    List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();
    String pickval='';
    public string departmentName;
    public string departmentexco;
    public string Applicationowner;
    public string ApplicationImplementor;

    List<NewHireFormProcess__c >inserlistOfcase= new list<NewHireFormProcess__c >();
    Id NewhireRequest=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormParent' and SobjectType = 'NewHireFormProcess__c'].id; 
    Id NewhireRequestSubcase=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormChild' and SobjectType = 'NewHireFormProcess__c'].id; 
    
    
     for(NewHireFormProcess__c n: Trigger.new)
    {
       if((n.recordtypeid==NewhireRequest)&&(n.HR_ManagerApprovalStatus__c=='Approved')&& (n.status__c!='closed'))
        {
            //-------Access Requested------
            
            if(n.MRI_Access_Requested__c==true)
             
                { 
                  
             NewHireFormProcess__c n1 = new NewHireFormProcess__c ();
             departmentName= 'MRI Request';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                     
                   }
             n1.Subject__c = departmentName;
             n1.MRI_Request_Status__c='Submitted for Approval';
             n1.Supervisor__c=n.Supervisor__c;
             n1.MRI_Access_Requested__c = true;
             n1.If_MRI_Access_is_Needed_Indicate_Secur__c = n.If_MRI_Access_is_Needed_Indicate_Secur__c;
             n1.HR_ManagerApprovalStatus__c ='Approved';
             n1.recordtypeid=NewhireRequestSubcase;
             n1.AccessRequest__c = n.id;
             n1.First_Name__c= n.First_Name__c;
             n1.Last_Name__c=n.Last_Name__c;
             n1.Supervisor__c=n.Supervisor__c;
             n1.Office_Location__c=n.Office_Location__c;
             n1.Employee_Role__c=n.Employee_Role__c;
             n1.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n1.Title__c=n.Title__c; 
             n1.Start_Date__c = n.Start_Date__c;
             n1.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n1.Application_owner__c=Applicationowner;
             n1.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n1);
            }
        
         if(n.Salesforce_Deal_Central_Access_Required__c==true)
         {
         
                NewHireFormProcess__c n2 = new NewHireFormProcess__c ();
             departmentName= 'Salesforce';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                   }
         
           
              n2.SalesForce_Deal_Central_Request_Status__c='Submitted for Approval';
              n2.Subject__c=departmentName;
              n2.Supervisor__c=n.Supervisor__c;
              n2.Salesforce_Deal_Central_Access_Required__c = true; 
              n2.HR_ManagerApprovalStatus__c ='Approved';  
              n2.recordtypeid=NewhireRequestSubcase;
              n2.AccessRequest__c = n.id;
              n2.First_Name__c=n.First_Name__c;
              n2.Last_Name__c=n.Last_Name__c;
              n2.Supervisor__c=n.Supervisor__c;
              n2.Office_Location__c=n.Office_Location__c;
              n2.Employee_Role__c=n.Employee_Role__c;
              n2.DepartmentUpdate__c=n.DepartmentUpdate__c;
              n2.Title__c=n.Title__c; 
              n2.Start_Date__c = n.Start_Date__c;
              n2.FINRA_Licensed__c = n.FINRA_Licensed__c;             
              n2.Application_owner__c=Applicationowner;
              n2.Application_Implementor1__c = ApplicationImplementor;
              inserlistOfcase.add(n2);
          }
         
         
         
        if(n.Webex_Access_Requested__c==true)
           {  
            
             NewHireFormProcess__c n3 = new NewHireFormProcess__c ();
             departmentName= 'Webex';
             departmentexco = n.DepartmentUpdate__c;
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      //Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                      //getting exco from Event automation
                      if(departmetnITEvent.Exco__c == true){
                        EventAutonation__c  ev=EventAutonation__c.getvalues(departmentexco);
                        Applicationowner = ev.Department_Exco__c;
                    }
                   }
                   
             n3.Subject__c = departmentName;
             n3.Webex_Access_Request_Status__c='Submitted for Approval';
             n3.Supervisor__c=n.Supervisor__c;
             n3.Webex_Access_Requested__c= true;
             n3.HR_ManagerApprovalStatus__c ='Approved';
             n3.recordtypeid=NewhireRequestSubcase;
             n3.AccessRequest__c = n.id;
             n3.First_Name__c=n.First_Name__c;
             n3.Last_Name__c=n.Last_Name__c;
             n3.Supervisor__c=n.Supervisor__c;
             n3.Office_Location__c=n.Office_Location__c;
             n3.Employee_Role__c=n.Employee_Role__c;
             n3.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n3.Title__c=n.Title__c; 
             n3.Start_Date__c = n.Start_Date__c;
             n3.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n3.Application_owner__c=Applicationowner;
             n3.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n3);
         }
           
        if(n.VDI_Access_Requested__c==true)
           {  
             NewHireFormProcess__c n5 = new NewHireFormProcess__c ();
             departmentName= 'VDI Access';
             departmentexco = n.DepartmentUpdate__c;
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      //Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                      if(departmetnITEvent.Exco__c == true){
                        EventAutonation__c  ev=EventAutonation__c.getvalues(departmentexco);
                        Applicationowner = ev.Department_Exco__c;
                    }
                   }
             n5.Subject__c=departmentName;
             n5.VDI_Access_Request_Status__c='Submitted for Approval';
             n5.Supervisor__c=n.Supervisor__c;
             n5.VDI_Access_Requested__c= true;
             n5.HR_ManagerApprovalStatus__c ='Approved';
             n5.recordtypeid=NewhireRequestSubcase;
             n5.AccessRequest__c = n.id;
             n5.First_Name__c=n.First_Name__c;
             n5.Last_Name__c=n.Last_Name__c;
             n5.Supervisor__c=n.Supervisor__c;
             n5.Office_Location__c=n.Office_Location__c;
             n5.Employee_Role__c=n.Employee_Role__c;
             n5.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n5.Title__c=n.Title__c; 
             n5.Start_Date__c = n.Start_Date__c;
             n5.FINRA_Licensed__c = n.FINRA_Licensed__c;    
             n5.Application_owner__c=Applicationowner;
             n5.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n5);
            }
        if(n.VPN_Access_Requested__c==true)
           {  
           NewHireFormProcess__c n6 = new NewHireFormProcess__c ();
           departmentName= 'VPN Access';
           departmentexco = n.DepartmentUpdate__c;
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      //Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                      if(departmetnITEvent.Exco__c == true){
                        EventAutonation__c  ev=EventAutonation__c.getvalues(departmentexco);
                        Applicationowner = ev.Department_Exco__c;
                    }
                   }
           
             n6.Subject__c=departmentName;
             n6.VPN_Access_Request_Status__c='Submitted for Approval';
             n6.Supervisor__c=n.Supervisor__c;
             n6.VPN_Access_Requested__c= true;
             n6.HR_ManagerApprovalStatus__c ='Approved';
             n6.recordtypeid=NewhireRequestSubcase;
             n6.AccessRequest__c = n.id;
             n6.First_Name__c=n.First_Name__c;
             n6.Last_Name__c=n.Last_Name__c;
             n6.Supervisor__c=n.Supervisor__c;
             n6.Office_Location__c=n.Office_Location__c;
             n6.Employee_Role__c=n.Employee_Role__c;
             n6.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n6.Title__c=n.Title__c; 
             n6.Start_Date__c = n.Start_Date__c;
             n6.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n6.Application_owner__c=Applicationowner;
             n6.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n6);
                 
           }
        if(n.Argus_Access_Requested__c==true)
           { 
               NewHireFormProcess__c n7 = new NewHireFormProcess__c ();
              departmentName= 'Argus';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                   }
            
             n7.Subject__c = departmentName;
             n7.Argus_Access_Request_Status__c='Submitted for Approval';
             n7.Supervisor__c=n.Supervisor__c;
             n7.Argus_Access_Requested__c= true;
             n7.HR_ManagerApprovalStatus__c ='Approved';
             n7.recordtypeid=NewhireRequestSubcase;
             n7.AccessRequest__c = n.id;
             n7.First_Name__c=n.First_Name__c;
             n7.Last_Name__c=n.Last_Name__c;
             n7.Supervisor__c=n.Supervisor__c;
             n7.Office_Location__c=n.Office_Location__c;
             n7.Employee_Role__c=n.Employee_Role__c;
             n7.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n7.Title__c=n.Title__c; 
             n7.Start_Date__c = n.Start_Date__c;
             n7.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n7.Application_owner__c=Applicationowner;
             n7.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n7);
            }
       
       if(n.DST_Access_Requested__c==true)
           {  
           NewHireFormProcess__c n8 = new NewHireFormProcess__c ();
              departmentName= 'DST';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                   }
             
             n8.Subject__c=departmentName;
             n8.DST_Access_Requested_Status__c='Submitted for Approval';
             n8.Supervisor__c=n.Supervisor__c;
             n8.DST_Access_Requested__c= true;
             n8.HR_ManagerApprovalStatus__c ='Approved';
             n8.recordtypeid=NewhireRequestSubcase;
             n8.AccessRequest__c = n.id;
             n8.First_Name__c=n.First_Name__c;
             n8.Last_Name__c=n.Last_Name__c;
             n8.Supervisor__c=n.Supervisor__c;
             n8.Office_Location__c=n.Office_Location__c;
             n8.Employee_Role__c=n.Employee_Role__c;
             n8.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n8.Title__c=n.Title__c; 
             n8.Start_Date__c = n.Start_Date__c;
             n8.FINRA_Licensed__c = n.FINRA_Licensed__c;
            n8.Application_owner__c=Applicationowner;
             n8.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n8);
            }
            
        if(n.AVID_Account_Requested__c==true)
           {  
            NewHireFormProcess__c n9 = new NewHireFormProcess__c ();
              departmentName= 'AVID';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                   }
            
             n9.Subject__c= departmentName;
             n9.AVID_Account_Request_Status__c='Submitted for Approval';
             n9.Supervisor__c=n.Supervisor__c;
             n9.AVID_Account_Requested__c= true;
             n9.HR_ManagerApprovalStatus__c ='Approved';
             n9.recordtypeid=NewhireRequestSubcase;
             n9.AccessRequest__c = n.id;
             n9.First_Name__c=n.First_Name__c;
             n9.Last_Name__c=n.Last_Name__c;
             n9.Supervisor__c=n.Supervisor__c;
             n9.Office_Location__c=n.Office_Location__c;
             n9.Employee_Role__c=n.Employee_Role__c;
             n9.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n9.Title__c=n.Title__c; 
             n9.Start_Date__c = n.Start_Date__c;
             n9.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n9.Application_owner__c=Applicationowner;
             n9.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n9);
             }
        
        if(n.BNA_Access_Requested__c==true)
           {  
          NewHireFormProcess__c n10 = new NewHireFormProcess__c ();
              departmentName= 'BNA Fixed Assets';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                   }
             
             n10.Subject__c = departmentName;
             n10.BNA_Access_Request_Status__c='Submitted for Approval';
             n10.Supervisor__c=n.Supervisor__c;
             n10.BNA_Access_Requested__c= true;
             n10.HR_ManagerApprovalStatus__c ='Approved';
             n10.recordtypeid=NewhireRequestSubcase;
             n10.AccessRequest__c = n.id;
             n10.First_Name__c=n.First_Name__c;
             n10.Last_Name__c=n.Last_Name__c;
             n10.Supervisor__c=n.Supervisor__c;
             n10.Office_Location__c=n.Office_Location__c;
             n10.Employee_Role__c=n.Employee_Role__c;
             n10.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n10.Title__c=n.Title__c; 
             n10.Start_Date__c = n.Start_Date__c;
             n10.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n10.Application_owner__c=Applicationowner;
             n10.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n10);
            }
           
         if(n.Concur_Account_Requested__c==true)
           {  
           NewHireFormProcess__c n11 = new NewHireFormProcess__c ();
              departmentName= 'Concur';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                   }
             
             
             n11.Subject__c=departmentName;
             n11.Concur_Account_Request_Status__c='Submitted for Approval';
             n11.Supervisor__c=n.Supervisor__c;
             n11.Concur_Account_Requested__c= true;
             n11.HR_ManagerApprovalStatus__c ='Approved';
             n11.recordtypeid=NewhireRequestSubcase;
             n11.AccessRequest__c = n.id;
             n11.First_Name__c=n.First_Name__c;
             n11.Last_Name__c=n.Last_Name__c;
             n11.Supervisor__c=n.Supervisor__c;
             n11.Office_Location__c=n.Office_Location__c;
             n11.Employee_Role__c=n.Employee_Role__c;
             n11.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n11.Title__c=n.Title__c; 
             n11.Start_Date__c = n.Start_Date__c;
             n11.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n11.Application_owner__c=Applicationowner;
             n11.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n11);
             
           }
          if(n.Data_Warehouse_GL_Requested__c==true)
           {  
            NewHireFormProcess__c n12 = new NewHireFormProcess__c ();
              departmentName= 'Data Warehouse GL';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                   }
             
           
             n12.Subject__c=departmentName;
             n12.Data_Warehouse_GL_Request_Status__c='Submitted for Approval';
             n12.Supervisor__c=n.Supervisor__c;
             n12.Data_Warehouse_GL_Requested__c= true;
             n12.HR_ManagerApprovalStatus__c ='Approved';
             n12.recordtypeid=NewhireRequestSubcase;
             n12.AccessRequest__c = n.id;
             n12.First_Name__c=n.First_Name__c;
             n12.Last_Name__c=n.Last_Name__c;
             n12.Supervisor__c=n.Supervisor__c;
             n12.Office_Location__c=n.Office_Location__c;
             n12.Employee_Role__c=n.Employee_Role__c;
             n12.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n12.Title__c=n.Title__c; 
             n12.Start_Date__c = n.Start_Date__c;
             n12.FINRA_Licensed__c = n.FINRA_Licensed__c;
              n12.Application_owner__c=Applicationowner;
             n12.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n12);
             
           }
           
         if(n.Data_Warehouse_Sales_Requested__c==true)
           {  
             NewHireFormProcess__c n13 = new NewHireFormProcess__c ();
              departmentName= 'Data Warehouse Sales';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                   }
           
             n13.Subject__c=departmentName;
             n13.Data_Warehouse_SalesRequestStatus__c='Submitted for Approval';
             n13.Supervisor__c=n.Supervisor__c;
             n13.Data_Warehouse_Sales_Requested__c= true;
             n13.HR_ManagerApprovalStatus__c ='Approved';
             n13.recordtypeid=NewhireRequestSubcase;
             n13.AccessRequest__c = n.id;
             n13.First_Name__c=n.First_Name__c;
             n13.Last_Name__c=n.Last_Name__c;
             n13.Supervisor__c=n.Supervisor__c;
             n13.Office_Location__c=n.Office_Location__c;
             n13.Employee_Role__c=n.Employee_Role__c;
             n13.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n13.Title__c=n.Title__c; 
             n13.Start_Date__c = n.Start_Date__c;
             n13.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n13.Application_owner__c=Applicationowner;
             n13.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n13);
             
           }
          
          if(n.Chatham_Requested__c==true)
           {  
              NewHireFormProcess__c n14 = new NewHireFormProcess__c ();
              departmentName= 'Chatham Access';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
           }
             n14.Subject__c=departmentName;
             n14.Chatham_Request_Status__c='Submitted for Approval';
             n14.Supervisor__c=n.Supervisor__c;
             n14.Chatham_Requested__c= true;
             n14.HR_ManagerApprovalStatus__c ='Approved';
             n14.recordtypeid=NewhireRequestSubcase;
             n14.AccessRequest__c = n.id;
             n14.First_Name__c=n.First_Name__c;
             n14.Last_Name__c=n.Last_Name__c;
             n14.Supervisor__c=n.Supervisor__c;
             n14.Office_Location__c=n.Office_Location__c;
             n14.Employee_Role__c=n.Employee_Role__c;
             n14.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n14.Title__c=n.Title__c; 
             n14.Start_Date__c = n.Start_Date__c;
             n14.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n14.Application_owner__c=Applicationowner;
             n14.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n14);
             
           }
          if(n.OneSource_Requested__c==true)
           {  
            NewHireFormProcess__c n14 = new NewHireFormProcess__c ();
              departmentName= 'OneSource Request';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
           }
             NewHireFormProcess__c n15 = new NewHireFormProcess__c ();
             n15.Subject__c=departmentName;
             n15.OneSource_Request_Status__c='Submitted for Approval';
             n15.Supervisor__c=n.Supervisor__c;
             n15.OneSource_Requested__c= true;
             n15.HR_ManagerApprovalStatus__c ='Approved';
             n15.recordtypeid=NewhireRequestSubcase;
             n15.AccessRequest__c = n.id;
             n15.First_Name__c=n.First_Name__c;
             n15.Last_Name__c=n.Last_Name__c;
             n15.Supervisor__c=n.Supervisor__c;
             n15.Office_Location__c=n.Office_Location__c;
             n15.Employee_Role__c=n.Employee_Role__c;
             n15.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n15.Title__c=n.Title__c; 
             n15.Start_Date__c = n.Start_Date__c;
             n15.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n15.Application_owner__c=Applicationowner;
             n15.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n15);
            }
          
          if(n.Web_Filings_Requested__c==true)
           {  
           NewHireFormProcess__c n16 = new NewHireFormProcess__c ();
              departmentName= 'Web Filings';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
           }
            
             n16.Subject__c=departmentName;
             n16.Web_Filings_Request_Status__c='Submitted for Approval';
             n16.Supervisor__c=n.Supervisor__c;
             n16.Web_Filings_Requested__c= true;
             n16.HR_ManagerApprovalStatus__c ='Approved';
             n16.recordtypeid=NewhireRequestSubcase;
             n16.AccessRequest__c = n.id;
             n16.First_Name__c=n.First_Name__c;
             n16.Last_Name__c=n.Last_Name__c;
             n16.Supervisor__c=n.Supervisor__c;
             n16.Office_Location__c=n.Office_Location__c;
             n16.Employee_Role__c=n.Employee_Role__c;
             n16.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n16.Title__c=n.Title__c; 
             n16.Start_Date__c = n.Start_Date__c;
             n16.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n16.Application_owner__c=Applicationowner;
             n16.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n16);
             
           }
            if(n.Kardin_Requested__c==true)
           {  
            NewHireFormProcess__c n17 = new NewHireFormProcess__c ();
              departmentName= 'Kardin Request';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
           }
             
             n17.Subject__c=departmentName;
             n17.Kardin_Request_Status__c='Submitted for Approval';
             n17.Supervisor__c=n.Supervisor__c;
             n17.Kardin_Requested__c= true;
             n17.HR_ManagerApprovalStatus__c ='Approved';
             n17.recordtypeid=NewhireRequestSubcase;
             n17.AccessRequest__c = n.id;
             n17.First_Name__c=n.First_Name__c;
             n17.Last_Name__c=n.Last_Name__c;
             n17.Supervisor__c=n.Supervisor__c;
             n17.Office_Location__c=n.Office_Location__c;
             n17.Employee_Role__c=n.Employee_Role__c;
             n17.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n17.Title__c=n.Title__c; 
             n17.Start_Date__c = n.Start_Date__c;
             n17.FINRA_Licensed__c = n.FINRA_Licensed__c;
            n17.Application_owner__c=Applicationowner;
             n17.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n17);
             
           }
           if(n.RisKonnect_Requested__c==true)
           { 
                NewHireFormProcess__c n18 = new NewHireFormProcess__c ();
             departmentName= 'RisKonnect';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
           }
                       
             
             n18.Subject__c=departmentName;
             n18.RisKonnect_Request_Status__c='Submitted for Approval';
             n18.Supervisor__c=n.Supervisor__c;
             n18.RisKonnect_Requested__c= true;
             n18.HR_ManagerApprovalStatus__c ='Approved';
             n18.recordtypeid=NewhireRequestSubcase;
             n18.AccessRequest__c = n.id;
             n18.First_Name__c=n.First_Name__c;
             n18.Last_Name__c=n.Last_Name__c;
             n18.Supervisor__c=n.Supervisor__c;
             n18.Office_Location__c=n.Office_Location__c;
             n18.Employee_Role__c=n.Employee_Role__c;
             n18.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n18.Title__c=n.Title__c; 
             n18.Start_Date__c = n.Start_Date__c;
             n18.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n18.Application_owner__c=Applicationowner;
             n18.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n18);
             
           }
           if(n.ADP_Requested__c==true)
           {  
            NewHireFormProcess__c n19 = new NewHireFormProcess__c ();
              departmentName= 'ADP Access';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
           }
             
             n19.Subject__c = departmentName;
             n19.ADP_Request_Status__c='Submitted for Approval';
             n19.Supervisor__c=n.Supervisor__c;
             n19.ADP_Requested__c= true;
             n19.HR_ManagerApprovalStatus__c ='Approved';
             n19.recordtypeid=NewhireRequestSubcase;
             n19.AccessRequest__c = n.id;
             n19.First_Name__c=n.First_Name__c;
             n19.Last_Name__c=n.Last_Name__c;
             n19.Supervisor__c=n.Supervisor__c;
             n19.Office_Location__c=n.Office_Location__c;
             n19.Employee_Role__c=n.Employee_Role__c;
             n19.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n19.Title__c=n.Title__c; 
             n19.Start_Date__c = n.Start_Date__c;
             n19.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n19.Application_owner__c=Applicationowner;
             n19.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n19);
             
           }

           if(n.Box_com_Requested__c==true)
           {  
            NewHireFormProcess__c n20 = new NewHireFormProcess__c ();
             departmentName= 'Box.com Access';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                   }
            
             n20.Subject__c= departmentName;
             n20.Box_com_Request_Status__c='Submitted for Approval';
             n20.Supervisor__c=n.Supervisor__c;
             n20.Box_com_Requested__c= true;
             n20.HR_ManagerApprovalStatus__c ='Approved';
             n20.recordtypeid=NewhireRequestSubcase;
             n20.AccessRequest__c = n.id;
             n20.First_Name__c=n.First_Name__c;
             n20.Last_Name__c=n.Last_Name__c;
             n20.Supervisor__c=n.Supervisor__c;
             n20.Office_Location__c=n.Office_Location__c;
             n20.Employee_Role__c=n.Employee_Role__c;
             n20.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n20.Title__c=n.Title__c; 
             n20.Start_Date__c = n.Start_Date__c;
             n20.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n20.Application_owner__c=Applicationowner;
             n20.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n20);
           }
           
           if(n.Data_Warehouse_Real_Estate_Requested__c==true)
           {  
              NewHireFormProcess__c n21 = new NewHireFormProcess__c ();
              departmentName= 'Data Warehouse Real Estate';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                   }
           
             n21.Subject__c=departmentName;
             n21.DataWarehouse_RealEstate_RequestStatus__c='Submitted for Approval';
             n21.Supervisor__c=n.Supervisor__c;
             n21.Data_Warehouse_Real_Estate_Requested__c= true;
             n21.HR_ManagerApprovalStatus__c ='Approved';
             n21.recordtypeid=NewhireRequestSubcase;
             n21.AccessRequest__c = n.id;
             n21.First_Name__c=n.First_Name__c;
             n21.Last_Name__c=n.Last_Name__c;
             n21.Supervisor__c=n.Supervisor__c;
             n21.Office_Location__c=n.Office_Location__c;
             n21.Employee_Role__c=n.Employee_Role__c;
             n21.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n21.Title__c=n.Title__c; 
             n21.Start_Date__c = n.Start_Date__c;
             n21.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n21.Application_owner__c=Applicationowner;
             n21.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n21);
             
           }
        //-------Building Access-------//  
        
        if(n.General__c==true)
           {
             departmentName= 'Building General Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }  
             NewHireFormProcess__c n22 = new NewHireFormProcess__c ();
             n22.Subject__c=departmentName;
             n22.Building_General_Status__c='Submitted for Approval';
             n22.Supervisor__c=n.Supervisor__c;
             n22.General__c= true;
             n22.HR_ManagerApprovalStatus__c ='Approved';
             n22.recordtypeid=NewhireRequestSubcase;
             n22.AccessRequest__c = n.id;
             n22.First_Name__c=n.First_Name__c;
             n22.Last_Name__c=n.Last_Name__c;
             n22.Supervisor__c=n.Supervisor__c;
             n22.Office_Location__c=n.Office_Location__c;
             n22.Employee_Role__c=n.Employee_Role__c;
             n22.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n22.Title__c=n.Title__c;
             n22.Start_Date__c = n.Start_Date__c;
             n22.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n22.Application_owner__c=Applicationowner;
             n22.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n22);
           }
           
        if(n.Facilities__c==true)
           { 
             departmentName= 'Building Facilities Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }         
             NewHireFormProcess__c n23 = new NewHireFormProcess__c ();
             n23.Subject__c=departmentName;
             n23.Building_Facilities_Status__c='Submitted for Approval';
             n23.Supervisor__c=n.Supervisor__c;
             n23.Facilities__c= true;
             n23.HR_ManagerApprovalStatus__c ='Approved';
             n23.recordtypeid=NewhireRequestSubcase;
             n23.AccessRequest__c = n.id;
             n23.First_Name__c=n.First_Name__c;
             n23.Last_Name__c=n.Last_Name__c;
             n23.Supervisor__c=n.Supervisor__c;
             n23.Office_Location__c=n.Office_Location__c;
             n23.Employee_Role__c=n.Employee_Role__c;
             n23.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n23.Title__c=n.Title__c; 
             n23.Start_Date__c = n.Start_Date__c;
             n23.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n23.Application_owner__c=Applicationowner;
             n23.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n23);
             
           }
        
        if(n.RE_Legal__c==true)
           { 
             departmentName= 'Building RE Legal Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             } 
             NewHireFormProcess__c n24 = new NewHireFormProcess__c ();
             n24.Subject__c=departmentName;
             n24.Building_RELegal_Status__c='Submitted for Approval';
             n24.Supervisor__c=n.Supervisor__c;
             n24.RE_Legal__c= true;
             n24.HR_ManagerApprovalStatus__c ='Approved';
             n24.recordtypeid=NewhireRequestSubcase;
             n24.AccessRequest__c = n.id;
             n24.First_Name__c=n.First_Name__c;
             n24.Last_Name__c=n.Last_Name__c;
             n24.Supervisor__c=n.Supervisor__c;
             n24.Office_Location__c=n.Office_Location__c;
             n24.Employee_Role__c=n.Employee_Role__c;
             n24.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n24.Title__c=n.Title__c; 
             n24.Start_Date__c = n.Start_Date__c;
             n24.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n24.Application_owner__c=Applicationowner;
             n24.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n24);
             
           }
           
        if(n.IT_Infrastructure__c==true)
           { 
             departmentName= 'Building IT Infrastructure Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             } 
             NewHireFormProcess__c n25 = new NewHireFormProcess__c ();
             n25.Subject__c=departmentName;
             n25.Building_IT_Infrastructure_Status__c='Submitted for Approval';
             n25.Supervisor__c=n.Supervisor__c;
             n25.IT_Infrastructure__c= true;
             n25.HR_ManagerApprovalStatus__c ='Approved';
             n25.recordtypeid=NewhireRequestSubcase;
             n25.AccessRequest__c = n.id;
             n25.First_Name__c=n.First_Name__c;
             n25.Last_Name__c=n.Last_Name__c;
             n25.Supervisor__c=n.Supervisor__c;
             n25.Office_Location__c=n.Office_Location__c;
             n25.Employee_Role__c=n.Employee_Role__c;
             n25.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n25.Title__c=n.Title__c; 
             n25.Start_Date__c = n.Start_Date__c;
             n25.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n25.Application_owner__c=Applicationowner;
             n25.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n25);
             
           }
        if(n.HR__c==true)
           { 
             departmentName= 'Building HR Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }         
             NewHireFormProcess__c n26 = new NewHireFormProcess__c ();
             n26.Subject__c=departmentName;
             n26.Building_HR_Status__c='Submitted for Approval';
             n26.Supervisor__c=n.Supervisor__c;
             n26.HR__c= true;
             n26.HR_ManagerApprovalStatus__c ='Approved';
             n26.recordtypeid=NewhireRequestSubcase;
             n26.AccessRequest__c = n.id;
             n26.First_Name__c=n.First_Name__c;
             n26.Last_Name__c=n.Last_Name__c;
             n26.Supervisor__c=n.Supervisor__c;
             n26.Office_Location__c=n.Office_Location__c;
             n26.Employee_Role__c=n.Employee_Role__c;
             n26.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n26.Title__c=n.Title__c; 
             n26.Start_Date__c = n.Start_Date__c;
             n26.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n26.Application_owner__c=Applicationowner;
             n26.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n26);
             
           }
        if(n.Compliance__c==true)
           {  
             departmentName= 'Building Compliance Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }         
             NewHireFormProcess__c n27 = new NewHireFormProcess__c ();
             n27.Subject__c=departmentName;
             n27.Building_Compliance_Status__c='Submitted for Approval';
             n27.Supervisor__c=n.Supervisor__c;
             n27.Compliance__c= true;
             n27.HR_ManagerApprovalStatus__c ='Approved';
             n27.recordtypeid=NewhireRequestSubcase;
             n27.AccessRequest__c = n.id;
             n27.First_Name__c=n.First_Name__c;
             n27.Last_Name__c=n.Last_Name__c;
             n27.Supervisor__c=n.Supervisor__c;
             n27.Office_Location__c=n.Office_Location__c;
             n27.Employee_Role__c=n.Employee_Role__c;
             n27.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n27.Title__c=n.Title__c; 
             n27.Start_Date__c = n.Start_Date__c;
             n27.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n27.Application_owner__c=Applicationowner;
             n27.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n27);
             
           }   
        if(n.Property_Management__c==true)
           { 
             departmentName= 'Building Property Management Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }         
             NewHireFormProcess__c n28 = new NewHireFormProcess__c ();
             n28.Subject__c=departmentName;
             n28.Building_PropertyManagement_Status__c='Submitted for Approval';
             n28.Supervisor__c=n.Supervisor__c;
             n28.Property_Management__c= true;
             n28.HR_ManagerApprovalStatus__c ='Approved';
             n28.recordtypeid=NewhireRequestSubcase;
             n28.AccessRequest__c = n.id;
             n28.First_Name__c=n.First_Name__c;
             n28.Last_Name__c=n.Last_Name__c;
             n28.Supervisor__c=n.Supervisor__c;
             n28.Office_Location__c=n.Office_Location__c;
             n28.Employee_Role__c=n.Employee_Role__c;
             n28.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n28.Title__c=n.Title__c; 
             n28.Start_Date__c = n.Start_Date__c;
             n28.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n28.Application_owner__c=Applicationowner;
             n28.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n28);
             
           }   
        if(n.Audio_Visual__c==true)
           { 
             departmentName= 'Building Audio/Visual Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }         
             NewHireFormProcess__c n29 = new NewHireFormProcess__c ();
             n29.Subject__c=departmentName;
             n29.Building_Audio_Visual_Status__c='Submitted for Approval';
             n29.Supervisor__c=n.Supervisor__c;
             n29.Audio_Visual__c= true;
             n29.HR_ManagerApprovalStatus__c ='Approved';
             n29.recordtypeid=NewhireRequestSubcase;
             n29.AccessRequest__c = n.id;
             n29.First_Name__c=n.First_Name__c;
             n29.Last_Name__c=n.Last_Name__c;
             n29.Supervisor__c=n.Supervisor__c;
             n29.Office_Location__c=n.Office_Location__c;
             n29.Employee_Role__c=n.Employee_Role__c;
             n29.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n29.Title__c=n.Title__c; 
             n29.Start_Date__c = n.Start_Date__c;
             n29.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n29.Application_owner__c=Applicationowner;
             n29.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n29);
             
           }   
        if(n.Executive__c==true)
           { 
             departmentName= 'Building Executive Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }         
             NewHireFormProcess__c n30 = new NewHireFormProcess__c ();
             n30.Subject__c=departmentName;
             n30.Building_Executive_Status__c='Submitted for Approval';
             n30.Supervisor__c=n.Supervisor__c;
             n30.Executive__c= true;
             n30.HR_ManagerApprovalStatus__c ='Approved';
             n30.recordtypeid=NewhireRequestSubcase;
             n30.AccessRequest__c = n.id;
             n30.First_Name__c=n.First_Name__c;
             n30.Last_Name__c=n.Last_Name__c;
             n30.Supervisor__c=n.Supervisor__c;
             n30.Office_Location__c=n.Office_Location__c;
             n30.Employee_Role__c=n.Employee_Role__c;
             n30.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n30.Title__c=n.Title__c; 
             n30.Start_Date__c = n.Start_Date__c;
             n30.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n30.Application_owner__c=Applicationowner;
             n30.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n30);
             
           }
           
        if(n.B2_Storage__c==true)
           { 
             departmentName= 'Building B2 Storage Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }         
             NewHireFormProcess__c n31 = new NewHireFormProcess__c ();
             n31.Subject__c=departmentName;
             n31.Building_B2Storage_Status__c='Submitted for Approval ';
             n31.Supervisor__c=n.Supervisor__c;
             n31.B2_Storage__c= true;
             n31.HR_ManagerApprovalStatus__c ='Approved';
             n31.recordtypeid=NewhireRequestSubcase;
             n31.AccessRequest__c = n.id;
             n31.First_Name__c=n.First_Name__c;
             n31.Last_Name__c=n.Last_Name__c;
             n31.Supervisor__c=n.Supervisor__c;
             n31.Office_Location__c=n.Office_Location__c;
             n31.Employee_Role__c=n.Employee_Role__c;
             n31.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n31.Title__c=n.Title__c; 
             n31.Start_Date__c = n.Start_Date__c;
             n31.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n31.Application_owner__c=Applicationowner;
             n31.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n31);
            }
           
        //-------Parking-------// 
        if(n.Parking_General__c==true)
           { 
             departmentName= 'Parking General Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }                 
             NewHireFormProcess__c n4 = new NewHireFormProcess__c ();
             n4.Subject__c=departmentName;
             n4.Parking_General_Status__c='Submitted for Approval';
             n4.Supervisor__c=n.Supervisor__c;
             n4.Parking_General__c= true;
             n4.HR_ManagerApprovalStatus__c ='Approved';
             n4.recordtypeid=NewhireRequestSubcase;
             n4.AccessRequest__c = n.id;
             n4.First_Name__c=n.First_Name__c;
             n4.Last_Name__c=n.Last_Name__c;
             n4.Supervisor__c=n.Supervisor__c;
             n4.Office_Location__c=n.Office_Location__c;
             n4.Employee_Role__c=n.Employee_Role__c;
             n4.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n4.Title__c=n.Title__c; 
             n4.Start_Date__c = n.Start_Date__c;
             n4.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n4.Application_owner__c=Applicationowner;
             n4.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n4);
          }
          
        if(n.Parking_Executive__c==true)
           {  
             departmentName= 'Parking Executive Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }                 
             NewHireFormProcess__c n32 = new NewHireFormProcess__c ();
             n32.Subject__c=departmentName;
             n32.Parking_Executive_Status__c='Submitted for Approval';
             n32.Supervisor__c=n.Supervisor__c;
             n32.Parking_Executive__c= true;
             n32.HR_ManagerApprovalStatus__c ='Approved';
             n32.recordtypeid=NewhireRequestSubcase;
             n32.AccessRequest__c = n.id;
             n32.First_Name__c=n.First_Name__c;
             n32.Last_Name__c=n.Last_Name__c;
             n32.Supervisor__c=n.Supervisor__c;
             n32.Office_Location__c=n.Office_Location__c;
             n32.Employee_Role__c=n.Employee_Role__c;
             n32.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n32.Title__c=n.Title__c; 
             n32.Start_Date__c = n.Start_Date__c;
             n32.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n32.Application_owner__c=Applicationowner;
             n32.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n32);
           }           

           if(n.Parking_Contractor__c==true)
           { 
             departmentName= 'Parking Contractor Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }                 
             NewHireFormProcess__c n33 = new NewHireFormProcess__c ();
             n33.Subject__c=departmentName;
             n33.Parking_Contractor_request__c='Submitted for Approval';
             n33.Supervisor__c=n.Supervisor__c;
             n33.Parking_Contractor__c= true;
             n33.HR_ManagerApprovalStatus__c ='Approved';
             n33.recordtypeid=NewhireRequestSubcase;
             n33.AccessRequest__c = n.id;
             n33.First_Name__c=n.First_Name__c;
             n33.Last_Name__c=n.Last_Name__c;
             n33.Supervisor__c=n.Supervisor__c;
             n33.Office_Location__c=n.Office_Location__c;
             n33.Employee_Role__c=n.Employee_Role__c;
             n33.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n33.Title__c=n.Title__c; 
             n33.Start_Date__c = n.Start_Date__c;
             n33.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n33.Application_owner__c=Applicationowner;
             n33.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n33);             
           }
           
         //-------Hardware Needed-------// 
        if(n.Deploy_External_IT_Package__c==true)
           {
             departmentName= 'Hardware External IT Package Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }                 
             NewHireFormProcess__c n37 = new NewHireFormProcess__c ();
             n37.Subject__c=departmentName;
             n37.Hardware_Deploy_External__c='Submitted for Approval';
             n37.Supervisor__c=n.Supervisor__c;
             n37.Deploy_External_IT_Package__c = true;
             n37.HR_ManagerApprovalStatus__c ='Approved';
             n37.recordtypeid=NewhireRequestSubcase;
             n37.AccessRequest__c = n.id;
             n37.First_Name__c=n.First_Name__c;
             n37.Last_Name__c=n.Last_Name__c;
             n37.Supervisor__c=n.Supervisor__c;
             n37.Office_Location__c=n.Office_Location__c;
             n37.Employee_Role__c=n.Employee_Role__c;
             n37.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n37.Title__c=n.Title__c; 
             n37.Start_Date__c = n.Start_Date__c;
             n37.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n37.Application_owner__c=Applicationowner;
             n37.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n37);
             
           }
           
        if(n.Provide_Desktop__c==true)
           {
             departmentName= 'Hardware Desktop Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }                 
             NewHireFormProcess__c n38 = new NewHireFormProcess__c ();
             n38.Subject__c=departmentName;
             n38.Hardware_Provide_Desktop__c='Submitted for Approval';
             n38.Supervisor__c=n.Supervisor__c;
             n38.Provide_Desktop__c = true;
             n38.HR_ManagerApprovalStatus__c ='Approved';
             n38.recordtypeid=NewhireRequestSubcase;
             n38.AccessRequest__c = n.id;
             n38.First_Name__c=n.First_Name__c;
             n38.Last_Name__c=n.Last_Name__c;
             n38.Supervisor__c=n.Supervisor__c;
             n38.Office_Location__c=n.Office_Location__c;
             n38.Employee_Role__c=n.Employee_Role__c;
             n38.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n38.Title__c=n.Title__c; 
             n38.Start_Date__c = n.Start_Date__c;
             n38.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n38.Application_owner__c=Applicationowner;
             n38.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n38);
             
           }   
         
        if(n.Provide_Laptop__c==true)
           {
             departmentName= 'Hardware Laptop Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }                 
             NewHireFormProcess__c n39 = new NewHireFormProcess__c ();
             n39.Subject__c=departmentName;
             n39.Hardware_Provide_Laptop__c='Submitted for Approval';
             n39.Supervisor__c=n.Supervisor__c;
             n39.Provide_Laptop__c= true;
             n39.HR_ManagerApprovalStatus__c ='Approved';
             n39.recordtypeid=NewhireRequestSubcase;
             n39.AccessRequest__c = n.id;
             n39.First_Name__c=n.First_Name__c;
             n39.Last_Name__c=n.Last_Name__c;
             n39.Supervisor__c=n.Supervisor__c;
             n39.Office_Location__c=n.Office_Location__c;
             n39.Employee_Role__c=n.Employee_Role__c;
             n39.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n39.Title__c=n.Title__c; 
             n39.Start_Date__c = n.Start_Date__c;
             n39.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n39.Application_owner__c=Applicationowner;
             n39.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n39);
            }
            
        if(n.Dual_Monitor_Display__c==true)
           {
             departmentName= 'Hardware Dual Monitor Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }                 
             NewHireFormProcess__c n40 = new NewHireFormProcess__c ();
             n40.Subject__c=departmentName;
             n40.Hardware_Dual_Monitor_Display__c='Submitted for Approval';
             n40.Supervisor__c=n.Supervisor__c;
             n40.Dual_Monitor_Display__c= true;
             n40.HR_ManagerApprovalStatus__c ='Approved';
             n40.recordtypeid=NewhireRequestSubcase;
             n40.AccessRequest__c = n.id;
             n40.First_Name__c=n.First_Name__c;
             n40.Last_Name__c=n.Last_Name__c;
             n40.Supervisor__c=n.Supervisor__c;
             n40.Office_Location__c=n.Office_Location__c;
             n40.Employee_Role__c=n.Employee_Role__c;
             n40.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n40.Title__c=n.Title__c; 
             n40.Start_Date__c = n.Start_Date__c;
             n40.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n40.Application_owner__c=Applicationowner;
             n40.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n40);
           }
           
        if(n.Provide_iPhone__c==true)
           {
             departmentName= 'Hardware Iphone Request';
             if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
             {    
                New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                Applicationowner=departmetnITEvent.Application_Owner__c;
                ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
             }                 
             NewHireFormProcess__c n47 = new NewHireFormProcess__c ();
             n47.Subject__c=departmentName;
             n47.Hardware_Provide_Iphone__c ='Submitted for Approval';
             n47.Supervisor__c=n.Supervisor__c;
             n47.Provide_iPhone__c = true;
             n47.HR_ManagerApprovalStatus__c ='Approved';
             n47.recordtypeid=NewhireRequestSubcase;
             n47.AccessRequest__c = n.id;
             n47.First_Name__c=n.First_Name__c;
             n47.Last_Name__c=n.Last_Name__c;
             n47.Supervisor__c=n.Supervisor__c;
             n47.Office_Location__c=n.Office_Location__c;
             n47.Employee_Role__c=n.Employee_Role__c;
             n47.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n47.Title__c=n.Title__c; 
             n47.Start_Date__c = n.Start_Date__c;
             n47.FINRA_Licensed__c = n.FINRA_Licensed__c;
             n47.Application_owner__c=Applicationowner;
             n47.Application_Implementor1__c =ApplicationImplementor;
             inserlistOfcase.add(n47);
             
           }
           
        if(n.Provide_iPad__c==true)
           { 
             NewHireFormProcess__c n48 = new NewHireFormProcess__c ();
             departmentName= 'Hardware Ipad Request';
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                   }           
             
             n48.Subject__c=departmentName;
             n48.Hardware_Provide_Ipad__c='Submitted for Approval';
             n48.Supervisor__c=n.Supervisor__c;
             n48.Provide_iPad__c = true;
             n48.HR_ManagerApprovalStatus__c ='Approved';
             n48.recordtypeid=NewhireRequestSubcase;
             n48.AccessRequest__c = n.id;
             n48.First_Name__c=n.First_Name__c;
             n48.Last_Name__c=n.Last_Name__c;
             n48.Supervisor__c=n.Supervisor__c;
             n48.Office_Location__c=n.Office_Location__c;
             n48.Employee_Role__c=n.Employee_Role__c;
             n48.DepartmentUpdate__c=n.DepartmentUpdate__c;
             n48.Title__c=n.Title__c; 
             n48.Start_Date__c = n.Start_Date__c;
             n48.FINRA_Licensed__c = n.FINRA_Licensed__c;
              n48.Application_owner__c=Applicationowner;
              n48.Application_Implementor1__c = ApplicationImplementor;
             inserlistOfcase.add(n48);
           }
           
         //----Active Directory---//
         
        if(n.Active_Directory_Group_1__c!= null)
            { 
                departmentName= n.Active_Directory_Group_1__c;
                if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                {    
                    New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                    Applicationowner=departmetnITEvent.Application_Owner__c;
                    ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                }
                NewHireFormProcess__c n41 = new NewHireFormProcess__c ();
                n41.Subject__c=n.Active_Directory_Group_1__c;
                n41.Supervisor__c=n.Supervisor__c;
                n41.DepartmentUpdate__c= n.DepartmentUpdate__c;
                n41.HR_ManagerApprovalStatus__c ='Approved';
                n41.recordtypeid=NewhireRequestSubcase;
                n41.AccessRequest__c = n.id;
                n41.Active_Directory_Group1_Status__c ='Submitted for Approval';
                n41.Active_Directory_Group_1__c = departmentName;   
                n41.Application_owner__c=Applicationowner;
                n41.First_Name__c=n.First_Name__c;
                n41.Last_Name__c=n.Last_Name__c;
                n41.Supervisor__c=n.Supervisor__c;
                n41.Office_Location__c=n.Office_Location__c;
                n41.Employee_Role__c=n.Employee_Role__c;
                n41.DepartmentUpdate__c=n.DepartmentUpdate__c;
                n41.Title__c=n.Title__c; 
                n41.Start_Date__c = n.Start_Date__c;
                n41.FINRA_Licensed__c = n.FINRA_Licensed__c;
                n41.Application_Implementor1__c = ApplicationImplementor;
                inserlistOfcase.add(n41);
                }
                 if(n.Active_Directory_Group_2__c!= null)
                { 
                  departmentName= n.Active_Directory_Group_2__c;
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                  }
                  
                 NewHireFormProcess__c n42 = new NewHireFormProcess__c ();
                 n42.Subject__c=n.Active_Directory_Group_2__c;
                 n42.Supervisor__c=n.Supervisor__c;
                 n42.DepartmentUpdate__c= n.DepartmentUpdate__c;
                 n42.HR_ManagerApprovalStatus__c ='Approved';
                 n42.recordtypeid=NewhireRequestSubcase;
                 n42.AccessRequest__c = n.id;
                 n42.Active_Directory_Group2_Status__c='Submitted for Approval';
                 n42.Active_Directory_Group_2__c  = departmentName;   
                 n42.Application_owner__c=Applicationowner;
                n42.First_Name__c=n.First_Name__c;
                n42.Last_Name__c=n.Last_Name__c;
                n42.Supervisor__c=n.Supervisor__c;
                n42.Office_Location__c=n.Office_Location__c;
                n42.Employee_Role__c=n.Employee_Role__c;
                n42.DepartmentUpdate__c=n.DepartmentUpdate__c;
                n42.Title__c=n.Title__c; 
                n42.Start_Date__c = n.Start_Date__c;
                n42.FINRA_Licensed__c = n.FINRA_Licensed__c;
                n42.Application_Implementor1__c = ApplicationImplementor;
                inserlistOfcase.add(n42);
                }
                  
                 if(n.Active_Directory_Group_3__c!= null)
                { 
                  departmentName= n.Active_Directory_Group_3__c;
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                  }
                  
                  NewHireFormProcess__c n43 = new NewHireFormProcess__c ();
                  n43.Subject__c=n.Active_Directory_Group_3__c;
                  n43.Supervisor__c=n.Supervisor__c;
                  n43.DepartmentUpdate__c= n.DepartmentUpdate__c;
                  n43.HR_ManagerApprovalStatus__c ='Approved';
                  n43.recordtypeid=NewhireRequestSubcase;
                  n43.AccessRequest__c = n.id;
                  n43.Active_Directory_Group3_Status__c='Submitted for Approval';
                  n43.Active_Directory_Group_3__c  = departmentName;   
                  n43.Application_owner__c=Applicationowner;
                  n43.First_Name__c=n.First_Name__c;
                  n43.Last_Name__c=n.Last_Name__c;
                  n43.Supervisor__c=n.Supervisor__c;
                  n43.Office_Location__c=n.Office_Location__c;
                  n43.Employee_Role__c=n.Employee_Role__c;
                  n43.DepartmentUpdate__c=n.DepartmentUpdate__c;
                  n43.Title__c=n.Title__c; 
                  n43.Start_Date__c = n.Start_Date__c;
                  n43.FINRA_Licensed__c = n.FINRA_Licensed__c;
                  n43.Application_Implementor1__c = ApplicationImplementor;
                  inserlistOfcase.add(n43);
                  }
                  
                if(n.Active_Directory_Group_4__c!= null)
                { 
                  departmentName= n.Active_Directory_Group_4__c;
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                  }
                  
                  NewHireFormProcess__c n44 = new NewHireFormProcess__c ();
                  n44.Subject__c=n.Active_Directory_Group_4__c;
                  
                  n44.Supervisor__c=n.Supervisor__c;
                  n44.DepartmentUpdate__c= n.DepartmentUpdate__c;
                  n44.HR_ManagerApprovalStatus__c ='Approved';
                  n44.recordtypeid=NewhireRequestSubcase;
                  n44.AccessRequest__c = n.id;
                  n44.Active_Directory_Group4_Status__c='Submitted for Approval';
                  n44.Active_Directory_Group_4__c  = departmentName;   
                  n44.Application_owner__c=Applicationowner;
                  n44.First_Name__c=n.First_Name__c;
                  n44.Last_Name__c=n.Last_Name__c;
                  n44.Supervisor__c=n.Supervisor__c;
                  n44.Office_Location__c=n.Office_Location__c;
                  n44.Employee_Role__c=n.Employee_Role__c;
                  n44.DepartmentUpdate__c=n.DepartmentUpdate__c;
                  n44.Title__c=n.Title__c; 
                  n44.Start_Date__c = n.Start_Date__c;
                  n44.FINRA_Licensed__c = n.FINRA_Licensed__c;
                  n44.Application_Implementor1__c = ApplicationImplementor;
                  inserlistOfcase.add(n44);
                  }
                  
                if(n.Active_Directory_Group_5__c!= null)
                { 
                  departmentName= n.Active_Directory_Group_5__c;
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                      Applicationowner=departmetnITEvent.Application_Owner__c;
                      ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                  }
                  
                 NewHireFormProcess__c n45 = new NewHireFormProcess__c ();
                 n45.Subject__c=n.Active_Directory_Group_5__c;
                
                 n45.Supervisor__c=n.Supervisor__c;
                 n45.DepartmentUpdate__c= n.DepartmentUpdate__c;
                 n45.HR_ManagerApprovalStatus__c ='Approved';
                 n45.recordtypeid=NewhireRequestSubcase;
                 n45.AccessRequest__c = n.id;
                 n45.Active_Directory_Group5_Status__c='Submitted for Approval';
                 n45.Active_Directory_Group_5__c  = departmentName;   
                 n45.Application_owner__c=Applicationowner;
                 n45.First_Name__c=n.First_Name__c;
                  n45.Last_Name__c=n.Last_Name__c;
                  n45.Supervisor__c=n.Supervisor__c;
                  n45.Office_Location__c=n.Office_Location__c;
                  n45.Employee_Role__c=n.Employee_Role__c;
                  n45.DepartmentUpdate__c=n.DepartmentUpdate__c;
                  n45.Title__c=n.Title__c; 
                  n45.Start_Date__c = n.Start_Date__c;
                  n45.FINRA_Licensed__c = n.FINRA_Licensed__c;
                  n45.Application_Implementor1__c = ApplicationImplementor;
                  inserlistOfcase.add(n45);
                 }
                if(n.Active_Directory_Group_6__c!= null)
                { 
                  departmentName= n.Active_Directory_Group_6__c;
                  if((departmentName!=null) &&(New_Hire_Request__c.getvalues(departmentName)!=null))    
                   {    
                     New_Hire_Request__c  departmetnITEvent=New_Hire_Request__c.getvalues(departmentName);
                     Applicationowner=departmetnITEvent.Application_Owner__c;
                     ApplicationImplementor = departmetnITEvent.Application_Implementor__c;
                  }
                  
                  NewHireFormProcess__c n46 = new NewHireFormProcess__c ();
                  n46.Subject__c=n.Active_Directory_Group_6__c;
                
                 n46.Supervisor__c=n.Supervisor__c;
                 n46.DepartmentUpdate__c= n.DepartmentUpdate__c;
                 n46.HR_ManagerApprovalStatus__c ='Approved';
                 n46.recordtypeid=NewhireRequestSubcase;
                 n46.AccessRequest__c = n.id;
                 n46.Active_Directory_Group6_Status__c='Submitted for Approval';
                 n46.Active_Directory_Group_6__c  = departmentName;   
                 n46.Application_owner__c=Applicationowner;
                 n46.First_Name__c=n.First_Name__c;
                 n46.Last_Name__c=n.Last_Name__c;
                 n46.Supervisor__c=n.Supervisor__c;
                 n46.Office_Location__c=n.Office_Location__c;
                 n46.Employee_Role__c=n.Employee_Role__c;
                 n46.DepartmentUpdate__c=n.DepartmentUpdate__c;
                 n46.Title__c=n.Title__c; 
                 n46.Start_Date__c = n.Start_Date__c;
                 n46.FINRA_Licensed__c = n.FINRA_Licensed__c;
                 n46.Application_Implementor1__c = ApplicationImplementor;
                 inserlistOfcase.add(n46);
                 }

        }

        if((n.recordtypeid==NewhireRequestSubcase)&&(n.AccessRequest__c!=null)&&(n.status__c=='closed'))           
          {
          
             
             caseid.add( n .AccessRequest__c);  
             system.debug('-------------caseid----------------------------------->'+caseid);
          }
        
    }
       
    if(inserlistOfcase.size()>0)
    insert inserlistOfcase;
system.debug('-----------------inserlistOfcase-----------------------------------'+inserlistOfcase.size());

ChildCaselist=[select Subject__c, id,Active_Directory_Group1_Status__c,
                Active_Directory_Group2_Status__c,Active_Directory_Group3_Status__c,
                Active_Directory_Group4_Status__c,Active_Directory_Group5_Status__c,
                Active_Directory_Group6_Status__c,
                Check_if_SEC_Filing_Capability_is_needed__c,
                If_MRI_Access_is_Needed_Indicate_Secur__c,
                DST_Access_Requested_Status__c,MRI_Request_Status__c,
                Salesforce_Deal_Central_Access_Required__c,
                Webex_Access_Request_Status__c,AVID_Account_Request_Status__c,
                Concur_Account_Request_Status__c,Argus_Access_Request_Status__c,
                VPN_Access_Request_Status__c,Remote_Desktop_Request_Status__c,
                Data_Warehouse_GL_Request_Status__c,Data_Warehouse_SalesRequestStatus__c,
                Chatham_Request_Status__c,OneSource_Request_Status__c,
                Web_Filings_Request_Status__c,Kardin_Request_Status__c,
                RisKonnect_Request_Status__c,ADP_Request_Status__c,
                VDI_Access_Request_Status__c,BNA_Access_Request_Status__c,
                DataWarehouse_RealEstate_RequestStatus__c,Box_com_Request_Status__c,
                Parking_Executive_Status__c,Parking_Contractor_request__c,
                Hardware_Provide_Laptop__c,Hardware_Provide_Iphone__c,
                Hardware_Provide_Ipad__c,Hardware_Provide_Desktop__c,
                Hardware_Deploy_External__c,Hardware_Dual_Monitor_Display__c,
                Building_RELegal_Status__c,Building_PropertyManagement_Status__c,
                Building_IT_Infrastructure_Status__c,Building_HR_Status__c,
                Building_General_Status__c,Building_Facilities_Status__c,
                Building_Executive_Status__c,Building_Compliance_Status__c,
                Building_Audio_Visual_Status__c,Building_B2Storage_Status__c,
                AccessRequest__c,Status__c,Parking_General_Status__c,Active_Directory_Group_1__c,
                Active_Directory_Group_2__c,Active_Directory_Group_3__c,Active_Directory_Group_4__c,
                Active_Directory_Group_5__c,Active_Directory_Group_6__c,SalesForce_Deal_Central_Request_Status__c from 
                NewHireFormProcess__c where AccessRequest__c in:caseid];     

          integer sizeOflist=ChildCaselist.size();
          integer count=0;
          if(ChildCaselist.size()>0)
          {
               for(NewHireFormProcess__c ch:ChildCaselist)
               {
                 if(ch.status__c=='Closed' )
                { 
                  count=count+1;
                  
               }
               
           }
           }
           
           if((count==sizeOflist)&&(ChildCaselist.size()>0))
           {  
                NewHireFormProcess__c Parentcase=[select id,
                Active_Directory_Group1_Status__c,Active_Directory_Group2_Status__c,Active_Directory_Group3_Status__c,
                Active_Directory_Group4_Status__c,Active_Directory_Group5_Status__c,Active_Directory_Group6_Status__c,
                Title__c,General__c,Facilities__c,RE_Legal__c,IT_Infrastructure__c,HR__c,Compliance__c,
                Property_Management__c,Audio_Visual__c,Executive__c,B2_Storage__c,
                Deploy_External_IT_Package__c,Provide_Laptop__c,Dual_Monitor_Display__c,
                Provide_iPhone__c,Provide_iPad__c,Provide_Desktop__c,Parking_General__c,
                Parking_Executive__c,Parking_Contractor__c,Web_Filings_Requested__c,
                Data_Warehouse_Real_Estate_Requested__c,Data_Warehouse_Sales_Requested__c,
                Kardin_Requested__c,OneSource_Requested__c,Remote_Desktop_Requested__c,
                RisKonnect_Requested__c,BNA_Access_Requested__c,Box_com_Requested__c,
                Chatham_Requested__c,ADP_Requested__c,Concur_Account_Requested__c,
                Data_Warehouse_GL_Requested__c,Salesforce_Deal_Central_Access_Required__c,
                VDI_Access_Requested__c,VPN_Access_Requested__c,Argus_Access_Requested__c,
                DST_Access_Requested__c,AVID_Account_Requested__c,Webex_Access_Requested__c,
                MRI_Access_Requested__c,DepartmentUpdate__c,Name,status__c,
                Check_if_SEC_Filing_Capability_is_needed__c,If_MRI_Access_is_Needed_Indicate_Secur__c,
                DST_Access_Requested_Status__c,Additional_Instructions_or_Requests__c,Distribution_Lists__c,
                Additional_Mailbox4__c,Additional_Mailbox3__c,Additional_Mailbox2__c,Additional_Mailbox1__c,
                Active_Directory_Group_6__c,Active_Directory_Group_5__c,Active_Directory_Group_4__c,
                Active_Directory_Group_3__c,Active_Directory_Group_2__c,Active_Directory_Group_1__c,
                FINRA_Licensed__c,Employee_Role__c,Start_Date__c,First_Name__c,Last_Name__c,Supervisor__c, 
                MRI_Request_Status__c,Webex_Access_Request_Status__c,AVID_Account_Request_Status__c,
                Concur_Account_Request_Status__c,Argus_Access_Request_Status__c,VPN_Access_Request_Status__c,
                Remote_Desktop_Request_Status__c,Data_Warehouse_GL_Request_Status__c,Data_Warehouse_SalesRequestStatus__c,
                Chatham_Request_Status__c,OneSource_Request_Status__c,Web_Filings_Request_Status__c,Kardin_Request_Status__c,
                RisKonnect_Request_Status__c,ADP_Request_Status__c,VDI_Access_Request_Status__c,BNA_Access_Request_Status__c,
                DataWarehouse_RealEstate_RequestStatus__c,Box_com_Request_Status__c,Parking_Executive_Status__c,
                Parking_Contractor_request__c,Hardware_Provide_Laptop__c,Hardware_Provide_Iphone__c,Hardware_Provide_Ipad__c,
                Hardware_Provide_Desktop__c,Hardware_Deploy_External__c,Hardware_Dual_Monitor_Display__c,Building_RELegal_Status__c,
                Building_PropertyManagement_Status__c,Building_IT_Infrastructure_Status__c,Building_HR_Status__c,Building_General_Status__c,
                Building_Facilities_Status__c,Building_Executive_Status__c,Building_Compliance_Status__c,Building_Audio_Visual_Status__c,
                Building_B2Storage_Status__c,Parking_General_Status__c,SalesForce_Deal_Central_Request_Status__c from NewHireFormProcess__c where id=:caseid[0]];
system.debug('-----------------insertchildlistOfcase-----------------------------------'+ChildCaselist.size());
                for(NewHireFormProcess__c n:ChildCaselist)
                {
                
                 
                  //------Building Access-----
                  if(n.Subject__c=='Building B2 Storage Request')
                  Parentcase.Building_B2Storage_Status__c=n.Building_B2Storage_Status__c;
                  
                  if(n.Subject__c=='Building General Request')
                  Parentcase.Building_General_Status__c=n.Building_General_Status__c;
                  
                  if(n.Subject__c=='Building Facilities Request')
                  Parentcase.Building_Facilities_Status__c=n.Building_Facilities_Status__c;
                  
                  if(n.Subject__c=='Building RE Legal Request')
                  Parentcase.Building_RELegal_Status__c=n.Building_RELegal_Status__c;
                  
                  if(n.Subject__c=='Building IT Infrastructure Request')
                  Parentcase.Building_IT_Infrastructure_Status__c=n.Building_IT_Infrastructure_Status__c;
                  
                  if(n.Subject__c=='Building HR Request')
                  Parentcase.Building_HR_Status__c=n.Building_HR_Status__c;
                  
                  if(n.Subject__c=='Building Compliance Request')
                  Parentcase.Building_Compliance_Status__c=n.Building_Compliance_Status__c;
                  
                  if(n.Subject__c=='Building Property Management Request')
                  Parentcase.Building_PropertyManagement_Status__c=n.Building_PropertyManagement_Status__c;
                  
                  if(n.Subject__c=='Building Audio/Visual Request')
                  Parentcase.Building_Audio_Visual_Status__c=n.Building_Audio_Visual_Status__c;
                  
                  if(n.Subject__c=='Building Executive Request')
                  Parentcase.Building_Executive_Status__c=n.Building_Executive_Status__c;
                  
                  //------Hardware Access------
                  if(n.Subject__c=='Hardware Desktop Request')
                  Parentcase.Hardware_Provide_Desktop__c=n.Hardware_Provide_Desktop__c;
                  
                  if(n.Subject__c=='Hardware External IT Package Request')
                  Parentcase.Hardware_Deploy_External__c=n.Hardware_Deploy_External__c;
                  
                  if(n.Subject__c=='Hardware Laptop Request')
                  Parentcase.Hardware_Provide_Laptop__c=n.Hardware_Provide_Laptop__c;
                  
                  if(n.Subject__c=='Hardware Dual Monitor Request')
                  Parentcase.Hardware_Dual_Monitor_Display__c=n.Hardware_Dual_Monitor_Display__c;
                  
                  if(n.Subject__c=='Hardware Iphone Request')
                  Parentcase.Hardware_Provide_Iphone__c=n.Hardware_Provide_Iphone__c;
                  
                  if(n.Subject__c=='Hardware Ipad Request')
                  Parentcase.Hardware_Provide_Ipad__c=n.Hardware_Provide_Ipad__c;
                   
                  //----Parking Access---
                  if(n.Subject__c=='Parking General Request')
                  Parentcase.Parking_General_Status__c=n.Parking_General_Status__c;
                  
                  if(n.Subject__c=='Parking Executive Request')
                  Parentcase.Parking_Executive_Status__c=n.Parking_Executive_Status__c;
                  
                  if(n.Subject__c=='Parking Contractor Request')
                  Parentcase.Parking_Contractor_request__c=n.Parking_Contractor_request__c;
                  
                  //---Application Access-------
                  if(n.Subject__c=='MRI Request')
                  Parentcase.MRI_Request_Status__c=n.MRI_Request_Status__c;
                  
                  if(n.Subject__c=='Salesforce')
                  Parentcase.SalesForce_Deal_Central_Request_Status__c=n.SalesForce_Deal_Central_Request_Status__c;
                  
                  if(n.Subject__c=='Webex')
                  Parentcase.Webex_Access_Request_Status__c=n.Webex_Access_Request_Status__c;
                  
                  if(n.Subject__c=='VDI Access')
                  Parentcase.VDI_Access_Request_Status__c=n.VDI_Access_Request_Status__c;
                  
                  if(n.Subject__c=='VPN Access')
                  Parentcase.VPN_Access_Request_Status__c=n.VPN_Access_Request_Status__c;
                  
                  if(n.Subject__c=='Argus')
                  Parentcase.Argus_Access_Request_Status__c=n.Argus_Access_Request_Status__c;
                  
                  if(n.Subject__c=='DST')
                  Parentcase.DST_Access_Requested_Status__c=n.DST_Access_Requested_Status__c;
                  
                  if(n.Subject__c=='AVID')
                  Parentcase.AVID_Account_Request_Status__c=n.AVID_Account_Request_Status__c;
                  
                  if(n.Subject__c=='BNA Fixed Assets')
                  Parentcase.BNA_Access_Request_Status__c=n.BNA_Access_Request_Status__c;
                  
                  if(n.Subject__c=='Box.com Access')
                  Parentcase.Box_com_Request_Status__c=n.Box_com_Request_Status__c;
                  
                  if(n.Subject__c=='Chatham Access')
                  Parentcase.Chatham_Request_Status__c=n.Chatham_Request_Status__c;
                  
                  if(n.Subject__c=='ADP Access')
                  Parentcase.ADP_Request_Status__c=n.ADP_Request_Status__c;
                  
                  if(n.Subject__c=='Concur')
                  Parentcase.Concur_Account_Request_Status__c=n.Concur_Account_Request_Status__c;
                  
                  if(n.Subject__c=='Data Warehouse GL')
                  Parentcase.Data_Warehouse_GL_Request_Status__c=n.Data_Warehouse_GL_Request_Status__c;
                  
                  if(n.Subject__c=='Data Warehouse Real Estate')
                  Parentcase.DataWarehouse_RealEstate_RequestStatus__c=n.DataWarehouse_RealEstate_RequestStatus__c;
                  
                  if(n.Subject__c=='Data Warehouse Sales')
                  Parentcase.Data_Warehouse_SalesRequestStatus__c=n.Data_Warehouse_SalesRequestStatus__c;
                  
                  if(n.Subject__c=='Kardin Request')
                  Parentcase.Kardin_Request_Status__c=n.Kardin_Request_Status__c;
                  
                  if(n.Subject__c=='OneSource Request')
                  Parentcase.OneSource_Request_Status__c=n.OneSource_Request_Status__c;
                  
                  if(n.Subject__c=='RisKonnect')
                  Parentcase.RisKonnect_Request_Status__c=n.RisKonnect_Request_Status__c;
                  
                  if(n.Subject__c=='Web Filings')
                  Parentcase.Web_Filings_Request_Status__c=n.Web_Filings_Request_Status__c;
                  
                  //--------Active directory Groups----------
                  
                  if(n.Active_Directory_Group_1__c != null)
                  Parentcase.Active_Directory_Group1_Status__c=n.Active_Directory_Group1_Status__c;
                  
                  if(n.Active_Directory_Group_2__c != null)
                  Parentcase.Active_Directory_Group2_Status__c=n.Active_Directory_Group2_Status__c;
                  
                  if(n.Active_Directory_Group_3__c != null)
                  Parentcase.Active_Directory_Group3_Status__c=n.Active_Directory_Group3_Status__c;
                  
                  if(n.Active_Directory_Group_4__c != null)
                  Parentcase.Active_Directory_Group4_Status__c=n.Active_Directory_Group4_Status__c;
                  
                  if(n.Active_Directory_Group_5__c != null)
                  Parentcase.Active_Directory_Group5_Status__c=n.Active_Directory_Group5_Status__c;
                  
                  if(n.Active_Directory_Group_6__c != null)
                  Parentcase.Active_Directory_Group6_Status__c=n.Active_Directory_Group6_Status__c;
                    
                }
                Parentcase.status__c='Closed';
                update Parentcase;
           
           //Create Employee Record if the parent case is closed
           if(Parentcase.status__c == 'Closed')
           {
             Employee__c e = new Employee__c();
             e.First_Name__c = Parentcase.First_Name__c;
             e.Last_Name__c = Parentcase.Last_Name__c;
             e.Name = Parentcase.First_Name__c + Parentcase.Last_Name__c;
             e.Supervisor__c = Parentcase.Supervisor__c;
             e.Department__c= Parentcase.DepartmentUpdate__c;
             e.Title__c = Parentcase.Title__c;
             e.Office_Location__c = Parentcase.Office_Location__c;
             e.Start_Date__c = Parentcase.Start_Date__c;
             e.Employee_Role__c = Parentcase.Employee_Role__c;
             e.FINRA_Licensed__c = Parentcase.FINRA_Licensed__c;
             e.Active_Directory_Group__c = Parentcase.Active_Directory_Group_1__c;
             e.Active_Directory_Group_2__c = Parentcase.Active_Directory_Group_2__c;
             e.Active_Directory_Group_3__c = Parentcase.Active_Directory_Group_3__c;
             e.Active_Directory_Group_4__c = Parentcase.Active_Directory_Group_4__c;
             e.Active_Directory_Group_5__c = Parentcase.Active_Directory_Group_5__c;
             e.Active_Directory_Group_6__c = Parentcase.Active_Directory_Group_6__c;
             e.Additional_MailBox1__c = Parentcase.Additional_Mailbox1__c;
             e.Additional_MailBox2__c = Parentcase.Additional_Mailbox2__c;
             e.Additional_MailBox3__c = Parentcase.Additional_Mailbox3__c;
             e.Additional_MailBox4__c = Parentcase.Additional_Mailbox4__c;
             e.Distribution_Lists__c =  Parentcase.Distribution_Lists__c;
             e.NewHireFormProcess__c = Parentcase.id;
             e.Additional_Instructions_or_Requests__c = Parentcase.Additional_Instructions_or_Requests__c;
             e.Check_if_SEC_Filing_Capability_is_needed__c = Parentcase.Check_if_SEC_Filing_Capability_is_needed__c;
             e.If_MRI_Access_is_Needed_Indicate_Securi__c= Parentcase.If_MRI_Access_is_Needed_Indicate_Secur__c;
             
             
             
             //-----Building Access-----
             if(Parentcase.Building_General_Status__c == 'Approved')
             {
              pickval = 'General';
              if(e.Building_Access__c == '' || e.Building_Access__c == null )
                 e.Building_Access__c = pickval + '; ';
              else
                 e.Building_Access__c += pickval + '; ';
             }
             if(Parentcase.Building_Executive_Status__c == 'Approved')
             {
              pickval = 'Executive';
              if(e.Building_Access__c == '' || e.Building_Access__c == null)
                 e.Building_Access__c = pickval + '; ';
              else
                 e.Building_Access__c += pickval + '; ';
             }
             if(Parentcase.Building_Facilities_Status__c == 'Approved')
             {
              pickval = 'Facilities';
              if(e.Building_Access__c == '' || e.Building_Access__c == null)
                 e.Building_Access__c = pickval + '; ';
              else
                 e.Building_Access__c += pickval + '; ';
             }
             if(Parentcase.Building_RELegal_Status__c == 'Approved')
             {
              pickval = 'RE Legal';
              if(e.Building_Access__c == '' || e.Building_Access__c == null)
                 e.Building_Access__c = pickval + '; ';
              else
                 e.Building_Access__c += pickval + '; ';
             }
             if(Parentcase.Building_IT_Infrastructure_Status__c == 'Approved')
             {
              pickval = 'IT Infrastructure';
              if(e.Building_Access__c == '' || e.Building_Access__c == null)
                 e.Building_Access__c = pickval + '; ';
              else
                 e.Building_Access__c += pickval + '; ';
             }
             if(Parentcase.Building_HR_Status__c == 'Approved')
             {
              pickval = 'HR';
              if(e.Building_Access__c == '' || e.Building_Access__c == null)
                 e.Building_Access__c = pickval + '; ';
              else
                 e.Building_Access__c += pickval + '; ';
             }
             if(Parentcase.Building_Compliance_Status__c == 'Approved')
             {
              pickval = 'Compliance';
              if(e.Building_Access__c == '' || e.Building_Access__c == null)
                 e.Building_Access__c = pickval + '; ';
              else
                 e.Building_Access__c += pickval + '; ';
             }
             if(Parentcase.Building_PropertyManagement_Status__c == 'Approved')
             {
              pickval = 'Property Management';
              if(e.Building_Access__c == '' || e.Building_Access__c == null)
                 e.Building_Access__c = pickval + '; ';
              else
                 e.Building_Access__c += pickval + '; ';
             }
             if(Parentcase.Building_Audio_Visual_Status__c == 'Approved')
             {
              pickval = 'Audio/Visual';
              if(e.Building_Access__c == '' || e.Building_Access__c == null)
                 e.Building_Access__c = pickval + '; ';
              else
                 e.Building_Access__c += pickval + '; ';
             }
             if(Parentcase.Building_B2Storage_Status__c == 'Approved')
             {
              pickval = 'B2 Storage';
              if(e.Building_Access__c == '' || e.Building_Access__c == null)
                 e.Building_Access__c = pickval + '; ';
              else
                 e.Building_Access__c += pickval + '; ';
             }
             
             //----Hardware Access--------------------------
             if(Parentcase.Hardware_Deploy_External__c == 'Approved')
             {
              pickval = 'Deploy External IT Package';
              if(e.Hardware_Needed__c == '' || e.Hardware_Needed__c == null)
                 e.Hardware_Needed__c = pickval + '; ';
              else
                 e.Hardware_Needed__c += pickval + '; ';
             }
             if(Parentcase.Hardware_Provide_Desktop__c == 'Approved')
             {
              pickval = 'Provide Desktop';
              if(e.Hardware_Needed__c == '' || e.Hardware_Needed__c == null)
                 e.Hardware_Needed__c = pickval + '; ';
              else
                 e.Hardware_Needed__c += pickval + '; ';
             }
             if(Parentcase.Hardware_Provide_Laptop__c == 'Approved')
             {
              pickval = 'Provide Laptop';
              if(e.Hardware_Needed__c == '' || e.Hardware_Needed__c == null)
                 e.Hardware_Needed__c = pickval + '; ';
              else
                 e.Hardware_Needed__c += pickval + '; ';
             }
             if(Parentcase.Hardware_Dual_Monitor_Display__c == 'Approved')
             {
              pickval = 'Dual Monitor Display';
              if(e.Hardware_Needed__c == '' || e.Hardware_Needed__c == null)
                 e.Hardware_Needed__c = pickval + '; ';
              else
                 e.Hardware_Needed__c += pickval + '; ';
             }
             if(Parentcase.Hardware_Provide_Iphone__c == 'Approved')
             {
              pickval = 'Provide iPhone';
              if(e.Hardware_Needed__c == '' || e.Hardware_Needed__c == null)
                 e.Hardware_Needed__c = pickval + '; ';
              else
                 e.Hardware_Needed__c += pickval + '; ';
             }
             if(Parentcase.Hardware_Provide_Ipad__c == 'Approved')
             {
              pickval = 'Provide iPad';
              if(e.Hardware_Needed__c == '' || e.Hardware_Needed__c == null)
                 e.Hardware_Needed__c = pickval + '; ';
              else
                 e.Hardware_Needed__c += pickval + '; ';
             }
             
             //-------Parking Access-------------------------------------
             if(Parentcase.Parking_General_Status__c == 'Approved')
             {
              pickval = 'General';
              if(e.Parking__c == '' || e.Parking__c == null)
                 e.Parking__c = pickval + '; ';
              else
                 e.Parking__c += pickval + '; ';
             }
             if(Parentcase.Parking_Executive_Status__c == 'Approved')
             {
              pickval = 'Executive';
              if(e.Parking__c == '' || e.Parking__c == null)
                 e.Parking__c = pickval + '; ';
              else
                 e.Parking__c += pickval + '; ';
             }
             if(Parentcase.Parking_Contractor_request__c == 'Approved')
             {
              pickval = 'Contractor';
              if(e.Parking__c == '' || e.Parking__c == null)
                 e.Parking__c = pickval + '; ';
              else
                 e.Parking__c += pickval + '; ';
             }
             
             //---------Access Requests----------------------------------
             if(Parentcase.MRI_Request_Status__c == 'Approved')
             {
              pickval = 'MRI Access';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.SalesForce_Deal_Central_Request_Status__c == 'Approved')
             {
              pickval = 'Salesforce/Deal Central Access';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.Webex_Access_Request_Status__c == 'Approved')
             {
              pickval = 'Webex';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.AVID_Account_Request_Status__c == 'Approved')
             {
              pickval = 'AVID Account';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.Concur_Account_Request_Status__c == 'Approved')
             {
              pickval = 'Concur Account';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.Argus_Access_Request_Status__c == 'Approved')
             {
              pickval = 'Argus Access';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.DST_Access_Requested_Status__c == 'Approved')
             {
              pickval = 'DST Access';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.VPN_Access_Request_Status__c == 'Approved')
             {
              pickval = 'VPN Access';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             /*if(Parentcase.Remote_Desktop_Request_Status__c == 'Approved')
             {
              pickval = 'Remote Desktop';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }*/
             if(Parentcase.Data_Warehouse_GL_Request_Status__c == 'Approved')
             {
              pickval = 'Data Warehouse - GL';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.Data_Warehouse_SalesRequestStatus__c == 'Approved')
             {
              pickval = 'Data Warehouse - Sales';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.Chatham_Request_Status__c == 'Approved')
             {
              pickval = 'Chatham';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.OneSource_Request_Status__c == 'Approved')
             {
              pickval = 'OneSource';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.Web_Filings_Request_Status__c == 'Approved')
             {
              pickval = 'Web Filings';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.Kardin_Request_Status__c == 'Approved')
             {
              pickval = 'Kardin';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.RisKonnect_Request_Status__c == 'Approved')
             {
              pickval = 'RisKonnect';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.ADP_Request_Status__c == 'Approved')
             {
              pickval = 'ADP';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.VDI_Access_Request_Status__c == 'Approved')
             {
              pickval = 'VDI Access';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.BNA_Access_Request_Status__c == 'Approved')
             {
              pickval = 'BNA Access';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.DataWarehouse_RealEstate_RequestStatus__c == 'Approved')
             {
              pickval = 'Data Warehouse – Real Estate';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             if(Parentcase.Box_com_Request_Status__c == 'Approved')
             {
              pickval = 'Box.com';
              if(e.Access_Requested__c == '' || e.Access_Requested__c == null)
                 e.Access_Requested__c = pickval + '; ';
              else
                 e.Access_Requested__c += pickval + '; ';
             }
             
             emplist.add(e);
             //System.debug('----------Employee details -------->' + emplist + emplist.size());
             insert emplist;
             
             //Assert Insertion -- Application Access--
             
             
             
            if(Parentcase.MRI_Access_Requested__c==true && Parentcase.MRI_Request_Status__c == 'Approved' )
            {   
              Asset__c a1= new Asset__c();
              a1.Access_Name__c = 'MRI Request';
              a1.Access_Type__c = 'Application';
              a1.Employee__c = emplist[0].id;
              assetlist.add(a1);
            }
            
            if(Parentcase.Webex_Access_Requested__c==true && Parentcase.Webex_Access_Request_Status__c == 'Approved')
            {   
              Asset__c a2= new Asset__c();
              a2.Access_Name__c = 'Webex';
              a2.Access_Type__c = 'Application';
              a2.Employee__c = emplist[0].id;
              assetlist.add(a2);
            }
            
            if(Parentcase.Salesforce_Deal_Central_Access_Required__c==true && Parentcase.SalesForce_Deal_Central_Request_Status__c == 'Approved')
            {   
              Asset__c a3= new Asset__c();
              a3.Access_Name__c = 'Salesforce';
              a3.Access_Type__c = 'Application';
              a3.Employee__c = emplist[0].id;
              assetlist.add(a3);
            }
            
            if(Parentcase.VDI_Access_Requested__c==true && Parentcase.VDI_Access_Request_Status__c == 'Approved')
            {   
              Asset__c a4= new Asset__c();
              a4.Access_Name__c = 'VDI Access';
              a4.Access_Type__c = 'Application';
              a4.Employee__c = emplist[0].id;
              assetlist.add(a4);
            }
            if(Parentcase.VPN_Access_Requested__c==true && Parentcase.VPN_Access_Request_Status__c == 'Approved')
            {   
              Asset__c a5= new Asset__c();
              a5.Access_Name__c = 'VPN Access';
              a5.Access_Type__c = 'Application';
              a5.Employee__c = emplist[0].id;
              assetlist.add(a5);
            }
            if(Parentcase.Argus_Access_Requested__c==true && Parentcase.Argus_Access_Request_Status__c == 'Approved')
            {   
              Asset__c a6= new Asset__c();
              a6.Access_Name__c = 'Argus';
              a6.Access_Type__c = 'Application';
              a6.Employee__c = emplist[0].id;
              assetlist.add(a6);
            }
            if(Parentcase.DST_Access_Requested__c==true && Parentcase.DST_Access_Requested_Status__c == 'Approved')
            {   
              Asset__c a7= new Asset__c();
              a7.Access_Name__c = 'DST';
              a7.Access_Type__c = 'Application';
              a7.Employee__c = emplist[0].id;
              assetlist.add(a7);
            }
            if(Parentcase.AVID_Account_Requested__c==true && Parentcase.AVID_Account_Request_Status__c == 'Approved')
            {   
              Asset__c a8= new Asset__c();
              a8.Access_Name__c = 'AVID';
              a8.Access_Type__c = 'Application';
              a8.Employee__c = emplist[0].id;
              assetlist.add(a8);
            }
            if(Parentcase.BNA_Access_Requested__c==true && Parentcase.BNA_Access_Request_Status__c == 'Approved')
            {   
              Asset__c a9= new Asset__c();
              a9.Access_Name__c = 'BNA Fixed Assets';
              a9.Access_Type__c = 'Application';
              a9.Employee__c = emplist[0].id;
              assetlist.add(a9);
            }
            if(Parentcase.Box_com_Requested__c==true && Parentcase.Box_com_Request_Status__c == 'Approved')
            {   
              Asset__c a10= new Asset__c();
              a10.Access_Name__c = 'Box.com Access';
              a10.Access_Type__c = 'Application';
              a10.Employee__c = emplist[0].id;
              assetlist.add(a10);
            }
            if(Parentcase.Chatham_Requested__c==true && Parentcase.Chatham_Request_Status__c == 'Approved')
            {   
              Asset__c a11= new Asset__c();
              a11.Access_Name__c = 'Chatham Access';
              a11.Access_Type__c = 'Application';
              a11.Employee__c = emplist[0].id;
              assetlist.add(a11);
            }
            if(Parentcase.ADP_Requested__c==true && Parentcase.ADP_Request_Status__c == 'Approved')
            {   
              Asset__c a12= new Asset__c();
              a12.Access_Name__c = 'ADP Access';
              a12.Access_Type__c = 'Application';
              a12.Employee__c = emplist[0].id;
              assetlist.add(a12);
            }
            if(Parentcase.Concur_Account_Requested__c==true && Parentcase.Concur_Account_Request_Status__c == 'Approved')
            {   
              Asset__c a13= new Asset__c();
              a13.Access_Name__c = 'Concur';
              a13.Access_Type__c = 'Application';
              a13.Employee__c = emplist[0].id;
              assetlist.add(a13);
            }
            if(Parentcase.Data_Warehouse_GL_Requested__c ==true && Parentcase.Data_Warehouse_GL_Request_Status__c == 'Approved')
            {   
              Asset__c a14= new Asset__c();
              a14.Access_Name__c = 'Data Warehouse GL';
              a14.Access_Type__c = 'Application';
              a14.Employee__c = emplist[0].id;
              assetlist.add(a14);
            }
            if(Parentcase.Data_Warehouse_Real_Estate_Requested__c==true && Parentcase.DataWarehouse_RealEstate_RequestStatus__c == 'Approved')
            {   
              Asset__c a15= new Asset__c();
              a15.Access_Name__c = 'Data Warehouse Real Estate';
              a15.Access_Type__c = 'Application';
              a15.Employee__c = emplist[0].id;
              assetlist.add(a15);
            }
            if(Parentcase.Data_Warehouse_Sales_Requested__c==true && Parentcase.Data_Warehouse_SalesRequestStatus__c == 'Approved')
            {   
              Asset__c a16= new Asset__c();
              a16.Access_Name__c = 'Data Warehouse Sales';
              a16.Access_Type__c = 'Application';
              a16.Employee__c = emplist[0].id;
              assetlist.add(a16);
            }
            if(Parentcase.Kardin_Requested__c ==true && Parentcase.Kardin_Request_Status__c == 'Approved')
            {   
              Asset__c a17= new Asset__c();
              a17.Access_Name__c = 'Kardin Request';
              a17.Access_Type__c = 'Application';
              a17.Employee__c = emplist[0].id;
              assetlist.add(a17);
            }
            if(Parentcase.OneSource_Requested__c ==true && Parentcase.OneSource_Request_Status__c == 'Approved')
            {   
              Asset__c a18= new Asset__c();
              a18.Access_Name__c = 'OneSource Request';
              a18.Access_Type__c = 'Application';
              a18.Employee__c = emplist[0].id;
              assetlist.add(a18);
            }
            /*if(Parentcase.Remote_Desktop_Requested__c==true&& Parentcase.Remote_Desktop_Request_Status__c == 'Approved')
            {   
              Asset__c a19= new Asset__c();
              a19.Access_Name__c = 'Remote Desktop';
              a19.Access_Type__c = 'Application';
              a19.Employee__c = emplist[0].id;
              assetlist.add(a19);
            }*/
            if(Parentcase.RisKonnect_Requested__c==true&& Parentcase.RisKonnect_Request_Status__c == 'Approved')
            {   
              Asset__c a20= new Asset__c();
              a20.Access_Name__c = 'RisKonnect';
              a20.Access_Type__c = 'Application';
              a20.Employee__c = emplist[0].id;
              assetlist.add(a20);
            }
            if(Parentcase.Web_Filings_Requested__c==true && Parentcase.Web_Filings_Request_Status__c == 'Approved')
            {   
              Asset__c a21= new Asset__c();
              a21.Access_Name__c = 'Web Filings';
              a21.Access_Type__c = 'Application';
              a21.Employee__c = emplist[0].id;
              assetlist.add(a21);
            }
            
            //---Parking Asset Insertion----
            if(Parentcase.Parking_General__c==true && Parentcase.Parking_General_Status__c == 'Approved')
            {   
              Asset__c a22= new Asset__c();
              a22.Access_Name__c = 'Parking General Request';
              a22.Access_Type__c = 'Parking';
              a22.Employee__c = emplist[0].id;
              assetlist.add(a22);
            }
            if(Parentcase.Parking_Executive__c==true && Parentcase.Parking_Executive_Status__c == 'Approved')
            {   
              Asset__c a23= new Asset__c();
              a23.Access_Name__c = 'Parking Executive Request';
              a23.Access_Type__c = 'Parking';
              a23.Employee__c = emplist[0].id;
              assetlist.add(a23);
            }
            if(Parentcase.Parking_Contractor__c==true && Parentcase.Parking_Contractor_request__c == 'Approved')
            {   
              Asset__c a24= new Asset__c();
              a24.Access_Name__c = 'Parking Contractor Request';
              a24.Access_Type__c = 'Parking';
              a24.Employee__c = emplist[0].id;
              assetlist.add(a24);
            }
            
            //---Hardware Asset insertion---//
            if(Parentcase.Provide_Desktop__c==true && Parentcase.Hardware_Provide_Desktop__c == 'Approved')
            {   
              Asset__c a25= new Asset__c();
              a25.Access_Name__c = 'Hardware Desktop Request';
              a25.Access_Type__c = 'Hardware';
              a25.Employee__c = emplist[0].id;
              assetlist.add(a25);
            }
            if(Parentcase.Deploy_External_IT_Package__c==true && Parentcase.Hardware_Deploy_External__c == 'Approved')
            {   
              Asset__c a26= new Asset__c();
              a26.Access_Name__c = 'Hardware External IT Package Request';
              a26.Access_Type__c = 'Hardware';
              a26.Employee__c = emplist[0].id;
              assetlist.add(a26);
            }
            if(Parentcase.Provide_Laptop__c==true && Parentcase.Hardware_Provide_Laptop__c == 'Approved')
            {   
              Asset__c a27= new Asset__c();
              a27.Access_Name__c = 'Hardware Laptop Request';
              a27.Access_Type__c = 'Hardware';
              a27.Employee__c = emplist[0].id;
              assetlist.add(a27);
            }
            if(Parentcase.Provide_iPad__c==true && Parentcase.Hardware_Provide_Ipad__c == 'Approved')
            {   
              Asset__c a28= new Asset__c();
              a28.Access_Name__c = 'Hardware Ipad Request';
              a28.Access_Type__c = 'Hardware';
              a28.Employee__c = emplist[0].id;
              assetlist.add(a28);
            }
            if(Parentcase.Dual_Monitor_Display__c==true && Parentcase.Hardware_Dual_Monitor_Display__c == 'Approved')
            {   
              Asset__c a29= new Asset__c();
              a29.Access_Name__c = 'Hardware Dual Monitor Request';
              a29.Access_Type__c = 'Hardware';
              a29.Employee__c = emplist[0].id;
              assetlist.add(a29);
            }
            if(Parentcase.Provide_iPhone__c==true && Parentcase.Hardware_Provide_Iphone__c == 'Approved')
            {   
              Asset__c a30= new Asset__c();
              a30.Access_Name__c = 'Hardware Iphone Request';
              a30.Access_Type__c = 'Hardware';
              a30.Employee__c = emplist[0].id;
              assetlist.add(a30);
            }
            
            
            //-----Building Asset Insertion---//
            if(Parentcase.General__c==true && Parentcase.Building_General_Status__c == 'Approved')
            {   
              Asset__c a31= new Asset__c();
              a31.Access_Name__c = 'Building General Request';
              a31.Access_Type__c = 'Building';
              a31.Employee__c = emplist[0].id;
              assetlist.add(a31);
            }
            if(Parentcase.Facilities__c==true && Parentcase.Building_Facilities_Status__c == 'Approved')
            {   
              Asset__c a32= new Asset__c();
              a32.Access_Name__c = 'Building Facilities Request';
              a32.Access_Type__c = 'Building';
              a32.Employee__c = emplist[0].id;
              assetlist.add(a32);
            }
            if(Parentcase.RE_Legal__c==true && Parentcase.Building_RELegal_Status__c == 'Approved') 
            {   
              Asset__c a33= new Asset__c();
              a33.Access_Name__c = 'Building RE Legal Request';
              a33.Access_Type__c = 'Building';
              a33.Employee__c = emplist[0].id;
              assetlist.add(a33);
            }
            if(Parentcase.IT_Infrastructure__c==true && Parentcase.Building_IT_Infrastructure_Status__c == 'Approved')
            {   
              Asset__c a34= new Asset__c();
              a34.Access_Name__c = 'Building IT Infrastructure Request';
              a34.Access_Type__c = 'Building';
              a34.Employee__c = emplist[0].id;
              assetlist.add(a34);
            }
            if(Parentcase.HR__c==true && Parentcase.Building_HR_Status__c == 'Approved')
            {   
              Asset__c a35= new Asset__c();
              a35.Access_Name__c = 'Building HR Request';
              a35.Access_Type__c = 'Building';
              a35.Employee__c = emplist[0].id;
              assetlist.add(a35);
            }
            if(Parentcase.Compliance__c==true && Parentcase.Building_Compliance_Status__c == 'Approved')
            {   
              Asset__c a36= new Asset__c();
              a36.Access_Name__c = 'Building Compliance Request';
              a36.Access_Type__c = 'Building';
              a36.Employee__c = emplist[0].id;
              assetlist.add(a36);
            }
            if(Parentcase.Property_Management__c==true && Parentcase.Building_PropertyManagement_Status__c == 'Approved')
            {   
              Asset__c a37= new Asset__c();
              a37.Access_Name__c = 'Building Property Management Request';
              a37.Access_Type__c = 'Building';
              a37.Employee__c = emplist[0].id;
              assetlist.add(a37);
            }
            if(Parentcase.Audio_Visual__c==true && Parentcase.Building_Audio_Visual_Status__c == 'Approved')
            {   
              Asset__c a38= new Asset__c();
              a38.Access_Name__c = 'Building Audio/Visual Request';
              a38.Access_Type__c = 'Building';
              a38.Employee__c = emplist[0].id;
              assetlist.add(a38);
            }
            if(Parentcase.Executive__c==true && Parentcase.Building_Executive_Status__c == 'Approved')
            {   
              Asset__c a39= new Asset__c();
              a39.Access_Name__c = 'Building Executive Request';
              a39.Access_Type__c = 'Building';
              a39.Employee__c = emplist[0].id;
              assetlist.add(a39);
            }
            if(Parentcase.B2_Storage__c==true && Parentcase.Building_B2Storage_Status__c == 'Approved')
            {   
              Asset__c a40= new Asset__c();
              a40.Access_Name__c = 'Building B2 Storage Request';
              a40.Access_Type__c = 'Building';
              a40.Employee__c = emplist[0].id;
              assetlist.add(a40);
            }
            //Active directory groups asset creations
             if(Parentcase.Active_Directory_Group_1__c!=null && Parentcase.Active_Directory_Group1_Status__c == 'Approved')
            {   
              Asset__c ativw1= new Asset__c();
              ativw1.Access_Name__c = Parentcase.Active_Directory_Group_1__c;
              ativw1.Access_Type__c = 'Active Directory Group';
              ativw1.Employee__c = emplist[0].id;
              assetlist.add(ativw1);
            }
            if(Parentcase.Active_Directory_Group_2__c!=null && Parentcase.Active_Directory_Group2_Status__c == 'Approved')
            {   
              Asset__c ativw11= new Asset__c();
              ativw11.Access_Name__c = Parentcase.Active_Directory_Group_2__c;
              ativw11.Access_Type__c = 'Active Directory Group';
              ativw11.Employee__c = emplist[0].id;
              assetlist.add(ativw11);
            }
            if(Parentcase.Active_Directory_Group_3__c!=null && Parentcase.Active_Directory_Group3_Status__c == 'Approved')
            {   
              Asset__c ativw3= new Asset__c();
              ativw3.Access_Name__c = Parentcase.Active_Directory_Group_3__c;
              ativw3.Access_Type__c = 'Active Directory Group';
              ativw3.Employee__c = emplist[0].id;
              assetlist.add(ativw3);
            }if(Parentcase.Active_Directory_Group_4__c!=null && Parentcase.Active_Directory_Group4_Status__c == 'Approved')
            {   
              Asset__c ativw4= new Asset__c();
              ativw4.Access_Name__c = Parentcase.Active_Directory_Group_4__c;
              ativw4.Access_Type__c = 'Active Directory Group';
              ativw4.Employee__c = emplist[0].id;
              assetlist.add(ativw4);
            }
            if(Parentcase.Active_Directory_Group_5__c!=null && Parentcase.Active_Directory_Group5_Status__c == 'Approved')
            {   
              Asset__c ativw5= new Asset__c();
              ativw5.Access_Name__c = Parentcase.Active_Directory_Group_5__c;
              ativw5.Access_Type__c = 'Active Directory Group';
              ativw5.Employee__c = emplist[0].id;
              assetlist.add(ativw5);
            }
            if(Parentcase.Active_Directory_Group_6__c!=null && Parentcase.Active_Directory_Group6_Status__c == 'Approved')
            {   
              Asset__c ativw6= new Asset__c();
              ativw6.Access_Name__c = Parentcase.Active_Directory_Group_6__c;
              ativw6.Access_Type__c = 'Active Directory Group';
              ativw6.Employee__c = emplist[0].id;
              assetlist.add(ativw6);
            }
            
             if(assetlist.size() > 0 )
             {
              insert assetlist;
             }
        }
    }
}