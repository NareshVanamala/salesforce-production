trigger createEventOnParalegal on Deal__c(after insert, after update)
{
/*  set<string> dealOwnedidSet =new set<string>();
 set<string> dealOwnedidSet1 =new set<string>();
 set<string> despositionOwnedidSet1 =new set<string>();
  
   map<string,Deal__c> dealMap = new map<string,Deal__c>();
    //map<string,Deal__c> dealMap1 = new map<string,Deal__c>();
    map<string,Deal__c> DespositiondealMap = new map<string,Deal__c>();

    // Id devRecordTypeId = Schema.SObjectType.Deal__c.getRecordTypeInfosByName().get('Acquisition').getRecordTypeId();
    // Id devRecordTypeId1 = Schema.SObjectType.Deal__c.getRecordTypeInfosByName().get('Sale Leaseback').getRecordTypeId();
    
     Id  despositionRecTypeid= Schema.SObjectType.Deal__c.getRecordTypeInfosByName().get('Disposition').getRecordTypeId();  

    for(Deal__c dealObj1 :trigger.new)
   {
        dealOwnedidSet1.add(dealObj1.Owned_Id__c);
        //dealMap1.put(dealObj1.Owned_Id__c,dealObj1);
        //system.debug('if Equals'+dealObj1);
    }

    
    for(Deal__c despositiondeal :trigger.new)
   {
          
         // if(despositiondeal.recordtypeid==despositionRecTypeid)
        
        despositionOwnedidSet1 .add(despositiondeal.Owned_Id__c);
        //dealMap1.put(dealObj1.Owned_Id__c,dealObj1);
        
        //system.debug('if Equals'+dealObj1);
    }
     system.debug('this is my set'+despositionOwnedidSet1  );
    

    for(Deal__c dealObj :[select id,Property_Owner_SPEL__c,Owned_Id__c from Deal__c where Owned_Id__c in :dealOwnedidSet1 and (RecordTypeId  =:devRecordTypeId OR RecordTypeId  =:devRecordTypeId1) order by createddate desc limit 1 ]){
        system.debug('if Equals'+dealObj );
        dealOwnedidSet.add(dealObj.Owned_Id__c);
        dealMap.put(dealObj.Owned_Id__c,dealObj);
    }
     system.debug('this is my set'+dealOwnedidSet);
    system.debug('Check my map'+dealMap);
    
 for(Deal__c dealdespositionobj :[select id,Property_Owner_SPEL__c,Owned_Id__c from Deal__c where Owned_Id__c in :despositionOwnedidSet1  and (RecordTypeId  =:despositionRecTypeid) order by createddate desc limit 1 ])
   {

        system.debug('if Equals'+dealdespositionobj );
        dealOwnedidSet.add(dealdespositionobj .Owned_Id__c);
        DespositiondealMap.put(dealdespositionobj.Owned_Id__c,dealdespositionobj);
    }
     system.debug('Check my map'+DespositiondealMap);
    
    
     list<MRI_PROPERTY__c> mriPropertyList = new list<MRI_PROPERTY__c>();
     for(MRI_PROPERTY__c mriObjTemp : [select id,Property_ID__c,Related_Deal__c,Property_Owner_SPE__c from MRI_PROPERTY__c where Property_ID__c in :dealOwnedidSet])
     {
          if(dealMap.containsKey(mriObjTemp.Property_ID__c))
          {
              system.debug('contains key');
              if(dealMap.get(mriObjTemp.Property_ID__c).Owned_Id__c == mriObjTemp.Property_ID__c)
             {
               // system.debug('if Equals');
               // system.debug('if Equals deal value'+dealMap.get(mriObjTemp.Property_ID__c).Property_Owner_SPEL__c);
                mriObjTemp.Related_Deal__c= dealMap.get(mriObjTemp.Property_ID__c).id;
                mriObjTemp.Property_Owner_SPE__c = dealMap.get(mriObjTemp.Property_ID__c).Property_Owner_SPEL__c;
                system.debug('if Equals update value'+mriObjTemp.Property_Owner_SPE__c);
                mriPropertyList.add(mriObjTemp);
            }
        }
       if(DespositiondealMap.containsKey(mriObjTemp.Property_ID__c))
       {

         system.debug('contains desposition key'); 
         
         if(DespositiondealMap.get(mriObjTemp.Property_ID__c).Owned_Id__c == mriObjTemp.Property_ID__c)
         { 
          mriObjTemp.Disposition_in_DC__c= DespositiondealMap.get(mriObjTemp.Property_ID__c).id;
                
 
        }
        
      }

       
   }
     if(mriPropertyList !=null && mriPropertyList.size()>0){
        update mriPropertyList;
     }*/

if(trigger.isUpdate)
{
    map<id,id>dealparalegal= new map<id,id>();
    map<id,date>dealEstimatedCOE= new map<id,date>();
    map<id,string>locationmap= new map<id,string>();   
    map<id,Event>dealEventMap= new map<id,Event>();

    set<id>dealid= new set<id>();
    set<id>paralegalids= new set<id>();
    List<Deal__c> deallist= new list<Deal__c>();
    
    list<Event>insertEventlist= new list<Event>();
    list<Event>UpdateEventlist= new list<Event>();
    list<Event>dealEventlist= new list<Event>();
    List<PublicCalendar__c> pc= PublicCalendar__c.getall().values();
    system.debug('>>>>>>>>>>'+PublicCalendar__c.id__c);
    //collect all trigger.new ids into one set   
     
     for(Deal__c dealrec:Trigger.new)
     {
       
         dealid.add(dealrec.id);
         paralegalids.add(dealrec.Paralegal__c); 
         system.debug('>>>>>>>>>'+dealid);
         
      }
     
      
deallist=[select Name,id,Address_Line_1__c,Paralegal__c,Deal_Status__c,State__c,City__c,Estimated_COE_Date__c from Deal__c where id in:dealid];
dealEventlist=[select id, subject,location,whatid,ownerid,IsAllDayEvent,StartDateTime,Appointment_status__c from Event where whatid in:dealid];
  
    system.debug('get the paralegalid list...'+paralegalids);
    system.debug('get the deal ids..'+dealid);
    system.debug('get the dealeventlist..'+dealEventlist);  
       
     //Let us form a map 
if(dealEventlist.size()>0)
     for(event et:dealEventlist)
      dealEventMap.put(et.whatid,et);
   system.debug('check the map--'+dealEventMap);
   
    for(Deal__c dealUp1:deallist)
    {
       if(dealUp1.Paralegal__c!=null)
        dealparalegal.put(dealUp1.id,dealUp1.Paralegal__c);    
       if(dealUp1.Estimated_COE_Date__c!=null)
        dealEstimatedCOE.put(dealUp1.id,dealUp1.Estimated_COE_Date__c);  
        
        if(dealUp1.Address_Line_1__c!=null)
         locationmap.put(dealUp1.id,dealUp1.Address_Line_1__c);
         
    }
     for(Deal__c dealrec:trigger.new)
     {
       Deal__c triggerold = new Deal__c();
       triggerold= trigger.oldmap.get(dealrec.id);
        
         If(dealrec.Estimated_COE_Date__c!=null && ((dealrec.Deal_Status__c!='Dead')||(dealrec.Deal_Status__c!='Dead-Legal')))
          {
              
              if(triggerold.Estimated_COE_Date__c==null) 
               {
                  
                 Event e = new Event();
                 If(PC.size()>0)
                 e.Ownerid= pc[0].ID__c;
                 e.StartDateTime=dealEstimatedCOE.get(dealrec.id);
                 e.whatid=dealrec.id;
                 e.IsAllDayEvent=true; 
                 e.Subject='COE';
                  insertEventlist.add(e);   
                }
               if(triggerold.Estimated_COE_Date__c!=null && triggerold.Estimated_COE_Date__c!=dealrec.Estimated_COE_Date__c)   
               {
               if(dealEventlist.size()>0)
               {
               if(dealEventMap!=null)
               {
               if(dealEventMap.get(dealrec.id).StartDateTime!=dealrec.Estimated_COE_Date__c)
               dealEventMap.get(dealrec.id).StartDateTime=dealrec.Estimated_COE_Date__c;
                dealEventMap.get(dealrec.id).whatid=dealrec.id;
              //  dealEventMap.get(dealrec.id).ownerid = pc[0].ID__c;
                dealEventMap.get(dealrec.id).Subject='COE';
               // dealEventMap.get(dealrec.id).location=dealrec.REthinkAP__Address_Line_1__c+','+dealrec.REthinkAP__City__c+','+dealrec.REthinkAP__State__c;
               dealEventMap.get(dealrec.id).IsAllDayEvent=true; 
               }
               }
               else
               {
                Event e = new Event();
               If(PC.size()>0)
                  e.Ownerid= pc[0].ID__c;
                  e.StartDateTime=dealEstimatedCOE.get(dealrec.id);
                  e.whatid=dealrec.id;
                  e.IsAllDayEvent=true; 
                  e.Subject='COE';
                  insertEventlist.add(e);   
               }
               }
               If(triggerold.deal_status__c!='Closed/Owned' && dealrec.deal_status__c=='Closed/Owned')
               {
               if(dealEventlist.size()>0)
               {
               dealEventMap.get(dealrec.id).Appointment_status__C='Completed';
               }
               }
              
             }
          
              
    }
            update dealEventMap.values();  
            insert insertEventlist;
}
}