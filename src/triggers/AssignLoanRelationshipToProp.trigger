trigger AssignLoanRelationshipToProp on Loan_Relationship__c (before insert, before update)
{
    list<id>dealidlist= new list<id>();
    //get the deal ids of the property just created
    map<string,string> mapToProperties = new map<string,string>();
    for(Loan_Relationship__c dct:Trigger.new){
          dealidlist.add(dct.Deal__c); 
          }
          
     list<Deal__c>deallist=[select id, Is_Property_Created__c from Deal__c where id in:dealidlist];  
    for(RC_Property__c rcp : [select id , Related_Deal__c from RC_Property__c where Related_Deal__c in :deallist]){
         mapToProperties.put(rcp.Related_Deal__c,rcp.id);
    }
    for(Loan_Relationship__c loanRelObj :Trigger.new ){
        loanRelObj.Property__c = mapToProperties.get(loanRelObj.Deal__c);
    }
    
 }