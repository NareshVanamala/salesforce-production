trigger updatepropertygrossleaseablearea on Vacant_Lease__c (after insert,after update) {
// Start : Submit for approval after creation of Vacant Lease
set<id> mriPropertyids = new set<id>();
    set<id> webPropertyids = new set<id>();
/*If(trigger.isInsert)
{
 ProcessInstance[] oPIs = [select Id, TargetObjectID from ProcessInstance WHERE  TargetObjectId IN :trigger.new];
 Approval.ProcessSubmitRequest[] lRequests = New Approval.ProcessSubmitRequest[]{};
 Map<id, ProcessInstance> mPI = New Map<id, ProcessInstance>();
 For(ProcessInstance gPI : oPIs)
 mPI.put(gPI.TargetObjectID, gPI);
//List<MRI_PROPERTY__c> vs = new List<MRI_PROPERTY__c>();
 For(Vacant_Lease__c oObj : trigger.new){
 
         system.debug('status11:'+oObj.MRI_Property__r.Vacant_Suites__c);
         system.debug('status1propid:'+oObj.MRI_Property__c);
         system.debug('status11:'+oObj.MRI_Property__r.Name);
          MRI_PROPERTY__c vs = [Select id,Vacant_Suites__c from MRI_PROPERTY__c where id=:oObj.MRI_Property__c];
         If (oObj.Name != NULL && vs.Vacant_Suites__c==false) {  
         
                        system.debug('loop entered status:'+oObj.MRI_Property__r.Vacant_Suites__c);
                        if(mPI.get(oObj.ID)== NULL || test.isrunningtest()) {    
                            // create the new approval request to submit            
                            Approval.ProcessSubmitRequest req   = new   Approval.ProcessSubmitRequest();            
                            req.setComments('Submitted for Approval');            
                            req.setObjectId(oObj.Id);          
                            lRequests.add(req);
                        }    
                    } 
                        
     } 
        
If(lRequests!=null && lRequests.size()>0){                         
            Approval.ProcessResult[] results =Approval.process(lRequests);    
            
            for(Approval.ProcessResult pResult: results ){
                if(pResult.isSuccess())
                    System.debug('Submitted for approval successfully:  ');
                else
                     System.debug('Submitted for approval failed:  ');
           }
       }  
}

    
    if(trigger.isUpdate && trigger.isAfter){
        for(Vacant_Lease__c vacantleaseobj : trigger.new){
            if(vacantleaseobj.Vacant_Suites_Approved__c){
                mriPropertyids.add(vacantleaseobj.MRI_Property__c);
                webPropertyids.add(vacantleaseobj.Web_Properties__c);
            }
        }
        if(mriPropertyids!=null){
            List<MRI_PROPERTY__c> mriPropertylist = new List<MRI_PROPERTY__c>();
            for(MRI_PROPERTY__c mripropertyObj : [Select id,Vacant_Suites__c from MRI_PROPERTY__c where id in :mriPropertyids]){
                mripropertyObj.Vacant_Suites__c = true;
                mriPropertylist.add(mripropertyObj);
            }
            if(mriPropertylist !=null && mriPropertylist.size()>0){
                update mriPropertylist;
            }
        }
        if(webPropertyids!=null){
            List<Web_Properties__c> webPropertylist = new List<Web_Properties__c>();
            for(Web_Properties__c webpropertyObj : [Select id,Vacant_Suites__c from Web_Properties__c where id in :webPropertyids]){
                webpropertyObj.Vacant_Suites__c = true;
                webPropertylist.add(webpropertyObj);
            }
            if(webPropertylist !=null && webPropertylist.size()>0){
                update webPropertylist;
            }
        }
        
    }
*/
// End : Submit for approval after creation of Vacant Lease

/*
map<string,web_properties__c> wbupdatedlist=new map<string,web_properties__c>();
set<string> cd=new set<string>();
for(Vacant_Lease__c vl:trigger.new)
{
cd.add(vl.name);
}
list<web_properties__c> wblist=[select id,name,Gross_Leaseable_Area__c,property_id__c from web_properties__c where property_id__c in:cd and web_ready__c='approved'];
list<Vacant_Lease__c> vllist=[select id,name,Suite_SQFT__c from Vacant_Lease__c where (name!=null or name in:cd)];
if(wblist.size()<50)
{

for(web_properties__c wb:wblist)
     {
       integer squareft=0;
       
       for(Vacant_Lease__c vl:vllist)
       {
        if(wb.property_id__c==vl.name)
        {
         if(vl.Suite_SQFT__c!=null)
         {        
         wb.Gross_Leaseable_Area__c=wb.Gross_Leaseable_Area__c+integer.valueof(vl.Suite_SQFT__c);
         }
        }
       }

     //  wb.Gross_Leaseable_Area__c=squareft;
       wbupdatedlist.put(wb.id,wb);
     }
update wbupdatedlist.values();
}
else
{
//if (!ProcessorControl.inFutureContext) {
Database.executeBatch(new updatepropertygrossleaseableareabatch(vllist),1);
//}
}*/
}