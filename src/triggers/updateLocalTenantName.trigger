trigger updateLocalTenantName on Property_Contact_Leases__c(after insert,after update){
    list<Property_Contacts__c> propConList = new list<Property_Contacts__c> ();
    map<string,string> mapToProp = new map<string,string>();
    set<string> propconset =new set<string>();
    set<string> propleaseset = new set<string>();
    for(Property_Contact_Leases__c propObj : [select id,Property_Contacts__c from Property_Contact_Leases__c  where id in :trigger.new]){
        propleaseset.add(propObj.Property_Contacts__c);
    }
    for(aggregateResult res : [select Lease__r.Tenant_Name__c loctent , count(id),max(Property_Contacts__c) propid from Property_Contact_Leases__c where Lease__r.Tenant_Name__c !=null and Property_Contacts__c in :propleaseset group by Lease__r.Tenant_Name__c  order by count(id) desc limit 1]){
        string propids = (string) res.get('propid');
        string loctentname = (string) res.get('loctent');
        mapToProp.put(propids,loctentname);
        propconset.add(propids);
    }
    if(mapToProp !=null && mapToProp.size()>0){
        for(Property_Contacts__c propconObj :[select id,Local_Tenant_Name__c from Property_Contacts__c where id in :propconset]){
                if(mapToProp.containsKey(propconObj.id)){
                propconObj.Local_Tenant_Name__c = mapToProp.get(propconObj.id);
                system.debug('local tentant name'+mapToProp.get(propconObj.id));
                propConList.add(propconObj);
                }
        }
    }
    if(propConList !=null && propConList.size()>0){
        update propConList;
    }
}