trigger UpdateEmployee on Asset__c (after update)
 {
 
   if(trigger.new[0].Employee__c!=null)
   {
        set<id>empid= new set<id>();
        list<Employee__c>emplist= new list<Employee__c>();
        list<Asset__c >assetlist= new list<Asset__c >();
        list<Asset__c >assetlist1= new list<Asset__c >();
        list<Asset__c >removalaccesslist= new list<Asset__c >();
        list<Asset__c >removalaccesstermination= new list<Asset__c >();
        list<Asset__c >terminatedlist= new list<Asset__c >();
        list<Asset__c>assetlist123=new list<Asset__c >();
        list<Asset__c>assetlistRemovalProcessed=new list<Asset__c >();
        list<Asset__c>optionchangeRejectedlist=new list<Asset__c>();
         list<Asset__c>Allassetlist=new list<Asset__c>();
        map<id,list<Asset__c>>EmployeeAssetlistmap= new map<id,list<Asset__c>>(); 
        Employee__c emp= new Employee__c();
        id myid;
        map<id,Employee__c>EmpMap= new map<id,Employee__c>();
        List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();
        integer sizeoflist;
        integer count=0;
       
        emp=[select id,Employee_Status__c from Employee__c where id=:trigger.new[0].Employee__c ]; 
        Allassetlist=[select Ecova_Options__c, ROLES__c,SECURITY_GROUPS__c, Tenrox__c,OneSource_Options__c,Chatham_Options__c,id,Argus_enterprise_options__c,Terminating__c,Termination_Status__c,Option_Change_Rejected__c,Added_On__c,Asset_Status__c,Employee__c,Removal_Final_Step_processed__c,Incident_No__c,Final_Step_Procssed__c,MRI_options_c__c,Previous_Options__c,Tableau_Sites_List_options__c,Workiva__c,DST_Options__c from Asset__c where Employee__c =:trigger.new[0].Employee__c];
        
        for(Asset__c cty : Allassetlist)
        {
          if(cty.Asset_Status__c=='In Process')
            assetlist.add(cty);
        
          if((cty.Asset_Status__c=='Removal In Process')&&(cty.Added_On__c!=null)&&(cty.Terminating__c==false)&&(cty.Termination_Status__c=='Inactive'))  
            removalaccesslist.add(cty);
          
          if((cty.Asset_Status__c=='Removal In Process')&&(cty.Added_On__c!=null)&&(cty.Terminating__c == true)&&(cty.Termination_Status__c=='Inactive'))
            removalaccesstermination.add(cty);
         
         if((cty.Asset_Status__c=='Active')||(cty.Asset_Status__c=='In Process')||(cty.Asset_Status__c=='Added')||(cty.Asset_Status__c=='Removal In Process'))   
            terminatedlist.add(cty);
        
         if((cty.Final_Step_Procssed__c==true)&&(cty.id==trigger.new[0].id))
            assetlist123.add(cty);
         if((cty.Removal_Final_Step_processed__c==true)&&(cty.id==trigger.new[0].id))
            assetlistRemovalProcessed.add(cty);  
         if((cty.Option_Change_Rejected__c==true)&&(cty.id==Trigger.new[0].id))   
            optionchangeRejectedlist.add(cty);
            
        }
        system.debug('check the size of removalaccesslist'+removalaccesslist.size());
        
                  
        if(optionchangeRejectedlist.size()>0)
        {
           system.debug('i am in the access process');
           getComments.UpdatetheOptionchange(optionchangeRejectedlist[0].id);
        
        }  
          
          
        if((assetlist.size()==0)&&(emp.Employee_Status__c=='On Boarding inProcess'))
        {
            emp.Employee_Status__c='On Board';
            update emp;
        }
        
        
        if(assetlist123.size()>0)
        {
            system.debug('i am in the access process');
            getComments.GetmetheApprovalhistory(trigger.new[0].id);
        
        }
        
        if(assetlistRemovalProcessed.size()>0)
        {
            system.debug('i am in the removal process');
            getComments.GetmetheApprovalhistory1(trigger.new[0].id);
        
        }
              
        if((terminatedlist.size()==0)&&(emp.Employee_Status__c=='On Board'))
        {
            emp.Employee_Status__c ='Terminated';
            update emp; 
        }
        // create the new approval request to submit 
        if(removalaccesslist.size()>0)
        {
            for(Asset__c ctt:removalaccesslist)
            {
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest(); 
                req.setComments('Access Removal Process');
                req.setObjectId(ctt.Id);
                approvalReqList.add(req); 
            //UtilClass.isSubmitted = true;
            } 
        }
        if((removalaccesstermination.size()>0))
        {
            for(Asset__c ctt:removalaccesstermination)
            {
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest(); 
                req.setComments('Access Removal Process');
                req.setObjectId(ctt.Id);
                approvalReqList.add(req);
                //UtilClass.isSubmitted = true; 
           } 
        }
        List<Approval.ProcessResult> resultList1 = Approval.process(approvalReqList); 
        for(Approval.ProcessResult result: resultList1)
        { 
            System.debug('--------Submitted for approval successfully:------> '+result.isSuccess()); 
            system.debug('-------Check the approvalhistory-----------> '+result); 
        }
   }
 }