trigger SendEventAutomationRequestAttachmentTrigger on Event_Automation_Request__c (after update) {    
    
    List<Event_Automation_Request__c> tobeProcess = new List<Event_Automation_Request__c>();
    Set<id> uidset = new Set<id>();
    Set<id> earidset = new Set<id>();
    Map<id,User> uidrecmap = new Map<id,User>();
    Map<id,List<Attachment>> pidAttListMap =new Map<id, List<Attachment>>();
    
    for(Event_Automation_Request__c obj: Trigger.new )
    {
        if(obj.Approved__c == true && obj.Approved__c != Trigger.oldMap.get(obj.id).Approved__c)
        {
            tobeProcess.add(obj);
            earidset.add(obj.id);
            if(obj.Key_Account_Manager_Name_Requestor__c != null)
            uidset.add(obj.Key_Account_Manager_Name_Requestor__c);
        }
    }
    if(uidset != null && uidset.size()>0){
        List<user> uList =[select id, email from user where id in : uidSet];
        uidrecmap.putAll(uList);
    }
    if(earidset != null && earidset.size()>0)
    {
        List<Attachment> attList = [SELECT Body,BodyLength,ContentType,Id,Name,ParentId FROM Attachment where ParentId in :earidset ];
        if(attList  != null && attList.size()>0)
        {
            for(Attachment att:attList )
            {
                if(pidAttListMap.containsKey(att.ParentId))
                {
                    pidAttListMap.get(att.ParentId).add(att);
                }
                else{
                    pidAttListMap.put(att.ParentId , new List<Attachment>());
                    pidAttListMap.get(att.ParentId).add(att);
                }
            }
            
        }
    }
    if(tobeProcess != null && tobeProcess.size()>0)
    {
         for(Event_Automation_Request__c obj: tobeProcess )
        {
            
           if(obj.Key_Account_Manager_Name_Requestor__c != null )
           {
                
                 // Create the email attachment
                List<Messaging.EmailFileAttachment> efaList = new List<Messaging.EmailFileAttachment>();
                
                if(pidAttListMap.containskey(obj.id))
                {
                    for(Attachment att: pidAttListMap.get(obj.id)){
                        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                        efa.setFileName(att.name);
                        efa.setBody(att.body);
                        efaList.add(efa);
                    }
                }
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                List<String> toAddresses = new  List<String>(); 
                toAddresses.add(uidrecmap.get(obj.Key_Account_Manager_Name_Requestor__c).email);
                
                mail.setToAddresses(toAddresses);
                mail.setSubject('Approved Event Attachments: '+obj.Name);
                mail.setPlainTextBody('Please find attachments of Event: '+obj.Name);
                if(efaList != null && efaList.size()>0)
                mail.setFileAttachments(efaList);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
            
        }
        
    }

}