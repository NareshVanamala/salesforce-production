trigger Blockdupe on Contact_Affiliation__c(before insert)
{
    Set<String>AccountContactcomb=new set<string>();
    Map<String,Contact_Affiliation__c>blockdupe= new map<String,Contact_Affiliation__c>();
    For(Contact_Affiliation__c ca : Trigger.New)
    {
       String con= ca.Contact_Name__c;
       String Acc= ca.Account__c;
       con=con.left(15);
       Acc= Acc.left(15);
       String AccCon=Acc+con;
       AccountContactcomb.add(AccCon);  
    }
    system.debug('Check the set...'+ AccountContactcomb);
    //string check='001W000000Bsw2s003W000000G0tb1';
    List<Contact_Affiliation__c>calist=[select Account__c,id,Contact_Name__c, name,CADupeBlocker__c from Contact_Affiliation__c where CADupeBlocker__c in:AccountContactcomb  ];
    system.debug('check the size of the list...'+ calist.size());
    if(calist.size()>0)
    {
        for(Contact_Affiliation__c cat: calist)  
        {
            string cnt= cat.Contact_Name__c;
            cnt=cnt.left(15);
            String Acc= cat.Account__c;
            Acc= Acc.left(15);
            String comb=Acc+cnt;
            blockdupe.put(comb,cat);
        }   
    }
    for(Contact_Affiliation__c catn: Trigger.new)
    {
        string cnt1= catn.Contact_Name__c;
        cnt1 = cnt1.left(15);
        String Acc1= catn.Account__c;
        Acc1= Acc1.left(15);
        String comb1=Acc1+cnt1;
        system.debug('Check the Comb1...'+ comb1);
          
        Contact_Affiliation__c duperecord= blockdupe.get(comb1);
        System.debug('check the duperecord...'+ duperecord);
        if(duperecord!=null)
        {
             String dupeacc=duperecord.Account__c;
             dupeacc= dupeacc.left(15);
             String dupeCon=duperecord.Contact_Name__c;
             dupeCon=dupeCon.left(15);
            if((dupeacc.equals(Acc1))&&(dupeCon.equals(cnt1)))
            
            {
                catn.addError('duplicate Record');    
            } 
            
          }
           else
            {
               blockdupe.put(comb1,catn);  
           }
        
    }
}