trigger AppendingDealName on Deal__c(before insert,  before update)
{
    set<id>dealset=new set<id>(); 
    list<Deal__c>deallist= new list<Deal__c>();
    map<id,Deal__c>delmap=new map<id,Deal__c>();
    map<id,string>dealcityMap= new map<id,string>();
    map<id,string>dealnameMap= new map<id,String>();
    string name; 
    set<string> propertids= new set<string>();
    set<string> ownerids = new set<string>();
   /* for (Deal__c deal : Trigger.New)
    {
     propertids.add(deal.Pipeline_Id__c);
     ownerids.add(deal.Owned_Id__c);
    }
    List<MRI_PROPERTY__c> MriPropList =new  List<MRI_PROPERTY__c>();
    if(ownerids!=null && ownerids.size()>0){
        MriPropList = [Select Name,Property_Manager__c,Common_Name__c,Property_ID__c from MRI_PROPERTY__c where Property_ID__c in : ownerids ];
     
    }if((MriPropList.size()==0)&&(propertids !=null && propertids.size()>0)){
        MriPropList = [Select Name,Property_Manager__c,Common_Name__c,Property_ID__c from MRI_PROPERTY__c where Property_ID__c in : propertids ];
        system.debug('MriPropList2'+MriPropList);
    }
    
    for (MRI_PROPERTY__c MRIobj : MriPropList)
    {
        for(Deal__c dealobj : trigger.new){
            dealobj.Building_Name__c= MRIobj.Name;
            dealobj.Center_Name__c= MRIobj.Common_Name__c;
            dealobj.Property_Manager__c= MRIobj.Property_Manager__c; 
           
        }
    }*/
    for(Deal__c dct:Trigger.new)
    dealset.add(dct.id);   
    deallist=[select id,Report_Deal_Name__c,name,Address__c,Class__c,City__c,State__c,Zip_Code__c from Deal__c where id in:dealset];
       
    for(Deal__c  ct1:deallist) 
    {
        delmap.put(ct1.id,ct1);
        dealnameMap.put(ct1.id,ct1.name);
        dealcityMap.put(ct1.id,ct1.City__c);    
    }
    if(trigger.isinsert)
    {
        For(Deal__c ct:Trigger.new)
        {
            //system.debug('---------------entering for'+ ct.name);
            if(delmap.get(ct.id)!=null)          
            /*{
                if(delmap.get(ct.id).City__c!=null)
                ct.name=ct.name+'/'+delmap.get(ct.id).City__c; 
                if(delmap.get(ct.id).State__c!=null)
                ct.name=ct.name+'/'+delmap.get(ct.id).State__c;
                if(delmap.get(ct.id).Address__c!=null)
                ct.name=ct.name+'/'+delmap.get(ct.id).Address__c;
                if(delmap.get(ct.id).Zip_Code__c!=null)
                ct.name=ct.name+'/'+delmap.get(ct.id).Zip_Code__c; 
                //system.debug('---------------entering is insert'+ ct.name);
            }*/
            //system.debug('---------------after  is insert1'+ ct.name);
            if(ct.City__c!=null)
            {
                ct.name=ct.name+'/'+ct.City__c;
            }
            if((ct.State__c!=null)&&(ct.name.length()<80))
            {
                ct.name=ct.name+'/'+ct.State__c;
            }
            if((ct.Address__c!=null)&&(ct.name.length()<80))
            {
                ct.name=ct.name+'/'+ct.Address__c;
            }
            if((ct.Zip_Code__c!=null)&&(ct.name.length()<80))
            {
                ct.name=ct.name+'/'+ct.Zip_Code__c;
            }
            system.debug('---------------after  is insert1'+ ct.name);
            
            if(ct.name.length() > 80 )
            {
                ct.adderror('Deal Name should not be more than 80 characters, Please reduce the length of Name or Address fields');
            }
        }   
    } 
    if(trigger.isUpdate)
    {
        For(Deal__c ct:Trigger.new)
        {
            system.debug('---------------for update'+ ct.name);
            if(delmap.get(ct.id)!=null)
            {
                Deal__c old = trigger.oldmap.get(ct.id);
                system.debug('---------------update if dealmap not equal to null'+ ct.name);
                
                if(old.Name!=ct.Name)
                {
                    system.debug('---------------update ifname not equal to ctname'+ ct.name);
                    Delmap.get(ct.id).Name=ct.Name;
                    system.debug('---------------entering old not eual to new'+ ct.name);
                    }
                else
                {
                    ct.name=delmap.get(ct.id).Report_Deal_Name__c;
                    system.debug('---------------entering else'+ ct.name);
                    
                    if(ct.City__c!=null)
                    {
                        ct.name=ct.name+'/'+ct.City__c;
                    }
                    if((ct.State__c!=null)&&(ct.name.length()<80))
                    {
                        ct.name=ct.name+'/'+ct.State__c;
                    }
                    if((ct.Address__c!=null)&&(ct.name.length()<80))
                    {
                        ct.name=ct.name+'/'+ct.Address__c;
                    }
                    if((ct.Zip_Code__c!=null)&&(ct.name.length()<80))
                    {
                        ct.name=ct.name+'/'+ct.Zip_Code__c;
                    }
                    system.debug('---------------after elseelse'+ ct.name);               
                }  
                system.debug('---------------entering old  eual to new'+ ct.name);                
                if(ct.name.length() > 80 )
                {
                ct.adderror('Deal Name should not be more than 80 characters, Please reduce the length of Name or Address fields');
                } 
            }
        }         
    }
    
    for(Deal__c dealupdate1:trigger.new)
    {
        if(dealupdate1.name!=null)
        {
            String[] sub= dealupdate1.name.split('/');
            //dealupdate1.Name= sub[0];
            dealupdate1.Report_Deal_Name__c= sub[0];
            System.debug('****' + dealupdate1.Report_Deal_Name__c);
        }   
        
    }
}