trigger codeonPIF on Project_Initiation__c  (before insert)
{
    public List<Project_Initiation__c> prList = new List<Project_Initiation__c>();
    public List<Project_Initiation__c> prList1 = new List<Project_Initiation__c>();
    public String valueincremented=null;
    public string fullcode=null;
    public integer codevalue=0;
    public Integer count;
    String year=string.valueOf(system.today().year()).right(2);
    integer yearthis=integer.valueOf(year);
    integer thisyear=system.today().year();
    String thisYearString= string.valueOf(thisyear);
    String thisYearString1= thisYearString.right(2);
    string valuetobeattached=null;
    prList=[select id, name,Code__c,ARCP_Integration_Project__c from Project_Initiation__c where ARCP_Integration_Project__c=true  order by createddate desc limit 1 ];
    //system.debug('check the prlist..'+prList[0]);
    prList1= [select id, name,Code__c,ARCP_Integration_Project__c from Project_Initiation__c where ARCP_Integration_Project__c=false  order by createddate desc limit 1 ]; 
   // system.debug('check the prlist1..'+prList1[0]);

    for(Project_Initiation__c ear: Trigger.new)
   {
         // system.debug('I am in the loop..');
          if(ear.Manager__c==null)
          ear.Manager__c=UserInfo.getUserId(); 
          if(ear.ARCP_Integration_Project__c == true)
          {
            if((prList.size() > 0))
             {
               // if(prList[0].Code__c!=null)
                //{
                    fullcode=prList[0].Code__c;
                    valueincremented= fullcode.right(3); 
                    codevalue= integer.valueOf(valueincremented);                
                    codevalue=codevalue+1;
                    if(codevalue<10)
                    {
                        //valuetobeattached='00'+string.valueOf(codevalue);
                        ear.Code__c = 'PRJA' + '00' + string.valueOf(codevalue);
                    }
                    if((codevalue>=10)&&(codevalue<100))
                    {
                        //valuetobeattached='00'+string.valueOf(codevalue);
                        ear.Code__c = 'PRJA' + '0' + string.valueOf(codevalue);
                    }

                     if((codevalue>=100))
                    {
                        //valuetobeattached='00'+string.valueOf(codevalue);
                        ear.Code__c = 'PRJA'  + string.valueOf(codevalue);
                    }
                 //}   
                              
               } 
              else if((prList.size() == 0))
              {
                    ear.Code__c = 'PRJA' + '001' ;         
              } 
            } 
     if(ear.ARCP_Integration_Project__c == false)
       {

            if((prList1.size() > 0))
             {
               // if(prList1[0].Code__c!=null)
                //{
                    fullcode=prList1[0].Code__c;
                    valueincremented= fullcode.right(3); 
                    codevalue= integer.valueOf(valueincremented);                
                    codevalue=codevalue+1;
                    if(codevalue<10)
                    {
                        //valuetobeattached='00'+string.valueOf(codevalue);
                        ear.Code__c = 'PRJ' + thisYearString1 + '00' + string.valueOf(codevalue);
                    }
                    if((codevalue>=10)&&(codevalue<100))
                    {
                        //valuetobeattached='00'+string.valueOf(codevalue);
                        ear.Code__c = 'PRJ' +thisYearString1 + '0' + string.valueOf(codevalue);
                    }

                     if((codevalue>=100))
                    {
                        //valuetobeattached='00'+string.valueOf(codevalue);
                        ear.Code__c = 'PRJ'  +thisYearString1 + string.valueOf(codevalue);
                    }
                // }   
                              
               } 
              else if((prList.size() == 0))
              {
                    ear.Code__c = 'PRJ' + thisYearString1 +'001' ;         
              } 



       }
    }
  }