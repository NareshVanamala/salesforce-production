trigger VacantSuiteValidator on Lease__c (after insert,after update) {
    List<Lease__c> activeLeaseList = new List<Lease__c>();
    List<Lease__c> inActiveLeaseList = new List<Lease__c>();
     Set<ID> ids = Trigger.newMap.keySet();
     set<string> activeSuiteids = new set<string>();
     set<string> inActiveSuiteids = new set<string>();
     set<id> mripropertyIds = new set<id>();
     for(Lease__c leaseObj : trigger.new){
        mripropertyIds.add(leaseObj.MRI_PROPERTY__c);
     }
     set<id> activeleases  = new set<id>();
     set<id> inactiveleases  = new set<id>();
    for(Lease__c leaseObj :[Select id,SuiteId__c,SuitVacancyIndicator__c,PropertId_SuiteId__c,Lease_Status__c,MRI_PROPERTY__r.Property_ID__c,MRI_PROPERTY__c from Lease__c where MRI_PROPERTY__c in:mripropertyIds and id in :ids and Lease_Status__c = 'Active']){
        if(leaseObj.Lease_Status__c=='Active'){
            activeleases.add(leaseObj.id);
            activeSuiteids.add(leaseObj.MRI_PROPERTY__r.Property_ID__c+'_'+leaseObj.SuiteId__c);
            system.debug('activeSuiteids'+activeSuiteids);
            
        } }
    for(Lease__c leaseObj1 :[Select id,SuiteId__c,SuitVacancyIndicator__c,PropertId_SuiteId__c,Lease_Status__c,MRI_PROPERTY__r.Property_ID__c,MRI_PROPERTY__c from Lease__c where MRI_PROPERTY__c in:mripropertyIds and id in :ids and Lease_Status__c != 'Active'])
         if(leaseObj1.Lease_Status__c!='Active' ){
            Boolean result;
            result = activeSuiteids.contains(leaseObj1.MRI_PROPERTY__r.Property_ID__c+'_'+leaseObj1.SuiteId__c);
            system.debug('test '+leaseObj1.MRI_PROPERTY__r.Property_ID__c+'_'+leaseObj1.SuiteId__c);
            system.debug('result '+result);
            if(!result){
            inactiveleases.add(leaseObj1.id);
            inActiveSuiteids.add(leaseObj1.MRI_PROPERTY__r.Property_ID__c+'_'+leaseObj1.SuiteId__c);
            system.debug('inActiveSuiteids'+inActiveSuiteids);
        }}
    
    
    
    if(activeSuiteids!=null && activeSuiteids.size()>0){
        for(Lease__c lease : [select id,Lease_Status__c,SuitVacancyIndicator__c,SuiteId__c,PropertId_SuiteId__c,MRI_PROPERTY__c from Lease__c where MRI_PROPERTY__c in: mripropertyIds and PropertId_SuiteId__c in :activeSuiteids ]){
            lease.SuitVacancyIndicator__c = false;
            system.debug('lease'+lease.SuitVacancyIndicator__c);
            activeLeaseList.add(lease);
        }
        if(activeLeaseList !=null && activeLeaseList.size()>0 && Avoidrecursivecls.isActive){
            Avoidrecursivecls.isActive = false;
            system.debug('activeLeaseList'+activeLeaseList);
            update activeLeaseList;
        }
    }
    if(inActiveSuiteids!=null && inActiveSuiteids.size()>0){
        for(Lease__c inactiveleaseObj : [select id,Lease_Status__c,SuitVacancyIndicator__c,PropertId_SuiteId__c,SuiteId__c,MRI_PROPERTY__c from Lease__c where MRI_PROPERTY__c in: mripropertyIds and PropertId_SuiteId__c in :inActiveSuiteids]){
            inactiveleaseObj.SuitVacancyIndicator__c = true;
            system.debug('inactiveleaseObj'+inactiveleaseObj.SuitVacancyIndicator__c);
            inActiveLeaseList.add(inactiveleaseObj);
        }
        
        if(inActiveLeaseList !=null && inActiveLeaseList.size()>0 && Avoidrecursivecls.isInActive){
            system.debug('inactive list'+inActiveLeaseList);
            Avoidrecursivecls.isInActive = false;
            system.debug('inActiveLeaseList'+inActiveLeaseList);
            update inActiveLeaseList;
        }
    }
    
}