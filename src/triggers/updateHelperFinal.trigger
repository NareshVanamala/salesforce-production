trigger updateHelperFinal on REIT_Investment__c (after insert)
{
    List<REIT_Investment__c> lstreits = new List<REIT_Investment__c>();
    List<REIT_Proxy_Votes__c> Insertlist= new List<REIT_Proxy_Votes__c>();
    Map<Id, National_Account_Helper__c> accountNAHMap = new Map<Id, National_Account_Helper__c>();
    Id recdBrokerDealer=[Select id,name,DeveloperName from RecordType where DeveloperName='BrokerDealerAccount' and SobjectType = 'Account'].id;
    //date firstdayCurrentyearminus3 = date.newInstance(Date.today().Year()-3,1,1); 
    List<National_Account_Helper__c> nahListExisting = new List<National_Account_Helper__c>();
    set<Id> accIds = new Set<Id> ();
    date firstdaylastyear = date.newInstance(Date.today().Year()- 1,1,1);
    date lastdaylastyear = date.newInstance(Date.today().Year()-1,12,31);  
    date firstdayCurrentyearminus2 = date.newInstance(Date.today().Year()- 2,1,1);
    date lastdayCurrentyearminus2  = date.newInstance(Date.today().Year()-2,12,31); 
    date firstdayCurrentyearminus3 = date.newInstance(Date.today().Year()-3,1,1);
    date lastdayCurrentyearminus3  = date.newInstance(Date.today().Year()-3,12,31); 
    date startdateJan=date.newInstance(Date.today().Year(),1,1);
    date enddateJan=date.newInstance(Date.today().Year(),1,31);
   date startdateFeb=date.newInstance(Date.today().Year(),2,1);
    date enddateFeb=date.newInstance(Date.today().Year(),2,28);
   date startdateMar=date.newInstance(Date.today().Year(),3,1);
    date enddateMar=date.newInstance(Date.today().Year(),3,31);
    date startdateApril=date.newInstance(Date.today().Year(),4,1);
    date enddateApril=date.newInstance(Date.today().Year(),4,30);
    date startdateMay=date.newInstance(Date.today().Year(),5,1);
    date enddateMay=date.newInstance(Date.today().Year(),5,31);
    date startdateJune=date.newInstance(Date.today().Year(),6,1);
    date enddateJune=date.newInstance(Date.today().Year(),6,30);
  
     date startdateJuly=date.newInstance(Date.today().Year(),7,1);
    date enddateJuly=date.newInstance(Date.today().Year(),7,31);
  
     date startdateAugust=date.newInstance(Date.today().Year(),8,1);
    date enddateAugust=date.newInstance(Date.today().Year(),8,31);
  
     date startdateSeptember=date.newInstance(Date.today().Year(),9,1);
    date enddateSeptember=date.newInstance(Date.today().Year(),9,30);
  
     date startdateoctober=date.newInstance(Date.today().Year(),10,1);
    date enddateOctober=date.newInstance(Date.today().Year(),10,31);
  
     date startdateNOv=date.newInstance(Date.today().Year(),11,1);
    date enddateNov=date.newInstance(Date.today().Year(),11,30);
  
     date startdateDec=date.newInstance(Date.today().Year(),12,1);
    date enddateDec=date.newInstance(Date.today().Year(),12,31);
   
    decimal currentcapitallastyearCCPt = 0;      
    decimal currentcapitallastyearCCPTIII = 0; 
    decimal currentcapitallastyearCCPTII = 0; 
    decimal currentcapitallastyearCCIT = 0; 
    decimal currentcapitallastyearINCOMENAV = 0; 
    decimal currentcapitallastyearCCPTIV = 0;
      
    system.debug('check the brokerdealer recordtype..'+recdBrokerDealer);      
    for(REIT_Investment__c objReit : Trigger.New)
    {
          accIds.add(objReit.accontid__c);
           
     }
system.debug('check the keyset'+accIds);  


nahListExisting=[ select IncomeNAVALastyear__c,IncomeNAVILastyear__c,CCITIILastyear__c,CCPTVLastyear__c,CurrentYearMinus2IncomeNAVA__c,CurrentYearMinus2IncomeNAVI__c,CurrentYearMinus2CCITII__c,CurrentYearMinus2CCPTV__c,CurrentyearMinus3IncomeNAVA__c,CurrentyearMinus3IncomeNAVI__c,CurrentyearMinus3CCITII__c,CurrentyearMinus3CCPTV__c,
JanIncomeNavA__c,JanIncomeNAVI__c,janCCITII__c,JanCCPTV__c,FebIncomeNAVA__c,FebIncomeNAVI__c,FebCCITII__c,FebCCPTV__c,MarIncomeNAVA__c,MarIncomeNAVI__c,MarCCITII__c,MarCCPTV__c,AprilIncomeNAVA__c,AprIncomeNAVI__c,AprCCITII__c,AprCCPTV__c,MayIncomeNAVA__c,MayIncomeNAVI__c,MayCCITII__c,MayCCPTV__c,JuneIncomeNAVA__c,JuneIncomeNAVI__c,JuneCCITII__c,JuneCCPTV__c,JulyIncomeNAVA__c,JulyIncomeNAVI__c,JulyCCITII__c,JulyCCPTV__c,AugIncomeNAVA__c,AugIncomeNAVI__c,AugCCITII__c,AugCCPTV__c,SepIncomeNAVA__c,SepIncomeNAVI__c,SepCCITII__c,SepCCPTV__c,OctIncomeNAVA__c,OctIncomeNAVI__c,octCCITII__c,OctCCPTV__c,NovIncomeNAVA__c,NovIncomeNAVI__c,NovCCITII__c,NovCCPTV__c,DecIncomeNAVA__c,DecIncomeNAVI__c,DecCCITII__c,DecCCPTV__c,
Account__c,AprilCCIT__c,AprilCCPT__c,AprilCCPTII__c,AprilCCPTIII__c,AprilCCPTIV__c,AprilIncomeNav__c,AugustCCIT__c,AugustCCPT__c,AugustCCPTII__c,AugustCCPTIII__c,AugustCCPTIV__c,
AugustIncomeNav__c,CCITlastyear__c,CCPTIIILastYear__c,CCPTIILastYear__c,CCPTIVlastyear__c,CCPT_LastYear__c,CurrentYearMinus2CCIT__c,CurrentYearMinus2CCPT__c,CurrentYearMinus2CCPTII__c,CurrentYearMinus2CCPTIII__c,
CurrentYearMinus2CCPTIV__c,CurrentYearMinus2IncomeNav__c,CurrentyearMinus3CCIT__c,CurrentYearMinus3CCPT__c,CurrentYearMinus3CCPTII__c,CurrentYearMinus3CCPTIII__c,CurrentYearMinus3CCPTIV__c,CurrentYearMinus3IncomeNav__c,
FebCCIT__c,FebCCPT__c,FebCCPTII__c,FebCCPTIII__c,FebCCPTIV__c,FebIncomeNav__c,INCOMENAVLastyear__c,JanCCIT__c,JanCCPT__c,JanCCPTII__c,JanCCPTIII__c,JanCCPTIV__c,JanIncomeNav__c,JulyCCIT__c,JulyCCPT__c,JulyCCPTII__c,JulyCCPTIII__c,JulyCCPTIV__c,
JulyIncomeNav__c,JuneCCIT__c,JuneCCPT__c,JuneCCPTII__c,JuneCCPTIII__c,JuneCCPTIV__c,JuneIncomeNav__c,MarCCIT__c,MarCCPT__c,MarCCPTII__c,MarCCPTIII__c,MarCCPTIV__c,MarIncomeNav__c,MayCCIT__c,MayCCPT__c,MayCCPTII__c,MayCCPTIII__c,MayCCPTIV__c,MayIncomeNav__c,NovCCIT__c,
NovCCPT__c,NovCCPTII__c,NovCCPTIII__c,NovIncomeNav__c,OctCCIT__c,OctCCPT__c,OctCCPTII__c,OctCCPTIII__c,OctCCPTIV__c,OctIncomeNav__c,SeptCCIT__c,SeptCCPT__c,SeptCCPTII__c,SeptCCPTIII__c,SeptCCPTIV__c,SeptIncomeNav__c,DecCCIT__c,DecCCPT__c,DecCCPTII__c,DecCCPTIII__c,DecCCPTIV__c,DecIncomeNav__c,NovCCPTIV__c from National_Account_Helper__c where Account__c in:accIds ]; 
  
  system.debug('$$$$$'+nahListExisting); 
       for(National_Account_Helper__c objNH:nahListExisting)
       {
          accountNAHMap.put(objNH.Account__c,objNH); 
       }
       system.debug('check the map..'+accountNAHMap);
 
for(REIT_Investment__c objReit : Trigger.New)
{
   
  if(accountNAHMap.get(objReit.accontid__c)!=null)
   {
     if((objReit.Deposit_Date__c >= startdateJuly)&&(objReit.Deposit_Date__c<=enddateJuly))
     {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).JulyCCPTIII__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).JulyCCPT__c += integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).JulyCCPTII__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).JulyCCIT__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).JulyIncomeNav__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).JulyCCPTIV__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3774')
   accountNAHMap.get(objReit.accontid__c).JulyIncomeNAVA__c+= integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3775')
   accountNAHMap.get(objReit.accontid__c).JulyIncomeNAVI__c+= integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3776')
   accountNAHMap.get(objReit.accontid__c).JulyCCITII__c+= integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3777')
   accountNAHMap.get(objReit.accontid__c).JulyCCPTV__c+= integer.valueOf(objReit.Current_Capital__c);
    
  }
 //************august************
       if((objReit.Deposit_Date__c >=startdateAugust)&&(objReit.Deposit_Date__c<=enddateAugust))
     {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).AugustCCPTII__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).AugustCCPT__c  += integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).AugustCCPTII__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).AugustCCIT__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).AugustIncomeNav__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).AugustCCPTIV__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3774')
    accountNAHMap.get(objReit.accontid__c).AugIncomeNAVA__c+= integer.valueOf(objReit.Current_Capital__c);  
   else if(objReit.Fund__c=='3775')
    accountNAHMap.get(objReit.accontid__c).AugIncomeNAVI__c+= integer.valueOf(objReit.Current_Capital__c);  
   else if(objReit.Fund__c=='3776')
    accountNAHMap.get(objReit.accontid__c).AugCCITII__c+= integer.valueOf(objReit.Current_Capital__c);
    else if(objReit.Fund__c=='3777')
    accountNAHMap.get(objReit.accontid__c).AugCCPTV__c+= integer.valueOf(objReit.Current_Capital__c); 
                
 }
 if((objReit.Deposit_Date__c >=startdateSeptember)&&(objReit.Deposit_Date__c<=enddateSeptember))
 
     {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).SeptCCPTIII__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).SeptCCPT__c += integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).SeptCCPTII__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).SeptCCIT__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).SeptIncomeNav__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).SeptCCPTIV__c += integer.valueOf(objReit.Current_Capital__c);  
   else if(objReit.Fund__c=='3774')
    accountNAHMap.get(objReit.accontid__c).SepIncomeNAVA__c+= integer.valueOf(objReit.Current_Capital__c); 
  else if(objReit.Fund__c=='3775')
    accountNAHMap.get(objReit.accontid__c).SepIncomeNAVI__c+= integer.valueOf(objReit.Current_Capital__c); 
  else if(objReit.Fund__c=='3776')
    accountNAHMap.get(objReit.accontid__c).SepCCITII__c+= integer.valueOf(objReit.Current_Capital__c); 
  else if(objReit.Fund__c=='3777')
    accountNAHMap.get(objReit.accontid__c).SepCCPTV__c+= integer.valueOf(objReit.Current_Capital__c);         
    
    
              
 }
 if((objReit.Deposit_Date__c >=startdateoctober)&&(objReit.Deposit_Date__c<=enddateOctober))
     {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).OctCCPTIII__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).OctCCPT__c  += integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).OctCCPTII__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).OctCCIT__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).OctIncomeNav__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).OctCCPTIV__c += integer.valueOf(objReit.Current_Capital__c); 
    else if(objReit.Fund__c=='3774')
    accountNAHMap.get(objReit.accontid__c).OctIncomeNAVA__c+= integer.valueOf(objReit.Current_Capital__c); 
     else if(objReit.Fund__c=='3775')
    accountNAHMap.get(objReit.accontid__c).OctIncomeNAVI__c+= integer.valueOf(objReit.Current_Capital__c); 
     else if(objReit.Fund__c=='3776')
    accountNAHMap.get(objReit.accontid__c).octCCITII__c+= integer.valueOf(objReit.Current_Capital__c); 
     else if(objReit.Fund__c=='3777')
    accountNAHMap.get(objReit.accontid__c).OctCCPTV__c+= integer.valueOf(objReit.Current_Capital__c); 
   
        
 }
 if((objReit.Deposit_Date__c >=startdateNOv)&&(objReit.Deposit_Date__c<=enddateNov))
     {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).NovCCPTIII__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).NovCCPT__c   += integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).NovCCPTII__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).NovCCIT__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).NovIncomeNav__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).NovCCPTIV__c += integer.valueOf(objReit.Current_Capital__c);  
    else if(objReit.Fund__c=='3774')
    accountNAHMap.get(objReit.accontid__c).NovIncomeNAVA__c+= integer.valueOf(objReit.Current_Capital__c); 
    else if(objReit.Fund__c=='3775')
    accountNAHMap.get(objReit.accontid__c).NovIncomeNAVI__c+= integer.valueOf(objReit.Current_Capital__c); 
     else if(objReit.Fund__c=='3776')
    accountNAHMap.get(objReit.accontid__c).NovCCITII__c+= integer.valueOf(objReit.Current_Capital__c); 
     else if(objReit.Fund__c=='3777')
    accountNAHMap.get(objReit.accontid__c).NovCCPTV__c+= integer.valueOf(objReit.Current_Capital__c);             
 }
 if((objReit.Deposit_Date__c >=startdateDec)&&(objReit.Deposit_Date__c<=enddateDec))
     {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).DecCCPTIII__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).DecCCPT__c  += integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).DecCCPTII__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).DecCCIT__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).DecIncomeNav__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).DecCCPTIV__c += integer.valueOf(objReit.Current_Capital__c);   
   else if(objReit.Fund__c=='3774')
    accountNAHMap.get(objReit.accontid__c).DecIncomeNAVA__c+= integer.valueOf(objReit.Current_Capital__c);
    else if(objReit.Fund__c=='3775')
    accountNAHMap.get(objReit.accontid__c).DecIncomeNAVI__c+= integer.valueOf(objReit.Current_Capital__c);
    else if(objReit.Fund__c=='3776')
    accountNAHMap.get(objReit.accontid__c).DecCCITII__c+= integer.valueOf(objReit.Current_Capital__c);
    else if(objReit.Fund__c=='3777')
    accountNAHMap.get(objReit.accontid__c).DecCCPTV__c+= integer.valueOf(objReit.Current_Capital__c);            
 }
 if((objReit.Deposit_Date__c >=startdateJan)&&(objReit.Deposit_Date__c<=enddateJan))
     {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).JanCCPTIII__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).JanCCPT__c += integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).JanCCPTII__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).JanCCIT__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).JanIncomeNav__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).JanCCPTIV__c += integer.valueOf(objReit.Current_Capital__c); 
    else if(objReit.Fund__c=='3774')
    accountNAHMap.get(objReit.accontid__c).JanIncomeNavA__c+= integer.valueOf(objReit.Current_Capital__c); 
     else if(objReit.Fund__c=='3775')
    accountNAHMap.get(objReit.accontid__c).JanIncomeNAVI__c+= integer.valueOf(objReit.Current_Capital__c); 
     else if(objReit.Fund__c=='3776')
    accountNAHMap.get(objReit.accontid__c).janCCITII__c+= integer.valueOf(objReit.Current_Capital__c); 
     else if(objReit.Fund__c=='3777')
    accountNAHMap.get(objReit.accontid__c).JanCCPTV__c+= integer.valueOf(objReit.Current_Capital__c);  
    
               
 }
 if((objReit.Deposit_Date__c >=startdateFeb)&&(objReit.Deposit_Date__c<=enddateFeb))
 {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).FebCCPTIII__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).FebCCPT__c  += integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).FebCCPTII__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).FebCCIT__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).FebIncomeNav__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).FebCCPTIV__c += integer.valueOf(objReit.Current_Capital__c);
  else if(objReit.Fund__c=='3774')
    accountNAHMap.get(objReit.accontid__c).FebIncomeNAVA__c+= integer.valueOf(objReit.Current_Capital__c);
    else if(objReit.Fund__c=='3775')
    accountNAHMap.get(objReit.accontid__c).FebIncomeNAVI__c+= integer.valueOf(objReit.Current_Capital__c);
    else if(objReit.Fund__c=='3776')
    accountNAHMap.get(objReit.accontid__c).FebCCITII__c+= integer.valueOf(objReit.Current_Capital__c);
    else if(objReit.Fund__c=='3777')
    accountNAHMap.get(objReit.accontid__c).FebCCPTV__c+= integer.valueOf(objReit.Current_Capital__c);              
 }
 if((objReit.Deposit_Date__c >=startdateMar)&&(objReit.Deposit_Date__c<=enddateMar))
 {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).MarCCPTIII__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).MarCCPT__c  += integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).MarCCPTII__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).MarCCIT__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).MarIncomeNav__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).MarCCPTIV__c += integer.valueOf(objReit.Current_Capital__c);    
    else if(objReit.Fund__c=='3774')
    accountNAHMap.get(objReit.accontid__c).MarIncomeNAVA__c+= integer.valueOf(objReit.Current_Capital__c);    
    else if(objReit.Fund__c=='3775')
    accountNAHMap.get(objReit.accontid__c).MarIncomeNAVI__c+= integer.valueOf(objReit.Current_Capital__c);    
    else if(objReit.Fund__c=='3776')
    accountNAHMap.get(objReit.accontid__c).MarCCITII__c+= integer.valueOf(objReit.Current_Capital__c);    
    else if(objReit.Fund__c=='3777')
    accountNAHMap.get(objReit.accontid__c).MarCCPTV__c+= integer.valueOf(objReit.Current_Capital__c);              
 }
 if((objReit.Deposit_Date__c >=startdateApril)&&(objReit.Deposit_Date__c<=enddateApril))
 {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).AprilCCPTIII__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).AprilCCPT__c += integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).AprilCCPTII__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).AprilCCIT__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).AprilIncomeNav__c+= integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).AprilCCPTIV__c += integer.valueOf(objReit.Current_Capital__c);
    else if(objReit.Fund__c=='3774')
    accountNAHMap.get(objReit.accontid__c).AprilIncomeNAVA__c+= integer.valueOf(objReit.Current_Capital__c);
     else if(objReit.Fund__c=='3775')
    accountNAHMap.get(objReit.accontid__c).AprIncomeNAVI__c+= integer.valueOf(objReit.Current_Capital__c);
     else if(objReit.Fund__c=='3776')
    accountNAHMap.get(objReit.accontid__c).AprCCITII__c+= integer.valueOf(objReit.Current_Capital__c);
     else if(objReit.Fund__c=='3777')
    accountNAHMap.get(objReit.accontid__c).AprCCPTV__c+= integer.valueOf(objReit.Current_Capital__c);             
 }
  if((objReit.Deposit_Date__c >=startdateMay)&&(objReit.Deposit_Date__c<=enddateMay))
 {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).MayCCPTIII__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).MayCCPT__c+= integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).MayCCPTII__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).MayCCIT__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).MayIncomeNav__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).MayCCPTIV__c += integer.valueOf(objReit.Current_Capital__c);   
     else if(objReit.Fund__c=='3774')
    accountNAHMap.get(objReit.accontid__c).MayIncomeNAVA__c+= integer.valueOf(objReit.Current_Capital__c);       
      else if(objReit.Fund__c=='3775')
    accountNAHMap.get(objReit.accontid__c).MayIncomeNAVI__c+= integer.valueOf(objReit.Current_Capital__c);       
      else if(objReit.Fund__c=='3776')
    accountNAHMap.get(objReit.accontid__c).MayCCITII__c+= integer.valueOf(objReit.Current_Capital__c);       
      else if(objReit.Fund__c=='3777')
    accountNAHMap.get(objReit.accontid__c).MayCCPTV__c+= integer.valueOf(objReit.Current_Capital__c);                 
 }
 if((objReit.Deposit_Date__c >=startdateJune)&&(objReit.Deposit_Date__c<=enddateJune))
 {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).JuneCCPTIII__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).JuneCCPT__c += integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).JuneCCPTII__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).JuneCCIT__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).JuneIncomeNav__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).JuneCCPTIV__c  += integer.valueOf(objReit.Current_Capital__c); 
    else if(objReit.Fund__c=='3774')
    accountNAHMap.get(objReit.accontid__c).JuneIncomeNAVA__c+= integer.valueOf(objReit.Current_Capital__c);  
     else if(objReit.Fund__c=='3775')
    accountNAHMap.get(objReit.accontid__c).JuneIncomeNAVI__c+= integer.valueOf(objReit.Current_Capital__c);  
     else if(objReit.Fund__c=='3776')
    accountNAHMap.get(objReit.accontid__c).JuneCCITII__c+= integer.valueOf(objReit.Current_Capital__c);  
     else if(objReit.Fund__c=='3777')
    accountNAHMap.get(objReit.accontid__c).JuneCCPTV__c+= integer.valueOf(objReit.Current_Capital__c);              
 }
if((objReit.Deposit_Date__c >=firstdaylastyear )&&(objReit.Deposit_Date__c<=lastdaylastyear ))
 {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).CCPTIIILastYear__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).CCPT_LastYear__c += integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).CCPTIILastYear__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).CCITlastyear__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).INCOMENAVLastyear__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).CCPTIVlastyear__c  += integer.valueOf(objReit.Current_Capital__c);  
   else if(objReit.Fund__c=='3774'){
       system.debug('$$$$$'+ accountNAHMap.get(objReit.accontid__c));
       system.debug('$$$$$'+ accountNAHMap.get(objReit.accontid__c).IncomeNAVALastyear__c);       
    accountNAHMap.get(objReit.accontid__c).IncomeNAVALastyear__c+= integer.valueOf(objReit.Current_Capital__c);  
    }
    else if(objReit.Fund__c=='3775')
    accountNAHMap.get(objReit.accontid__c).IncomeNAVILastyear__c+= integer.valueOf(objReit.Current_Capital__c);  
     else if(objReit.Fund__c=='3776')
    accountNAHMap.get(objReit.accontid__c).CCITIILastyear__c+= integer.valueOf(objReit.Current_Capital__c);  
     else if(objReit.Fund__c=='3777')
    accountNAHMap.get(objReit.accontid__c).CCPTVLastyear__c+= integer.valueOf(objReit.Current_Capital__c);               
 }

 if((objReit.Deposit_Date__c >=firstdayCurrentyearminus2 )&&(objReit.Deposit_Date__c<=lastdayCurrentyearminus2  ))
 {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).CurrentYearMinus2CCPTIII__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).CurrentYearMinus2CCPT__c += integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).CurrentYearMinus2CCPTII__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).CurrentYearMinus2CCIT__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).CurrentYearMinus2IncomeNav__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).CurrentYearMinus2CCPTIV__c   += integer.valueOf(objReit.Current_Capital__c);   
    else if(objReit.Fund__c=='3774')
    accountNAHMap.get(objReit.accontid__c).CurrentYearMinus2IncomeNAVA__c+= integer.valueOf(objReit.Current_Capital__c);   
    else if(objReit.Fund__c=='3775')
    accountNAHMap.get(objReit.accontid__c).CurrentYearMinus2IncomeNAVI__c+= integer.valueOf(objReit.Current_Capital__c);   
     else if(objReit.Fund__c=='3776')
    accountNAHMap.get(objReit.accontid__c).CurrentYearMinus2CCITII__c+= integer.valueOf(objReit.Current_Capital__c);   
     else if(objReit.Fund__c=='3777')
    accountNAHMap.get(objReit.accontid__c).CurrentYearMinus2CCPTV__c+= integer.valueOf(objReit.Current_Capital__c);               
 }
 if((objReit.Deposit_Date__c >=firstdayCurrentyearminus3)&&(objReit.Deposit_Date__c<=lastdayCurrentyearminus3))
 {
   if(objReit.Fund__c=='3767')
   accountNAHMap.get(objReit.accontid__c).CurrentYearMinus3CCPTIII__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3700')
   accountNAHMap.get(objReit.accontid__c).CurrentYearMinus3CCPT__c  += integer.valueOf(objReit.Original_Capital__c);     
   else if(objReit.Fund__c=='3701')
   accountNAHMap.get(objReit.accontid__c).CurrentYearMinus3CCPTII__c  += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3770')
   accountNAHMap.get(objReit.accontid__c).CurrentyearMinus3CCIT__c   += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3771')
   accountNAHMap.get(objReit.accontid__c).CurrentYearMinus3IncomeNav__c += integer.valueOf(objReit.Current_Capital__c);
   else if(objReit.Fund__c=='3772')
    accountNAHMap.get(objReit.accontid__c).CurrentYearMinus3CCPTIV__c   += integer.valueOf(objReit.Current_Capital__c);  
    else if(objReit.Fund__c=='3774')
    accountNAHMap.get(objReit.accontid__c).CurrentyearMinus3IncomeNAVA__c+= integer.valueOf(objReit.Current_Capital__c);  
    else if(objReit.Fund__c=='3775')
    accountNAHMap.get(objReit.accontid__c).CurrentyearMinus3IncomeNAVI__c+= integer.valueOf(objReit.Current_Capital__c);  
     else if(objReit.Fund__c=='3776')
    accountNAHMap.get(objReit.accontid__c).CurrentyearMinus3CCITII__c+= integer.valueOf(objReit.Current_Capital__c); 
    else if(objReit.Fund__c=='3777')
    accountNAHMap.get(objReit.accontid__c).CurrentyearMinus3CCPTV__c += integer.valueOf(objReit.Current_Capital__c);                
 }

            
             
        }
            
            
}

for(REIT_Investment__c ccl: Trigger.New)
     {
        
         REIT_Proxy_Votes__c RV= new REIT_Proxy_Votes__c();
         RV.Investor__c=ccl.Investor_Name__c;
         RV.Fund__c=ccl.Fund__c;
         RV.Contact__c=ccl.Rep_Contact__c;
         RV.Amount__c=ccl.Original_Capital__c;
         RV.Shares__c=ccl.Current_Units__c;
         RV.DST_Account_Number__c=ccl.Account__c;
         RV.REIT_Investemnt_id__c=ccl.id; 
         Insertlist.add(RV);
     }
        insert Insertlist;



update accountNAHMap.values(); 
         system.debug('check the map...'+accountNAHMap);
    }