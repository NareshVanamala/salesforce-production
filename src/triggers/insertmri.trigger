trigger insertmri on MRI_PROPERTY__c(after insert,after update,before insert,before update) 
{
List<Web_PROPERTIES__c> MRILIST = new List<Web_PROPERTIES__c>();
List<id> delList= new List<id>();
List<MRI_PROPERTY__c> webproList= new List<MRI_PROPERTY__c>();
List<Leases__c> webList = new List<Leases__c>();
map<string,Deal__c> dealMap = new map<string,Deal__c>();

/*if(Trigger.isinsert && trigger.isBefore ){
    set<string> mripropidSet = new set<string>();
    set<id> mriPropId = new set<id>();
    for(MRI_PROPERTY__c mriTemp : trigger.new){
        mripropidSet.add(mriTemp.Property_ID__c);
        mriPropId.add(mriTemp.id);
        system.debug('MriPropids' +mriPropId);
        system.debug('mripropidSet' +mripropidSet);
    }
    if(mripropidSet!=null && mripropidSet.size()>0){
        Id devRecordTypeId = Schema.SObjectType.Deal__c.getRecordTypeInfosByName().get('Acquisition').getRecordTypeId();
        Id devRecordTypeId1 = Schema.SObjectType.Deal__c.getRecordTypeInfosByName().get('Sale Leaseback').getRecordTypeId();
        system.debug('devRecordTypeId '+devRecordTypeId);
        for(Deal__c dealTemp: [select id,Owned_Id__c,Property_Owner_SPEL__c from Deal__c where Owned_Id__c in :mripropidSet and (RecordTypeId  =:devRecordTypeId OR RecordTypeId  =:devRecordTypeId1) order by createddate desc limit 1]){
            dealMap.put(dealTemp.Owned_Id__c,dealTemp);
            system.debug('dealTemp' +dealTemp);
        }
        system.debug('dealMap' +dealMap);
        
    }
    list<MRI_PROPERTY__c> mriPropertyList = new list<MRI_PROPERTY__c>();
     for(MRI_PROPERTY__c mriObjTemp : trigger.new){
          if(dealMap.containsKey(mriObjTemp.Property_ID__c)){
              system.debug('contains key');
            if(dealMap.get(mriObjTemp.Property_ID__c).Owned_Id__c == mriObjTemp.Property_ID__c){
                //system.debug('if Equals');
                //system.debug('if Equals deal value'+dealMap.get(mriObjTemp.Property_ID__c).Property_Owner_SPEL__c);
                mriObjTemp.Related_Deal__c= dealMap.get(mriObjTemp.Property_ID__c).id;
                mriObjTemp.Property_Owner_SPE__c = dealMap.get(mriObjTemp.Property_ID__c).Property_Owner_SPEL__c;
                system.debug('if Equals update value'+mriObjTemp.Property_Owner_SPE__c);
                
            }
        }
     }
    
    }*/
If(Trigger.isinsert && trigger.isAfter)
{

 ProcessInstance[] oPIs = [select Id, TargetObjectID from ProcessInstance WHERE  TargetObjectId IN :trigger.new];
 Approval.ProcessSubmitRequest[] lRequests = New Approval.ProcessSubmitRequest[]{};
 Map<id, ProcessInstance> mPI = New Map<id, ProcessInstance>();
 For(ProcessInstance gPI : oPIs)
 mPI.put(gPI.TargetObjectID, gPI);
 For(MRI_PROPERTY__c oObj : [select id,Web_Ready__c from MRI_PROPERTY__c where id in :trigger.new and (Program_Id__c = 'CCPT3' OR Program_Id__c = 'ARCP' OR Program_Id__c ='ARCORP')]){
 If (oObj.Web_Ready__c == 'Pending Approval') {   
                    if(mPI.get(oObj.ID)== NULL || test.isrunningtest()) {    
                    // create the new approval request to submit            
                    Approval.ProcessSubmitRequest req   = new   Approval.ProcessSubmitRequest();            
                    req.setComments('Submitted for Approval');            
                    req.setObjectId(oObj.Id);          
                    lRequests.add(req);
                }    
            } 
                
        } 
        
If(lRequests.size()>0){                         
            Approval.ProcessResult[] results =Approval.process(lRequests);    
            
            for(Approval.ProcessResult pResult: results ){
                if(pResult.isSuccess())
                    System.debug('Submitted for approval successfully:  ');
                else
                     System.debug('Submitted for approval failed:  ');
           }
       }  
}
If(Trigger.isupdate && trigger.isAfter)
{   
    set<string> propertyIdSet = new set<string>();
    boolean isExisting = false;
    for(MRI_PROPERTY__c mriprop : Trigger.new){
        propertyIdSet.add(mriprop.Property_ID__c);
    }
   set<id> webpropertiesSet = new set<id>();
    if(propertyIdSet !=null && propertyIdSet.size()>0){
        for(Web_PROPERTIES__c webpropObj : [select id,Property_ID__c from Web_PROPERTIES__c where Property_ID__c in :propertyIdSet]){
            webpropertiesSet.add(webpropObj.id);
        }
    }
    if(webpropertiesSet !=null && webpropertiesSet.size()>0){
        isExisting = true;
    }

 for(MRI_PROPERTY__c temp: Trigger.new)
 {
 //MRI_PROPERTY__c OldObj = trigger.oldMap.get(temp.Id);
  // if(OldObj.Related_Resources__c==temp.Related_Resources__c) {
//   system.debug('old'+OldObj.Related_Resources__c);
 //  system.debug('new'+temp.Related_Resources__c);

 If(temp.status__c == 'Owned' || temp.status__c == 'Managed' || isExisting )
 {
    If(((temp.Program_Id__c == 'CCPT3' && temp.Web_Ready__c == 'Approved') || (temp.Program_Id__c == 'ARCP' && temp.Web_Ready__c == 'Approved') || (temp.Program_Id__c == 'ARCORP' && temp.Web_Ready__c == 'Approved')) || (temp.Program_Id__c != 'ARCP' && temp.Program_Id__c != 'CCPT3' && temp.Program_Id__c != 'ARCORP' && temp.Web_Ready__c == 'Approved'))
     {
 //system.debug('Program_Id__c'+temp.Program_Id__c);
 //system.debug('Web_Ready__c'+temp.Web_Ready__c);
  Web_PROPERTIES__c MRIcopy= new Web_PROPERTIES__c();
   MRIcopy.Related_MRI_Property__c = temp.id;
        MRIcopy.Address__c = temp.Address__c;
        MRIcopy.City__c = temp.City__c;
         MRIcopy.Common_Name__c = temp.Common_Name__c;
          MRIcopy.AvgLeaseTerm__c = temp.AvgLeaseTerm__c;
           MRIcopy.Country__c = temp.Country__c;
            MRIcopy.Date_Acquired__c = temp.Date_Acquired__c;
             MRIcopy.Program_Id__c = temp.Program_Id__c;
              MRIcopy.ProgramName__c = temp.ProgramName__c;
               MRIcopy.Name = temp.Name;
                MRIcopy.Property_ID__c = temp.Property_ID__c; 
                 MRIcopy.Approved__c = temp.Approved__c;
                  MRIcopy.Lease_Status__c = temp.Lease_Status__c;
                  //MRIcopy.Default_Photo__c = temp.Default_Photo__c;
                   MRIcopy.Display_Map_Tab_del__c = temp.Display_Map_Tab_del__c;
                   // MRIcopy.Image__c = temp.Image__c;
                     //MRIcopy.Is_Featured_Property__c = temp.Is_Featured_Property__c;
                       //MRIcopy.Long_Description__c = temp.Long_Description__c;
                         MRIcopy.NonOccupancyCount__c = temp.NonOccupancyCount__c;
                          MRIcopy.OccupancyCount__c = temp.OccupancyCount__c;
                           MRIcopy.Other_Photo__c = temp.Other_Photo__c;
                            MRIcopy.Overview_display__c= temp.Overview_display__c;
                             MRIcopy.ParkSpace__c= temp.ParkSpace__c;
                              //MRIcopy.Photo__c= temp.Photo__c;
                               MRIcopy.Property_Type__c= temp.Property_Type__c;
                                MRIcopy.PurchasePrice__c= temp.PurchasePrice__c;
                                 MRIcopy.Sector__c= temp.Sector__c;
                                  MRIcopy.Short_Description__c= temp.Short_Description__c;
                                   MRIcopy.Site_Plan__c= temp.Site_Plan__c;
                                    MRIcopy.SqFt__c= temp.SqFt__c;
                                     MRIcopy.State__c= temp.State__c;
                                      MRIcopy.Status__c= temp.Website_Status__c;
                                       // MRIcopy.Thumbnail__c= temp.Thumbnail__c;
                                         MRIcopy.TotalSuites__c= temp.TotalSuites__c;
                                          MRIcopy.Web_Ready__c= temp.Web_Ready__c;
                                           MRIcopy.Zip_Code__c= temp.Zip_Code__c;
                                            MRIcopy.Asset_Count__c= temp.Asset_Count__c;
                                             MRIcopy.Sale_Date__c= temp.Sale_Date__c;
                                              MRIcopy.Property_Type_Group__c= temp.Property_Type_Group__c;
                                               MRIcopy.MRI_Building_Name__c=temp.MRI_Building_Name__c;
        
                              MRILIST.add(MRIcopy);
                              delList.add(temp.id);
           }
      }
   }
  //}

           //if(Avoidrecursivecls.iswebupdate==true){
        if(MRILIST !=null&&MRILIST.size()>0){
        system.debug('Enter');
             //  Avoidrecursivecls.iswebupdate=false;

               upsert MRILIST property_id__c;
            //   }
            }
if(delList!=null && delList.size()>0){



    webproList = [select id,Property_ID__c,(select id,Name,Default_Photo__c,Industry__c,Lease_ID__c,LeaseType__c,Moodys__c,NYSE__c,Property_ID__c,S_P__c,SuiteId__c,Tenant_ID__c,Tenant_Name__c,web_Ready__c from Lease__r) from MRI_PROPERTY__c where id in:delList];
 for(Integer i=0;i<MRILIST.Size();i++){
    for(Integer j=0;j<webproList.Size();j++){
        If(MRILIST[i].Property_ID__c == webproList[j].Property_ID__c || test.isrunningtest()){
            for(Integer k=0;k<webproList[j].Lease__r.Size();k++){
                Leases__c weblease = new Leases__c();
                weblease.Name = webproList[j].Lease__r[k].Name;
                weblease.MRI_PROPERTIES__c = MRILIST[i].Id;
                weblease.Default_Photo__c = webproList[j].Lease__r[k].Default_Photo__c;
                weblease.Industry__c = webproList[j].Lease__r[k].Industry__c;
                weblease.Lease_ID__c = webproList[j].Lease__r[k].Lease_ID__c;
                weblease.Lease_Type__c = webproList[j].Lease__r[k].LeaseType__c;
                weblease.Moodys__c= webproList[j].Lease__r[k].Moodys__c;
                weblease.NYSE__c = webproList[j].Lease__r[k].NYSE__c;
                weblease.Property_ID__c = webproList[j].Lease__r[k].Property_ID__c;
                weblease.S_P__c= webproList[j].Lease__r[k].S_P__c;
                weblease.SuiteId__c = webproList[j].Lease__r[k].SuiteId__c;
                weblease.Tenant_ID__c = webproList[j].Lease__r[k].Tenant_ID__c;
                weblease.Tenant_Name__c= webproList[j].Lease__r[k].Tenant_Name__c;
                weblease.Web_Ready__c= webproList[j].Lease__r[k].web_Ready__c;
                webList.add(weblease);
                
             }
           
        }
    }
}

upsert webList Lease_ID__c; 
}

    set<string> propertyIdList = new set<string>();
    list<Web_PROPERTIES__c> webPropertyList = new list<Web_PROPERTIES__c>();
    for(MRI_PROPERTY__c mriProprertyObj: [Select id,Web_Ready__c,Property_ID__c from MRI_PROPERTY__c where id in:Trigger.new and Web_Ready__c='Deleted' ]){


        propertyIdList.add(mriProprertyObj.Property_ID__c);
    }

    if((propertyIdList!=null && propertyIdList.size()>0) || test.isrunningtest()){
        for(Web_PROPERTIES__c webPropertyObj : [select id ,Web_Ready__c,Property_ID__c from Web_PROPERTIES__c where Property_ID__c IN :propertyIdList ]){
            webPropertyObj.Web_Ready__c ='Deleted';
            webPropertyObj.Status__c ='Deleted';
            webPropertyList.add(webPropertyObj);
        }
        if(webPropertyList!=null && webPropertyList.size()>0){
            update webPropertyList;
        }
    }
    
            Approval.ProcessSubmitRequest[] lRequests = New Approval.ProcessSubmitRequest[]{};
            Approval.ProcessWorkitemRequest[] RecallRequests = New Approval.ProcessWorkitemRequest[]{};
            For(MRI_PROPERTY__c oObj : [select id,Web_Ready__c,Status__c,Name,City__c,State__c,SqFt__c,Property_Type__c,Property_ID__c from MRI_PROPERTY__c where id in :trigger.new and (Program_Id__c = 'CCPT3' OR Program_Id__c = 'ARCP' OR Program_Id__c ='ARCORP')]){             
                  MRI_PROPERTY__c OldObj = trigger.oldMap.get(oObj.Id);
             
                  If  (( (oObj.Web_Ready__c == 'Pending Approval' && OldObj.Web_Ready__c != 'Pending Approval') && 
                  (oObj.Status__c != OldObj.Status__c  || oObj.Name != OldObj.Name || oObj.City__c != OldObj.City__c  ||  oObj.State__c != OldObj.State__c  || oObj.SqFt__c != OldObj.SqFt__c  || oObj.Property_Type__c != OldObj.Property_Type__c  || oObj.Property_ID__c != OldObj.Property_ID__c)) || (oObj.Web_Ready__c == 'Pending Deleted' && OldObj.Web_Ready__c != 'Pending Deleted' ) || (oObj.Web_Ready__c == 'Pending Approval' && OldObj.Web_Ready__c == 'Pending Deleted' )){

                                             
                  If(OldObj.Web_Ready__c != 'Approved' && OldObj.Web_Ready__c != 'Rejected' && OldObj.Web_Ready__c != 'Deleted' && OldObj.Web_Ready__c != 'Archived')
                  {
                       List<ProcessInstanceWorkitem> piwi = [SELECT Id, ProcessInstanceId, ProcessInstance.TargetObjectId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =:oObj.Id];
                       if(piwi !=null && piwi.size()>0)
                       {  
                      // system.debug('Entering123456');
                         Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                         req.setAction('Removed');        
                         req.setWorkitemId(piwi.get(0).Id);
                         Approval.process(req,false);                       
                        }                                  
                              
                }}
                
                If (((oObj.Web_Ready__c == 'Pending Approval' && OldObj.Web_Ready__c != 'Pending Approval') && 
                (oObj.Status__c != OldObj.Status__c  || oObj.Name != OldObj.Name || oObj.City__c != OldObj.City__c  ||  oObj.State__c != OldObj.State__c  || oObj.SqFt__c != OldObj.SqFt__c  || oObj.Property_Type__c != OldObj.Property_Type__c  || oObj.Property_ID__c != OldObj.Property_ID__c)) || (oObj.Web_Ready__c == 'Pending Deleted' && OldObj.Web_Ready__c !='Pending Deleted' ) || (oObj.Web_Ready__c == 'Pending Approval' && OldObj.Web_Ready__c == 'Pending Deleted' ))
                {    
                              

                         // create the new approval request to submit            
                        Approval.ProcessSubmitRequest req   = new   Approval.ProcessSubmitRequest();            
                        req.setComments('Submitted for Approval');            
                        req.setObjectId(oObj.Id);          
                        lRequests.add(req);       
                   } 
                           
                }
                        
                        If(lRequests.size()>0){                         
                        Approval.ProcessResult[] results =Approval.process(lRequests); 
                 } 
           

}}