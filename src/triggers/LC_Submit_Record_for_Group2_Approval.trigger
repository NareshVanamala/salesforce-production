trigger LC_Submit_Record_for_Group2_Approval on LC_LeaseOpprtunity__c (after Update) 
{
     
  //************************This for Dead LOI **************************
    list<LC_LeaseOpprtunity__c> LeaseOpplist = new list<LC_LeaseOpprtunity__c>();
    
    if(trigger.isAfter)
    { 
         for(LC_LeaseOpprtunity__c lcl:Trigger.new)
        {
           if(lcl.Final_Step_Processes__c==true)
            LeaseOpplist.add(lcl); 
       }
    
     
    if(LeaseOpplist.size()>0)
    LC_LeaseCentralHelper.GetLeaseOpportunityRecord(LeaseOpplist[0].id);
   
  } 
  
}