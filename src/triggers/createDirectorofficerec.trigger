trigger createDirectorofficerec on Director__c (after insert, after update, after delete) 
{
    list<Director_and_Officer__c> lstdirectorandOfficers= new list<Director_and_Officer__c>();
    list<Director_and_Officer__c> lstdirecorandofficeupdate= new list<Director_and_Officer__c>();
    list<Director_and_Officer__c>lstdirectorandOfficersdelete= new list<Director_and_Officer__c>();
    list<Director_and_Officer__c> lstdirectorandOfficers1= new list<Director_and_Officer__c>();
    if(trigger.isInsert)
    {
        
        for(Director__c dir: Trigger.new){ 
            Director_and_Officer__c diroffce= new Director_and_Officer__c();           
            diroffce.Name=dir.name;
            diroffce.Director_and_Officer_Name__c=dir.Director_Name_Lookup__c;
            diroffce.Type__c='Director';
            diroffce.Date_of_Election__c=dir.Date_of_Election__c;
            diroffce.Date_of_Resignation__c=dir.Date_of_Resignation__c;
            diroffce.Entity__c=dir.Entity__c;
            lstdirectorandOfficers.add(diroffce);
        }
        if(lstdirectorandOfficers.size()>0)
        insert lstdirectorandOfficers;
    }
    if(trigger.isupdate)
    {
        for(Director__c dir: Trigger.new){
        list<Director_and_Officer__c> diroffce= [select id,Name,Director_and_Officer_Name__c,Type__c,Date_of_Election__c,Date_of_Resignation__c,Entity__c from Director_and_Officer__c where Name=:dir.name limit 1];                
        if(diroffce.size()>0)
        {
        diroffce[0].Director_and_Officer_Name__c=dir.Director_Name_Lookup__c;
        diroffce[0].Type__c='Director';
        diroffce[0].Date_of_Election__c=dir.Date_of_Election__c;
        diroffce[0].Date_of_Resignation__c=dir.Date_of_Resignation__c;
        diroffce[0].Entity__c=dir.Entity__c;
        lstdirecorandofficeupdate.add(diroffce[0]);
        }
        if(diroffce.size()==0)
        {
          Director_and_Officer__c diroffce1= new Director_and_Officer__c();
          diroffce1.Name=dir.name;
          diroffce1.Director_and_Officer_Name__c=dir.Director_Name_Lookup__c;
          diroffce1.Type__c='Director';
          diroffce1.Date_of_Election__c=dir.Date_of_Election__c;
          diroffce1.Date_of_Resignation__c=dir.Date_of_Resignation__c;
          diroffce1.Entity__c=dir.Entity__c;
          lstdirectorandOfficers1.add(diroffce1);
        
        }
        }
        if(lstdirecorandofficeupdate.size()>0)
        update lstdirecorandofficeupdate;
        if(lstdirectorandOfficers1.size()>0)
        insert lstdirectorandOfficers1;
    }
    if(trigger.isdelete)
    {
        for(Director__c dir: Trigger.old){
        system.debug('..................................................'+dir);
        list<Director_and_Officer__c> diroffce1= [select id,Name,Director_and_Officer_Name__c,Type__c,Date_of_Election__c,Date_of_Resignation__c,Entity__c from Director_and_Officer__c where Name=:dir.name limit 1];            
        if(diroffce1.size()>0)
        lstdirectorandOfficersdelete.add(diroffce1[0]);
        }
        if(lstdirectorandOfficersdelete.size()>0)
        delete lstdirectorandOfficersdelete;
     }
}