trigger ContactTrigger on Contact (after insert,after update,before insert,before update) {
    if(trigger.isAfter){
    Avoidrecursivecls.iscallingfrom=true;
    system.debug('if entered');
    list<Contact_InvestorList__c> conInvList = new list<Contact_InvestorList__c>();
    set<id> coninvids = new set<id>();
    list<Task> tasklist = new list<Task>();
      for(Contact_InvestorList__c conInv : [select id,Fund__c,Send_Pdoc_complete__c,Advisor_Name__r.Pdoc_Ready__c,Attachment_Id__c,Advisor_Name__r.Sending_PDoc_Complete__c,Advisor_Name__c,Advisor_Name__r.Pdoc_Email_delivery__c,Pdoc__c from Contact_InvestorList__c where Advisor_Name__c in :trigger.new]){
      if((conInv.Advisor_Name__r.Pdoc_Email_delivery__c && conInv.Fund__c!=null && (!conInv.Advisor_Name__r.Sending_PDoc_Complete__c))|| Test.isRunningtest()){
        conInv.Pdoc__c  = true;
        //conInv.Advisor_Name__r.Pdoc_Email_delivery__c = false;
        system.debug('entered');
        conInvList.add(conInv);
        coninvids.add(conInv.Advisor_Name__c);
    }
   /*if(conInv.Advisor_Name__r.Pdoc_Ready__c==true){
        conInv.Send_Pdoc_complete__c = false;
        conInvList.add(conInv);
    }*/
      
      } 
      if(conInvList!=null && conInvList.size()>0){
 
           //list<Contact> conList = new list<Contact>();
          for(Contact con : trigger.new){
                if(con.Pdoc_Email_delivery__c){
                    Task newTask = new Task(Description = 'pdoc requested',
                                        Priority = 'Normal', 
                                        Status = 'In Progress', 
                                        Subject = 'Pdoc Requested', 
                                        IsReminderSet = true, 
                                        //ReminderDateTime = System.now()+30, 
                                        WhoId =con.id,
                                        OwnerId = userinfo.getuserid());
                tasklist.add(newTask) ; 
                }
               }
         // insert tasklist;
           update conInvList;
      }
}
   /* if(trigger.isBefore){

    for(Contact conobj : trigger.new){
      conobj.GeneratePdocflag__c= true;
        
    }
}*/
    
}