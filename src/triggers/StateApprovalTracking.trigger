trigger StateApprovalTracking on Case (after update) {
    StateApprovalTracking.trackChanges(Trigger.new, Trigger.oldMap);

}