// *** Trigger to create the Automated call list by Priya and Snehal *****
trigger getname on Contact_Call_List__c (before insert)
{
//defined a set of strings tp store the call list names in trigger.new
   /* Set<String> clNameSet = new Set<String>();
 //defined a set of strings to store the users if any in the field owner__c over trigger.new
    Set<String> clUserSet = new Set<String>();
    set<String> contactSet= new Set<String>();
 // This sting of unique combination concatenates contact and call list name and is used for dupe blocking
    public string uniquecombination ;
 // cclList a list of Contact Call List has been defined
    List<Contact_Call_List__c >ccllist= new list<Contact_Call_List__c >();
// This set will store the unique combination values formed
    Set<String> calllistContact= new Set<String>(); 
    Map<String,Contact_Call_List__c >MapofCalllistNameContact= new Map<String,Contact_Call_List__c>();
    Map<String,Id> mapOfNametoId = new Map<String,Id>();
    List<Automated_Call_List__c> clordList = new List<Automated_Call_List__c>();
    List<Automated_Call_List__c> lstAutoCallList = new List<Automated_Call_List__c>();
    public boolean changepriority;
    public List<Contact_Call_List__c> lstContCL= new list<Contact_Call_List__c>();
    public list<Contact_Call_List__c> snoozedlist= new list<Contact_Call_List__c>();
    Map<id,datetime>SnoozeTimemap= new Map<id,datetime>(); 
    Map<id,String>snoozeHoursDaysmap= new Map<id,String>(); 
    Map<id,date>snoozedActiveDateMap= new Map<id,date>();   
    
    Manual_or_Automatic__c ma = [Select Id, Name, Manual_Assignment__c from Manual_or_Automatic__c where Name != null];
     
      
   // **Get the calllistname into the below set from contactcalllist object(new record)**
    for(Contact_Call_List__c conCallListOrd: Trigger.new)
    { 
 // *** Added By Priya to avoid the creation of Calllist with ID **** 
// Here we add the Call list Name and User Ids in the sets defined above
        if(conCallListOrd.CallList_Name__c != null)
        { 
            clNameSet.add(conCallListOrd.CallList_Name__c);
            clUserSet.add(conCallListOrd.Owner__c);
           // Added below on 2/15 by Kunal for dupe Blocking 
            string cn = conCallListOrd.contact__c;
            cn = cn.Left(15);
           //string unique combination defined above stores the concatenated value
            uniquecombination=conCallListOrd.CallList_Name__c + cn;
           //Unique Combination value added in the set defined above
            calllistContact.add(uniquecombination);
           //Added below for snooze
            contactSet.add(conCallListOrd.contact__c);
            system.debug('Check record.....'+calllistContact);
            // Finished adding the values in set for dupe blocking 
            }
         // **********Added from Manual Assignment Trigger*************
            If(ma.Manual_Assignment__c == TRUE)
               conCallListOrd.Owner__c = null;
    }
    System.Debug('queries'+Limits.getQueries());
    System.Debug('myHeapSize'+Limits.getHeapSize());
    // ** check the name against the existing AutomatedCalllist Object and store into the map below**
    // The query below finds the Automated call list values against the call lists being inserted through trigger.new
    lstAutoCallList = [Select Id, Name from Automated_Call_List__c WHERE Name IN :clNameSet];
    // creating the map nametoId where we check if the inserted value exists in the database we put it in the map
    for(Automated_Call_List__c clOrder : lstAutoCallList)
    {
        if(clNameSet.contains(clOrder.Name))
            mapOfNametoId.put(clOrder.Name,clOrder.Id);
    }
    // ** check if the name not exists already, then create the new automated call list record**
    
    //If the map does not contains the value that is being loaded. New automated call list is created
    for(String clName : clNameSet)
    {
        if(!mapOfNametoId.keyset().contains(clName))
        {
            Automated_Call_List__c cordr = new Automated_Call_List__c();
            cordr.Name = clName;
            clordList.add(cordr); 
        }
    }
    // ** insert new records in Contact Call List Object
    try
    {
        System.Debug(Limits.getDMLRows());
        insert clordList;
    }
    catch(Exception e)
    {
        System.Debug(Limits.getDMLRows());
    }
    // ** Get all the newly created Automatedcalllist name and id into the map***
    // put the newly inserted ACL in the map against its ID
    for(Automated_Call_List__c clOrder : clordList)
    {
        mapOfNametoId.put(clOrder.Name,clOrder.Id);
    } 
  //  lstContCL =[select id, CallList_Name__c, Owner__c, Priority__c from Contact_Call_List__c  where CallList_Name__c IN :clNameSet and Owner__c IN:clUserSet];
     //@Kunal
     // Added for the priority 
      // creating the map to connect User Call List and priority
      Map<String,Decimal> mapUserCLConnector = new Map<String,Decimal>();
      If(clNameSet.size()>0)
      {
          for(string st : clNameSet )
          {
              string tem = st;
              system.debug('name of the calllist per user'+tem);
              List <User>  UserList = [Select ID, Name,(Select Id, Priority__c from Contact_Call_List_Orders__r where Priority__c != null  AND CallList_Name__c = :tem  order by lastmodifieddate DESC limit 1) from User where ID in :clUserSet ];
              If(UserList.size()>0)
              {
                  for(User u : UserList)
                  {
                      List<Contact_call_List__c> x = u.Contact_Call_List_Orders__r;
                      system.debug('Size of the child rec list'+ x.size());
                      // Contact_Call_List__c xx = x.get(0);
                      // system.debug('retireved call list for a user from nested recs'+xx);
                      system.debug('Get me the child records per user'+ x);
                      string a = u.ID;
                      a= a.LEFT(15);
                      a = a + '-'  + st;
                      system.debug('key put in priority map'+a) ;
                       // system.debug('val put in priority map'+ u.Contact_Call_List_Orders__r[0].Priority__c ) ;
                       // mapUserCLConnector.put(a,xx.Priority__c );
                        If(u.Contact_Call_List_Orders__r.size()>0)
                        {
                            mapUserCLConnector.put(a,u.Contact_Call_List_Orders__r[0].Priority__c );
                        }
                        else
                        {
                            mapUserCLConnector.put(a,null );
                        }
                      }
            
                 }
           
           
           } 
         
        }
     // Finished Making Map above
     //@Kunal
     // CCl List will have the list of values from contact call list where contact and call list combination exists in trigger.new
     
    ccllist=[select id, ContactCalllist_UniqueConbination__c,contact__c,CallList_Name__c from Contact_Call_List__c where ContactCalllist_UniqueConbination__c In:calllistContact and Call_Complete__c =false and CallList_Name__c!=null and contact__c!=null and ContactCalllist_UniqueConbination__c!=null  ];
    system.debug('SIze of duplicates List is'+ccllist.size());
    
     If(ccllist.size()>0)
    {
    // iterating over the ccl list we will form the combination string where we only consider first 15 chars of contact ID and call list name
    // after forming this unique combination we will add it to the map where key is this combination and value is contact call list within the database
        for(Contact_Call_List__c CCL : ccllist)
        {
            string ccc = CCL.contact__c;
            ccc = ccc.Left(15);
            string combination= CCL.CallList_Name__c+ ccc;
            MapofCalllistNameContact.put(combination,CCL);
         }
    //Finished forming the map of unique combination and ccl in database
    }
    //snoozedlist will have the values of all the snoozed CCL records whose contact ids are present in the set
    snoozedlist=[select id,Active_date__c, Snoozed__c,Snooze_Time__c,Hours_Days_to_Snooze_del__c,contact__c,Call_Complete__c  from Contact_Call_List__c where Call_Complete__c= false and Snoozed__c=true and Hours_Days_to_Snooze_del__c!=null and Snooze_Time__c!=null and contact__c in:contactSet];
   //Put values in the map
    if(snoozedlist.size()>0)
    {
       for(Contact_Call_List__c cclrec:snoozedlist) 
       {
            SnoozeTimemap.put(cclrec.contact__c,cclrec.Snooze_Time__c);    
            snoozeHoursDaysmap.put(cclrec.contact__c,cclrec.Hours_Days_to_Snooze_del__c); 
            snoozedActiveDateMap.put(cclrec.contact__c,cclrec.Active_date__c);  
       }
    
    }
    Integer iCounter = 0;
    Integer iCount = 0;       
   // Here we perform our validations and aggregations over trigger.new
    for(Contact_Call_List__c ct: Trigger.new)
    {
     //If the call list name is not null we associate newly created record with the parent Automated call list
        if(ct.CallList_Name__c != null)    
        {
            ct.CallListOrder__c = mapOfNametoId.get(ct.CallList_Name__c);
        }
     // While inserting through csv insert if the owner is not null and assigned date is null todays date goes in for assign date
        if(ct.Owner__c != null && ct.Assigned_Date__c == null)
        {
            ct.Assigned_Date__c = System.today();
        }
    // If Priority is null and csv generated is true update the priority   
    // First we verify if the new record which is being created has a owner and if that record is through file upload process for which our check is CSVGenerated flag
    // Then we form a string of owner and call list name
    // Then we get this value from our map which we created where we got this concatenated value from database and put it in key and  priority from CCl as the value     
       if((ct.Owner__c != null)&& (ct.CSV_Generated__c == TRUE) )
       {  
           string ow = ct.Owner__c;
           ow = ow.Left(15);
              ow = ow + '-'+ ct.CallList_Name__c;
                    ct.Priority__c= mapUserCLConnector.get(ow);
                  }
    // Finished the method of setting priority over trigger.new
    // check the map for contact id
     if((SnoozeTimemap.get(ct.contact__c)!=null)&&(snoozeHoursDaysmap.get(ct.contact__c)!=null)&&(snoozedActiveDateMap.get(ct.contact__c)!=null))
     {
          ct.Snooze_Time__c=SnoozeTimemap.get(ct.contact__c);
          ct.Hours_Days_to_Snooze_del__c= snoozeHoursDaysmap.get(ct.contact__c);
          ct.Active_date__c= snoozedActiveDateMap.get(ct.contact__c);
          ct.Snoozed__c=true;
      }
    // The following code forms a string where we concatenate call list name and 15 character contact ID
        If((ct.Contact__c != null))
        { 
                
                   string cns = ct.contact__c;
                   cns = cns.Left(15);
                   String check=ct.CallList_Name__c+ cns;
                   system.debug('check if the newly inserted rec is a dupe     '+check);
            
                  // Finished forming the string  combination for the newly inserted record
            
                   system.debug('Check the valuees in map..'+MapofCalllistNameContact.keyset());
                  // Defining a call list where we are getting the values from our map created while finding dupes in system if any..
                  // If the map returns a value we check  against the record being inserted 
                  // If the call list name and contact ID match we block it by adding the error
                  Contact_call_List__c dupelist = MapofCalllistNameContact.get(check);
                   If(dupelist != null)
                     {
                       if((dupelist.Contact__c == ct.Contact__c)&& (dupelist.CallList_Name__c == ct.CallList_Name__c))
                           {
                             ct.addError('Duplicate record');
                           }   
                     } 
                     else
                     {
                      MapofCalllistNameContact.put(check,ct);
                     
                     }
     
                                
              }
            
      }*/
      
     
    
      
      
      
      
      }