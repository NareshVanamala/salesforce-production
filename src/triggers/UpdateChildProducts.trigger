trigger UpdateChildProducts on Products__c (after update)
{

   list<Products__c>UpdateProductlist=new list<Products__c>();
   list<Products__c>Productlist=new list<Products__c>();

    list<Products__c>thisismylist=new list<Products__c>();
   Map<String, List<Products__c>> accountREITs = new Map<String,List<Products__c>>();
   set<string>productname= new set<string>();
     
   for(Products__c pc:Trigger.new)
   {
     if(pc.Is_Master_Record__c==true)
      productname.add(pc.name);
   }
   
   Productlist=[select Yield__c,Tax_Implication__c,Status__c,Major_BD__c,Last_Valuation_Date__c,Invested__c, name,id,Capital_Rate_per_Month__c,Competitor__c,Competitor__r.name,Total_Properties_Loans__c,Offering_size__c,Occupancy_Rate__c,Price_Per_Share__c,MFFO_Ratio__c,Launch_Date__c,FFO_Ratio__c,Expected_Close_Date__c,Diversification__c,Average_Lease_term__c,Strategy__c,Portfolio_Cap_Rate__c,Dividend__c,Talking_Points__c,Products__c,As_of_Date__c from Products__c where name in:productname and Is_Master_Record__c=false];
   if(Productlist.size()>0)
   {
       for(Products__c ri : Productlist)
    {
        if(accountREITs.containsKey(ri.name))
        {
            accountREITs.get(ri.name).add(ri);
        }
        else
        {
            accountREITs.put(ri.name, new List<Products__c>{ri});
        }
    } 
 }
   system.debug('Check the map'+accountREITs);
   for(Products__c ri : trigger.new)
   {
   
      if(accountREITs.get(ri.name)!=null)
     UpdateProductlist= accountREITs.get(ri.name);
     for(Products__c cpp:UpdateProductlist)
     {
   
           cpp.As_of_Date__c=ri.As_of_Date__c;
           cpp.Average_Lease_term__c=ri.Average_Lease_term__c;
                   cpp.Diversification__c=ri.Diversification__c;
                   cpp.Capital_Rate_per_Month__c=ri.Capital_Rate_per_Month__c;
                   cpp.Last_Valuation_Date__c=ri.Last_Valuation_Date__c;
                   cpp.Invested__c=ri.Invested__c;
                   cpp.Major_BD__c=ri.Major_BD__c;
                   cpp.Dividend__c=ri.Dividend__c;
                   cpp.Expected_Close_Date__c=ri.Expected_Close_Date__c;
                   cpp.FFO_Ratio__c= ri.FFO_Ratio__c;
                   cpp.Launch_Date__c= ri.Launch_Date__c;
                   cpp.MFFO_Ratio__c=ri.MFFO_Ratio__c;
                   cpp.Occupancy_Rate__c=ri.Occupancy_Rate__c;
                   cpp.Offering_size__c=ri.Offering_size__c;
                   cpp.Portfolio_Cap_Rate__c=ri.Portfolio_Cap_Rate__c;
                   cpp.Strategy__c=ri.Strategy__c;
                   cpp.Talking_Points__c= ri.Talking_Points__c;
                   cpp.Total_Properties_Loans__c=ri.Total_Properties_Loans__c;
                   cpp.Price_Per_Share__c = ri.Price_Per_Share__c;
                   cpp.Status__c=ri.Status__c;
                   cpp.Tax_Implication__c=ri.Tax_Implication__c;
                   cpp.Yield__c=ri.Yield__c;
                    thisismylist.add(cpp);
     }
   update thisismylist;
   
   }
         
 }