trigger processtaskplan on Task (before update, before delete) {
  if(processtaskplan.processinitializer('processtaskplan')){
        Set<Id> ActionPlanIdSet = new Set<Id>();
        Set<Id> ActionPlanIdSet_Del = new Set<Id>();
        List<Id> temps = new List<Id>();
        List<Id> currentDeals = new List<Id>();
         List<Id> CurrentLoans= new List<Id>();
        list<Deal__c>deallist23= new list<Deal__c>();  

        if(trigger.isupdate){
        
            Map<Id, Task> PlanIdTaskMap = new Map<id, Task>();
        
            for(Task TaskRec: Trigger.new){
                ActionPlanIdSet.add(TaskRec.X18_digit_Id__c);
                PlanIdTaskMap.put(TaskRec.X18_digit_Id__c, TaskRec);
            }
            
            if(ActionPlanIdSet.size()>0){
                
                List<Task_Plan__c> TaskPlanList = new List<Task_Plan__c>(); 
                               
                for(Task_Plan__c TP:[Select TaskRelatedto__c, Status__c, Priority__c, Date_Ordered__c,Template__c, Date_Received__c, Date_Needed__c, 
                Date_Completed__c, Comments__c,TaskRelatedToloan__c, id from Task_Plan__c where ID IN :ActionPlanIdSet]){
                
                
                System.debug('TP ===========>>'+TP);
                
                  temps.add(TP.Template__c); 
                   if(PlanIdTaskMap.Containskey(TP.id)){
                    TP.Comments__c=PlanIdTaskMap.get(TP.id).Description;
                   
                   if(TP.Taskrelatedto__c!=null)
                   currentDeals.add(tp.Taskrelatedto__c);
                   
                   if(TP.TaskRelatedToloan__c!=null)
                      CurrentLoans.add(tp.TaskRelatedToloan__c);
                   
                    TP.Date_Completed__c=PlanIdTaskMap.get(TP.id).Date_Completed__c;
                 //   TP.Date_Needed__c=PlanIdTaskMap.get(TP.id).Date_Needed__c;
                    TP.Date_Received__c=PlanIdTaskMap.get(TP.id).Date_Received__c;
                    TP.Date_Ordered__c=PlanIdTaskMap.get(TP.id).Date_Ordered__c;
                    TP.Date_Needed__c=PlanIdTaskMap.get(TP.id).ActivityDate;
                    TP.Priority__c=PlanIdTaskMap.get(TP.id).Priority;
                    if(PlanIdTaskMap.get(TP.id).Date_Received__c!=null)
                    {
                     PlanIdTaskMap.get(TP.id).Status='completed';
                     TP.status__c = PlanIdTaskMap.get(TP.id).Status;
                    }
                    else
                    {
                    TP.status__c = PlanIdTaskMap.get(TP.id).Status;
                    }
                    TaskPlanList.add(TP);
                   }
                }
        
               if(TaskPlanList.Size()>0){
                    update TaskPlanList;
               }
               
                List<Task_Plan__c> taskplans =[Select Id ,Date_Completed__c,Priority__c,Date_Ordered__c,Date_Needed__c,Status__c,Comments__c,Date_Received__c, Name,(Select Index__c, Name,  Field_to_update_Date_Received__c ,
                Status__c,Date_Needed__c,Date_Received__c,Date_Ordered__c,Parent__c,
                Field_to_update_for_date_needed__c, Notify_Emails__c, Priority__c, 
                Comments__c from Task_Plans__r),Index__c,Field_to_update_Date_Received__c,Field_to_update_for_date_needed__c
                from Task_Plan__c where ID IN:ActionPlanIdSet];
     
     Deal__c Currentdeal=null;
     try{
         Currentdeal =  [Select Id from Deal__c where id in:currentDeals Limit 1] ;
     }catch(Exception e){
     
     }
         
         Loan__c CurrentLoan=null;
         try{
         CurrentLoan =  [Select Id from Loan__c where id in:CurrentLoans Limit 1];
         }catch(Exception e){
         
         }
         
         
      
         System.debug('==========taskplans ========'+taskplans );
             
       System.debug('=========Currentdeal====== '+Currentdeal);
       System.debug('=========CurrentLoan======'+CurrentLoan);
           for(Task_Plan__c T:taskplans) 
            {
            system.debug('============================');
            system.debug('@@@@@@@@@@@'+T.Date_Received__c);
            
             If(T.Date_Received__c!=null && T.Field_to_update_Date_Received__c!=null)
           {
           if(Currentdeal!=null)
           {
            if(T.Field_to_update_Date_Received__c=='Site_Visit_Date__c')
             currentdeal.Site_Visit_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Passed_Date__c')
             currentdeal.Passed_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Estimated_COE_Date__c')
             currentdeal.Estimated_COE_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Hard_Date__c')
             currentdeal.Hard_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Estimated_SP_Date__c')
             currentdeal.Estimated_SP_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Purchase_Date__c')
             currentdeal.Purchase_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Estimated_Hard_Date__c')
             currentdeal.Estimated_Hard_Date__c=T.Date_Received__c;  
            if(T.Field_to_update_Date_Received__c=='Contract_Date__c')
             currentdeal.Contract_Date__c=T.Date_Received__c;
            if(T.Field_to_update_Date_Received__c=='Closing_Date__c')
             currentdeal.Closing_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='LOI_Signed_Date__c')
             currentdeal.LOI_Signed_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='LOI_Sent_Date__c')
             currentdeal.LOI_Sent_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Lease_Abstract_Date__c')
             currentdeal.Lease_Abstract_Date__c=T.Date_Received__c; 
             // if(T.Field_to_update_Date_Received__c=='Lease_Expiration__c')
             //currentdeal.Lease_Expiration__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Open_Escrow_Date__c')
             currentdeal.Open_Escrow_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Dead_Date__c')
             currentdeal.Dead_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Tenant_ROFR_Waiver__c')
             currentdeal.Tenant_ROFR_Waiver__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Date_Audit_Completed__c')
             currentdeal.Date_Audit_Completed__c=T.Date_Received__c; 
             if(T.Field_to_update_Date_Received__c=='Date_Identified__c')
             currentdeal.Date_Identified__c=T.Date_Received__c; 
             if(T.Field_to_update_Date_Received__c=='Investment_Committee_Approval__c')
             currentdeal.Investment_Committee_Approval__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Part_1_Start_Date__c')
             currentdeal.Part_1_Start_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='Estoppel__c')
             currentdeal.Estoppel__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='nd_Estimated_SP_Date__c')
             currentdeal.nd_Estimated_SP_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='nd_SP_End_Date__c')
             currentdeal.nd_SP_End_Date__c=T.Date_Received__c;  
           }
            if(currentLoan!=null)
             {    
             System.debug('$$$'+currentLoan);
             if(T.Field_to_update_Date_Received__c=='Paid_Off_Date__c')
             currentLoan.Paid_Off_Date__c=T.Date_Received__c;
             if(T.Field_to_update_Date_Received__c=='COE_Date__c')
             currentLoan.COE_Date__c=T.Date_Received__c;
             }
             }
           If(T.Date_Needed__c!=null && T.Field_to_update_for_date_needed__c!=null)
           {       
             if(currentdeal!=null)
           {            
             if(T.Field_to_update_for_date_needed__c=='Dead_Date__c')
             currentdeal.Dead_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Appraisal_Date__c')
             currentdeal.Appraisal_Date__c=T.Date_Needed__c;
            If(T.Field_to_update_for_date_needed__c=='Accenture_Due_Date__c')
             currentdeal.Accenture_Due_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Go-Hard Date')
             currentdeal.Go_Hard_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Rep_Burn_Off__c')
             currentdeal.Rep_Burn_Off__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Site_Visit_Date__c')
            currentdeal.Site_Visit_Date__c=T.Date_Needed__c;
           // if(T.Field_to_update_for_date_needed__c=='Query_Sorted_Date__c')
            // currentdeal.Query_Sorted_Date__c=T.Rep_Burn_Off__c;
            if(T.Field_to_update_for_date_needed__c=='Passed_Date__c')
             currentdeal.Passed_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Estimated_COE_Date__c')
             currentdeal.Estimated_COE_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Hard_Date__c')
             currentdeal.Hard_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Estimated_SP_Date__c')
             currentdeal.Estimated_SP_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Purchase_Date__c')
             currentdeal.Purchase_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Estimated_Hard_Date__c')
             currentdeal.Estimated_Hard_Date__c=T.Date_Needed__c;  
            if(T.Field_to_update_for_date_needed__c=='Contract_Date__c')
             currentdeal.Contract_Date__c=T.Date_Needed__c;
            if(T.Field_to_update_for_date_needed__c=='Closing_Date__c')
             currentdeal.Closing_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='LOI_Signed_Date__c')
             currentdeal.LOI_Signed_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='LOI_Sent_Date__c')
             currentdeal.LOI_Sent_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Lease_Abstract_Date__c')
             currentdeal.Lease_Abstract_Date__c=T.Date_Needed__c;  
             // if(T.Field_to_update_for_date_needed__c=='Lease_Expiration__c')
             //currentdeal.Lease_Expiration__c=T.Date_Needed__c;
              if(T.Field_to_update_for_date_needed__c=='Open_Escrow_Date__c')
             currentdeal.Open_Escrow_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Purchase_Date__c')
             currentdeal.Purchase_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Tenant_ROFR_Waiver__c')
             currentdeal.Tenant_ROFR_Waiver__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Date_Audit_Completed__c')
             currentdeal.Date_Audit_Completed__c=T.Date_Needed__c; 
             if(T.Field_to_update_for_date_needed__c=='Date_Identified__c')
             currentdeal.Date_Identified__c=T.Date_Needed__c; 
             if(T.Field_to_update_for_date_needed__c=='Investment_Committee_Approval__c')
             currentdeal.Investment_Committee_Approval__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Part_1_Start_Date__c')
             currentdeal.Part_1_Start_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='Estoppel__c')
             currentdeal.Estoppel__c=T.Date_Needed__c; 
             if(T.Field_to_update_for_date_needed__c=='nd_Estimated_SP_Date__c')
             currentdeal.nd_Estimated_SP_Date__c=T.Date_Needed__c; 
             if(T.Field_to_update_for_date_needed__c=='nd_SP_End_Date__c')
             currentdeal.nd_SP_End_Date__c=T.Date_Needed__c; 
             }
              if(currentLoan!=null)
           {
           system.debug('************'+currentLoan);
           if(T.Field_to_update_for_date_needed__c=='Paid_Off_Date__c')
             currentLoan.Paid_Off_Date__c=T.Date_Needed__c;
             if(T.Field_to_update_for_date_needed__c=='COE_Date__c')
             currentLoan.COE_Date__c=T.Date_Needed__c;
             }
             }
            } 
            
              
        
        if(Currentdeal!=null){
        update Currentdeal;   
        }
        
         if(CurrentLoan!=null){ 
        update CurrentLoan;
        }
        System.debug('=========Currentdeal after update====== '+Currentdeal);
   
         }
        }
        
        if(trigger.isdelete){
            for(Task TaskRec:trigger.old){
                ActionPlanIdSet_Del.add(TaskRec.X18_digit_Id__c);
            }
            if(ActionPlanIdSet_Del.size()>0){
                List<Task_Plan__c> TaskPlanList_Del = new List<Task_Plan__c>();
                TaskPlanList_Del = [Select id from Task_Plan__c where ID IN: ActionPlanIdSet_Del];
                if(TaskPlanList_Del.size()>0){
                    delete TaskPlanList_Del;
                }
            }
        }
    }
  
}