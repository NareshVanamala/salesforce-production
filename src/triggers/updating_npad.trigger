trigger updating_npad on Contact (before update) {
list<contact> clist = new list<contact>();
Map<id,contact> oldmap = new Map<id,contact>();
Map<id,contact> newmap= new Map<id,contact>();
list<RecordType> rrrecordtype= new list<RecordType>();
rrrecordtype = [select Id, Name,DeveloperName from RecordType where (DeveloperName ='NonInvestorRepContact' or DeveloperName='Registered_Investment_Advisor_RIA_Contact' or DeveloperName='Dually_Registered_Contact')and SobjectType = 'Contact'];
 for(contact c:trigger.old)
 { 
  oldmap.put(c.id,c);
 }

 for(contact c:trigger.new)
 {
    date d1=c.Last_Outbound_Call__c;
    date d2=c.Last_Virtual_Appointment__c;
    date d3=c.Last_External_Appointment__c;
    date d4,d5,d6;
    date d7=system.today();
    
    if(c.Last_Outbound_Call__c==null)
     d1=d7.adddays(-730);
    if(c.Last_Virtual_Appointment__c==null)
     d2=d7.adddays(-740);
    if(c.Last_External_Appointment__c==null)
     d3=d7.adddays(-750);
      
    if((c.recordtypeid=='012500000005Rme')||(c.recordtypeid=='012300000004rLn')||(c.recordtypeid=='012500000005Qqz'))
    {
      if(c.priority__c!=oldmap.get(c.id).priority__c && c.Next_Planned_External_Appointment__c != null)
          {
       //c.priority_check__c = true;
        // if(c.Next_Planned_External_Appointment__c != null)
            
              if(c.Priority__c=='1')
              {
                   c.Next_Planned_External_Appointment__c = system.now() + 30; 
                  // c.priority_check__c = false;
               }
              else if(c.Priority__c=='2' || c.Priority__c=='5')
               {
                        c.Next_Planned_External_Appointment__c = system.now() + 90;
                        // c.priority_check__c = false;
               }
                else if(c.Priority__c=='3' || c.Priority__c=='4' || c.Priority__c=='6')
                {
                        c.Next_Planned_External_Appointment__c = system.now() + 120;
                      // c.priority_check__c = false;
               }
               clist.add(c);
               
      
           }
    } 
      d4=(d1>d2? d1:d2);
      d5=(d4>d3? d4:d3);
      c.Last_Successful_Contact__c=d5;
 
    if(c.Last_Outbound_Call__c==null)
     c.Last_Outbound_Call__c=null;
    if(c.Last_Virtual_Appointment__c==null)
     c.Last_Virtual_Appointment__c=null;
    if(c.Last_External_Appointment__c==null)
     c.Last_External_Appointment__c=null;
    if((c.Last_Outbound_Call__c==null)&&(c.Last_Virtual_Appointment__c==null)&&(c.Last_External_Appointment__c==null))
     c.Last_Successful_Contact__c=null;
 }
    
}