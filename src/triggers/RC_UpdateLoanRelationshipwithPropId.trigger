trigger RC_UpdateLoanRelationshipwithPropId on RC_Property__c (after insert,after update)
{
    list<id>dealidlist= new list<id>();
    //get the deal ids of the property just created
    map<string,string> mapToProperties = new map<string,string>();
    for(RC_Property__c dct:Trigger.new){
          dealidlist.add(dct.Related_Deal__c); 
          mapToProperties.put(dct.Related_Deal__c,dct.id);
          }
      
    //get the deal records      
    list<Deal__c>deallist=[select id, Is_Property_Created__c from Deal__c where id in:dealidlist];
    list<Loan_Relationship__c> UpdateLoanRelationshiplist=new list<Loan_Relationship__c>();
    for(Loan_Relationship__c loanRelObj :[select id,Deal__c,Property__c from Loan_Relationship__c where Deal__c in :deallist]){
        loanRelObj.Property__c = mapToProperties.get(loanRelObj.Deal__c);
        UpdateLoanRelationshiplist.add(loanRelObj);
    }
   if(UpdateLoanRelationshiplist !=null &&UpdateLoanRelationshiplist.size()>0){
    update UpdateLoanRelationshiplist;
   }
    
}