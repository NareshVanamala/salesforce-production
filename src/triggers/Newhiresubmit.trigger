trigger Newhiresubmit on NewHireFormProcess__c (after insert) 
{
  List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();
  Id NewhireRequest=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormParent' and SobjectType = 'NewHireFormProcess__c'].id; 
  Id NewhireRequestSubcase=[Select id,name,DeveloperName from RecordType where DeveloperName='NewHireFormChild' and SobjectType = 'NewHireFormProcess__c'].id;
  for(NewHireFormProcess__c n:Trigger.new)
      {
       if((n.recordtypeid==NewhireRequest)&&(n.HR_ManagerApprovalStatus__c=='Submitted For Approval')
            
            //-------Building-------//
            
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Building_General_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Building_Facilities_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Building_RELegal_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Building_IT_Infrastructure_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Building_HR_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Building_Compliance_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Building_PropertyManagement_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Building_Audio_Visual_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Building_Executive_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Building_B2Storage_Status__c=='Submitted for Approval'))
            
            //-------Parking-------//   
            
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Parking_General_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Parking_Executive_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Parking_Contractor_request__c=='Submitted for Approval'))
            
            //-------Hardware-------//
            
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Hardware_Deploy_External__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Hardware_Provide_Desktop__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Hardware_Provide_Laptop__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Hardware_Dual_Monitor_Display__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Hardware_Provide_Iphone__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Hardware_Provide_Ipad__c=='Submitted for Approval'))
            
            //-------Active Directory Group------//
            
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Active_Directory_Group1_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Active_Directory_Group2_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Active_Directory_Group3_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Active_Directory_Group4_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Active_Directory_Group5_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Active_Directory_Group6_Status__c=='Submitted for Approval'))
            
            //-------Access Requested------//
            
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.MRI_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.SalesForce_Deal_Central_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Webex_Access_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.AVID_Account_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Concur_Account_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Argus_Access_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.DST_Access_Requested_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.VPN_Access_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.BNA_Access_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Data_Warehouse_GL_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Data_Warehouse_SalesRequestStatus__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Chatham_Request_Status__c=='Submitted for Approval')) 
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.OneSource_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Web_Filings_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Kardin_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.RisKonnect_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.ADP_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.VDI_Access_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.Box_com_Request_Status__c=='Submitted for Approval'))
            ||((n.recordtypeid==NewhireRequestSubcase)&&(n.HR_ManagerApprovalStatus__c=='Approved')&&(n.DataWarehouse_RealEstate_RequestStatus__c=='Submitted for Approval')))
      
          {
            // create the new approval request to submit    
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();   
            //req.setComments('Submitted for approval. Please approve.');
            req.setObjectId(n.Id);
            approvalReqList.add(req);        
          }
      }
  
    // submit the approval request for processing        
    List<Approval.ProcessResult> resultList = Approval.process(approvalReqList);        
    // display if the reqeust was successful

    for(Approval.ProcessResult result: resultList ){        
             System.debug('Submitted for approval successfully: '+result.isSuccess()); 
             system.debug('Check the approvalhistory '+result);    
            }
  
}