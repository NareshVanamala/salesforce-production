trigger Dupesplitticketcheck on SplitTickets__c (before insert, before update) 
{


    List<SplitTickets__c> oldspt= [SELECT Id,REIT_Investment__c,Rep__c FROM SplitTickets__c];
    
    for(SplitTickets__c objspt:Trigger.new)
    {
        if(Trigger.isInsert)
        {           
            System.debug('objspt--b--'+objspt);
                if(oldspt!=null && oldspt.size()>0)
                {
                    for(SplitTickets__c spt:oldspt)
                    {
                        if(spt!=null)
                        {
                            if(objspt.REIT_Investment__c!= null && objspt.REIT_Investment__c== spt.REIT_Investment__c && objspt.Rep__c == spt.Rep__c && objspt.Rep__c != null)
                            {    System.debug('objspt-00---'+objspt);
                                objspt.addError('Split ticket already exists for this contact');
                                
                            }
                          
                        }
                        
                    }
                }
            }
        }        
}