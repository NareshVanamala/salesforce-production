trigger checkthePercentage on Acquisition_Split__c (after insert, after update, after delete) 
{
   map<id,Acquisition_Split__c > dealSplitmap= new map<id,Acquisition_Split__c>();
   map<id,deal__c> dealIsMap= new map<id,deal__c>();
   set<id>dealidSet= new set<id>();
   list<deal__c>deallist= new list<deal__c>();
   list<Acquisition_Split__c> acquisitionsplitlist=new list<Acquisition_Split__c>();
   Map<Id,List<Acquisition_Split__c>>AcquistiontdealMap = new Map<Id,List<Acquisition_Split__c>>();
   deal__c updatedeal= new deal__c(); 
   list<Acquisition_Split__c> acquisitionsplitlist1=new list<Acquisition_Split__c>();
   decimal total=0;
   decimal remaining=0;
   decimal remaining1=0;
   String coleinitial=null;
  if((trigger.isinsert)||(trigger.isupdate)) 
  {
   for(Acquisition_Split__c  act: Trigger.new)
   {
      dealSplitmap.put(act.deal__c,act);
      dealidSet.add(act.deal__c);
   }
  }
  else if(trigger.isdelete)
  {
   for(Acquisition_Split__c  act: Trigger.old)
   {
      dealSplitmap.put(act.deal__c,act);
      dealidSet.add(act.deal__c);
   }
 
  }
   deallist=[select id,Cole_Initials__c from deal__c where id in:dealidSet];
   acquisitionsplitlist=[select id,Acquisition__r.alias,Acquisition__c,Acquisition_Split__c,Cole_Initials__c,Deal__c,Old_Split_DC_External_ID__c,Total__c from Acquisition_Split__c where deal__c in:dealidSet];
   if(deallist.size()>0)
   {
       for(deal__c ct:deallist)
         dealIsMap.put(ct.id,ct);
     
   }
     for(Acquisition_Split__c  ri : acquisitionsplitlist)
    { 
         if(AcquistiontdealMap.containsKey(ri.Deal__c ))
             AcquistiontdealMap.get(ri.Deal__c ).add(ri);  
         else 
           AcquistiontdealMap.put(ri.Deal__c , new List<Acquisition_Split__c>{ri }); 
        
     }
    /*if(Trigger.isInsert)
    { 
         for(Acquisition_Split__c  act: Trigger.new)
         {
           acquisitionsplitlist1=AcquistiontdealMap.get(act.deal__c);
           if(acquisitionsplitlist1.size()>1)
           {
              
               for(Acquisition_Split__c act1:acquisitionsplitlist1)
               {
                 if(act1.Acquisition_Split__c!=null)
                     total=total+act1.Acquisition_Split__c;
               }
              dealIsMap.get(act.deal__c).Cole_Initials__c=dealIsMap.get(act.deal__c).Cole_Initials__c+'/'+ act.Acquisition__r.alias;

             }  
            else 
            {
            dealIsMap.get(act.deal__c).Cole_Initials__c=act.Acquisition__r.alias;
             } 
             system.debug('Check the total...'+total);
             if(total>100)
                act.Acquisition_Split__c.addError('Total Acquisition percentage should not exceed 100');
           
              }
           update dealIsMap.values();
        }*/
       
      if((Trigger.isUpdate)||(Trigger.isInsert))
    { 
         for(Acquisition_Split__c  act: Trigger.new)
         {
           remaining=act.Acquisition_Split__c;
           system.debug('check the current split percentage..'+act.Acquisition_Split__c);
           acquisitionsplitlist1=AcquistiontdealMap.get(act.deal__c);
           if(acquisitionsplitlist1.size()>0)
           {
               for(Acquisition_Split__c act1:acquisitionsplitlist1)
               {
                if(coleinitial!=null)
                   coleinitial=coleinitial+'/'+act1.Acquisition__r.alias;
                else
                  coleinitial=act1.Acquisition__r.alias;   
                
                 if(act1.Acquisition_Split__c!=null)
                     total=total+act1.Acquisition_Split__c;
                }
                
               dealIsMap.get(act.deal__c).Cole_Initials__c=coleinitial;   
                
             } 
            else
             {
             
                 dealIsMap.get(act.deal__c).Cole_Initials__c=act.Acquisition__r.alias; 
             
             }  
             
             system.debug('Check the total...'+total);
             if(total>100)
             {
              remaining1=100-(total-remaining);
               act.Acquisition_Split__c.addError('Total Acquisition percentage should not exceed 100. You can add up to'+ remaining1+'%');
                       
             }
          }
          }
      update dealIsMap.values();
      if(Trigger.isdelete)
    { 
         for(Acquisition_Split__c  act: Trigger.old)
         {
            remaining=act.Acquisition_Split__c;
           if(AcquistiontdealMap.get(act.deal__c)!=null) 
           {
           acquisitionsplitlist1=AcquistiontdealMap.get(act.deal__c);
           if(acquisitionsplitlist1.size()>0)
           {
               for(Acquisition_Split__c act1:acquisitionsplitlist1)
               {
                if(coleinitial!=null)
                   coleinitial=coleinitial+'/'+act1.Acquisition__r.alias;
                else
                  coleinitial=act1.Acquisition__r.alias;   
                
                 if(act1.Acquisition_Split__c!=null)
                     total=total+act1.Acquisition_Split__c;
                }
                
               dealIsMap.get(act.deal__c).Cole_Initials__c=coleinitial;   
                
             } 
            else if(acquisitionsplitlist1.size()==1)
             {
             
                 dealIsMap.get(act.deal__c).Cole_Initials__c=act.Acquisition__r.alias; 
             
             } 
             
             
             system.debug('Check the total...'+total);
             if(total>100)
             {
                remaining1=100-(total-remaining);
                act.Acquisition_Split__c.addError('Total Acquisition percentage should not exceed 100');
           
            }
      }
       
       else
       {
        dealIsMap.get(act.deal__c).Cole_Initials__c='';
       }
          }
          }
      update dealIsMap.values();
       
       
     }