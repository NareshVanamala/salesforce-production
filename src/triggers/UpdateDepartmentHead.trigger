trigger UpdateDepartmentHead on Event_Request_Split__c(after insert,after update,before delete)
{ 
   public string getdepartment;
   String department=null;
   id DepartmentHead=null;
   id DepartmentExco=null;
   public string eventid1;
   public string eventid2;
   public set<id> setids=new set<id>();
map<id,Event_Automation_Request__c> eventmap= new Map<id,Event_Automation_Request__c>();
list<Event_Automation_Request__c> updatedlist= new list<Event_Automation_Request__c>(); 
 if(trigger.isinsert || trigger.isupdate)
 {
  for(Event_Request_Split__c Ect : Trigger.New)
  {
    setids.add(Ect.Event_Automation_Request__c);
    string eventid=Ect.Event_Automation_Request__c;
    getdepartment=Ect.Department_Split__c;
    Event_Automation_Request__c ct=[select id ,Department_Split__c,Department_Aggregations__c,Region_Aggregations__c,fund_values__c from Event_Automation_Request__c where id=:eventid];
    if(Ect.Department_Split__c!=null)
    {

        system.debug('check the department..+'+getdepartment);
        if(EventAutonation__c.getvalues(getdepartment)!=null)
        {
            EventAutonation__c departmetnITEvent=EventAutonation__c.getvalues(getdepartment);
            department=departmetnITEvent.Department_Name__c;
            DepartmentHead =departmetnITEvent.Department_Head__c;
            DepartmentExco =departmetnITEvent.Department_Exco__c;
        }
         ct.Department_Split__c = department;
         ct.Owner_Department__c=department;
         ct.Owner_Department_Head__c=DepartmentHead;
         ct.Owner_Department_Exco__c=DepartmentExco;
        

        if(Ect.Department_Split__c=='26-Information Technology')
        {
            ct.Department_Split_IT__c= Ect.Department_Split_Percentage__c;
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='25-Human Resources')
        {
            ct.Human_Resources__c= Ect.Department_Split_Percentage__c;
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='01-External Sales')
        {
            ct.ISRG__c= Ect.Department_Split_Percentage__c;
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='02-Internal Sales')
        {
            ct.ISRG__c= Ect.Department_Split_Percentage__c;
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='17-Leasing')
        {
            ct.Leasing__c= Ect.Department_Split_Percentage__c;
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='20-Executive and Admin')
        {
            ct.Leasing__c= Ect.Department_Split_Percentage__c;
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
         if(Ect.Department_Split__c=='21-Accounting')
        {
            ct.Leasing__c= Ect.Department_Split_Percentage__c;
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='12-Legal')
        {
            ct.Legal__c= Ect.Department_Split_Percentage__c;
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='23-Property Management')
        { 
            ct.Property_Management__c= Ect.Department_Split_Percentage__c; 
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
         if(Ect.Department_Split__c=='23-Property Management')
        { 
            ct.Property_Management__c= Ect.Department_Split_Percentage__c; 
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='24-Investment Strategy and Analytics')
        {
            ct.Real_Estate_Finance__c= Ect.Department_Split_Percentage__c;
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='04-Sales Strategy and Analytics')
        {
            ct.Sales_Strategy_and_Analytics__c= Ect.Department_Split_Percentage__c;
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='10-Real Estate Acquisitions')
        {
            ct.Real_Estate_Acquisitions__c= Ect.Department_Split_Percentage__c;
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='15-Underwriting')
        {
           ct.Underwriting__c= Ect.Department_Split_Percentage__c;
           If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='National Accounts')
        {
           ct.National_Accounts__c= Ect.Department_Split_Percentage__c;
           If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='05-Event Planning')
        {
            ct.Event_Planning__c= Ect.Department_Split_Percentage__c;
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='40-External Sales - Advisor')
        {
            ct.External_Sales__c= Ect.Department_Split_Percentage__c; 
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
        }
        if(Ect.Department_Split__c=='45-Internal Sales - Advisor')
        {
            ct.Internal_Sales__c= Ect.Department_Split_Percentage__c;
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            } 
        }
       if(Ect.Department_Split__c=='07-Marketing and Sales Support')
       {
        ct.Marketing_and_Sales_Support__c= Ect.Department_Split_Percentage__c; 
        If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
       }
        if(Ect.Department_Split__c=='22-Operations and Service')
       {
            ct.Operations_and_Service__c= Ect.Department_Split_Percentage__c; 
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
       }
        if(Ect.Department_Split__c=='06-Relationship Management')
       {
            ct.Relationship_Management__c= Ect.Department_Split_Percentage__c; 
            If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
       }
        if(Ect.Department_Split__c=='09-Securities Legal and Compliance')
       { 
           ct.Securities_Legal_and_Compliance__c= Ect.Department_Split_Percentage__c; 
           If(ct.Department_Aggregations__c!=null)
            {
            ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
            else if(ct.Department_Aggregations__c==null)
            {
            ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
            }
       }
            update ct;

//check for all the remaining fields and add here. 
//updatedlist.add(ct);
    } 

if(Ect.Region_Split__c!=null)
{
if(Ect.Region_Split__c=='IBD Central CA')
{
ct.Central_CA__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Chicago')
{
ct.Chicago__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Florida')
{
ct.Florida__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Great Lakes')
{
ct.Great_Lakes__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Mid-Atlantic')
{
ct.Mid_Atlantic__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Midwest')
{
ct.Mid_West__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD New England')
{
ct.New_England__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD North Central')
{
ct.North_Central__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Northern CA')
{
ct.North_CA__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD NY Metro')
{
ct.Ny_Metro__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Ohio Valley/Upstate NY')
{
ct.Ohio_Valley__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Pacific Northwest/Alaska')
{
ct.Pacific_NW__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD South Central')
{
ct.South_Central__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Southeast')
{
ct.SouthEast__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Southern')
{
ct.Southern__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Southern CA')
{
ct.Southern_CA__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Southwest')
{
ct.SouthWest__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='RIA East Metro')
{
ct.East_Metro_Region__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='RIA Midwest')
{
ct.Midwest_Region__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='RIA Northeast')
{
ct.NorthEast_Region__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='RIA Southeast')
{
ct.SouthEast_Region__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='RIA West')
{
ct.West_Region__c= Ect.Split__c; 
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Tri-State')
{
ct.Arizona_Region__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='RIA Central')
{
ct.Central_Region__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Hawaii')
{
ct.Central_Region__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Heartland')
{
ct.Central_Region__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Eastern FL')
{
ct.Central_Region__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
if(Ect.Region_Split__c=='IBD Western FL')
{
ct.Central_Region__c= Ect.Split__c;
If(ct.Region_Aggregations__c!=null)
{
ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
else if(ct.Region_Aggregations__c==null)
{
ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
}
}
update ct;
}

    if(Ect.Fund_Split__c!=null)
    {
        if(Ect.Fund_Split__c=='CCPT II')
        {
          ct.Fund_split_REIT_II__c= Ect.Fund_Split_Percentage__c;
          If(ct.Fund_values__c!=null)
          {
              ct.Fund_values__c = ct.Fund_values__c+','+ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
          }
          else If(ct.Fund_values__c==null)
          {
              ct.Fund_values__c = ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
          }
      }
   if(Ect.Fund_Split__c=='CCIT')
   {
     ct.Fund_split_CCIT__c= Ect.Fund_Split_Percentage__c;
     If(ct.Fund_values__c!=null)
     {
        ct.Fund_values__c = ct.Fund_values__c+','+ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
     else if(ct.Fund_values__c==null)
      {
          ct.Fund_values__c = ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
 }
if(Ect.Fund_Split__c=='INCOME NAV')
{
ct.Fund_split_INCOME_NAV__c= Ect.Fund_Split_Percentage__c;
If(ct.Fund_values__c!=null)
     {
        ct.Fund_values__c = ct.Fund_values__c+','+ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
     else if(ct.Fund_values__c==null)
      {
          ct.Fund_values__c = ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
}
if(Ect.Fund_Split__c=='CCPT I')
{
ct.Fund_split_REIT_1__c= Ect.Fund_Split_Percentage__c;
If(ct.Fund_values__c!=null)
     {
        ct.Fund_values__c = ct.Fund_values__c+','+ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
     else if(ct.Fund_values__c==null)
      {
          ct.Fund_values__c = ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      } 
}
if(Ect.Fund_Split__c=='CCPT III')
{
ct.FundSplitREIT_III__c= Ect.Fund_Split_Percentage__c;
If(ct.Fund_values__c!=null)
     {
        ct.Fund_values__c = ct.Fund_values__c+','+ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
     else if(ct.Fund_values__c==null)
      {
          ct.Fund_values__c = ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
}
if(Ect.Fund_Split__c=='CCPT IV')
{
ct.Fund_Split_REIT_IV__c= Ect.Fund_Split_Percentage__c;
If(ct.Fund_values__c!=null)
     {
        ct.Fund_values__c = ct.Fund_values__c+','+ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
     else if(ct.Fund_values__c==null)
      {
          ct.Fund_values__c = ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
}
if(Ect.Fund_Split__c=='CCIT II')
{
ct.CCIT_II__c= Ect.Fund_Split_Percentage__c;
If(ct.Fund_values__c!=null)
     {
        ct.Fund_values__c = ct.Fund_values__c+','+ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
     else if(ct.Fund_values__c==null)
      {
          ct.Fund_values__c = ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
}
if(Ect.Fund_Split__c=='General')
{
 ct.Fund_Split_General__c= Ect.Fund_Split_Percentage__c;
 If(ct.Fund_values__c!=null)
     {
        ct.Fund_values__c = ct.Fund_values__c+','+ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
     else if(ct.Fund_values__c==null)
      {
          ct.Fund_values__c = ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
}
if(Ect.Fund_Split__c=='CCPT V')
{
If(ct.Fund_values__c!=null)
     {
        ct.Fund_values__c = ct.Fund_values__c+','+ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
     else if(ct.Fund_values__c==null)
      {
          ct.Fund_values__c = ECT.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
      }
}

//please add another funds splits here thanks
// updatedlist.add(ct);
update ct;
} 
} 
}
if(trigger.isdelete)
{
for(Event_Request_Split__c Ect : Trigger.old)
{
    eventid1=Ect.Event_Automation_Request__c;
    eventid2=Ect.id;
}    
Event_Automation_Request__c ct=[select id,Department_Split__c,Region_Aggregations__c,fund_values__c from Event_Automation_Request__c where id=:eventid1 limit 1];
ct.Region_Aggregations__c=null;
ct.fund_values__c=null;
ct.Department_Aggregations__c=null;
update ct;
list<Event_Request_Split__c> ert=[select id,name,Region_Split__c,Split__c,Event_Automation_Request__c from Event_Request_Split__c where Event_Automation_Request__c=:eventid1 and id!=:eventid2 and Region_Split__c!=null and Split__c!=null]; 
list<Event_Request_Split__c> ert1=[select id,name,Fund_Split_Percentage__c,Fund_Split__c,Region_Split__c,Split__c,Event_Automation_Request__c from Event_Request_Split__c where Event_Automation_Request__c=:eventid1 and id!=:eventid2 and Fund_Split__c!=null and Fund_Split_Percentage__c!=null]; 
list<Event_Request_Split__c> ert2=[select id,name,Fund_Split_Percentage__c,Department_Split__c,Department_Split_Percentage__c,Fund_Split__c,Region_Split__c,Split__c,Event_Automation_Request__c from Event_Request_Split__c where Event_Automation_Request__c=:eventid1 and id!=:eventid2 and Department_split__c!=null and Department_Split_Percentage__c!=null]; 

if(ert.size()>0)
{
for(Event_Request_Split__c ect:ert)
{
    
   If(ct.Region_Aggregations__c!=null)
   {
    ct.Region_Aggregations__c= ct.Region_Aggregations__c+','+ Ect.Region_Split__c+'-'+Ect.Split__c+'%';
   }
   else if(ct.Region_Aggregations__c==null)
   {
   ct.Region_Aggregations__c= Ect.Region_Split__c+'-'+Ect.Split__c+'%';
   }
   
   
}
update ct;
}
if(ert1.size()>0)
{
for(Event_Request_Split__c ect:ert1)
{
    
   If(ct.fund_values__c!=null)
   {
    ct.fund_values__c= ct.fund_values__c+','+ Ect.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
   }
   else if(ct.fund_values__c==null)
   {
   ct.fund_values__c= Ect.Fund_Split__c+'-'+Ect.Fund_Split_Percentage__c+'%';
   }
   
   
}
update ct;
}  
if(ert2.size()>0)
{
for(Event_Request_Split__c ect:ert2)
{
    
   If(ct.Department_Aggregations__c!=null)
   {
    ct.Department_Aggregations__c= ct.Department_Aggregations__c+','+ Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
   }
   else if(ct.Department_Aggregations__c==null)
   {
   ct.Department_Aggregations__c= Ect.Department_Split__c+'-'+Ect.Department_Split_Percentage__c+'%';
   }
   
   
}
update ct;
}  
}
//update updatedlist;
}