trigger RentRoll on Rent_Roll__c(After insert,After update, After delete)
{
   Decimal term=0;
   Integer sizeOflist=0;
   Integer count=0;
   set<id>dealid= new set<id>();
   list<Deal__c>dealupdatelist= new list<Deal__c>();
   list<Rent_Roll__c>rentrolllist= new list<Rent_Roll__c>();
   Map<Id,List<Rent_Roll__c >>RentRolllistdealMap = new Map<Id,List<Rent_Roll__c >>();
   map<id,Deal__c>dealMap= new map<id,Deal__c>();
   List<Rent_Roll__c>processesdrentrolllist= new list<Rent_Roll__c>();
   string tenantname=null;
   if((Trigger.isInsert)||(Trigger.isupdate))
   {
       For(Rent_Roll__c rent: trigger.new)
         dealid.add(rent.Deal__c);
   }
  if(trigger.isdelete)
   {
       For(Rent_Roll__c rent: trigger.old)
         dealid.add(rent.Deal__c);
   }
   dealupdatelist=[select Tenant_s__c, id,Lease_Expiration1_c__c, RR_RemainingTerm__c,RR_Rent_Bump__c,RR_SPRating__c,RR_ExpenseRecoveries__c,RR_LeaseExpiration__c,RR_Reimb_Type__c from Deal__c where id in:dealid];
   rentrolllist=[select Name_of_Tenant__c, id,Lease_Expiration__c,Remaining_Term_Years__c,Deal__c from Rent_Roll__c where Deal__c in:dealid];
  //forming the map begin
    for(Rent_Roll__c ri : rentrolllist)
    {
        if(RentRolllistdealMap.containsKey(ri.Deal__c ))
           RentRolllistdealMap.get(ri.Deal__c ).add(ri );
        else
         RentRolllistdealMap.put(ri.Deal__c , new List<Rent_Roll__c >{ri });
        
    } 
   for(Deal__c deal1:dealupdatelist)
    dealMap.put(deal1.id,deal1); 
  //end of forming the deal rent roll map
   if(Trigger.isinsert)
  {
   For(Rent_Roll__c ct: Trigger.new)
   {
    
         processesdrentrolllist=RentRolllistdealMap.get(ct.Deal__c); 
         system.debug('check the size of the list'+processesdrentrolllist);
         if(processesdrentrolllist.size()>1)
         {
           dealMap.get(ct.deal__c).RR_SPRating__c='MT';
           dealMap.get(ct.deal__c).RR_LeaseExpiration__c='Varies'; 
           dealMap.get(ct.deal__c).RR_ExpenseRecoveries__c='Varies';
           dealMap.get(ct.deal__c).RR_Rent_Bump__c='Varies';
          //update the tenants//
           if(dealMap.get(ct.deal__c).Tenant_s__c!=null)
            dealMap.get(ct.deal__c).Tenant_s__c=dealMap.get(ct.deal__c).Tenant_s__c+','+' '+ct.Name_of_Tenant__c;
           else
            dealMap.get(ct.deal__c).Tenant_s__c=ct.Name_of_Tenant__c;
         
           if(dealMap.get(ct.deal__c).Tenancy__c=='Multi-Tenant')
               dealMap.get(ct.deal__c).RR_Reimb_Type__c='NN';
           else
            dealMap.get(ct.deal__c).RR_Reimb_Type__c='Varies';
            for(Rent_Roll__c rr: processesdrentrolllist)
            {    
                if(rr.Remaining_Term_Years__c!=null)
                {
                    term= term + rr.Remaining_Term_Years__c;
                    sizeOflist = sizeOflist+1; 
                }
                else if(rr.Remaining_Term_Years__c==null )
                {
                    count=count+1;
                }
            }
                    
            if(term != 0)
            {
                term=(term/sizeOflist).setScale(2);
            }   
            if(count==processesdrentrolllist.size())
            {
                dealMap.get(ct.deal__c).RR_RemainingTerm__c=null;
            }
            else 
            {    
                dealMap.get(ct.deal__c).RR_RemainingTerm__c= String.valueof(term);
            }
        }
        if(processesdrentrolllist.size()==1)
        {
            dealMap.get(ct.deal__c).RR_SPRating__c = ct.S_P_Rating__c;
            dealMap.get(ct.deal__c).RR_LeaseExpiration__c= String.valueof(ct.Lease_Expiration__c);   
            dealMap.get(ct.deal__c).RR_RemainingTerm__c= String.valueof(ct.Remaining_Term_Years__c);
            dealMap.get(ct.deal__c).RR_ExpenseRecoveries__c= ct.Expense_Recoveries__c;
            dealMap.get(ct.deal__c).RR_Rent_Bump__c= ct.Rent_Bump__c;
            if(dealMap.get(ct.deal__c).Tenancy__c=='Multi-Tenant')
             dealMap.get(ct.deal__c).RR_Reimb_Type__c='NN';
            else
             dealMap.get(ct.deal__c).RR_Reimb_Type__c=ct.Reimb_Type__c;
             
           if(dealMap.get(ct.deal__c).Tenant_s__c!=null)
            dealMap.get(ct.deal__c).Tenant_s__c=dealMap.get(ct.deal__c).Tenant_s__c+','+' '+ct.Name_of_Tenant__c;
           else
            dealMap.get(ct.deal__c).Tenant_s__c=ct.Name_of_Tenant__c;
             
             
             
         }
        
   }
   update dealMap.values();
   }
   
   if(Trigger.isUpdate)
   {
   
      For(Rent_Roll__c ct: Trigger.new)
   {
       processesdrentrolllist=RentRolllistdealMap.get(ct.Deal__c); 
         system.debug('check the size of the list'+processesdrentrolllist);
         if(processesdrentrolllist.size()>1)
         {
           dealMap.get(ct.deal__c).RR_SPRating__c='MT';
           dealMap.get(ct.deal__c).RR_LeaseExpiration__c='Varies'; 
           dealMap.get(ct.deal__c).RR_ExpenseRecoveries__c='Varies';
           dealMap.get(ct.deal__c).RR_Rent_Bump__c='Varies';
           system.debug('check the tenant..'+ dealMap.get(ct.deal__c).Tenant_s__c);
          
           
           if(dealMap.get(ct.deal__c).Tenancy__c=='Multi-Tenant')
               dealMap.get(ct.deal__c).RR_Reimb_Type__c='NN';
           else
            dealMap.get(ct.deal__c).RR_Reimb_Type__c='Varies';
          
            
                        
            for(Rent_Roll__c rr: processesdrentrolllist)
            {    
              //update the tenant//
              if(tenantname!=null)
                  tenantname=tenantname+','+' '+rr.Name_of_Tenant__c;
              else
                  tenantname=rr.Name_of_Tenant__c;
              
                if(rr.Remaining_Term_Years__c!=null)
                {
                    term= term + rr.Remaining_Term_Years__c;
                    sizeOflist = sizeOflist+1; 
                }
                else if(rr.Remaining_Term_Years__c==null )
                {
                    count=count+1;
                }
            }
               dealMap.get(ct.deal__c).Tenant_s__c=tenantname ;      
            if(term != 0)
            {
                term=(term/sizeOflist).setScale(2);
            }   
            if(count==processesdrentrolllist.size())
            {
                dealMap.get(ct.deal__c).RR_RemainingTerm__c=null;
            }
            else 
            {    
                dealMap.get(ct.deal__c).RR_RemainingTerm__c= String.valueof(term);
            }
            
        }
        if(processesdrentrolllist.size()==1)
        {
            dealMap.get(ct.deal__c).RR_SPRating__c = ct.S_P_Rating__c;
            dealMap.get(ct.deal__c).RR_LeaseExpiration__c= String.valueof(ct.Lease_Expiration__c);   
            dealMap.get(ct.deal__c).RR_RemainingTerm__c= String.valueof(ct.Remaining_Term_Years__c);
            dealMap.get(ct.deal__c).RR_ExpenseRecoveries__c= ct.Expense_Recoveries__c;
            dealMap.get(ct.deal__c).RR_Rent_Bump__c= ct.Rent_Bump__c;
            if(dealMap.get(ct.deal__c).Tenancy__c=='Multi-Tenant')
             dealMap.get(ct.deal__c).RR_Reimb_Type__c='NN';
            else
             dealMap.get(ct.deal__c).RR_Reimb_Type__c=ct.Reimb_Type__c;
             
            //first make it null and then update the value//
           dealMap.get(ct.deal__c).Tenant_s__c=null;
           dealMap.get(ct.deal__c).Tenant_s__c=ct.Name_of_Tenant__c; 
        }
      }
    }
    update dealMap.values();
    
  if(Trigger.isdelete)
   {
      system.debug('Welcome to the delete trigger');
     For(Rent_Roll__c ct: Trigger.old)
    {
      system.debug('check the ct..' +RentRolllistdealMap.get(ct.deal__c));
       if(RentRolllistdealMap.get(ct.deal__c)!=null)   
       {
         system.debug('Welcome to the delete trigger');
         processesdrentrolllist=RentRolllistdealMap.get(ct.Deal__c); 
         system.debug('check the size of the list'+processesdrentrolllist);
         if(processesdrentrolllist.size()>1)
         {
           dealMap.get(ct.deal__c).RR_SPRating__c='MT';
           dealMap.get(ct.deal__c).RR_LeaseExpiration__c='Varies'; 
           dealMap.get(ct.deal__c).RR_ExpenseRecoveries__c='Varies';
           dealMap.get(ct.deal__c).RR_Rent_Bump__c='Varies';
           system.debug('check the tenant..'+ dealMap.get(ct.deal__c).Tenant_s__c);
           if(dealMap.get(ct.deal__c).Tenancy__c=='Multi-Tenant')
               dealMap.get(ct.deal__c).RR_Reimb_Type__c='NN';
           else
            dealMap.get(ct.deal__c).RR_Reimb_Type__c='Varies';
                     
            for(Rent_Roll__c rr: processesdrentrolllist)
            {    
              //update the tenant//
              if(tenantname!=null)
                  tenantname=tenantname+','+' '+rr.Name_of_Tenant__c;
              else
                  tenantname=rr.Name_of_Tenant__c;
              
                if(rr.Remaining_Term_Years__c!=null)
                {
                    term= term + rr.Remaining_Term_Years__c;
                    sizeOflist = sizeOflist+1; 
                }
                else if(rr.Remaining_Term_Years__c==null )
                {
                    count=count+1;
                }
            }
             dealMap.get(ct.deal__c).Tenant_s__c=tenantname ;      
            if(term != 0)
            {
                term=(term/sizeOflist).setScale(2);
            }   
            if(count==processesdrentrolllist.size())
            {
                dealMap.get(ct.deal__c).RR_RemainingTerm__c=null;
            }
            else 
            {    
                dealMap.get(ct.deal__c).RR_RemainingTerm__c= String.valueof(term);
            }
            
        }
      else if(processesdrentrolllist.size()==1)
        {
            dealMap.get(ct.deal__c).RR_SPRating__c = ct.S_P_Rating__c;
            dealMap.get(ct.deal__c).RR_LeaseExpiration__c= String.valueof(ct.Lease_Expiration__c);   
            dealMap.get(ct.deal__c).RR_RemainingTerm__c= String.valueof(ct.Remaining_Term_Years__c);
            dealMap.get(ct.deal__c).RR_ExpenseRecoveries__c= ct.Expense_Recoveries__c;
            dealMap.get(ct.deal__c).RR_Rent_Bump__c= ct.Rent_Bump__c;
            if(dealMap.get(ct.deal__c).Tenancy__c=='Multi-Tenant')
             dealMap.get(ct.deal__c).RR_Reimb_Type__c='NN';
            else
             dealMap.get(ct.deal__c).RR_Reimb_Type__c=ct.Reimb_Type__c;
             
            //first make it null and then update the value//
           dealMap.get(ct.deal__c).Tenant_s__c=null;
           dealMap.get(ct.deal__c).Tenant_s__c=processesdrentrolllist[0].Name_of_Tenant__c; 
        }
       else 
        {
           //ct.deal__r.RR_SPRating__c = null;
           // dealMap.get(ct.deal__c).RR_LeaseExpiration__c= null;
            //dealMap.get(ct.deal__c).RR_RemainingTerm__c= null;
            //dealMap.get(ct.deal__c).RR_ExpenseRecoveries__c= null;
            //dealMap.get(ct.deal__c).RR_Rent_Bump__c= null;
            //dealMap.get(ct.deal__c).RR_Reimb_Type__c=null;
            //dealMap.get(ct.deal__c).RR_Reimb_Type__c=null;
            dealMap.get(ct.deal__c).Tenant_s__c=''; 
            //first make it null and then update the value//
         } 
      }
      
       
    }
    update dealMap.values();
   
    
    }
      
   
}