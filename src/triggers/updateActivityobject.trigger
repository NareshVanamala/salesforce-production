trigger updateActivityobject  on Case (after insert,after update) {
 Attachment a = new Attachment();
 Activity__c  av = new Activity__c();
  Activity__c act= new Activity__c();
Task ah = new Task();
 act.name='Priority is set to Medium'; 
For (Case c: Trigger.new)
{
   Case trigger_Old= System.Trigger.oldMap.get(c.Id);
if(trigger.isupdate){
//if(c.Response__c != Trigger.old[0].Response__c)
  if(c.Response__c != trigger_Old.Response__c)
 {
  act.name= 'Response changed';
  act.Case_Created_On__c= c.LastModifiedDate;
  act.Modified_By__c=UserInfo.getUserId();
  act.Description__c='Response changed from '+Trigger.old[0].Response__c+' to '+ c.Response__c;
  act.Type__c='Response';
 }
//if( c.Priority != Trigger.old[0].Priority)
  if(c.Response__c != trigger_Old.Priority)
 {
   act.name= 'Priority changed';
   act.Case_Created_On__c= c.LastModifiedDate;
   act.Modified_By__c=UserInfo.getUserId();
   act.Description__c='Priority changed from '+Trigger.old[0].Priority+' to '+ c.Priority;
   act.Type__c='Priority';
 } 
 //if(c.Note__c != Trigger.old[0].Note__c)
  if(c.Response__c != trigger_Old.Note__c)

 {
   act.name= 'Email Sent';
   act.Case_Created_On__c= c.LastModifiedDate;
   act.Modified_By__c=UserInfo.getUserId();
   act.Description__c=c.Email_Sent__c +' with message '+c.Note__c;
   act.Type__c='Email Sent';
 }
//if(c.Owner.name != Trigger.old[0].Owner.Name)
if(c.Owner.name != trigger_Old.Owner.Name)
{
   act.name= 'Owner Changed';
   act.Case_Created_On__c= c.LastModifiedDate;
   act.Modified_By__c=Userinfo.getUserid();
   act.Description__c='Owner changed from '+Trigger.old[0].Owner.Name+' to '+ c.Owner.Name;
   act.Type__c='Owner';

}

//if(c.Status!= Trigger.old[0].Status)

if(c.Status!= trigger_Old.Status)
{
   act.name= 'Status Changed';
   act.Case_Created_On__c= c.LastModifiedDate;
   act.Modified_By__c=UserInfo.getUserId();
   act.Description__c='Status changed from '+Trigger.old[0].Status+' to '+ c.Status;
   act.Type__c='Status';

}

}

else if(Trigger.isinsert){


  act.name= 'Response received';
  act.Case_Created_On__c= c.LastModifiedDate;
  act.Modified_By__c=UserInfo.getUserId();
  act.Description__c='Response received: '+ c.Response__c;
  act.Type__c='Response';
   

   act.name= 'Priority set';
   act.Case_Created_On__c= c.LastModifiedDate;
   act.Modified_By__c=UserInfo.getUserId();
   act.Description__c='Priority set to '+ c.Priority;
   act.Type__c='Priority';
   
   act.name= 'Email Sent';
   act.Case_Created_On__c= c.LastModifiedDate;
   act.Modified_By__c=UserInfo.getUserId();
   act.Description__c=c.Email_Sent__c;
   act.Type__c='Email Sent';
   
   act.name= 'Analytics Request Created';
   act.Case_Created_On__c= c.LastModifiedDate;
   act.Modified_By__c=UserInfo.getUserId();
   act.Description__c='Case Created';
   act.Type__c='Owner';
   
   
} 
/*if(act.name == '')
{  
 system.debug('+++++act name'+act.name);
act.name='Priority is set to Medium'; 
}*/
 act.Case__c=c.id;
 insert act;

 }
}