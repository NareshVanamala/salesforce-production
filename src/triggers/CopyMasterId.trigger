trigger CopyMasterId on Campaign (before insert,before update) {
    if(!trigger.isdelete){
        if(trigger.isInsert){
            list<string> lstParCamId = new list<string>();
            for(Campaign objCamp : trigger.new){
                if(objCamp.parentid!=null)
                    lstParCamId.add(objCamp.parentid);                    
                else{
                    objCamp.Master_Camp_Id__c = objCamp.id;                        
                }    
            }
            if(lstParCamId.size()>0){
                list<Campaign> lstCamp = [select id,name,Master_Camp_Id__c,parentid,parent.Master_Camp_Id__c from Campaign where id in : lstParCamId];
                for(Campaign objCamp : trigger.new){            
                    for(Campaign objCamp1 : lstCamp){
                         if(objCamp.parentid == objCamp1.id){                         
                            if(objCamp1.Master_Camp_Id__c!=null) 
                                objCamp.Master_Camp_Id__c = objCamp1.Master_Camp_Id__c;                         
                            else
                                objCamp.Master_Camp_Id__c = objCamp1.parentid;
                            
                         }  
                    }
                }
            }
        
        }else{
            list<Campaign> lstCamp = [select id,name,Master_Camp_Id__c,parentid,parent.Master_Camp_Id__c from Campaign where id in : trigger.new];
            
            for(Campaign objCamp : trigger.new){            
                for(Campaign objCamp1 : lstCamp){
                     if(objCamp.id == objCamp1.id)
                     if(objCamp.parentid !=null){
                        objCamp.Master_Camp_Id__c = objCamp1.parent.Master_Camp_Id__c ;
                     }else{
                        objCamp.Master_Camp_Id__c =  objCamp1.id; 
                        system.debug('----'+objCamp.Master_Camp_Id__c);
                     }
                }     
            }
        }                
    }
    else{
     for(Campaign objCamp : trigger.new){            
       objCamp.Master_Camp_Id__c = ''; 
     }
    }
}