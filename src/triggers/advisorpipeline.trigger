trigger advisorpipeline on Task(after insert,after update)
{
    for(Task tl : trigger.new)
    {   
      /*  if(tl.subject=='Study Period End Date' )
        {
          List< REthinkAP__Deal__c >deltest=[select id, Report_Hard_Date__c from REthinkAP__Deal__c where id= :tl.WhatId limit 1];            
            if (deltest.size()>0)
          {
             //system.debug('check the deal whatid...'+deltest); 
            if(tl.REthinkAP__Date_Completed__c!=null)
                {
                    date d= tl.REthinkAP__Date_Completed__c;
                    Integer month1= d.month();
                    Integer day1= d.day();
                    Integer year1= d.year();
                    deltest[0].Report_Hard_Date__c =string.valueOf(month1)+'/'+String.valueOf(day1)+'/'+String.valueOf(year1);    
                                    
                 }
            else if(tl.ActivityDate!=null)
                {
                    date d= tl.ActivityDate;
                    Integer month1= d.month();
                    Integer day1= d.day();
                    Integer year1= d.year();
                    deltest[0].Report_Hard_Date__c =string.valueOf(month1)+'/'+String.valueOf(day1)+'/'+String.valueOf(year1);
                 }
             
           }  
             update deltest;   
    
         }  */   
        if(tl.Advisor_Pipeline__c!=null)
        {
            Contact ct=[select id, name ,ColorRBG__c,Advisor_Pipeline__c from Contact where id= :tl.whoid];
            list<task>tlist=new list<task>();
            list<task>updatedlist= new list<task>();
            tlist=[select id,Advisor_Pipeline__c, whoid from task where whoid=:ct.id];
            system.debug('Check the list size....'+tlist.size()); 
            //system.debug('Check the contact....'+ct);
            if(tl.Advisor_Pipeline__c=='Red')
            {
                ct.ColorRBG__c='Red-Yellow-Green-NQ';  
                ct.Advisor_Pipeline__c='Red';
            }   
            else if(tl.Advisor_Pipeline__c=='Green')
            {
                ct.ColorRBG__c='Green-Red-Yellow-NQ'; 
                ct.Advisor_Pipeline__c='Green'; 
            }
            else if(tl.Advisor_Pipeline__c=='Yellow')
            {
                ct.ColorRBG__c='Yellow-Red-Green-NQ';  
                ct.Advisor_Pipeline__c='Yellow';           
            }       
            else if(tl.Advisor_Pipeline__c=='NQ')
            {
                ct.ColorRBG__c='NQ-Red-Yellow-Green';          
                ct.Advisor_Pipeline__c='NQ';
            }
            update ct;
        }
        
        
       
   }
   
   
                
  }