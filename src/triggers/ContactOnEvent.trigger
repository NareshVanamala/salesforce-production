trigger ContactOnEvent on Event (before update,after insert,after update) 
{ 

    set<id> setContactIds = new set<id>();
    set<id> eid = new set<id>();
    list<contact> conlist = new list<contact>();
    list<contact> conlist123 = new list<contact>();
    list<id> cids= new list<id>();
    Datetime dt;
    list<contact> eventconlist = new list<contact>();
    Map<id,Event> MapEvent = new Map<id,Event>();
    Map<Event,String> Mapsubject = new Map<Event,String>();
    list<task>insertTasklist= new list<Task>();
    list<Task>updatedTasklist= new list<Task>();
    list<Contact>clist= new list<contact>();
    string source='';
    
    for(Event objEvent:trigger.new)
    {
        if((objEvent.subject !='COE')&&((objEvent.subject=='Ext Appt Event') || (objEvent.subject=='Phone Appointment') || (objEvent.subject=='Group Appointment')))
        {
        
            setContactIds.add(objEvent.whoid);
            MapEvent.put(objEvent.whoid,objEvent);
            eid.add(objEvent.id);
            source=objevent.Source__c;
            dt = objEvent.StartDateTime;
         }
       
        if((objEvent.subject=='Ext Appt Event') || (objEvent.subject=='Phone Appointment') || (objEvent.subject=='Group Appointment'))
        {
            Mapsubject.put(objEvent,objEvent.subject);
        }
    }  
    
    List<task> tlist=[select id, whoid from task where whoid in:setContactIds and status!='completed' and (subject='External Appointment' or subject='Ext Appt Event' or subject='Phone Appointment' or subject='Group Appointment')  limit 10000];   
    List<EventRelation> whoRelations1 = [SELECT Id,relationid, Relation.Name FROM EventRelation WHERE EventId in:eid limit 40000];
    eventconlist=[select Location__c,Next_Planned_External_Appointment_c__c,Id,Name,Next_Planned_External_Appointment__c,Next_External_Appointment__c,Priority__c from contact where id in:setContactIds limit 40000];
    
    for(EventRelation t:whoRelations1)
    { 
        cids.add(t.relationid);
    }
    if(cids.size()>0)
    {
        clist=[select id,ownerid from contact where id in:cids]; 
    } 
    
    if(trigger.isinsert)
    {
        if(Mapsubject.size()>0 && source!='CockpitLite')
        {
            for(Contact c: clist)
            {
                c.Next_External_Appointment__c=dt; 
                c.Next_Planned_External_Appointment__c=null;
                conlist123.add(c); 
            }
            
            for(Contact c: eventconlist)
            { 
                c.Next_External_Appointment__c=MapEvent.get(c.id).StartDateTime;
                c.Location__c =MapEvent.get(c.id).Location;
                c.Next_Planned_External_Appointment__c=null;
                conlist.add(c);
            }
            for(Contact c: eventconlist)
            {
                Task t= new task();
                t.whoid=c.id;
                t.ownerid=MapEvent.get(c.id).ownerid;
                if(MapEvent.get(c.id).subject!='Ext Appt Event')
                {
                    t.subject=MapEvent.get(c.id).subject;
                }
                else
                {
                    t.subject='External Appointment';
                }
                t.ActivityDate=MapEvent.get(c.id).EndDateTime.Date();
                t.status='Not Started';
                insertTasklist.add(t);
            }
          //  insert insertTasklist; 
         
        } 
    }
    
    if((trigger.isupdate))
    { 
        if(Mapsubject.size()>0 && source!='CockpitLite')
        { 
            for(Contact c: eventconlist)
            { 
                c.Next_External_Appointment__c=MapEvent.get(c.id).StartDateTime;
                c.Location__c =MapEvent.get(c.id).Location;
                c.Next_Planned_External_Appointment__c=null;
                conlist.add(c);
            }
            
            for(Contact c: clist)
            {
                c.Next_External_Appointment__c=dt; 
                c.Next_Planned_External_Appointment__c=null;
                conlist123.add(c); 
            }
            
            for(Event e : trigger.new)
            {
                if(tlist.size()>0)
                {
                    for(Task t: tlist)
                    {
                        if(e.subject!='Ext Appt Event')
                        {
                            t.subject=e.subject;
                        }
                        else
                        {
                            t.subject='External Appointment';
                        }
                        t.ActivityDate=e.EndDateTime.Date();
                        t.ownerid=e.ownerid;
                        updatedTasklist.add(t);
                    }
                    
                    
                    if(tlist.size()==0)
                    {
                        for(Contact c: eventconlist)
                        {
                            Task t= new task();
                            t.whoid=c.id;
                            t.ownerid=MapEvent.get(c.id).ownerid;
                            if(MapEvent.get(c.id).subject!='Ext Appt Event')
                            {
                                t.subject=MapEvent.get(c.id).subject;
                            }
                            else
                            {
                                t.subject='External Appointment';
                            }
                            t.ActivityDate=MapEvent.get(c.id).EndDateTime.Date();
                            t.status='Not Started';
                            insertTasklist.add(t);
                        }
                       // insert insertTasklist; 
                    } 
                }
            } 
        } 
    }
    
    if(trigger.isupdate && trigger.isbefore)
    {
        for(Event e:trigger.new)
        {
            string a=e.Description;
            if(a!=null)
            {
                if(!a.contains('CRMID:'))
                {
                    e.Description = e.Description + '\n' + 'CRMID:'+ e.id;
                }
            } 
        }
     } 
 
    
    if(conlist.size()>0)
    update conlist;
    
    if(conlist123.size()>0)
    update conlist123;
    
    //if(updatedTasklist.size()>0)
    //update updatedTasklist;
    
}