trigger LC_RS_Months_YearsValidation on LC_RentSchedule__c(before insert,before Update,after insert,after update)
{
    list<LC_RentSchedule__c> LC_Rentlist =new list<LC_RentSchedule__c>();
    list<LC_RentSchedule__c> LC_RentlistUpdate =new list<LC_RentSchedule__c>();
    list<LC_RentSchedule__c> afterInsList=new list<LC_RentSchedule__c>();
    list<LC_RentSchedule__c> afterUpdList=new list<LC_RentSchedule__c>();
    list<LC_RentSchedule__c> updateafterIns1=new list<LC_RentSchedule__c>();
    list<LC_RentSchedule__c> updateafterUpd=new list<LC_RentSchedule__c>();
    list<LC_RentSchedule__c> updateafterIns=new list<LC_RentSchedule__c>();
    list<LC_RentSchedule__c> autoUpdateList=new list<LC_RentSchedule__c>();
    
    LC_LeaseOpprtunity__c lcp= new LC_LeaseOpprtunity__c();

    lcp=[select id, name,Base_Rent__c,Suite_Sqft__c,Annual_Base_Rent_at_expiration__c,Annual_Base_Rent_PSF__c,Base_Rent_PSF__c from LC_LeaseOpprtunity__c where id=:Trigger.new[0].Lease_Opportunity__c];
    string myid= Trigger.new[0].id;
    
    LC_Rentlist=[select id, From_In_Months__c,To_In_Months__c,Increase_in_rent_n__c,Lease_Opportunity__c,Increase_in_rent__c,Average_Base_Rent_over_Initial_Term__c,
    Base_Rent_Additional_Info__c,Base_Rent_PSF__c,Income_Catgeory__c,Monthly_PSF__c,Months_Years__c,sqft__c,Annual_PSF__c  from LC_RentSchedule__c where Lease_Opportunity__c=:lcp.id];
    system.debug('----------LC_Rentlist---'+LC_Rentlist); 
    
    afterInsList=[select id, From_In_Months__c,To_In_Months__c,Increase_in_rent_n__c,Lease_Opportunity__c,Increase_in_rent__c,Average_Base_Rent_over_Initial_Term__c,
    Base_Rent_Additional_Info__c,Base_Rent_PSF__c,Income_Catgeory__c,Monthly_PSF__c,Months_Years__c,sqft__c,Annual_PSF__c  from LC_RentSchedule__c where Lease_Opportunity__c=:lcp.id and id!=:myid order by createddate desc limit 1];
    system.debug('----------afterInsList---'+afterInsList);  

        /*afterUpdList=[select id, From_In_Months__c,To_In_Months__c,Increase_in_rent_n__c,Lease_Opportunity__c,Increase_in_rent__c,Average_Base_Rent_over_Initial_Term__c,
                        Base_Rent_Additional_Info__c,Base_Rent_PSF__c,Income_Catgeory__c,Monthly_PSF__c,Months_Years__c,sqft__c,Annual_PSF__c  from LC_RentSchedule__c where Lease_Opportunity__c=:lcp.id ];
                        system.debug('----------afterUpdList---'+afterUpdList);  */
                        
                        
    if(trigger.isinsert) 
    { 
        if(trigger.isbefore) 
        { 
            for(LC_RentSchedule__c sNew:Trigger.new)
            { 
                if(LC_Rentlist.size()==0)
                {
                    sNew.sqft__c  = lcp.Suite_Sqft__c;
                    If(lcp.Base_Rent__c==Null)
                    {
                     if(sNew.Annual_PSF__c!=Null)
                     {
                      sNew.Annual_PSF__c =sNew.Annual_PSF__c;
                       sNew.Monthly_PSF__c =(sNew.Annual_PSF__c)/12;
                        sNew.Base_Rent_PSF__c= (sNew.Annual_PSF__c)/sNew.sqft__c;
                        sNew.Increase_in_rent_n__c = sNew.Annual_PSF__c-(sNew.Annual_PSF__c);
                        sNew.Increase_in_rent__c= sNew.Increase_in_rent_n__c;
                      }
                    }
                    If(lcp.Base_Rent__c!=Null)
                    {
                        sNew.Annual_PSF__c =lcp.Base_Rent__c;
                        sNew.Monthly_PSF__c =(sNew.Annual_PSF__c)/12;
                        sNew.Base_Rent_PSF__c= lcp.Base_Rent_PSF__c;
                        sNew.Increase_in_rent_n__c = sNew.Annual_PSF__c-(sNew.Annual_PSF__c);
                        sNew.Increase_in_rent__c= sNew.Increase_in_rent_n__c;
                    }
                   
                    if(snew.Annual_PSF__c==0)
                    { 
                        sNew.Increase_in_rent_n__c = sNew.Annual_PSF__c-(sNew.Annual_PSF__c);
                        sNew.Increase_in_rent__c= sNew.Increase_in_rent_n__c;
                    }
                }
            }
            if(LC_Rentlist.size()>0)
            {
                for(LC_RentSchedule__c sNew:Trigger.new)
                {  
                    for(LC_RentSchedule__c LCP1:LC_Rentlist)
                    {
                        if((sNew.From_In_Months__c == LCP1.From_In_Months__c)&&(sNew.To_In_Months__c == LCP1.To_In_Months__c)) 
                        sNew.addError('Duplicate From and To values.');   
                    }
                    sNew.sqft__c  = lcp.Suite_Sqft__c;   
                }
            }
            
          if(afterInsList.size()>0)
            {
                for(LC_RentSchedule__c LCP1:afterInsList)
                {  
                    for(LC_RentSchedule__c sNew:Trigger.new)            
                    {
                        if(LCP1.Annual_PSF__c!=null && sNew.Annual_PSF__c!=null)
                        {
                            sNew.Monthly_PSF__c =(sNew.Annual_PSF__c)/12;
                            sNew.Base_Rent_PSF__c = (sNew.Annual_PSF__c)/sNew.sqft__c;
                            
                            if(LCP1.Annual_PSF__c==0)
                            {
                                sNew.Increase_in_rent_n__c = sNew.Annual_PSF__c-(sNew.Annual_PSF__c);
                                sNew.Increase_in_rent__c=sNew.Increase_in_rent_n__c;
                            }
                            else
                            { 
                                sNew.Increase_in_rent_n__c=   sNew.Annual_PSF__c - (LCP1.Annual_PSF__c); 
                                sNew.Increase_in_rent__c= (sNew.Increase_in_rent_n__c)/(LCP1.Annual_PSF__c)*100;
                                //sNew.Annual_PSF__c =lcp.Base_Rent__c;
                            }
                            //sNew.Base_Rent_PSF__c=null;
                            }
                        updateafterIns.add(LCP1);
                        }
                    }
                update(updateafterIns);
            }       
        } 
    }
    
   
        
     if(trigger.isUpdate)
   { 
   
  
  
    If(trigger.isBefore)
    {
    if(LC_Rentlist.size()==1)
                {
        for(LC_RentSchedule__c sNew:Trigger.new)
            { 
              
                    sNew.sqft__c  = lcp.Suite_Sqft__c;
                    
                    If(lcp.Base_Rent__c==Null)
                    {
                        sNew.Annual_PSF__c =sNew.Annual_PSF__c;
                        sNew.Monthly_PSF__c =(sNew.Annual_PSF__c)/12;
                        sNew.Increase_in_rent_n__c = sNew.Annual_PSF__c-(sNew.Annual_PSF__c);
                         sNew.Base_Rent_PSF__c= (sNew.Annual_PSF__c)/sNew.sqft__c;
                         sNew.Increase_in_rent__c= sNew.Increase_in_rent_n__c;
                     }
                    
                   if(lcp.Base_Rent__c!=Null)
                    {
                        sNew.Annual_PSF__c =lcp.Base_Rent__c;
                        sNew.Monthly_PSF__c =(sNew.Annual_PSF__c)/12;
                        sNew.Base_Rent_PSF__c= lcp.Base_Rent_PSF__c;
                        sNew.Increase_in_rent_n__c = sNew.Annual_PSF__c-(sNew.Annual_PSF__c);
                        sNew.Increase_in_rent__c= sNew.Increase_in_rent_n__c;
                    }
                   
                  if(snew.Annual_PSF__c==0)
                    {
                        sNew.Monthly_PSF__c =sNew.Annual_PSF__c;
                        sNew.Base_Rent_PSF__c=sNew.Annual_PSF__c;
                        sNew.Increase_in_rent_n__c = sNew.Annual_PSF__c-(sNew.Annual_PSF__c);
                        sNew.Increase_in_rent__c= sNew.Increase_in_rent_n__c;
                    }
               
                }
            }

     
     if(LC_Rentlist.size()>1)
     {
     
      LC_RentlistUpdate=[select id, From_In_Months__c,To_In_Months__c,Increase_in_rent_n__c,Lease_Opportunity__c,Increase_in_rent__c,Average_Base_Rent_over_Initial_Term__c,
                            Base_Rent_Additional_Info__c,Base_Rent_PSF__c,Income_Catgeory__c,Monthly_PSF__c,Months_Years__c,sqft__c,Annual_PSF__c  from LC_RentSchedule__c where Lease_Opportunity__c=:lcp.id and id!=:myid and createddate>:Trigger.new[0].createddate  order by createddate];
       
       
       If (LC_RentlistUpdate.size()==1)
       {  
       for(LC_RentSchedule__c sNew:Trigger.new)
       {  
         
          for(LC_RentSchedule__c LCP1:LC_RentlistUpdate)
          {
              
              if((sNew.From_In_Months__c == LCP1.From_In_Months__c)&&(sNew.To_In_Months__c == LCP1.To_In_Months__c)) 
              sNew.addError('Duplicate From and To values.'); 
             
              sNew.Monthly_PSF__c =(sNew.Annual_PSF__c)/12;
              sNew.Base_Rent_PSF__c = (sNew.Annual_PSF__c)/sNew.sqft__c;
              sNew.Increase_in_rent_n__c=sNew.Annual_PSF__c-(LCP1.Annual_PSF__c) ; 
              if(LCP1.Annual_PSF__c!=0)
              sNew.Increase_in_rent__c= (sNew.Increase_in_rent_n__c)/(LCP1.Annual_PSF__c)*100;    
     
          }   
        
       } 
       }
       
        If (LC_RentlistUpdate.size()>1)
       {  
       
       
       
       
       }
        
    }
    }
    
     /*If(trigger.isAfter)
     {
    
       afterUpdList=[select id, From_In_Months__c,To_In_Months__c,Increase_in_rent_n__c,Lease_Opportunity__c,Increase_in_rent__c,Average_Base_Rent_over_Initial_Term__c,
                        Base_Rent_Additional_Info__c,Base_Rent_PSF__c,Income_Catgeory__c,Monthly_PSF__c,Months_Years__c,sqft__c,Annual_PSF__c  from LC_RentSchedule__c where Lease_Opportunity__c=:lcp.id and id=:myid  order by  LastModifiedDate desc ];
                        system.debug('----------afterUpdList---'+afterUpdList);
      autoUpdateList=[select id, From_In_Months__c,To_In_Months__c,Increase_in_rent_n__c,Lease_Opportunity__c,Increase_in_rent__c,Average_Base_Rent_over_Initial_Term__c,
                        Base_Rent_Additional_Info__c,Base_Rent_PSF__c,Income_Catgeory__c,Monthly_PSF__c,Months_Years__c,sqft__c,Annual_PSF__c  from LC_RentSchedule__c where Lease_Opportunity__c=:lcp.id and id!=:myid  order by  LastModifiedDate desc ];
                        system.debug('----------autoUpdateList---'+autoUpdateList);
             for(LC_RentSchedule__c LCP2:afterUpdList)
          {             
           for(LC_RentSchedule__c LCP3:autoUpdateList)
          {
              LCP3.Increase_in_rent_n__c=LCP3.Annual_PSF__c-(LCP2.Annual_PSF__c) ; 
              if(LCP2.Annual_PSF__c!=0)
              LCP3.Increase_in_rent__c= (LCP3.Increase_in_rent_n__c)/(LCP2.Annual_PSF__c)*100;    
            }       
     }
     
     
     }*/
  
    }  
}