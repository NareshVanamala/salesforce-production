trigger leaseChargesRequest on Lease_Charges_Request__c (before insert,before update) {
//List<Lease_Charges_Request__c> leasechargerequestList = new List<Lease_Charges_Request__c>();
set<id> leaseids =new set<id>();
for (Lease_Charges_Request__c leasechargerequest : Trigger.New)
{
    leaseids.add(leasechargerequest.Lease__c);
}
for (Lease__c leaseobj : [Select MRI_PROPERTY__r.Property_Manager__c,MRI_PROPERTY__c,MRI_PROPERTY__r.Asset_Manager__c,MRI_PROPERTY__r.SVP__c,MRI_PROPERTY__r.Director_Vice_President__c from Lease__c where MRI_PROPERTY__c != null and id in : leaseids])
{
    for(Lease_Charges_Request__c leasechargeobj : trigger.new){
        leasechargeobj.Property_Manager__c = leaseobj.MRI_PROPERTY__r.Property_Manager__c;
        leasechargeobj.Asset_Manager__c = leaseobj.MRI_PROPERTY__r.Asset_Manager__c;
        leasechargeobj.SVP__c = leaseobj.MRI_PROPERTY__r.SVP__c;
        leasechargeobj.Director_Vice_President__c = leaseobj.MRI_PROPERTY__r.Director_Vice_President__c;
        leasechargeobj.MRI_Property__c= leaseobj.MRI_PROPERTY__c;
        
       
    }
}

}