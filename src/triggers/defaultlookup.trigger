trigger defaultlookup  on Event_Automation_Request__c (before insert, before update)
{

    public List<Event_Automation_Request__c> eaList = new List<Event_Automation_Request__c>();
    public List<Event_Automation_Request__c>eaList1 = new List<Event_Automation_Request__c>();
    public List<Event_Automation_Request__c>eaList2 = new List<Event_Automation_Request__c>();
    public List<Event_Automation_Request__c>BDlist=new List<Event_Automation_Request__c>();
     public List<Event_Automation_Request__c>Lastrecordlists=new List<Event_Automation_Request__c>();
    public String Eventtype=null; 
    public String fullname1=null;
    public String fullname2=null;
    public String Name=null;
    public String startnumber=null;
    public integer strtnum=0;
    public string EventyearIT=null;
    public set<id> setids = new set<id>();
    public set<id> setids1=new set<id>();
    public date d;
    public date d1;
    public integer d2;
    public date d3;
    public date d4;
    public date d5;
    public date d6;
    public integer d7;
    public String BDname1=null;
    public String BDname2=null;
    public String BDSName=null;
    public String BDSstartnumber=null;
    public integer BDSstrtnum=0;
    public integer BDSstrtnu=0;
     public String fullBDSName=null;
    
    
    Integer count=000;
    integer bdscount=000;
    String year=string.valueOf(system.today().year()).right(2);
    integer yearthis=integer.valueOf(year);
    integer thisyear=system.today().year();
    for(Event_Automation_Request__c ear: Trigger.new)
    {
        if(ear.Key_Account_Manager_Name_Requestor__c==null)
        { 
            ear.Key_Account_Manager_Name_Requestor__c=UserInfo.getUserId(); 
        }
        if(ear.Broker_Dealer_Name__c!=null)
        {
            setids.add(ear.Broker_Dealer_Name__c);
        }
        setids1.add(ear.id);
        Eventtype=ear.Event_Type__c;
        EventyearIT = ear.event_year_for_it__c;
        d=ear.start_date__c;
    } 
   // if(Trigger.isUpdate)
   //Account a = [select id,name,billingstreet,billingcity,billingpostalcode,billingstate,billingcountry from account where id in:setids limit 1];
     d1=d;
     d2 =d1.year();
     system.debug(d2);
     d3 = date.newInstance(d2,12,31);
     system.debug(d3);
     d4 = date.newInstance(d2,01,01);
     system.debug(d4);
     system.debug('>>>>>>>>>>>>>>>Eventtype>>>>>>>>>>>>>>>>>>'+Eventtype);
     system.debug('>>>>>>>>>>>>>>>Date>>>>>>>>>>>>>>>>>>'+d1);
     system.debug('>>>>>>>>>>>>>>>Date>>>>>>>>>>>>>>>>>>'+d2);
     system.debug('>>>>>>>>>>>>>>>Date>>>>>>>>>>>>>>>>>>'+d3);
     system.debug('>>>>>>>>>>>>>>>Date>>>>>>>>>>>>>>>>>>'+d4);
     eaList=[select id, name,Event_Type__c,Start_Date__c from Event_Automation_Request__c where Event_Type__c=:Eventtype and start_date__c<=:d3 and start_date__c>=:d4 order by name desc limit 1 ];
     ealist1= [select id,name,event_type__c from event_automation_request__c Where event_type__c=:Eventtype and start_date__c<=:d3 and start_date__c>=:d4 and ID NOT IN :setids1 order by name DESC limit 1]; 
     ealist2= [select id,name,event_type__c from event_automation_request__c Where event_type__c=:Eventtype and start_date__c<=:d3 and start_date__c>=:d4 and Testing_Field__c='BDS' and ID NOT IN :setids1 order by name DESC limit 1]; 
     BDlist=[select id,name,event_type__c from event_automation_request__c Where event_type__c=:Eventtype and start_date__c<=:d3 and start_date__c>=:d4 and Testing_Field__c='BDS'  order by name DESC limit 1]; 
     //Vereitlist=[select id, name,event_type__c from event_automation_request__c where event_type__c=:Eventtype and Testing_Field__c='Vereit'and   ]
     //ealist2= [select id,name,event_type__c from event_automation_request__c Where event_type__c=:Eventtype and start_date__c<=:d3 and start_date__c>=:d4 order by name DESC limit 1];
    //Integer year=Integer.valueOf(string.valueOf(eaList[0].Start_Date__c.year()).right(2));
     if(eaList.size()>0)
    {
       fullname1=eaList[0].name;
       system.debug('>>>>>>>>>>>>>>>fullname1>>>>>>>>>>>>>>>>>>'+fullname1);
       Name=fullname1;
       fullname2=fullname1.right(3);
       startnumber=fullname1.right(5);
       if(startnumber!='Snull')
       strtnum=integer.valueOf(startnumber);
       system.debug('>>>>>>>>>>>>>>>startnumber>>>>>>>>>>>>>>>>>>'+startnumber);
       system.debug('>>>>>>>>>>>>>>>fullname2>>>>>>>>>>>>>>>>>>'+fullname2);
       if(fullname2!='ull')
       Name=Name.remove(fullname2);
       system.debug('>>>>>>>>>>>>>>>Name>>>>>>>>>>>>>>>>>>'+Name);
       if(fullname2!='ull')
       count=Integer.valueOf(fullname2);
       system.debug('check the count'+count);
     } 
     if(BDlist.size()>0)
     {
     
     fullBDSName=BDlist[0].name;
     BDSName=fullBDSName;
     BDname1=fullBDSName.right(3);
     BDSstartnumber=fullBDSName.right(5);
     if(BDSstartnumber!='Snull')
     BDSstrtnu=integer.valueOf(BDSstartnumber);
      if(BDname1!='ull')
     BDSName=BDSName.remove(BDname1);
     if(BDname1!='ull')
     bdscount=Integer.valueOf(BDname1);
     
      
     }
//start the record from 13400// 
        system.debug('Check the count'+count); 
        for(Event_Automation_Request__c ear: Trigger.new)
        { 
            if(Trigger.isInsert)
            {
                if((startnumber!='13400')&&(strtnum<13400))
                {
                   startnumber='13400';
                   if(Eventtype=='Conference-Broker Dealer Platform')
                        ear.name= 'BDE'+ startnumber;
                   if(Eventtype=='Cole Hosted (Internal)')
                       ear.name= 'COL'+startnumber; 
                   if(Eventtype=='Conference-Industry/Real Estate')
                        ear.name= 'IND'+startnumber; 
                   if(Eventtype=='Broker Dealer-Miscellaneous')
                        ear.name= 'MIS'+startnumber; 
                   if(Eventtype=='BD Marketing Support')
                   {
                        ear.name= 'BDS'+BDSstartnumber; 
                   
                   }
                   if(Eventtype=='Broker Dealer Due Diligence')
                       ear.name= 'BDD'+startnumber; 
                    if(Eventtype=='Cole Hosted Rep Training CE')
                        ear.name= 'REP'+startnumber; 
                    if(Eventtype=='Cole Hosted Rep Training Non CE')
                        ear.name= 'SAL'+startnumber; 
                    if(Eventtype=='Employee Event')
                       ear.name= 'COL'+year+'400'; 
                    if(Eventtype=='Employee Management Meeting')
                        ear.name= 'COL'+year+'400'; 
                     if(Eventtype=='VEREIT Hosted Event')
                      ear.name='VER'+year+'400';     
                  }
                  else if((startnumber=='13400')||(strtnum>=13400))
                  {
                    system.debug('>>>>>>>>>>>>>>>else if startnumber==13400 ||strtnum>=13400>>>>>>>>>>>>>>>>>>'+count);
                    count=count+1;
                    if(Eventtype=='Conference-Broker Dealer Platform')
                        ear.name= 'BDE'+year+ string.valueOf(count);
                    if(Eventtype=='Cole Hosted (Internal)')
                        ear.name= 'COL'+year+string.valueOf(count); 
                    if(Eventtype=='Conference-Industry/Real Estate')
                        ear.name= 'IND'+year+ string.valueOf(count); 
                    if(Eventtype=='Broker Dealer-Miscellaneous')
                        ear.name= 'MIS'+year+ string.valueOf(count); 
                    if(Eventtype=='BD Marketing Support')
                    {
                        bdscount=bdscount+1;
                        ear.name= 'BDS'+year+ string.valueOf(bdscount); 
                    
                                       
                     }
                    if(Eventtype=='Broker Dealer Due Diligence')
                        ear.name= 'BDD'+year+ string.valueOf(count); 
                    if(Eventtype=='Cole Hosted Rep Training CE')
                        ear.name= 'REP'+year+ string.valueOf(count); 
                    if(Eventtype=='Cole Hosted Rep Training Non CE')
                        ear.name= 'SAL'+year+ string.valueOf(count); 
                    if(Eventtype=='Employee Event')
                        ear.name= 'COL'+year+ string.valueOf(count); 
                    if(Eventtype=='Employee Management Meeting')
                        ear.name= 'COL'+year+ string.valueOf(count); 
                    if(Eventtype=='VEREIT Hosted Event')
                      ear.name='VER'+year+string.valueOf(count); 
                }

            } 
          if(Trigger.isUpdate)

           {
            if(setids.size()>0)
            {
            Account a = [select id,name,billingstreet,billingcity,billingpostalcode,billingstate,billingcountry from account where id in:setids limit 1];
           for(Event_Automation_Request__c oear:trigger.old)
           {
            if(((ear.Broker_Dealer_Name__c != oear.Broker_Dealer_Name__c)))
            {
               //ear.Mailing_Address__c = a.billingstreet+','+a.billingcity+','+a.billingstate+','+'\n'+a.billingpostalcode+','+'\n'+a.billingcountry;
               //ear.Mailing_Address__c = a.billingstreet+','+a.billingcity+','+'\n'+a.billingstate+','+a.billingpostalcode+','+'\n'+a.billingcountry;     
               ear.Mailing_Address__c = a.billingstreet+'\n'+a.billingcity+','+' '+a.billingstate+' '+a.billingpostalcode+'\n'+a.billingcountry;   
            }
            
            if(((ear.Mailing_Address__c == oear.Mailing_Address__c) || (ear.Mailing_Address__c==''))&&((ear.Broker_Dealer_Name__c == oear.Broker_Dealer_Name__c) || (ear.Broker_Dealer_Name__c=='')))
              {

              if(Eventtype=='BD Marketing Support')
               ear.Mailing_Address__c = a.billingstreet+'\n'+a.billingcity+','+' '+a.billingstate+' '+a.billingpostalcode+'\n'+a.billingcountry; 
       
              } 
            } 
           } 
          }         

           if(Trigger.isUpdate)
           {
           for(Event_Automation_Request__c oear:trigger.old)
           {
                d5=oear.start_date__c;
                d6=d5;
                d7 =d6.year();
           }        
           system.debug('old date--------------------'+d7);
           if(d2!=d7)
           {
               Integer Startdateyear=Integer.valueOf(string.valueOf(ear.Start_Date__c.year()).right(2));
               string startyear11= string.valueOF(Startdateyear); 
          
               Integer Startdateyear1=Integer.valueOf(string.valueOf(system.today().year()).right(2));

               string startyear12= string.valueOF(Startdateyear1); 
               
               system.debug('check the start date year...'+Startdateyear);
                                         
               //if(yearthis!=Startdateyear)

              //  {

                    if(eaList1.size()==0)

                    {

                        if(Eventtype=='Conference-Broker Dealer Platform')

                            ear.name= 'BDE'+ startyear11+string.valueOf(100); 

                        if(Eventtype=='Cole Hosted (Internal)')

                            ear.name= 'COL'+startyear11+string.valueOf(100); 

                        if(Eventtype=='Conference-Industry/Real Estate')

                            ear.name= 'IND'+startyear11+string.valueOf(100);

                        if(Eventtype=='Broker Dealer-Miscellaneous')

                            ear.name= 'MIS'+startyear11+string.valueOf(100);

                        if(Eventtype=='BD Marketing Support')

                            ear.name= 'BDS'+startyear11+string.valueOf(100);

                        if(Eventtype=='Broker Dealer Due Diligence')

                            ear.name= 'BDD'+startyear11+string.valueOf(100);

                        if(Eventtype=='Cole Hosted Rep Training CE')

                            ear.name= 'REP'+startyear11+string.valueOf(100);

                       if(Eventtype=='Cole Hosted Rep Training Non CE')

                           ear.name= 'SAL'+startyear11+string.valueOf(100);

                       if(Eventtype=='Employee Event')

                           ear.name= 'COL'+startyear11+string.valueOf(100);

                        if(Eventtype=='Employee Management Meeting')

                            ear.name= 'COL'+startyear11+string.valueOf(100);
                        
                        if(Eventtype=='VEREIT Hosted Event')

                            ear.name= 'VER'+startyear11+string.valueOf(100);


                      } 

                    Integer earYY=integer.valueof((ear.name).substring(3,5));

                    system.debug('check the start date year...'+earYY);
                
               // if(Eventtype != 'BD Marketing Support')
                // {

                    if((eaList1.size()>0))

                    {

                        String thiscount=String.valueOf(eaList1[0].name).right(3);
                        system.debug('----------------------------thiscount-------'+thiscount);

                        Integer mycounter=integer.valueOf(thiscount);
                         system.debug('----------------------------thiscount12-------'+mycounter);

                        mycounter=mycounter+1;
                         system.debug('----------------------------thiscount123-------'+mycounter);

                        Integer mycounter_length=((string.valueOf(mycounter)).length());

                        system.debug('>>>>>>>>>>>>>>>> '+mycounter_length);

                        if(mycounter_length==1)

                        {

                            String mycounter_str= '00'+mycounter;

                             mycounter=integer.valueOf(mycounter_str);

                        }

                        else if(mycounter_length==2)

                        {

                            String mycounter_str= '0'+mycounter;

                            mycounter=integer.valueOf(mycounter_str);

                         }

            system.debug('>>>>>>>>>>>>>>>> '+mycounter);

            if(Eventtype=='Conference-Broker Dealer Platform')

                ear.name= 'BDE'+ startyear11+mycounter; 

            if(Eventtype=='Cole Hosted (Internal)')

                ear.name= 'COL'+startyear11+mycounter;

            if(Eventtype=='Conference-Industry/Real Estate')

                ear.name= 'IND'+startyear11+mycounter;

            if(Eventtype=='Broker Dealer-Miscellaneous')

                ear.name= 'MIS'+startyear11+mycounter;

            //if(Eventtype=='BD Marketing Support')
             //   ear.name= 'BDS'+startyear11+mycounter;
            if(Eventtype=='Broker Dealer Due Diligence')

                ear.name= 'BDD'+startyear11+mycounter;

            if(Eventtype=='Cole Hosted Rep Training CE')

                ear.name= 'REP'+startyear11+mycounter;

            if(Eventtype=='Cole Hosted Rep Training Non CE')

                ear.name= 'SAL'+startyear11+mycounter;

            if(Eventtype=='Employee Event')

                ear.name= 'COL'+startyear11+mycounter;

            if(Eventtype=='Employee Management Meeting')

                ear.name= 'COL'+startyear11+mycounter;
             if(Eventtype=='VEREIT Hosted Event')

                ear.name= 'VER'+startyear11+mycounter;   

             } 
          // }  
        // }  
            
            if(Eventtype=='BD Marketing Support')
            {        
   
              if((eaList2.size()>0))

               {

                        String thiscount1=String.valueOf(eaList2[0].name).right(3);
                        Integer mycounter1=integer.valueOf(thiscount1);
                        mycounter1=mycounter1+1;

                        Integer mycounter1_length=((string.valueOf(mycounter1)).length());

                        system.debug('>>>>>>>>>>>>>>>> '+mycounter1_length);
                                                          

                        if(mycounter1_length==1)

                        {

                            String mycounter1_str= '00'+mycounter1;

                             mycounter1=integer.valueOf(mycounter1_str);

                        }

                        else if(mycounter1_length==2)

                        {

                            String mycounter1_str= '0'+mycounter1;

                            mycounter1=integer.valueOf(mycounter1_str);

                        }

            system.debug('>>>>>>>>>>>>>>>> '+mycounter1);
            
            if(Eventtype=='BD Marketing Support')
            
                ear.name= 'BDS'+startyear11+mycounter1;
                
   
             }              
             
            }
           
           }
} 
} 
}