trigger trTaskupdate on Task (after insert,after update) 
{

    List<Task> lstTask = new List<Task>();
    List<User> lstTerritoryExternal = new List<User>();
    List<User> lstTerritoryInternal = new List<User>();
    
    for ( Task objTask : Trigger.New)
    {
        if (objTask.Subject != NULL && objTask.Subject.startsWithIgnoreCase('Check-In:') && objTask.OwnerId != null)
        {
            lstTask.add(objTask);   
            //System.debug('============> objTask.Subject : '+objTask.Subject);
        }
    }    
    
    if (lstTask.size() > 0)
    {
        Set<Id> userIds = new Set<Id> ();
        Map<Id,String> userTerritoryMap = new Map<Id,String> ();
        Map<String,String> territoryEmailMap = new Map<String,String>();
        Map<Id,String> userIDNameMap = new Map<Id,String> ();
        
        for(Task objTask : lstTask)
        {
            userIds.add(objTask.OwnerId);
            //System.debug('============> userIds : '+userIds);
        }
        
        for(User u : [ SELECT id,Territory__c, name FROM User WHERE Id in :userIds and Territory__c != NULL ])
        {
            userTerritoryMap.put(u.id,u.Territory__c);
            userIDNameMap.put(u.id,u.name);
            //System.debug('============> user u : '+u);            
            //System.debug('============> userTerritoryMap : '+userTerritoryMap);
            //System.debug('============> userIDNameMap : '+userIDNameMap);
        }
        
        for(User u : [ SELECT ID, Email, Profile.name, UserRole.name, Territory__c FROM User WHERE isActive = True and Territory__c in :userTerritoryMap.values() and UserRole.name = 'Internal Sales'])
        {
            territoryEmailMap.put(u.Territory__c,u.Email);  
            //System.debug('============> user u 1 : '+u);             
            //System.debug('============> territoryEmailMap : '+territoryEmailMap);           
        }
        
        for(Task objTask : lstTask)
        {
            if (userTerritoryMap.get(objTask.OwnerId)!=null)
            {                           
                
                //user U= [select id, name from User where id=:objTask.ownerid ];  
                //string name= U.name; 
                string name= userIDNameMap.get(objTask.OwnerId); 
                string subject=objTask.subject;
                String comments=subject.mid(9,subject.length());
                //System.Debug('Original Email id===============>'+territoryEmailMap.get(userTerritoryMap.get(objTask.OwnerId)));
                if(territoryEmailMap.get(userTerritoryMap.get(objTask.OwnerId))!=null)
                {
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    //email.setToAddresses(new String[] { 'ssarwa@colecapital.com' });      
                    email.setToAddresses(new String[] { territoryEmailMap.get(userTerritoryMap.get(objTask.OwnerId)) });                 
                    //email.setCcAddresses(new String[] { 'skalamkar@colecapital.com' });      
                    //String bccAddress='ntambe@colecapital.com';
                    email.setBccAddresses(new String[]{'ntambe@colecapital.com'});
                    email.setSubject('External appointment by '+name);
                    String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + objTask.ID;

                    email.setPlainTextBody('\n Hi,\n\n\tPlease find the External appointment created by '+name+' which needs your review.\n\n\tPlease find the link '+fullRecordURL+'\n\n\tComments: '+Comments+' \n\n Thank You');

                    List<Messaging.SendEmailResult> results = Messaging.sendEmail(new Messaging.Email[] { email });
                    System.debug('============> results : '+results);
                    if (!results.get(0).isSuccess()) 
                    {
                        System.StatusCode statusCode = results.get(0).getErrors()[0].getStatusCode();
                        String errorMessage = results.get(0).getErrors()[0].getMessage();
                        //System.debug('============> statusCode : '+statusCode);
                        //System.debug('============> errorMessage : '+errorMessage);
                    }                                       
                }        
            }
        }
    }
}