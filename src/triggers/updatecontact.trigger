trigger updatecontact on REIT_Investment__c(after insert){

    List<ID> conids= New List<ID>();
    List<contact> con = new List<contact>();
    List<ID> con3775List = New List<ID>();
    List<ID> con3771List = New List<ID>();
    List<ID> con3776List = New List<ID>();
    for(REIT_Investment__c o : Trigger.new)
    {
        if(o.Rep_Contact__c!= null)
        {
            if(o.Fund__c == '3774'){
                conids.add(o.Rep_Contact__c);
            }
            else if(o.Fund__c == '3775'){
                con3775List.add(o.Rep_Contact__c);
            }
            else if(o.Fund__c == '3771'){
                con3771List.add(o.Rep_Contact__c);
            }
             else if(o.Fund__c == '3776'){
                con3776List.add(o.Rep_Contact__c);
        }
    }
   } 
   
    //Income NAV – A & Code - 3774  
    List<contact> con3774 = new List<contact>();
    con3774=[select id,Income_NAV_A__c,Income_NAV_A_First_Investment_Date__c,Income_NAV_A_Tickets__c from contact where id in:conids];

    for(Integer i=0;i<con3774.Size();i++){
        for(Integer j=0;j<Trigger.New.Size();j++){
           // con3774[i].Income_NAV_A_First_Investment_Date__c = trigger.new[j].Deposit_Date__c;
            //con3774[i].Income_NAV_A_Tickets__c = trigger.new[j].Ticket_Count__c;
            if(((con3774[i].Income_NAV_A__c == 0.0)||(con3774[i].Income_NAV_A__c ==null))&&((con3774[i].Income_NAV_A_Tickets__c == 0)||(con3774[i].Income_NAV_A_Tickets__c ==null))&& con3774[i].Income_NAV_A_First_Investment_Date__c==null)
            {
                con3774[i].Income_NAV_A_First_Investment_Date__c = trigger.new[j].Deposit_Date__c;
               // con3774[i].Income_NAV_A__c = trigger.new[j].Current_Capital__c;
                //con3774[i].Income_NAV_A_Tickets__c = trigger.new[j].Ticket_Count__c;
            }
            else
            {
            if(con3774[i].Income_NAV_A_First_Investment_Date__c < trigger.new[j].Deposit_Date__c){
                con3774[i].Income_NAV_A_First_Investment_Date__c = con3774[i].Income_NAV_A_First_Investment_Date__c ;
                }
                else{
                if(con3774[i].Income_NAV_A_First_Investment_Date__c > trigger.new[j].Deposit_Date__c){
               con3774[i].Income_NAV_A_First_Investment_Date__c = trigger.new[j].Deposit_Date__c;
                }
                
                }
               // con3774[i].Income_NAV_A__c = con3774[i].Income_NAV_A__c  + trigger.new[j].Current_Capital__c;
               // con3774[i].Income_NAV_A_Tickets__c = con3774[i].Income_NAV_A_Tickets__c + trigger.new[j].Ticket_Count__c;
            }
        }
    }
    con.addAll(con3774);
    
    //Income NAV – I & Code - 3775
    List<contact> con3775 = new List<contact>();
    con3775=[select id,Income_NAV_I__c,Income_NAV_I_First_Investment_Date__c,Income_NAV_I_Tickets__c from contact where id in:con3775List];

    for(Integer i=0;i<con3775.Size();i++){
        for(Integer j=0;j<Trigger.New.Size();j++){
           // con3775[i].Income_NAV_I_First_Investment_Date__c = trigger.new[j].Deposit_Date__c;
            //con3775[i].Income_NAV_I_Tickets__c = trigger.new[j].Ticket_Count__c;
            if(((con3775[i].Income_NAV_I__c == 0.0)||(con3775[i].Income_NAV_I__c ==null))&&((con3775[i].Income_NAV_I_Tickets__c == 0)||(con3775[i].Income_NAV_I_Tickets__c == null))&& con3775[i].Income_NAV_I_First_Investment_Date__c==null)
            {
                con3775[i].Income_NAV_I_First_Investment_Date__c = trigger.new[j].Deposit_Date__c;
             //   con3775[i].Income_NAV_I__c = trigger.new[j].Current_Capital__c;
              //  con3775[i].Income_NAV_I_Tickets__c = trigger.new[j].Ticket_Count__c;
            }
            else
            {
            if(con3775[i].Income_NAV_I_First_Investment_Date__c < trigger.new[j].Deposit_Date__c){
                con3775[i].Income_NAV_I_First_Investment_Date__c = con3775[i].Income_NAV_I_First_Investment_Date__c;
                }
                else{
                if(con3775[i].Income_NAV_I_First_Investment_Date__c > trigger.new[j].Deposit_Date__c){
               con3775[i].Income_NAV_I_First_Investment_Date__c= trigger.new[j].Deposit_Date__c;
                }
                
                }
               // con3775[i].Income_NAV_I__c = con3775[i].Income_NAV_I__c  + trigger.new[j].Current_Capital__c;
              //  con3775[i].Income_NAV_I_Tickets__c =con3775[i].Income_NAV_I_Tickets__c + trigger.new[j].Ticket_Count__c;
            }
        }
    }
    con.addAll(con3775);
    
    //Income NAV – W & Code - 3771  
    List<contact> con3771 = new List<contact>();
    con3771=[select id,Income_NAV_Tickets__c,Income_NAV_W__c,Income_NAV_W_First_Investment_Date__c,Income_NAV_W_Tickets__c from contact where id in:con3771List];

    for(Integer i=0;i<con3771.Size();i++){
        for(Integer j=0;j<Trigger.New.Size();j++){
        
         //   con3771[i].Income_NAV_W_First_Investment_Date__c = trigger.new[j].Deposit_Date__c;
            //con3771[i].Income_NAV_W_Tickets__c = trigger.new[j].Ticket_Count__c;
            if(((con3771[i].Income_NAV_W__c == 0.0)||(con3771[i].Income_NAV_W__c ==null))&&((con3771[i].Income_NAV_W_Tickets__c == 0)||(con3771[i].Income_NAV_W_Tickets__c ==null))&& con3771[i].Income_NAV_W_First_Investment_Date__c==null)
            {
                con3771[i].Income_NAV_W_First_Investment_Date__c = trigger.new[j].Deposit_Date__c;
               // con3771[i].Income_NAV_W__c = trigger.new[j].Current_Capital__c;
               // con3771[i].Income_NAV_W_Tickets__c = trigger.new[j].Ticket_Count__c;
            }
            else
            {
            if(con3771[i].Income_NAV_W_First_Investment_Date__c < trigger.new[j].Deposit_Date__c){
                con3771[i].Income_NAV_W_First_Investment_Date__c = con3771[i].Income_NAV_W_First_Investment_Date__c;
                }
                else{
                if(con3771[i].Income_NAV_W_First_Investment_Date__c > trigger.new[j].Deposit_Date__c){
               con3771[i].Income_NAV_W_First_Investment_Date__c = trigger.new[j].Deposit_Date__c;
                }
                
                }
               
               // con3771[i].Income_NAV_W__c += trigger.new[j].Current_Capital__c;
               // con3771[i].Income_NAV_W_Tickets__c +=  trigger.new[j].Ticket_Count__c;
            }
        }
    }
    con.addAll(con3771);
    //CCIT II code-3776
    List<contact> con3776 = new List<contact>();
    con3776=[select id,CCIT_II__c,CCIT_II_First_Investment_Date__c,CCIT_II_Tickets__c from contact where id in:con3776List];
      for(Integer i=0;i<con3776.Size();i++){
        for(Integer j=0;j<Trigger.New.Size();j++){
            //con3776[i].CCIT_II_First_Investment_Date__c = trigger.new[j].Deposit_Date__c;
            //con3771[i].Income_NAV_W_Tickets__c = trigger.new[j].Ticket_Count__c;
            if(((con3776[i].CCIT_II__c == 0.0)||(con3776[i].CCIT_II__c ==null))&&((con3776[i].CCIT_II_Tickets__c == 0)||(con3776[i].CCIT_II_Tickets__c ==null)) && con3776[i].CCIT_II_First_Investment_Date__c==null)
            {
                con3776[i].CCIT_II_First_Investment_Date__c = trigger.new[j].Deposit_Date__c;
               // con3776[i].CCIT_II__c = trigger.new[j].Current_Capital__c;
               // con3776[i].CCIT_II_Tickets__c = trigger.new[j].Ticket_Count__c;
            }
            else
            {
                if(con3776[i].CCIT_II_First_Investment_Date__c < trigger.new[j].Deposit_Date__c){
                con3776[i].CCIT_II_First_Investment_Date__c = con3776[i].CCIT_II_First_Investment_Date__c;
                }
                else{
                if(con3776[i].CCIT_II_First_Investment_Date__c > trigger.new[j].Deposit_Date__c){
               con3776[i].CCIT_II_First_Investment_Date__c = trigger.new[j].Deposit_Date__c;
                }
                
                }
              //  con3776[i].CCIT_II__c += trigger.new[j].Current_Capital__c;
               // con3776[i].CCIT_II_Tickets__c +=  trigger.new[j].Ticket_Count__c;
                }
                }
                }
           con.addAll(con3776);   
           
    // updating contacts
    update con;
    
   
    //To calcualte current capital from REIT and update in to the Contact field.--Start--
    List<REIT_Investment__c> lstreits = new List<REIT_Investment__c>();
    Map<Id, Contact> conMap = new Map<Id, Contact>();
    List<Contact> conlist= new List<Contact>();
    set<Id> currentconIds = new Set<Id> ();

    date firstdaythisyear = date.newInstance(Date.today().Year(),1,1);
    date lastdaythisyear = date.newInstance(Date.today().Year(),12,31);

    decimal currentcapitalthisyear= 0;

    for(REIT_Investment__c objReit : Trigger.New)
    {
          currentconIds.add(objReit.Rep_Contact__c);
           
    }
    system.debug('check the keyset'+currentconIds);

    conlist = [Select id, Year_To_Date__c from Contact where id in : currentconIds limit 1];

     for(Contact objNH:conlist)
     {
          conMap.put(objNH.id,objNH); 
     }
     system.debug('check the map..'+conMap);

    for(REIT_Investment__c objReit : Trigger.New)
     {
     if(conMap.get(objReit.Rep_Contact__c)!=null)
      {
       if((objReit.Deposit_Date__c >= firstdaythisyear)&&(objReit.Deposit_Date__c<=lastdaythisyear))
        {
         if(conMap.get(objReit.Rep_Contact__c).Year_To_Date__c == null)
         {
          conMap.get(objReit.Rep_Contact__c).Year_To_Date__c = 0;
         }
         
         System.debug('&&&&&&&&11' + conMap.get(objReit.Rep_Contact__c).Year_To_Date__c);
         //conMap.get(objReit.Rep_Contact__c).Year_To_Date__c  += integer.valueOf(objReit.Current_Capital__c);
         conMap.get(objReit.Rep_Contact__c).Year_To_Date__c  += (objReit.Current_Capital__c != null ? integer.valueOf(objReit.Current_Capital__c) : 0);
         
         System.debug('&&&&&&&&22' + conMap.get(objReit.Rep_Contact__c).Year_To_Date__c);
         
        }
       }
     }
  
  update conMap.values();
  system.debug('check the map...'+conMap);
   
}