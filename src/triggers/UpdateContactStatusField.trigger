trigger UpdateContactStatusField on CampaignMember (after insert, after update)
{
   set<id>contactidset=new set<id>();
   For (CampaignMember a: Trigger.new)
   { 
      if(Trigger.isInsert)
      {
        contactidset.add(a.ContactId);
            
      }
      
      if(Trigger.isUpdate)
      {
         CampaignMember oldvalues=Trigger.oldMap.get(a.Id);
         if(a.Status!= oldvalues.Status)
          contactidset.add(a.ContactId);
      
      }
      
   
   }
   list<Contact>clist=[select id,Contact_Status_Changed_on__c from Contact where id in:contactidset];
   list<Contact>Updatedlist=new list<Contact>();
   for(Contact ct:clist)
   {
    ct.Contact_Status_Changed_on__c =system.now();
    Updatedlist.add(ct);
   }
    update Updatedlist;
}