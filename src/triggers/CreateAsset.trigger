trigger CreateAsset on Asset_Inventory__c (after insert,after update)
{
    
  list<Employee__c>emplist=[select id from Employee__c where Employee_Status__c!='Terminated']; 
  list<Asset__c>assetlist= new list<Asset__c>(); 
  list<Asset__c>existingList=[select id, name,Access_Name__c,Asset_Status__c,Access_Type__c,Application_Owner__c,Application_Implementor__c,Employee__c,Asset_Inventory__c from Asset__c where Asset_Inventory__c=:Trigger.new[0].id];
  list<Asset__c>UpdatedList=new list<Asset__c>();

 if(Trigger.isInsert) 
 { 
      for(Asset_Inventory__c Ast:Trigger.new)
      {
          for(Employee__c ec:emplist)
          {
              Asset__c ct= new Asset__c();
              ct.Access_Name__c=Ast.name;
              ct.Asset_Status__c='InActive';         
              ct.Access_Type__c=Ast.Asset_Type__c;
              ct.Application_Owner__c=Ast.Application_Owner__c;  
              ct.Application_Implementor__c=Ast.Application_Implementor__c;
              ct.Verifier__c=Ast.Verifier__c;
              ct.Employee__c=ec.id;
              ct.Asset_Inventory__c=Ast.id;
              ct.Additional_Description__c=Ast.Additional_Descirption__c;
              assetlist.add(ct);
          }
        }
  
    insert assetlist;
    }
    
     if(Trigger.isUpdate) 
   { 
      for(Asset__c ct:existingList)
      {
           ct.Application_Owner__c=Trigger.new[0].Application_Owner__c;  
           ct.Application_Implementor__c=Trigger.new[0].Application_Implementor__c;
           ct.Verifier__c=Trigger.new[0].Verifier__c;
           ct.Access_Name__c=Trigger.new[0].name;
           //ct.Employee__c=ec.id;
           ct.Access_Type__c=Trigger.new[0].Asset_Type__c;
           UpdatedList.add(ct);
       }
    
  
    update UpdatedList;
    }

    
    
    
     
}