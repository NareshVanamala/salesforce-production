trigger updateMemberStatus on BDApprovalHelper__c (after insert) 
{
  public List<CampaignMember> ApprovedcampList ;
   public List<CampaignMember> DeclinedcampList ;
  public List<CampaignMember> UpdateedcampList=new list<CampaignMember>() ;
  public set<string>Approvedcamped= new set<String>(); 
  public set<string>ApprovedcampnameSet= new set<String>(); 
  public set<string>Declinedcamped= new set<String>(); 
  public set<string>DeclinedcampnameSet= new set<String>();   
 for(BDApprovalHelper__c bdc:trigger.new)
 {
     if(bdc.Status__c=='Approved')
     {
         Approvedcamped.add(bdc.CampaignMemeberId__c);
         ApprovedcampnameSet.add(bdc.CampaignMemeberName__c);
      }
     if(bdc.Status__c=='Declined')
     {
         Declinedcamped.add(bdc.CampaignMemeberId__c);
         DeclinedcampnameSet.add(bdc.CampaignMemeberName__c);
      }
 }      
   ApprovedcampList =[select id, RecordTypeId, CampaignId ,Contact.FirstName,Contact.name, Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c,Broker_Dealer_Name__c from CampaignMember where id In:Approvedcamped and Campaign.Name in:ApprovedcampnameSet];
   DeclinedcampList =[select id, RecordTypeId, CampaignId ,Contact.FirstName,Contact.name, Contact.MailingCity,Contact.MailingState,Campaign.Name ,status,Account__c,Broker_Dealer_Name__c from CampaignMember where id In:Declinedcamped and Campaign.Name in:DeclinedcampnameSet];
    
     
    system.debug('check the size...'+ApprovedcampList .size());
    for(CampaignMember cmb:ApprovedcampList )
  {
      cmb.status='Approved';
      UpdateedcampList.add(cmb);
   }
   for(CampaignMember cmb:DeclinedcampList )
  {
      cmb.status='Declined';
      UpdateedcampList.add(cmb);
   }


 if(UpdateedcampList.size()>0)
  update UpdateedcampList;
  
 system.debug('updatedlistofcmpaignmemners..'+UpdateedcampList) ;
}