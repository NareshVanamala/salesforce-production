trigger LC_OS_Months_YearsValidation on LC_OptionSchedule__c (before insert,before update)
{
     list<LC_OptionSchedule__c >LC_Optionlist=new list<LC_OptionSchedule__c >();
      list<LC_OptionSchedule__c >LC_OptionUpdatelist=new list<LC_OptionSchedule__c >();
     LC_LeaseOpprtunity__c lcp= new LC_LeaseOpprtunity__c();
     lcp=[select id, name from LC_LeaseOpprtunity__c where id=:Trigger.new[0].Lease_Opportunity__c];
       
     LC_Optionlist=[select id,Option_Type__c, From_In_Months__c,To_In_Months__c,Lease_Opportunity__c from LC_OptionSchedule__c where Lease_Opportunity__c=:lcp.id];
     string myid= Trigger.new[0].id;
     LC_OptionUpdatelist=[select id,Option_Type__c, From_In_Months__c,To_In_Months__c,Lease_Opportunity__c from LC_OptionSchedule__c where Lease_Opportunity__c=:lcp.id and id!=:myid ];
     
      if(trigger.isinsert) 
    { 
        for(LC_OptionSchedule__c sNew:Trigger.new)
        { 
            if(LC_Optionlist.size()>0)
            {
                for(LC_OptionSchedule__c LCP1:LC_Optionlist)
                {
                    if((sNew.From_In_Months__c == LCP1.From_In_Months__c)&&(sNew.To_In_Months__c == LCP1.To_In_Months__c)) 
                    sNew.addError('Duplicate From and To values.'); 
                }  
            } 
        }
    }
    
     if(trigger.isupdate) 
    { 
        for(LC_OptionSchedule__c sNew:Trigger.new)
        { 
            if(LC_OptionUpdatelist.size()>0)
            {
                for(LC_OptionSchedule__c LCP1:LC_OptionUpdatelist)
                {
                    if((sNew.From_In_Months__c == LCP1.From_In_Months__c)&&(sNew.To_In_Months__c == LCP1.To_In_Months__c)) 
                    sNew.addError('Duplicate From and To values.'); 
                }  
            } 
        }
    }
    
    
 }