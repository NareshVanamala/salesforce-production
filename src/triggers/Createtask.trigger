trigger Createtask on Task_Plan__c (after insert, after update, after delete)
{
      list<Task_Plan__c>ListToclass=new list<Task_Plan__c>();
       
           if((trigger.isUpdate)||(trigger.isInsert))
           {
              for(Task_Plan__c ct:trigger.new)
              {
                  if(ct.TaskRelatedto__c!=null)
                  {
                    if((ct.name=='appraisal')||(ct.name=='Property Condition Report')||(ct.name=='Roof Report')||(ct.name=='Environmental Report')||(ct.name=='Copy of CC&Rs saved and distributed'))
                    {
                      ListToclass.add(ct);
                      if(ListToclass.size()>0)
                       RC_UpdateAppraisaldateOFPropertyFinal.UpdateAppraisalDate1(ListToclass);
                     }   
                     
                    } 
            } 
        }  
    if(trigger.isinsert){
        List<Task> inserttasklist = new List<Task>();
        
        Task t;
        
        for(Task_Plan__c ct:trigger.new)
        {
          
        
        
        
        
                    
            if((ct.TaskRelatedto__c!=null || ct.TaskRelatedtoLoan__c!=null) &&(ct.create_Task__c==true)) {
                t= new Task();
                t.subject = ct.Name;
                system.debug('Give me the Name of record'+ct.Name);
                if(ct.Assigned_To__c!=null)t.ownerid=ct.Assigned_To__c;
                if(ct.Comments__c!=null)t.Description=ct.Comments__c;
                if(ct.Date_Completed__c!=null)t.Date_Completed__c=ct.Date_Completed__c;
                if(ct.Date_Needed__c!=null)t.Date_Needed__c=ct.Date_Needed__c;
                if(ct.Date_Received__c!=null)t.Date_Received__c=ct.Date_Received__c;
                if(ct.Date_Ordered__c!=null)t.Date_Ordered__c=ct.Date_Ordered__c;
                if(ct.Date_Needed__c!=null)t.ActivityDate = ct.Date_Needed__c;
                if(ct.Priority__c!=null)t.Priority=ct.Priority__c; 
                if(ct.Status__c!=null){t.Status=ct.Status__c;}
                if(ct.TaskRelatedto__c!=null)
                {
                    t.whatid=ct.TaskRelatedto__c;
                }
                if(ct.TaskRelatedToloan__c!=null)
                {
                    t.whatid=ct.TaskRelatedToloan__c;
                }
                String RecordId = ct.id;
                t.Action_Plan_Task__c= RecordId.substring(0,15);
                inserttasklist.add(t);
            }
        }
        if(inserttasklist.size()>0)
        {
        insert inserttasklist;
        system.debug('This is the my list of the created tasks..'+inserttasklist);
        }
    }
    if(trigger.isupdate)
    {
     if(processtaskplan.processinitializer('processtaskplan'))
     {   
         Map<id, Task_Plan__c> TaskPlan_idmap = new Map<id, Task_Plan__c>();
         Map<id, Task> TaskPlanmap = new Map<id, Task>();
         Set<String> IdSet = new Set<String>();
         set<id> Idset1 = new set<id>();
         List<String> IdList = new List<String>();
         
         for(Task_Plan__c T:trigger.new){
             
             String Recid=T.id;
             IdSet.add(Recid.substring(0,15));
             IdList.add(Recid.substring(0,15));
             system.debug('get the Idlist'+IdList);
             Idset1.add(T.id);
             System.debug('get the List of set'+Idset1);
         }
         TaskPlan_idmap = trigger.newmap;
         
         for(Task T:[Select id, whatid, Action_Plan_Task__c, Status, Priority, X18_digit_Id__c,ActivityDate, Date_Ordered__c, Date_Received__c, Date_Needed__c, Date_Completed__c, Description, Ownerid from Task where X18_digit_Id__c IN :Idset1 and Action_Plan_Task__c!= null]){
             TaskPlanmap.put(T.X18_digit_Id__c, T);
         }
         if(TaskPlanmap.size()>0){
            // task t;
             List<Task> inserttasklist = new List<Task>();
             for(Task_Plan__c ct:trigger.new){                 
                if(ct.TaskRelatedto__c!=null || ct.TaskRelatedtoLoan__c!=null) {             
                  Task  t= new Task();
                   if(TaskPlanmap.Containskey(ct.id))
                   {
                   
                   t=TaskPlanmap.get(ct.id);
                   system.debug('Get me the the record for update'+TaskPlanmap.get(ct.id));
                   }
                   system.debug('give me the final record'+t.id);
                    t.subject = ct.Name;
                    system.debug('give me Action Plan task'+ct.Name);
                    if(ct.Assigned_To__c!=null)t.ownerid=ct.Assigned_To__c;
                    if(ct.Comments__c!=null)t.Description=ct.Comments__c;
                    if(ct.Date_Completed__c!=null)t.Date_Completed__c=ct.Date_Completed__c;
                    if(ct.Date_Needed__c!=null)t.Date_Needed__c=ct.Date_Needed__c;
                    if(ct.Date_Received__c!=null)t.Date_Received__c=ct.Date_Received__c;
                    if(ct.Date_Ordered__c!=null)t.Date_Ordered__c=ct.Date_Ordered__c;
                    if(ct.Date_Needed__c!=null)t.ActivityDate = ct.Date_Needed__c;
                    if(ct.Priority__c!=null)t.Priority=ct.Priority__c; 
                    if(ct.Status__c!=null){t.Status=ct.Status__c;}
                    String RecordId = ct.id;
                    t.Action_Plan_Task__c= RecordId.substring(0,15);               
                    if(ct.TaskRelatedto__c!=null)
                    {
                    t.whatid=ct.TaskRelatedto__c;
                    }
                    if(ct.TaskRelatedToloan__c!=null)
                    {
                    t.whatid=ct.TaskRelatedToloan__c;
                    }
                    inserttasklist.add(t);
                    }
                
                
            }
            if(inserttasklist.size()>0){
                Upsert inserttasklist;
            }
         }
     }
   }  
    if(trigger.isdelete){
       /* Map<id, Task_Plan__c> TaskPlan_idmap_Del = new Map<id, Task_Plan__c>();
        TaskPlan_idmap_Del = trigger.oldmap;
        List<Task> Del_TaskList = new List<Task>();
        Del_TaskList=[Select id from Task where Action_Plan_Task__c IN: TaskPlan_idmap_Del.keyset()];
        system.debug('$$$$'+Del_TaskList);
        if(Del_TaskList.size()>0){Delete Del_TaskList;}
        List<Id> taskplanstring = new List<Id>();
        for(integer i =0;i<trigger.Old.Size();i++){
        taskplanstring.add(trigger.Old[i].Id);
        }
        List<Task> Del_TaskList = new List<Task>();
        Del_TaskList=[Select id from Task where Action_Plan_Task__c IN: taskplanstring];
        if(Del_TaskList.size()>0){Delete Del_TaskList;}*/
       if(processtaskplan.processinitializer('processtaskplan')){
        Map<id, Task_Plan__c> TaskPlan_idmap = new Map<id, Task_Plan__c>();
         Map<id, Task> TaskPlanmap = new Map<id, Task>();
         Set<String> IdSet = new Set<String>();
         List<String> IdList = new List<String>();
         set<id> Idset1 = new set<id>();
         for(Task_Plan__c T:trigger.old){
             
             String Recid=T.id;
             IdSet.add(Recid.substring(0,15));
             IdList.add(Recid.substring(0,15));
              Idset1.add(T.id);
         }
         TaskPlan_idmap = trigger.oldmap;
         
         for(Task T:[Select id, whatid, Action_Plan_Task__c,X18_digit_Id__c, Status, Priority, ActivityDate, Date_Ordered__c, Date_Received__c, Date_Needed__c, Date_Completed__c, Description, Ownerid from Task where X18_digit_Id__c =:IdList and Action_Plan_Task__c!= null]){
             TaskPlanmap.put(T.Action_Plan_Task__c, T);
         }
         if(TaskPlanmap.size()>0){
             task t;
             List<Task> inserttasklist = new List<Task>();
             for(Task_Plan__c ct:trigger.old){                 
                if(ct.TaskRelatedto__c!=null || ct.TaskRelatedto__c!=null){             
                    t= new Task();
                    if(TaskPlanmap.Containskey(ct.id)){t=TaskPlanmap.get(ct.id);}
                    
                    inserttasklist.add(t);
                }
            }
            if(inserttasklist.size()>0){
            system.debug('@@@'+inserttasklist.size());
                Delete inserttasklist;
            }
         }
     
      }  
        
    }
}