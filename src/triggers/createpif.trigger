trigger createpif on Project_Initiation__c (after update) 
{ 
   
     list<PIF_Child__c > pclist = new list<PIF_Child__c >();
     List<GroupMember> gmlist = new list<GroupMember>();
     List<GroupMember> gmlist1 = new list<GroupMember>();
     List<GroupMember> gmlist3 = new list<GroupMember>();
     List<GroupMember> gmlist4 = new list<GroupMember>();
     list<PIF_Child__c>PMOChildList= new list<PIF_Child__c>();
     list<PIF_Child__c>PIFChildList= new list<PIF_Child__c>();
     list<PIF_Child__c>ChampionlistChildList= new list<PIF_Child__c>();
     list<PIF_Child__c>sponsorChildList= new list<PIF_Child__c>();
     list<PIF_Child__c>CFOChildList= new list<PIF_Child__c>();
     list<PIF_Child__c>CompliancechildList= new list<PIF_Child__c>();

                   
    list<PIF_Child__c>Childlist=[select id,name,CFO_GRoup__c,CFO_Group_Status__c,Champion_group__c,Champion_Group_Status__c,Compliance_group__c,Compliance_Group_Status__c,PIF_Group__c,PIF_Group_Status__c,PIF_Name__c,PIF_parent__c,PMO_Group__c,PMO_Group_Status__c,Project_Requires_Compliance_Approval__c,Sponsor__c,Sponsor_Status__c from PIF_Child__c where PIF_parent__c=:trigger.new[0].id ];
    if(Childlist.size()>0)
    {
        for(PIF_Child__c pc1:Childlist)
        {
        if(pc1.name=='PMO Approval Process')
         PMOChildList.add(pc1);
        
        if(pc1.name=='PIF Review Group Approval Process')
         PIFChildList.add(pc1); 
        
        if(pc1.name=='Champion Approval Process')
         ChampionlistChildList.add(pc1);  
        
        if(pc1.name=='Sponsor Approval Process')
         sponsorChildList.add(pc1);  
      
         if(pc1.name=='Compliance Approval Process')
         CompliancechildList.add(pc1);
        
         if(pc1.name=='CFO Approval Process')
         CFOChildList.add(pc1); 
       } 
    
    }
    
    
    
    
    
    
     group g = new group();
     g= [select id,name from Group where name='PMO'limit 1];
     gmlist = [Select UserOrGroupId From GroupMember where GroupId =:g.id];   
     
     group g1 = new group();
     g1= [select id,name from Group where name='PIF'];
     gmlist1 = [Select UserOrGroupId From GroupMember where GroupId =:g1.id];  
     
     group g3 = new group();
    g3= [select id,name from Group where name='Compliance'];
      system.debug('????????????g33333333??????????????????'+ g3);
    gmlist3 = [Select UserOrGroupId From GroupMember where GroupId =:g3.id];
     system.debug('????????????gmlist3??????????????????'+ gmlist3);
      group g4 = new group();
     g4= [select id,name from Group where name='CFO'];
    gmlist4 = [Select UserOrGroupId From GroupMember where GroupId =:g4.id];
      
      
     for(Project_Initiation__c objectparent : trigger.new)
        {
        
        Project_Initiation__c p = [select id,name,Selected_Champions__c  from Project_Initiation__c where id =:objectparent.id];
            system.debug('????????????ppppp??????????????????'+p);
           
           string s =  p.Selected_Champions__c ;
                        list<string> slist = new list<string>();
                        list<id> listids = new list<id>();
                        if( p.Selected_Champions__c  != null)
                        slist=  p.Selected_Champions__c .split(',');
                         system.debug('????????????slist??????????????????'+slist);
                        list<user> u = [select id,name from user where name in:slist];
                        system.debug('????????????userlist??????????????????'+u);
                       for(user u1:u)
                            {
                          listids.add(u1.id);
                           }
                          /* group g2 = new group();
                         g2= [select id,name from Group where name='Champion'];
                       List<GroupMember> gmlist2  = [Select UserOrGroupId From GroupMember where GroupId =:g2.id and UserOrGroupId in:listids];
                              system.debug('??????????????????????????????'+gmlist2);*/
      
        
           if(objectparent.PMO_Group_Status__c == 'Submitted for Approval')
             {
               if(PMOChildList.size()==0)
               {
                 for(integer i=0;i<gmlist.size();i++)
                  {
                      PIF_Child__c  objectchild = new PIF_Child__c();
                      objectchild.PMO_Group_Status__c = 'Submitted for Approval';
                      objectchild.Name='PMO Approval Process';
                      objectchild.PMO_Group__c = gmlist[i].UserOrGroupId;
                      objectchild.PIF_parent__c = objectparent.id;
                      pclist.add(objectchild);
                  }
                }
             }
            if((objectparent.PMO_Group_Status__c == 'Approved')&&(objectparent.PIF_Group_Status__c =='Submitted for Approval'))
             {
                if(PIFChildList.size()==0)
                {
                   for(integer i=0;i<gmlist1.size();i++)
                  {
                     PIF_Child__c  objectchild1 = new PIF_Child__c();
                      objectchild1.PMO_Group_Status__c = 'Approved';
                      objectchild1.PIF_Group_Status__c = 'Submitted for Approval';
                      objectchild1.Name='PIF Review Group Approval Process';
                      objectchild1.PIF_Group__c = gmlist1[i].UserOrGroupId;
                      objectchild1.PIF_parent__c = objectparent.id;
                      pclist.add(objectchild1);
                  }
                 } 
             }
             
             
                      
         if((objectparent.PMO_Group_Status__c == 'Approved')&&(objectparent.PIF_Group_Status__c == 'Approved')&&(objectparent.Champion_Status__c =='Submitted for Approval'))
             {
            //system.debug('------------gmlist2--------------->'+gmlist2.size());
                
                if(ChampionlistChildList.size()==0)
                {
                 for(integer i=0;i<listids.size();i++)
                  {
                      PIF_Child__c  objectchild2 = new PIF_Child__c();
                      objectchild2.PMO_Group_Status__c = 'Approved';
                      objectchild2.PIF_Group_Status__c = 'Approved';
                      objectchild2.Champion_Group_Status__c = 'Submitted for Approval';
                      objectchild2.Name='Champion Approval Process';
                      objectchild2.Champion_group__c = listids[i];
                      objectchild2.PIF_parent__c = objectparent.id;
                      pclist.add(objectchild2);
                  }
              }
            } 
             
        if((objectparent.PMO_Group_Status__c == 'Approved')&&(objectparent.PIF_Group_Status__c == 'Approved')&&(objectparent.Champion_Status__c =='Approved')&&(objectparent.Sponsor_status__c =='Submitted for Approval'))             
              {
                
                 if(sponsorChildList.size()==0)
                 {
                
                      PIF_Child__c  objectchild3 = new PIF_Child__c();
                      objectchild3.PMO_Group_Status__c = 'Approved';
                      objectchild3.PIF_Group_Status__c = 'Approved';
                      objectchild3.Name='Sponsor Approval Process';
                      objectchild3.Champion_Group_Status__c = 'Approved';
                      objectchild3.Sponsor_Status__c ='Submitted for Approval';
                      objectchild3.Sponsor__c = objectparent.Sponsor__c;
                      objectchild3.PIF_parent__c = objectparent.id;
                      pclist.add(objectchild3);
                      
                 }     
        
             } 
             
         if((objectparent.PMO_Group_Status__c == 'Approved')&&(objectparent.PIF_Group_Status__c == 'Approved')&&(objectparent.Champion_Status__c =='Approved')&&(objectparent.Sponsor_status__c =='Approved')&&(objectparent.compliance_Approval_Status__c =='Submitted for Approval')&&(objectparent.Projecr_Requires_Compliance_Approval__c==true))             
              {
                
                if(CompliancechildList.size()==0)
                {
                    for(integer i=0;i<gmlist3.size();i++)
                  {
                    
                      PIF_Child__c  objectchild4 = new PIF_Child__c();
                      objectchild4.PMO_Group_Status__c = 'Approved';
                      objectchild4.PIF_Group_Status__c = 'Approved';
                      objectchild4.Champion_Group_Status__c = 'Approved';
                      objectchild4.Sponsor_Status__c ='Approved';
                      objectchild4.Compliance_Group_Status__c= 'Submitted for Approval';
                      objectchild4.Name='Compliance Approval Process';
                      objectchild4.Project_Requires_Compliance_Approval__c=true;
                      objectchild4.Compliance_group__c = gmlist3[i].UserOrGroupId;
                      objectchild4.PIF_parent__c = objectparent.id;
                      pclist.add(objectchild4);
                   }
                } 
           }
        if((objectparent.PMO_Group_Status__c == 'Approved')&&(objectparent.PIF_Group_Status__c == 'Approved')&&(objectparent.Champion_Status__c =='Approved')&&(objectparent.Sponsor_status__c =='Approved')&&(objectparent.compliance_Approval_Status__c =='Approved')&&(objectparent.CFO_Status__c== 'Submitted for Approval')&&(objectparent.TOTAL__c>0))             
              {
                
                if(CFOChildList.size()==0)
                {
                   for(integer i=0;i<gmlist4.size();i++)
                  {
                    
                      PIF_Child__c  objectchild5 = new PIF_Child__c();
                      objectchild5.PMO_Group_Status__c = 'Approved';
                      objectchild5.PIF_Group_Status__c = 'Approved';
                      objectchild5.Champion_Group_Status__c = 'Approved';
                      objectchild5.Sponsor_Status__c ='Approved';
                      objectchild5.Compliance_Group_Status__c= 'Approved';
                      objectchild5.CFO_Group_Status__c ='Submitted for Approval';
                      objectchild5.CFO_GRoup__c = gmlist4[i].UserOrGroupId;
                      objectchild5.Name='CFO Approval Process';
                      objectchild5.PIF_parent__c = objectparent.id;
                      pclist.add(objectchild5);
                  }
                  
                }  
             } 
        else if(objectparent.PMO_Group_Status__c == 'Rejected')
             {
                 PIF_Child__c  objectchild6 = new PIF_Child__c();
                  objectchild6.PMO_Group_Status__c = 'Rejected';
             }
             if((objectparent.PMO_Group_Status__c == 'Approved')&&(objectparent.PIF_Group_Status__c =='Rejected'))
             {
                
                     PIF_Child__c  objectchild7 = new PIF_Child__c();
                      objectchild7.PMO_Group_Status__c = 'Approved';
                      objectchild7.PIF_Group_Status__c = 'Rejected';
             }
             if((objectparent.PMO_Group_Status__c == 'Approved')&&(objectparent.PIF_Group_Status__c == 'Approved')&&(objectparent.Champion_Status__c =='Rejected'))
             {
                    PIF_Child__c  objectchild8 = new PIF_Child__c();
                      objectchild8.PMO_Group_Status__c = 'Approved';
                      objectchild8.PIF_Group_Status__c = 'Approved';
                      objectchild8.Champion_Group_Status__c = 'Rejected';
               
             }
            if((objectparent.PMO_Group_Status__c == 'Approved')&&(objectparent.PIF_Group_Status__c == 'Approved')&&(objectparent.Champion_Status__c =='Approved')&&(objectparent.Sponsor_status__c =='Rejected'))             
              {
                      PIF_Child__c  objectchild9 = new PIF_Child__c();
                      objectchild9.PMO_Group_Status__c = 'Approved';
                      objectchild9.PIF_Group_Status__c = 'Approved';
                      objectchild9.Champion_Group_Status__c = 'Approved';
                      objectchild9.Sponsor_Status__c ='Rejected';
              }
            if((objectparent.PMO_Group_Status__c == 'Approved')&&(objectparent.PIF_Group_Status__c == 'Approved')&&(objectparent.Champion_Status__c =='Approved')&&(objectparent.Sponsor_status__c =='Approved')&&(objectparent.compliance_Approval_Status__c =='Rejected'))             
              {
                    
                      PIF_Child__c  objectchild10 = new PIF_Child__c();
                      objectchild10.PMO_Group_Status__c = 'Approved';
                      objectchild10.PIF_Group_Status__c = 'Approved';
                      objectchild10.Champion_Group_Status__c = 'Approved';
                      objectchild10.Sponsor_Status__c ='Approved';
                      objectchild10.Compliance_Group_Status__c= 'Rejected';
              }
             if((objectparent.PMO_Group_Status__c == 'Approved')&&(objectparent.PIF_Group_Status__c == 'Approved')&&(objectparent.Champion_Status__c =='Approved')&&(objectparent.Sponsor_status__c =='Approved')&&(objectparent.compliance_Approval_Status__c =='Approved')&&(objectparent.CFO_Status__c== 'Rejected'))             
              {
                   
                      PIF_Child__c  objectchild11 = new PIF_Child__c();
                      objectchild11.PMO_Group_Status__c = 'Approved';
                      objectchild11.PIF_Group_Status__c = 'Approved';
                      objectchild11.Champion_Group_Status__c = 'Approved';
                      objectchild11.Sponsor_Status__c ='Approved';
                      objectchild11.Compliance_Group_Status__c= 'Approved';
                      objectchild11.CFO_Group_Status__c ='Rejected';
                }
            insert pclist;
          
        }




}