//***Created By Priya on May 23rd 2012*****

trigger BusinessProfileContactTrack on Contact(after insert,after update) {

 List<BusinessProfileContactTracking__c> bptlist = new List<BusinessProfileContactTracking__c>(); 
 List<BusinessProfileContactTracking__c> bptlist1 = new List<BusinessProfileContactTracking__c>(); 

 
 
For (Contact a: Trigger.new)
{
 if(a.RecordTypeid == '012300000004rLn'){ 
if(trigger.isupdate){
Contact oldvalues=Trigger.oldMap.get(a.Id);
if(helper.run == TRUE)
{
if(a.BusSize_Total_AUM__c != oldvalues.BusSize_Total_AUM__c)
 {
  BusinessProfileContactTracking__c bpt4 = new BusinessProfileContactTracking__c();
  bpt4.name= 'Total AUM Updated';
  bpt4.Modified_on__c= a.LastModifiedDate;
  bpt4.Modified_By__c=a.OwnerId;
  bpt4.Comments__c='Value changed from '+Trigger.old[0].BusSize_Total_AUM__c+' to '+ a.BusSize_Total_AUM__c;
  bpt4.Business_Type__c='Total AUM';
  bpt4.Contact__c = a.id;
  bptlist.add(bpt4);
 }
 if(a.TOP5_of_Clients_with_RE_in_Portfolio__c != oldvalues.TOP5_of_Clients_with_RE_in_Portfolio__c)
 {
  BusinessProfileContactTracking__c bpt5 = new BusinessProfileContactTracking__c();
  bpt5.name= '% of Clients with RE in Portfolio Updated';
  bpt5.Modified_on__c= a.LastModifiedDate;
  bpt5.Modified_By__c=a.OwnerId;
  bpt5.Comments__c='Value changed from '+Trigger.old[0].TOP5_of_Clients_with_RE_in_Portfolio__c+' to '+ a.TOP5_of_Clients_with_RE_in_Portfolio__c;
  bpt5.Business_Type__c='% of Clients with RE in Portfolio';
  bpt5.Contact__c = a.id;
  bptlist.add(bpt5);
 }
 /*if(a.Other_RE_Exposure__c !=oldvalues.Other_RE_Exposure__c)
 {
  BusinessProfileContactTracking__c bpt6 = new BusinessProfileContactTracking__c();
  bpt6.name= 'Other RE Exposure Updated';
  bpt6.Modified_on__c= a.LastModifiedDate;
  bpt6.Modified_By__c=a.OwnerId;
  bpt6.Comments__c='Value changed from '+Trigger.old[0].Other_RE_Exposure__c+' to '+ a.Other_RE_Exposure__c;
  bpt6.Business_Type__c='Other RE Exposure';
  bpt6.Contact__c = a.id;
  bptlist.add(bpt6);
 }*/
 if(a.Bus_Size_Allocated_to_RE__c != oldvalues.Bus_Size_Allocated_to_RE__c)
 {
  BusinessProfileContactTracking__c bpt7 = new BusinessProfileContactTracking__c();
  bpt7.name= 'Allocated to RE Updated';
  bpt7.Modified_on__c= a.LastModifiedDate;
  bpt7.Modified_By__c=a.OwnerId;
  bpt7.Comments__c='Value changed from '+Trigger.old[0].Bus_Size_Allocated_to_RE__c+' to '+ a.Bus_Size_Allocated_to_RE__c;
  bpt7.Business_Type__c='Allocated to RE';
  bpt7.Contact__c = a.id;
  bptlist.add(bpt7);
 }
 if(a.BD_Top_Producer__c != oldvalues.BD_Top_Producer__c)
 {
  BusinessProfileContactTracking__c bpt8 = new BusinessProfileContactTracking__c();
  bpt8.name= 'BD Top Producer Updated';
  bpt8.Modified_on__c= a.LastModifiedDate;
  bpt8.Modified_By__c=a.OwnerId;
  bpt8.Comments__c='Value changed from '+Trigger.old[0].BD_Top_Producer__c+' to '+ a.BD_Top_Producer__c;
  bpt8.Business_Type__c='BD Top Producer';
  bpt8.Contact__c = a.id;
  bptlist.add(bpt8);
 }
 //commented by snehal on 29th june2012//
 /*if(a.Top_REIT_Producer__c != oldvalues.Top_REIT_Producer__c)
 {
  BusinessProfileContactTracking__c bpt27 = new BusinessProfileContactTracking__c();
  bpt27.name= 'Top REIT Producer Updated';
  bpt27.Modified_on__c= a.LastModifiedDate;
  bpt27.Modified_By__c=a.OwnerId;
  bpt27.Comments__c='Value changed from '+Trigger.old[0].Top_REIT_Producer__c+' to '+ a.Top_REIT_Producer__c;
  bpt27.Business_Type__c='Top REIT Producer';
  bpt27.Contact__c = a.id;
  bptlist.add(bpt27);
 }*/
 if(a.Fee_Based_Non_Commission__c != oldvalues.Fee_Based_Non_Commission__c)
 {
  BusinessProfileContactTracking__c bpt9 = new BusinessProfileContactTracking__c();
  bpt9.name= 'Fee Based (Non Commission) Updated';
  bpt9.Modified_on__c= a.LastModifiedDate;
  bpt9.Modified_By__c=a.OwnerId;
  bpt9.Comments__c='Value changed from '+Trigger.old[0].Fee_Based_Non_Commission__c+' to '+ a.Fee_Based_Non_Commission__c;
  bpt9.Business_Type__c='Fee Based (Non Commission)';
  bpt9.Contact__c = a.id;
  bptlist.add(bpt9);
 }
 if(a.Business_Stage__c != oldvalues.Business_Stage__c)
 {
  BusinessProfileContactTracking__c bpt10 = new BusinessProfileContactTracking__c();
  bpt10.name= 'Business Stage Updated';
  bpt10.Modified_on__c= a.LastModifiedDate;
  bpt10.Modified_By__c=a.OwnerId;
  bpt10.Comments__c='Value changed from '+Trigger.old[0].Business_Stage__c+' to '+ a.Business_Stage__c;
  bpt10.Business_Type__c='Business Stage';
  bpt10.Contact__c = a.id;
  bptlist.add(bpt10);
 }
 /*if(a.Other_Business_Stage__c != oldvalues.Other_Business_Stage__c)
 {
  BusinessProfileContactTracking__c bpt11 = new BusinessProfileContactTracking__c();
  bpt11.name= 'Other Business Stage Updated';
  bpt11.Modified_on__c= a.LastModifiedDate;
  bpt11.Modified_By__c=a.OwnerId;
  bpt11.Comments__c='Value changed from '+Trigger.old[0].Other_Business_Stage__c+' to '+ a.Other_Business_Stage__c;
  bpt11.Business_Type__c='Other Business Stage';
  bpt11.Contact__c = a.id;
  bptlist.add(bpt11);
 }*/
 if(a.How_Can_We_Help__c !=oldvalues.How_Can_We_Help__c)
 {
  BusinessProfileContactTracking__c bpt12 = new BusinessProfileContactTracking__c();
  bpt12.name= 'How Can We Help Updated';
  bpt12.Modified_on__c= a.LastModifiedDate;
  bpt12.Modified_By__c=a.OwnerId;
  bpt12.Comments__c='Value changed from '+Trigger.old[0].How_Can_We_Help__c+' to '+ a.How_Can_We_Help__c;
  bpt12.Business_Type__c='How Can We Help?';
  bpt12.Contact__c = a.id;
  bptlist.add(bpt12);
 }
 if(a.Income_Products_Used__c != oldvalues.Income_Products_Used__c)
 {
  BusinessProfileContactTracking__c bpt14 = new BusinessProfileContactTracking__c();
  bpt14.name= 'Income Products Used Updated';
  bpt14.Modified_on__c= a.LastModifiedDate;
  bpt14.Modified_By__c=a.OwnerId;
  bpt14.Comments__c='Value changed from '+Trigger.old[0].Income_Products_Used__c+' to '+ a.Income_Products_Used__c;
  bpt14.Business_Type__c='Income Products Used';
  bpt14.Contact__c = a.id;
  bptlist.add(bpt14);
 }
 //  ** commented by Priya for New Requirement  - Start
 /*if(a.TOP5_ETF__c != oldvalues.TOP5_ETF__c)
 {
     system.debug('You just updated me...ETF');
  BusinessProfileContactTracking__c bpt13 = new BusinessProfileContactTracking__c();
  bpt13.name= 'ETF Updated';
  bpt13.Modified_on__c= a.LastModifiedDate;
  bpt13.Modified_By__c=a.OwnerId;
  bpt13.Comments__c='Value changed from '+Trigger.old[0].TOP5_ETF__c+' to '+ a.TOP5_ETF__c;
  bpt13.Business_Type__c='ETF';
  bpt13.Contact__c = a.id;
  bptlist.add(bpt13);
 }
 if(a.TOP5_REIT__c != oldvalues.TOP5_REIT__c)
 {
  BusinessProfileContactTracking__c bpt14 = new BusinessProfileContactTracking__c();
  bpt14.name= 'REIT Updated';
  bpt14.Modified_on__c= a.LastModifiedDate;
  bpt14.Modified_By__c=a.OwnerId;
  bpt14.Comments__c='Value changed from '+Trigger.old[0].TOP5_REIT__c+' to '+ a.TOP5_REIT__c;
  bpt14.Business_Type__c='REIT';
  bpt14.Contact__c = a.id;
  bptlist.add(bpt14);
 }
 if(a.TOP5_SMA__c != oldvalues.TOP5_SMA__c)
 {
  BusinessProfileContactTracking__c bpt15 = new BusinessProfileContactTracking__c();
  bpt15.name= 'SMA Updated';
  bpt15.Modified_on__c= a.LastModifiedDate;
  bpt15.Modified_By__c=a.OwnerId;
  bpt15.Comments__c='Value changed from '+Trigger.old[0].TOP5_SMA__c+' to '+ a.TOP5_SMA__c;
  bpt15.Business_Type__c='SMA';
  bpt15.Contact__c = a.id;
  bptlist.add(bpt15);
 }
 if(a.Hedge_Funds__c != oldvalues.Hedge_Funds__c)
 {
  BusinessProfileContactTracking__c bpt16 = new BusinessProfileContactTracking__c();
  bpt16.name= 'Hedge Fund Updated';
  bpt16.Modified_on__c= a.LastModifiedDate;
  bpt16.Modified_By__c=a.OwnerId;
  bpt16.Comments__c='Value changed from '+Trigger.old[0].Hedge_Funds__c+' to '+ a.Hedge_Funds__c;
  bpt16.Business_Type__c='Hedge Fund';
  bpt16.Contact__c = a.id;
  bptlist.add(bpt16);
 }
 if(a.TOP5_Actively_Managed_Funds__c != oldvalues.TOP5_Actively_Managed_Funds__c)
 {
  BusinessProfileContactTracking__c bpt17 = new BusinessProfileContactTracking__c();
  bpt17.name= 'Actively Managed Funds Updated';
  bpt17.Modified_on__c= a.LastModifiedDate;
  bpt17.Modified_By__c=a.OwnerId;
  bpt17.Comments__c='Value changed from '+Trigger.old[0].TOP5_Actively_Managed_Funds__c+' to '+ a.TOP5_Actively_Managed_Funds__c;
  bpt17.Business_Type__c='Actively Managed Funds';
  bpt17.Contact__c = a.id;
  bptlist.add(bpt17);
 }
 if(a.TOP5_Independent_Stocks_and_Bonds__c != oldvalues.TOP5_Independent_Stocks_and_Bonds__c)
 {
  BusinessProfileContactTracking__c bpt18 = new BusinessProfileContactTracking__c();
  bpt18.name= 'Independent Stocks and Bonds Updated';
  bpt18.Modified_on__c= a.LastModifiedDate;
  bpt18.Modified_By__c=a.OwnerId;
  bpt18.Comments__c='Value changed from '+Trigger.old[0].TOP5_Independent_Stocks_and_Bonds__c+' to '+ a.TOP5_Independent_Stocks_and_Bonds__c;
  bpt18.Business_Type__c='Independent Stocks and Bonds';
  bpt18.Contact__c = a.id;
  bptlist.add(bpt18);
 }
 if(a.Limited_Partnership__c != oldvalues.Limited_Partnership__c)
 {
  BusinessProfileContactTracking__c bpt19 = new BusinessProfileContactTracking__c();
  bpt19.name= 'Limited Partnership Updated';
  bpt19.Modified_on__c= a.LastModifiedDate;
  bpt19.Modified_By__c=a.OwnerId;
  bpt19.Comments__c='Value changed from '+Trigger.old[0].Limited_Partnership__c+' to '+ a.Limited_Partnership__c;
  bpt19.Business_Type__c='Limited Partnership';
  bpt19.Contact__c = a.id;
  bptlist.add(bpt19);
 }
 if(a.Used_Nonlisted_REIT_before__c != oldvalues.Used_Nonlisted_REIT_before__c)
 {
  BusinessProfileContactTracking__c bpt1 = new BusinessProfileContactTracking__c();
  bpt1.name= 'Used REIT Before Updated';
  bpt1.Modified_on__c= a.LastModifiedDate;
  bpt1.Modified_By__c=a.OwnerId;
  bpt1.Comments__c='Value changed from '+Trigger.old[0].Used_Nonlisted_REIT_before__c+' to '+ a.Used_Nonlisted_REIT_before__c;
  bpt1.Business_Type__c='Used Nonlisted REIT before?';
  bpt1.Contact__c = a.id;
  bptlist.add(bpt1);
 }
 if(a.How_Long__c !=oldvalues.How_Long__c)
 {
  BusinessProfileContactTracking__c bpt21 = new BusinessProfileContactTracking__c();
  bpt21.name= 'How Long Updated';
  bpt21.Modified_on__c= a.LastModifiedDate;
  bpt21.Modified_By__c=a.OwnerId;
  bpt21.Comments__c='Value changed from '+Trigger.old[0].How_Long__c+' to '+ a.How_Long__c;
  bpt21.Business_Type__c='How Long?';
  bpt21.Contact__c = a.id;
  bptlist.add(bpt21);
 }*/
 // **** End ****
 if(a.What_REITs__c != oldvalues.What_REITs__c)
 {
  BusinessProfileContactTracking__c bpt22 = new BusinessProfileContactTracking__c();
  bpt22.name= 'What REITs Updated';
  bpt22.Modified_on__c= a.LastModifiedDate;
  bpt22.Modified_By__c=a.OwnerId;
  bpt22.Comments__c='Value changed from '+Trigger.old[0].What_REITs__c+' to '+ a.What_REITs__c;
  bpt22.Business_Type__c='What REITs?';
  bpt22.Contact__c = a.id;
  bptlist.add(bpt22);
 }
 if(a.Would_you_invest__c != oldvalues.Would_you_invest__c)
 {
  BusinessProfileContactTracking__c bpt23 = new BusinessProfileContactTracking__c();
  bpt23.name= 'Would You Invest Updated';
  bpt23.Modified_on__c= a.LastModifiedDate;
  bpt23.Modified_By__c=a.OwnerId;
  bpt23.Comments__c='Value changed from '+Trigger.old[0].Would_you_invest__c+' to '+ a.Would_you_invest__c;
  bpt23.Business_Type__c='Would You Invest?';
  bpt23.Contact__c = a.id;
  bptlist.add(bpt23);
 }
 if(a.Inv_Why_Not__c != oldvalues.Inv_Why_Not__c)
 {
  BusinessProfileContactTracking__c bpt24 = new BusinessProfileContactTracking__c();
  bpt24.name= 'WHy Not Updated';
  bpt24.Modified_on__c= a.LastModifiedDate;
  bpt24.Modified_By__c=a.OwnerId;
  bpt24.Comments__c='Value changed from '+Trigger.old[0].Inv_Why_Not__c+' to '+ a.Inv_Why_Not__c;
  bpt24.Business_Type__c='WHy Not?';
  bpt24.Contact__c = a.id;
  bptlist.add(bpt24);
 }
 if(a.What_Changes_in_Volitilaty__c != oldvalues.What_Changes_in_Volitilaty__c)
 {
  BusinessProfileContactTracking__c bpt25 = new BusinessProfileContactTracking__c();
  bpt25.name= 'Changes in Volitality Updated';
  bpt25.Modified_on__c= a.LastModifiedDate;
  bpt25.Modified_By__c=a.OwnerId;
  bpt25.Comments__c='Value changed from '+Trigger.old[0].What_Changes_in_Volitilaty__c+' to '+ a.What_Changes_in_Volitilaty__c;
  bpt25.Business_Type__c='Changes in Volitality';
  bpt25.Contact__c = a.id;
  bptlist.add(bpt25);
 } 
 //commented by snehal on 29th june2012//
 
/* if(a.Business_Profile__c !=oldvalues.Business_Profile__c)
 {
  BusinessProfileContactTracking__c bpt26 = new BusinessProfileContactTracking__c();
  bpt26.name= 'Business Profile Updated';
  bpt26.Modified_on__c= a.LastModifiedDate;
  bpt26.Modified_By__c=a.OwnerId;
  bpt26.Comments__c='Value changed from '+Trigger.old[0].Business_Profile__c+' to '+ a.Business_Profile__c;
  bpt26.Business_Type__c='Business Profile';
  bpt26.Contact__c = a.id;
  bptlist.add(bpt26);
 
 }
 if(a.Product_Profile__c !=oldvalues.Product_Profile__c)
 {
  BusinessProfileContactTracking__c bpt28 = new BusinessProfileContactTracking__c();
  bpt28.name= 'Product Profile Updated';
  bpt28.Modified_on__c= a.LastModifiedDate;
  bpt28.Modified_By__c=a.OwnerId;
  bpt28.Comments__c='Value changed from '+Trigger.old[0].Product_Profile__c+' to '+ a.Product_Profile__c;
  bpt28.Business_Type__c='Product Profile';
  bpt28.Contact__c = a.id;
  bptlist.add(bpt28);
 
 }
 if(a.Client_Profile__c !=oldvalues.Client_Profile__c)
 {
  BusinessProfileContactTracking__c bpt29 = new BusinessProfileContactTracking__c();
  bpt29.name= 'Client Profile Updated';
  bpt29.Modified_on__c= a.LastModifiedDate;
  bpt29.Modified_By__c=a.OwnerId;
  bpt29.Comments__c='Value changed from '+Trigger.old[0].Client_Profile__c+' to '+ a.Client_Profile__c;
  bpt29.Business_Type__c='Client Profile';
  bpt29.Contact__c = a.id;
  bptlist.add(bpt29);
 
 }*/
 if(a.Alternative_Investments_Firm_Provided__c !=oldvalues.Alternative_Investments_Firm_Provided__c)
 {
  BusinessProfileContactTracking__c bpt30 = new BusinessProfileContactTracking__c();
  bpt30.name= 'Alternative Investments(Firm Provided) Updated';
  bpt30.Modified_on__c= a.LastModifiedDate;
  bpt30.Modified_By__c=a.OwnerId;
  bpt30.Comments__c='Value changed from '+Trigger.old[0].Alternative_Investments_Firm_Provided__c+' to '+ a.Alternative_Investments_Firm_Provided__c;
  bpt30.Business_Type__c='Alternative Investments(Firm Provided)';
  bpt30.Contact__c = a.id;
  bptlist.add(bpt30);
 
 }
 if(a.Total_Sales_Last_Year__c !=oldvalues.Total_Sales_Last_Year__c)
 {
  BusinessProfileContactTracking__c bpt31 = new BusinessProfileContactTracking__c();
  bpt31.name= 'Total Sales Last Year Updated';
  bpt31.Modified_on__c= a.LastModifiedDate;
  bpt31.Modified_By__c=a.OwnerId;
  bpt31.Comments__c='Value changed from '+Trigger.old[0].Total_Sales_Last_Year__c+' to '+ a.Total_Sales_Last_Year__c;
  bpt31.Business_Type__c='Total Sales Last Year Profile';
  bpt31.Contact__c = a.id;
  bptlist.add(bpt31);
 
 }
 /*if(a.Other_Alternate_Investments__c !=oldvalues.Other_Alternate_Investments__c)
 {
  BusinessProfileContactTracking__c bpt32 = new BusinessProfileContactTracking__c();
  bpt32.name= 'Other Alternate Investments Updated';
  bpt32.Modified_on__c= a.LastModifiedDate;
  bpt32.Modified_By__c=a.OwnerId;
  bpt32.Comments__c='Value changed from '+Trigger.old[0].Other_Alternate_Investments__c+' to '+ a.Other_Alternate_Investments__c;
  bpt32.Business_Type__c='Other Alternate Investments';
  bpt32.Contact__c = a.id;
  bptlist.add(bpt32);
 
 }*/
 if(a.Bus_Size_RE_Exposure_Through__c !=oldvalues.Bus_Size_RE_Exposure_Through__c)
 {
  BusinessProfileContactTracking__c bpt33 = new BusinessProfileContactTracking__c();
  bpt33.name= 'RE Exposure Through Updated';
  bpt33.Modified_on__c= a.LastModifiedDate;
  bpt33.Modified_By__c=a.OwnerId;
  bpt33.Comments__c='Value changed from '+Trigger.old[0].Bus_Size_RE_Exposure_Through__c+' to '+ a.Bus_Size_RE_Exposure_Through__c;
  bpt33.Business_Type__c='RE Exposure Through';
  bpt33.Contact__c = a.id;
  bptlist.add(bpt33);
  }
 if(a.How_would_you_describe_your_client_base__c!=oldvalues.How_would_you_describe_your_client_base__c)
 {
  BusinessProfileContactTracking__c bpt34 = new BusinessProfileContactTracking__c();
  bpt34.name= 'How_would_you_describe_your_client_base__c Updated';
  bpt34.Modified_on__c= a.LastModifiedDate;
  bpt34.Modified_By__c=a.OwnerId;
  bpt34.Comments__c='Value changed from '+Trigger.old[0].How_would_you_describe_your_client_base__c +' to '+ a.How_would_you_describe_your_client_base__c;
  bpt34.Business_Type__c='How would you describe your client base?';
  bpt34.Contact__c = a.id;
  bptlist.add(bpt34);
  }
 
}
}
else if(Trigger.isinsert){
if(helper.run == TRUE)
{

  /*BusinessProfileContactTracking__c inbpt = new BusinessProfileContactTracking__c();
  inbpt.name= 'Used REIT Before Created';
  inbpt.Modified_on__c= a.LastModifiedDate;
  inbpt.Modified_By__c= a.OwnerId;
  inbpt.Comments__c='Used REIT Before field Created : ' + a.Used_Nonlisted_REIT_before__c;
  inbpt.Business_Type__c='Used Nonlisted REIT before?';
  inbpt.Contact__c = a.id;
  bptlist1.add(inbpt);*/
  
  BusinessProfileContactTracking__c inbpt1 = new BusinessProfileContactTracking__c();
  inbpt1.name= 'Income Products Used Created';
  inbpt1.Modified_on__c= a.LastModifiedDate;
  inbpt1.Modified_By__c= a.OwnerId;
  inbpt1.Comments__c='Income Products Used created : ' + a.Income_Products_Used__c;
  inbpt1.Business_Type__c='Income Products Used';
  inbpt1.Contact__c = a.id;
  bptlist1.add(inbpt1);
  
  BusinessProfileContactTracking__c inbpt2 = new BusinessProfileContactTracking__c();
  inbpt2.name= 'Total AUM Created';
  inbpt2.Modified_on__c= a.LastModifiedDate;
  inbpt2.Modified_By__c= a.OwnerId;
  inbpt2.Comments__c='Total AUM field created : ' + a.BusSize_Total_AUM__c;
  inbpt2.Business_Type__c='Total AUM';
  inbpt2.Contact__c = a.id;
  bptlist1.add(inbpt2);
  
  BusinessProfileContactTracking__c inbpt3 = new BusinessProfileContactTracking__c();
  inbpt3.name= 'Clients with RE in Portfolio Created';
  inbpt3.Modified_on__c= a.LastModifiedDate;
  inbpt3.Modified_By__c= a.OwnerId;
  inbpt3.Comments__c='Clients with RE in Portfolio field created : ' + a.TOP5_of_Clients_with_RE_in_Portfolio__c;
  inbpt3.Business_Type__c='% of Clients with RE in Portfolio';
  inbpt3.Contact__c = a.id;
  bptlist1.add(inbpt3);
  
  /*BusinessProfileContactTracking__c inbpt4 = new BusinessProfileContactTracking__c();
  inbpt4.name= 'Other RE Exposure Created';
  inbpt4.Modified_on__c= a.LastModifiedDate;
  inbpt4.Modified_By__c= a.OwnerId;
  inbpt4.Comments__c='Other RE Exposure field created : ' + a.Other_RE_Exposure__c;
  inbpt4.Business_Type__c='Other RE Exposure';
  inbpt4.Contact__c = a.id;
  bptlist1.add(inbpt4);*/
  
  BusinessProfileContactTracking__c inbpt5 = new BusinessProfileContactTracking__c();
  inbpt5.name= 'Allocated to RE';
  inbpt5.Modified_on__c= a.LastModifiedDate;
  inbpt5.Modified_By__c= a.OwnerId;
  inbpt5.Comments__c='Allocated to RE field created : ' + a.Bus_Size_Allocated_to_RE__c;
  inbpt5.Business_Type__c='Allocated to RE';
  inbpt5.Contact__c = a.id;
  bptlist.add(inbpt5);
  
  BusinessProfileContactTracking__c inbpt7 = new BusinessProfileContactTracking__c();
  inbpt7.name= 'Fee Based (Non Commission) Created';
  inbpt7.Modified_on__c= a.LastModifiedDate;
  inbpt7.Modified_By__c= a.OwnerId;
  inbpt7.Comments__c='Fee Based (Non Commission) Based field created : ' + a.Fee_Based_Non_Commission__c;
  inbpt7.Business_Type__c='Fee Based (Non Commission)';
  inbpt7.Contact__c = a.id;
  bptlist.add(inbpt7);
  
  BusinessProfileContactTracking__c inbpt8 = new BusinessProfileContactTracking__c();
  inbpt8.name= 'Business Stage Created';
  inbpt8.Modified_on__c= a.LastModifiedDate;
  inbpt8.Modified_By__c= a.OwnerId;
  inbpt8.Comments__c='Business Stage field created : ' + a.Business_Stage__c;
  inbpt8.Business_Type__c='Business Stage';
  inbpt8.Contact__c = a.id;
  bptlist1.add(inbpt8);
  
  /*BusinessProfileContactTracking__c inbpt9 = new BusinessProfileContactTracking__c();
  inbpt9.name= 'Other Business Stage Created';
  inbpt9.Modified_on__c= a.LastModifiedDate;
  inbpt9.Modified_By__c= a.OwnerId;
  inbpt9.Comments__c='Other Business Stage field created : ' + a.Other_Business_Stage__c;
  inbpt9.Business_Type__c='Other Business Stage';
  inbpt9.Contact__c = a.id;
  bptlist.add(inbpt9);*/
  
  BusinessProfileContactTracking__c inbpt10 = new BusinessProfileContactTracking__c();
  inbpt10.name= 'How Can We Help Created';
  inbpt10.Modified_on__c= a.LastModifiedDate;
  inbpt10.Modified_By__c= a.OwnerId;
  inbpt10.Comments__c='How Can We Help field created : ' + a.How_Can_We_Help__c;
  inbpt10.Business_Type__c='How Can We Help?';
  inbpt10.Contact__c = a.id;
  bptlist1.add(inbpt10);
  
  /*BusinessProfileContactTracking__c inbpt11 = new BusinessProfileContactTracking__c();
  inbpt11.name= 'ETF Created';
  inbpt11.Modified_on__c= a.LastModifiedDate;
  inbpt11.Modified_By__c= a.OwnerId;
  inbpt11.Comments__c='ETF field created : ' + a.TOP5_ETF__c;
  inbpt11.Business_Type__c='ETF';
  inbpt11.Contact__c = a.id;
  bptlist1.add(inbpt11);
  
  BusinessProfileContactTracking__c inbpt12 = new BusinessProfileContactTracking__c();
  inbpt12.name= 'REIT Created';
  inbpt12.Modified_on__c= a.LastModifiedDate;
  inbpt12.Modified_By__c= a.OwnerId;
  inbpt12.Comments__c='REIT field created : ' + a.TOP5_REIT__c;
  inbpt12.Business_Type__c='REIT';
  inbpt12.Contact__c = a.id;
  bptlist1.add(inbpt12);
  
  BusinessProfileContactTracking__c inbpt13 = new BusinessProfileContactTracking__c();
  inbpt13.name= 'SMA Created';
  inbpt13.Modified_on__c= a.LastModifiedDate;
  inbpt13.Modified_By__c= a.OwnerId;
  inbpt13.Comments__c='SMA field created : ' + a.TOP5_SMA__c;
  inbpt13.Business_Type__c='SMA';
  inbpt13.Contact__c = a.id;
  bptlist1.add(inbpt13);
  
  BusinessProfileContactTracking__c inbpt14 = new BusinessProfileContactTracking__c();
  inbpt14.name= 'Hedge Fund Created';
  inbpt14.Modified_on__c= a.LastModifiedDate;
  inbpt14.Modified_By__c= a.OwnerId;
  inbpt14.Comments__c='Hedge Funds field created : ' + a.Hedge_Funds__c;
  inbpt14.Business_Type__c='Hedge Fund';
  inbpt14.Contact__c = a.id;
  bptlist1.add(inbpt14);
  
  BusinessProfileContactTracking__c inbpt15 = new BusinessProfileContactTracking__c();
  inbpt15.name= 'Actively Managed Funds Created';
  inbpt15.Modified_on__c= a.LastModifiedDate;
  inbpt15.Modified_By__c= a.OwnerId;
  inbpt15.Comments__c='Actively Managed Funds field created : ' + a.TOP5_Actively_Managed_Funds__c;
  inbpt15.Business_Type__c='Actively Managed Funds';
  inbpt15.Contact__c = a.id;
  bptlist1.add(inbpt15);
  
  BusinessProfileContactTracking__c inbpt16 = new BusinessProfileContactTracking__c();
  inbpt16.name= 'Independent Stocks and Bonds Created';
  inbpt16.Modified_on__c= a.LastModifiedDate;
  inbpt16.Modified_By__c= a.OwnerId;
  inbpt16.Comments__c='Independent Stocks and Bonds field created : ' + a.TOP5_Independent_Stocks_and_Bonds__c;
  inbpt16.Business_Type__c='Independent Stocks and Bonds';
  inbpt16.Contact__c = a.id;
  bptlist1.add(inbpt16);
  
  BusinessProfileContactTracking__c inbpt17 = new BusinessProfileContactTracking__c();
  inbpt17.name= 'Limited Partnership  Created';
  inbpt17.Modified_on__c= a.LastModifiedDate;
  inbpt17.Modified_By__c= a.OwnerId;
  inbpt17.Comments__c='Limited Partnership  field created : ' + a.Limited_Partnership__c;
  inbpt17.Business_Type__c='Limited Partnership ';
  inbpt17.Contact__c = a.id;
  bptlist1.add(inbpt17);
  
  BusinessProfileContactTracking__c inbpt18 = new BusinessProfileContactTracking__c();
  inbpt18.name= 'Other Alternate Investments Created';
  inbpt18.Modified_on__c= a.LastModifiedDate;
  inbpt18.Modified_By__c= a.OwnerId;
  inbpt18.Comments__c='Other Alternate Investments field created : ' + a.Acct_Other_Alternative_Investments__c;
  inbpt18.Business_Type__c='Other Alternate Investments';
  inbpt18.Contact__c = a.id;
  bptlist1.add(inbpt18);
  
  BusinessProfileContactTracking__c inbpt19 = new BusinessProfileContactTracking__c();
  inbpt19.name= 'How Long Created';
  inbpt19.Modified_on__c= a.LastModifiedDate;
  inbpt19.Modified_By__c= a.OwnerId;
  inbpt19.Comments__c='How Long? field created : ' + a.How_Long__c;
  inbpt19.Business_Type__c='How Long?';
  inbpt19.Contact__c = a.id;
  bptlist1.add(inbpt19);*/
  
  BusinessProfileContactTracking__c inbpt20 = new BusinessProfileContactTracking__c();
  inbpt20.name= 'What REITs Created';
  inbpt20.Modified_on__c= a.LastModifiedDate;
  inbpt20.Modified_By__c= a.OwnerId;
  inbpt20.Comments__c='What REITs field created : ' + a.What_REITs__c;
  inbpt20.Business_Type__c='What REITs?';
  inbpt20.Contact__c = a.id;
  bptlist1.add(inbpt20);
  
  BusinessProfileContactTracking__c inbpt21 = new BusinessProfileContactTracking__c();
  inbpt21.name= 'Would You Invest Created';
  inbpt21.Modified_on__c= a.LastModifiedDate;
  inbpt21.Modified_By__c= a.OwnerId;
  inbpt21.Comments__c='Would You Invest field created : ' + a.Would_you_invest__c;
  inbpt21.Business_Type__c='Would You Invest?';
  inbpt21.Contact__c = a.id;
  bptlist1.add(inbpt21);
  
  BusinessProfileContactTracking__c inbpt22 = new BusinessProfileContactTracking__c();
  inbpt22.name= 'WHy Not Created';
  inbpt22.Modified_on__c= a.LastModifiedDate;
  inbpt22.Modified_By__c= a.OwnerId;
  inbpt22.Comments__c='WHy Not field created : ' + a.Inv_Why_Not__c;
  inbpt22.Business_Type__c='WHy Not?';
  inbpt22.Contact__c = a.id;
  bptlist1.add(inbpt22);
  
  BusinessProfileContactTracking__c inbpt23 = new BusinessProfileContactTracking__c();
  inbpt23.name= 'Changes in Volitality Created';
  inbpt23.Modified_on__c= a.LastModifiedDate;
  inbpt23.Modified_By__c= a.OwnerId;
  inbpt23.Comments__c='Changes in Volitality field created : ' + a.What_Changes_in_Volitilaty__c;
  inbpt23.Business_Type__c='Changes in Volitality';
  inbpt23.Contact__c = a.id;
  bptlist1.add(inbpt23);
  //commented by snehal on 29th june//
  /*BusinessProfileContactTracking__c inbpt24 = new BusinessProfileContactTracking__c();
  inbpt24.name= 'Business Profile Created';
  inbpt24.Modified_on__c= a.LastModifiedDate;
  inbpt24.Modified_By__c= a.OwnerId;
  inbpt24.Comments__c='Business Profile field created : ' + a.Business_Profile__c;
  inbpt24.Business_Type__c='Business Profile';
  inbpt24.Contact__c = a.id;
  bptlist1.add(inbpt24);
  
  BusinessProfileContactTracking__c inbpt25 = new BusinessProfileContactTracking__c();
  inbpt25.name= 'Product Profile Created';
  inbpt25.Modified_on__c= a.LastModifiedDate;
  inbpt25.Modified_By__c= a.OwnerId;
  inbpt25.Comments__c='Product Profile field created : ' + a.Product_Profile__c;
  inbpt25.Business_Type__c='Product Profile';
  inbpt25.Contact__c = a.id;
  bptlist1.add(inbpt25);
  
  BusinessProfileContactTracking__c inbpt26 = new BusinessProfileContactTracking__c();
  inbpt26.name= 'Client Profile Created';
  inbpt26.Modified_on__c= a.LastModifiedDate;
  inbpt26.Modified_By__c= a.OwnerId;
  inbpt26.Comments__c='Client Profile field created : ' + a.Client_Profile__c;
  inbpt26.Business_Type__c='Client Profile';
  inbpt26.Contact__c = a.id;
  bptlist1.add(inbpt26);*/
  
  BusinessProfileContactTracking__c inbpt27 = new BusinessProfileContactTracking__c();
  inbpt27.name= 'BD Top Producer Created';
  inbpt27.Modified_on__c= a.LastModifiedDate;
  inbpt27.Modified_By__c= a.OwnerId;
  inbpt27.Comments__c='BD Top Producer field created : ' + a.BD_Top_Producer__c;
  inbpt27.Business_Type__c='BD Top Producer';
  inbpt27.Contact__c = a.id;
  bptlist1.add(inbpt27);
  //commented by snehal on 29th june//
  /*BusinessProfileContactTracking__c inbpt28 = new BusinessProfileContactTracking__c();
  inbpt28.name= 'Top REIT Producer Created';
  inbpt28.Modified_on__c= a.LastModifiedDate;
  inbpt28.Modified_By__c= a.OwnerId;
  inbpt28.Comments__c='Top REIT Producer field created : ' + a.Top_REIT_Producer__c;
  inbpt28.Business_Type__c='Top REIT Producer';
  inbpt28.Contact__c = a.id;
  bptlist1.add(inbpt28);*/
  
  BusinessProfileContactTracking__c inbpt29 = new BusinessProfileContactTracking__c();
  inbpt29.name= 'Alternative Investments(Firm Provided) Created';
  inbpt29.Modified_on__c= a.LastModifiedDate;
  inbpt29.Modified_By__c= a.OwnerId;
  inbpt29.Comments__c='Top REIT Producer field created : ' + a.Alternative_Investments_Firm_Provided__c;
  inbpt29.Business_Type__c='Alternative Investments(Firm Provided)';
  inbpt29.Contact__c = a.id;
  bptlist1.add(inbpt29);
  
  BusinessProfileContactTracking__c inbpt30 = new BusinessProfileContactTracking__c();
  inbpt30.name= 'Total Sales Last Year Created';
  inbpt30.Modified_on__c= a.LastModifiedDate;
  inbpt30.Modified_By__c= a.OwnerId;
  inbpt30.Comments__c='Top REIT Producer field created : ' + a.Total_Sales_Last_Year__c;
  inbpt30.Business_Type__c='Total Sales Last Year';
  inbpt30.Contact__c = a.id;
  bptlist1.add(inbpt30);
  
  /*BusinessProfileContactTracking__c inbpt31 = new BusinessProfileContactTracking__c();
  inbpt31.name= 'Other Alternate Investments Created';
  inbpt31.Modified_on__c= a.LastModifiedDate;
  inbpt31.Modified_By__c= a.OwnerId;
  inbpt31.Comments__c='Top REIT Producer field created : ' + a.Other_Alternate_Investments__c;
  inbpt31.Business_Type__c='Other Alternate Investments';
  inbpt31.Contact__c = a.id;
  bptlist1.add(inbpt31);*/
  
  BusinessProfileContactTracking__c inbpt32 = new BusinessProfileContactTracking__c();
  inbpt32.name= 'RE Exposure Through Created';
  inbpt32.Modified_on__c= a.LastModifiedDate;
  inbpt32.Modified_By__c= a.OwnerId;
  inbpt32.Comments__c='Top REIT Producer field created : ' + a.Bus_Size_RE_Exposure_Through__c;
  inbpt32.Business_Type__c='RE Exposure Through';
  inbpt32.Contact__c = a.id;
  bptlist1.add(inbpt32);
  
  BusinessProfileContactTracking__c bpt33 = new BusinessProfileContactTracking__c();
  bpt33.name= 'How would you describe your client base Created';
  bpt33.Modified_on__c= a.LastModifiedDate;
  bpt33.Modified_By__c=a.OwnerId;
  bpt33.Comments__c='How would you describe your client base created : ' + a.How_would_you_describe_your_client_base__c;
  bpt33.Business_Type__c='How would you describe your client base?';
  bpt33.Contact__c = a.id;
  bptlist.add(bpt33);
  
}
}
}
  insert bptlist;
  insert bptlist1;
}
}