trigger contactInvAttachment on Attachment (after insert) {
     Avoidrecursivecls.iscallingfrom=true;
     List<Contact_InvestorList__c> coninvlist = new List<Contact_InvestorList__c>();
      List<Contact_InvestorList__c> updateConinvlist = new List<Contact_InvestorList__c>();
     set<id> coninvids = new set<id>();
    map<id,id> attachmentmap = new map<id,id>();
    for(Attachment attobj :trigger.new){
        coninvids.add(attobj.ParentId);
        attachmentmap.put(attObj.parentid,attObj.id);
    }
    set<id> contids = new set<id>();
    list<contact> conlist = new list<contact>();
    for(Contact_InvestorList__c coninvobj : [select id,Send_Pdoc_complete__c,Fund__c,Pdoc__c ,Pdoc_Ready__c,Attachment_Id__c,Advisor_Name__c from Contact_InvestorList__c where id in :coninvids]){
        system.debug('mapresult'+(attachmentmap.get(coninvobj.id)));
    coninvobj.Attachment_Id__c = (attachmentmap.get(coninvobj.id));
    coninvobj.Pdoc__c = false;
    
   
    coninvlist.add(coninvobj);
    contids.add(coninvobj.Advisor_Name__c);
    }   
   if(coninvlist!=null && coninvlist.size()>0){
   for(contact conobj : [select id,Pdoc_Ready__c from contact where id in :contids and Pdoc_Ready__c = FALSE]){
       conobj.Pdoc_Ready__c = true;
       conlist.add(conobj);
   }
       update  coninvlist;
       update conlist;
    for(Contact_InvestorList__c coninv : [select id,Send_Pdoc_complete__c,Pdoc_Ready__c from Contact_InvestorList__c where id in :coninvids ]){
    if(coninv.Pdoc_Ready__c == true){
        system.debug('enter');
        coninv.Send_Pdoc_complete__c = false;
        updateConinvlist.add(coninv);
    }
    }
    if(updateConinvlist!=null && updateConinvlist.size()>0){
        update updateConinvlist;
    }
    
    }

}