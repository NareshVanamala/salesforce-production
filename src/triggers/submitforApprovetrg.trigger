trigger submitforApprovetrg on Asset__c (after update) 
{
  List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();
  for(Asset__c a:Trigger.new){
       if(a.Termination_Status__c == 'In Process' && a.Application_Implementor__c != null)
          {
            // create the new approval request to submit    
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();   
            //req.setComments('Submitted for approval. Please approve.');
            req.setObjectId(a.Id);
            approvalReqList.add(req);        
          }
      }
  
    // submit the approval request for processing        
    List<Approval.ProcessResult> resultList = Approval.process(approvalReqList);        
    // display if the reqeust was successful

    for(Approval.ProcessResult result: resultList ){        
             System.debug('--------Submitted for approval successfully:------> '+result.isSuccess()); 
             system.debug('-------Check the approvalhistory-----------> '+result);    
            }
  
}