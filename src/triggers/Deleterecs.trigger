trigger Deleterecs on Business_Profile_Tracking__c (after insert) {

For(Business_Profile_Tracking__c bpt : trigger.new)
{
List<Business_Profile_Tracking__c> deletelst = new List<Business_Profile_Tracking__c>();

//deletelst = [Select Name, Id from Business_Profile_Tracking__c where Name Like :bpt.Name and Account__c=:bpt.Account__c order By CreatedDate desc];
deletelst = [Select Name, Id, Business_Type__c from Business_Profile_Tracking__c where Business_Type__c Like :bpt.Business_Type__c and Account__c=:bpt.Account__c order By CreatedDate desc];
deletelst.remove(0);

 delete deletelst;

}

}