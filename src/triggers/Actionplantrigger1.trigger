trigger Actionplantrigger1 on Template__c( before delete, after insert, after undelete ){

         set<ID> apIds= new set<ID>();
        set<ID> apttIds= new set<ID>();
       //set<String> apttIds = new set<String>();
        set<ID> taskIds= new set<ID>();
        List<Task> tasks = new List<Task>();
        Set<Id> DealIdSet = new Set<Id>();
        Set<Id> LoanIdSet = new Set<Id>();
    //Execution when a Action Plan is Deleted, delete all asociated Task
    if( trigger.isDelete && trigger.isBefore ) {
      if ( !ProcessorControl1.inBatchContext){
          // if action plan is deleted, delete all tasks associated with it
          for( Template__c ap : trigger.old ){
              apIds.add( ap.id);
              if(ap.Deal__c!=null)
              {
              DealIdSet.add(ap.Deal__c);
              }
              if(ap.Loan__c!=null)
              {
              LoanIdset.add(ap.Loan__c);
              }
              system.debug('----------PRT Test----'+ap.Deal__c);
          }
         System.debug('+++'+apIds);
  
      If(DealIdset.size()>0)
      {
      system.debug('**********'+Dealidset.size());
      for(Task_Plan__c aptt : [ select Id, CreatedDate, Query_Param__c, Template__r.Deal__c, Name from Task_Plan__c where Template__c in :apIds and Template__r.Deal__c IN:DealIdSet]){
            apttIds.add(aptt.id);
              
          } 
          
           for( Task ap : [select Subject, Id from Task where X18_digit_Id__c IN :apttIds and WhatId IN :DealIdSet]){ 
                    
             taskIds.add(ap.id);
          } 
          system.debug('@@@----'+taskids.size());
         
          Delete [select Id from Task where Id in:Taskids];
     }
      If(LoanIdset.size()>0)
      {
      for(Task_Plan__c aptt : [ select Id, CreatedDate, Query_Param__c, Template__r.Loan__c, Name from Task_Plan__c where Template__c in :apIds and Template__r.Loan__c IN:LoanIdSet]){
       
              apttIds.add(aptt.id);
              
          } 
          
            for( Task ap : [select Subject, Id from Task where X18_digit_Id__c IN : apttIds and WhatId IN :LoanIdSet]){
             taskIds.add(ap.id);
          } 
          system.debug('@@@----'+taskids.size());
         
        
     }
     Delete [select Id from Task where Id in:Taskids];
        }
       
    }
   if ( trigger.isUnDelete && trigger.isAfter ){
      Database.UndeleteResult[] unDel_errors;
      //get All Action PLan Ids
      for( Template__c ap : trigger.new ){
            apIds.add( ap.Id );
            if(ap.Deal__c!=null)
              {
              DealIdSet.add(ap.Deal__c);
              }
              if(ap.Loan__c!=null)
              {
              LoanIdset.add(ap.Loan__c);
              }
        }
      //get Id of all deleted Action Plan Tasks Templates
      If(DealIdset.size()>0)
      {
      Map<Id,Task_plan__c> map_APTasks =new Map<ID, Task_plan__c>([select  a.Id ,a.Template__c,a.Template__r.Deal__c
                          from Task_plan__c a 
                          where a.Template__c in: apIds and Template__r.Deal__c IN:DealIdSet ALL ROWS ]);
                
      //get all tasks asociated to this APTasks
      List<Task> taskList =  [select  a.Status , a.Id, a.ReminderDateTime, a.Action_Plan_Task__c ,a.X18_digit_Id__c ,a.Date_Completed__c,a.Date_Needed__c,a.Date_Ordered__c,a.Date_Received__c, a.Subject, a.isDeleted
                    from Task a 
                    where a.X18_digit_Id__c in: map_APTasks.keySet() and WhatId IN :DealIdSet and a.isDeleted = true  ALL ROWS ];  
                                                      
    if (taskList.size()>0){
      try{
        unDel_errors =Database.undelete( taskList,false);
      }catch(Exception e)
      {
        for(integer i =0; i< unDel_errors.size(); i++)
                if(unDel_errors[i].getErrors().size()>0){
                  // Process any errors here  
                    
                //todo: handle this
              
            }
      }
    } 
    }
    If(LoanIdset.size()>0)
      {
      Map<Id,Task_plan__c> map_APTasks =new Map<ID, Task_plan__c>([select  a.Id ,a.Template__c,a.Template__r.Loan__c
                          from Task_plan__c a 
                          where a.Template__c in: apIds and Template__r.Loan__c IN:LoanIdset ALL ROWS ]);
                
      //get all tasks asociated to this APTasks
      List<Task> taskList =  [select  a.Status , a.Id, a.ReminderDateTime, a.Action_Plan_Task__c ,a.X18_digit_Id__c ,a.Date_Completed__c,a.Date_Needed__c,a.Date_Ordered__c,a.Date_Received__c, a.Subject, a.isDeleted
                    from Task a 
                    where a.X18_digit_Id__c in: map_APTasks.keySet() and WhatId IN :LoanIdSet  and a.isDeleted = true  ALL ROWS ];  
                                                      
    if (taskList.size()>0){
      try{
        unDel_errors =Database.undelete( taskList,false);
      }catch(Exception e)
      {
        for(integer i =0; i< unDel_errors.size(); i++)
                if(unDel_errors[i].getErrors().size()>0){
                  // Process any errors here  
                    
                //todo: handle this
              
            }
      }
    } 
    }
    }      
   
                                   
}