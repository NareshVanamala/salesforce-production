trigger EC_DealParametersToEntity on Deal__c (before insert, after update) 
{ 
    List<Entity__c> parentEntyLst = new List<Entity__c>(); 
    List<Id> listIds = new List<Id>();
    List<Deal__c> chldDealLst = new List<Deal__c>(); 
    List<string> iteam1=new list<string>();
    List<string> iteam2=new list<string>();
    set<string> tConcept1=new set<string>();
    list<Deal__c>SMDealList=new list<Deal__c>();
    map<string,list<Deal__c>>entitytodeallist=new map<string,list<Deal__c>>();
    
    Try
    {
       for (Deal__c childObjDl : Trigger.new) 
        {
           //if((childObjDl.RecordType.Name == 'Acquisition')||(childObjDl.RecordType.Name == 'Sale_Leaseback'))
            listIds.add(childObjDl.Property_Owner_SPEL__c );
        }
        system.debug('listIds---------'+listIds); 
        
        parentEntyLst = [SELECT id, Address__c,Owned_ID__c, REIT_Affiliation__c, Team__c, Portfolio_Alias__c,Portfolio_Description__c,
                        Property_Alias__c,Property_Description__c,Tenant_Concept__c From Entity__c Where Id IN : listIds];
            
      chldDealLst = [SELECT id, Workspace_Name__c,Property_Owner_SPEL__c,Owned_Id__c,Fund__c,Team_Alias__c,Entity__c,
                      Portfolio_Alias__c,Portfolio_Description__c,Property_Alias__c,Property_Description__c,
                      Report_Deal_Name__c,Tenant_Concept_Alias__c From Deal__c Where Property_Owner_SPEL__c IN :listIds and RecordType.Name IN ('Acquisition','Sale_Leaseback') LIMIT 250 ];    
         
        system.debug('parentEntyLst---------'+parentEntyLst);   
        system.debug('chldDealLst---------'+chldDealLst);  
        
        if(chldDealLst.size()>0)
        {
          for(Deal__c  ri : chldDealLst)
          {
              if(entitytodeallist.containsKey(ri.Property_Owner_SPEL__c ))
             {
                entitytodeallist.get(ri.Property_Owner_SPEL__c).add(ri);
             }
            else
            {
              entitytodeallist.put(ri.Property_Owner_SPEL__c , new List<sobject>{ri});
            }
          }
              
       }
           
        
        system.debug('check my map'+entitytodeallist);
                      
        for(Entity__c objEnt : parentEntyLst)
        {
            system.debug('objEnt ---------'+objEnt);  
            
            list<Deal__c>deallist=new list<Deal__c>();
            deallist=entitytodeallist.get(objEnt.id);
            if(deallist.size()==1)
            {
             objEnt.Owned_ID__c = deallist[0].Owned_Id__c;
             objEnt.Address__c = deallist[0].Workspace_Name__c;
             objEnt.REIT_Affiliation__c = deallist[0].Fund__c;
            objEnt.Team__c = deallist[0].Team_Alias__c;
            objEnt.Portfolio_Alias__c = deallist[0].Portfolio_Alias__c;
            objEnt.Portfolio_Description__c = deallist[0].Portfolio_Description__c;
            objEnt.Property_Alias__c = deallist[0].Property_Alias__c;
            objEnt.Property_Description__c = deallist[0].Property_Description__c;
            objEnt.Tenant_Concept__c = deallist[0].Report_Deal_Name__c;
            system.debug('objEnt----------------------'+objEnt);
            
            }
            
             if(deallist.size()>1)
            {
             objEnt.Address__c = 'multiple Properties';
            //objEnt.Owned_ID__c = 'Multiple Properties';
            objEnt.Property_Alias__c = 'Multiple Properties';
            objEnt.Property_Description__c = 'Multiple Properties';
            objEnt.Tenant_Concept__c = 'Multiple Properties';
            system.debug('objEnt----------------------'+objEnt);  
            }
            
            
            for(Deal__c cObjDeal:chldDealLst)
            {
                  /*   if(cObjDeal.Report_Deal_Name__c != Null)
                    {
                         tConcept1.add(cObjDeal.Report_Deal_Name__c); 
                    }
                     system.debug('tConcept1---------'+tConcept1); 
                    if(tConcept1.size()==1 )
                    { 
                            objEnt.Tenant_Concept__c = cObjDeal.Report_Deal_Name__c;
                    }        
                    else if(tConcept1.size()>1 )
                    {
                         objEnt.Tenant_Concept__c = 'Multiple Properties';
                    }     */              
                
                if(cObjDeal.Team_Alias__c=='Phoenix')
                {
                    iteam1.add(cObjDeal.Team_Alias__c); 
                }
                else if(cObjDeal.Team_Alias__c=='Charlotte')
                {
                    iteam2.add(cObjDeal.Team_Alias__c);
                }  
                if(iteam1.size()>0 && iteam2.size()>0)
                { 
                    objEnt.Team__c='Phoenix & Charlotte';
                }  
                 else if(cObjDeal.Team_Alias__c=='Phoenix') 
                {
                       objEnt.Team__c='Phoenix';
                }
                else if(cObjDeal.Team_Alias__c=='Charlotte')
                objEnt.Team__c='Charlotte';
             }
            }
             update parentEntyLst;
        }
    
         
    Catch(exception e)
    {
    }
}