trigger LC_InsertRecordInGridONOption on LC_OptionSchedule__c(before insert)
{
     list<LC_OptionSchedule__c> updateafterIns=new list<LC_OptionSchedule__c>();
    //get the lease Opportunity of the rent schedule.
    LC_LeaseOpprtunity__c lcp= new LC_LeaseOpprtunity__c();
    lcp=[select id, name,Base_Rent__c,Suite_Sqft__c,Annual_Base_Rent_at_expiration__c,Annual_Base_Rent_PSF__c,Base_Rent_PSF__c from LC_LeaseOpprtunity__c where id=:Trigger.new[0].Lease_Opportunity__c];
    
    //get the value of annual base rent and Annual base rent psf also.
    string myid= Trigger.new[0].id;
    String LOPPBaseRent=string.valueof(lcp.Base_Rent__c);
    String LOPNaseRentPSF=string.valueof(lcp.Base_Rent_PSF__c);
    
    //Now check all the scenarios for insert first 
    if(trigger.isinsert)
    {
        //now we will get all the records of therent schedule records of the lease opportunity to decide which number of records we are inserting
        list<LC_OptionSchedule__c>LC_OPPBseRentnotNull=[select id, From_In_Months__c,To_In_Months__c,Increase_in_rent_n__c,Lease_Opportunity__c,Increase_in_rent__c,
       Base_Rent_PSF__c,Option_Type__c,Monthly_PSF__c,Months_Years__c,sqft__c from LC_OptionSchedule__c where Lease_Opportunity__c=:lcp.id and Option_Type__c=:Trigger.new[0].Option_Type__c];
        
        list<LC_OptionSchedule__c>LC_afterInsList=[select id, From_In_Months__c,To_In_Months__c,Increase_in_rent_n__c,Lease_Opportunity__c,Increase_in_rent__c,
       Option_Type__c,Monthly_PSF__c,Months_Years__c,sqft__c,Base_Rent_PSF__c,Amount__c from LC_OptionSchedule__c where Lease_Opportunity__c=:lcp.id and Option_Type__c=:Trigger.new[0].Option_Type__c and id!=:myid order by createddate desc limit 1];
        system.debug('----------afterInsList---'+LC_afterInsList); 
        
        //if this is first record , do the following.
        
        if((LC_OPPBseRentnotNull.size()==0)&&(LC_afterInsList.size()==0))
        {
            for(LC_OptionSchedule__c sNew:Trigger.new)
            { 
                if(lcp.Base_Rent__c!=Null) 
                {
                    if((sNew.Amount__c==Null)&&(sNew.Increase_in_rent__c==Null) )
                    {
                        sNew.addError('Both Annual Amount and Increase in % rent  cannot be Null');
                    }
                    else
                    {
                        sNew.sqft__c  = lcp.Suite_Sqft__c;
                         if((sNew.Amount__c==Null)&&(sNew.Increase_in_rent__c==Null) )
                    {
                        sNew.addError('Annual Amount cannot be Null');
                    } 
                    else if((sNew.Amount__c==Null)&&(sNew.Increase_in_rent__c!=Null) )
                    {
                        sNew.addError('Cannot autocalculate using Increase in Rent. Please enter Annual Amount value.');
                    } 
                    else if((sNew.Amount__c!=Null)&&(sNew.Increase_in_rent__c!=Null) )
                    {
                        sNew.addError('Cannot autocalculate when both Annual Amount and Increase in rent are entered. Please enter only Annual amount value.');
                    }
                  // Annaual base rent of lease opportunity  is  null and forward calculation (backward calculation not possible in this Scenario)
                  
                    if((sNew.Amount__c!=Null)&&(sNew.Increase_in_rent__c==Null) )
                    {
                        //if(sNew.Option_Type__c=='RNT')
                       //sNew.Amount__c=lcp.Base_Rent__c;
                        sNew.Monthly_PSF__c =(sNew.Amount__c)/12;
                        sNew.Base_Rent_PSF__c=(sNew.Amount__c)/sNew.sqft__c;
                        //sNew.Base_Rent_PSF__c= lcp.Base_Rent_PSF__c;
                        sNew.Increase_in_rent_n__c = sNew.Amount__c-(sNew.Amount__c);
                        sNew.Increase_in_rent__c= sNew.Increase_in_rent_n__c;
                    } 
                   
                    }  
                }
                //check the scenario where annaual base rent of lease oppoertunity  is null
                else If(lcp.Base_Rent__c == Null) 
                {  
                    sNew.sqft__c  = lcp.Suite_Sqft__c; 
                    if((sNew.Amount__c==Null)&&(sNew.Increase_in_rent__c==Null) )
                    {
                        sNew.addError('Annual Amount cannot be Null');
                    } 
                    else if((sNew.Amount__c==Null)&&(sNew.Increase_in_rent__c!=Null) )
                    {
                        sNew.addError('Cannot autocalculate using Increase in rent. Please enter Annual Amount value.');
                    } 
                    else if((sNew.Amount__c!=Null)&&(sNew.Increase_in_rent__c!=Null) )
                    {
                        sNew.addError('Cannot autocalculate when both Annual Amount and Increase in % rent  are entered. Please enter only Annual Amount value.');
                    }
                      // Annaual base rent of lease opportunity  is  null and forward calculation (backward calculation not possible in this Scenario)
                    else if((sNew.Amount__c!=Null)&&(sNew.Amount__c!=0.00)&&(sNew.Increase_in_rent__c==Null))
                    {
                        sNew.Base_Rent_PSF__c=(sNew.Amount__c)/sNew.sqft__c;
                        sNew.Monthly_PSF__c =(sNew.Amount__c)/12;
                        sNew.Increase_in_rent_n__c = sNew.Amount__c-(sNew.Amount__c);
                        sNew.Increase_in_rent__c= sNew.Increase_in_rent_n__c;
                    }
                                   
                    //check rent abatement scenario
                    else if((sNew.Amount__c!=Null)&&(sNew.Amount__c==0.00))
                    {
                        sNew.Monthly_PSF__c =0.00;
                        sNew.Base_Rent_PSF__c=0.00;
                        sNew.Increase_in_rent_n__c =0.00;
                        sNew.Increase_in_rent__c=0.00;
                    }
                } 
            }
        }
        //If the new record is second or following records, do the following
        else if(LC_OPPBseRentnotNull.size()>0 && LC_afterInsList.size()>0)    
        {       
            //check the other condition that if they have entered Base rent on lease opportunity
            for(LC_OptionSchedule__c LCP1:LC_afterInsList)
            {   
                for(LC_OptionSchedule__c sNew:Trigger.new)
                { 
                    // Donot allow duplicate records with in the Income Category
                    for( LC_OptionSchedule__c dup:LC_OPPBseRentnotNull)
                    {
                        if( (sNew.From_In_Months__c==dup.From_In_Months__c)&&(sNew.To_In_Months__c==dup.To_In_Months__c))
                        {
                             sNew.addError('Duplicate records');
                        }
                                                     
                        if( (sNew.From_In_Months__c== dup.To_In_Months__c))
                        {
                            sNew.addError('From value cannot be same as To value of previous record.');
                        }
                
                    }
                    // display an error messgae if both Annual Base rent and Increse in rent% are null
                    if((sNew.Amount__c==Null)&&(snew.Increase_in_rent__c==Null) )
                    {
                        sNew.addError('Both Annual Amount and Increase in  Rent % cannot be empty');
                    } 
                    
                
                    //Forward calculations    
                    else if(LCP1.Amount__c!=null && sNew.Amount__c!=null && snew.Increase_in_rent__c==null)
                    {
                        sNew.sqft__c  = lcp.Suite_Sqft__c;
                        sNew.Monthly_PSF__c =(sNew.Amount__c)/12;
                        sNew.Base_Rent_PSF__c = (sNew.Amount__c)/sNew.sqft__c;  
                        //Check rent abatement scenario 
                        if(LCP1.Amount__c==0)
                        {
                            sNew.Increase_in_rent_n__c = sNew.Amount__c-(sNew.Amount__c);
                            sNew.Increase_in_rent__c=sNew.Increase_in_rent_n__c;
                        }
                        else
                        { 
                            sNew.Increase_in_rent_n__c=   sNew.Amount__c- (LCP1.Amount__c); 
                            sNew.Increase_in_rent__c= (sNew.Increase_in_rent_n__c)/(LCP1.Amount__c)*100;  
                        }
                    } 
                    //Backward calculations                     
                    else If (LCP1.Amount__c!=null && LCP1.Amount__c!=0 && sNew.Amount__c==null && snew.Increase_in_rent__c!=null)
                    {
                       
                        System.debug('I am in this ugly loop'+snew.Increase_in_rent__c);
                        system.debug('Check the annual psf'+LCP1.Amount__c);
                        System.debug('Thisis New Record'+sNew);
                        System.debug('This is my last record'+LCP1);
                        sNew.sqft__c = lcp.Suite_Sqft__c;  
                        
                        snew.Increase_in_rent_n__c = ((sNew.Increase_in_rent__c)*(LCP1.Amount__c))/100; 
                        sNew.Amount__c=  snew.Increase_in_rent_n__c+LCP1.Amount__c;
                        sNew.Monthly_PSF__c =(sNew.Amount__c)/12;
                        sNew.Base_Rent_PSF__c = (sNew.Amount__c)/sNew.sqft__c; 
                      }
                     //Check Backward calculations rent abatement scenario 
                     else If (LCP1.Amount__c!=null && LCP1.Amount__c==0 && sNew.Amount__c==null && snew.Increase_in_rent__c!=null) 
                        {
                            sNew.addError('Auto calculations based on Increase in % rent  not possible. Please enter Annual BAse Rent');
                          
                                    
                        }
                     else If (LCP1.Amount__c!=null && LCP1.Amount__c==0 && sNew.Amount__c!=null && snew.Increase_in_rent__c!=null)
                      sNew.addError('Cannot autocalculate when both Annual Amount and Increase in % rent  are entered. Please enter only Annual Amount value.');
 
                } 
               
            }
            
        }
        
    }
               
}