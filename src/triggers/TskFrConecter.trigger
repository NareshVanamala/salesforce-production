trigger TskFrConecter on Activity_Connector_Log_A_Call__c (after insert) {
List <Activity_Connector__c> insertact = new List<Activity_Connector__c >();
for(Activity_Connector_Log_A_Call__c lac : trigger.new)
{
List <Activity_Connector__c> Check= new List<Activity_Connector__c >();
Check = [ Select Id From Activity_Connector__c where Campaign__c = :lac.Campaign__c And Account__c = :lac.Account__c];
if(Check.size() == 0)
{
  Activity_Connector__c Ac = new Activity_Connector__c();
  ac.Name = 'Test';
  ac.Account__c = lac.Account__c;
  ac.Campaign__c= lac.Campaign__c;
  insertact.add(ac);
}
  }
  If(insertact.size()!=0)
 { 
insert insertact;
}
List<Task> ctr = new List<Task>();
For(Activity_Connector_Log_A_Call__c crt: trigger.new ){
Activity_Connector__c  ts = new Activity_Connector__c();

ts = [Select ID, Campaign__c, Account__c From Activity_Connector__c Where Campaign__c =:crt.Campaign__c AND Account__c = :crt.Account__c];
task tsk = new task();
If(crt.User__c != Null){
tsk.ownerId = crt.User__c;
}
tsk.of_Clients__c = crt.Of_Clients__c;
tsk.of_Reps__c = crt.of_Reps__c;
tsk.Complaint__c = crt.Complaint__c;
//tsk.Previous__c = crt.Previous__c;
tsk.type = crt.Type__c;
tsk.Status = 'Completed';
tsk.ActivityDate = crt.Due_Date__c;
tsk.whoId = crt.Contact__c;
If(crt.Subject__c ==Null)
{tsk.Subject = 'Call';
}else{
tsk.Subject = crt.Subject__c;
}
tsk.Priority = crt.Priority__c;
tsk.WhatId = ts.Id;
//tsk.Yesterday__c = crt.Yesterday__c;
tsk.Investor_Type__c = crt.Investor_Type__c;
tsk.Fund__c = crt.Fund__c;
tsk.EMailName__c = crt.E_Mail_Name__c;                                  
tsk.EMailSubject__c = crt.E_Mail_Subject__c;                          
tsk.DateSent__c = crt.Date_Sent__c;                              
tsk.OpenDate__c = crt.Open_Date__c;                                    
tsk.ViewasAWebPage__c = crt.View_As_a_WebPage__c;                                      
tsk.Link1__c = crt.Link_1__c;                   
tsk.Link2__c = crt.Link_2__c;                          
tsk.Link3__c = crt.Link_3__c;                      
tsk.Link4__c = crt.Link_4__c;                                  
tsk.Description = crt.Comments__c;                                     
tsk.Quality_Conversation__c = crt.Quality_Conversation__c;                     
ctr.add(tsk);


task tsk1 =  new task();
If(crt.AssignedTofu__c != Null){
tsk1.ownerId = crt.AssignedTofu__c;
}
tsk1.Type = crt.Typefu__c;
tsk1.ActivityDate = crt.Due_Date_FU__c;
tsk1.WhoId = crt.Contact__c;
tsk1.WhatId = ts.Id;
tsk1.Subject = crt.Subjectfu__c;
tsk1.Priority = crt.Priorityfu__c;
tsk1.Status = crt.Statusfu__c;                             
tsk1.of_Clients__c = crt.Of_Clients_fu__c;                                
tsk1.of_Reps__c  = crt.Of_Reps_Fu__c;      
//tsk1.Previous__c = crt.Previousfu__c;                                                  
tsk1.Complaint__c = crt.Complaintfu__c;                                                
//tsk1.Yesterday__c = crt.Yesyterdayfu__c ;                    
tsk1.Investor_Type__c = crt.Investor_Type_Fu__c;                                             
tsk1.Fund__c = crt.Fundfu__c;                                           
tsk1.EMailName__c =  crt.E_Mail_Namefu__c;                                             
tsk1.EMailSubject__c = crt.E_Mail_Subjectfu__c;                                               
tsk1.DateSent__c  = crt.Date_Sentfu__c;
tsk1.OpenDate__c  = crt.Open_Datefu__c;                                           
tsk1.ViewasAWebPage__c = crt.View_as_Web_Pagefu__c;                                                 
tsk1.Link1__c = crt.Link_1fu__c;
tsk1.Link1__c = crt.Link_2fu__c;                            
tsk1.Link3__c = crt.Link_3fu__c;                                   
tsk1.Link4__c = crt.Link_4fu__c;                                        
tsk1.Description = crt.Commentsfu__c;                                                                                
tsk1.Quality_Conversation__c = crt.Quality_Conversionfu__c;                                                                                             
                     
ctr.add(tsk1);
}
insert ctr;
}