trigger createhelper on Account (after insert,after update) {
List<National_Account_Helper__c> NA = new List<National_Account_Helper__c>();

For( Account acc : trigger.new)
{
If(acc.RecordTypeId == '012500000004uvF')
{
list<National_Account_Helper__c> NAH =[select id, Account__c,X30To60days1to3Ticlets__c,X30To60days25moreTickets__c,X30To60days4to24Tickets__c,X60To180days1to3Tickets__c,X60to180days25moreTickets__c,X60to180days4to24Tickets__c,X180Plusdays1to3Tickets__c,X180Plusdays25moreTickets__c,X180Plusdays4to24Tickets__c, AprilCCIT__c,AprilCCPT__c,AprilCCPTII__c,AprilCCPTIII__c,AprilCCPTIV__c,AprilIncomeNav__c,AugustCCIT__c,AugustCCPT__c,AugustCCPTII__c,AugustCCPTIII__c,AugustCCPTIV__c,
AugustIncomeNav__c,CCITlastyear__c,CCPTIIILastYear__c,CCPTIILastYear__c,CCPTIVlastyear__c,CCPT_LastYear__c,CurrentYearMinus2CCIT__c,CurrentYearMinus2CCPT__c,CurrentYearMinus2CCPTII__c,CurrentYearMinus2CCPTIII__c,
CurrentYearMinus2CCPTIV__c,CurrentYearMinus2IncomeNav__c,CurrentyearMinus3CCIT__c,CurrentYearMinus3CCPT__c,CurrentYearMinus3CCPTII__c,CurrentYearMinus3CCPTIII__c,CurrentYearMinus3CCPTIV__c,CurrentYearMinus3IncomeNav__c,
FebCCIT__c,FebCCPT__c,FebCCPTII__c,FebCCPTIII__c,FebCCPTIV__c,FebIncomeNav__c,INCOMENAVLastyear__c,JanCCIT__c,JanCCPT__c,JanCCPTII__c,JanCCPTIII__c,JanCCPTIV__c,JanIncomeNav__c,JulyCCIT__c,JulyCCPT__c,JulyCCPTII__c,JulyCCPTIII__c,JulyCCPTIV__c,
JulyIncomeNav__c,JuneCCIT__c,JuneCCPT__c,JuneCCPTII__c,JuneCCPTIII__c,JuneCCPTIV__c,JuneIncomeNav__c,MarCCIT__c,MarCCPT__c,MarCCPTII__c,MarCCPTIII__c,MarCCPTIV__c,MarIncomeNav__c,MayCCIT__c,MayCCPT__c,MayCCPTII__c,MayCCPTIII__c,MayCCPTIV__c,MayIncomeNav__c,NovCCIT__c,
NovCCPT__c,NovCCPTII__c,NovCCPTIII__c,NovIncomeNav__c,OctCCIT__c,OctCCPT__c,OctCCPTII__c,OctCCPTIII__c,OctCCPTIV__c,OctIncomeNav__c,SeptCCIT__c,SeptCCPT__c,SeptCCPTII__c,SeptCCPTIII__c,SeptCCPTIV__c,SeptIncomeNav__c,DecCCIT__c,DecCCPT__c,DecCCPTII__c,DecCCPTIII__c,DecCCPTIV__c,DecIncomeNav__c,NovCCPTIV__c from  National_Account_Helper__c where Account__c=:acc.id limit 1] ;
if(NAH.size()==0)
{
National_Account_Helper__c NAH1= new National_Account_Helper__c();
NAH1.Account__c = acc.ID;
NAH1.X30To60days1to3Ticlets__c = 0;
NAH1.X30To60days4to24Tickets__c = 0;
NAH1.X30To60days25moreTickets__c = 0;
NAH1.X60To180days1to3Tickets__c = 0;  
NAH1.X60to180days4to24Tickets__c = 0;
NAH1.X60to180days25moreTickets__c = 0;                    
NAH1.X180Plusdays1to3Tickets__c = 0;  
NAH1.X180Plusdays4to24Tickets__c = 0;
NAH1.X180Plusdays25moreTickets__c = 0; 
NAH1.AprilCCIT__c=0;
NAH1.AprilCCPT__c=0;
NAH1.AprilCCPTII__c=0;
NAH1.AprilCCPTIII__c=0;
NAH1.AprilCCPTIV__c=0;
NAH1.AprilIncomeNav__c=0;
NAH1.AugustCCIT__c=0;
NAH1.AugustCCPT__c=0;
NAH1.AugustCCPTII__c=0;
NAH1.AugustCCPTIII__c=0;
NAH1.AugustCCPTIV__c=0;
NAH1.AugustIncomeNav__c=0;
NAH1.CCITlastyear__c=0;
NAH1.CCPTIIILastYear__c=0;
NAH1.CCPTIILastYear__c=0;
NAH1.CCPTIVlastyear__c=0;
NAH1.CCPT_LastYear__c=0;
NAH1.CurrentYearMinus2CCIT__c=0;
NAH1.CurrentYearMinus2CCPT__c=0;
NAH1.CurrentYearMinus2CCPTII__c=0;
NAH1.CurrentYearMinus2CCPTIII__c=0;
NAH1.CurrentYearMinus2CCPTIV__c=0;
NAH1.CurrentYearMinus2IncomeNav__c=0;
NAH1.CurrentyearMinus3CCIT__c=0;
NAH1.CurrentYearMinus3CCPT__c=0;
NAH1.CurrentYearMinus3CCPTII__c=0;
NAH1.CurrentYearMinus3CCPTIII__c=0;
NAH1.CurrentYearMinus3CCPTIV__c=0;
NAH1.CurrentYearMinus3IncomeNav__c=0;
NAH1.FebCCIT__c=0;
NAH1.FebCCPT__c=0;
NAH1.FebCCPTII__c=0;
NAH1.FebCCPTIII__c=0;
NAH1.FebCCPTIV__c=0;
NAH1.FebIncomeNav__c=0;
NAH1.INCOMENAVLastyear__c=0;
NAH1.JanCCIT__c=0;
NAH1.JanCCPT__c=0;
NAH1.JanCCPTII__c=0;
NAH1.JanCCPTIII__c=0;
NAH1.JanCCPTIV__c=0;
NAH1.JanIncomeNav__c=0;
NAH1.JulyCCIT__c=0;
NAH1.JulyCCPT__c=0;
NAH1.JulyCCPTII__c=0;
NAH1.JulyCCPTIII__c=0;
NAH1.JulyCCPTIV__c=0;
NAH1.JulyIncomeNav__c=0;
NAH1.JuneCCIT__c=0;
NAH1.JuneCCPT__c=0;
NAH1.JuneCCPTII__c=0;
NAH1.JuneCCPTIII__c=0;
NAH1.JuneCCPTIV__c=0;
NAH1.JuneIncomeNav__c=0;
NAH1.MarCCIT__c=0;
NAH1.MarCCPT__c=0;
NAH1.MarCCPTII__c=0;
NAH1.MarCCPTIII__c=0;
NAH1.MarCCPTIV__c=0;
NAH1.MarIncomeNav__c=0;
NAH1.MayCCIT__c=0;
NAH1.MayCCPT__c=0;
NAH1.MayCCPTII__c=0;
NAH1.MayCCPTIII__c=0;
NAH1.MayCCPTIV__c=0;
NAH1.MayIncomeNav__c=0;
NAH1.NovCCIT__c=0;
NAH1.NovCCPT__c=0;
NAH1.NovCCPTII__c=0;
NAH1.NovCCPTIII__c=0;
NAH1.NovIncomeNav__c=0;
NAH1.NovCCPTIV__c=0;
NAH1.OctCCIT__c=0;
NAH1.OctCCPT__c=0;
NAH1.OctCCPTII__c=0;
NAH1.OctCCPTIII__c=0;
NAH1.OctCCPTIV__c=0;
NAH1.OctIncomeNav__c=0;
NAH1.SeptCCIT__c=0;
NAH1.SeptCCPT__c=0;
NAH1.SeptCCPTII__c=0;
NAH1.SeptCCPTIII__c=0;
NAH1.SeptCCPTIV__c=0;
NAH1.SeptIncomeNav__c=0;
NAH1.DecCCIT__c=0;
NAH1.DecCCPT__c=0;
NAH1.DecCCPTII__c=0;
NAH1.DecCCPTIII__c=0;
NAH1.DecCCPTIV__c=0;
NAH1.DecIncomeNav__c=0;

NA.add(NAH1);
}
}
}
insert NA;
}