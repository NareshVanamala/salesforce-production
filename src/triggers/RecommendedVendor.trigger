trigger RecommendedVendor on Bid__c (before insert,before update){

    set<string> ProjectIds = new set<string>();

    map<string,List<Bid__c>> maptoBid = new map<string,List<Bid__c>>();

    for(Bid__c bidObj :trigger.new){

          ProjectIds.add(bidObj.CM_Project__c);

    }

    if(ProjectIds!=null&&ProjectIds.size()>0){

        for(Bid__c bidObj :[select id,Recommended_Vendor__c,CM_Project__c from Bid__c where CM_Project__c in :ProjectIds]){

            if(maptoBid.get(bidObj.CM_Project__c)!=null){

              maptoBid.get(bidObj.CM_Project__c).add(bidObj);

              }else{

                 maptoBid.put(bidObj.CM_Project__c,new list<Bid__c>{bidObj});  

              }

        }

        for(string projectId : ProjectIds)
        {
            set<string> bidids = new  set<string>();
             if(maptoBid.get(projectId)!=null) 
             { 
                for(Bid__c con :maptoBid.get(projectId))
                {
                   if(con.Recommended_Vendor__c==true)
                   {
                      bidids.add(con.id);

                    }
                }  
             }

             if(bidids.size()>=1){

                 for(Bid__c con : trigger.new){
                    if(trigger.isInsert){
                        if(con.Recommended_Vendor__c==true){

                        con.Recommended_Vendor__c.addError('Project can have only one Bid has Recommended vendor'); 

                        }
                    }else if(trigger.isUpdate)
                        if((trigger.oldmap.get(con.id).Recommended_Vendor__c!=con.Recommended_Vendor__c) && con.Recommended_Vendor__c==true){

                            con.Recommended_Vendor__c.addError('Project can have only one Bid has Recommended vendor'); 

                        }
                        
                 }

             }

         }

       

    }

}