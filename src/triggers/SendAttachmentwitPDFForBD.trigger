trigger SendAttachmentwitPDFForBD on Event_Automation_Request__c (after update) 
{
    public set<id> test;
    public list<attachment> insertlist = new list<attachment>();
    public list<attachment> deletelist = new list<attachment>();
    //List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();
    if(trigger.isupdate)
    {
     
       EmailTemplate e=[select id,name,subject,(select id,name from attachments) from EmailTemplate where name='New BD Marketing Support Final Approval']; 
        
       for(attachment a:e.attachments)
       {
          
         deletelist.add(a);
       } 
         delete deletelist; 
         
         for(Event_Automation_Request__c efc:trigger.new)
         {
            if(efc.Event_Type__c=='BD Marketing Support')
            {
              test = new set<id>();
              test.add(efc.id); 
            }  
         }
        
       list<attachment> ant=[select id,name,body,parentid from attachment where parentid in:test];        
 
       for(Attachment at:ant)
       {
           attachment a = new attachment();
           a.name=at.name;
           a.body=at.body;
           a.parentid=e.id;
           insertlist.add(a);
       } 
       insert insertlist;
       
     
    }
 
 
 
}