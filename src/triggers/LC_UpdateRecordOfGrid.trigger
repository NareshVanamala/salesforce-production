trigger LC_UpdateRecordOfGrid on LC_RentSchedule__c (before update)
{
        list<LC_RentSchedule__c> updateafterIns=new list<LC_RentSchedule__c>();
        //get the lease Opportunity of the rent schedule.
        LC_LeaseOpprtunity__c lcp= new LC_LeaseOpprtunity__c();
        LC_RentSchedule__c oldvalues=new LC_RentSchedule__c ();
        LC_Helper_for_Grid__c mvc=[select id,Helper__c from LC_Helper_for_Grid__c where name='Update Now'];
        
        
        //boolean updateRecord=false;
        lcp=[select id, name,Base_Rent__c,Suite_Sqft__c,Annual_Base_Rent_at_expiration__c,Annual_Base_Rent_PSF__c,Base_Rent_PSF__c from LC_LeaseOpprtunity__c where id=:Trigger.new[0].Lease_Opportunity__c];
        //get the value of annual base rent and Annual base rent psf also.
        string myid= Trigger.new[0].id;
        String LOPPBaseRent=string.valueof(lcp.Base_Rent__c);
        String LOPNaseRentPSF=string.valueof(lcp.Base_Rent_PSF__c);
        Datetime D= Trigger.new[0].createddate;
       
        list<LC_RentSchedule__c>LC_AfterInsertedList=[select id, From_In_Months__c,To_In_Months__c,Increase_in_rent_n__c,Lease_Opportunity__c,Increase_in_rent__c,Average_Base_Rent_over_Initial_Term__c,
        Base_Rent_Additional_Info__c,Base_Rent_PSF__c,Income_Category_1__c,Monthly_PSF__c,Months_Years__c,sqft__c,Annual_PSF__c  from LC_RentSchedule__c where Lease_Opportunity__c=:lcp.id and Income_Category_1__c=:Trigger.new[0].Income_Category_1__c and id!=:myid and createddate>:d];
       
        system.debug('----------afterInsList---'+LC_AfterInsertedList); 
       
        System.debug('This is trigger.news myid '+myid);
        System.debug('Thisis income category'+trigger.new[0].Income_Category_1__c);
        System.debug('This is the createddate'+d);
            
       
       
        
        list<LC_RentSchedule__c>LC_bforeInsertedList=[select id, From_In_Months__c,To_In_Months__c,Increase_in_rent_n__c,Lease_Opportunity__c,Increase_in_rent__c,Average_Base_Rent_over_Initial_Term__c,
        Base_Rent_Additional_Info__c,Base_Rent_PSF__c,Income_Category_1__c,Monthly_PSF__c,Months_Years__c,sqft__c,Annual_PSF__c  from LC_RentSchedule__c where Lease_Opportunity__c=:lcp.id and Income_Category_1__c=:Trigger.new[0].Income_Category_1__c and id!=:myid and createddate<:d order by createddate DESC];
        System.debug('This is my Trigger.new record'+Trigger.new[0]);
        System.debug('This is the AfterInsertedList'+LC_AfterInsertedList);  
        //we will check first scenario where Annual_PSF__c changes in the first rceord of the rent schedule.
        if(LC_bforeInsertedList.size()==0)
        {
            for(LC_RentSchedule__c ctr:trigger.new)
                oldvalues=Trigger.oldMap.get(ctr.Id);
             
             if(Trigger.new[0].Annual_PSF__c==null)
             {  
               Trigger.new[0].addError('Anual base rent cannot be Null');
             }   
            System.debug('Check the old values'+oldvalues);  
            if((Trigger.new[0].Annual_PSF__c!=oldvalues.Annual_PSF__c)&&(Trigger.new[0].Annual_PSF__c!=Null)&&(Trigger.new[0].Increase_in_rent__c==oldvalues.Increase_in_rent__c)&&(mvc.Helper__c==false))
            {
                system.debug('I am here where I have changed the valus of Annual_PSF__c of the rent schedules first record');
                //perform all the calculations on the trigger.new 
                //If base rent on Lease Opportunity is null
                If(LOPPBaseRent== Null) 
                {
                      
                        if(Trigger.new[0].Annual_PSF__c!=0)
                        {
                            Trigger.new[0].Base_Rent_PSF__c=(Trigger.new[0].Annual_PSF__c)/Trigger.new[0].sqft__c;
                            Trigger.new[0].Monthly_PSF__c =(Trigger.new[0].Annual_PSF__c)/12;
                            Trigger.new[0].Increase_in_rent_n__c = Trigger.new[0].Annual_PSF__c-(Trigger.new[0].Annual_PSF__c);
                            Trigger.new[0].Increase_in_rent__c= Trigger.new[0].Increase_in_rent_n__c;
                            if(LC_AfterInsertedList.size()>0)
                             {
                             LC_AfterInsertedList[0].Increase_in_rent_n__c=LC_AfterInsertedList[0].Annual_PSF__c-Trigger.new[0].Annual_PSF__c;
                              LC_AfterInsertedList[0].Increase_in_rent__c=(LC_AfterInsertedList[0].Increase_in_rent_n__c)/(Trigger.new[0].Annual_PSF__c)*100; 
                           }
                            mvc.Helper__c=true;
                             update mvc;  
                             if(LC_AfterInsertedList.size()>0)
                            update  LC_AfterInsertedList[0];     
                       }
                      else if(Trigger.new[0].Annual_PSF__c==0) 
                      {
                      
                            Trigger.new[0].Base_Rent_PSF__c=0;
                            Trigger.new[0].Monthly_PSF__c =0;
                            Trigger.new[0].Increase_in_rent_n__c = 0;
                            Trigger.new[0].Increase_in_rent__c= 0;
                             if(LC_AfterInsertedList.size()>0)
                             {
                                LC_AfterInsertedList[0].Increase_in_rent_n__c=0;
                                LC_AfterInsertedList[0].Increase_in_rent__c=0; 
                             }
                            mvc.Helper__c=true;
                            update mvc;  
                            if(LC_AfterInsertedList.size()>0)
                            update  LC_AfterInsertedList[0];
                     }
                                     
                }
                  //If base rent on Lease Opportunity is Not Null
                  else if(LOPPBaseRent!= Null )
                  {
                        if(Trigger.new[0].Income_Category_1__c=='RNT')
                        Trigger.new[0].Annual_PSF__c=lcp.Base_Rent__c;
                        Trigger.new[0].Base_Rent_PSF__c=(Trigger.new[0].Annual_PSF__c)/Trigger.new[0].sqft__c;
                        Trigger.new[0].Monthly_PSF__c =(Trigger.new[0].Annual_PSF__c)/12;
                         if( LC_AfterInsertedList.size()>0)
                         {
                            LC_AfterInsertedList[0].Increase_in_rent_n__c=LC_AfterInsertedList[0].Annual_PSF__c-Trigger.new[0].Annual_PSF__c;
                            LC_AfterInsertedList[0].Increase_in_rent__c=(LC_AfterInsertedList[0].Increase_in_rent_n__c)/(Trigger.new[0].Annual_PSF__c)*100; 
                          }
                        mvc.Helper__c=true;
                        update mvc;  
                       if( LC_AfterInsertedList.size()>0)
                        update  LC_AfterInsertedList[0];
                        
                  }
              }
              
            //this is the new scenario added when old value of  Annual PSF of first record is not 0 and then it has 0  
            if((Trigger.new[0].Annual_PSF__c!=oldvalues.Annual_PSF__c)&&(Trigger.new[0].Annual_PSF__c==0)&&(Trigger.new[0].Increase_in_rent__c==oldvalues.Increase_in_rent__c)&&(mvc.Helper__c==false)&&(LOPPBaseRent== Null))
            {
                  
                        Trigger.new[0].Base_Rent_PSF__c=0;
                        Trigger.new[0].Monthly_PSF__c =0;
                        Trigger.new[0].Increase_in_rent_n__c =0;
                        Trigger.new[0].Increase_in_rent__c= 0;   
                        LC_AfterInsertedList[0].Increase_in_rent_n__c =0;
                        LC_AfterInsertedList[0].Increase_in_rent__c= 0; 
                        mvc.Helper__c=true;
                        update mvc;  
                        update  LC_AfterInsertedList[0];     
                
            
           }
             //Backward calclulations not possible for the first record is updated.
              if((Trigger.new[0].Increase_in_rent__c!=Null)&&(Trigger.new[0].Increase_in_rent__c!=oldvalues.Increase_in_rent__c)&&(Trigger.new[0].Annual_PSF__c==oldvalues.Annual_PSF__c))
             {
                  Trigger.new[0].addError('For the first record: Cannot autocalculate using Increase in rent.');
             } 
             
              if(Trigger.new[0].Increase_in_rent__c==Null)
              {
                  Trigger.new[0].addError('Increase in Rent can not be null');
              } 
          
        }
        else if((LC_bforeInsertedList.size()>0)&&(mvc.Helper__c==false))
        {
            for(LC_RentSchedule__c ctr:trigger.new)
              oldvalues=Trigger.oldMap.get(ctr.Id);
          
            System.debug('Check the old values'+oldvalues); 
             // Donot allow duplicate records with in the Income Category
              for( LC_RentSchedule__c dup:LC_bforeInsertedList)
            {
                if( (Trigger.new[0].From_In_Months__c== dup.From_In_Months__c)&&(Trigger.new[0].To_In_Months__c==dup.To_In_Months__c))
                {
                 Trigger.new[0].addError('Duplicate records');
                }
            }
              //IF both Annual Base Rent and Increase in rent % are changed. Throw an alert
             if((Trigger.new[0].Annual_PSF__c!=oldvalues.Annual_PSF__c)&&(Trigger.new[0].Increase_in_rent__c!=oldvalues.Increase_in_rent__c))
             {
                 Trigger.new[0].addError('Cannot autocalculate when both Annual Base rent and Increase in rent are modified. Please enter either Annual amount value or Increase in rent.');
             }
              if((Trigger.new[0].Annual_PSF__c==Null)&&(Trigger.new[0].Increase_in_rent__c!=Null))
             {
                 Trigger.new[0].addError('Anual base rent cannot be Null');
             }
             if(Trigger.new[0].Increase_in_rent__c==Null)
              {
                  Trigger.new[0].addError('Increase in Rent can not be null');
              } 
             
              // Forward  calclulations when the remaining records are updated
             else if((Trigger.new[0].Annual_PSF__c!=oldvalues.Annual_PSF__c)&&(Trigger.new[0].Annual_PSF__c!=Null)&&(Trigger.new[0].Increase_in_rent__c==oldvalues.Increase_in_rent__c))
             {
                         system.debug('Thisis forward calculation loop');
                         Trigger.new[0].Base_Rent_PSF__c=(Trigger.new[0].Annual_PSF__c)/Trigger.new[0].sqft__c;
                         Trigger.new[0].Monthly_PSF__c =(Trigger.new[0].Annual_PSF__c)/12; 
                     // rent abatement scenario. If the first record Annual base rent is zero
                     if((Trigger.new[0].Annual_PSF__c==0) ||(LC_bforeInsertedList[0].Annual_PSF__c==0))
                     {
                           Trigger.new[0].Increase_in_rent_n__c =Trigger.new[0].Annual_PSF__c-(Trigger.new[0].Annual_PSF__c);
                           Trigger.new[0].Increase_in_rent__c=Trigger.new[0].Increase_in_rent_n__c;
                     }
                     else if((Trigger.new[0].Annual_PSF__c!=0) &&(LC_bforeInsertedList[0].Annual_PSF__c!=0))
                     {
                        Trigger.new[0].Increase_in_rent_n__c=Trigger.new[0].Annual_PSF__c-(LC_bforeInsertedList[0].Annual_PSF__c);
                        Trigger.new[0].Increase_in_rent__c=(Trigger.new[0].Increase_in_rent_n__c)/(LC_bforeInsertedList[0].Annual_PSF__c)*100;  
                     }
                 list<LC_RentSchedule__c>rclist1=[select id, From_In_Months__c,To_In_Months__c,Increase_in_rent_n__c,Lease_Opportunity__c,Increase_in_rent__c,Average_Base_Rent_over_Initial_Term__c,
                 Base_Rent_Additional_Info__c,Base_Rent_PSF__c,Income_Category_1__c,Monthly_PSF__c,Months_Years__c,sqft__c,Annual_PSF__c  from LC_RentSchedule__c where Lease_Opportunity__c=:lcp.id and Income_Category_1__c=:Trigger.new[0].Income_Category_1__c and createddate>:d order by createddate ASC limit 1 ]; 
                 system.debug('Check the list to be updated'+rclist1);
               if(rclist1.size()>0)
               {
                        if(Trigger.new[0].Annual_PSF__c==0)
                        {
                            rclist1[0].Increase_in_rent_n__c=(rclist1[0].Increase_in_rent_n__c)-(rclist1[0].Increase_in_rent_n__c);
                            rclist1[0].Increase_in_rent__c=rclist1[0].Increase_in_rent_n__c;
                        }
                        else if(Trigger.new[0].Annual_PSF__c!=0 && Trigger.new[0].Annual_PSF__c!=null)
                        {
                         rclist1[0].Increase_in_rent_n__c=rclist1[0].Annual_PSF__c-Trigger.new[0].Annual_PSF__c;
                         rclist1[0].Increase_in_rent__c=(rclist1[0].Increase_in_rent_n__c)/(Trigger.new[0].Annual_PSF__c)*100;
                         rclist1[0].Annual_PSF__c=  rclist1[0].Increase_in_rent_n__c+Trigger.new[0].Annual_PSF__c;
                       }
                      mvc.Helper__c=true;
                      update mvc;  
                      update rclist1[0];
                }
                
          } 
                    
          // Backward calclulations when the remaining records are updated
          if((Trigger.new[0].Increase_in_rent__c!=oldvalues.Increase_in_rent__c)&&(Trigger.new[0].Increase_in_rent__c!=Null)&&(Trigger.new[0].Annual_PSF__c==oldvalues.Annual_PSF__c)&&(mvc.Helper__c==false))
          {
                  if(LC_bforeInsertedList[0].Annual_PSF__c==0)
                  {
                     Trigger.new[0].addError('Auto calculations based on Increase in rent % not possible.');
                  }
                  if((Trigger.new[0].Annual_PSF__c!=Null)&&(Trigger.new[0].Increase_in_rent__c==Null))
                     {
                         Trigger.new[0].addError('Increase in rent %  cannot be Null');
                     }
                  else if(oldvalues.Annual_PSF__c!=0 && LC_bforeInsertedList[0].Annual_PSF__c!=0 )
                  {
                      system.debug('AM I in this backward calculations loop');
                      Trigger.new[0].Increase_in_rent_n__c = ((Trigger.new[0].Increase_in_rent__c)*(LC_bforeInsertedList[0].Annual_PSF__c))/100; 
                      Trigger.new[0].Annual_PSF__c=  Trigger.new[0].Increase_in_rent_n__c+LC_bforeInsertedList[0].Annual_PSF__c;
                      Trigger.new[0].Monthly_PSF__c =(Trigger.new[0].Annual_PSF__c)/12;
                      Trigger.new[0].Base_Rent_PSF__c = (Trigger.new[0].Annual_PSF__c)/Trigger.new[0].sqft__c; 
                  }                 
            
                    system.debug('Check the list'+LC_AfterInsertedList);
                    list<LC_RentSchedule__c>rclist=[select id, From_In_Months__c,To_In_Months__c,Increase_in_rent_n__c,Lease_Opportunity__c,Increase_in_rent__c,Average_Base_Rent_over_Initial_Term__c,
                    Base_Rent_Additional_Info__c,Base_Rent_PSF__c,Income_Category_1__c,Monthly_PSF__c,Months_Years__c,sqft__c,Annual_PSF__c  from LC_RentSchedule__c where Lease_Opportunity__c=:lcp.id and Income_Category_1__c=:Trigger.new[0].Income_Category_1__c and createddate>:d order by createddate ASC ]; 
                    system.debug('Check the list to be updated'+rclist);
                    list<LC_RentSchedule__c>Updaterclist1=new list<LC_RentSchedule__c>();
                    for(integer i=0;i<=rclist.size()-1;i++)
                    {
                    
                          if(i==0)
                          {
                          System.debug('I am updating the next record');
                          rclist[0].Increase_in_rent_n__c = ((rclist[0].Increase_in_rent__c)*(Trigger.new[0].Annual_PSF__c))/100; 
                          rclist[0].Annual_PSF__c= rclist[0].Increase_in_rent_n__c+Trigger.new[0].Annual_PSF__c;
                          rclist[0].Monthly_PSF__c =(rclist[0].Annual_PSF__c)/12;
                          rclist[0].Base_Rent_PSF__c = (rclist[0].Annual_PSF__c)/rclist[0].sqft__c; 
                          Updaterclist1.add(rclist[0]);
                          system.debug('Thisis the updated list');
                          }
                          if(i>0)
                          {
                          
                             system.debug('I am updating the rest records..');
                             rclist[i].Increase_in_rent_n__c = ((rclist[i].Increase_in_rent__c)*(rclist[i-1].Annual_PSF__c))/100; 
                             rclist[i].Annual_PSF__c= rclist[i].Increase_in_rent_n__c+rclist[i-1].Annual_PSF__c;
                             rclist[i].Monthly_PSF__c =(rclist[i].Annual_PSF__c)/12;
                             rclist[i].Base_Rent_PSF__c = (rclist[i].Annual_PSF__c)/rclist[i].sqft__c; 
                             Updaterclist1.add(rclist[i]);
                             System.debug('this is updated list of the remaing records');
                          
                          } 
                               
                    
                    }
                      mvc.Helper__c=true;
                      update mvc;
                      update Updaterclist1;
           }
     
      //lets update the following records list.
     
     }
    mvc.Helper__c=false;
    update mvc;
     
}