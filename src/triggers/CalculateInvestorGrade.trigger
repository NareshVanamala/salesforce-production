trigger CalculateInvestorGrade on REIT_Investment__c (after delete, after insert, after undelete) {
    REITInvestmentTriggers.CalculateInvestorGrade(Trigger.isDelete?Trigger.old:Trigger.new);
}