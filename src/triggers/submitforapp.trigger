trigger submitforapp on PIF_Child__c (after insert,after update) 
{
   List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();
   List<PIF_Child__c >pifchildlist= new list<PIF_Child__c >();
   List<Project_Initiation__c>pifParentidist= new list<Project_Initiation__c>();
   map<id,Project_Initiation__c>MapidtoObject= new map<id,Project_Initiation__c>();
   list<PIF_Child__c>pifReviewchildlist= new list<PIF_Child__c>();
   list<PIF_Child__c>Championchildlist= new list<PIF_Child__c>();
   
   list<PIF_Child__c>MypifReviewchildlist= new list<PIF_Child__c>();
   list<PIF_Child__c>Mychampionchildlist= new list<PIF_Child__c>();
   
   Map<Id, List<PIF_Child__c>>mapOfpifReviewchilds  = new Map<Id, List<PIF_Child__c>>();
   Map<Id, List<PIF_Child__c>>mapOfcampionchilds  = new Map<Id, List<PIF_Child__c>>();
   
   
   set<id>parentid= new set<id>();
   
    for(PIF_Child__c pc :trigger.new)
    {
      parentid.add(pc.PIF_parent__c);
    }
      Integer count=0;
      Integer championCount=0;
       
   pifParentidist=[select id,CFO_Status__c,PIF_Group_Status__c,PMO_Group_Status__c,Champion_Status__c,Sponsor_status__c,compliance_Approval_Status__c from Project_Initiation__c where id =:parentid ];
     for(Project_Initiation__c ct:pifParentidist)
       MapidtoObject.put(ct.id,ct);
    
    pifReviewchildlist=[select id,name,CFO_GRoup__c,CFO_Group_Status__c,Champion_group__c,Champion_Group_Status__c,Compliance_group__c,Compliance_Group_Status__c,PIF_Group__c,PIF_Group_Status__c,PIF_Name__c,PIF_parent__c,PMO_Group__c,PMO_Group_Status__c,Project_Requires_Compliance_Approval__c,Sponsor__c,Sponsor_Status__c from PIF_Child__c where name='PIF Review Group Approval Process'and PIF_parent__c in:parentid];   
    Championchildlist=[select id,name,CFO_GRoup__c,CFO_Group_Status__c,Champion_group__c,Champion_Group_Status__c,Compliance_group__c,Compliance_Group_Status__c,PIF_Group__c,PIF_Group_Status__c,PIF_Name__c,PIF_parent__c,PMO_Group__c,PMO_Group_Status__c,Project_Requires_Compliance_Approval__c,Sponsor__c,Sponsor_Status__c from PIF_Child__c where name='Champion Approval Process' and PIF_parent__c in:parentid ];   
   
       for(PIF_Child__c ri : pifReviewchildlist)
    {
        if(mapOfpifReviewchilds.containsKey(ri.PIF_parent__c ))
        {
            mapOfpifReviewchilds.get(ri.PIF_parent__c ).add(ri);
        }
        else
        {
            mapOfpifReviewchilds.put(ri.PIF_parent__c,new List<PIF_Child__c >{ri});
        }
    }    
    
    
    for(PIF_Child__c ri : Championchildlist)
    {
        if(mapOfcampionchilds.containsKey(ri.PIF_parent__c ))
        {
            mapOfcampionchilds.get(ri.PIF_parent__c ).add(ri);
        }
        else
        {
            mapOfcampionchilds.put(ri.PIF_parent__c,new List<PIF_Child__c >{ri});
        }
    }    
   
   
   
     
    if(Trigger.isInsert)
   {
       for(PIF_Child__c pc :trigger.new)
       {
            if((pc.PMO_Group_Status__c=='Submitted for Approval')
 ||(pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c == 'Submitted for Approval')
 ||(pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c == 'Approved')&&(pc.Champion_Group_Status__c =='Submitted for Approval')
 ||(pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c == 'Approved')&&(pc.Champion_Group_Status__c =='Approved')&&(pc.Sponsor_Status__c == 'Submitted for Approval')
 ||(pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c == 'Approved')&&(pc.Champion_Group_Status__c =='Approved')&&(pc.Sponsor_Status__c == 'Approved')&&(pc.Compliance_Group_Status__c== 'Submitted for Approval')
 ||(pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c == 'Approved')&&(pc.Champion_Group_Status__c =='Approved')&&(pc.Sponsor_Status__c == 'Approved')&&(pc.Compliance_Group_Status__c== 'Approved')&&(pc.CFO_Group_Status__c == 'Submitted for Approval'))
            {
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();   
                req.setObjectId(pc.Id);
                approvalReqList.add(req); 
            }
          }
          List<Approval.ProcessResult> resultList = Approval.process(approvalReqList);        
          for(Approval.ProcessResult result: resultList)
          {        
             System.debug('Submitted for approval successfully: '+result.isSuccess()); 
             system.debug('Check the approvalhistory '+result);    
            }
     }
    if(Trigger.isUpdate)
   {
        For(PIF_Child__c pc :trigger.new)
       {
          if((pc.PMO_Group_Status__c=='Approved')&&(MapidtoObject.get(pc.PIF_parent__c)!=null))
          {
             MapidtoObject.get(pc.PIF_parent__c).PMO_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).PIF_Group_Status__c='Submitted for Approval';
          }
            if((pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c=='Approved')&&(MapidtoObject.get(pc.PIF_parent__c)!=null))
          {
            MypifReviewchildlist= mapOfpifReviewchilds.get(pc.PIF_parent__c);
            if(MypifReviewchildlist !=null)
            {
           // system.debug('check my list..'+MypifReviewchildlist.size());
            for(PIF_Child__c  ctyy:MypifReviewchildlist)
            {
                   if(ctyy.PIF_Group_Status__c=='Approved')
                    count=count+1;
                
            }
              system.debug('check the count'+count);
            if(MypifReviewchildlist.size()==count)
            {
             MapidtoObject.get(pc.PIF_parent__c).PMO_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).PIF_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).Champion_Status__c= 'Submitted for Approval';
             }
          }
          }
          if((pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c=='Approved')&&(pc.Champion_Group_Status__c =='Approved')&&(MapidtoObject.get(pc.PIF_parent__c)!=null))
          {
            Mychampionchildlist= mapOfcampionchilds.get(pc.PIF_parent__c);
            //system.debug('check my list..'+Mychampionchildlist.size());
            if(Mychampionchildlist != null)
            {
            for(PIF_Child__c  ctyy:Mychampionchildlist)
            {
                   if(ctyy.Champion_Group_Status__c=='Approved')
                    championCount=championCount+1;
                
            }
              system.debug('check the count'+championCount);
            if(Mychampionchildlist.size()==championCount)
            {
                  
             MapidtoObject.get(pc.PIF_parent__c).PMO_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).PIF_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).Champion_Status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).Sponsor_status__c= 'Submitted for Approval';
            }
          }
          } 
         if((pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c=='Approved')&&(pc.Champion_Group_Status__c =='Approved')&&(pc.Sponsor_Status__c == 'Approved')&&(MapidtoObject.get(pc.PIF_parent__c)!=null))
          {
             MapidtoObject.get(pc.PIF_parent__c).PMO_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).PIF_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).Champion_Status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).Sponsor_status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).compliance_Approval_Status__c= 'Submitted for Approval';
          }  
         if((pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c=='Approved')&&(pc.Champion_Group_Status__c =='Approved')&&(pc.Sponsor_Status__c == 'Approved')&&(pc.Compliance_Group_Status__c == 'Approved')&&(MapidtoObject.get(pc.PIF_parent__c)!=null))
          {
             MapidtoObject.get(pc.PIF_parent__c).PMO_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).PIF_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).Champion_Status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).Sponsor_status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).compliance_Approval_Status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).CFO_Status__c= 'Submitted for Approval';
          } 
         if((pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c=='Approved')&&(pc.Champion_Group_Status__c =='Approved')&&(pc.Sponsor_Status__c == 'Approved')&&(pc.Compliance_Group_Status__c == 'Approved')&&(pc.CFO_Group_Status__c == 'Approved')&&(MapidtoObject.get(pc.PIF_parent__c)!=null))
          {
             MapidtoObject.get(pc.PIF_parent__c).PMO_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).PIF_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).Champion_Status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).Sponsor_status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).compliance_Approval_Status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).CFO_Status__c= 'Approved';
          }     
        else if((pc.PMO_Group_Status__c=='Rejected')&&(MapidtoObject.get(pc.PIF_parent__c)!=null))
          {
             MapidtoObject.get(pc.PIF_parent__c).PMO_Group_Status__c='Rejected';
            
          }
        if((pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c=='Rejected')&&(MapidtoObject.get(pc.PIF_parent__c)!=null))
          {
             MapidtoObject.get(pc.PIF_parent__c).PMO_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).PIF_Group_Status__c='Rejected';
            
          }
        if((pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c=='Approved')&&(pc.Champion_Group_Status__c =='Rejected')&&(MapidtoObject.get(pc.PIF_parent__c)!=null))
          {
             MapidtoObject.get(pc.PIF_parent__c).PMO_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).PIF_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).Champion_Status__c= 'Rejected';
            
          } 
        if((pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c=='Approved')&&(pc.Champion_Group_Status__c =='Approved')&&(pc.Sponsor_Status__c == 'Rejected')&&(MapidtoObject.get(pc.PIF_parent__c)!=null))
          {
             MapidtoObject.get(pc.PIF_parent__c).PMO_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).PIF_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).Champion_Status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).Sponsor_status__c= 'Rejected';
             
          } 
          if((pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c=='Approved')&&(pc.Champion_Group_Status__c =='Approved')&&(pc.Sponsor_Status__c == 'Approved')&&(pc.Compliance_Group_Status__c == 'Rejected')&&(MapidtoObject.get(pc.PIF_parent__c)!=null))
          {
             MapidtoObject.get(pc.PIF_parent__c).PMO_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).PIF_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).Champion_Status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).Sponsor_status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).compliance_Approval_Status__c= 'Rejected';
             
          } 
          if((pc.PMO_Group_Status__c=='Approved')&&(pc.PIF_Group_Status__c=='Approved')&&(pc.Champion_Group_Status__c =='Approved')&&(pc.Sponsor_Status__c == 'Approved')&&(pc.Compliance_Group_Status__c == 'Approved')&&(pc.CFO_Group_Status__c == 'Rejected')&&(MapidtoObject.get(pc.PIF_parent__c)!=null))
          {
             MapidtoObject.get(pc.PIF_parent__c).PMO_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).PIF_Group_Status__c='Approved';
             MapidtoObject.get(pc.PIF_parent__c).Champion_Status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).Sponsor_status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).compliance_Approval_Status__c= 'Approved';
             MapidtoObject.get(pc.PIF_parent__c).CFO_Status__c= 'Rejected';
          } 
         
        }
        update MapidtoObject.values();
    }
  
     
}