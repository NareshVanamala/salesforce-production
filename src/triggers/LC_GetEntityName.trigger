trigger LC_GetEntityName on LC_LeaseOpprtunity__c (after insert) 
{
     
  //************************This for Dead LOI **************************
    list<LC_LeaseOpprtunity__c> LeaseOpplist = new list<LC_LeaseOpprtunity__c>();
    set<id>loiIds=new set<id>();
    set<id>leaseIds=new set<id>();
    set<id>BldgIds= new set<Id>();
    set<String>SuiteIds = new set<String>();
    map<id,id>LOItoOppMap=new map<id,id>();
    map<id,list<attachment>>LOItoattachemntlistMap=new map<id,list<attachment>>();
    
    for(LC_LeaseOpprtunity__c lcl:Trigger.new)
    {
      if(lcl.LOI__c!=Null)
      loiIds.add(lcl.LOI__c);
      if(lcl.Lease__c!=Null)
      leaseIds.add(lcl.Lease__c);
      if(lcl.MRI_Property__c!=Null)
      BldgIds.add(lcl.MRI_Property__c);
      if(lcl.Suite_Id__c!=Null)
      SuiteIds.add(lcl.Suite_Id__c);
      LOItoOppMap.put(lcl.LOI__c,lcl.id);
    }
    list<LC_LOI__c>lOIlist=[select id, name,Lease__c,Milestone__c,MRI_Property__c,Suite_Id__c from LC_LOI__c where id in:loiIds and Milestone__c='LOI Signed' ];

    list<LC_LOI__c>deadLoilist=[select id,Lease__c,Milestone__c,MRI_Property__c,Suite_Id__c from LC_LOI__c where id not in:loiIds and Lease__c in:leaseIds and Milestone__c='LOI In Progress'];
    
    System.Debug('Dead LOI List'+deadLoilist);
    
    list<LC_LOI__c>DeadList=new list<LC_LOI__c>();
    
    if (deadLoilist.size()>0)
    {
        for(LC_LOI__c lci:deadLoilist)
        {
          
                    lci.Milestone__c='Dead Deal'; 
                    DeadList.add(lci);
            
        }
         update DeadList;
    }

  //************************This for Dead LOI **************************
  
  //************************This is for inserting 13 clauses to Lease Opportunity **************************
    
 list<LC_Clauses__c>cluaselist=[select id,Clause_Type__c,Clause_Body__c,Show_On_LETs_Form__c,name from LC_Clauses__c where Show_On_LETs_Form__c=True];
  list<LC_ClauseType__c>InsertCluaselist=new list<LC_ClauseType__c>();
  
  for(LC_LeaseOpprtunity__c c: trigger.new)
  {
  
      for(LC_Clauses__c cl:cluaselist)
    {
    LC_ClauseType__c clt=new LC_ClauseType__c();    
     clt.Lease_Opportunity__c=c.Id;
     clt.Clauses__c=cl.Id;
      clt.Name = cl.Name;  
    clt.ClauseType_Name__c=cl.Clause_Type__c;
    
     InsertCluaselist.add(clt); 
    }
     insert InsertCluaselist;
  
  } 
      
   


  //************************This is for inserting 13 clauses to Lease Opportunity **************************
  
   //************************This is for PRoperty Owner SPE from Enity Central **************************

   /* list<LC_LeaseOpprtunity__c> Lopplst = new list<LC_LeaseOpprtunity__c>();
    Lopplst =[Select Id,Name,Entity_Id__c,Suite_Sqft__c,Entity_Name__c,Standard_Deviation__c,Actual_NER__c,Budgeted_NER__c  from LC_LeaseOpprtunity__c WHERE Id IN: Trigger.newMap.keySet()];
   List<Entity__c> ent = new List<Entity__c>();
   
         for(LC_LeaseOpprtunity__c Lopp: Lopplst )
        {
           ent =[select id,name,Owned_ID__c from Entity__c where Owned_ID__c != Null AND Owned_ID__c=:Lopp.Entity_Id__c];
           Lopp.Suite_Sqft__c = Lopp.Suite_Sqft__c;
          
            for(Entity__c e1:ent )
            {
                if(e1.Owned_ID__c==Lopp.Entity_Id__c )
                Lopp.Entity_Name__c = e1.Name;
            }
        }
        update Lopplst;*/
        
       //************************This is for PRoperty Owner SPE from Enity Central **************************
       
        }