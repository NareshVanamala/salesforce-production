trigger CaseSubmitForApproval on Case (after insert)
 {
    
    Id MISRequest=[Select id,name,DeveloperName from RecordType where DeveloperName='MI_S_Request' and SobjectType = 'Case'].id; 
     List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();
     
      
        for (Case c: Trigger.New)
        {   
                if(((c.recordtypeid==MISRequest)&&(c.origin=='Email'))
             /*   ||((c.recordtypeid==NewhireRequest)&&(c.origin=='Web Site')&&(c.HRManager_Approval__c=='Sumitted For Approval'))
               ||((c.recordtypeid==TerminationRequest)&&(c.origin=='Web Site')&&(c.HRManager_Approval__c=='Sumitted For Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.MRI_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.SalesForce_Deal_Central__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Webex_Access_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.VDI_Access_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.VPN_Access_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Argus_Access_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.DST_Access_Requested_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.AVID_Account_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.BNA_Access_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Box_com_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Chatham_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Concur_Account_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.ADP_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Data_Warehouse_GL_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.DataWarehouse_RealEstate_RequestStatus__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Data_Warehouse_SalesRequestStatus__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Kardin_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.OneSource_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Remote_Desktop_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.RisKonnect_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Web_Filings_Request_Status__c=='Submitted for Approval'))
               
                //-------Parking--------//
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Parking_General_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Parking_Executive_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Parking_Contractor_request__c=='Submitted for Approval'))
                
                //-------Hardware-------//
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Hardware_Deploy_External__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Hardware_Provide_Desktop__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Hardware_Provide_Laptop__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Hardware_Dual_Monitor_Display__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Hardware_Provide_Iphone__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Hardware_Provide_Ipad__c=='Submitted for Approval'))
                
                //-------Building-------//
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Building_General_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Building_Facilities_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Building_RELegal_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Building_IT_Infrastructure_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Building_HR_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Building_Compliance_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Building_PropertyManagement_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Building_Audio_Visual_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Building_Executive_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==NewhireRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Building_B2Storage_Status__c=='Submitted for Approval'))*/
                

                /*||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.MRI_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.SalesForce_Deal_Central__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Webex_Access_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.VDI_Access_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.VPN_Access_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Argus_Access_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.AVID_Account_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.BNA_Access_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Box_com_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Chatham_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Concur_Account_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.ADP_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Data_Warehouse_GL_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.DataWarehouse_RealEstate_RequestStatus__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Data_Warehouse_SalesRequestStatus__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Kardin_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.OneSource_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Remote_Desktop_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.RisKonnect_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Web_Filings_Request_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Parking_General_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Parking_Executive_Status__c=='Submitted for Approval'))
                ||((c.recordtypeid==TerminationRequestSubcase)&&(c.origin=='Internal')&&(c.HRManager_Approval__c=='Approved')&&(c.Parking_Contractor_request__c=='Submitted for Approval'))
                */
                  )
               
               
               {
                 // create the new approval request to submit    
                  Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();   
                  //req.setComments('Submitted for approval. Please approve.');
                  req.setObjectId(c.Id);
                 approvalReqList.add(req);        
              }
       }
    

    // submit the approval request for processing      
    //List<Approval.ProcessResult> resultList = Approval.process(approvalReqList);        
    // display if the reqeust was successful
    /*for(Approval.ProcessResult result: resultList )
    {        
        System.debug('Submitted for approval successfully: '+result.isSuccess()); 
         system.debug('Check the approvalhistory '+result);    
    }
   }*/
   
   
     
}