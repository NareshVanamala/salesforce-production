trigger LeaseChargeDetails on Lease_Charge_Details__c (before insert,before update,after insert,after update) {

if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
 set<id> lcrid =new set<id>();
 for (Lease_Charge_Details__c leasechargerequest : Trigger.New)
        {
            lcrid.add(leasechargerequest.LeaseChargesRequest__c);
            system.debug('lcrid'+lcrid);
        }
for (Lease_Charges_Request__c LCR: [Select id,Name,MRI_Property__c from Lease_Charges_Request__c where id in:lcrid])
{
for(Lease_Charge_Details__c leasechargeobj : trigger.new){
 leasechargeobj.MRI_Property__c=LCR.MRI_Property__c;
 system.debug('leasechargeobj'+leasechargeobj);
}}
}
}