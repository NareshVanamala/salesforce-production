//***Created By Priya on May 17th 2012*****

trigger BusinessProfileTrack1 on Account (after insert,after update) {

 List<Business_Profile_Tracking__c> bptlist = new List<Business_Profile_Tracking__c>(); 
 List<Business_Profile_Tracking__c> bptlist1 = new List<Business_Profile_Tracking__c>(); 

 
 
For (Account a: Trigger.new)
{
 // **** Modify by Snehal on May 21st 2012*****
 
 
if(a.RecordTypeid == '012500000005RQ4'){ 
if(trigger.isupdate){
Account oldvalues=Trigger.oldMap.get(a.Id);
if(helper.run == TRUE)
{
//if (NotificationHelper.isScreenTooImpaired(newScreen) && !NotificationHelper.isScreenTooImpaired(oldScreen))
 
if(a.Income_Products_Used__c != oldvalues.Income_Products_Used__c)
 {
 system.debug('You just updated me...Income_Products_Used__c');
  Business_Profile_Tracking__c bpt = new Business_Profile_Tracking__c();
  bpt.name= 'Income Products Used Updated';
  bpt.Modified_on__c= a.LastModifiedDate;
  bpt.Modified_By__c=a.OwnerId;
  bpt.Comments__c='Value changed from '+Trigger.old[0].Income_Products_Used__c+' to '+ a.Income_Products_Used__c;
  bpt.Business_Type__c='Income Products Used';
  bpt.Account__c = a.id;
  bptlist.add(bpt);
 }
 /*if(a.Used_REIT_Before__c != oldvalues.Used_REIT_Before__c)
 {
  Business_Profile_Tracking__c bpt1 = new Business_Profile_Tracking__c();
  bpt1.name= 'Used REIT Before Updated';
  bpt1.Modified_on__c= a.LastModifiedDate;
  bpt1.Modified_By__c=a.OwnerId;
  bpt1.Comments__c='Value changed from '+Trigger.old[0].Used_REIT_Before__c+' to '+ a.Used_REIT_Before__c;
  bpt1.Business_Type__c='Used Nonlisted REIT before?';
  bpt1.Account__c = a.id;
  bptlist.add(bpt1);
 }*/
 /*if(a.RIA_Experience__c != oldvalues.RIA_Experience__c)
 {
  Business_Profile_Tracking__c bpt2 = new Business_Profile_Tracking__c();
  bpt2.name= 'RIA_Experience__c Updated';
  bpt2.Modified_on__c= a.LastModifiedDate;
  bpt2.Modified_By__c=a.OwnerId;
  bpt2.Comments__c='Value changed from '+Trigger.old[0].RIA_Experience__c+' to '+ a.RIA_Experience__c;
  bpt2.Business_Type__c='How long have you been an RIA?';
  bpt2.Account__c = a.id;
  bptlist.add(bpt2);
 }*/
 /*if(a.Fee_Based_Non_Commission__c != oldvalues.Fee_Based_Non_Commission__c)
 {
  Business_Profile_Tracking__c bpt3 = new Business_Profile_Tracking__c();
  bpt3.name= 'Business Fee Based Updated';
  bpt3.Modified_on__c= a.LastModifiedDate;
  bpt3.Modified_By__c=a.OwnerId;
  bpt3.Comments__c='Value changed from '+Trigger.old[0].Percentage_fee_base__c+' to '+ a.Percentage_fee_base__c;
  bpt3.Business_Type__c='Business % of FeeBased vs Commissionable';
  bpt3.Account__c = a.id;
  bptlist.add(bpt3);
 }*/
 if(a.BusSize_Total_AUM__c != oldvalues.BusSize_Total_AUM__c)
 {
  Business_Profile_Tracking__c bpt4 = new Business_Profile_Tracking__c();
  bpt4.name= 'Total AUM Updated';
  bpt4.Modified_on__c= a.LastModifiedDate;
  bpt4.Modified_By__c=a.OwnerId;
  bpt4.Comments__c='Value changed from '+Trigger.old[0].BusSize_Total_AUM__c+' to '+ a.BusSize_Total_AUM__c;
  bpt4.Business_Type__c='Total AUM';
  bpt4.Account__c = a.id;
  bptlist.add(bpt4);
 }
 if(a.TOP5_of_Clients_with_RE_in_Portfolio__c != oldvalues.TOP5_of_Clients_with_RE_in_Portfolio__c)
 {
  Business_Profile_Tracking__c bpt5 = new Business_Profile_Tracking__c();
  bpt5.name= '% of Clients with RE in Portfolio Updated';
  bpt5.Modified_on__c= a.LastModifiedDate;
  bpt5.Modified_By__c=a.OwnerId;
  bpt5.Comments__c='Value changed from '+Trigger.old[0].TOP5_of_Clients_with_RE_in_Portfolio__c+' to '+ a.TOP5_of_Clients_with_RE_in_Portfolio__c;
  bpt5.Business_Type__c='% of Clients with RE in Portfolio';
  bpt5.Account__c = a.id;
  bptlist.add(bpt5);
 }
 // ***Below code commented by Priya for validatiion rule
 /*if(a.Othe_RE_Exposure__c !=oldvalues.Othe_RE_Exposure__c)
 {
  Business_Profile_Tracking__c bpt6 = new Business_Profile_Tracking__c();
  bpt6.name= 'Other RE Exposure Updated';
  bpt6.Modified_on__c= a.LastModifiedDate;
  bpt6.Modified_By__c=a.OwnerId;
  bpt6.Comments__c='Value changed from '+Trigger.old[0].Othe_RE_Exposure__c+' to '+ a.Othe_RE_Exposure__c;
  bpt6.Business_Type__c='Other RE Exposure';
  bpt6.Account__c = a.id;
  bptlist.add(bpt6);
 }*/
 if(a.Bus_SizeAllocated_to_RE__c != oldvalues.Bus_SizeAllocated_to_RE__c)
 {
  Business_Profile_Tracking__c bpt7 = new Business_Profile_Tracking__c();
  bpt7.name= 'Allocated to RE Updated';
  bpt7.Modified_on__c= a.LastModifiedDate;
  bpt7.Modified_By__c=a.OwnerId;
  bpt7.Comments__c='Value changed from '+Trigger.old[0].Bus_SizeAllocated_to_RE__c+' to '+ a.Bus_SizeAllocated_to_RE__c;
  bpt7.Business_Type__c='Allocated to RE';
  bpt7.Account__c = a.id;
  bptlist.add(bpt7);
 }
 if(a.Total_Sales_Last_Year_RIA__c != oldvalues.Total_Sales_Last_Year_RIA__c)
 {
  Business_Profile_Tracking__c bpt8 = new Business_Profile_Tracking__c();
  bpt8.name= 'Total Sales Last Year Updated';
  bpt8.Modified_on__c= a.LastModifiedDate;
  bpt8.Modified_By__c=a.OwnerId;
  bpt8.Comments__c='Value changed from '+Trigger.old[0].Total_Sales_Last_Year_RIA__c+' to '+ a.Total_Sales_Last_Year_RIA__c;
  bpt8.Business_Type__c='Total Sales Last Year';
  bpt8.Account__c = a.id;
  bptlist.add(bpt8);
 }
 if(a.Fee_Based_Non_Commission__c != oldvalues.Fee_Based_Non_Commission__c)
 {
  Business_Profile_Tracking__c bpt9 = new Business_Profile_Tracking__c();
  bpt9.name= 'Fee Based (Non Commission) Updated';
  bpt9.Modified_on__c= a.LastModifiedDate;
  bpt9.Modified_By__c=a.OwnerId;
  bpt9.Comments__c='Value changed from '+Trigger.old[0].Fee_Based_Non_Commission__c+' to '+ a.Fee_Based_Non_Commission__c;
  bpt9.Business_Type__c='Fee Based (Non Commission)';
  bpt9.Account__c = a.id;
  bptlist.add(bpt9);
 }
 if(a.Business_Stage__c != oldvalues.Business_Stage__c)
 {
  Business_Profile_Tracking__c bpt10 = new Business_Profile_Tracking__c();
  bpt10.name= 'Business Stage Updated';
  bpt10.Modified_on__c= a.LastModifiedDate;
  bpt10.Modified_By__c=a.OwnerId;
  bpt10.Comments__c='Value changed from '+Trigger.old[0].Business_Stage__c+' to '+ a.Business_Stage__c;
  bpt10.Business_Type__c='Business Stage';
  bpt10.Account__c = a.id;
  bptlist.add(bpt10);
 }
 // ***Below code commented by Priya for validatiion rule
 /*
 if(a.Other_Business_Stage__c != oldvalues.Other_Business_Stage__c)
 {
  Business_Profile_Tracking__c bpt11 = new Business_Profile_Tracking__c();
  bpt11.name= 'Other Business Stage Updated';
  bpt11.Modified_on__c= a.LastModifiedDate;
  bpt11.Modified_By__c=a.OwnerId;
  bpt11.Comments__c='Value changed from '+Trigger.old[0].Other_Business_Stage__c+' to '+ a.Other_Business_Stage__c;
  bpt11.Business_Type__c='Other Business Stage';
  bpt11.Account__c = a.id;
  bptlist.add(bpt11);
 }*/
 if(a.How_Can_We_Help__c !=oldvalues.How_Can_We_Help__c)
 {
  Business_Profile_Tracking__c bpt12 = new Business_Profile_Tracking__c();
  bpt12.name= 'How Can We Help Updated';
  bpt12.Modified_on__c= a.LastModifiedDate;
  bpt12.Modified_By__c=a.OwnerId;
  bpt12.Comments__c='Value changed from '+Trigger.old[0].How_Can_We_Help__c+' to '+ a.How_Can_We_Help__c;
  bpt12.Business_Type__c='How Can We Help?';
  bpt12.Account__c = a.id;
  bptlist.add(bpt12);
 }
 /*if(a.TOP5_ETF__c != oldvalues.TOP5_ETF__c)
 {
     system.debug('You just updated me...ETF');
  Business_Profile_Tracking__c bpt13 = new Business_Profile_Tracking__c();
  bpt13.name= 'ETF Updated';
  bpt13.Modified_on__c= a.LastModifiedDate;
  bpt13.Modified_By__c=a.OwnerId;
  bpt13.Comments__c='Value changed from '+Trigger.old[0].TOP5_ETF__c+' to '+ a.TOP5_ETF__c;
  bpt13.Business_Type__c='ETF';
  bpt13.Account__c = a.id;
  bptlist.add(bpt13);
 }
 if(a.TOP5_REIT__c != oldvalues.TOP5_REIT__c)
 {
  Business_Profile_Tracking__c bpt14 = new Business_Profile_Tracking__c();
  bpt14.name= 'REIT Updated';
  bpt14.Modified_on__c= a.LastModifiedDate;
  bpt14.Modified_By__c=a.OwnerId;
  bpt14.Comments__c='Value changed from '+Trigger.old[0].TOP5_REIT__c+' to '+ a.TOP5_REIT__c;
  bpt14.Business_Type__c='REIT';
  bpt14.Account__c = a.id;
  bptlist.add(bpt14);
 }
 if(a.TOP5_SMA__c != oldvalues.TOP5_SMA__c)
 {
  Business_Profile_Tracking__c bpt15 = new Business_Profile_Tracking__c();
  bpt15.name= 'SMA Updated';
  bpt15.Modified_on__c= a.LastModifiedDate;
  bpt15.Modified_By__c=a.OwnerId;
  bpt15.Comments__c='Value changed from '+Trigger.old[0].TOP5_SMA__c+' to '+ a.TOP5_SMA__c;
  bpt15.Business_Type__c='SMA';
  bpt15.Account__c = a.id;
  bptlist.add(bpt15);
 }
 if(a.Acct_Hedge_Fund__c != oldvalues.Acct_Hedge_Fund__c)
 {
  Business_Profile_Tracking__c bpt16 = new Business_Profile_Tracking__c();
  bpt16.name= 'Hedge Fund Updated';
  bpt16.Modified_on__c= a.LastModifiedDate;
  bpt16.Modified_By__c=a.OwnerId;
  bpt16.Comments__c='Value changed from '+Trigger.old[0].Acct_Hedge_Fund__c+' to '+ a.Acct_Hedge_Fund__c;
  bpt16.Business_Type__c='Hedge Fund';
  bpt16.Account__c = a.id;
  bptlist.add(bpt16);
 }
 if(a.Actively_Managed_Funds__c != oldvalues.Actively_Managed_Funds__c)
 {
  Business_Profile_Tracking__c bpt17 = new Business_Profile_Tracking__c();
  bpt17.name= 'Actively Managed Funds Updated';
  bpt17.Modified_on__c= a.LastModifiedDate;
  bpt17.Modified_By__c=a.OwnerId;
  bpt17.Comments__c='Value changed from '+Trigger.old[0].Actively_Managed_Funds__c+' to '+ a.Actively_Managed_Funds__c;
  bpt17.Business_Type__c='Actively Managed Funds';
  bpt17.Account__c = a.id;
  bptlist.add(bpt17);
 }
 if(a.TOP5_Independent_Srocks_and_Bonds__c != oldvalues.TOP5_Independent_Srocks_and_Bonds__c)
 {
  Business_Profile_Tracking__c bpt18 = new Business_Profile_Tracking__c();
  bpt18.name= 'Independent Stocks and Bonds Updated';
  bpt18.Modified_on__c= a.LastModifiedDate;
  bpt18.Modified_By__c=a.OwnerId;
  bpt18.Comments__c='Value changed from '+Trigger.old[0].TOP5_Independent_Srocks_and_Bonds__c+' to '+ a.TOP5_Independent_Srocks_and_Bonds__c;
  bpt18.Business_Type__c='Independent Stocks and Bonds';
  bpt18.Account__c = a.id;
  bptlist.add(bpt18);
 }
 if(a.Acct_Limited_Partnerships__c != oldvalues.Acct_Limited_Partnerships__c)
 {
  Business_Profile_Tracking__c bpt19 = new Business_Profile_Tracking__c();
  bpt19.name= 'Limited Partnership Updated';
  bpt19.Modified_on__c= a.LastModifiedDate;
  bpt19.Modified_By__c=a.OwnerId;
  bpt19.Comments__c='Value changed from '+Trigger.old[0].Acct_Limited_Partnerships__c+' to '+ a.Acct_Limited_Partnerships__c;
  bpt19.Business_Type__c='Limited Partnership';
  bpt19.Account__c = a.id;
  bptlist.add(bpt19);
 }
 if(a.Acct_Other_Alternative_Investments__c !=oldvalues.Acct_Other_Alternative_Investments__c)
 {
  Business_Profile_Tracking__c bpt20 = new Business_Profile_Tracking__c();
  bpt20.name= 'Other Alternate Investments Updated';
  bpt20.Modified_on__c= a.LastModifiedDate;
  bpt20.Modified_By__c=a.OwnerId;
  bpt20.Comments__c='Value changed from '+Trigger.old[0].Acct_Other_Alternative_Investments__c+' to '+ a.Acct_Other_Alternative_Investments__c;
  bpt20.Business_Type__c='Other Alternate Investments';
  bpt20.Account__c = a.id;
  bptlist.add(bpt20);
 }
 if(a.How_Long__c !=oldvalues.How_Long__c)
 {
  Business_Profile_Tracking__c bpt21 = new Business_Profile_Tracking__c();
  bpt21.name= 'How Long Updated';
  bpt21.Modified_on__c= a.LastModifiedDate;
  bpt21.Modified_By__c=a.OwnerId;
  bpt21.Comments__c='Value changed from '+Trigger.old[0].How_Long__c+' to '+ a.How_Long__c;
  bpt21.Business_Type__c='How Long?';
  bpt21.Account__c = a.id;
  bptlist.add(bpt21);
 }*/
 if(a.What_REITs__c != oldvalues.What_REITs__c)
 {
  Business_Profile_Tracking__c bpt22 = new Business_Profile_Tracking__c();
  bpt22.name= 'What REITs Updated';
  bpt22.Modified_on__c= a.LastModifiedDate;
  bpt22.Modified_By__c=a.OwnerId;
  bpt22.Comments__c='Value changed from '+Trigger.old[0].What_REITs__c+' to '+ a.What_REITs__c;
  bpt22.Business_Type__c='What REITs?';
  bpt22.Account__c = a.id;
  bptlist.add(bpt22);
 }
 if(a.Would_you_invest__c != oldvalues.Would_you_invest__c)
 {
  Business_Profile_Tracking__c bpt23 = new Business_Profile_Tracking__c();
  bpt23.name= 'Would You Invest Updated';
  bpt23.Modified_on__c= a.LastModifiedDate;
  bpt23.Modified_By__c=a.OwnerId;
  bpt23.Comments__c='Value changed from '+Trigger.old[0].Would_you_invest__c+' to '+ a.Would_you_invest__c;
  bpt23.Business_Type__c='Would You Invest?';
  bpt23.Account__c = a.id;
  bptlist.add(bpt23);
 }
 if(a.Inv_Why_Not__c != oldvalues.Inv_Why_Not__c)
 {
  Business_Profile_Tracking__c bpt24 = new Business_Profile_Tracking__c();
  bpt24.name= 'WHy Not Updated';
  bpt24.Modified_on__c= a.LastModifiedDate;
  bpt24.Modified_By__c=a.OwnerId;
  bpt24.Comments__c='Value changed from '+Trigger.old[0].Inv_Why_Not__c+' to '+ a.Inv_Why_Not__c;
  bpt24.Business_Type__c='WHy Not?';
  bpt24.Account__c = a.id;
  bptlist.add(bpt24);
 }
 if(a.What_Changes_in_Volitality__c != oldvalues.What_Changes_in_Volitality__c)
 {
  Business_Profile_Tracking__c bpt25 = new Business_Profile_Tracking__c();
  bpt25.name= 'Changes in Volitality Updated';
  bpt25.Modified_on__c= a.LastModifiedDate;
  bpt25.Modified_By__c=a.OwnerId;
  bpt25.Comments__c='Value changed from '+Trigger.old[0].What_Changes_in_Volitality__c+' to '+ a.What_Changes_in_Volitality__c;
  bpt25.Business_Type__c='Changes in Volitality';
  bpt25.Account__c = a.id;
  bptlist.add(bpt25);
 } 
 if(a.RE_Exposure_Through__c !=oldvalues.RE_Exposure_Through__c)
 {
  Business_Profile_Tracking__c bpt26 = new Business_Profile_Tracking__c();
  bpt26.name= 'RE Exposure Through Updated';
  bpt26.Modified_on__c= a.LastModifiedDate;
  bpt26.Modified_By__c=a.OwnerId;
  bpt26.Comments__c='Value changed from '+Trigger.old[0].RE_Exposure_Through__c+' to '+ a.RE_Exposure_Through__c;
  bpt26.Business_Type__c='RE Exposure Through';
  bpt26.Account__c = a.id;
  bptlist.add(bpt26);
 
}
  if(a.How_would_you_describe_your_client_base__c !=oldvalues.How_would_you_describe_your_client_base__c)
 {
  Business_Profile_Tracking__c bpt27 = new Business_Profile_Tracking__c();
  bpt27.name= 'How would you describe your client base Updated';
  bpt27.Modified_on__c= a.LastModifiedDate;
  bpt27.Modified_By__c=a.OwnerId;
  bpt27.Comments__c='Value changed from '+Trigger.old[0].How_would_you_describe_your_client_base__c  +' to '+ a.How_would_you_describe_your_client_base__c ;
  bpt27.Business_Type__c='How would you describe your client base?';
  bpt27.Account__c = a.id;
  bptlist.add(bpt27);
 
}  



}
}
else if(Trigger.isinsert){
if(helper.run == TRUE)
{



  /*Business_Profile_Tracking__c inbpt = new Business_Profile_Tracking__c();
  inbpt.name= 'Used REIT Before Created';
  inbpt.Modified_on__c= a.LastModifiedDate;
  inbpt.Modified_By__c= a.OwnerId;
  inbpt.Comments__c='Used REIT Before field Created : ' + a.Used_REIT_Before__c;
  inbpt.Business_Type__c='Used Nonlisted REIT before?';
  inbpt.Account__c = a.id;
  bptlist1.add(inbpt);*/
  
  Business_Profile_Tracking__c inbpt1 = new Business_Profile_Tracking__c();
  inbpt1.name= 'Income Products Used Created';
  inbpt1.Modified_on__c= a.LastModifiedDate;
  inbpt1.Modified_By__c= a.OwnerId;
  inbpt1.Comments__c='Income Products Used created : ' + a.Income_Products_Used__c;
  inbpt1.Business_Type__c='Income Products Used';
  inbpt1.Account__c = a.id;
  bptlist1.add(inbpt1);
  
  Business_Profile_Tracking__c inbpt2 = new Business_Profile_Tracking__c();
  inbpt2.name= 'Total AUM Created';
  inbpt2.Modified_on__c= a.LastModifiedDate;
  inbpt2.Modified_By__c= a.OwnerId;
  inbpt2.Comments__c='Total AUM field created : ' + a.BusSize_Total_AUM__c;
  inbpt2.Business_Type__c='Total AUM';
  inbpt2.Account__c = a.id;
  bptlist1.add(inbpt2);
  
  Business_Profile_Tracking__c inbpt3 = new Business_Profile_Tracking__c();
  inbpt3.name= 'Clients with RE in Portfolio Created';
  inbpt3.Modified_on__c= a.LastModifiedDate;
  inbpt3.Modified_By__c= a.OwnerId;
  inbpt3.Comments__c='Clients with RE in Portfolio field created : ' + a.TOP5_of_Clients_with_RE_in_Portfolio__c;
  inbpt3.Business_Type__c='% of Clients with RE in Portfolio';
  inbpt3.Account__c = a.id;
  bptlist1.add(inbpt3);
  
  /*Business_Profile_Tracking__c inbpt4 = new Business_Profile_Tracking__c();
  inbpt4.name= 'Other RE Exposure Created';
  inbpt4.Modified_on__c= a.LastModifiedDate;
  inbpt4.Modified_By__c= a.OwnerId;
  inbpt4.Comments__c='Other RE Exposure field created : ' + a.Othe_RE_Exposure__c;
  inbpt4.Business_Type__c='Other RE Exposure';
  inbpt4.Account__c = a.id;
  bptlist1.add(inbpt4);*/
  
  Business_Profile_Tracking__c inbpt5 = new Business_Profile_Tracking__c();
  inbpt5.name= 'Allocated to RE';
  inbpt5.Modified_on__c= a.LastModifiedDate;
  inbpt5.Modified_By__c= a.OwnerId;
  inbpt5.Comments__c='Allocated to RE field created : ' + a.Bus_SizeAllocated_to_RE__c;
  inbpt5.Business_Type__c='Allocated to RE';
  inbpt5.Account__c = a.id;
  bptlist.add(inbpt5);
  
  Business_Profile_Tracking__c inbpt6 = new Business_Profile_Tracking__c();
  inbpt6.name= 'Total Sales Last Year Created';
  inbpt6.Modified_on__c= a.LastModifiedDate;
  inbpt6.Modified_By__c= a.OwnerId;
  inbpt6.Comments__c='Total Sales Last Year field created : ' + a.Total_Sales_Last_Year_RIA__c;
  inbpt6.Business_Type__c='Total Sales Last Year';
  inbpt6.Account__c = a.id;
  bptlist1.add(inbpt6);
  
  Business_Profile_Tracking__c inbpt7 = new Business_Profile_Tracking__c();
  inbpt7.name= 'Fee Based (Non Commission) Created';
  inbpt7.Modified_on__c= a.LastModifiedDate;
  inbpt7.Modified_By__c= a.OwnerId;
  inbpt7.Comments__c='Fee Based (Non Commission) Based field created : ' + a.Fee_Based_Non_Commission__c;
  inbpt7.Business_Type__c='Fee Based (Non Commission)';
  inbpt7.Account__c = a.id;
  bptlist.add(inbpt7);
  
  Business_Profile_Tracking__c inbpt8 = new Business_Profile_Tracking__c();
  inbpt8.name= 'Business Stage Created';
  inbpt8.Modified_on__c= a.LastModifiedDate;
  inbpt8.Modified_By__c= a.OwnerId;
  inbpt8.Comments__c='Business Stage field created : ' + a.Business_Stage__c;
  inbpt8.Business_Type__c='Business Stage';
  inbpt8.Account__c = a.id;
  bptlist1.add(inbpt8);
  
  /*Business_Profile_Tracking__c inbpt9 = new Business_Profile_Tracking__c();
  inbpt9.name= 'Other Business Stage Created';
  inbpt9.Modified_on__c= a.LastModifiedDate;
  inbpt9.Modified_By__c= a.OwnerId;
  inbpt9.Comments__c='Other Business Stage field created : ' + a.Other_Business_Stage__c;
  inbpt9.Business_Type__c='Other Business Stage';
  inbpt9.Account__c = a.id;
  bptlist.add(inbpt9);*/
  
  Business_Profile_Tracking__c inbpt10 = new Business_Profile_Tracking__c();
  inbpt10.name= 'How Can We Help Created';
  inbpt10.Modified_on__c= a.LastModifiedDate;
  inbpt10.Modified_By__c= a.OwnerId;
  inbpt10.Comments__c='How Can We Help field created : ' + a.How_Can_We_Help__c;
  inbpt10.Business_Type__c='How Can We Help?';
  inbpt10.Account__c = a.id;
  bptlist1.add(inbpt10);
  
 /* Business_Profile_Tracking__c inbpt11 = new Business_Profile_Tracking__c();
  inbpt11.name= 'ETF Created';
  inbpt11.Modified_on__c= a.LastModifiedDate;
  inbpt11.Modified_By__c= a.OwnerId;
  inbpt11.Comments__c='ETF field created : ' + a.TOP5_ETF__c;
  inbpt11.Business_Type__c='ETF';
  inbpt11.Account__c = a.id;
  bptlist1.add(inbpt11);
  
  Business_Profile_Tracking__c inbpt12 = new Business_Profile_Tracking__c();
  inbpt12.name= 'REIT Created';
  inbpt12.Modified_on__c= a.LastModifiedDate;
  inbpt12.Modified_By__c= a.OwnerId;
  inbpt12.Comments__c='REIT field created : ' + a.TOP5_REIT__c;
  inbpt12.Business_Type__c='REIT';
  inbpt12.Account__c = a.id;
  bptlist1.add(inbpt12);
  
  Business_Profile_Tracking__c inbpt13 = new Business_Profile_Tracking__c();
  inbpt13.name= 'SMA Created';
  inbpt13.Modified_on__c= a.LastModifiedDate;
  inbpt13.Modified_By__c= a.OwnerId;
  inbpt13.Comments__c='SMA field created : ' + a.TOP5_SMA__c;
  inbpt13.Business_Type__c='SMA';
  inbpt13.Account__c = a.id;
  bptlist1.add(inbpt13);
  
  Business_Profile_Tracking__c inbpt14 = new Business_Profile_Tracking__c();
  inbpt14.name= 'Hedge Fund Created';
  inbpt14.Modified_on__c= a.LastModifiedDate;
  inbpt14.Modified_By__c= a.OwnerId;
  inbpt14.Comments__c='Hedge Fund field created : ' + a.Acct_Hedge_Fund__c;
  inbpt14.Business_Type__c='Hedge Fund';
  inbpt14.Account__c = a.id;
  bptlist1.add(inbpt14);
  
  Business_Profile_Tracking__c inbpt15 = new Business_Profile_Tracking__c();
  inbpt15.name= 'Actively Managed Funds Created';
  inbpt15.Modified_on__c= a.LastModifiedDate;
  inbpt15.Modified_By__c= a.OwnerId;
  inbpt15.Comments__c='Actively Managed Funds field created : ' + a.Actively_Managed_Funds__c;
  inbpt15.Business_Type__c='Actively Managed Funds';
  inbpt15.Account__c = a.id;
  bptlist1.add(inbpt15);
  
  Business_Profile_Tracking__c inbpt16 = new Business_Profile_Tracking__c();
  inbpt16.name= 'Independent Stocks and Bonds Created';
  inbpt16.Modified_on__c= a.LastModifiedDate;
  inbpt16.Modified_By__c= a.OwnerId;
  inbpt16.Comments__c='Independent Stocks and Bonds field created : ' + a.TOP5_Independent_Srocks_and_Bonds__c;
  inbpt16.Business_Type__c='Independent Stocks and Bonds';
  inbpt16.Account__c = a.id;
  bptlist1.add(inbpt16);
  
  Business_Profile_Tracking__c inbpt17 = new Business_Profile_Tracking__c();
  inbpt17.name= 'Limited Partnership  Created';
  inbpt17.Modified_on__c= a.LastModifiedDate;
  inbpt17.Modified_By__c= a.OwnerId;
  inbpt17.Comments__c='Limited Partnership  field created : ' + a.Acct_Limited_Partnerships__c;
  inbpt17.Business_Type__c='Limited Partnership ';
  inbpt17.Account__c = a.id;
  bptlist1.add(inbpt17);
  
  Business_Profile_Tracking__c inbpt18 = new Business_Profile_Tracking__c();
  inbpt18.name= 'Other Alternate Investments Created';
  inbpt18.Modified_on__c= a.LastModifiedDate;
  inbpt18.Modified_By__c= a.OwnerId;
  inbpt18.Comments__c='Other Alternate Investments field created : ' + a.Acct_Other_Alternative_Investments__c;
  inbpt18.Business_Type__c='Other Alternate Investments';
  inbpt18.Account__c = a.id;
  bptlist1.add(inbpt18);
  
  Business_Profile_Tracking__c inbpt19 = new Business_Profile_Tracking__c();
  inbpt19.name= 'How Long Created';
  inbpt19.Modified_on__c= a.LastModifiedDate;
  inbpt19.Modified_By__c= a.OwnerId;
  inbpt19.Comments__c='How Long? field created : ' + a.How_Long__c;
  inbpt19.Business_Type__c='How Long?';
  inbpt19.Account__c = a.id;
  bptlist1.add(inbpt19);*/
  
  Business_Profile_Tracking__c inbpt20 = new Business_Profile_Tracking__c();
  inbpt20.name= 'What REITs Created';
  inbpt20.Modified_on__c= a.LastModifiedDate;
  inbpt20.Modified_By__c= a.OwnerId;
  inbpt20.Comments__c='What REITs field created : ' + a.What_REITs__c;
  inbpt20.Business_Type__c='What REITs?';
  inbpt20.Account__c = a.id;
  bptlist1.add(inbpt20);
  
  Business_Profile_Tracking__c inbpt21 = new Business_Profile_Tracking__c();
  inbpt21.name= 'Would You Invest Created';
  inbpt21.Modified_on__c= a.LastModifiedDate;
  inbpt21.Modified_By__c= a.OwnerId;
  inbpt21.Comments__c='Would You Invest field created : ' + a.Would_you_invest__c;
  inbpt21.Business_Type__c='Would You Invest?';
  inbpt21.Account__c = a.id;
  bptlist1.add(inbpt21);
  
  Business_Profile_Tracking__c inbpt22 = new Business_Profile_Tracking__c();
  inbpt22.name= 'WHy Not Created';
  inbpt22.Modified_on__c= a.LastModifiedDate;
  inbpt22.Modified_By__c= a.OwnerId;
  inbpt22.Comments__c='WHy Not field created : ' + a.Inv_Why_Not__c;
  inbpt22.Business_Type__c='WHy Not?';
  inbpt22.Account__c = a.id;
  bptlist1.add(inbpt22);
  
  Business_Profile_Tracking__c inbpt23 = new Business_Profile_Tracking__c();
  inbpt23.name= 'Changes in Volitality Created';
  inbpt23.Modified_on__c= a.LastModifiedDate;
  inbpt23.Modified_By__c= a.OwnerId;
  inbpt23.Comments__c='Changes in Volitality field created : ' + a.What_Changes_in_Volitality__c;
  inbpt23.Business_Type__c='Changes in Volitality';
  inbpt23.Account__c = a.id;
  bptlist1.add(inbpt23);
  
  Business_Profile_Tracking__c inbpt24 = new Business_Profile_Tracking__c();
  inbpt24.name= 'RE Exposure Through Created';
  inbpt24.Modified_on__c= a.LastModifiedDate;
  inbpt24.Modified_By__c= a.OwnerId;
  inbpt24.Comments__c='RE Exposure Through field created : ' + a.RE_Exposure_Through__c;
  inbpt24.Business_Type__c='RE Exposure Through';
  inbpt24.Account__c = a.id;
  bptlist1.add(inbpt24);
  
  Business_Profile_Tracking__c bpt27 = new Business_Profile_Tracking__c();
  bpt27.name= 'How would you describe your client base Created';
  bpt27.Modified_on__c= a.LastModifiedDate;
  bpt27.Modified_By__c=a.OwnerId;
  bpt27.Comments__c='How would you describe your client base Created : '+ a.How_would_you_describe_your_client_base__c ;
  bpt27.Business_Type__c='How would you describe your client base?';
  bpt27.Account__c = a.id;
  bptlist.add(bpt27);
 
  
 
}
}
}
  insert bptlist;
  insert bptlist1;
}
}