trigger submitforApprovaltrg on Employee__c (after insert,after update) 
{
    List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();
    List<Approval.ProcessSubmitRequest> approvalReqListemp=new List<Approval.ProcessSubmitRequest>();
    List<Approval.ProcessSubmitRequest> approvalReqList1=new List<Approval.ProcessSubmitRequest>();
    List<Approval.ProcessSubmitRequest> approvalReqList2=new List<Approval.ProcessSubmitRequest>();
    List<Approval.ProcessSubmitRequest> MRIApprovalReqList2=new List<Approval.ProcessSubmitRequest>();
    public list<Asset__c>assetlist= new list<Asset__c>(); 
    list<Asset__c>UpdateAssetlist= new list<Asset__c>();
    list<Asset__c>approvedassetlist= new list<Asset__c>();
    list<Asset__c>changeUserlist=new list<Asset__c>();
    list<Asset__c>MRIOptionschangelist=new list<Asset__c>();
    set<id>empid= new set<id>();
    map<id,list<Asset__c >>assetToEmployeeList= new map<id,list<Asset__c >>();
    for(Employee__c e:Trigger.new) 
    {
        if((trigger.isUpdate)&&(e.HR_Manager_Status__c == 'Approved')&&(e.Employee_Status__c=='On Boarding inProcess'))
        {
             empid.add(e.id);
            //getComments.GetmetheApprovalhistory11(e.id); 
            system.debug('Am I getting the id?'+empid); 
            // List<Employee__c>EmpAssets=[Select c.Id,c.HR_Manager_Status__c,(Select Id, IsPending, ProcessInstanceId, TargetObjectId, StepStatus, OriginalActorId, ActorId, RemindersSent, Comments, IsDeleted, CreatedDate, CreatedById, SystemModstamp From ProcessSteps where stepstatus='Approved' )From Employee__c c where id=:e.id and HR_Manager_Status__c='Approved']; 
            //system.debug('get the assets list.'+EmpAssets[0].ProcessSteps);
        }
    }
    for(Employee__c e:Trigger.new) 
    {
        if((trigger.isUpdate)&&(e.HR_Manager_Status__c == 'Approved')&&(e.Employee_Status__c=='On Boarding inProcess')&&(e.Employee_Final_Step_Processed__c==true))
        {
            empid.add(e.id);
            getComments.GetmetheApprovalhistory11(e.id); 
            system.debug('Am I getting the id?'+empid); 
            // List<Employee__c>EmpAssets=[Select c.Id,c.HR_Manager_Status__c,(Select Id, IsPending, ProcessInstanceId, TargetObjectId, StepStatus, OriginalActorId, ActorId, RemindersSent, Comments, IsDeleted, CreatedDate, CreatedById, SystemModstamp From ProcessSteps where stepstatus='Approved' )From Employee__c c where id=:e.id and HR_Manager_Status__c='Approved']; 
            //system.debug('get the assets list.'+EmpAssets[0].ProcessSteps);
        }
    }
    assetlist=[select id, Asset_Status__c,Added_On__c,Application_Owner__c ,Application_Implementor__c from Asset__c where Employee__c =:Trigger.new[0].id and Added_On__c=null and Asset_Status__c='In Process' and Termination_Status__c='Inactive' ];
    //approvedassetlist=[select id, Asset_Status__c,Added_On__c from Asset__c where Employee__c =:Trigger.new[0] and Added_On__c!=null and Asset_Status__c='Added'];
    changeUserlist=[select id, Asset_Status__c,Added_On__c,Updating__c,Application_Owner__c ,Application_Implementor__c from Asset__c where Employee__c =:Trigger.new[0].id and Added_On__c=null and Asset_Status__c='InActive' and Updating__c=true and Termination_Status__c='Inactive'];
    MRIOptionschangelist=[select Access_Name__c ,Options_Changed__c,Termination_Status__c ,id,Asset_Status__c,Added_On__c,Updating__c from Asset__c where Employee__c =:Trigger.new[0].id and Asset_Status__c='Option Changes in Process' and Options_Changed__c=true and Updating__c=true and Termination_Status__c='Inactive'];
    system.debug('Check the list size'+changeUserlist);
    for(Employee__c e:Trigger.new)
    {
        if(trigger.isUpdate)
        { 
            if((e.HR_Manager_Status__c == 'Submitted for Approval')&&(e.HR_step_1_submission_time__c == null))
            {
                //create the new approval request to submit 
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest(); 
                //req.setComments('Submitted for approval. Please approve.');
                req.setObjectId(e.Id);
                approvalReqList.add(req); 
            }
        
                // submit the approval request for processing 
                List<Approval.ProcessResult> resultList = Approval.process(approvalReqList); 
                //List<Approval.ProcessResult> resultListemp = Approval.process(approvalReqListemp); 
                // display if the reqeust was successful
                for(Approval.ProcessResult result: resultList)
                { 
                    System.debug('--------Submitted for approval successfully:------> '+result.isSuccess()); 
                    system.debug('-------Check the approvalhistory-----------> '+result); 
                }

        //system.debug('I am in Update loop');
        if((e.HR_Manager_Status__c == 'Approved')&&(e.Employee_Status__c=='On Boarding inProcess')&&(assetlist.size()>0))
        { 
            system.debug('Can we check now..'+assetlist); 
            for(Asset__c ct11:assetlist)
            {
            
                system.debug('Have I entered this ugly loop'); 
                //system.debug('Check the value of'+ct11.Asset_Status__c);
                //system.debug('Check the owner'+ct11.Application_Owner__c);
                //system.debug('Check the Implementor'+ct11.Application_Implementor__c);
                if(ct11.Asset_Status__c=='In Process')
                {
                    system.debug('I am in the updating loop'+ct11);
                    Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest(); 
                    req1.setComments(' Access Approval Process'); 
                    req1.setObjectId(ct11.Id); 
                    approvalReqList1.add(req1);
                    //UtilClass.isSubmitted = true;
                } 
            }
        } 
        for(Asset__c ct12:changeUserlist)
        {
            //system.debug('Check the value of'+ct12.Asset_Status__c);
            //system.debug('Check the owner'+ct12.Application_Owner__c);
            //system.debug('Check the Implementor'+ct12.Application_Implementor__c);
            if((ct12.Asset_Status__c=='InActive')&&(ct12.Added_On__c==null )&&(ct12.Updating__c=true ))
            {
                system.debug('I am in the updating loop'+ct12);
                Approval.ProcessSubmitRequest req2 = new Approval.ProcessSubmitRequest(); 
                req2.setComments('Access Approval Process'); 
                req2.setObjectId(ct12.Id); 
                approvalReqList2.add(req2);
                
            } 
        }
        for(Asset__c ct123:MRIOptionschangelist)
        {
            if((ct123.Asset_Status__c=='Option Changes in Process')&&(ct123.Options_Changed__c==true)&&(ct123.Updating__c==true)&&(ct123.Termination_Status__c=='Inactive'))
            { 
                Approval.ProcessSubmitRequest req223 = new Approval.ProcessSubmitRequest(); 
                req223.setObjectId(ct123.Id); 
                MRIApprovalReqList2.add(req223);
            } 
        
        }
        List<Approval.ProcessResult> resultList1 = Approval.process(approvalReqList1); 
        List<Approval.ProcessResult> resultList2 = Approval.process(approvalReqList2);
        List<Approval.ProcessResult> resultList3 = Approval.process(MRIApprovalReqList2);
        
        for(Approval.ProcessResult result: resultList1)
        { 
        System.debug('--------Submitted for approval successfully:------> '+result.isSuccess()); 
        system.debug('-------Check the approvalhistory-----------> '+result); 
        }
    }
} 

}